# 맘시터 관리자용 웹 어플리케이션

# Changelog
관리자 웹 어플리케이션의 업데이트 내용을 기록

- 변경사항은 사람을 위해서 작성하는 것이지, 기계를 위해서 작성하는 것이 아닙니다.
- 버전별로 변경사항이 기록되어있어야합니다.
-


아래는 그 예시임
### Added
새로운 기능이 추가됨
### Changed
기존 기능이 변경됨

### Removed
기존 기능이 삭제됨
### Fixed
기존에 있던 버그가 고쳐짐
### Deprecated
기존에 있던 기능이 Deprecated됨
### Security
보안에 취약함



## [Unreleased]

## [2.0.0] - 2018-09-12
### Added
### Changed
### Removed
### Fixed
### Deprecated
### Security

