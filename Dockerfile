FROM node:carbon

# Set a working directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY ./build/package.json .
COPY ./build/yarn.lock .

# Install Node.js dependencies
RUN yarn install --production --no-progress

# Copy application files
COPY ./build .

EXPOSE 3000

CMD [ "node", "server.js" ]
