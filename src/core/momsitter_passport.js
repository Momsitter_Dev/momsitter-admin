/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

/**
 * Passport.js reference implementation.
 * The database schema used in this sample is available at
 * https://github.com/membership/membership.db/tree/master/postgres
 */

import passport from 'passport';
import { Admin } from '../data/models/index';
import { Strategy as JwtStrategy, ExtractJwt } from 'passport-jwt';
import Debug from 'debug';

const debug = Debug('momsitter:passport');
const jwtOpts = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: 'momsitter-admin',
};

passport.use(
  new JwtStrategy(jwtOpts, async (payload, done) => {
    try {
      const user = await Admin.findOne({
        where: {
          adminKey: payload.adminKey,
        },
      });

      if (!user) {
        return done(null, false);
      }
      return done(null, {
        adminId: user.adminId,
        adminKey: user.adminKey,
        adminName: user.adminName,
      });
    } catch (err) {
      debug(err);
      return done(err);
    }
  }),
);

/*
passport.serializeUser((user, cb) => {
  cb(null, user.adminId);
});

passport.deserializeUser((id, cb) => {
  Admin.findByPrimary(id).then(data => cb(null, data.dataValues));
});
*/

export default passport;
