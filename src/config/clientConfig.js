const clientConfig = {};

if (process.env.BROWSER) {
  // Note. 브라우저 레벨에서 설정값을 사용할 때
  if (!window) {
    throw new Error('The window object is undefined. Check the DOM is completely loaded.');
  }
  if (!window.APP_CONFIG) {
    throw new Error('not initialized APP_CONFIG');
  }

  const { domain, port, apiServer, appServer, addressServer } = window.APP_CONFIG;

  clientConfig.domain = domain;
  clientConfig.port = port;
  clientConfig.url = `${domain}:${port}`;
  clientConfig.apiServer = apiServer;
  clientConfig.appServer = appServer;
  clientConfig.addressServer = addressServer;
} else {
  // Note. 서버사이드 렌더링 레벨에서 설정값을 사용할 때
  // eslint-disable-next-line global-require
  const serverConfig = require('../config/config').default;

  const { domain, port, apiServer, appServer, addressServer } = serverConfig;
  clientConfig.domain = domain;
  clientConfig.port = port;
  clientConfig.url = `${domain}:${port}`;
  clientConfig.apiServer = apiServer;
  clientConfig.appServer = appServer;
  clientConfig.addressServer = addressServer;
}

export default clientConfig;
