export default {
  appServer: process.env.APP_SERVER || 'http://13.124.243.28:5000',
  apiServer: process.env.API_SERVER || 'http://13.124.243.28:5000',
  // appServer: process.env.APP_SERVER || 'http://13.124.174.138',
  // apiServer: process.env.API_SERVER || 'http://13.124.174.138:5000',
  messageServer: process.env.MESSAGE_SERVER || 'http://13.124.174.138:5000/message',
  addressServer: process.env.ADDRESS_SERVER || 'http://13.124.174.138:5000/address',

  site: {
    domain: process.env.SITE_DOMAIN || 'http://13.124.243.28',
    port: process.env.SITE_PORT || 3000,
  },

  database: {
    host: process.env.DB_HOST || 'momsitter-dev-20181101.cbycsob2qplw.ap-northeast-2.rds.amazonaws.com',
    port: process.env.DB_PORT || '3306',
    database: process.env.DB_NAME || 'momsitter',
    user: process.env.DB_USER || 'momsitter',
    password: process.env.DB_PASSWORD || 'momsitter1811%$',
    dialect: process.env.DB_DIALECT || 'mysql',
    pool_max: process.env.DB_POOL_MAX || '',
    pool_min: process.env.DB_POOL_MIN || '',
    idleTimeoutMillis: process.env.DB_IDEL_TIMEOUT || '',
    logging: process.env.DB_LOGGING || true,
    timezone: process.env.DB_TIMEZONE || 'Asia/Seoul',
    appLog: {
      host: 'momsitter-mysql.mysql.database.azure.com',
      port: '3306',
      database: 'momsitter_app_log',
      user: 'momsitter@momsitter-mysql',
      password: 'akatlxj16!',
      dialect: 'mysql',
      pool_max: process.env.DB_POOL_MAX || '',
      pool_min: process.env.DB_POOL_MIN || '',
      idleTimeoutMillis: process.env.DB_IDEL_TIMEOUT || '',
      logging: process.env.DB_LOGGING || true,
      timezone: process.env.DB_TIMEZONE || 'Asia/Seoul',
    },
  },
  api: {
    google: {
      key: process.env.GOOGLE_API_KEY || 'AIzaSyAsXgJPuyy94_wKsCBVcJy5SYZVbr-ebck',
    },
  },
  sentry: {
    dsn: process.env.SENTRY_DSN || 'https://02497bfd382242b6a03b4a0bd0648655@sentry.io/1221184',
    release: process.env.SENTRY_RELEASE || 'no-release',
    environment: process.env.SENTRY_ENV || 'develop',
  },
};
