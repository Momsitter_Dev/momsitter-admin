import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'CustomLog',
  {
    customLogId: {
      type: DataType.INTEGER,
      defaultValue: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    sessionId: {
      type: DataType.STRING(50),
    },
    userId: {
      type: DataType.INTEGER,
    },
    logDate: {
      type: DataType.DATE,
    },
    remoteIp: {
      type: DataType.STRING(100),
    },
    currentUrl: {
      type: DataType.STRING(500),
    },
    userAgent: {
      type: DataType.STRING(200),
    },
    logType: {
      type: DataType.STRING(50),
    },
    value: {
      type: DataType.STRING(500),
    },
    attr1: {
      type: DataType.INTEGER,
    },
    platform: {
      type: DataType.STRING(50),
    },
    os: {
      type: DataType.STRING(50),
    },
    osVersion: {
      type: DataType.STRING(50),
    },
    osString: {
      type: DataType.STRING(50),
    },
    browser: {
      type: DataType.STRING(50),
    },
    browserVersion: {
      type: DataType.STRING(50),
    },
    browserString: {
      type: DataType.STRING(50),
    },
    device: {
      type: DataType.STRING(50),
    },
    deviceVersion: {
      type: DataType.STRING(50),
    },
    deviceString: {
      type: DataType.STRING(50),
    },
    utm_source: {
      type: DataType.STRING(50),
    },
    utm_medium: {
      type: DataType.STRING(50),
    },
    utm_campaign: {
      type: DataType.STRING(50),
    },
    utm_term: {
      type: DataType.STRING(50),
    },
    n_media: {
      type: DataType.STRING(50),
    },
    n_query: {
      type: DataType.STRING(50),
    },
    n_rank: {
      type: DataType.STRING(50),
    },
    n_ad_group: {
      type: DataType.STRING(50),
    },
    n_ad: {
      type: DataType.STRING(50),
    },
    n_keyword_id: {
      type: DataType.STRING(50),
    },
    n_keyword: {
      type: DataType.STRING(50),
    },
    n_campaign_type: {
      type: DataType.STRING(50),
    },
    NaPm: {
      type: DataType.STRING(50),
    },
    query: {
      type: DataType.STRING(50),
    },
    oquery: {
      type: DataType.STRING(50),
    },
    q: {
      type: DataType.STRING(50),
    },
    oq: {
      type: DataType.STRING(50),
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_custom_log',
  }
)
