/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */
/**
 *  @description initialize Sequelize.js
 *  @ref  http://docs.sequelizejs.com/en/v3/api/sequelize/
 **/
import Sequelize from 'sequelize';
import config from '../../config/config';

const sequelize = new Sequelize(
  config.database.appLog.database,
  config.database.appLog.user,
  config.database.appLog.password,
  {
    define: {
      freezeTableName: true,
      timestamps: false,
      underscored: false,
      underscoredAll: false,
      engine: 'InnoDB',
      charset: 'utf8',
    },
    host: config.database.appLog.host,
    dialect: config.database.appLog.dialect,
    logging: config.database.appLog.logging,
    timezone: config.database.appLog.timezone,
  },
);

export default sequelize;
