export default function (where, order, limit) {
  return `
  SELECT *
	FROM (
		SELECT userRole.*,
			profile.profileId,
			profile.viewCount,
			DATE_FORMAT(profile.createDate, '%Y-%m-%d %H:%i:%s') AS createDate,
			DATE_FORMAT(profile.updateDate, '%Y-%m-%d %H:%i:%s') AS updateDate,
			profile.profileStatus,
			profile.profileTitle,
			profile.profileDescription,
			profile.wantedPayment,
			profile.responseRate,
			profile.responseTime,
			profile.searchable,
			profile.cctv
			FROM momsitter_sitter_profile AS profile
			RIGHT JOIN (
				SELECT 
          mu.userId,
				  mu.userKey,
				  mu.userSnsKey,
				  mu.userName,
				  mu.userPassword,
				  DATE_FORMAT(mu.userSignUpDate, '%Y-%m-%d %H:%i:%s') AS userSignUpDate,
				  mu.userBirthday,
				  mu.userAge,
				  mu.userConnectType,
				  mu.userPhone,
				  mu.userGender,
				  mu.userEmail,
				  mu.userDup,
				  mu.userNationality,
				  mu.userAssignmentTos,
				  mu.userAssignmentPp,
				  mu.userAssignmentMsg,
				  mu.userStatus,
				  mu.userSearchable,
				  mu.contactWay,
				  mu.contactWayDetail,
				  mu.joinType,
				  mu.isOwnPhone,
				  role.userTypeId, 
				  role.userDetailTypeDesc
					FROM momsitter_user AS mu
					JOIN (
							SELECT mr.userId, mr.userTypeId, userDetailTypeDesc
								FROM momsitter_user_role AS mr
								JOIN momsitter_user_type AS mt
								ON mr.userTypeId = mt.userTypeId
					) AS role
					on mu.userId = role.userId
					WHERE userTypeId != 5
			) AS userRole
			ON profile.userId = userRole.userId
	) AS wrap
	WHERE 1 = 1 
	${where}
	${order}
	${limit} 
  `;
};
