export default function (where) {
  return `
  SELECT COUNT(*) AS CNT
	FROM (
		SELECT 
			t1.*,
			t2.userId AS msgReceiveUserId,
			t2.userName As msgReceiveUserName
			FROM (
				SELECT 
					t1.messageId,
					t1.senderId,
					t1.receiverId,
					t1.msgType,
					t1.to,
					t1.msg,
					DATE_FORMAT(t1.sendDate, '%Y-%m-%d %H:%i:%s') AS sendDate,
					t1.attribute,
					t2.userId AS msgSendUserId,
					t2.userName AS msgSendUserName 
					FROM momsitter_log_message AS t1
					LEFT JOIN momsitter_user AS t2
					ON t1.senderId = t2.userId
			) AS t1
			LEFT JOIN momsitter_user AS t2
			ON t1.receiverId = t2.userId
	) AS t1
	WHERE 1 = 1
  ${where}
  `;
}
