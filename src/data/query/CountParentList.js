export default function (where) {
  return `
  SELECT COUNT(*) AS CNT
	FROM (
		SELECT userRole.*,
			profile.profileId,
			profile.viewCount,
			profile.createDate,
			profile.updateDate,
			profile.lastFinishedDate,
			profile.profileStatus,
			profile.profileTitle,
			profile.profileDescription,
			profile.wantedPayment,
			profile.wantedInterviewWay,
			profile.wantedSitterGender,
			profile.wantedCareWay,
			profile.longTerm,
			profile.paymentNegotiable,
			profile.durationNegotiable,
			profile.weekdayNegotiable,
			profile.timeNegotiable,
			profile.moreThanThreeMonth,
			profile.childCount,
			profile.preferMinAge,
			profile.preferMaxAge
			FROM momsitter_parent_profile AS profile
			RIGHT JOIN (
				SELECT mu.*, role.userTypeId, role.userDetailTypeDesc
					FROM momsitter_user AS mu
					JOIN (
							SELECT mr.userId, mr.userTypeId, userDetailTypeDesc
								FROM momsitter_user_role AS mr
								JOIN momsitter_user_type AS mt
								ON mr.userTypeId = mt.userTypeId
					) AS role
					on mu.userId = role.userId
					WHERE userTypeId = 5
			) AS userRole
			ON profile.userId = userRole.userId
	) AS wrap
	WHERE 1 = 1
	${where}
  `;
};
