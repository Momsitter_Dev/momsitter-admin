export default function (where, order, limit) {
  return `
  SELECT 
    userId,
    userName,
    userStatus,
    userGender,
    DATE_FORMAT(userSignupDate, '%Y-%m-%d %H:%i:%s') AS userSignupDate,
    userBirthDay,
    userPhone,
    outReason,
    status,
    DATE_FORMAT(regDate, '%Y-%m-%d %H:%i:%s') AS regDate,
    DATE_FORMAT(doneDate, '%Y-%m-%d %H:%i:%s') AS doneDate,
    userTypeId,
    sitterProfileStatus,
    parentProfileStatus
    FROM (
      SELECT t2.*, t1.profileStatus AS parentProfileStatus
        FROM momsitter_parent_profile AS t1
        RIGHT JOIN (
          SELECT t2.*, t1.profileStatus AS sitterProfileStatus
            FROM momsitter_sitter_profile AS t1
            RIGHT JOIN (
              SELECT t2.*, t1.userTypeId
                FROM momsitter_user_role AS t1
                JOIN (
                  SELECT 
                    t1.userId,
                    t1.userName,
                    t1.userStatus,
                    t1.userGender,
                    t1.userSignupDate,
                    t1.userBirthDay,
                    t1.userPhone,
                    t2.outReason,
                    t2.regDate,
                    t2.status,
                    t2.doneDate
                    FROM momsitter_user AS t1
                    JOIN momsitter_user_out_request AS t2
                    ON t1.userId = t2.userId
                ) AS t2
                ON t1.userId = t2.userId
            ) AS t2
            ON t1.userId = t2.userId
        ) AS t2
        ON t1.userId = t2.userId
    ) AS final
    WHERE 1 = 1
  ${where || ''}
  ${order || ''}
  ${limit || ''} 
  `;
}
