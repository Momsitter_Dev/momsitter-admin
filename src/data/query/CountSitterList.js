export default function (where) {
  return `
  SELECT COUNT(*) AS CNT
	FROM (
  	SELECT userRole.*,
			profile.profileId,
			profile.viewCount,
			profile.createDate,
			profile.updateDate,
			profile.profileStatus,
			profile.profileTitle,
			profile.profileDescription,
			profile.wantedPayment,
			profile.responseRate,
			profile.responseTime,
			profile.searchable,
			profile.cctv
			FROM momsitter_sitter_profile AS profile
			RIGHT JOIN (
				SELECT mu.*, role.userTypeId, role.userDetailTypeDesc
					FROM momsitter_user AS mu
					JOIN (
							SELECT mr.userId, mr.userTypeId, userDetailTypeDesc
								FROM momsitter_user_role AS mr
								JOIN momsitter_user_type AS mt
								ON mr.userTypeId = mt.userTypeId
					) AS role
					on mu.userId = role.userId
					WHERE userTypeId != 5
			) AS userRole
			ON profile.userId = userRole.userId
	) AS wrap
	WHERE 1 = 1
	${where}
  `;
};
