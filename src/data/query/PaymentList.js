export default function (where, order, limit) {
  return `
  SELECT 
    final.userId,
    final.userName,
    final.userPhone,
    final.userTypeId,
    final.paymentId,
    final.paymentStatus,
    DATE_FORMAT(final.paymentRequestDate, '%Y-%m-%d %H:%i:%s') AS paymentRequestDate,
    DATE_FORMAT(final.paymentDoneDate, '%Y-%m-%d %H:%i:%s') AS paymentDoneDate,
    final.paymentPrice,
    final.couponDiscount,
    final.pointDiscount,
    final.card_name,
    final.productCode,
    final.productName,
    final.productPrice,
    final.productFor,
    final.productPeriod
	FROM (
		SELECT 
			t1.*,
			t2.productId, 
			t2.productName, 
			t2.productPrice, 
			t2.productFor, 
			t2.productPeriod
			FROM (
					SELECT 
						t1.*, 
						t2.paymentId,
						t2.paymentStatus,
						t2.paymentRequestDate, 
						t2.paymentDoneDate, 
						t2.paymentPrice, 
						t2.couponDiscount, 
						t2.pointDiscount, 
						t2.card_name, 
						t2.productCode
						FROM (
								SELECT t1.userId, t1.userName, t1.userPhone, t2.userTypeId
								FROM momsitter_user AS t1
								JOIN momsitter_user_role AS t2
								ON t1.userId = t2.userId
						) AS t1
						JOIN momsitter_payment AS t2
						ON t1.userId = t2.userId
			) AS t1
			JOIN momsitter_product AS t2
			ON t1.productCode = t2.productCode			
	) AS final
	WHERE 1 = 1
	${where}
	${order}
	${limit} 
  `;
}
