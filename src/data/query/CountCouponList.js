export default function (where) {
  return `
  SELECT COUNT(*) AS CNT
    FROM (
      SELECT t3.*,
          t6.couponId,
          t6.couponTypeId,
          t6.codeId,
          DATE_FORMAT(t6.expireDate, '%Y-%m-%d %H:%i:%s') AS expireDate,
          DATE_FORMAT(t6.useDate, '%Y-%m-%d %H:%i:%s') AS useDate,
          DATE_FORMAT(t6.pubDate, '%Y-%m-%d %H:%i:%s') AS pubDate,
          t6.status,
          t6.paymentId,
          t6.read,
          t6.couponName,
          t6.couponPrice
        FROM (
          SELECT t1.userId, t1.userName, t1.userPhone, t2.userTypeId
            FROM momsitter_user AS t1
            JOIN momsitter_user_role AS t2
            ON t1.userId = t2.userId
        ) AS t3
        JOIN (
          SELECT t4.*, t5.couponName, t5.couponPrice
            FROM momsitter_coupon AS t4
            JOIN momsitter_coupon_type AS t5
            ON t4.couponTypeId = t5.couponTypeId
        ) AS t6
        ON t3.userId = t6.userId
    ) AS final
    WHERE 1 = 1
    ${where}
  `;
}
