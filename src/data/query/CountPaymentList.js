export default function (where) {
  return `
  SELECT COUNT(*) AS CNT
    FROM (
      SELECT 
        t1.*,
        t2.productId, 
        t2.productName, 
        t2.productPrice, 
        t2.productFor, 
        t2.productPeriod
        FROM (
            SELECT 
              t1.*, 
              t2.paymentId,
              t2.paymentStatus,
              t2.paymentRequestDate, 
              t2.paymentDoneDate, 
              t2.paymentPrice, 
              t2.couponDiscount, 
              t2.pointDiscount, 
              t2.card_name, 
              t2.productCode
              FROM (
                  SELECT t1.userId, t1.userName, t1.userPhone, t2.userTypeId
                  FROM momsitter_user AS t1
                  JOIN momsitter_user_role AS t2
                  ON t1.userId = t2.userId
              ) AS t1
              JOIN momsitter_payment AS t2
              ON t1.userId = t2.userId
        ) AS t1
        JOIN momsitter_product AS t2
        ON t1.productCode = t2.productCode			
    ) AS final
    WHERE 1 = 1
	  ${where}
  `;
};
