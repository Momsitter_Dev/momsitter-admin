export default function(where = '', order = '', limit = '') {
  return `
  SELECT *
	FROM (
		SELECT *
			FROM (
				SELECT t2.*, t1.certLabelName
					FROM momsitter_certificate_label AS t1
					RIGHT JOIN (
						SELECT *
							FROM (
								SELECT t2.*,
									t1.certType,
									t1.certTypeName,
									t1.certTypeDesc,
									t1.certInfoMsg,
									t1.certWaitingMsg,
									t1.certApprovedMsg,
									t1.certRejectedMsg,
									t1.referenceType
									FROM momsitter_certificate_type AS t1
									JOIN (
										SELECT t2.*,
											DATE_FORMAT(t1.uploadDate, '%Y-%m-%d %H:%i:%s') AS uploadDate,
											t1.fileUuid,
											t1.fileUrl,
											t1.fileName,
											t1.fileSize,
											t1.mimeType,
											t1.auth,
											t1.tag
											FROM momsitter_upload_files AS t1
											RIGHT JOIN (
												SELECT 
													t2.*,
													t1.certId,
													t1.fileId,
													t1.certTypeId,
													t1.certStatus,
													t1.rejectMsg,
													t1.certData,
													DATE_FORMAT(t1.regDate, '%Y-%m-%d %H:%i:%s') AS regDate,
													DATE_FORMAT(t1.issueDate, '%Y-%m-%d %H:%i:%s') AS issueDate,
													DATE_FORMAT(t1.updateDate, '%Y-%m-%d %H:%i:%s') AS updateDate
													FROM momsitter_user_certificate AS t1
													LEFT JOIN (
														SELECT 
															mu.userId,
															mu.userKey,
															mu.userSnsKey,
															mu.userName,
															mu.userPassword,
															DATE_FORMAT(mu.userSignUpDate, '%Y-%m-%d %H:%i:%s') AS userSignUpDate,
															mu.userBirthday,
															mu.userAge,
															mu.userConnectType,
															mu.userPhone,
															mu.userGender,
															mu.userEmail,
															mu.userDup,
															mu.userNationality,
															mu.userAssignmentTos,
															mu.userAssignmentPp,
															mu.userAssignmentMsg,
															mu.userStatus,
															mu.userSearchable,
															mu.contactWay,
															mu.contactWayDetail,
															mu.joinType,
															mu.isOwnPhone,
															role.userTypeId,
															role.userDetailTypeDesc
															FROM momsitter_user AS mu
															JOIN (
																SELECT mr.userId, mr.userTypeId, userDetailTypeDesc
																	FROM momsitter_user_role AS mr
																	JOIN momsitter_user_type AS mt
																	ON mr.userTypeId = mt.userTypeId
															) AS role
															on mu.userId = role.userId
													) AS t2
													ON t1.userId = t2.userId
											) AS t2
											ON t1.fileId = t2.fileId
											AND t1.fileId IS NOT NULL
									) AS t2
									ON t1.certTypeId = t2.certTypeId
							) AS t1
					) AS t2
					ON t1.userId = t2.userId
		) AS t1
		GROUP BY t1.certId
	) AS t1
	WHERE 1 = 1
	${where}
	${order}
	${limit} 
  `;
}
