export default function (where, order, limit) {
  return `
SELECT *
	FROM (
		SELECT 
			t1.userId,
			t1.userName,
			t1.userTypeId,
			t2.pointId,
			t2.pointTypeId,
			t2.totalPoint,
			t2.remainPoint,
			DATE_FORMAT(t2.pubDate, '%Y-%m-%d %H:%i:%s') AS pubDate,
			DATE_FORMAT(t2.expireDate, '%Y-%m-%d %H:%i:%s') AS expireDate,		
			t2.read,
			t2.removed,
			t2.issueReason,
			t2.removeReason,
			t2.pointType,
			t2.pointTypeName,
			t2.pointPrice
			FROM (
				SELECT 
					t1.userId,
					t1.userName,
					t2.userTypeId
					FROM momsitter_user AS t1
					JOIN momsitter_user_role AS t2
					ON t1.userId = t2.userId
			) AS t1
			JOIN (
				SELECT 
					t1.*,
					t2.pointType,
					t2.pointTypeName,
					t2.pointPrice
					FROM momsitter_point AS t1
					JOIN momsitter_point_type AS t2
					ON t1.pointTypeId = t2.pointTypeId
			) AS t2
			ON t1.userId = t2.userId
	) AS wrap
	WHERE 1 = 1
	${where}
	${order}
	${limit} 
  `;
}
