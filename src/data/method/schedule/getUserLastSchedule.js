/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import Debug from 'debug';
import moment from 'moment';
import { UserSchedule } from '../../models/index';
import sequelize from '../../sequelize';

/**
 * @function getUserLastSchedule
 * @param userId
 * @return {Promise}
 * @description 회원의 마지막 스케줄 일자를 가져오는 함수
 * @todo scheduleTypeId에 따른 구분이 필요해질 경우에 파라메터와 where 절에 scheduleTypeId 조건을 추가해야야함
 */
const debug = Debug('api');
const getUserLastSchedule = async (userId) => {
  return new Promise(async (resolve, reject) => {
    try {
      const userSchedule = await UserSchedule.findOne({
        attributes: ['userId', [sequelize.fn('MAX', sequelize.col('scheduleDate')), 'lastScheduleDate']],
        where: { userId },
        group: ['userId'],
      }).catch((err) => {
        debug(err);
        return false;
      });

      if (userSchedule && userSchedule.dataValues && userSchedule.dataValues.lastScheduleDate) {
        resolve(moment(userSchedule.dataValues.lastScheduleDate));
      } else {
        reject();
      }
    } catch (err) {
      debug(err);
      reject();
    }
  });
};

export default getUserLastSchedule;
