/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

/**
 *  @description initialize Sequelize.js
 *  @reference  http://docs.sequelizejs.com/en/v3/api/sequelize/
 * */

import Sequelize from 'sequelize';
import config from '../config/config';

export default new Sequelize(config.database.database, config.database.user, config.database.password, {
  define: {
    freezeTableName: true,
    timestamps: false,
    underscored: false,
    underscoredAll: false,
    engine: 'InnoDB',
    charset: 'utf8',
  },
  host: config.database.host,
  dialect: config.database.dialect,
  logging: false,
  timezone: config.database.timezone,
});
