/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'UserType',
  {
    userTypeId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    userType: {
      type: DataType.STRING(50),
    },
    userTypeDesc: {
      type: DataType.STRING(255),
    },
    userDetailType: {
      type: DataType.STRING(50),
    },
    userDetailTypeDesc: {
      type: DataType.STRING(255),
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_user_type',
  },
);
