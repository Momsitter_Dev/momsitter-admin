/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'SitterProfile',
  {
    profileId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    userId: {
      type: DataType.INTEGER,
    },
    viewCount: {
      type: DataType.INTEGER,
    },
    createDate: {
      type: DataType.DATE,
    },
    updateDate: {
      type: DataType.DATE,
    },
    profileStatus: {
      type: DataType.STRING(50),
    },
    profileTitle: {
      type: DataType.STRING(255),
    },
    profileDescription: {
      type: DataType.STRING(500),
    },
    wantedPayment: {
      type: DataType.INTEGER,
    },
    responseRate: {
      type: DataType.FLOAT,
    },
    responseTime: {
      type: DataType.INTEGER,
    },
    searchable: {
      type: DataType.BOOLEAN,
    },
    cctv: {
      type: DataType.BOOLEAN,
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_sitter_profile',
  },
);
