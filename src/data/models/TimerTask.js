import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'TimerTask',
  {
    timerTaskId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    timerRuleId: {
      type: DataType.INTEGER,
      allowNull: false,
    },
    regDate: {
      type: DataType.DATE,
      defaultValue: DataType.NOW,
    },
    reservedDate: {
      type: DataType.DATE,
    },
    excutedDate: {
      type: DataType.DATE,
    },
    status: {
      type: DataType.STRING(50),
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_timer_task',
  },
);
