/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'Apply',
  {
    applyId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    applicantId: {
      type: DataType.INTEGER,
    },
    receiverId: {
      type: DataType.INTEGER,
    },
    applyDate: {
      type: DataType.DATE,
    },
    updateDate: {
      type: DataType.DATE,
    },
    responseDate: {
      type: DataType.DATE,
    },
    readDate: {
      type: DataType.DATE,
    },
    applyStatus: {
      type: DataType.STRING(50),
    },
    parentData: {
      type: DataType.TEXT,
    },
    sitterData: {
      type: DataType.TEXT,
    },
    declineReason: {
      type: DataType.STRING(255),
    },
    declineType: {
      type: DataType.STRING(50),
    },
    tuningReason: {
      type: DataType.STRING(255),
    },
    applyType: {
      type: DataType.STRING(50),
    },
    legacyApplyId: {
      type: DataType.INTEGER,
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_apply',
  },
);
