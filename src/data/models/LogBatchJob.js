/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'LogBatchJob',
  {
    batchId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    jobType: {
      type: DataType.STRING(50),
    },
    startTime: {
      type: DataType.DATE,
      defaultValue: DataType.NOW,
    },
    endTime: {
      type: DataType.DATE,
    },
    result: {
      type: DataType.STRING(50),
    },
    message: {
      type: DataType.STRING(500),
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_log_batch_job',
  },
);
