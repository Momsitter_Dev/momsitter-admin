/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'SearchIndexSitter',
  {
    indexParentId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    userId: {
      type: DataType.INTEGER,
      unique: true,
    },
    userName: {
      type: DataType.STRING(50),
    },
    userGender: {
      type: DataType.STRING(50),
    },
    userAge: {
      type: DataType.INTEGER,
    },
    userStatus: {
      type: DataType.BOOLEAN,
    },
    userSearchable: {
      type: DataType.BOOLEAN,
    },
    profileSearchable: {
      type: DataType.BOOLEAN,
    },
    userTypeId: {
      type: DataType.INTEGER,
    },
    userLocationHome: {
      type: DataType.STRING(100),
    },
    userLocationHomeLat: {
      type: DataType.FLOAT,
    },
    userLocationHomeLng: {
      type: DataType.FLOAT,
    },
    userLocationOther1: {
      type: DataType.STRING(100),
    },
    userLocationOther1Lat: {
      type: DataType.FLOAT,
    },
    userLocationOther1Lng: {
      type: DataType.FLOAT,
    },
    userLocationOther2: {
      type: DataType.STRING(100),
    },
    userLocationOther2Lat: {
      type: DataType.FLOAT,
    },
    userLocationOther2Lng: {
      type: DataType.FLOAT,
    },
    userLocationOther3: {
      type: DataType.STRING(100),
    },
    userLocationOther3Lat: {
      type: DataType.FLOAT,
    },
    userLocationOther3Lng: {
      type: DataType.FLOAT,
    },
    mainProfileImageUrl: {
      type: DataType.STRING(255),
    },
    certLabelName: {
      type: DataType.STRING(50),
    },
    certCount: {
      type: DataType.INTEGER,
    },
    certEnrollment: {
      type: DataType.BOOLEAN,
    },
    certMedicalExamination: {
      type: DataType.BOOLEAN,
    },
    certFamilyRelation: {
      type: DataType.BOOLEAN,
    },
    certChildCare: {
      type: DataType.BOOLEAN,
    },
    certResidentRegistration: {
      type: DataType.BOOLEAN,
    },
    certPersonality: {
      type: DataType.BOOLEAN,
    },
    preferChildAgeNew: {
      type: DataType.BOOLEAN,
    },
    preferChildAgeYong: {
      type: DataType.BOOLEAN,
    },
    preferChildAgeChild: {
      type: DataType.BOOLEAN,
    },
    preferChildAgeElement: {
      type: DataType.BOOLEAN,
    },
    responseRate: {
      type: DataType.FLOAT,
    },
    responseAvgTime: {
      type: DataType.FLOAT,
    },
    acceptRate: {
      type: DataType.FLOAT,
    },
    quickResponse: {
      type: DataType.BOOLEAN,
    },
    updateDate: {
      type: DataType.DATE,
    },
    profileStatus: {
      type: DataType.BOOLEAN,
    },
    profileTitle: {
      type: DataType.STRING(255),
    },
    wantedPayment: {
      type: DataType.INTEGER,
    },
    cctv: {
      type: DataType.BOOLEAN,
    },
    shortResidentSitter: {
      type: DataType.BOOLEAN,
    },
    longResidentSitter: {
      type: DataType.BOOLEAN,
    },
    simpleCooking: {
      type: DataType.BOOLEAN,
    },
    simpleDishWashing: {
      type: DataType.BOOLEAN,
    },
    simpleCleaning: {
      type: DataType.BOOLEAN,
    },
    indoorActivity: {
      type: DataType.BOOLEAN,
    },
    outdoorActivity: {
      type: DataType.BOOLEAN,
    },
    athleticPlay: {
      type: DataType.BOOLEAN,
    },
    englishPlay: {
      type: DataType.BOOLEAN,
    },
    storytelling: {
      type: DataType.BOOLEAN,
    },
    briefEducation: {
      type: DataType.BOOLEAN,
    },
    professionalEducation: {
      type: DataType.BOOLEAN,
    },
    schoolCompanion: {
      type: DataType.BOOLEAN,
    },
    koreaStudy: {
      type: DataType.BOOLEAN,
    },
    studyGuide: {
      type: DataType.BOOLEAN,
    },
    scheduleStartDate: {
      type: DataType.DATE,
    },
    scheduleEndDate: {
      type: DataType.DATE,
    },
    scheduleDuration: {
      type: DataType.INTEGER,
    },
    mon: {
      type: DataType.BOOLEAN,
    },
    monMorning: {
      type: DataType.BOOLEAN,
    },
    monNoon: {
      type: DataType.BOOLEAN,
    },
    monNight: {
      type: DataType.BOOLEAN,
    },
    tue: {
      type: DataType.BOOLEAN,
    },
    tueMorning: {
      type: DataType.BOOLEAN,
    },
    tueNoon: {
      type: DataType.BOOLEAN,
    },
    tueNight: {
      type: DataType.BOOLEAN,
    },
    wed: {
      type: DataType.BOOLEAN,
    },
    wedMorning: {
      type: DataType.BOOLEAN,
    },
    wedNoon: {
      type: DataType.BOOLEAN,
    },
    wedNight: {
      type: DataType.BOOLEAN,
    },
    thu: {
      type: DataType.BOOLEAN,
    },
    thuMorning: {
      type: DataType.BOOLEAN,
    },
    thuNoon: {
      type: DataType.BOOLEAN,
    },
    thuNight: {
      type: DataType.BOOLEAN,
    },
    fri: {
      type: DataType.BOOLEAN,
    },
    friMorning: {
      type: DataType.BOOLEAN,
    },
    friNoon: {
      type: DataType.BOOLEAN,
    },
    friNight: {
      type: DataType.BOOLEAN,
    },
    sat: {
      type: DataType.BOOLEAN,
    },
    satMorning: {
      type: DataType.BOOLEAN,
    },
    satNoon: {
      type: DataType.BOOLEAN,
    },
    satNight: {
      type: DataType.BOOLEAN,
    },
    sun: {
      type: DataType.BOOLEAN,
    },
    sunMorning: {
      type: DataType.BOOLEAN,
    },
    sunNoon: {
      type: DataType.BOOLEAN,
    },
    sunNight: {
      type: DataType.BOOLEAN,
    },
    averageReviewRate: {
      type: DataType.FLOAT,
    },
    reviewCount: {
      type: DataType.INTEGER,
    },
    lastIndexTime: {
      type: DataType.DATE,
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_search_index_sitter',
  },
);
