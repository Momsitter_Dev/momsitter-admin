/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'User',
  {
    userId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    userKey: {
      type: DataType.STRING(50),
    },
    userSnsKey: {
      type: DataType.STRING(50),
    },
    userName: {
      type: DataType.STRING(50),
    },
    userPassword: {
      type: DataType.STRING(50),
    },
    userSignUpDate: {
      type: DataType.DATE,
    },
    userBirthday: {
      type: DataType.STRING(50),
    },
    userAge: {
      type: DataType.INTEGER,
    },
    userConnectType: {
      type: DataType.STRING(50),
    },
    userPhone: {
      type: DataType.STRING(50),
    },
    userGender: {
      type: DataType.STRING(50),
    },
    userEmail: {
      type: DataType.STRING(50),
    },
    userDup: {
      type: DataType.STRING(200),
      unique: true,
    },
    userNationality: {
      type: DataType.BOOLEAN,
    },
    userAssignmentTos: {
      type: DataType.BOOLEAN,
    },
    userAssignmentPp: {
      type: DataType.BOOLEAN,
    },
    userAssignmentMsg: {
      type: DataType.BOOLEAN,
    },
    userStatus: {
      type: DataType.STRING(10),
    },
    userSearchable: {
      type: DataType.BOOLEAN,
    },
    contactWay: {
      type: DataType.STRING(50),
    },
    contactWayDetail: {
      type: DataType.STRING(500),
    },
    lastLoginDate: {
      type: DataType.DATE,
    },
    joinType: {
      type: DataType.STRING(10),
    },
    isOwnPhone: {
      type: DataType.BOOLEAN,
      defaultValue: true,
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_user',
    comment: '사용자 정보를 저장하고 있는 테이블',
  },
);
