/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'UserCertificate',
  {
    certId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    fileId: {
      type: DataType.INTEGER,
    },
    certTypeId: {
      type: DataType.INTEGER,
    },
    userId: {
      type: DataType.INTEGER,
    },
    regDate: {
      type: DataType.DATE,
    },
    certStatus: {
      type: DataType.STRING(50),
    },
    rejectMsg: {
      type: DataType.STRING(500),
    },
    issueDate: {
      type: DataType.DATE,
    },
    updateDate: {
      type: DataType.DATE,
    },
    certData: {
      type: DataType.TEXT,
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_user_certificate',
  },
);
