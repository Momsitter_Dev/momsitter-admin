/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'LogAlarmNonresponse',
  {
    logId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    checkDate: {
      type: DataType.DATE,
    },
    applyId: {
      type: DataType.INTEGER,
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_log_alarm_nonresponse',
  },
);
