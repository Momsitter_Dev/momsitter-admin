/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'ScheduleType',
  {
    scheduleTypeId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    scheduleType: {
      type: DataType.STRING(50),
    },
    scheduleDescription: {
      type: DataType.STRING(255),
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_schedule_type',
  },
);
