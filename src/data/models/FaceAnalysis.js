import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'FaceAnalysis',
  {
    faceAnalysisId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    userId: {
      type: DataType.INTEGER,
    },
    fileId: {
      type: DataType.INTEGER,
    },
    analysisResult: {
      type: DataType.TEXT,
      allowNull: false,
    },
    resultSummary: {
      type: DataType.BOOLEAN,
    },
    status: {
      type: DataType.STRING,
    },
    regDate: {
      type: DataType.DATE,
      allowNull: false,
      defaultValue: DataType.NOW,
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_face_analysis',
  },
);
