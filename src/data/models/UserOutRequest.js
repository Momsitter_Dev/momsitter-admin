import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'UserOutRequest',
  {
    userOutRequestId: {
      type: DataType.INTEGER,
      defaultValue: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    userId: {
      type: DataType.INTEGER,
      allowNull: false,
    },
    outReason: {
      type: DataType.TEXT,
    },
    regDate: {
      type: DataType.DATE,
      defaultValue: DataType.NOW,
    },
    status: {
      type: DataType.STRING(50),
    },
    doneDate: {
      type: DataType.DATE,
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_user_out_request',
  },
);
