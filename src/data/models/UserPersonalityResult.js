import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'UserPersonalityResult',
  {
    resultId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    gmailId: {
      type: DataType.STRING(100),
    },
    originSequence: {
      type: DataType.STRING(50),
    },
    sequence: {
      type: DataType.STRING(50),
    },
    name: {
      type: DataType.STRING(50),
    },
    userId: {
      type: DataType.INTEGER,
    },
    resultDate: {
      type: DataType.DATE,
    },
    crawledDate: {
      type: DataType.DATE,
    },
    integrityName: {
      type: DataType.STRING(50),
    },
    integrityValue: {
      type: DataType.STRING(50),
    },
    integrityGrade: {
      type: DataType.STRING(50),
    },
    integrityPriority: {
      type: DataType.STRING(50),
    },
    integrityDesc: {
      type: DataType.STRING(500),
    },
    relationShipName: {
      type: DataType.STRING(50),
    },
    relationShipValue: {
      type: DataType.STRING(50),
    },
    relationShipGrade: {
      type: DataType.STRING(50),
    },
    releationShipPriority: {
      type: DataType.STRING(50),
    },
    relationShipDesc: {
      type: DataType.STRING(500),
    },
    altitudeName: {
      type: DataType.STRING(50),
    },
    altitudeValue: {
      type: DataType.STRING(50),
    },
    altitudeGrade: {
      type: DataType.STRING(50),
    },
    altitudePriority: {
      type: DataType.STRING(50),
    },
    altitudeDesc: {
      type: DataType.STRING(500),
    },
    intellectualName: {
      type: DataType.STRING(50),
    },
    intellectualValue: {
      type: DataType.STRING(50),
    },
    intellectualGrade: {
      type: DataType.STRING(50),
    },
    intellectualPriority: {
      type: DataType.STRING(50),
    },
    intellectualDesc: {
      type: DataType.STRING(500),
    },
    honestyName: {
      type: DataType.STRING(50),
    },
    honestyValue: {
      type: DataType.STRING(50),
    },
    honestyGrade: {
      type: DataType.STRING(50),
    },
    honestyPriority: {
      type: DataType.STRING(50),
    },
    honestyDesc: {
      type: DataType.STRING(500),
    },
    leadershipName: {
      type: DataType.STRING(50),
    },
    leadershipValue: {
      type: DataType.STRING(50),
    },
    leadershipGrade: {
      type: DataType.STRING(50),
    },
    leadershipPriority: {
      type: DataType.STRING(50),
    },
    leadershipDesc: {
      type: DataType.STRING(500),
    },
    stabilityName: {
      type: DataType.STRING(50),
    },
    stabilityValue: {
      type: DataType.STRING(50),
    },
    stabilityGrade: {
      type: DataType.STRING(50),
    },
    stabilityPriority: {
      type: DataType.STRING(50),
    },
    stabilityDesc: {
      type: DataType.STRING(500),
    },
    resultAVG: {
      type: DataType.STRING(50),
    },
    resultSTDEV: {
      type: DataType.STRING(50),
    },
    resultValue: {
      type: DataType.STRING(50),
    },
    resultDesc: {
      type: DataType.TEXT,
    },
    resultChartUrl: {
      type: DataType.STRING(255),
    },
    screen_reliabilityValue: {
      type: DataType.STRING(50),
    },
    screen_reliabilityYn: {
      type: DataType.STRING(50),
    },
    screen_fictionalReactionValue: {
      type: DataType.STRING(50),
    },
    screen_fictionalReactionYn: {
      type: DataType.STRING(50),
    },
    screen_personalityAvgValue: {
      type: DataType.STRING(50),
    },
    screen_personalityAvgYn: {
      type: DataType.STRING(50),
    },
    screen_stabilityValue: {
      type: DataType.STRING(50),
    },
    screen_stabilityYn: {
      type: DataType.STRING(50),
    },
    screen_totalValue: {
      type: DataType.STRING(50),
    },
    screen_totalYn: {
      type: DataType.STRING(50),
    },
    source_categoryName: {
      type: DataType.STRING(50),
    },
    source_mainCategoryName: {
      type: DataType.STRING(50),
    },
    source_mainCategoryDesc: {
      type: DataType.STRING(500),
    },
    source_subCategoryName: {
      type: DataType.STRING(50),
    },
    source_subCategoryDesc: {
      type: DataType.STRING(500),
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_user_personality_result',
  },
);
