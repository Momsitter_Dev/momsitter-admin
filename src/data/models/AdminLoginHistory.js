import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'AdminLoginHistory',
  {
    historyId: {
      type: DataType.INTEGER,
      defaultValue: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    adminId: {
      type: DataType.INTEGER,
      allowNull: false,
    },
    adminLoginDate: {
      type: DataType.DATE,
      defaultValue: DataType.NOW,
    },
    accessIp: {
      type: DataType.STRING(50),
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_admin_login_history',
  },
);
