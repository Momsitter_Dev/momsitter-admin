/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'CertificateMain',
  {
    mainCertId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    userTypeId: {
      type: DataType.INTEGER,
    },
    certTypeId: {
      type: DataType.INTEGER,
    },
    mainCertType: {
      type: DataType.STRING(50),
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_certificate_main',
  },
);
