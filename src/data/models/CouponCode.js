/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';
import CouponCodeTypeGroup from '../../util/const/CouponCodeTypeGroup';

export default Model.define(
  'Coupon',
  {
    // 쿠폰 코드 고유 번호
    codeId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    // 쿠폰 코드 소유자 번호 (사용자 개개인의 초대코드가 아니면 null)
    userId: {
      type: DataType.INTEGER,
    },
    // 해당 쿠폰 코드로 발급받을 수 있는 쿠폰 유형 번호
    couponTypeId: {
      type: DataType.INTEGER,
    },
    // 식별가능한 코드 7자리
    code: {
      type: DataType.STRING(7),
      unique: true,
    },
    // 코드 유형 (invite: 초대 코드, event: 이벤트 코드)
    type: {
      type: DataType.STRING(50),
      // 기본값 초대 코드
      defaultValue: CouponCodeTypeGroup.INVITE,
    },
    // 코드 만료 날짜 (type 이 invite 인 경우는 만료날짜가 없음)
    expireDate: {
      type: DataType.DATE,
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_coupon_code',
  },
);
