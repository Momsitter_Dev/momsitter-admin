/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'CertificateType',
  {
    certTypeId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    certType: {
      type: DataType.STRING(50),
    },
    certTypeName: {
      type: DataType.STRING(255),
    },
    certTypeDesc: {
      type: DataType.STRING(255),
    },
    certInfoMsg: {
      type: DataType.STRING(255),
    },
    certWaitingMsg: {
      type: DataType.STRING(255),
    },
    certApprovedMsg: {
      type: DataType.STRING(255),
    },
    certRejectedMsg: {
      type: DataType.STRING(255),
    },
    referenceType: {
      type: DataType.STRING(255),
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_certificate_type',
  },
);
