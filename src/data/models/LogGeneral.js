/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'LogGeneral',
  {
    logId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    logDate: {
      type: DataType.DATE,
    },
    userId: {
      type: DataType.INTEGER,
    },
    remoteIp: {
      type: DataType.STRING(100),
    },
    userAgent: {
      type: DataType.STRING(200),
    },
    requestUrl: {
      type: DataType.STRING(300),
    },
    requestMethod: {
      type: DataType.STRING(50),
    },
    referer: {
      type: DataType.STRING(300),
    },
    cookie: {
      type: DataType.STRING(2000),
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_log_general',
  },
);
