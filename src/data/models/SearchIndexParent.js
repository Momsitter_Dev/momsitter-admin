/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'SearchIndexParent',
  {
    indexParentId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    userId: {
      type: DataType.INTEGER,
      unique: true,
    },
    userName: {
      type: DataType.STRING(50),
    },
    userGender: {
      type: DataType.STRING(50),
    },
    userAge: {
      type: DataType.INTEGER,
    },
    userStatus: {
      type: DataType.BOOLEAN,
    },
    userSearchable: {
      type: DataType.BOOLEAN,
    },
    userTypeId: {
      type: DataType.INTEGER,
    },
    userLocation: {
      type: DataType.STRING(100),
    },
    userLocationLat: {
      type: DataType.FLOAT,
    },
    userLocationLng: {
      type: DataType.FLOAT,
    },
    mainProfileImageUrl: {
      type: DataType.STRING(255),
    },
    updateDate: {
      type: DataType.DATE,
    },
    profileStatus: {
      type: DataType.BOOLEAN,
    },
    profileTitle: {
      type: DataType.STRING(255),
    },
    paymentNegotiable: {
      type: DataType.BOOLEAN,
    },
    wantedPayment: {
      type: DataType.INTEGER,
    },
    longTerm: {
      type: DataType.BOOLEAN,
    },
    childCount: {
      type: DataType.INTEGER,
    },
    child1AgeId: {
      type: DataType.INTEGER,
    },
    child2AgeId: {
      type: DataType.INTEGER,
    },
    child1Age: {
      type: DataType.INTEGER,
    },
    child2Age: {
      type: DataType.INTEGER,
    },
    child1Gender: {
      type: DataType.STRING(50),
    },
    child2Gender: {
      type: DataType.STRING(50),
    },
    child1ChildBirthDay: {
      type: DataType.DATE,
    },
    child2ChildBirthDay: {
      type: DataType.DATE,
    },
    shortResidentSitter: {
      type: DataType.BOOLEAN,
    },
    longResidentSitter: {
      type: DataType.BOOLEAN,
    },
    simpleCooking: {
      type: DataType.BOOLEAN,
    },
    simpleDishWashing: {
      type: DataType.BOOLEAN,
    },
    simpleCleaning: {
      type: DataType.BOOLEAN,
    },
    indoorActivity: {
      type: DataType.BOOLEAN,
    },
    outdoorActivity: {
      type: DataType.BOOLEAN,
    },
    athleticPlay: {
      type: DataType.BOOLEAN,
    },
    englishPlay: {
      type: DataType.BOOLEAN,
    },
    storytelling: {
      type: DataType.BOOLEAN,
    },
    briefEducation: {
      type: DataType.BOOLEAN,
    },
    professionalEducation: {
      type: DataType.BOOLEAN,
    },
    schoolCompanion: {
      type: DataType.BOOLEAN,
    },
    koreaStudy: {
      type: DataType.BOOLEAN,
    },
    studyGuide: {
      type: DataType.BOOLEAN,
    },
    scheduleType: {
      type: DataType.STRING(50),
    },
    scheduleNegotiable: {
      type: DataType.BOOLEAN,
    },
    scheduleStartDate: {
      type: DataType.DATE,
    },
    scheduleEndDate: {
      type: DataType.DATE,
    },
    scheduleDuration: {
      type: DataType.INTEGER,
    },
    mon: {
      type: DataType.BOOLEAN,
    },
    monMorning: {
      type: DataType.BOOLEAN,
    },
    monNoon: {
      type: DataType.BOOLEAN,
    },
    monNight: {
      type: DataType.BOOLEAN,
    },
    tue: {
      type: DataType.BOOLEAN,
    },
    tueMorning: {
      type: DataType.BOOLEAN,
    },
    tueNoon: {
      type: DataType.BOOLEAN,
    },
    tueNight: {
      type: DataType.BOOLEAN,
    },
    wed: {
      type: DataType.BOOLEAN,
    },
    wedMorning: {
      type: DataType.BOOLEAN,
    },
    wedNoon: {
      type: DataType.BOOLEAN,
    },
    wedNight: {
      type: DataType.BOOLEAN,
    },
    thu: {
      type: DataType.BOOLEAN,
    },
    thuMorning: {
      type: DataType.BOOLEAN,
    },
    thuNoon: {
      type: DataType.BOOLEAN,
    },
    thuNight: {
      type: DataType.BOOLEAN,
    },
    fri: {
      type: DataType.BOOLEAN,
    },
    friMorning: {
      type: DataType.BOOLEAN,
    },
    friNoon: {
      type: DataType.BOOLEAN,
    },
    friNight: {
      type: DataType.BOOLEAN,
    },
    sat: {
      type: DataType.BOOLEAN,
    },
    satMorning: {
      type: DataType.BOOLEAN,
    },
    satNoon: {
      type: DataType.BOOLEAN,
    },
    satNight: {
      type: DataType.BOOLEAN,
    },
    sun: {
      type: DataType.BOOLEAN,
    },
    sunMorning: {
      type: DataType.BOOLEAN,
    },
    sunNoon: {
      type: DataType.BOOLEAN,
    },
    sunNight: {
      type: DataType.BOOLEAN,
    },
    receivedApplyCount: {
      type: DataType.INTEGER,
    },
    lastIndexTime: {
      type: DataType.DATE,
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_search_index_parent',
  },
);
