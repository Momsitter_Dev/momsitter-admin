/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

/*
  Note.
  사용자들의 적립금 내역을 저장하고 있는 테이블
 */
export default Model.define(
  'Point',
  {
    // 적립금 번호
    pointId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    // 적립금 유형 번호
    pointTypeId: {
      type: DataType.INTEGER,
    },
    // 소유자 사용자 번호
    userId: {
      type: DataType.INTEGER,
    },
    // 총 적립금
    totalPoint: {
      type: DataType.INTEGER,
    },
    // 남은 적립금
    remainPoint: {
      type: DataType.INTEGER,
    },
    // 적립금 지급 날짜
    pubDate: {
      type: DataType.DATE,
      defaultValue: DataType.NOW,
    },
    // 적립금 만료 날짜
    expireDate: {
      type: DataType.DATE,
    },
    // 적립금을 확인했는지 여부
    read: {
      type: DataType.BOOLEAN,
      defaultValue: false,
    },
    removed: {
      type: DataType.BOOLEAN,
      defaultValue: false,
    },
    // [관리자] 지급 사유
    issueReason: {
      type: DataType.STRING(500),
    },
    // [관리자] 삭제 사유
    removeReason: {
      type: DataType.STRING(500),
    },
    removedDate: {
      type: DataType.DATE,
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_point',
  },
);
