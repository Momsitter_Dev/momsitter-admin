import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'AdminTimeLine',
  {
    timeLineId: {
      type: DataType.INTEGER,
      defaultValue: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    adminId: {
      type: DataType.INTEGER,
      allowNull: false,
    },
    taskId: {
      type: DataType.INTEGER,
      allowNull: false,
    },
    taskStatus: {
      type: DataType.DATE,
    },
    regDate: {
      type: DataType.DATE,
    },
    checkDate: {
      type: DataType.STRING(50),
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_admin_timeline',
  },
);
