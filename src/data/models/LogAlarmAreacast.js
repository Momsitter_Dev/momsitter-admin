/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'LogAlarmAreacast',
  {
    logId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    userId: {
      type: DataType.INTEGER,
    },
    messageType: {
      type: DataType.STRING(50),
    },
    logDate: {
      type: DataType.DATE,
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_log_alarm_areacast',
  },
);
