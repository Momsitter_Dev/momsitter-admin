/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'Product',
  {
    productId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    productCode: {
      type: DataType.STRING(50),
    },
    productName: {
      type: DataType.STRING(50),
    },
    productDesc: {
      type: DataType.STRING(500),
    },
    productPrice: {
      type: DataType.INTEGER,
    },
    productType: {
      type: DataType.STRING(50),
    },
    productFor: {
      type: DataType.STRING(50),
    },
    productPeriod: {
      type: DataType.INTEGER,
    },
    productApplyToSitterCount: {
      type: DataType.INTEGER,
    },
    productApplyToParentCount: {
      type: DataType.INTEGER,
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_product',
  },
);
