/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';
import CouponTypeTargetGroup from '../../util/const/CouponTypeTargetGroup';

export default Model.define(
  'Coupon',
  {
    // 쿠폰 유형 번호
    couponTypeId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    // 쿠폰 이름
    couponName: {
      type: DataType.STRING(100),
    },
    // 할인 가격
    couponPrice: {
      type: DataType.INTEGER,
    },
    // 쿠폰 유형 상태 (available: 발행 가능, unavailable: 발행 불가능)
    status: {
      type: DataType.STRING(50),
    },
    // 발행되는 쿠폰을 사용할 수 있는 대상 (all: 모두, sitter: 시터만, parent: 부모만)
    target: {
      type: DataType.STRING(50),
      // 기본값, 시터 & 부모 둘다 사용 가능
      defaultValue: CouponTypeTargetGroup.ALL,
    },
    // 쿠폰 기간 (일 기준)
    couponDate: {
      type: DataType.INTEGER,
      // 디폴트 30일 만료
      defaultValue: 30,
    },
    // 쿠폰 유형 등록 날짜
    registerDate: {
      type: DataType.DATE,
      defaultValue: DataType.NOW,
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_coupon_type',
  },
);
