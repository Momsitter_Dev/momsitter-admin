/**
 * Created by seungjaeyuk on 2017. 9. 1..
 */
import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'TimerRule',
  {
    timerRuleId: {
      type: DataType.INTEGER,
      defaultValue: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    timerRuleName: {
      type: DataType.STRING(50),
    },
    targetType: {
      type: DataType.STRING(50),
    },
    messageTemplateCode: {
      type: DataType.STRING(50),
    },
    timerRuleDesc: {
      type: DataType.STRING(500),
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_timer_rule',
  },
);
