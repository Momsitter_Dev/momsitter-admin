/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'LogMessage',
  {
    messageId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    senderId: {
      type: DataType.INTEGER,
    },
    receiverId: {
      type: DataType.INTEGER,
    },
    msgType: {
      type: DataType.STRING(50),
    },
    to: {
      type: DataType.STRING(50),
    },
    msg: {
      type: DataType.STRING(5000),
    },
    sendDate: {
      type: DataType.DATE,
    },
    attribute: {
      type: DataType.STRING(500),
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_log_message',
  },
);
