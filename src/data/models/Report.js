/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'Report',
  {
    reportId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    reporterId: {
      type: DataType.INTEGER,
    },
    targetId: {
      type: DataType.INTEGER,
    },
    reportType: {
      type: DataType.STRING(50),
    },
    reportContent: {
      type: DataType.STRING(2000),
    },
    reportDate: {
      type: DataType.DATE,
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_report',
  },
);
