/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'BoardContent',
  {
    boardContentId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    boardId: {
      type: DataType.INTEGER,
    },
    writerId: {
      type: DataType.INTEGER,
    },
    title: {
      type: DataType.STRING(255),
    },
    content: {
      type: DataType.TEXT,
    },
    registerDate: {
      type: DataType.DATE,
    },
    updateDate: {
      type: DataType.DATE,
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_board_content',
  },
);
