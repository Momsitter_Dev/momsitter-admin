/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';
import CouponStatusGroup from '../../util/const/CouponStatusGroup';

export default Model.define(
  'Coupon',
  {
    couponId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    // 쿠폰 소유 사용자 번호
    userId: {
      type: DataType.INTEGER,
    },
    // 쿠폰 유형 번호
    couponTypeId: {
      type: DataType.INTEGER,
    },
    // 해당 쿠폰이 발급될 때 사용된 코드 번호 (코드로 발급된 쿠폰이 아닐시 null)
    codeId: {
      type: DataType.INTEGER,
    },
    // 쿠폰이 사용된 결제 번호
    paymentId: {
      type: DataType.INTEGER,
    },
    // 쿠폰 발급 날짜
    pubDate: {
      type: DataType.DATE,
      defaultValue: DataType.NOW,
    },
    // 쿠폰 만료 날짜
    expireDate: {
      type: DataType.DATE,
    },
    // 쿠폰 사용 날짜
    useDate: {
      type: DataType.DATE,
    },
    // 쿠폰 상태 (available, used)
    status: {
      type: DataType.STRING(50),
      defaultValue: CouponStatusGroup.AVAILABLE,
    },
    // coupon read flag (true, false: default)
    read: {
      type: DataType.BOOLEAN,
      defaultValue: false,
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_coupon',
  },
);
