/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'UserWeekdayTimeIndex',
  {
    indexId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    userId: {
      type: DataType.INTEGER,
    },
    startDate: {
      type: DataType.DATE,
    },
    endDate: {
      type: DataType.DATE,
    },
    mon: {
      type: DataType.BOOLEAN,
    },
    monMorning: {
      type: DataType.BOOLEAN,
    },
    monNoon: {
      type: DataType.BOOLEAN,
    },
    monNight: {
      type: DataType.BOOLEAN,
    },
    tue: {
      type: DataType.BOOLEAN,
    },
    tueMorning: {
      type: DataType.BOOLEAN,
    },
    tueNoon: {
      type: DataType.BOOLEAN,
    },
    tueNight: {
      type: DataType.BOOLEAN,
    },
    wed: {
      type: DataType.BOOLEAN,
    },
    wedMorning: {
      type: DataType.BOOLEAN,
    },
    wedNoon: {
      type: DataType.BOOLEAN,
    },
    wedNight: {
      type: DataType.BOOLEAN,
    },
    thu: {
      type: DataType.BOOLEAN,
    },
    thuMorning: {
      type: DataType.BOOLEAN,
    },
    thuNoon: {
      type: DataType.BOOLEAN,
    },
    thuNight: {
      type: DataType.BOOLEAN,
    },
    fri: {
      type: DataType.BOOLEAN,
    },
    friMorning: {
      type: DataType.BOOLEAN,
    },
    friNoon: {
      type: DataType.BOOLEAN,
    },
    friNight: {
      type: DataType.BOOLEAN,
    },
    sat: {
      type: DataType.BOOLEAN,
    },
    satMorning: {
      type: DataType.BOOLEAN,
    },
    satNoon: {
      type: DataType.BOOLEAN,
    },
    satNight: {
      type: DataType.BOOLEAN,
    },
    sun: {
      type: DataType.BOOLEAN,
    },
    sunMorning: {
      type: DataType.BOOLEAN,
    },
    sunNoon: {
      type: DataType.BOOLEAN,
    },
    sunNight: {
      type: DataType.BOOLEAN,
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_user_weekday_time_index',
  },
);
