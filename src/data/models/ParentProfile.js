/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'ParentProfile',
  {
    profileId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    userId: {
      type: DataType.INTEGER,
    },
    viewCount: {
      type: DataType.INTEGER,
    },
    createDate: {
      type: DataType.DATE,
      defaultValue: DataType.NOW,
    },
    updateDate: {
      type: DataType.DATE,
    },
    lastFinishedDate: {
      type: DataType.DATE,
    },
    profileStatus: {
      type: DataType.STRING(50),
    },
    profileTitle: {
      type: DataType.STRING(500),
    },
    profileDescription: {
      type: DataType.STRING(500),
    },
    wantedPayment: {
      type: DataType.INTEGER,
    },
    wantedInterviewWay: {
      type: DataType.STRING(50),
    },
    wantedSitterGender: {
      type: DataType.STRING(50),
    },
    wantedCareWay: {
      type: DataType.STRING(50),
    },
    longTerm: {
      type: DataType.BOOLEAN,
    },
    paymentNegotiable: {
      type: DataType.BOOLEAN,
    },
    durationNegotiable: {
      type: DataType.BOOLEAN,
    },
    weekdayNegotiable: {
      type: DataType.BOOLEAN,
    },
    timeNegotiable: {
      type: DataType.BOOLEAN,
    },
    moreThanThreeMonth: {
      type: DataType.BOOLEAN,
    },
    childCount: {
      type: DataType.INTEGER,
    },
    preferMinAge: {
      type: DataType.INTEGER,
    },
    preferMaxAge: {
      type: DataType.INTEGER,
    },
    scheduleType: {
      type: DataType.STRING(50),
    },
    scheduleNegotiable: {
      type: DataType.BOOLEAN,
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_parent_profile',
  },
);
