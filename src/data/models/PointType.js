/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

/*
  Note.
  적립금의 메타 유형을 저장하는 테이블
 */
export default Model.define(
  'PointType',
  {
    // 적립금 번호
    pointTypeId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    // 적립금 이름 (영문)
    pointType: {
      type: DataType.STRING(100),
    },
    // 적립금 이름 (한글)
    pointTypeName: {
      type: DataType.STRING(100),
    },
    // 적립금액
    pointPrice: {
      type: DataType.INTEGER,
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_point_type',
  },
);
