import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'TimerTaskTarget',
  {
    taskTargetId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    timerTaskId: {
      type: DataType.INTEGER,
      allowNull: false,
    },
    userId: {
      type: DataType.INTEGER,
      allowNull: false,
    },
    relatedUserId: {
      type: DataType.INTEGER,
    },
    buttonName: {
      type: DataType.STRING(500),
    },
    buttonLink: {
      type: DataType.STRING(500),
    },
    message: {
      type: DataType.STRING(5000),
    },
    result: {
      type: DataType.BOOLEAN,
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_timer_task_target',
  },
);
