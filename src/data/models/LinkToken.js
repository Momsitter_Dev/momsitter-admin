/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

/**
 * 리다이렉트 될 링크와 엑세스토큰을 함께 받을 수 있는 링크 토큰 모델 정의
 */
export default Model.define(
  'LinkToken',
  {
    tokenId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    // Note. 34자리 링크 토큰
    linkToken: {
      type: DataType.STRING(34),
      unique: true,
    },
    // Note. 발급 대상 (링크 토큰 소유 사용자 번호)
    userId: {
      type: DataType.INTEGER,
    },
    // Note. 리다이렉션 링크
    link: {
      type: DataType.STRING(500),
    },
    // Note. 링크 토큰 생성 날짜
    genDate: {
      type: DataType.DATE,
    },
    // Note. 링크 토큰 최초 접근 날짜
    accessDate: {
      type: DataType.DATE,
    },
    // Note. 링크 토큰 만료 날짜
    expireDate: {
      type: DataType.DATE,
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_link_token',
  },
);
