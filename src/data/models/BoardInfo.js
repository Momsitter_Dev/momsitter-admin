/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'BoardInfo',
  {
    boardId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    boardName: {
      type: DataType.STRING(255),
    },
    createDate: {
      type: DataType.DATE,
    },
    updateDate: {
      type: DataType.DATE,
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_board_info',
  },
);
