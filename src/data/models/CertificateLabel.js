/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'CertificateLabel',
  {
    certLabelId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    certTypeId: {
      type: DataType.INTEGER,
    },
    userId: {
      type: DataType.INTEGER,
    },
    certLabelName: {
      type: DataType.STRING(50),
    },
    createDate: {
      type: DataType.DATE,
      defaultValue: DataType.NOW,
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_certificate_label',
  },
);
