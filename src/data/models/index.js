/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */
/** *
 * 2017-07-04 TUE
 * @Author seungjaey
 * Sequelize create with association,
 * 미리 정의한 관계를 이용해 INSERT 할때  'as'속성으로 해당 관계에 대한 별칭을 지정하고 실제 쿼리시에도 지정한 별칭으로 값을 넘겨야한다. 1:1 => Object / 1:many => Array
 */
import Debug from 'debug';
import sequelize from '../sequelize';
import Admin from './Admin';
import AdminLoginHistory from './AdminLoginHistory';
import AdminMemo from './AdminMemo';
import AdminTimeLine from './AdminTimeLine';
import TimerRule from './TimerRule';
import TimerTask from './TimerTask';
import TimerTaskTarget from './TimerTaskTarget';
import ActivityType from '../models/ActivityType';
import Apply from '../models/Apply';
import BoardContent from '../models/BoardContent';
import BoardInfo from '../models/BoardInfo';
import CertificateLabel from '../models/CertificateLabel';
import CertificateMain from '../models/CertificateMain';
import CertificateType from '../models/CertificateType';
import ChildAge from '../models/ChildAge';
import CodeDecline from '../models/CodeDecline';
import Coupon from '../models/Coupon';
import LogAlarmApply from '../models/LogAlarmApply';
import LogAlarmAreacast from '../models/LogAlarmAreacast';
import LogAlarmNonresponse from '../models/LogAlarmNonresponse';
import LogAlarmReview from '../models/LogAlarmReview';
import LogGeneral from '../models/LogGeneral';
import LogMessage from '../models/LogMessage';
import ParentActivity from '../models/ParentActivity';
import ParentChildInfo from '../models/ParentChildInfo';
import ParentProfile from '../models/ParentProfile';
import Payment from '../models/Payment';
import Product from '../models/Product';
import Report from '../models/Report';
import Review from '../models/Review';
import ScheduleType from '../models/ScheduleType';
import SitterActivity from '../models/SitterActivity';
import SitterExperience from '../models/SitterExperience';
import SitterPaymentPolicy from '../models/SitterPaymentPolicy';
import SitterPreferChildInfo from '../models/SitterPreferChildInfo';
import SitterProfile from '../models/SitterProfile';
import UploadFiles from '../models/UploadFiles';
import User from '../models/User';
import UserApplyCount from '../models/UserApplyCount';
import UserBookmark from '../models/UserBookmark';
import UserCertificate from '../models/UserCertificate';
import UserLocation from '../models/UserLocation';
import UserProduct from '../models/UserProduct';
import UserProfileImage from '../models/UserProfileImage';
import UserRole from '../models/UserRole';
import UserSchedule from '../models/UserSchedule';
import UserType from '../models/UserType';
import FaceAnalysis from './FaceAnalysis';
import UserWeekdayTimeIndex from './UserWeekdayTimeIndex';
import Feedback from './Feedback';
import UserOutRequest from './UserOutRequest';
import UserSnapshot from './UserSnapshot';
import UserPersonalityResult from './UserPersonalityResult';
import CouponCode from './CouponCode';
import CouponType from './CouponType';
import LinkToken from './LinkToken';
import Point from './Point';
import PointType from './PointType';
import LogNoResponseCheck from './LogNoResponseCheck';
import LogUsingPoint from './LogUsingPoint';
import LogBatchJob from './LogBatchJob';
import SearchIndexParent from './SearchIndexParent';
import SearchIndexSitter from './SearchIndexSitter';
import Notice from './Notice';

const debug = Debug('api');

function sync(...args) {
  // Sync => Seqeulize로 DB에 모델을 작성한 뒤 실행되는 루틴
  return sequelize.sync(...args).then(async () => {
    // TODO 공통 코드값을 사용하기위해 임시로 구현함
  });
}

Admin.hasMany(AdminLoginHistory, { foreignKey: 'adminId' });
AdminLoginHistory.belongsTo(Admin, { foreignKey: 'adminId' });

Admin.hasMany(AdminMemo, { foreignKey: 'adminId', as: 'adminMemo' });
AdminMemo.belongsTo(Admin, { foreignKey: 'adminId', as: 'memoAdmin' });

Admin.hasMany(AdminTimeLine, { foreignKey: 'adminId' });
AdminTimeLine.belongsTo(Admin, { foreignKey: 'adminId' });

User.hasMany(AdminMemo, { foreignKey: 'userId', as: 'userMemo' });
AdminMemo.belongsTo(User, { foreignKey: 'userId', as: 'memoUser' });

AdminMemo.hasMany(AdminMemo, { foreignKey: 'memoResId', targetKey: 'memoId', as: 'childMemo' });
AdminMemo.belongsTo(AdminMemo, { foreignKey: 'memoId', sourceKey: 'memoResId' });
//AdminMemo.belongsToMany(AdminMemo, { as: 'childMemo', through: 'childMemos', foreignKey: 'memoId', targetKey: 'memoResId' });

User.hasMany(FaceAnalysis, { foreignKey: 'userId', as: 'userFace' });
FaceAnalysis.belongsTo(User, { foreignKey: 'userId', as: 'faceUser' });

// Timer.hasMany(TimerTask, { foreignKey: 'taskId' });
// TimerTask.belongsTo()
TimerRule.hasMany(TimerTask, { foreignKey: 'timerRuleId' });
TimerTask.belongsTo(TimerRule, { foreignKey: 'timerRuleId', as: 'timerTaskRule' });
TimerTask.hasMany(TimerTaskTarget, { foreignKey: 'timerTaskId', as: 'timerTaskUser' });
TimerTaskTarget.belongsTo(TimerTask, { foreignKey: 'timerTaskId' });

//MessageTemplate.hasMany(TimerRule, { foreignKey: 'messageTemplateId' });

/*
 Note
 - 의도치 않은 KEY 가 추가되는 것을 방지하기 위해서, 모든 relation 은 foreign key 를 명시하는 것을 convention 으로 삼음
 -
 */

//
// Relation configuration
// -----------------------------------------------------------------------------

User.hasOne(SitterProfile, { foreignKey: 'userId', as: 'sitterProfile' });
User.hasOne(ParentProfile, { foreignKey: 'userId', as: 'parentProfile' });
User.hasMany(SitterPreferChildInfo, {
  foreignKey: 'userId',
  as: 'sitterPreferChildInfo',
});
User.hasMany(ParentChildInfo, { foreignKey: 'userId', as: 'parentChildInfo' });
User.hasMany(SitterActivity, { foreignKey: 'userId', as: 'sitterActivity' });
User.hasMany(ParentActivity, { foreignKey: 'userId', as: 'parentActivity' });
User.hasMany(UserBookmark, { foreignKey: 'subjectId', as: 'subject' });
User.hasMany(UserBookmark, { foreignKey: 'targetUserId', as: 'target' });
User.hasMany(UserSchedule, { foreignKey: 'userId', as: 'userSchedule' });
User.hasMany(Payment, { foreignKey: 'userId' });
User.hasMany(Report, { foreignKey: 'reporterId', as: 'reporter' });
User.hasMany(Report, { foreignKey: 'targetId', as: 'targetReport' });
User.hasMany(UserProfileImage, {
  foreignKey: 'userId',
  as: 'userProfileImage',
});
User.hasMany(UploadFiles, { foreignKey: 'userId', as: 'userFiles' });
User.hasOne(CertificateLabel, { foreignKey: 'userId' });
User.hasMany(UserRole, { foreignKey: 'userId', as: 'userRole' });
User.hasMany(UserCertificate, { foreignKey: 'userId', as: 'userCertificate' });
User.hasMany(UserLocation, { foreignKey: 'userId', as: 'userLocation' });
User.hasOne(UserApplyCount, { foreignKey: 'userId', as: 'userApplyCount' });
User.hasMany(Review, { foreignKey: 'senderId', as: 'sentReview' });
User.hasMany(Review, { foreignKey: 'targetId', as: 'receivedReview' });
User.hasMany(Apply, { foreignKey: 'applicantId', as: 'sentApply' });
User.hasMany(Apply, { foreignKey: 'receiverId', as: 'receivedApply' });
User.hasMany(SitterExperience, {
  foreignKey: 'userId',
  as: 'sitterExperience',
});
User.hasMany(UserProduct, { foreignKey: 'userId' });

UserRole.belongsTo(UserType, { foreignKey: 'userTypeId', as: 'userType' });
UserRole.belongsTo(User, { foreignKey: 'userId' });

UserLocation.belongsTo(User, { foreignKey: 'userId' });

UserApplyCount.belongsTo(User, { foreignKey: 'userId' });

UserProduct.belongsTo(Payment, { foreignKey: 'paymentId', as: 'userProductPayment'});
UserProduct.belongsTo(Product, { foreignKey: 'productId', as: 'product' });
UserProduct.belongsTo(User, { foreignKey: 'userId', as: 'productUser' });

UserSchedule.belongsTo(ScheduleType, { foreignKey: 'scheduleTypeId' });

UserBookmark.belongsTo(User, { foreignKey: 'subjectId', as: 'subject' });
UserBookmark.belongsTo(User, { foreignKey: 'targetUserId', as: 'target' });

UploadFiles.hasOne(UserProfileImage, {
  foreignKey: 'fileId',
  as: 'fileProfile',
});
UserProfileImage.belongsTo(UploadFiles, {
  foreignKey: 'fileId',
  as: 'profileFile',
});

UploadFiles.hasOne(FaceAnalysis, { foreignKey: 'fileId', as: 'uploadFace' });
FaceAnalysis.belongsTo(UploadFiles, { foreignKey: 'fileId', as: 'faceUpload' });

UserProfileImage.belongsTo(User, { foreignKey: 'userId' });

UserCertificate.belongsTo(User, { foreignKey: 'userId', as: 'certUser' });
UserCertificate.belongsTo(UploadFiles, { foreignKey: 'fileId', as: 'certFile' });
UserCertificate.belongsTo(CertificateType, { foreignKey: 'certTypeId' });

UploadFiles.hasOne(UserCertificate, { foreignKey: 'fileId' });

SitterProfile.belongsTo(User, { foreignKey: 'userId' });
SitterPreferChildInfo.belongsTo(ChildAge, {
  foreignKey: 'childAgeId',
  as: 'sitterPreferChildAge',
});
SitterActivity.belongsTo(ActivityType, {
  foreignKey: 'activityId',
  as: 'sitterActivityType',
});
SitterPaymentPolicy.belongsTo(UserType, { foreignKey: 'userTypeId' });
SitterPaymentPolicy.belongsTo(CertificateType, { foreignKey: 'certTypeId' });

ParentProfile.belongsTo(User, { foreignKey: 'userId' });
ParentChildInfo.belongsTo(ChildAge, {
  foreignKey: 'childAgeId',
  as: 'parentChildAge',
});
ParentActivity.belongsTo(ActivityType, {
  foreignKey: 'activityId',
  as: 'parentActivityType',
});

Payment.belongsTo(User, { foreignKey: 'userId', as: 'paymentUser' });
Product.hasMany(Payment, { foreignKey: 'productCode', as: 'paymentProduct', sourceKey: 'productCode' });
Payment.belongsTo(Product, { foreignKey: 'productCode', as: 'paymentProduct', targetKey: 'productCode' });

Report.belongsTo(User, { foreignKey: 'reporterId', as: 'reportUser' });
Report.belongsTo(User, { foreignKey: 'targetId', as: 'reportTargetUser' });

CertificateLabel.belongsTo(CertificateType, { foreignKey: 'certTypeId' });
CertificateMain.belongsTo(UserType, { foreignKey: 'userTypeId' });
CertificateMain.belongsTo(CertificateType, { foreignKey: 'certTypeId' });

Review.belongsTo(Apply, { foreignKey: 'applyId' });
Review.belongsTo(User, { foreignKey: 'senderId', as: 'sender' });
Review.belongsTo(User, { foreignKey: 'targetId', as: 'target' });

Apply.belongsTo(User, { foreignKey: 'applicantId', as: 'applicant' });
Apply.belongsTo(User, { foreignKey: 'receiverId', as: 'receiver' });

LogMessage.belongsTo(User, { foreignKey: 'senderId', as: 'sender' });
LogMessage.belongsTo(User, { foreignKey: 'receiverId', as: 'receiver' });
LogAlarmNonresponse.belongsTo(Apply, { foreignKey: 'applyId' });
LogAlarmReview.belongsTo(Apply, { foreignKey: 'applyId' });
LogAlarmApply.belongsTo(Apply, { foreignKey: 'applyId' });
LogAlarmAreacast.belongsTo(User, { foreignKey: 'userId' });

BoardInfo.hasMany(BoardContent, { foreignKey: 'boardId' });
BoardContent.belongsTo(BoardInfo, { foreignKey: 'boardId' });

User.hasOne(UserWeekdayTimeIndex, { foreignKey: 'userId', as: 'userWeekdayTimeIndex' });
UserWeekdayTimeIndex.belongsTo(User, { foreignKey: 'userId', as: 'user' });

User.hasOne(UserOutRequest, { foreignKey: 'userId' });
UserOutRequest.belongsTo(User, { foreignKey: 'userId', as: 'outRequestUser' });

User.hasOne(UserSnapshot, { foreignKey: 'userId' });
UserSnapshot.belongsTo(User, { foreignKey: 'userId' });

// Coupon Relation
Coupon.belongsTo(CouponType, {
  foreignKey: 'couponTypeId',
  as: 'couponType',
});

Coupon.belongsTo(CouponCode, {
  foreignKey: 'codeId',
});

Coupon.belongsTo(Payment, {
  foreignKey: 'paymentId',
});

Coupon.belongsTo(User, {
  foreignKey: 'userId',
});

CouponCode.belongsTo(CouponType, {
  foreignKey: 'couponTypeId',
});

CouponCode.belongsTo(User, {
  foreignKey: 'userId',
});

Payment.hasMany(Coupon, {
  foreignKey: 'paymentId',
  as: 'paymentCoupon',
});

Coupon.belongsTo(Payment, {
  foreignKey: 'paymentId',
  as: 'couponPayment',
});

LinkToken.belongsTo(User, { foreignKey: 'userId', as: 'user' });

Point.belongsTo(PointType, {
  foreignKey: 'pointTypeId',
  as: 'pointType',
});

Point.belongsTo(User, {
  foreignKey: 'userId',
});

Point.hasMany(LogUsingPoint, {
  foreignKey: 'pointId',
  as: 'pointLog',
});

LogUsingPoint.belongsTo(Point, {
  foreignKey: 'pointId',
  as: 'logPoint',
});

LogUsingPoint.belongsTo(Payment, {
  foreignKey: 'paymentId',
});

SearchIndexParent.belongsTo(User, { foreignKey: 'userId', targetKey: 'userId', as: 'searchIndexParent' });
SearchIndexSitter.belongsTo(User, { foreignKey: 'userId', targetKey: 'userId', as: 'searchIndexSitter' });

export default { sync };
export {
  ActivityType,
  Apply,
  BoardContent,
  BoardInfo,
  CertificateLabel,
  CertificateMain,
  CertificateType,
  ChildAge,
  CodeDecline,
  Coupon,
  CouponType,
  CouponCode,
  LogAlarmApply,
  LogAlarmAreacast,
  LogAlarmNonresponse,
  LogAlarmReview,
  LogGeneral,
  LogMessage,
  ParentActivity,
  ParentChildInfo,
  ParentProfile,
  Payment,
  Product,
  Report,
  Review,
  ScheduleType,
  SitterActivity,
  SitterExperience,
  SitterPaymentPolicy,
  SitterPreferChildInfo,
  SitterProfile,
  UploadFiles,
  User,
  UserApplyCount,
  UserBookmark,
  UserCertificate,
  UserLocation,
  UserProduct,
  UserProfileImage,
  UserRole,
  UserSchedule,
  UserType,
  Admin,
  AdminLoginHistory,
  AdminMemo,
  AdminTimeLine,
  TimerRule,
  TimerTask,
  TimerTaskTarget,
  FaceAnalysis,
  UserWeekdayTimeIndex,
  Feedback,
  UserOutRequest,
  UserSnapshot,
  UserPersonalityResult,
  LinkToken,
  Point,
  PointType,
  LogNoResponseCheck,
  LogUsingPoint,
  LogBatchJob,
  SearchIndexSitter,
  SearchIndexParent,
  Notice,
};
