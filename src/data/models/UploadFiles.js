/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'UploadFiles',
  {
    fileId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    userId: {
      type: DataType.INTEGER,
    },
    uploadDate: {
      type: DataType.DATE,
    },
    fileUuid: {
      type: DataType.STRING(50),
    },
    fileUrl: {
      type: DataType.STRING(255),
    },
    fileName: {
      type: DataType.STRING(255),
    },
    fileSize: {
      type: DataType.INTEGER,
    },
    mimeType: {
      type: DataType.STRING(50),
    },
    auth: {
      type: DataType.STRING(50),
    },
    tag: {
      type: DataType.STRING(50),
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_upload_files',
  },
);
