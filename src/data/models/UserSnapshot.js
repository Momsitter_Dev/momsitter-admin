import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'UserSnapshot',
  {
    userSnapshotId: {
      type: DataType.INTEGER,
      defaultValue: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    userId: {
      type: DataType.INTEGER,
      allowNull: false,
    },
    snapshotData: {
      type: DataType.TEXT,
      allowNull: false,
    },
    regDate: {
      type: DataType.DATE,
      defaultValue: DataType.NOW,
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_user_snapshot',
  },
);
