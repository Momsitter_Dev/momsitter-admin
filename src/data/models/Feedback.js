/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'Feedback',
  {
    feedbackId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    postDate: {
      type: DataType.DATE,
      defaultValue: DataType.NOW,
    },
    feedbackContent: {
      type: DataType.STRING(5000),
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_feedback',
  },
);
