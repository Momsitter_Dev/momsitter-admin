import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'Notice',
  {
    id: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    title: {
      type: DataType.STRING(255),
    },
    content: {
      type: DataType.TEXT,
    },
    author: {
      type: DataType.STRING(50),
    },
    registerDate: {
      type: DataType.DATE,
    },
    updateDate: {
      type: DataType.DATE,
    },
    view: {
      type: DataType.INTEGER,
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_notice',
  },
);
