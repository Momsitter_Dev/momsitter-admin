/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'ActivityType',
  {
    activityId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    activityCode: {
      type: DataType.STRING(50),
    },
    activityTitle: {
      type: DataType.STRING(500),
    },
    activityDescription: {
      type: DataType.STRING(500),
    },
    activityCategory: {
      type: DataType.STRING(50),
    },
    activityAbbreviation: {
      type: DataType.STRING(50),
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_activity_type',
    validate: {},
  },
);
