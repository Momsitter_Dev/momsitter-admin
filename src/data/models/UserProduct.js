/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'UserProduct',
  {
    userProductId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    userId: {
      type: DataType.INTEGER,
    },
    productId: {
      type: DataType.INTEGER,
    },
    paymentId: {
      type: DataType.INTEGER,
    },
    purchaseDate: {
      type: DataType.DATE,
    },
    expireDate: {
      type: DataType.DATE,
    },
    memo: {
      type: DataType.STRING(200),
    },
    productStatus: {
      type: DataType.STRING(50),
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_user_product',
  },
);
