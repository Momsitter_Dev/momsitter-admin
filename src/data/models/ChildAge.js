/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'ChildAge',
  {
    childAgeId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    ageType: {
      type: DataType.STRING(50),
    },
    ageName: {
      type: DataType.STRING(50),
    },
    ageSubName: {
      type: DataType.STRING(50),
    },
    ageDescription: {
      type: DataType.STRING(255),
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_child_age',
  },
);
