/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'Review',
  {
    reviewId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    applyId: {
      type: DataType.INTEGER,
    },
    senderId: {
      type: DataType.INTEGER,
    },
    targetId: {
      type: DataType.INTEGER,
    },
    reviewRate: {
      type: DataType.INTEGER,
    },
    reviewContent: {
      type: DataType.STRING(2000),
    },
    reviewType: {
      type: DataType.STRING(50),
    },
    writeDate: {
      type: DataType.DATE,
    },
    status: {
      type: DataType.STRING(50),
    },
    readDate: {
      type: DataType.DATE,
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_review',
  },
);
