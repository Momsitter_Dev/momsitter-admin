/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'UserApplyCount',
  {
    applyCountId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    userId: {
      type: DataType.INTEGER,
      unique: true,
    },
    applyToParentCount: {
      type: DataType.INTEGER,
    },
    applyToSitterCount: {
      type: DataType.INTEGER,
    },
    lastUpdated: {
      type: DataType.DATE,
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_user_apply_count',
  },
);
