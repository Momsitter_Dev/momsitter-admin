/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

/*
  Note.
  사용자들의 적립금 사용 내역을 기록하기 위한 테이블
 */
export default Model.define(
  'LogUsingPoint',
  {
    logId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    paymentId: {
      type: DataType.INTEGER,
    },
    pointId: {
      type: DataType.INTEGER,
    },
    logDate: {
      type: DataType.DATE,
      defaultValue: DataType.NOW,
    },
    spentPoint: {
      type: DataType.INTEGER,
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_log_using_point',
  },
);
