import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'AdminMemo',
  {
    memoId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    adminId: {
      type: DataType.INTEGER,
      allowNull: false,
    },
    userId: {
      type: DataType.INTEGER,
      allowNull: false,
    },
    memoResId: {
      type: DataType.INTEGER,
    },
    memoType: {
      type: DataType.STRING(45),
    },
    memoClass: {
      type: DataType.STRING(45),
    },
    memoContent: {
      type: DataType.STRING(2000),
    },
    memoReqMethod: {
      type: DataType.STRING(45),
    },
    memoStatus: {
      type: DataType.STRING(50),
    },
    memoRegDate: {
      type: DataType.DATE,
      defaultValue: DataType.NOW,
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_admin_memo',
  },
);
