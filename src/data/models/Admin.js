import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'Admin',
  {
    adminId: {
      type: DataType.INTEGER,
      defaultValue: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    adminKey: {
      type: DataType.STRING(50),
    },
    adminName: {
      type: DataType.STRING(50),
    },
    adminPassword: {
      type: DataType.STRING(50),
    },
    adminRegDate: {
      type: DataType.DATE,
      defaultValue: DataType.NOW,
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_admin',
  },
);
