/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'Payment',
  {
    paymentId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    userId: {
      type: DataType.INTEGER,
    },
    productCode: {
      type: DataType.CHAR(50),
    },
    paymentStatus: {
      type: DataType.CHAR(50),
    },
    paymentRequestDate: {
      type: DataType.DATE,
    },
    paymentDoneDate: {
      type: DataType.DATE,
    },
    paymentPrice: {
      type: DataType.INTEGER,
    },
    // 쿠폰으로 할인 받은 가격
    couponDiscount: {
      type: DataType.INTEGER,
    },
    // 적립금으로 할인 받은 가격
    pointDiscount: {
      type: DataType.INTEGER,
    },
    goodname: {
      type: DataType.STRING(100),
    },
    price: {
      type: DataType.INTEGER,
    },
    recvphone: {
      type: DataType.STRING(50),
    },
    memo: {
      type: DataType.STRING(500),
    },
    reqaddr: {
      type: DataType.CHAR(50),
    },
    reqdate: {
      type: DataType.DATE,
    },
    pay_memo: {
      type: DataType.STRING(50),
    },
    pay_addr: {
      type: DataType.STRING(50),
    },
    pay_date: {
      type: DataType.DATE,
    },
    pay_type: {
      type: DataType.INTEGER,
    },
    pay_state: {
      type: DataType.INTEGER,
    },
    var1: {
      type: DataType.TEXT,
    },
    var2: {
      type: DataType.TEXT,
    },
    mul_no: {
      type: DataType.STRING(50),
    },
    payurl: {
      type: DataType.STRING(50),
    },
    csturl: {
      type: DataType.STRING(50),
    },
    card_name: {
      type: DataType.STRING(50),
    },
    vbank: {
      type: DataType.STRING(50),
    },
    vbankno: {
      type: DataType.STRING(50),
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_payment',
  },
);
