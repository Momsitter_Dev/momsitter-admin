/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'CodeDecline',
  {
    codeDeclineId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    userType: {
      type: DataType.STRING(50),
    },
    declineCode: {
      type: DataType.STRING(50),
    },
    declineName: {
      type: DataType.STRING(50),
    },
    declineReason: {
      type: DataType.STRING(255),
    },
    declineMessage: {
      type: DataType.STRING(500),
    },
    memo: {
      type: DataType.STRING(50),
    },
    registerDate: {
      type: DataType.DATE,
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_code_decline',
  },
);
