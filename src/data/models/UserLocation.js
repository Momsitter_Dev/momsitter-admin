/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import DataType from 'sequelize';
import Model from '../sequelize';

export default Model.define(
  'UserLocation',
  {
    locationId: {
      type: DataType.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    locationType: {
      type: DataType.STRING(50),
    },
    userId: {
      type: DataType.INTEGER,
    },
    jibunAddr: {
      type: DataType.STRING(100),
    },
    roadAddr: {
      type: DataType.STRING(100),
    },
    locationLatitude: {
      type: DataType.DOUBLE,
    },
    locationLongitude: {
      type: DataType.DOUBLE,
    },
    siNm: {
      type: DataType.STRING(50),
    },
    sggNm: {
      type: DataType.STRING(50),
    },
    emdNm: {
      type: DataType.STRING(50),
    },
    detailAddr: {
      type: DataType.STRING(50),
    },
    zonecode: {
      type: DataType.STRING(50),
    },
    postcode: {
      type: DataType.STRING(50),
    },
  },
  {
    timestamps: false,
    tableName: 'momsitter_user_location',
  },
);
