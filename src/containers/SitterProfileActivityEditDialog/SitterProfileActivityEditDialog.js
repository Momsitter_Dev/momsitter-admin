import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { compose } from 'recompose';
import React from 'react';
import { Dialog, DialogTitle, DialogActions, DialogContent, Grid, Button } from '@material-ui/core';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './SitterProfileActivityEditDialog.css';
import * as sitterProfileActivityEditDialogActions from '../../modules/sitterProfileActivityEditDialog';
import * as sitterProfileActions from '../../modules/sitterProfile';
import FormActivity from '../../components/FormActivity';

const propTypes = {
  open: PropTypes.bool.isRequired,
  sitterProfile: PropTypes.object.isRequired,
  SitterProfileActions: PropTypes.object.isRequired,
  SitterProfileActivityEditDialogActions: PropTypes.object.isRequired,
};
const defaultProps = {};

class SitterProfileActivityEditDialog extends React.Component {
  constructor(props) {
    super(props);
    this.activitys = this.getDefaultValue();
  }

  onChange = data => {
    this.activitys = data.activityIds.map(item => ({
      activityId: Number(item),
    }));
  };

  onClose = () => {
    const { SitterProfileActivityEditDialogActions } = this.props;
    return SitterProfileActivityEditDialogActions.closeDialog();
  };

  onSave = () => {
    const { sitterProfile, SitterProfileActions, SitterProfileActivityEditDialogActions } = this.props;

    SitterProfileActions.setSitterProfileData({
      ...sitterProfile,
      data: {
        ...sitterProfile.data,
        sitterActivity: [...this.activitys],
      },
    });

    return SitterProfileActivityEditDialogActions.closeDialog();
  };

  getDefaultValue = () => {
    const { sitterProfile } = this.props;
    if (!sitterProfile) {
      return null;
    }
    if (!sitterProfile.data) {
      return null;
    }
    if (!sitterProfile.data.sitterActivity) {
      return null;
    }
    return sitterProfile.data.sitterActivity.map(item => item.activityId);
  };

  render() {
    const { open } = this.props;
    return (
      <div className={s.root}>
        <Dialog open={open} onClose={this.onClose}>
          <DialogTitle> 시터회원 - 가능한 활동 수정 </DialogTitle>
          <DialogContent style={{ width: '600px' }}>
            <Grid container spacing={16}>
              <Grid item xs={12}>
                <FormActivity onChange={this.onChange} initialSelectedIndex={this.getDefaultValue()} />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button variant="flat" onClick={this.onClose}>
              취소
            </Button>
            <Button variant="raised" onClick={this.onSave}>
              저장
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

SitterProfileActivityEditDialog.propTypes = propTypes;
SitterProfileActivityEditDialog.defaultProps = defaultProps;

export default compose(
  connect(
    state => ({
      open: state.sitterProfileActivityEditDialog.open,
      sitterProfile: state.sitterProfile,
    }),
    dispatch => ({
      SitterProfileActivityEditDialogActions: bindActionCreators(sitterProfileActivityEditDialogActions, dispatch),
      SitterProfileActions: bindActionCreators(sitterProfileActions, dispatch),
    }),
  ),
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(SitterProfileActivityEditDialog);
