/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
  Grid,
  Typography,
  TextField,
  CircularProgress,
} from '@material-ui/core';
import { Error as ErrorIcon } from '@material-ui/icons';
import { withStyles as withMaterialStyle } from '@material-ui/core/styles';
import React from 'react';
import { toast } from 'react-toastify';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CertRejectDialog.css';
import * as certRejectDialogActions from '../../modules/certRejectDialog';

const propTypes = {
  certRejectDialog: PropTypes.object.isRequired,
  CertRejectDialogActions: PropTypes.object.isRequired,
};
const defaultProps = {};

class CertRejectDialog extends React.Component {
  constructor(props) {
    super(props);
    this.rejectMsg = '';
  }

  onClickCancel = () => {
    const { CertRejectDialogActions } = this.props;
    return CertRejectDialogActions.close();
  };

  onClickSave = async () => {
    const { CertRejectDialogActions } = this.props;
    try {
      const rejectMsg = this.rejectMsg.value;
      if (!rejectMsg) {
        return toast.error('반려사유를 입력해 주세요.');
      }
      await CertRejectDialogActions.save(rejectMsg);
    } catch (err) {
      return CertRejectDialogActions.saveError(err);
    }
  };

  renderContent = () => {
    const { certRejectDialog } = this.props;
    const { pending, exception, exceptionMessage } = certRejectDialog;
    if (pending) {
      return this.renderPending();
    }
    if (exception) {
      return this.renderException(exceptionMessage);
    }
    return (
      <Grid container spacing={16}>
        <Grid item xs={12}>
          <TextField
            placeholder={'반려사유'}
            fullWidth
            multiline
            inputRef={r => {
              this.rejectMsg = r;
            }}
          />
        </Grid>
      </Grid>
    );
  };

  renderPending = () => {
    return (
      <Grid container spacing={16}>
        <Grid item xs={12}>
          <CircularProgress />
        </Grid>
        <Grid item xs={12}>
          <Typography variant={'caption'}>저장중입니다.</Typography>
        </Grid>
      </Grid>
    );
  };

  renderException = exceptionMessage => {
    return (
      <Grid container spacing={16}>
        <Grid item xs={1}>
          <ErrorIcon />
        </Grid>
        <Grid item xs={11}>
          <Typography variant={'caption'}>오류 : {exceptionMessage}</Typography>
        </Grid>
      </Grid>
    );
  };

  render() {
    const { certRejectDialog } = this.props;
    const { open } = certRejectDialog;
    return (
      <div>
        <Dialog open={open}>
          <DialogTitle>인증반려사유 입력</DialogTitle>
          <DialogContent>
            <DialogContentText>입력한 반려사유가 회원에게 메세지로 전달됩니다.</DialogContentText>
            {this.renderContent()}
          </DialogContent>
          <DialogActions>
            <Button variant={'flat'} onClick={this.onClickCancel}>
              취소
            </Button>
            <Button variant={'raised'} color={'primary'} onClick={this.onClickSave}>
              저장하기
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

CertRejectDialog.propTypes = propTypes;
CertRejectDialog.defaultProps = defaultProps;

export default compose(
  connect(
    state => ({
      certRejectDialog: state.certRejectDialog,
    }),
    dispatch => ({
      CertRejectDialogActions: bindActionCreators(certRejectDialogActions, dispatch),
    }),
  ),
  withStyles(s),
  withMaterialStyle(s, { withTheme: true }),
)(CertRejectDialog);
