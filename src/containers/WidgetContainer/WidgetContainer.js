/* eslint-disable prettier/prettier */
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { toast } from 'react-toastify';
import { compose } from 'recompose';
import { Button, Grid, Paper, Typography, withStyles as withMaterialStyles } from '@material-ui/core';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './WidgetContainer.css';

import ParentProfile from '../ParentProfile';
import SitterProfile from '../SitterProfile';
import * as widgetContainerActions from '../../modules/widgetContainer';

import history from '../../history';
import ResponseResultTypeGroup from '../../util/const/ResponseResultTypeGroup';
import isParent from '../../util/user/isParent';
import isSitter from '../../util/user/isSitter';

import ApplyCount from '../../components/UserManage/WidgetContainer/ApplyCount';
import UserAgent from '../../components/UserManage/WidgetContainer/UserAgent';
import BasicInfo from '../../components/UserManage/WidgetContainer/BasicInfo';
import Reviews from '../../components/UserManage/WidgetContainer/Reviews';
import Applications from '../../components/UserManage/WidgetContainer/Applications';
import Reports from '../../components/UserManage/WidgetContainer/Reports';
import Products from '../../components/UserManage/WidgetContainer/Products';
import Payments from '../../components/UserManage/WidgetContainer/Payments';
import Certifications from '../../components/UserManage/WidgetContainer/Certifications';
import Memos from '../../components/UserManage/WidgetContainer/Memos';
import Messages from '../../components/UserManage/WidgetContainer/Messages';
import Coupons from '../../components/UserManage/WidgetContainer/Coupons';
import Points from '../../components/UserManage/WidgetContainer/Points';

const propTypes = {
  WidgetContainerActions: PropTypes.object.isRequired,
  userId: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
  userTypeId: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
};

const defaultProps = {};

class WidgetContainer extends React.Component {
  onClickDeleteUser = async function() {
    if (confirm('회원의 모든 정보가 삭제됩니다. 계속 진행하시겠습니까?')) {
      const { userId } = this.props;
      try {
        const response = await Request(`api/destroy/${userId}`, { method: 'DELETE' });
        if (response && response.result === ResponseResultTypeGroup.SUCCESS) {
          toast.success(`${userId} 회원 정보가 모두 제거되었습니다.`);
          return history.go('/users');
        }
        throw new Error('request fail');
      } catch (err) {
        return toast.error(`${userId} 회원 정보를 삭제할 수 없습니다. : ${err.message}`);
      }
    }
    return null;
  };

  openNotificationDialog = (title, message) => {
    return toast.info(`[${title}] : ${message}`);
  };

  renderApplications = (userId, userTypeId) => {
    const title = '신청/지원 현황';
    const tabTitle = [];
    if (isParent(userTypeId)) {
      tabTitle.push('내가 신청한');
      tabTitle.push('내게 지원한');
    } else {
      tabTitle.push('내가 지원한');
      tabTitle.push('내게 신청한');
    }
    return <Applications title={title} tabTitle={tabTitle} userId={userId} />;
  };

  renderProfile = () => {
    const { userId, userTypeId } = this.props;
    if (isParent(userTypeId)) {
      return <ParentProfile userId={userId} userTypeId={userTypeId} />;
    }
    if (isSitter(userTypeId)) {
      return <SitterProfile userId={userId} userTypeId={userTypeId} />;
    }
    return <Typography> 유효하지 않은 회원 타입입니다. {userTypeId} </Typography>;
  };

  render() {
    const { userId, userTypeId } = this.props;
    if (userId) {
      return (
        <Grid container spacing={16}>
          <Grid item xs={12}>
            <Paper>
              <div style={{ padding: '25px' }}>회원을 선택해 주세요!</div>
            </Paper>
          </Grid>
        </Grid>
      );
    }

    return (
      <Grid container spacing={16}>
        <Grid item xs={12}>
          <ApplyCount userId={userId} />
        </Grid>
        <Grid item xs={12}>
          <UserAgent userId={userId} />
        </Grid>
        <Grid item xs={12} sm={12} md={6}>
          <BasicInfo
            title="기본 정보"
            userId={userId}
            userTypeId={userTypeId}
            openNotificationDialog={this.openNotificationDialog}
          />
        </Grid>
        <Grid item xs={12} sm={12} md={6}>
          {this.renderProfile()}
        </Grid>
        <Grid item xs={12} sm={12} md={6}>
          {this.renderApplications(userId, userTypeId)}
        </Grid>
        <Grid item xs={12} sm={12} md={6}>
          <Products title="이용권 현황" userId={userId} openNotificationDialog={this.openNotificationDialog} />
        </Grid>
        <Grid item xs={12} sm={12} md={6}>
          <Reviews title="후기 현황" userId={userId} openNotificationDialog={this.openNotificationDialog} />
        </Grid>
        <Grid item xs={12} sm={12} md={6}>
          <Reports title="신고 현황" userId={userId} openNotificationDialog={this.openNotificationDialog} />
        </Grid>
        <Grid item xs={12} sm={12} md={6}>
          <Payments title="결제 내역" userId={userId} openNotificationDialog={this.openNotificationDialog} />
        </Grid>
        <Grid item xs={12} sm={12} md={6}>
          <Certifications title="인증 내역" userId={userId} openNotificationDialog={this.openNotificationDialog} />
        </Grid>
        <Grid item xs={12} sm={12} md={6}>
          <Messages title="메세지 현황" userId={userId} openNotificationDialog={this.openNotificationDialog} />
        </Grid>
        <Grid item xs={12} sm={12} md={6}>
          <Coupons title="쿠폰 현황" userId={userId} openNotificationDialog={this.openNotificationDialog} />
        </Grid>
        <Grid item xs={12} sm={12} md={12}>
          <Points userId={userId} openNotificationDialog={this.openNotificationDialog} />
        </Grid>
        <Grid item xs={12} sm={12} md={12}>
          <Memos title="메모 내역" userId={userId} openNotificationDialog={this.openNotificationDialog} />
        </Grid>
        <Grid item xs={12}>
          <Button variant={'raised'} onClick={this.onClickDeleteUser}>
            회원 정보 삭제
          </Button>
        </Grid>
      </Grid>
    );
  }
}

WidgetContainer.propTypes = propTypes;
WidgetContainer.defaultProps = defaultProps;

export default compose(
  connect(
    state => ({
      userId: state.widgetContainer.userId,
      userTypeId: state.widgetContainer.userTypeId,
    }),
    dispatch => ({
      WidgetContainerActions: bindActionCreators(widgetContainerActions, dispatch),
    }),
  ),
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(WidgetContainer);
