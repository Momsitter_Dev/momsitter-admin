import { toast } from 'react-toastify';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { compose } from 'recompose';
import React from 'react';
import { Dialog, DialogTitle, DialogActions, DialogContent, Grid, Button } from '@material-ui/core';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './SitterProfileScheduleEditDialog.css';
import * as sitterProfileScheduleEditDialogActions from '../../modules/sitterProfileScheduleEditDialog';
import * as sitterProfileActions from '../../modules/sitterProfile';
import FormSitterSchedule from '../../components/FormSitterSchedule';

const propTypes = {
  open: PropTypes.bool.isRequired,
  sitterProfile: PropTypes.object.isRequired,
  SitterProfileActions: PropTypes.object.isRequired,
  SitterProfileScheduleEditDialogActions: PropTypes.object.isRequired,
};
const defaultProps = {};

class SitterProfileScheduleEditDialog extends React.Component {
  constructor(props) {
    super(props);
    this.data = this.getDefaultValue();
  }

  onChange = data => {
    this.data = data;
  };

  onClose = () => {
    const { SitterProfileScheduleEditDialogActions } = this.props;
    return SitterProfileScheduleEditDialogActions.closeDialog();
  };

  onSave = () => {
    if (!this.data) {
      return toast.warn('스케줄을 선택해주세요.');
    }
    if (this.data.length === 0) {
      return toast.warn('스케줄을 선택해주세요.');
    }

    const { sitterProfile, SitterProfileActions, SitterProfileScheduleEditDialogActions } = this.props;

    SitterProfileActions.setSitterProfileData({
      ...sitterProfile,
      data: {
        ...sitterProfile.data,
        userSchedule: [...this.data],
      },
    });

    return SitterProfileScheduleEditDialogActions.closeDialog();
  };

  getDefaultValue = () => {
    const { sitterProfile } = this.props;
    const data = [];
    if (!sitterProfile) {
      return data;
    }
    if (!sitterProfile.data) {
      return data;
    }
    if (!sitterProfile.data.userSchedule) {
      return data;
    }
    return sitterProfile.data.userSchedule;
  };

  render() {
    const { open } = this.props;
    return (
      <div className={s.root}>
        <Dialog open={open} onClose={this.onClose}>
          <DialogTitle> 시터회원 - 활동 가능 시간 수정 </DialogTitle>
          <DialogContent style={{ width: '600px' }}>
            <Grid container spacing={16}>
              <Grid item xs={12}>
                <FormSitterSchedule
                  schedules={this.getDefaultValue()}
                  onChange={this.onChange}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button variant="flat" onClick={this.onClose}>
              취소
            </Button>
            <Button variant="raised" onClick={this.onSave}>
              저장
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

SitterProfileScheduleEditDialog.propTypes = propTypes;
SitterProfileScheduleEditDialog.defaultProps = defaultProps;

export default compose(
  connect(
    state => ({
      open: state.sitterProfileScheduleEditDialog.open,
      sitterProfile: state.sitterProfile,
    }),
    dispatch => ({
      SitterProfileScheduleEditDialogActions: bindActionCreators(
        sitterProfileScheduleEditDialogActions,
        dispatch,
      ),
      SitterProfileActions: bindActionCreators(sitterProfileActions, dispatch),
    }),
  ),
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(SitterProfileScheduleEditDialog);
