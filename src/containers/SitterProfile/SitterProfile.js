/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */
import moment from 'moment';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { compose } from 'recompose';

import React from 'react';
import {
  Grid,
  Typography,
  Button,
  TextField,
  CircularProgress,
  withStyles as withMaterialStyles,
} from '@material-ui/core';
import { ErrorOutline as ErrorIcon } from '@material-ui/icons';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './SitterProfile.css';
import Widget from '../../components/Widget';

import * as sitterProfileActions from '../../modules/sitterProfile';

import ProfileSitterLocation from '../../components/ProfileSitterLocation';
import ProfileSitterExperience from '../../components/ProfileSitterExperience';
import ProfileActivity from '../../components/ProfileActivity';
import ProfileSitterPreferChild from '../../components/ProfileSitterPreferChild';
import ProfileSitterSchedule from '../../components/ProfileSitterSchedule';
import ProfileStatusGroup from '../../util/const/ProfileStatusGroup';

import * as sitterProfileActivityEditDialogActions from '../../modules/sitterProfileActivityEditDialog';
import * as sitterProfileExperienceEditDialogActions from '../../modules/sitterProfileExperienceEditDialog';
import * as sitterProfileLocationEditDialogActions from '../../modules/sitterProfileLocationEditDialog';
import * as sitterProfilePreferChildEditDialogActions from '../../modules/sitterProfilePreferChildEditDialog';
import * as sitterProfileScheduleEditDialogActions from '../../modules/sitterProfileScheduleEditDialog';
import * as detailScheduleDialogActions from '../../modules/detailScheduleDialog';

import SitterProfileActivityEditDialog from '../SitterProfileActivityEditDialog';
import SitterProfileExperienceEditDialog from '../SitterProfileExperienceEditDialog';
import SitterProfileLocationEditDialog from '../SitterProfileLocationEditDialog';
import SitterProfilePreferChildEditDialog from '../SitterProfilePreferChildEditDialog';
import SitterProfileScheduleEditDialog from '../SitterProfileScheduleEditDialog';
import DetailScheduleDialog from '../DetailScheduleDialog';

moment.locale('ko');

const propTypes = {
  userId: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  userTypeId: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  sitterProfile: PropTypes.object.isRequired,
  SitterProfileActions: PropTypes.object.isRequired,
  SitterProfileActivityEditDialogActions: PropTypes.object.isRequired,
  SitterProfileExperienceEditDialogActions: PropTypes.object.isRequired,
  SitterProfileLocationEditDialogActions: PropTypes.object.isRequired,
  SitterProfilePreferChildEditDialogActions: PropTypes.object.isRequired,
  SitterProfileScheduleEditDialogActions: PropTypes.object.isRequired,
  DetailScheduleDialogActions: PropTypes.object.isRequired,
};
const defaultProps = {};

class SitterProfile extends React.Component {
  async componentDidMount() {
    const { SitterProfileActions, userId } = this.props;
    await SitterProfileActions.fetchSitterProfileData(userId);
  }

  async componentWillReceiveProps(nextProps) {
    const { SitterProfileActions, userId } = nextProps;
    if (this.props.userId !== userId) {
      await SitterProfileActions.fetchSitterProfileData(userId);
    }
  }

  onClickDetailSchedule = () => {
    const { DetailScheduleDialogActions, sitterProfile } = this.props;
    return DetailScheduleDialogActions.setAndOpen(sitterProfile.data.userSchedule);
  };

  onClickSave = async () => {
    const { SitterProfileActions, userId, userTypeId } = this.props;
    await SitterProfileActions.putSitterProfileData(userId, userTypeId);
  };

  onChangeWantedPayment = event => {
    const { sitterProfile, SitterProfileActions } = this.props;
    return SitterProfileActions.setSitterProfileData({
      ...sitterProfile,
      data: {
        ...sitterProfile.data,
        sitterProfile: {
          ...sitterProfile.data.sitterProfile,
          wantedPayment: event.target.value,
        },
      },
    });
  };

  onChangeProfileTitle = event => {
    const { sitterProfile, SitterProfileActions } = this.props;
    return SitterProfileActions.setSitterProfileData({
      ...sitterProfile,
      data: {
        ...sitterProfile.data,
        sitterProfile: {
          ...sitterProfile.data.sitterProfile,
          profileTitle: event.target.value,
        },
      },
    });
  };

  onChangeProfileDescription = event => {
    const { sitterProfile, SitterProfileActions } = this.props;
    return SitterProfileActions.setSitterProfileData({
      ...sitterProfile,
      data: {
        ...sitterProfile.data,
        sitterProfile: {
          ...sitterProfile.data.sitterProfile,
          profileDescription: event.target.value,
        },
      },
    });
  };

  onChangeProfileCCTV = cctv => {
    const { sitterProfile, SitterProfileActions } = this.props;
    return SitterProfileActions.setSitterProfileData({
      ...sitterProfile,
      data: {
        ...sitterProfile.data,
        sitterProfile: {
          ...sitterProfile.data.sitterProfile,
          cctv,
        },
      },
    });
  };

  onChangeProfileStatus = profileStatus => {
    const { sitterProfile, SitterProfileActions } = this.props;
    return SitterProfileActions.setSitterProfileData({
      ...sitterProfile,
      data: {
        ...sitterProfile.data,
        sitterProfile: {
          ...sitterProfile.data.sitterProfile,
          profileStatus,
        },
      },
    });
  };

  onChangeProfileSearchable = searchable => {
    const { sitterProfile, SitterProfileActions } = this.props;
    return SitterProfileActions.setSitterProfileData({
      ...sitterProfile,
      data: {
        ...sitterProfile.data,
        sitterProfile: {
          ...sitterProfile.data.sitterProfile,
          searchable,
        },
      },
    });
  };

  renderProfileSearchable = () => {
    const { sitterProfile } = this.props;
    const { searchable } = sitterProfile.data.sitterProfile;
    const style = {
      active: {
        color: 'default',
      },
      inactive: {
        color: 'default',
      },
    };

    if (searchable) {
      style.active = {
        color: 'primary',
      };
    } else {
      style.inactive = {
        color: 'primary',
      };
    }

    return (
      <Grid container spacing={0}>
        <Grid item xs={6}>
          <Button
            fullWidth
            variant={'raised'}
            color={style.active.color}
            onClick={() => this.onChangeProfileSearchable(true)}
          >
            가능
          </Button>
        </Grid>
        <Grid item xs={6}>
          <Button
            fullWidth
            variant={'raised'}
            color={style.inactive.color}
            onClick={() => this.onChangeProfileSearchable(false)}
          >
            불가능
          </Button>
        </Grid>
      </Grid>
    );
  };

  renderProfileCCTV = () => {
    const { sitterProfile } = this.props;
    const { cctv } = sitterProfile.data.sitterProfile;
    const style = {
      active: {
        color: 'default',
      },
      inactive: {
        color: 'default',
      },
    };

    if (cctv) {
      style.active = {
        color: 'primary',
      };
    } else {
      style.inactive = {
        color: 'primary',
      };
    }

    return (
      <Grid container spacing={0}>
        <Grid item xs={6}>
          <Button
            fullWidth
            variant={'raised'}
            color={style.active.color}
            onClick={() => this.onChangeProfileCCTV(true)}
          >
            동의
          </Button>
        </Grid>
        <Grid item xs={6}>
          <Button
            fullWidth
            variant={'raised'}
            color={style.inactive.color}
            onClick={() => this.onChangeProfileCCTV(false)}
          >
            비동의
          </Button>
        </Grid>
      </Grid>
    );
  };

  renderProfileStatus = () => {
    const { sitterProfile } = this.props;
    const { profileStatus } = sitterProfile.data.sitterProfile;
    const style = {
      active: {
        color: 'default',
      },
      inactive: {
        color: 'default',
      },
    };

    if (profileStatus === ProfileStatusGroup.SITTER_ACTIVE) {
      style.active = {
        color: 'primary',
      };
    } else {
      style.inactive = {
        color: 'primary',
      };
    }

    return (
      <Grid container spacing={0}>
        <Grid item xs={6}>
          <Button
            fullWidth
            variant={'raised'}
            color={style.active.color}
            onClick={() => this.onChangeProfileStatus(ProfileStatusGroup.SITTER_ACTIVE)}
          >
            활성
          </Button>
        </Grid>
        <Grid item xs={6}>
          <Button
            fullWidth
            variant={'raised'}
            color={style.inactive.color}
            onClick={() => this.onChangeProfileStatus(ProfileStatusGroup.SITTER_INACTIVE)}
          >
            비활성
          </Button>
        </Grid>
      </Grid>
    );
  };

  renderPendingUI = () => {
    return (
      <Grid container spacing={16} alignItems={'center'} justify={'center'}>
        <Grid item xs={12}>
          <CircularProgress size={50} />
        </Grid>
      </Grid>
    );
  };

  renderException = message => {
    return (
      <Grid container spacing={16}>
        <Grid item xs={12}>
          <Typography variant={'caption'}> {message} </Typography>
        </Grid>
      </Grid>
    );
  };

  renderNoData = () => {
    return (
      <Grid container spacing={16}>
        <Grid item xs={12}>
          <Typography variant={'caption'}> 표시할 데이터가 없습니다. </Typography>
        </Grid>
      </Grid>
    );
  };

  renderSitterProfile = () => {
    const {
      sitterProfile,
      SitterProfileActivityEditDialogActions,
      SitterProfileExperienceEditDialogActions,
      SitterProfileLocationEditDialogActions,
      SitterProfilePreferChildEditDialogActions,
      SitterProfileScheduleEditDialogActions,
    } = this.props;

    return (
      <Grid container spacing={16}>
        <Grid item xs={12}>
          <Typography variant={'caption'}> 생성일자 </Typography>
        </Grid>
        <Grid item xs={12}>
          {moment(sitterProfile.data.sitterProfile.createDate).format('YYYY년 MM월 DD일 HH시 mm분')}
        </Grid>

        <Grid item xs={12}>
          <Typography variant={'caption'}> 최근 수정일자 </Typography>
        </Grid>
        <Grid item xs={12}>
          {moment(sitterProfile.data.sitterProfile.updateDate).format('YYYY년 MM월 DD일 HH시 mm분')}
        </Grid>

        <Grid item xs={12}>
          <Typography variant={'caption'}> 조회수 </Typography>
        </Grid>
        <Grid item xs={12}>
          {sitterProfile.data.sitterProfile.viewCount}
        </Grid>

        <Grid item xs={12}>
          <Typography variant={'caption'}> 검색가능 여부 </Typography>
        </Grid>
        <Grid item xs={12}>
          {this.renderProfileSearchable()}
        </Grid>

        <Grid item xs={12}>
          <Typography variant={'caption'}> 프로필 상태 </Typography>
        </Grid>
        <Grid item xs={12}>
          {this.renderProfileStatus()}
        </Grid>

        <Grid item xs={12}>
          <Typography variant={'caption'}> CCTV 동의여부 </Typography>
        </Grid>
        <Grid item xs={12}>
          {this.renderProfileCCTV()}
        </Grid>

        <Grid item xs={12}>
          <Typography variant={'caption'}> 프로필 제목 </Typography>
        </Grid>
        <Grid item xs={12}>
          <TextField
            fullWidth
            defaultValue={sitterProfile.data.sitterProfile.profileTitle}
            onChange={this.onChangeProfileTitle}
          />
        </Grid>

        <Grid item xs={12}>
          <Typography variant={'caption'}> 간단 자기소개 </Typography>
        </Grid>
        <Grid item xs={12}>
          <TextField
            fullWidth
            defaultValue={sitterProfile.data.sitterProfile.profileDescription}
            onChange={this.onChangeProfileDescription}
            multiline
          />
        </Grid>

        <Grid item xs={12}>
          <Typography variant={'caption'}> 희망 시급 </Typography>
        </Grid>
        <Grid item xs={12}>
          <TextField
            fullWidth
            defaultValue={sitterProfile.data.sitterProfile.wantedPayment}
            onChange={this.onChangeWantedPayment}
          />
        </Grid>

        <Grid item xs={12}>
          <Button variant={'raised'} onClick={this.onClickDetailSchedule}>
            상세 스케줄 보기
          </Button>
        </Grid>
        <Grid item xs={12}>
          <ProfileSitterSchedule
            title={'돌봄 가능 시간'}
            schedules={sitterProfile.data.userSchedule}
            onEdit={() => SitterProfileScheduleEditDialogActions.openDialog()}
          />
        </Grid>

        <Grid item xs={12}>
          <ProfileSitterPreferChild
            title={'돌봄 가능 연령'}
            childAgeIds={sitterProfile.data.sitterPreferChildInfo.map(item => item.childAgeId)}
            onEdit={() => SitterProfilePreferChildEditDialogActions.openDialog()}
          />
        </Grid>

        <Grid item xs={12}>
          <ProfileActivity
            title={'가능한 활동'}
            activityIds={sitterProfile.data.sitterActivity.map(item => item.activityId)}
            onEdit={() => SitterProfileActivityEditDialogActions.openDialog()}
          />
        </Grid>

        <Grid item xs={12}>
          <ProfileSitterExperience
            title={'관련 경험'}
            sitterExperience={sitterProfile.data.sitterExperience}
            onEdit={() => SitterProfileExperienceEditDialogActions.openDialog()}
          />
        </Grid>

        <Grid item xs={12}>
          <ProfileSitterLocation
            title={'활동 가능한 장소'}
            locations={sitterProfile.data.userLocation}
            onEdit={() => SitterProfileLocationEditDialogActions.openDialog()}
          />
        </Grid>
        <SitterProfileActivityEditDialog />
        <SitterProfileExperienceEditDialog />
        <SitterProfileLocationEditDialog />
        <SitterProfilePreferChildEditDialog />
        <SitterProfileScheduleEditDialog />
        <DetailScheduleDialog />
      </Grid>
    );
  };

  renderContent = () => {
    const { sitterProfile } = this.props;
    if (sitterProfile.pending) {
      return this.renderPendingUI();
    }
    if (sitterProfile.exception) {
      return this.renderException(sitterProfile.exceptionMessage);
    }
    if (!sitterProfile.data) {
      return this.renderNoData();
    }
    if (!sitterProfile.data.sitterProfile) {
      return this.renderProfileNotExist();
    }

    return this.renderSitterProfile();
  };

  renderProfileNotExist = () => {
    return (
      <Grid container spacing={16} alignItems={'center'}>
        <Grid item xs={1}>
          <ErrorIcon />
        </Grid>
        <Grid item xs={11}>
          <Typography variant={'caption'}> 프로필이 없습니다. </Typography>
        </Grid>
      </Grid>
    );
  };

  render() {
    const { SitterProfileActions, userId } = this.props;
    return (
      <Widget
        title={'시터회원 프로필 정보'}
        onRefresh={async () => await SitterProfileActions.fetchSitterProfileData(userId)}
        actions={[
          <Button
            key={'widget-sitter-profile-btn-save'}
            color={'primary'}
            variant={'raised'}
            onClick={this.onClickSave}
          >
            저장하기
          </Button>,
        ]}
      >
        {this.renderContent()}
      </Widget>
    );
  }
}

SitterProfile.propTypes = propTypes;
SitterProfile.defaultProps = defaultProps;

export default compose(
  connect(
    state => ({
      sitterProfile: state.sitterProfile,
    }),
    dispatch => ({
      SitterProfileActions: bindActionCreators(sitterProfileActions, dispatch),
      SitterProfileActivityEditDialogActions: bindActionCreators(sitterProfileActivityEditDialogActions, dispatch),
      SitterProfileExperienceEditDialogActions: bindActionCreators(sitterProfileExperienceEditDialogActions, dispatch),
      SitterProfileLocationEditDialogActions: bindActionCreators(sitterProfileLocationEditDialogActions, dispatch),
      SitterProfilePreferChildEditDialogActions: bindActionCreators(
        sitterProfilePreferChildEditDialogActions,
        dispatch,
      ),
      SitterProfileScheduleEditDialogActions: bindActionCreators(sitterProfileScheduleEditDialogActions, dispatch),
      DetailScheduleDialogActions: bindActionCreators(detailScheduleDialogActions, dispatch),
    }),
  ),
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(SitterProfile);
