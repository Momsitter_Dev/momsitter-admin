import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { compose } from 'recompose';
import React from 'react';
import { Dialog, DialogTitle, DialogActions, DialogContent, Grid, Button, Typography } from '@material-ui/core';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ParentProfileOtherInfoEditDialog.css';
import * as parentProfileOtherInfoEditDialogActions from '../../modules/parentProfileOtherInfoEditDialog';
import * as parentProfileActions from '../../modules/parentProfile';
import FormParentOtherInfo from '../../components/FormParentOtherInfo';

const propTypes = {
  open: PropTypes.bool.isRequired,
  parentProfile: PropTypes.object.isRequired,
  ParentProfileOtherInfoEditDialogActions: PropTypes.object.isRequired,
  ParentProfileActions: PropTypes.object.isRequired,
};
const defaultProps = {};

class ParentProfileOtherInfoEditDialog extends React.Component {
  constructor(props) {
    super(props);
    this.data = this.getDefaultValue();
  }

  onChange = data => {
    this.data = data;
  };

  onClose = () => {
    const { ParentProfileOtherInfoEditDialogActions } = this.props;
    return ParentProfileOtherInfoEditDialogActions.closeDialog();
  };

  onSave = () => {
    const {
      parentProfile,
      ParentProfileActions,
      ParentProfileOtherInfoEditDialogActions,
    } = this.props;

    const {
      wantedSitterGender,
      wantedInterviewWay,
      preferMinAge,
      preferMaxAge,
      wantedCareWay,
    } = this.data;

    ParentProfileActions.setParentProfileData({
      ...parentProfile,
      data: {
        ...parentProfile.data,
        parentProfile: {
          ...parentProfile.data.parentProfile,
          wantedSitterGender,
          wantedInterviewWay,
          preferMinAge,
          preferMaxAge,
          wantedCareWay,
        },
      },
    });

    return ParentProfileOtherInfoEditDialogActions.closeDialog();
  };

  getDefaultValue = () => {
    const { parentProfile } = this.props;
    const data = {
      wantedSitterGender: null,
      wantedInterviewWay: null,
      preferMinAge: null,
      preferMaxAge: null,
      wantedCareWay: null,
    };
    if (!parentProfile) {
      return data;
    }
    if (!parentProfile.data) {
      return data;
    }
    if (!parentProfile.data.parentProfile) {
      return data;
    }

    return {
      wantedSitterGender: parentProfile.data.parentProfile.wantedSitterGender,
      wantedInterviewWay: parentProfile.data.parentProfile.wantedInterviewWay,
      preferMinAge: parentProfile.data.parentProfile.preferMinAge,
      preferMaxAge: parentProfile.data.parentProfile.preferMaxAge,
      wantedCareWay: parentProfile.data.parentProfile.wantedCareWay,
    };
  };

  render() {
    const { open } = this.props;
    const {
      wantedSitterGender,
      wantedInterviewWay,
      preferMinAge,
      preferMaxAge,
      wantedCareWay,
    } = this.getDefaultValue();
    return (
      <div className={s.root}>
        <Dialog open={open} onClose={this.onClose}>
          <DialogTitle>부모회원 - 돌봄 장소 수정</DialogTitle>
          <DialogContent style={{ width: '600px' }}>
            <Grid container spacing={16}>
              <Grid item xs={12}>
                <FormParentOtherInfo
                  defaultPreferMaxAge={preferMaxAge}
                  defaultPreferMinAge={preferMinAge}
                  defaultWantedCareWay={wantedCareWay}
                  defaultWantedInterviewWay={wantedInterviewWay}
                  defaultWantedSitterGender={wantedSitterGender}
                  onChange={this.onChange}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button variant="flat" onClick={this.onClose}>
              취소
            </Button>
            <Button variant="raised" onClick={this.onSave}>
              저장
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

ParentProfileOtherInfoEditDialog.propTypes = propTypes;
ParentProfileOtherInfoEditDialog.defaultProps = defaultProps;

export default compose(
  connect(
    state => ({
      open: state.parentProfileOtherInfoEditDialog.open,
      parentProfile: state.parentProfile,
    }),
    dispatch => ({
      ParentProfileOtherInfoEditDialogActions: bindActionCreators(parentProfileOtherInfoEditDialogActions, dispatch),
      ParentProfileActions: bindActionCreators(parentProfileActions, dispatch),
    }),
  ),
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(ParentProfileOtherInfoEditDialog);
