import { bindActionCreators } from 'redux';
import { compose } from 'recompose';
import moment from 'moment';
import React from 'react';
import { connect } from 'react-redux';
import { Warning as WarningIcon } from '@material-ui/icons';
import {
  Grid,
  Paper,
  IconButton,
  Typography,
  CircularProgress,
  withStyles as withMaterialStyles,
} from '@material-ui/core';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './WidgetCoupon.css';
import Widget from '../../components/Widget';
import SimpleDateFormat from '../../util/dataFormat/SimpleDateFormat';

import * as widgetCouponActions from '../../modules/widgetCoupon';
import * as constCouponTypeActions from '../../modules/constCouponType';

moment.locale('ko');

const propTypes = {
  userId: PropTypes.number.isRequired,

  ConstCouponTypeActions: PropTypes.object.isRequired,
  WidgetCouponActions: PropTypes.object.isRequired,

  pending: PropTypes.bool.isRequired,
  exception: PropTypes.bool.isRequired,
  exceptionMessage: PropTypes.string,

  data: PropTypes.shape({
    couponCode: PropTypes.shape({
      code: PropTypes.string,
      codeId: PropTypes.number,
      couponTypeId: PropTypes.number,
      expireDate: PropTypes.string,
      type: PropTypes.string,
      userId: PropTypes.number,
    }),
    coupons: PropTypes.arrayOf(
      PropTypes.shape({
        codeId: PropTypes.number,
        couponId: PropTypes.number,
        couponTypeId: PropTypes.number,
        expireDate: PropTypes.string,
        paymentId: PropTypes.number,
        pubDate: PropTypes.string,
        read: PropTypes.bool,
        status: PropTypes.string,
        useDate: PropTypes.string,
        userId: PropTypes.number,
      }),
    ),
  }),
  couponTypes: PropTypes.arrayOf(
    PropTypes.shape({
      couponDate: PropTypes.number,
      couponName: PropTypes.string,
      couponPrice: PropTypes.number,
      couponTypeId: PropTypes.number,
      registerDate: PropTypes.string,
      status: PropTypes.string,
      target: PropTypes.string,
    }),
  ),
};
const defaultProps = {
  data: null,
  exceptionMessage: '',
  couponTypes: null,
};

class WidgetCoupon extends React.Component {
  async componentDidMount() {
    const { WidgetCouponActions, ConstCouponTypeActions, couponTypes, userId } = this.props;

    if (!couponTypes) {
      await ConstCouponTypeActions.fetchCouponType();
    }
    await WidgetCouponActions.fetchUserCoupon(userId);
  }

  async componentWillReceiveProps(nextProps) {
    const { WidgetCouponActions, userId } = nextProps;
    if (this.props.userId !== userId) {
      await WidgetCouponActions.fetchUserCoupon(userId);
    }
  }

  onRefresh = async () => {
    const { WidgetCouponActions, userId } = this.props;
    await WidgetCouponActions.fetchUserCoupon(userId);
    return null;
  };

  getCouponName = couponTypeId => {
    try {
      const { couponTypes } = this.props;

      if (!couponTypeId) {
        throw new Error('couponTypeId가 유효하지 않습니다.');
      }

      if (couponTypes instanceof Array) {
        return couponTypes.reduce((acc, item) => {
          if (item.couponTypeId === couponTypeId) {
            acc = item.couponName;
          }
          return acc;
        }, '');
      }
      throw new Error(`쿠폰명을 표시할 수 없습니다. : ${JSON.stringify({ couponTypes, couponTypeId })}`);
    } catch (err) {
      return `표시할 수 없습니다. ${err.message}`;
    }
  };

  renderContent = () => {
    const { pending, exception } = this.props;
    if (exception) {
      return this.renderErrorUI();
    }
    if (pending) {
      return this.renderLoadingUI();
    }
    return this.renderCoupon();
  };

  renderCouponList = () => {
    try {
      const { data } = this.props;
      const { coupons } = data;
      return coupons.map((item, index) => {
        return (
          <Grid item xs={12} key={`coupon-item-${index}`}>
            <Paper style={{ backgroundColor: '#f4f8fb', padding: '10px' }}>
              <Grid container spacing={16}>
                <Grid item xs={12}>
                  <Typography variant={'caption'}> ID </Typography>
                </Grid>
                <Grid item xs={12}>
                  <Typography variant={'body2'}> {item.couponId} </Typography>
                </Grid>

                <Grid item xs={12}>
                  <Typography variant={'caption'}> 쿠폰이름 </Typography>
                </Grid>
                <Grid item xs={12}>
                  <Typography variant={'body2'}> {this.getCouponName(item.couponTypeId)} </Typography>
                </Grid>

                <Grid item xs={12}>
                  <Typography variant={'caption'}> 코드번호 </Typography>
                </Grid>
                <Grid item xs={12}>
                  <Typography variant={'body2'}> {item.codeId} </Typography>
                </Grid>

                <Grid item xs={12}>
                  <Typography variant={'caption'}> 발행일자 </Typography>
                </Grid>
                <Grid item xs={12}>
                  <Typography variant={'body2'}> {SimpleDateFormat(item.pubDate)} </Typography>
                </Grid>

                <Grid item xs={12}>
                  <Typography variant={'caption'}> 만료일자 </Typography>
                </Grid>
                <Grid item xs={12}>
                  <Typography variant={'body2'}> {SimpleDateFormat(item.expireDate)} </Typography>
                </Grid>

                <Grid item xs={12}>
                  <Typography variant={'caption'}> 사용일자 </Typography>
                </Grid>
                <Grid item xs={12}>
                  <Typography variant={'body2'}> {SimpleDateFormat(item.useDate) || '미사용'} </Typography>
                </Grid>

                <Grid item xs={12}>
                  <Typography variant={'caption'}> 결제번호 </Typography>
                </Grid>
                <Grid item xs={12}>
                  <Typography variant={'body2'}> {item.paymentId} </Typography>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
        );
      });
    } catch (err) {
      return (
        <Grid item xs={12}>
          <Paper style={{ padding: '10px' }}>
            <Grid container spacing={16}>
              <Grid item xs={1}>
                <WarningIcon />
              </Grid>
              <Grid item xs={11}>
                <Typography variant={'caption'}> {err.message} </Typography>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
      );
    }
  };

  renderCouponCode = couponCode => {
    if (!couponCode) {
      return (
        <Paper style={{ padding: '10px' }}>
          <Typography variant={'caption'}> 할인코드를 발급받지 않은 회원입니다. </Typography>
        </Paper>
      );
    }

    if (!couponCode.code) {
      return (
        <Paper style={{ padding: '10px' }}>
          <Typography variant={'caption'}> 할인코드를 발급받지 않은 회원입니다. </Typography>
        </Paper>
      );
    }

    return (
      <Paper style={{ padding: '10px' }}>
        <Grid item xs={12}>
          <Typography variant={'caption'}> 쿠폰코드 </Typography>
        </Grid>
        <Grid item xs={12}>
          <Typography variant={'body2'}> {couponCode.code} </Typography>
        </Grid>
      </Paper>
    );
  };

  renderCoupon = () => {
    try {
      const { data } = this.props;
      const { couponCode } = data;

      return (
        <Grid container spacing={16}>
          <Grid item xs={12}>
            {this.renderCouponCode(couponCode)}
          </Grid>
          {this.renderCouponList()}
        </Grid>
      );
    } catch (err) {
      return <Typography variant={'caption'}> 쿠폰정보를 표시할 수 없습니다. {err.message} </Typography>;
    }
  };

  renderLoadingUI = () => {
    return (
      <Grid container spacing={16}>
        <Grid item xs={12}>
          <Paper style={{ padding: '15px', textAlign: 'center' }}>
            <CircularProgress size={50} />
            <Typography variant={'caption'}>데이터를 불러오는 중입니다.</Typography>
          </Paper>
        </Grid>
      </Grid>
    );
  };

  renderErrorUI = () => {
    return (
      <div style={{ textAlign: 'center' }}>
        <IconButton onClick={this.onRefresh}>
          <WarningIcon />
        </IconButton>
        <div style={{ marginTop: '10px' }}> 오류가 발생했습니다. </div>
      </div>
    );
  };

  render() {
    return (
      <Widget title={'쿠폰 현황'} onRefresh={this.onRefresh}>
        {this.renderContent()}
      </Widget>
    );
  }
}

WidgetCoupon.propTypes = propTypes;
WidgetCoupon.defaultProps = defaultProps;

export default compose(
  connect(
    state => ({
      pending: state.widgetCoupon.pending,
      exception: state.widgetCoupon.exception,
      exceptionMessage: state.widgetCoupon.exceptionMessage,
      data: state.widgetCoupon.data,
      couponTypes: state.constCouponType.data,
    }),
    dispatch => ({
      ConstCouponTypeActions: bindActionCreators(constCouponTypeActions, dispatch),
      WidgetCouponActions: bindActionCreators(widgetCouponActions, dispatch),
    }),
  ),
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(WidgetCoupon);
