/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */
import uuid from 'uuid/v4';
import moment from 'moment';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import FlatPagination from 'material-ui-flat-pagination';
import { Warning as WarningIcon } from '@material-ui/icons';
import {
  Grid,
  Paper,
  IconButton,
  Typography,
  CircularProgress,
  withStyles as withMaterialStyles,
} from '@material-ui/core';
import React from 'react';

import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './WidgetMessages.css';

import * as widgetMessagesActions from '../../modules/widgetMessages';

import Widget from '../../components/Widget';
import FilterDataTypeGroup from '../../util/const/FilterDataTypeGroup';

const propTypes = {
  classes: PropTypes.object.isRequired,
  userId: PropTypes.number.isRequired,
  messages: PropTypes.array,
  page: PropTypes.object.isRequired,
  pending: PropTypes.bool,
  exception: PropTypes.bool,
  exceptionMessage: PropTypes.string,
  WidgetMessagesActions: PropTypes.object.isRequired,
};
const defaultProps = {
  messages: [],
  pending: false,
  exception: false,
  exceptionMessage: '',
};

class WidgetMessages extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchOptions: [
        {
          generatedKey: uuid(),
          filterType: 'receiverId',
          filterValue: props.userId,
          filterDataType: FilterDataTypeGroup.UNIQUE_TEXT,
        },
      ],
      orderOption: {
        column: 'sendDate',
        method: 'DESC',
      },
    };
  }

  async componentDidMount() {
    const { WidgetMessagesActions } = this.props;
    await WidgetMessagesActions.fetch(this.getFetchOption());
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.userId !== nextProps.userId) {
      this.setState({
        searchOptions: [
          {
            generatedKey: uuid(),
            filterType: 'receiverId',
            filterValue: nextProps.userId,
            filterDataType: FilterDataTypeGroup.UNIQUE_TEXT,
          },
        ],
      }, async () => { await this.onRefresh(); });
    }
  }

  onClickPage = async (e, offset) => {
    await this.props.WidgetMessagesActions.fetch({ ...this.getFetchOption(), requestPage: offset });
  };

  onRefresh = async () => {
    const { WidgetMessagesActions } = this.props;
    await WidgetMessagesActions.fetch(this.getFetchOption());
    return null;
  };

  getFetchOption = () => {
    return {
      searchOptions: [...this.state.searchOptions],
      orderOption: { ...this.state.orderOption },
      page: this.props.page.currentPage,
    };
  };

  renderPagination = () => {
    const { classes } = this.props;
    const { currentPage, maxPage } = this.props.page;
    return (
      <Paper className={classes.paper} style={{ textAlign: 'left' }}>
        <Grid container spacing={0}>
          <Grid item xs={12}>
            <FlatPagination offset={currentPage} limit={1} total={maxPage} onClick={this.onClickPage} />
          </Grid>
        </Grid>
      </Paper>
    );
  };

  renderMessages = () => {
    const { messages } = this.props;
    if (messages.length === 0) {
      return this.renderEmpty();
    }

    return messages.map((item, index) => {
      const {
        attribute,
        messageId,
        msg,
        msgType,
        receiverId,
        sendDate,
        senderId,
        to,
        msgReceiveUserId,
        msgReceiveUserName,
        msgSendUserId,
        msgSendUserName,
      } = item;
      return (
        <Grid container spacing={16} key={`user-receive-message-item-${index}`}>
          <Grid item xs={12}>
            <Paper style={{ padding: '10px' }}>
              <Grid container spacing={16}>
                <Grid item xs={12}>
                  <Typography variant={'caption'}>메세지 번호</Typography>
                </Grid>
                <Grid item xs={12}>
                  <Typography variant={'body2'}>{messageId}</Typography>
                </Grid>
                <Grid item xs={12}>
                  <Typography variant={'caption'}>발송일자</Typography>
                </Grid>
                <Grid item xs={12}>
                  <Typography variant={'body2'}>{moment(sendDate).format('YYYY년 MM월 DD일 HH시 mm분')}</Typography>
                </Grid>
                <Grid item xs={12}>
                  <Typography variant={'caption'}>핸드폰 번호</Typography>
                </Grid>
                <Grid item xs={12}>
                  <Typography variant={'body2'}>{to}</Typography>
                </Grid>
                {msgSendUserName && (
                  <React.Fragment>
                    <Grid item xs={12}>
                      <Typography variant={'caption'}>발송인</Typography>
                    </Grid>
                    <Grid item xs={12}>
                      <Typography variant={'body2'}>
                        [{msgSendUserId}] {msgSendUserName}
                      </Typography>
                    </Grid>
                  </React.Fragment>
                )}
                {msgType && (
                  <React.Fragment>
                    <Grid item xs={12}>
                      <Typography variant={'caption'}>메세지 타입</Typography>
                    </Grid>
                    <Grid item xs={12}>
                      <Typography variant={'body2'}>{msgType}</Typography>
                    </Grid>
                  </React.Fragment>
                )}
                <Grid item xs={12}>
                  <Typography variant={'caption'}>메세지 내용</Typography>
                </Grid>
                <Grid item xs={12}>
                  <Typography variant={'body2'}>{msg}</Typography>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
        </Grid>
      );
    });
  };

  renderEmpty = () => {
    return (
      <Grid container spacing={16}>
        <Grid item xs={12}>
          <WarningIcon />
          <Typography variant={'caption'}> 표시할 정보가 없습니다. </Typography>
        </Grid>
      </Grid>
    );
  };

  renderContent = () => {
    try {
      const { pending, exception } = this.props;
      if (exception) {
        throw new Error('state exception');
      }
      if (pending) {
        return this.renderLoadingUI();
      }
      return (
        <Grid container spacing={16}>
          <Grid item xs={12}>{this.renderMessages()}</Grid>
          <Grid item xs={12}>{this.renderPagination()}</Grid>
        </Grid>
      );
    } catch (err) {
      return this.renderErrorUI();
    }
  };

  renderLoadingUI = () => {
    return (
      <Grid container spacing={16}>
        <Grid item xs={12}>
          <Paper style={{ padding: '15px', textAlign: 'center' }}>
            <CircularProgress size={50} />
            <Typography variant={'caption'}>데이터를 불러오는 중입니다.</Typography>
          </Paper>
        </Grid>
      </Grid>
    );
  };

  renderErrorUI = (exceptionMessage) => {
    return (
      <div style={{ textAlign: 'center' }}>
        <IconButton onClick={this.onRefresh}>
          <WarningIcon />
        </IconButton>
        <div style={{ marginTop: '10px' }}>오류가 발생했습니다. : {exceptionMessage}</div>
      </div>
    );
  };

  render() {
    return (
      <Widget title={'메세지 현황'} onRefresh={this.onRefresh} defaultExpanded={false}>
        {this.renderContent()}
      </Widget>
    );
  }
}

WidgetMessages.propTypes = propTypes;
WidgetMessages.defaultProps = defaultProps;

export default compose(
  connect(
    state => ({
      messages: state.widgetMessages.data,
      page: state.widgetMessages.page,
    }),
    dispatch => ({
      WidgetMessagesActions: bindActionCreators(widgetMessagesActions, dispatch),
    }),
  ),
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(WidgetMessages);
