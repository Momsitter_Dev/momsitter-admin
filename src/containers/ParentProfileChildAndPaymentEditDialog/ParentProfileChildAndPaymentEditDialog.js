import { toast } from 'react-toastify';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { compose } from 'recompose';
import React from 'react';
import { Dialog, DialogTitle, DialogActions, DialogContent, Grid, Button } from '@material-ui/core';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ParentProfileChildAndPaymentEditDialog.css';
import * as parentProfileChildAndPaymentEditDialogActions from '../../modules/parentProfileChildAndPaymentEditDialog';
import * as parentProfileActions from '../../modules/parentProfile';
import FormParentChildAndPayment from '../../components/FormParentChildAndPayment';

const propTypes = {
  open: PropTypes.bool.isRequired,
  parentProfile: PropTypes.object.isRequired,
  ParentProfileActions: PropTypes.object.isRequired,
  ParentProfileChildAndPaymentEditDialogActions: PropTypes.object.isRequired,
};
const defaultProps = {};

class ParentProfileChildAndPaymentEditDialog extends React.Component {
  constructor(props) {
    super(props);
    this.data = this.getDefaultValue();
  }

  onChange = data => {
    this.data = data;
  };

  onClose = () => {
    const { ParentProfileChildAndPaymentEditDialogActions } = this.props;
    return ParentProfileChildAndPaymentEditDialogActions.closeDialog();
  };

  onSave = () => {
    const { parentProfile, ParentProfileActions, ParentProfileChildAndPaymentEditDialogActions } = this.props;
    const { parentChildInfo, paymentNegotiable, wantedPayment } = this.data;

    const isChildValid = parentChildInfo.reduce((acc, item) => {
      if (!item.childAgeId) {
        acc = false;
      }
      return acc;
    }, true);

    if (!isChildValid) {
      toast.warn('아이정보가 잘못되었습니다. 확인후 다시 저장해주세요.');
      return null;
    }

    ParentProfileActions.setParentProfileData({
      ...parentProfile,
      data: {
        ...parentProfile.data,
        parentProfile: {
          ...parentProfile.data.parentProfile,
          wantedPayment,
          paymentNegotiable,
        },
        parentChildInfo: [...parentChildInfo],
      },
    });

    return ParentProfileChildAndPaymentEditDialogActions.closeDialog();
  };

  getDefaultValue = () => {
    const { parentProfile } = this.props;
    if (!parentProfile) {
      return null;
    }
    if (!parentProfile.data) {
      return null;
    }
    if (!parentProfile.data.parentProfile) {
      return null;
    }
    return {
      wantedPayment: parentProfile.data.parentProfile.wantedPayment,
      paymentNegotiable: parentProfile.data.parentProfile.paymentNegotiable,
      parentChildInfo: parentProfile.data.parentChildInfo,
    };
  };

  getDefaultValues = () => {
    const { parentProfile } = this.props;
    const data = {
      paymentNegotiable: null,
      wantedPayment: null,
      parentChildInfo: null,
    };
    if (!parentProfile) {
      return data;
    }
    if (!parentProfile.data) {
      return data;
    }
    if (!parentProfile.data.parentProfile) {
      return data;
    }
    return {
      paymentNegotiable: parentProfile.data.parentProfile.paymentNegotiable,
      wantedPayment: parentProfile.data.parentProfile.wantedPayment,
      parentChildInfo: parentProfile.data.parentChildInfo,
    };
  };

  render() {
    const { open } = this.props;
    const { paymentNegotiable, wantedPayment, parentChildInfo } = this.getDefaultValues();

    return (
      <div className={s.root}>
        <Dialog open={open} onClose={this.onClose}>
          <DialogTitle>부모회원 - [아이정보, 원하는 시급] 수정</DialogTitle>
          <DialogContent style={{ width: '600px' }}>
            <Grid container spacing={16}>
              <Grid item xs={12}>
                <FormParentChildAndPayment
                  onChange={this.onChange}
                  paymentNegotiable={paymentNegotiable}
                  wantedPayment={wantedPayment}
                  parentChildInfo={parentChildInfo}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button variant="flat" onClick={this.onClose}>
              취소
            </Button>
            <Button variant="raised" onClick={this.onSave}>
              저장
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

ParentProfileChildAndPaymentEditDialog.propTypes = propTypes;
ParentProfileChildAndPaymentEditDialog.defaultProps = defaultProps;

export default compose(
  connect(
    state => ({
      open: state.parentProfileChildAndPaymentEditDialog.open,
      parentProfile: state.parentProfile,
    }),
    dispatch => ({
      ParentProfileChildAndPaymentEditDialogActions: bindActionCreators(
        parentProfileChildAndPaymentEditDialogActions,
        dispatch,
      ),
      ParentProfileActions: bindActionCreators(parentProfileActions, dispatch),
    }),
  ),
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(ParentProfileChildAndPaymentEditDialog);
