/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */
import moment from 'moment';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  Grid,
  Typography,
  TextField,
  CircularProgress,
  Select,
  MenuItem,
  Paper,
} from '@material-ui/core';
import {
  Error as ErrorIcon,
  Delete as DeleteIcon,
} from '@material-ui/icons';
import { withStyles as withMaterialStyle } from '@material-ui/core/styles';
import React from 'react';
import { toast } from 'react-toastify';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CertDataEditDialog.css';
import * as certDataEditDialogActions from '../../modules/certDataEditDialog';
import CertificateTypeIdGroup from '../../util/const/CertificateTypeIdGroup';
import UnivStatusTypeGroup from '../../util/const/UnivStatusTypeGroup';
import UnivDegreeTypeGroup from '../../util/const/UnivDegreeTypeGroup';
import GenderTypeGroup from '../../util/const/GenderTypeGroup';

const styles = theme => ({
  paper: {
    padding: theme.spacing.unit * 3,
    color: theme.palette.text.secondary,
  },
});

const propTypes = {
  classes: PropTypes.object.isRequired,
  certDataEditDialog: PropTypes.object.isRequired,
  CertDataEditDialogActions: PropTypes.object.isRequired,
};
const defaultProps = {};

class CertDataEditDialog extends React.Component {
  constructor(props) {
    super(props);
    this.rejectMsg = '';
    this.state = {
      children: [],
    };
  }

  onChangeChildInfo = (itemIndex, type, value) => {
    const { certDataEditDialog, CertDataEditDialogActions } = this.props;
    const { data } = certDataEditDialog;
    const { certDataParse } = data;
    const { children } = certDataParse;
    return CertDataEditDialogActions.change({
      children: children.map((item, index) => {
        if (index === itemIndex) {
          return {
            ...item,
            [type]: value,
          };
        }
        return item;
      }),
    });
  };

  onClickDeleteChildItem = itemIndex => {
    const { certDataEditDialog, CertDataEditDialogActions } = this.props;
    const { data } = certDataEditDialog;
    const { certDataParse } = data;
    const { children } = certDataParse;
    return CertDataEditDialogActions.change({
      children: children.filter((item, index) => {
        return index !== itemIndex;
      }),
    });
  };

  onClickAddChildItem = () => {
    const { certDataEditDialog, CertDataEditDialogActions } = this.props;
    const { data } = certDataEditDialog;
    const { certDataParse } = data;
    const { children } = certDataParse;
    return CertDataEditDialogActions.change({
      children: [...children, { gender: '', birthday: '' }],
    });
  };

  onClickCancel = () => {
    const { CertDataEditDialogActions } = this.props;
    return CertDataEditDialogActions.close();
  };

  onClickSave = async () => {
    const { CertDataEditDialogActions } = this.props;
    try {
      await CertDataEditDialogActions.save();
    } catch (err) {
      return CertDataEditDialogActions.saveError(err);
    }
  };

  onChangeCertData = (attr, value) => {
    const { CertDataEditDialogActions } = this.props;
    return CertDataEditDialogActions.change({
      [attr]: value,
    });
  };

  renderEnrollmentForm = certDataParse => {
    const { m_univ, m_status, m_degree, m_grade, major, issueDate, graduateDate, expelledDate } = certDataParse;
    return (
      <Grid container spacing={16}>
        <Grid item xs={12}>
          <Typography variant={'caption'}>학교이름</Typography>
        </Grid>
        <Grid item xs={12}>
          <TextField
            fullWidth
            defaultValue={m_univ}
            onChange={event => this.onChangeCertData('m_univ', event.target.value)}
          />
        </Grid>

        <Grid item xs={12}>
          <Typography variant={'caption'}> 전공 </Typography>
        </Grid>
        <Grid item xs={12}>
          <TextField
            fullWidth
            defaultValue={major}
            onChange={event => this.onChangeCertData('major', event.target.value)}
          />
        </Grid>

        <Grid item xs={12}>
          <Typography variant={'caption'}> 학년 </Typography>
        </Grid>
        <Grid item xs={12}>
          <Select
            value={m_grade}
            style={{ width: '100%', textAlign: 'right' }}
            onChange={event => this.onChangeCertData('m_grade', event.target.value)}
          >
            <MenuItem value={1}> 1학년 </MenuItem>
            <MenuItem value={2}> 2학년 </MenuItem>
            <MenuItem value={3}> 3학년 </MenuItem>
            <MenuItem value={4}> 4학년 </MenuItem>
          </Select>
        </Grid>

        <Grid item xs={12}>
          <Typography variant={'caption'}> 학위 </Typography>
        </Grid>
        <Grid item xs={12}>
          <Select
            value={m_degree}
            style={{ width: '100%', textAlign: 'right' }}
            onChange={event => this.onChangeCertData('m_degree', event.target.value)}
          >
            <MenuItem value={UnivDegreeTypeGroup.BACHELOR}> 학사 </MenuItem>
            <MenuItem value={UnivDegreeTypeGroup.MASTER}> 석사 </MenuItem>
            <MenuItem value={UnivDegreeTypeGroup.DOCTOR}> 박사 </MenuItem>
          </Select>
        </Grid>

        <Grid item xs={12}>
          <Typography variant={'caption'}> 학사상태 </Typography>
        </Grid>
        <Grid item xs={12}>
          <Select
            value={m_status}
            style={{ width: '100%', textAlign: 'right' }}
            onChange={event => this.onChangeCertData('m_status', event.target.value)}
          >
            <MenuItem value={UnivStatusTypeGroup.ATTENDING}> 재학 </MenuItem>
            <MenuItem value={UnivStatusTypeGroup.LEAVE}> 휴학 </MenuItem>
            <MenuItem value={UnivStatusTypeGroup.GRADUATE}> 졸업 </MenuItem>
            <MenuItem value={UnivStatusTypeGroup.WITHDRAW}> 퇴학 </MenuItem>
            <MenuItem value={UnivStatusTypeGroup.EXPELLED}> 제적 </MenuItem>
          </Select>
        </Grid>

        <Grid item xs={12}>
          <Typography variant={'caption'}> 발급일자 </Typography>
        </Grid>
        <Grid item xs={12}>
          <TextField
            type={'date'}
            fullWidth
            defaultValue={moment(issueDate).format('YYYY-MM-DD')}
            onChange={event => this.onChangeCertData('issueDate', event.target.value)}
          />
        </Grid>

        {m_status === UnivStatusTypeGroup.GRADUATE ? (
          <Grid container spacing={16}>
            <Grid item xs={12}>
              <Typography variant={'caption'}> 졸업일자 </Typography>
            </Grid>
            <Grid item xs={12}>
              <TextField
                type={'date'}
                fullWidth
                defaultValue={moment(graduateDate).format('YYYY-MM-DD')}
                onChange={event => this.onChangeCertData('graduateDate', event.target.value)}
              />
            </Grid>
          </Grid>
        ) : null}

        {m_status === UnivStatusTypeGroup.EXPELLED ? (
          <Grid container spacing={16}>
            <Grid item xs={12}>
              <Typography variant={'caption'}> 제적일자 </Typography>
            </Grid>
            <Grid item xs={12}>
              <TextField
                type={'date'}
                fullWidth
                defaultValue={moment(expelledDate).format('YYYY-MM-DD')}
                onChange={event => this.onChangeCertData('expelledDate', event.target.value)}
              />
            </Grid>
          </Grid>
        ) : null}
      </Grid>
    );
  };

  renderFamilyRelationForm = certDataParse => {
    const { children, issueDate } = certDataParse;
    return (
      <Grid container spacing={16}>
        <Grid item xs={12}>
          <Typography variant={'caption'}>발급일자</Typography>
        </Grid>
        <Grid item xs={12}>
          <TextField
            type={'date'}
            fullWidth
            defaultValue={moment(issueDate).format('YYYY-MM-DD')}
            onChange={event => this.onChangeCertData('issueDate', event.target.value)}
          />
        </Grid>

        <Grid item xs={12}>
          <Typography variant={'caption'}>아이정보</Typography>
        </Grid>
        <Grid item xs={12}>
          <Button variant={'raised'} onClick={this.onClickAddChildItem}>
            아이정보 추가
          </Button>
        </Grid>
        <Grid item xs={12}>
          {children.map((item, index) => {
            const { gender, birthday } = item;
            return (
              <Paper
                key={`cert-child-info-item-${index}`}
                className={this.props.classes.paper}
                style={{ marginBottom: '15px' }}
              >
                <Grid container spacing={16}>
                  <Button variant={'raised'} onClick={() => this.onClickDeleteChildItem(index)}>
                    아이정보 삭제
                    <DeleteIcon />
                  </Button>
                </Grid>
                <Grid container spacing={16}>
                  <Grid item xs={12}>
                    <Typography variant={'caption'}> 성별 </Typography>
                  </Grid>
                  <Grid item xs={12}>
                    <Select
                      value={gender}
                      onChange={event => this.onChangeChildInfo(index, 'gender', event.target.value)}
                      style={{ width: '100%' }}
                    >
                      <MenuItem value={GenderTypeGroup.MAN}>남자</MenuItem>
                      <MenuItem value={GenderTypeGroup.WOMEN}>여자</MenuItem>
                    </Select>
                  </Grid>
                  <Grid item xs={12}>
                    <Typography variant={'caption'}> 생년월일 </Typography>
                  </Grid>
                  <Grid item xs={12}>
                    <TextField
                      type={'date'}
                      fullWidth
                      defaultValue={moment(birthday).format('YYYY-MM-DD')}
                      onChange={event => this.onChangeChildInfo(index, 'birthday', event.target.value)}
                    />
                  </Grid>
                </Grid>
              </Paper>
            );
          })}
        </Grid>
      </Grid>
    );
  };

  renderPersonalityForm = certDataParse => {
    return (
      <Grid container spacing={16}>
        <Grid item xs={12}>
          <Typography variant={'caption'}>길을 잃으셨군요! 개발자에게 문의해주세요.</Typography>
        </Grid>
      </Grid>
    );
  };

  renderChildcareForm = certDataParse => {
    const { qualificationType, issueDate } = certDataParse;
    return (
      <Grid container spacing={16}>
        <Grid item xs={12}>
          <Typography variant={'caption'}>발급일자</Typography>
        </Grid>
        <Grid item xs={12}>
          <TextField
            type={'date'}
            fullWidth
            defaultValue={moment(issueDate).format('YYYY-MM-DD')}
            onChange={event => this.onChangeCertData('issueDate', event.target.value)}
          />
        </Grid>
        <Grid item xs={12}>
          <Typography variant={'caption'}>자격증 이름</Typography>
        </Grid>
        <Grid item xs={12}>
          <TextField
            fullWidth
            defaultValue={qualificationType}
            onChange={event => this.onChangeCertData('qualificationType', event.target.value)}
          />
        </Grid>
      </Grid>
    );
  };

  renderMedicalExamination = certDataParse => {
    const { issueDate } = certDataParse;
    return (
      <Grid container spacing={16}>
        <Grid item xs={12}>
          <Typography variant={'caption'}>발급일자</Typography>
        </Grid>
        <Grid item xs={12}>
          <TextField
            type={'date'}
            fullWidth
            defaultValue={moment(issueDate).format('YYYY-MM-DD')}
            onChange={event => this.onChangeCertData('issueDate', event.target.value)}
          />
        </Grid>
      </Grid>
    );
  };

  renderResidentRegistrationForm = certDataParse => {
    const { issueDate, location } = certDataParse;
    return (
      <Grid container spacing={16}>
        <Grid item xs={12}>
          <Typography variant={'caption'}>발급일자</Typography>
        </Grid>
        <Grid item xs={12}>
          <TextField
            type={'date'}
            fullWidth
            defaultValue={moment(issueDate).format('YYYY-MM-DD')}
            onChange={event => this.onChangeCertData('issueDate', event.target.value)}
          />
        </Grid>

        <Grid item xs={12}>
          <Typography variant={'caption'}>주소</Typography>
        </Grid>
        <Grid item xs={12}>
          <TextField
            fullWidth
            defaultValue={location}
            onChange={event => this.onChangeCertData('location', event.target.value)}
          />
        </Grid>
      </Grid>
    );
  };

  renderContent = () => {
    try {
      const { certDataEditDialog } = this.props;
      const { pending, exception, exceptionMessage, data } = certDataEditDialog;
      const { certTypeId, certDataParse } = data;
      if (pending) {
        return this.renderPending();
      }
      if (exception) {
        throw new Error(exceptionMessage);
      }
      switch (certTypeId) {
        case CertificateTypeIdGroup.ENROLLMENT:
          return this.renderEnrollmentForm(certDataParse);
        case CertificateTypeIdGroup.FAMILY_RELATION:
          return this.renderFamilyRelationForm(certDataParse);
        case CertificateTypeIdGroup.RESIDENT_REGISTRATION:
          return this.renderResidentRegistrationForm(certDataParse);
        case CertificateTypeIdGroup.PERSONALITY:
          return this.renderPersonalityForm(certDataParse);
        case CertificateTypeIdGroup.CHILDCARE:
          return this.renderChildcareForm(certDataParse);
        case CertificateTypeIdGroup.MEDICAL_EXAMINATION:
          return this.renderMedicalExamination(certDataParse);
        default:
          throw new Error(`모르는 인증타입이에요! ${certTypeId}`);
      }
    } catch (err) {
      return this.renderException(err.message);
    }
  };

  renderPending = () => {
    return (
      <Grid container spacing={16}>
        <Grid item xs={12}>
          <CircularProgress />
        </Grid>
        <Grid item xs={12}>
          <Typography variant={'caption'}>저장중입니다.</Typography>
        </Grid>
      </Grid>
    );
  };

  renderException = exceptionMessage => {
    return (
      <Grid container spacing={16}>
        <Grid item xs={1}>
          <ErrorIcon />
        </Grid>
        <Grid item xs={11}>
          <Typography variant={'caption'}>오류 : {exceptionMessage}</Typography>
        </Grid>
      </Grid>
    );
  };

  render() {
    const { certDataEditDialog } = this.props;
    const { open } = certDataEditDialog;
    return (
      <div>
        <Dialog open={open}>
          <DialogTitle>인증 데이터 수정</DialogTitle>
          <DialogContent>{this.renderContent()}</DialogContent>
          <DialogActions>
            <Button variant={'flat'} onClick={this.onClickCancel}>
              취소
            </Button>
            <Button variant={'raised'} color={'primary'} onClick={this.onClickSave}>
              저장하기
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

CertDataEditDialog.propTypes = propTypes;
CertDataEditDialog.defaultProps = defaultProps;

export default compose(
  connect(
    state => ({
      certDataEditDialog: state.certDataEditDialog,
    }),
    dispatch => ({
      CertDataEditDialogActions: bindActionCreators(certDataEditDialogActions, dispatch),
    }),
  ),
  withStyles(s),
  withMaterialStyle(styles, { withTheme: true }),
)(CertDataEditDialog);
