import {
  withStyles as withMaterialStyles,
  AppBar,
  Button,
  Drawer,
  Divider,
  IconButton,
  Toolbar,
  Typography,
} from '@material-ui/core';
import { ChevronLeft as ChevronLeftIcon, ChevronRight as ChevronRightIcon, Menu as MenuIcon } from '@material-ui/icons';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import classNames from 'classnames';
import { compose } from 'recompose';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Container.css';
import PageContainer from '../../components/PageContainer';
import MenuList from '../../components/MenuList';
import * as containerActions from '../../modules/container';
import * as certPersonalityAddDialogActions from '../../modules/certPersonalityAddDialog';
import CertPersonalityAddDialog from '../../containers/CertPersonalityAddDialog';

const drawerWidth = 240;

const styles = theme => ({
  root: {
    flexGrow: 1,
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  drawerPaper: {
    position: 'relative',
    whiteSpace: 'nowrap',
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing.unit * 7,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing.unit * 9,
    },
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
    paddingTop: '90px',
    overflow: 'auto',
  },
});

const propTypes = {
  CertPersonalityAddDialogActions: PropTypes.object.isRequired,
  ContainerActions: PropTypes.object.isRequired,
  open: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired,
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};
const defaultProps = {};

class Container extends React.Component {
  onClickAddCertPersonality = () => {
    const { CertPersonalityAddDialogActions } = this.props;
    return CertPersonalityAddDialogActions.open();
  };

  handleDrawerOpen = () => {
    const { ContainerActions } = this.props;
    return ContainerActions.openDrawer();
  };

  handleDrawerClose = () => {
    const { ContainerActions } = this.props;
    return ContainerActions.closeDrawer();
  };

  render() {
    const { classes, theme, open } = this.props;

    return (
      <div className={classes.root}>
        <AppBar position="absolute" className={classNames(classes.appBar, open && classes.appBarShift)}>
          <Toolbar disableGutters={!open}>
            <IconButton
              color="inherit"
              aria-label="Open drawer"
              onClick={this.handleDrawerOpen}
              className={classNames(classes.menuButton, open && classes.hide)}
            >
              <MenuIcon />
            </IconButton>
            <div className={s.header}>
              <Typography variant="title" color="inherit" noWrap>
                맘시터
              </Typography>
              <Button variant={'raised'} color={'secondary'} onClick={this.onClickAddCertPersonality}>인성검사 추가</Button>
            </div>
          </Toolbar>
        </AppBar>

        <Drawer
          variant="permanent"
          classes={{
            paper: classNames(classes.drawerPaper, !open && classes.drawerPaperClose),
          }}
          open={open}
        >
          <div className={classes.toolbar}>
            <IconButton onClick={this.handleDrawerClose}>
              {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
            </IconButton>
          </div>
          <Divider />
          <MenuList />
        </Drawer>

        <main className={classes.content}>
          <PageContainer>{this.props.children}</PageContainer>
        </main>
        <CertPersonalityAddDialog />
      </div>
    );
  }
}

Container.propTypes = propTypes;
Container.defaultProps = defaultProps;

export default compose(
  connect(
    state => ({
      open: state.container.open,
    }),
    dispatch => ({
      ContainerActions: bindActionCreators(containerActions, dispatch),
      CertPersonalityAddDialogActions: bindActionCreators(certPersonalityAddDialogActions, dispatch),
    }),
  ),
  withStyles(s),
  withMaterialStyles(styles, { withTheme: true }),
)(Container);
