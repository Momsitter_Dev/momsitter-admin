import { toast } from 'react-toastify';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { compose } from 'recompose';
import React from 'react';
import { Dialog, DialogTitle, DialogActions, DialogContent, Grid, Button } from '@material-ui/core';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ParentProfileScheduleEditDialog.css';
import * as parentProfileScheduleEditDialogActions from '../../modules/parentProfileScheduleEditDialog';
import * as parentProfileActions from '../../modules/parentProfile';
import FormParentSchedule from '../../components/FormParentSchedule';
import ParentScheduleTypeGroup from '../../util/const/ParentScheduleTypeGroup';

const propTypes = {
  open: PropTypes.bool.isRequired,
  parentProfile: PropTypes.object.isRequired,
  ParentProfileActions: PropTypes.object.isRequired,
  ParentProfileScheduleEditDialogActions: PropTypes.object.isRequired,
};
const defaultProps = {};

class ParentProfileScheduleEditDialog extends React.Component {
  constructor(props) {
    super(props);
    this.data = this.getDefaultValue();
  }

  onChange = data => {
    this.data = {
      schedules: data.schedules,
      scheduleType: data.data.scheduleType,
      scheduleNegotiable: data.data.scheduleNegotiable,
    };
  };

  onClose = () => {
    const { ParentProfileScheduleEditDialogActions } = this.props;
    return ParentProfileScheduleEditDialogActions.closeDialog();
  };

  onSave = () => {
    const { parentProfile, ParentProfileActions, ParentProfileScheduleEditDialogActions } = this.props;
    const { schedules, scheduleType, scheduleNegotiable } = this.data;

    if (schedules.length === 0) {
      toast.error('스케줄을 선택해 주세요.');
      return null;
    }

    ParentProfileActions.setParentProfileData({
      ...parentProfile,
      data: {
        ...parentProfile.data,
        parentProfile: {
          ...parentProfile.data.parentProfile,
          scheduleType,
          scheduleNegotiable,
        },
        userSchedule: [...schedules],
      },
    });

    return ParentProfileScheduleEditDialogActions.closeDialog();
  };

  getDefaultValue = () => {
    const { parentProfile } = this.props;
    const data = {
      scheduleType: ParentScheduleTypeGroup.SCHEDULE_TYPE_REGULAR,
      schedules: [],
      scheduleNegotiable: false,
    };
    if (!parentProfile) {
      return data;
    }
    if (!parentProfile.data) {
      return data;
    }
    if (!parentProfile.data.parentProfile) {
      return data;
    }

    return {
      scheduleType: parentProfile.data.parentProfile.scheduleType,
      schedules: parentProfile.data.userSchedule,
      scheduleNegotiable: parentProfile.data.parentProfile.scheduleNegotiable,
    };
  };

  render() {
    const { open } = this.props;
    const { scheduleType, scheduleNegotiable, schedules } = this.getDefaultValue();

    return (
      <div className={s.root}>
        <Dialog open={open} onClose={this.onClose}>
          <DialogTitle>부모회원 - [아이정보, 원하는 시급] 수정</DialogTitle>
          <DialogContent style={{ width: '600px' }}>
            <Grid container spacing={16}>
              <Grid item xs={12}>
                <FormParentSchedule
                  schedules={schedules}
                  scheduleType={scheduleType}
                  scheduleNegotiable={scheduleNegotiable}
                  onChange={this.onChange}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button variant="flat" onClick={this.onClose}>
              취소
            </Button>
            <Button variant="raised" onClick={this.onSave}>
              저장
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

ParentProfileScheduleEditDialog.propTypes = propTypes;
ParentProfileScheduleEditDialog.defaultProps = defaultProps;

export default compose(
  connect(
    state => ({
      open: state.parentProfileScheduleEditDialog.open,
      parentProfile: state.parentProfile,
    }),
    dispatch => ({
      ParentProfileScheduleEditDialogActions: bindActionCreators(parentProfileScheduleEditDialogActions, dispatch),
      ParentProfileActions: bindActionCreators(parentProfileActions, dispatch),
    }),
  ),
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(ParentProfileScheduleEditDialog);
