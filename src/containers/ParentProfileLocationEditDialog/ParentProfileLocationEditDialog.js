import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { compose } from 'recompose';
import React from 'react';
import { Dialog, DialogTitle, DialogActions, DialogContent, Grid, Button, Typography } from '@material-ui/core';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ParentProfileLocationEditDialog.css';
import FormParentLocation from '../../components/FormParentLocation/FormParentLocation';
import * as parentProfileLocationEditDialogActions from '../../modules/parentProfileLocationEditDialog';
import * as parentProfileActions from '../../modules/parentProfile';

const propTypes = {
  open: PropTypes.bool.isRequired,
  parentProfile: PropTypes.object.isRequired,
  ParentProfileLocationEditDialogActions: PropTypes.object.isRequired,
  ParentProfileActions: PropTypes.object.isRequired,
};
const defaultProps = {};

class ParentProfileLocationEditDialog extends React.Component {
  constructor(props) {
    super(props);
    this.location = this.getDefaultValue();
  }

  onChange = data => {
    this.location = data;
  };

  onClose = () => {
    const { ParentProfileLocationEditDialogActions } = this.props;
    return ParentProfileLocationEditDialogActions.closeDialog();
  };

  onSave = () => {
    const { parentProfile, ParentProfileActions, ParentProfileLocationEditDialogActions } = this.props;

    ParentProfileActions.setParentProfileData({
      ...parentProfile,
      data: {
        ...parentProfile.data,
        userLocation: [
          {
            ...this.location,
          },
        ],
      },
    });

    return ParentProfileLocationEditDialogActions.closeDialog();
  };

  getDefaultValue = () => {
    const { parentProfile } = this.props;
    if (!parentProfile) {
      return null;
    }
    if (!parentProfile.data) {
      return null;
    }
    if (!parentProfile.data.userLocation) {
      return null;
    }
    return parentProfile.data.userLocation.reduce((acc, item, index) => {
      if (index === 0) {
        return item;
      }
      return acc;
    }, {});
  };

  render() {
    const { open } = this.props;
    return (
      <div className={s.root}>
        <Dialog open={open} onClose={this.onClose}>
          <DialogTitle>부모회원 - 돌봄 장소 수정</DialogTitle>
          <DialogContent style={{ width: '600px' }}>
            <Grid container spacing={16}>
              <Grid item xs={12}>
                <FormParentLocation defaultValue={this.getDefaultValue()} onChange={this.onChange} />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button variant="flat" onClick={this.onClose}>
              취소
            </Button>
            <Button variant="raised" onClick={this.onSave}>
              저장
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

ParentProfileLocationEditDialog.propTypes = propTypes;
ParentProfileLocationEditDialog.defaultProps = defaultProps;

export default compose(
  connect(
    state => ({
      open: state.parentProfileLocationEditDialog.open,
      parentProfile: state.parentProfile,
    }),
    dispatch => ({
      ParentProfileLocationEditDialogActions: bindActionCreators(parentProfileLocationEditDialogActions, dispatch),
      ParentProfileActions: bindActionCreators(parentProfileActions, dispatch),
    }),
  ),
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(ParentProfileLocationEditDialog);
