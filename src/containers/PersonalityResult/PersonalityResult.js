/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import moment from 'moment';
import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui/svg-icons/navigation/close';
import ArrowUpIcon from 'material-ui/svg-icons/hardware/keyboard-arrow-up';
import ArrowDownIcon from 'material-ui/svg-icons/hardware/keyboard-arrow-down';
import FlatButton from 'material-ui/FlatButton';
import EnhancedButton from 'material-ui/internal/EnhancedButton';
import CircularProgress from 'material-ui/CircularProgress';
import Drawer from '@material-ui/core/Drawer';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { withStyles as withMaterialStyle } from '@material-ui/core/styles';
import React from 'react';

import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './PersonalityResult.css';

import * as personalityResultDrawerActions from '../../modules/personalityResultDrawer';

import DoneIcon from '../../components/svg-icons/Done';
import ClearIcon from '../../components/svg-icons/Clear';
import BarProgress from '../../components/BarProgress';
import ReceptionIcon from '../../components/svg-icons/Reception';

const propTypes = {
  data: PropTypes.object.isRequired,
  exception: PropTypes.bool.isRequired,
  exceptionMessage: PropTypes.bool.isRequired,
  pending: PropTypes.bool.isRequired,
  userId: PropTypes.number.isRequired,
  open: PropTypes.bool.isRequired,
  PersonalityResultDrawerActions: PropTypes.object.isRequired,
};
const defaultProps = {};

class PersonalityResult extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expandChart: false,
      expand: false,
    };
  }

  async componentDidMount() {
    await this.props.PersonalityResultDrawerActions.load();
  }

  async componentWillReceiveProps(nextProps) {
    if (this.props.userId !== nextProps.userId) {
      await this.props.PersonalityResultDrawerActions.load();
    }
  }

  onClickClose = () => {
    return this.props.PersonalityResultDrawerActions.close();
  };

  onClickCheckScore = () => {
    return this.props.PersonalityResultDrawerActions.close();
  };

  toggleChart = () => {
    return this.setState({
      expandChart: !this.state.expandChart,
    });
  };

  toggleExpand = () => {
    return this.setState({ expand: !this.state.expand });
  };

  fetchPersonalityResult = userId => {
    return new Promise(async (resolve, reject) => {});
  };

  getDescriptionStyle = () => {
    if (this.state.expand) {
      return { height: 'auto' };
    }
    return { height: '130px' };
  };

  getGradientStyle = () => {
    if (this.state.expand) {
      return { display: 'none' };
    }
    return { display: 'block' };
  };

  getResultColor = type => {
    if (type === '적합') {
      return '#11b38f';
    }
    return '#ff5252';
  };

  renderResultItem = type => {
    const { data } = this.props;
    if (!data) {
      return null;
    }
    try {
      const checkList = ['personalityAvg', 'stability', 'reliability', 'fictionalReaction'];
      let renderTarget = null;

      if (type) {
        renderTarget = checkList.filter((item, index) => {
          return index === 0;
        });
      } else {
        renderTarget = checkList.filter((item, index) => {
          return index !== 0;
        });
      }

      if (renderTarget.length > 0) {
        return renderTarget.map(item => {
          let unit = '%';
          if (item === 'personalityAvg' || item === 'stability') {
            unit = '점';
          }

          return (
            <div>
              <div className={s.testResultItem} style={{ paddingBottom: type ? '15px' : '40px' }}>
                <div className={s.title}> {CATEGORY_MAP[item]} </div>
                <div className={s.content}>
                  {this.renderRectChartWrap(item)}
                  <div className={s.score}>
                    {' '}
                    {this.props.data[`screen_${item}Value`]}
                    {unit}{' '}
                  </div>
                  <div
                    className={s.circle}
                    style={{ backgroundColor: this.getResultColor(this.props.data[`screen_${item}Yn`]) }}
                  >
                    <div> {this.props.data[`screen_${item}Yn`]} </div>
                    <div> {this.renderResultIcon(this.props.data[`screen_${item}Yn`])} </div>
                  </div>
                </div>
              </div>
              {type ? null : <div className={s.sectionSeparator} />}
            </div>
          );
        });
      }
      return null;
    } catch (err) {
      return null;
    }
  };

  renderRectChartWrap = type => {
    let avg = '60';
    let text = '10점 미만 시 부적합';
    let unit = '%';
    if (type === 'personalityAvg' || type === 'stability') {
      avg = '10';
      unit = '점';
    }
    if (type === 'reliability') {
      text = '60% 이하 시 부적합';
    }
    if (type === 'fictionalReaction') {
      text = '60% 이상 시 부적합';
    }
    return (
      <div className={s.rectChartWrap}>
        <div className={s.top}>
          <div> 0{unit} </div>
          <div>
            {' '}
            100
            {unit}{' '}
          </div>
        </div>
        <div className={s.rectChart}>
          <BarProgress
            value={this.props.data[`screen_${type}Value`]}
            bgColor="#f0f0f0"
            valueColor={this.getResultColor(this.props.data[`screen_${type}Yn`])}
            height="60px"
            showLabel={false}
            avg={avg}
            showAvgLine
            showCross
          />
        </div>
        <div
          className={s.bottom}
          style={{ paddingLeft: !(type === 'personalityAvg' || type === 'stability') ? '20%' : '0' }}
        >
          {' '}
          {text}{' '}
        </div>
      </div>
    );
  };

  renderResultBox = () => {
    try {
      const result = this.props.data.screen_totalYn;
      const trimedResult = result.trim();
      if (trimedResult === '적합') {
        return (
          <div className={s.finalResultBox} style={{ backgroundColor: '#fff', border: '1px solid #d3d3d3' }}>
            <div className={s.title} style={{ color: '#000' }}>
              {' '}
              적합 처리되었습니다.{' '}
            </div>
            <div className={s.desc} style={{ color: '#000', display: 'none' }}>
              {' '}
              위 4가지 항목 모두 적합일 경우만{' '}
            </div>
            <div className={s.desc} style={{ color: '#000', display: 'none' }}>
              {' '}
              최종 적합판정을 받을 수 있습니다.{' '}
            </div>
            <div className={s.btnWrap}>
              <EnhancedButton onClick={this.onClickCheckScore}>
                <div className={s.btnCheckScore} style={{ backgroundColor: '#0fb38e' }}>
                  {' '}
                  내 점수 확인{' '}
                </div>
              </EnhancedButton>
            </div>
          </div>
        );
      }

      return (
        <div className={s.finalResultBox} style={{ backgroundColor: '#ffe4e4' }}>
          <div className={s.title}> 부적합 처리되었습니다. </div>
          <div className={s.desc}> 위 4가지 항목 모두 적합일 경우만 </div>
          <div className={s.desc}> 최종 적합판정을 받을 수 있습니다. </div>
          <div className={s.btnWrap}>
            <EnhancedButton onClick={this.onClickCheckScore}>
              <div className={s.btnCheckScore} style={{ backgroundColor: '#ff5252' }}>
                {' '}
                내 점수 확인{' '}
              </div>
            </EnhancedButton>
          </div>
        </div>
      );
    } catch (err) {
      console.log(err);
      //return this.setState({ error: true });
    }
  };

  renderResultIcon = type => {
    if (type === '적합') {
      return <DoneIcon />;
    }
    return <ClearIcon />;
  };

  renderContent = () => {
    const { exception, pending, exceptionMessage, data } = this.props;
    if (exception) {
      return this.renderError(exceptionMessage);
    }
    if (pending) {
      return this.renderLoading();
    }

    if (!data) {
      return this.renderLoading();
    }
    return this.renderResult();
  };

  renderResult = () => {
    return (
      <div>
        <div className={s.header}>
          <div className={s.btnClose}>
            <IconButton onClick={this.onClickClose}>
              <CloseIcon />
            </IconButton>
          </div>
          <div className={s.title}> 맘시터 인성검사 결과 </div>
          <div className={s.userInfo}>
            <div className={s.userInfoItem}>
              <div className={s.userInfoLabel}> 고유번호: </div>
              <div> {this.props.data.originSequence} </div>
            </div>
            <div className={s.separator} />
            <div className={s.userInfoItem}>
              <div className={s.userInfoLabel}> 검사일자: </div>
              <div> {moment(this.props.data.resultDate).format('MM.DD HH:mm')} </div>
            </div>
            <div className={s.separator} />
            <div className={s.userInfoItem}>
              <div className={s.userInfoLabel}> 성명: </div>
              <div> {this.props.data.userId || this.props.data.name} </div>
            </div>
          </div>
        </div>

        <div className={s.descriptionWrap}>
          <div className={s.descriptionRow}> 맘시터 인성검사는 사람인 VQ 검사로, </div>
          <div className={s.descriptionRow}> (1)인성평균, (2)정서안정성, (3)응답신뢰도, (4)허구반응 </div>
          <div className={s.descriptionRow}> 4가지를 종합적으로 판단합니다. </div>
        </div>

        <div className={s.finalResultWrap}>
          <div className={s.title}> - 최종 결과 - </div>
          <div className={s.finalResultItemWrap}>
            <div className={s.finalResultItem}>
              <div className={s.label}> (1)인성평균 </div>
              <div
                className={s.circle}
                style={{ backgroundColor: this.getResultColor(this.props.data.screen_personalityAvgYn) }}
              >
                <div className={s.label}> {this.props.data.screen_personalityAvgYn} </div>
                <div> {this.renderResultIcon(this.props.data.screen_personalityAvgYn)} </div>
              </div>
            </div>
            <div className={s.finalResultItem}>
              <div className={s.label}> (2)정서안정성 </div>
              <div
                className={s.circle}
                style={{ backgroundColor: this.getResultColor(this.props.data.screen_stabilityYn) }}
              >
                <div className={s.label}> {this.props.data.screen_stabilityYn} </div>
                <div> {this.renderResultIcon(this.props.data.screen_stabilityYn)} </div>
              </div>
            </div>
            <div className={s.finalResultItem}>
              <div className={s.label}> (3)응답신뢰도 </div>
              <div
                className={s.circle}
                style={{ backgroundColor: this.getResultColor(this.props.data.screen_reliabilityYn) }}
              >
                <div className={s.label}> {this.props.data.screen_reliabilityYn} </div>
                <div> {this.renderResultIcon(this.props.data.screen_reliabilityYn)} </div>
              </div>
            </div>
            <div className={s.finalResultItem}>
              <div className={s.label}> (4)허구반응 </div>
              <div
                className={s.circle}
                style={{ backgroundColor: this.getResultColor(this.props.data.screen_fictionalReactionYn) }}
              >
                <div className={s.label}> {this.props.data.screen_fictionalReactionYn} </div>
                <div> {this.renderResultIcon(this.props.data.screen_fictionalReactionYn)} </div>
              </div>
            </div>
          </div>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="302"
            height="90"
            viewBox="0 0 302 90"
            style={{ width: '100%' }}
          >
            <defs>
              <linearGradient id="a" x1="0%" x2="73.271%" y1="50%" y2="50%">
                <stop offset="0%" stopColor="#C5C5C5" stopOpacity="0" />
                <stop offset="100%" stopColor="#C7C7C7" />
              </linearGradient>
            </defs>
            <path
              fill="url(#a)"
              fillRule="evenodd"
              d="M208.335 524.248v-8.988L234 531.485l-25.665 16.225v-8.97c-2.045.107-4.23.164-6.556.164-25.035 0-44.34 50.535-57.915 151.607V380.489c12.845 95.736 32.15 143.604 57.915 143.604 2.33 0 4.514.054 6.556.155z"
              transform="rotate(90 415.5 271)"
            />
          </svg>
          {this.renderResultBox()}
        </div>

        <div className={s.finalResultDesc}>
          <div className={s.title}> - 결과 해설 - </div>
          <div className={s.descriptions} style={this.getDescriptionStyle()}>
            {this.props.data.resultDesc}
            <div className={s.gradientback} style={this.getGradientStyle()} />
          </div>
          <FlatButton
            fullWidth
            label="더보기"
            labelStyle={{ color: '#5e5e5e' }}
            backgroundColor="#f0f0f0"
            onClick={this.toggleExpand}
            icon={this.state.expand ? <ArrowUpIcon /> : <ArrowDownIcon />}
          />
        </div>

        <div className={s.sectionSeparator} />

        {/* TODO: 부적합 항목 먼저 렌더링 */}

        <div id="reason">{this.renderResultItem(true)}</div>
        {this.state.expandChart ? (
          <div>
            <div>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="302"
                height="90"
                viewBox="0 0 302 90"
                style={{ width: '100%' }}
              >
                <defs>
                  <linearGradient id="a" x1="0%" x2="73.271%" y1="50%" y2="50%">
                    <stop offset="0%" stopColor="#C5C5C5" stopOpacity="0" />
                    <stop offset="100%" stopColor="#C7C7C7" />
                  </linearGradient>
                </defs>
                <path
                  fill="url(#a)"
                  fillRule="evenodd"
                  d="M208.335 524.248v-8.988L234 531.485l-25.665 16.225v-8.97c-2.045.107-4.23.164-6.556.164-25.035 0-44.34 50.535-57.915 151.607V380.489c12.845 95.736 32.15 143.604 57.915 143.604 2.33 0 4.514.054 6.556.155z"
                  transform="rotate(90 415.5 271)"
                />
              </svg>
            </div>

            <div id="chartWrap" className={s.resultChartsWrap}>
              <div className={s.chartTitle}> * 인성평균 백분위 분포도 * </div>
              <div className={s.lineChartWrap}>
                <img
                  className={s.charImage}
                  src={`https://vq.saramin.co.kr/webvqV02/result/chart/2xyDEF.asp?avg=${
                    this.props.data.resultAVG
                  }&stdev=${this.props.data.resultSTDEV}&mans=${this.props.data.resultValue}`}
                />
              </div>
              <div className={s.barChartWrap}>
                <div className={s.chartDesc}>
                  <div className={s.chartDescTitle}> * 백분위 점수 * </div>
                  <div className={s.descItemWrap}>
                    <div className={s.descItem}>
                      <div> 90점이상 : 매우높음(S) </div>
                      <div> 30점이상 : 보통하(C) </div>
                    </div>
                    <div className={s.descItem}>
                      <div> 70점이상 : 높음(A) </div>
                      <div> 10점이상 : 낮음(D) </div>
                    </div>
                    <div className={s.descItem}>
                      <div> 50이상 : 보통상(B) </div>
                      <div> 10점미만 : 매우낮음(E) </div>
                    </div>
                  </div>
                </div>
                <div className={s.barChartRowWrap}>
                  <div className={s.chartBorder} />
                  <div className={s.barChartRow}>
                    <div className={s.name}> 성실성 </div>
                    <div className={s.barWrap}>
                      <BarProgress
                        value={this.props.data.integrityValue}
                        bgColor="transparent"
                        valueColor="#4b9cd2"
                        height="25px"
                      />
                    </div>
                    <div className={s.emptySpace} />
                    <div className={s.rank}>
                      <div className={s.rankGrade}> {this.props.data.integrityGrade} </div>
                      <div> 순위 {this.props.data.integrityPriority} </div>
                    </div>
                  </div>
                  <div className={s.chartBorder} />
                  <div className={s.barChartRow}>
                    <div className={s.name}> 대인관계성 </div>
                    <div className={s.barWrap}>
                      <BarProgress
                        value={this.props.data.relationShipValue}
                        bgColor="transparent"
                        valueColor="#4b9cd2"
                        height="25px"
                      />
                    </div>
                    <div className={s.emptySpace} />
                    <div className={s.rank}>
                      <div className={s.rankGrade}> {this.props.data.relationShipGrade} </div>
                      <div> 순위 {this.props.data.releationShipPriority} </div>
                    </div>
                  </div>
                  <div className={s.chartBorder} />
                  <div className={s.barChartRow}>
                    <div className={s.name}> 이타성 </div>
                    <div className={s.barWrap}>
                      <BarProgress
                        value={this.props.data.altitudeValue}
                        bgColor="transparent"
                        valueColor="#4b9cd2"
                        height="25px"
                      />
                    </div>
                    <div className={s.emptySpace} />
                    <div className={s.rank}>
                      <div className={s.rankGrade}> {this.props.data.altitudeGrade} </div>
                      <div> 순위 {this.props.data.altitudePriority} </div>
                    </div>
                  </div>
                  <div className={s.chartBorder} />
                  <div className={s.barChartRow}>
                    <div className={s.name}> 지적개방성 </div>
                    <div className={s.barWrap}>
                      <BarProgress
                        value={this.props.data.intellectualValue}
                        bgColor="transparent"
                        valueColor="#4b9cd2"
                        height="25px"
                      />
                    </div>
                    <div className={s.emptySpace} />
                    <div className={s.rank}>
                      <div className={s.rankGrade}> {this.props.data.intellectualGrade} </div>
                      <div> 순위 {this.props.data.intellectualPriority} </div>
                    </div>
                  </div>
                  <div className={s.chartBorder} />
                  <div className={s.barChartRow}>
                    <div className={s.name}> 정직성 </div>
                    <div className={s.barWrap}>
                      <BarProgress
                        value={this.props.data.honestyValue}
                        bgColor="transparent"
                        valueColor="#4b9cd2"
                        height="25px"
                      />
                    </div>
                    <div className={s.emptySpace} />
                    <div className={s.rank}>
                      <div className={s.rankGrade}> {this.props.data.honestyGrade} </div>
                      <div> 순위 {this.props.data.honestyPriority} </div>
                    </div>
                  </div>
                  <div className={s.chartBorder} />
                  <div className={s.barChartRow}>
                    <div className={s.name}> 리더십 </div>
                    <div className={s.barWrap}>
                      <BarProgress
                        value={this.props.data.leadershipValue}
                        bgColor="transparent"
                        valueColor="#4b9cd2"
                        height="25px"
                      />
                    </div>
                    <div className={s.emptySpace} />
                    <div className={s.rank}>
                      <div className={s.rankGrade}> {this.props.data.leadershipGrade} </div>
                      <div> 순위 {this.props.data.leadershipPriority} </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : null}

        <div style={{ padding: '0 30px', marginBottom: '25px' }}>
          <FlatButton
            fullWidth
            backgroundColor="#f0f0f0"
            labelStyle={{ color: '#5e5e5e' }}
            label="더보기"
            icon={this.state.expandChart ? <ArrowUpIcon /> : <ArrowDownIcon />}
            onClick={this.toggleChart}
          />
        </div>
        <div className={s.sectionSeparator} />

        <div>{this.renderResultItem()}</div>

        <div className={s.footer}>
          <div className={s.icon}>
            <ReceptionIcon />
          </div>

          <div className={s.titleWrap}>
            <div className={s.title}> 인성검사 전문 상담 </div>
            <div className={s.subTitle}> 검사결과 해석에 대한 문의는 </div>
            <div className={s.subTitle}> 02-2025-2651로 해주세요. </div>
          </div>

          <div className={s.desc}> - 상담 가능 시간 - </div>
          <div className={s.desc}> 평일 09:00 ~ 19:00 </div>
          <div className={s.desc}> 토/일/공휴일 휴무 </div>
        </div>
      </div>
    );
  };

  renderLoading = () => {
    return (
      <div className={s.loading}>
        <div className={s.btnClose}>
          <IconButton onClick={this.onClickClose}>
            <CloseIcon />
          </IconButton>
        </div>
        <CircularProgress size={100} />
        <div style={{ marginTop: '30px', fontSize: '1.1rem', fontWeight: '500', color: '#5b5b5b' }}>
          {' '}
          인성검사 결과를 불러오는 중입니다.{' '}
        </div>
      </div>
    );
  };

  renderError = message => {
    return (
      <div style={{ position: 'relative' }}>
        <div className={s.btnClose}>
          <IconButton onClick={this.onClickClose}>
            <CloseIcon />
          </IconButton>
        </div>
        인성검사 결과를 조회할 수 없습니다. {`: ${message}`}
      </div>
    );
  };

  render() {
    return (
      <div>
        <Drawer open={this.props.open} anchor="right" onClose={this.props.PersonalityResultDrawerActions.close}>
          <div className={s.root}>
            {this.renderContent()}
          </div>
        </Drawer>
      </div>
    );
  }
}

PersonalityResult.propTypes = propTypes;
PersonalityResult.defaultProps = defaultProps;

export default compose(
  connect(
    state => ({
      data: state.personalityResultDrawer.data,
      userId: state.personalityResultDrawer.userId,
      open: state.personalityResultDrawer.open,
      pending: state.personalityResultDrawer.pending,
      exception: state.personalityResultDrawer.exception,
      exceptionMessage: state.personalityResultDrawer.exceptionMessage,
    }),
    dispatch => ({
      PersonalityResultDrawerActions: bindActionCreators(personalityResultDrawerActions, dispatch),
    }),
  ),
  withStyles(s),
  withMaterialStyle(s, { withTheme: true }),
)(PersonalityResult);
