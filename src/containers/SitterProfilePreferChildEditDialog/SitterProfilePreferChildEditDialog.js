import { toast } from 'react-toastify';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { compose } from 'recompose';
import React from 'react';
import { Dialog, DialogTitle, DialogActions, DialogContent, Grid, Button } from '@material-ui/core';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './SitterProfilePreferChildEditDialog.css';
import * as sitterProfilePreferChildEditDialogActions from '../../modules/sitterProfilePreferChildEditDialog';
import * as sitterProfileActions from '../../modules/sitterProfile';
import FormCareAge from '../../components/FormCareAge';

const propTypes = {
  open: PropTypes.bool.isRequired,
  sitterProfile: PropTypes.object.isRequired,
  SitterProfileActions: PropTypes.object.isRequired,
  SitterProfilePreferChildEditDialogActions: PropTypes.object.isRequired,
};
const defaultProps = {};

class SitterProfilePreferChildEditDialog extends React.Component {
  constructor(props) {
    super(props);
    this.data = this.getDefaultValue();
  }

  onChange = data => {
    this.data = data.ageIds.map((item) => {
      return {
        childAgeId: parseInt(item, 10),
      };
    });
  };

  onClose = () => {
    const { SitterProfilePreferChildEditDialogActions } = this.props;
    return SitterProfilePreferChildEditDialogActions.closeDialog();
  };

  onSave = () => {
    if (!this.data) {
      return toast.warn('돌봄 가능연령을 선택해주세요.');
    }
    if (this.data.length === 0) {
      return toast.warn('돌봄 가능연령을 선택해주세요.');
    }

    const { sitterProfile, SitterProfileActions, SitterProfilePreferChildEditDialogActions } = this.props;

    SitterProfileActions.setSitterProfileData({
      ...sitterProfile,
      data: {
        ...sitterProfile.data,
        sitterPreferChildInfo: [...this.data],
      },
    });

    return SitterProfilePreferChildEditDialogActions.closeDialog();
  };

  getDefaultValue = () => {
    const { sitterProfile } = this.props;
    const data = [];
    if (!sitterProfile) {
      return data;
    }
    if (!sitterProfile.data) {
      return data;
    }
    if (!sitterProfile.data.sitterPreferChildInfo) {
      return data;
    }
    return sitterProfile.data.sitterPreferChildInfo.map(item => item.childAgeId);
  };

  render() {
    const { open } = this.props;
    return (
      <div className={s.root}>
        <Dialog open={open} onClose={this.onClose}>
          <DialogTitle> 시터회원 - 돌봄 가능 연령 수정 </DialogTitle>
          <DialogContent style={{ width: '600px' }}>
            <Grid container spacing={16}>
              <Grid item xs={12}>
                <FormCareAge
                  initialSelectedIndex={this.getDefaultValue()}
                  onChange={this.onChange}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button variant="flat" onClick={this.onClose}>
              취소
            </Button>
            <Button variant="raised" onClick={this.onSave}>
              저장
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

SitterProfilePreferChildEditDialog.propTypes = propTypes;
SitterProfilePreferChildEditDialog.defaultProps = defaultProps;

export default compose(
  connect(
    state => ({
      open: state.sitterProfilePreferChildEditDialog.open,
      sitterProfile: state.sitterProfile,
    }),
    dispatch => ({
      SitterProfilePreferChildEditDialogActions: bindActionCreators(
        sitterProfilePreferChildEditDialogActions,
        dispatch,
      ),
      SitterProfileActions: bindActionCreators(sitterProfileActions, dispatch),
    }),
  ),
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(SitterProfilePreferChildEditDialog);
