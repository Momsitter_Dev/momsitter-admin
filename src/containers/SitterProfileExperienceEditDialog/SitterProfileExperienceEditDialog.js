import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { compose } from 'recompose';
import React from 'react';
import { Dialog, DialogTitle, DialogActions, DialogContent, Grid, Button } from '@material-ui/core';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './SitterProfileExperienceEditDialog.css';
import * as sitterProfileExperienceEditDialogActions from '../../modules/sitterProfileExperienceEditDialog';
import * as sitterProfileActions from '../../modules/sitterProfile';
import FormSitterProfileExperience from '../../components/FormSitterProfileExperience';

const propTypes = {
  open: PropTypes.bool.isRequired,
  sitterProfile: PropTypes.object.isRequired,
  SitterProfileActions: PropTypes.object.isRequired,
  SitterProfileExperienceEditDialogActions: PropTypes.object.isRequired,
};
const defaultProps = {};

class SitterProfileExperienceEditDialog extends React.Component {
  constructor(props) {
    super(props);
    this.data = this.getDefaultValue();
  }

  onChange = data => {
    this.data = data;
  };

  onClose = () => {
    const { SitterProfileExperienceEditDialogActions } = this.props;
    return SitterProfileExperienceEditDialogActions.closeDialog();
  };

  onSave = () => {
    const { sitterProfile, SitterProfileActions, SitterProfileExperienceEditDialogActions } = this.props;

    SitterProfileActions.setSitterProfileData({
      ...sitterProfile,
      data: {
        ...sitterProfile.data,
        sitterExperience: [...this.data],
      },
    });

    return SitterProfileExperienceEditDialogActions.closeDialog();
  };

  getDefaultValue = () => {
    const { sitterProfile } = this.props;
    const data = [];
    if (!sitterProfile) {
      return data;
    }
    if (!sitterProfile.data) {
      return data;
    }
    if (!sitterProfile.data.sitterExperience) {
      return data;
    }
    return sitterProfile.data.sitterExperience;
  };

  render() {
    const { open } = this.props;
    return (
      <div className={s.root}>
        <Dialog open={open} onClose={this.onClose}>
          <DialogTitle> 시터회원 - 관련 경험 수정 </DialogTitle>
          <DialogContent style={{ width: '600px' }}>
            <Grid container spacing={16}>
              <Grid item xs={12}>
                <FormSitterProfileExperience
                  onChange={this.onChange}
                  experiences={this.getDefaultValue()}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button variant="flat" onClick={this.onClose}>
              취소
            </Button>
            <Button variant="raised" onClick={this.onSave}>
              저장
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

SitterProfileExperienceEditDialog.propTypes = propTypes;
SitterProfileExperienceEditDialog.defaultProps = defaultProps;

export default compose(
  connect(
    state => ({
      open: state.sitterProfileExperienceEditDialog.open,
      sitterProfile: state.sitterProfile,
    }),
    dispatch => ({
      SitterProfileExperienceEditDialogActions: bindActionCreators(sitterProfileExperienceEditDialogActions, dispatch),
      SitterProfileActions: bindActionCreators(sitterProfileActions, dispatch),
    }),
  ),
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(SitterProfileExperienceEditDialog);
