import moment from 'moment';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { compose } from 'recompose';

import React from 'react';
import {
  Grid,
  Typography,
  Button,
  TextField,
  CircularProgress,
  withStyles as withMaterialStyles,
} from '@material-ui/core';
import { ErrorOutline as ErrorIcon, Warning as WarningIcon } from '@material-ui/icons';

import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ParentProfile.css';

import Widget from '../../components/Widget/Widget';
import ParentSchedule from '../../components/ParentSchedule/ParentSchedule';
import * as parentProfileActions from '../../modules/parentProfile';
import * as parentProfileLocationEditDialogActions from '../../modules/parentProfileLocationEditDialog';
import * as parentProfileActivityEditDialogActions from '../../modules/parentProfileActivityEditDialog';
import * as parentProfileChildAndPaymentEditDialogActions from '../../modules/parentProfileChildAndPaymentEditDialog';
import * as parentProfileScheduleEditDialogActions from '../../modules/parentProfileScheduleEditDialog';
import * as parentProfileOtherInfoEditDialogActions from '../../modules/parentProfileOtherInfoEditDialog';
import * as detailScheduleDialogActions from '../../modules/detailScheduleDialog';

import ProfileActivity from '../../components/ProfileActivity/ProfileActivity';
import ProfileParentLocation from '../../components/ProfileParentLocation/ProfileParentLocation';
import ProfileParentChildInfo from '../../components/ProfileParentChildInfo/ProfileParentChildInfo';
import ProfileParentOtherInfo from '../../components/ProfileParentOtherInfo/ProfileParentOtherInfo';
import ProfileStatusGroup from '../../util/const/ProfileStatusGroup';

import ParentProfileLocationEditDialog from '../ParentProfileLocationEditDialog';
import ParentProfileActivityEditDialog from '../ParentProfileActivityEditDialog';
import ParentProfileChildAndPaymentEditDialog from '../ParentProfileChildAndPaymentEditDialog';
import ParentProfileScheduleEditDialog from '../ParentProfileScheduleEditDialog';
import ParentProfileOtherInfoEditDialog from '../ParentProfileOtherInfoEditDialog';
import DetailScheduleDialog from '../DetailScheduleDialog';

const propTypes = {
  userId: PropTypes.number.isRequired,
  userTypeId: PropTypes.number.isRequired,
  parentProfile: PropTypes.object.isRequired,
  ParentProfileActions: PropTypes.object.isRequired,
  ParentProfileLocationEditDialogActions: PropTypes.object.isRequired,
  ParentProfileActivityEditDialogActions: PropTypes.object.isRequired,
  ParentProfileChildAndPaymentEditDialogActions: PropTypes.object.isRequired,
  ParentProfileScheduleEditDialogActions: PropTypes.object.isRequired,
  ParentProfileOtherInfoEditDialogActions: PropTypes.object.isRequired,
  DetailScheduleDialogActions: PropTypes.object.isRequired,
};
const defaultProps = {};

// TODO: 응답률 데이터 표시
class ParentProfile extends React.Component {
  async componentDidMount() {
    const { ParentProfileActions, userId } = this.props;
    await ParentProfileActions.fetchParentProfileData(userId);
  }

  async componentWillReceiveProps(nextProps) {
    const { ParentProfileActions, userId } = nextProps;
    if (this.props.userId !== userId) {
      await ParentProfileActions.fetchParentProfileData(userId);
    }
  }

  onClickDetailSchedule = () => {
    const { DetailScheduleDialogActions, parentProfile } = this.props;
    return DetailScheduleDialogActions.setAndOpen(parentProfile.data.userSchedule);
  };

  onClickSave = async () => {
    const { ParentProfileActions, userId, userTypeId } = this.props;
    await ParentProfileActions.putParentProfileData(userId, userTypeId);
  };

  onChangeProfileTitle = event => {
    const { parentProfile, ParentProfileActions } = this.props;
    return ParentProfileActions.setParentProfileData({
      ...parentProfile,
      data: {
        ...parentProfile.data,
        parentProfile: {
          ...parentProfile.data.parentProfile,
          profileTitle: event.target.value,
        },
      },
    });
  };

  onChangeProfileDescription = event => {
    const { parentProfile, ParentProfileActions } = this.props;
    return ParentProfileActions.setParentProfileData({
      ...parentProfile,
      data: {
        ...parentProfile.data,
        parentProfile: {
          ...parentProfile.data.parentProfile,
          profileDescription: event.target.value,
        },
      },
    });
  };

  onChangeProfileStatus = profileStatus => {
    const { parentProfile, ParentProfileActions } = this.props;
    return ParentProfileActions.setParentProfileData({
      ...parentProfile,
      data: {
        ...parentProfile.data,
        parentProfile: {
          ...parentProfile.data.parentProfile,
          profileStatus,
        },
      },
    });
  };

  renderPendingUI = () => (
    <Grid container spacing={16}>
      <Grid item xs={4}>
        <CircularProgress size={25} />
      </Grid>
      <Grid item xs={8}>
        <Typography variant="caption"> 데이터를 불러오는 중입니다. </Typography>
      </Grid>
    </Grid>
  );

  renderExceptionUI = message => (
    <Grid container spacing={16}>
      <Grid item xs={4}>
        <WarningIcon />
      </Grid>
      <Grid item xs={8}>
        <Typography variant="caption"> {message} </Typography>
      </Grid>
    </Grid>
  );

  renderProfileStatus = () => {
    const { parentProfile } = this.props;
    const { profileStatus } = parentProfile.data.parentProfile;
    const style = {
      active: {
        color: 'default',
      },
      inactive: {
        color: 'default',
      },
    };

    if (profileStatus === ProfileStatusGroup.PARENT_ACTIVE) {
      style.active = {
        color: 'primary',
      };
    } else {
      style.inactive = {
        color: 'primary',
      };
    }

    return (
      <Grid container spacing={0}>
        <Grid item xs={6}>
          <Button
            fullWidth
            variant="raised"
            color={style.active.color}
            onClick={() => this.onChangeProfileStatus(ProfileStatusGroup.PARENT_ACTIVE)}
          >
            활성
          </Button>
        </Grid>
        <Grid item xs={6}>
          <Button
            fullWidth
            variant="raised"
            onClick={() => this.onChangeProfileStatus(ProfileStatusGroup.PARENT_INACTIVE)}
            color={style.inactive.color}
          >
            비활성
          </Button>
        </Grid>
      </Grid>
    );
  };

  renderProfile = () => {
    const {
      parentProfile,
      ParentProfileLocationEditDialogActions,
      ParentProfileActivityEditDialogActions,
      ParentProfileChildAndPaymentEditDialogActions,
      ParentProfileScheduleEditDialogActions,
      ParentProfileOtherInfoEditDialogActions,
    } = this.props;

    return (
      <Grid container spacing={16} alignItems="center">
        <Grid item xs={12}>
          <Typography variant="caption"> 생성일자 </Typography>
        </Grid>
        <Grid item xs={12}>
          <Typography>
            {moment(parentProfile.data.parentProfile.createDate).format('YYYY년 MM월 DD일 HH시 mm분')}
          </Typography>
        </Grid>

        <Grid item xs={12}>
          <Typography variant="caption"> 최근 수정일자 </Typography>
        </Grid>
        <Grid item xs={12}>
          {moment(parentProfile.data.parentProfile.updateDate).format('YYYY년 MM월 DD일 HH시 mm분')}
        </Grid>

        <Grid item xs={12}>
          <Typography variant="caption"> 조회수 </Typography>
        </Grid>
        <Grid item xs={12}>
          {parentProfile.data.parentProfile.viewCount}
        </Grid>

        <Grid item xs={12}>
          <Typography variant="caption"> 신청서 상태 </Typography>
        </Grid>

        <Grid item xs={12}>
          {this.renderProfileStatus()}
        </Grid>

        <Grid item xs={12}>
          <Typography variant="caption"> 신청서 제목 </Typography>
        </Grid>
        <Grid item xs={12}>
          <TextField
            fullWidth
            onChange={this.onChangeProfileTitle}
            defaultValue={parentProfile.data.parentProfile.profileTitle}
          />
        </Grid>

        <Grid item xs={12}>
          <Typography variant="caption"> 신청서 설명 </Typography>
        </Grid>
        <Grid item xs={12}>
          <TextField
            fullWidth
            multiline
            onChange={this.onChangeProfileDescription}
            defaultValue={parentProfile.data.parentProfile.profileDescription}
          />
        </Grid>

        <Grid item xs={12}>
          <ProfileParentChildInfo
            title="아이정보 / 원하는 시급"
            parentChildInfo={parentProfile.data.parentChildInfo}
            wantedPayment={parentProfile.data.parentProfile.wantedPayment}
            paymentNegotiable={parentProfile.data.parentProfile.paymentNegotiable}
            onEdit={() => ParentProfileChildAndPaymentEditDialogActions.openDialog()}
          />
        </Grid>

        <Grid item xs={12}>
          <ProfileParentLocation
            title="원하는 장소"
            locations={parentProfile.data.userLocation}
            onEdit={() => ParentProfileLocationEditDialogActions.openDialog()}
          />
        </Grid>

        <Grid item xs={12}>
          <ProfileActivity
            title="원하는 활동"
            activityIds={parentProfile.data.parentActivity.map(item => item.activityId)}
            onEdit={() => ParentProfileActivityEditDialogActions.openDialog()}
          />
        </Grid>

        <Grid item xs={12}>
          <Button variant={'raised'} onClick={this.onClickDetailSchedule}>
            {' '}
            상세 스케줄 보기{' '}
          </Button>
        </Grid>
        <Grid item xs={12}>
          <ParentSchedule
            scheduleType={parentProfile.data.parentProfile.scheduleType}
            schedules={parentProfile.data.userSchedule}
            scheduleNegotiable={parentProfile.data.parentProfile.scheduleNegotiable}
            onEdit={() => ParentProfileScheduleEditDialogActions.openDialog()}
          />
        </Grid>

        <Grid item xs={12}>
          <ProfileParentOtherInfo
            title="그 외 요청사항"
            wantedInterviewWay={parentProfile.data.parentProfile.wantedInterviewWay}
            wantedCareWay={parentProfile.data.parentProfile.wantedCareWay}
            wantedSitterGender={parentProfile.data.parentProfile.wantedSitterGender}
            preferMinAge={parentProfile.data.parentProfile.preferMinAge}
            preferMaxAge={parentProfile.data.parentProfile.preferMaxAge}
            onEdit={() => ParentProfileOtherInfoEditDialogActions.openDialog()}
          />
        </Grid>
        <ParentProfileLocationEditDialog />
        <ParentProfileActivityEditDialog />
        <ParentProfileChildAndPaymentEditDialog />
        <ParentProfileScheduleEditDialog />
        <ParentProfileOtherInfoEditDialog />
        <DetailScheduleDialog />
      </Grid>
    );
  };

  renderProfileNotExist = () => {
    return (
      <Grid container spacing={16} alignItems={'center'}>
        <Grid item xs={1}>
          <ErrorIcon />
        </Grid>
        <Grid item xs={11}>
          <Typography variant={'caption'}> 프로필이 없습니다. </Typography>
        </Grid>
      </Grid>
    );
  };

  renderContent = () => {
    const { parentProfile } = this.props;
    const { pending, exception, exceptionMessage } = parentProfile;

    if (exception) {
      return this.renderExceptionUI(exceptionMessage);
    }

    if (pending) {
      return this.renderPendingUI();
    }

    if (!parentProfile) {
      return this.renderPendingUI();
    }

    if (!parentProfile.data) {
      return this.renderPendingUI();
    }

    if (!parentProfile.data.parentProfile) {
      return this.renderProfileNotExist();
    }

    return this.renderProfile();
  };

  render() {
    const { ParentProfileActions, userId } = this.props;
    return (
      <Widget
        title="부모회원 프로필 정보"
        onRefresh={async () => await ParentProfileActions.fetchParentProfileData(userId)}
        actions={[
          <Button key={'widget-parent-profile-btn-save'} variant="raised" color="primary" onClick={this.onClickSave}>
            저장하기
          </Button>,
        ]}
      >
        {this.renderContent()}
      </Widget>
    );
  }
}

ParentProfile.propTypes = propTypes;
ParentProfile.defaultProps = defaultProps;

export default compose(
  connect(
    state => ({
      parentProfile: state.parentProfile,
    }),
    dispatch => ({
      ParentProfileActions: bindActionCreators(parentProfileActions, dispatch),
      ParentProfileLocationEditDialogActions: bindActionCreators(parentProfileLocationEditDialogActions, dispatch),
      ParentProfileActivityEditDialogActions: bindActionCreators(parentProfileActivityEditDialogActions, dispatch),
      ParentProfileScheduleEditDialogActions: bindActionCreators(parentProfileScheduleEditDialogActions, dispatch),
      ParentProfileChildAndPaymentEditDialogActions: bindActionCreators(
        parentProfileChildAndPaymentEditDialogActions,
        dispatch,
      ),
      ParentProfileOtherInfoEditDialogActions: bindActionCreators(parentProfileOtherInfoEditDialogActions, dispatch),
      DetailScheduleDialogActions: bindActionCreators(detailScheduleDialogActions, dispatch),
    }),
  ),
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(ParentProfile);
