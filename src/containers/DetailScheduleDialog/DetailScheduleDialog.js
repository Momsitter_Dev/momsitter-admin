/* eslint-disable prettier/prettier */
import uuid from 'uuid/v4';
import moment from 'moment';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { compose } from 'recompose';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import Slide from '@material-ui/core/Slide';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import { Table, TableRow, TableHeader, TableHeaderColumn, TableBody, TableRowColumn } from 'material-ui/Table';

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './DetailScheduleDialog.css';
import ScheduleTypeIdGroup from '../../util/const/ScheduleTypeIdGroup';
import * as detailScheduleDialogActions from '../../modules/detailScheduleDialog';

const propTypes = {
  open: PropTypes.bool.isRequired,
  data: PropTypes.array.isRequired,
  DetailScheduleDialogActions: PropTypes.object.isRequired,
};
const defaultProps = {};

function Transition(props) {
  return <Slide direction="up" {...props} />;
}

class DetailScheduleDialog extends React.Component {
  getScheduleDateText = scheduleDate => {
    let text = '';
    if (scheduleDate) {
      text = moment(scheduleDate).format('YYYY년 MM월 DD일');
    } else {
      text = '없음';
    }
    return text;
  };

  getScheduleTimeText = scheduleTime => {
    let text = '';
    if (scheduleTime) {
      text = moment(scheduleTime, 'HH:mm').format('HH시 mm분');
    } else {
      text = '없음';
    }
    return text;
  };

  getScheduleTypeText = scheduleType => {
    let text = '';
    if (scheduleType) {
      if (scheduleType === ScheduleTypeIdGroup.PARENT_WANTED) {
        text = '부모가 원하는 시간';
      } else if (scheduleType === ScheduleTypeIdGroup.PARENT_UNABLE) {
        text = '부모가 원하지 않는 시간';
      } else if (scheduleType === ScheduleTypeIdGroup.SITTER_UNABLE) {
        text = '시터가 불가능한 시간';
      } else if (scheduleType === ScheduleTypeIdGroup.SITTER_WANTED) {
        text = '시터가 원하는 시간';
      }
    } else {
      text = '없음';
    }
    return text;
  };

  getScheduleLocation = scheduleLocation => {
    let text = '';
    if (scheduleLocation) {
      text = scheduleLocation;
    } else {
      text = '없음';
    }
    return text;
  };

  onClickClose = () => {
    const { DetailScheduleDialogActions } = this.props;
    return DetailScheduleDialogActions.closeDialog();
  };

  renderScheduleData = data => {
    let ui = null;
    if (data && data.length > 0) {
      ui = (
        <Table allRowsSelected={false} selectable={false} multiSelectable={false}>
          <TableHeader displaySelectAll={false}>
            <TableRow>
              <TableHeaderColumn>스케줄 번호</TableHeaderColumn>
              <TableHeaderColumn>스케줄 타입</TableHeaderColumn>
              <TableHeaderColumn>날짜</TableHeaderColumn>
              <TableHeaderColumn>시간</TableHeaderColumn>
              <TableHeaderColumn>장소</TableHeaderColumn>
            </TableRow>
          </TableHeader>
          <TableBody displayRowCheckbox={false}>
            {data.map(item => {
              return (
                <TableRow key={uuid()}>
                  <TableRowColumn>{item.scheduleId}</TableRowColumn>
                  <TableRowColumn>{this.getScheduleTypeText(item.scheduleTypeId)}</TableRowColumn>
                  <TableRowColumn>{this.getScheduleDateText(item.scheduleDate)}</TableRowColumn>
                  <TableRowColumn>
                    {this.getScheduleTimeText(item.scheduleStartTime)}~{this.getScheduleTimeText(item.scheduleEndTime)}
                  </TableRowColumn>
                  <TableRowColumn>{this.getScheduleLocation(item.scheduleLocation)}</TableRowColumn>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      );
    } else {
      ui = <div> 데이터가 없습니다. </div>;
    }
    return ui;
  };

  render() {
    const { open, data } = this.props;
    return (
      <Dialog open={open} fullScreen onClose={this.onClickClose} TransitionComponent={Transition}>
        <DialogTitle> 상세 스케줄 </DialogTitle>
        <DialogContent> {this.renderScheduleData(data)} </DialogContent>
        <DialogActions>
          <Button variant={'flat'} onClick={this.onClickClose}>
            {' '}
            확인{' '}
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

DetailScheduleDialog.propTypes = propTypes;
DetailScheduleDialog.defaultProps = defaultProps;

export default compose(
  connect(
    (state) => ({
      open: state.detailScheduleDialog.open,
      data: state.detailScheduleDialog.data,
    }),
    (dispatch) => ({
      DetailScheduleDialogActions: bindActionCreators(detailScheduleDialogActions, dispatch),
    }),
  ),
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(DetailScheduleDialog);
