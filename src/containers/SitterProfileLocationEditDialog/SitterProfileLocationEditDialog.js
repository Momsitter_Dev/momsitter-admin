import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { compose } from 'recompose';
import React from 'react';
import { Dialog, DialogTitle, DialogActions, DialogContent, Grid, Button } from '@material-ui/core';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './SitterProfileLocationEditDialog.css';
import * as sitterProfileLocationEditDialogActions from '../../modules/sitterProfileLocationEditDialog';
import * as sitterProfileActions from '../../modules/sitterProfile';
import FormSitterLocation from '../../components/FormSitterLocation';

const propTypes = {
  open: PropTypes.bool.isRequired,
  sitterProfile: PropTypes.object.isRequired,
  SitterProfileActions: PropTypes.object.isRequired,
  SitterProfileLocationEditDialogActions: PropTypes.object.isRequired,
};
const defaultProps = {};

class SitterProfileLocationEditDialog extends React.Component {
  constructor(props) {
    super(props);
    this.data = this.getDefaultValue();
  }

  onChange = data => {
    this.data = data;
  };

  onClose = () => {
    const { SitterProfileLocationEditDialogActions } = this.props;
    return SitterProfileLocationEditDialogActions.closeDialog();
  };

  onSave = () => {
    const { sitterProfile, SitterProfileActions, SitterProfileLocationEditDialogActions } = this.props;

    SitterProfileActions.setSitterProfileData({
      ...sitterProfile,
      data: {
        ...sitterProfile.data,
        userLocation: [...this.data],
      },
    });

    return SitterProfileLocationEditDialogActions.closeDialog();
  };

  getDefaultValue = () => {
    const { sitterProfile } = this.props;
    const data = [];
    if (!sitterProfile) {
      return data;
    }
    if (!sitterProfile.data) {
      return data;
    }
    if (!sitterProfile.data.userLocation) {
      return data;
    }
    return sitterProfile.data.userLocation;
  };

  render() {
    const { open } = this.props;
    return (
      <div className={s.root}>
        <Dialog open={open} onClose={this.onClose}>
          <DialogTitle> 시터회원 - 활동 가능 지역 수정 </DialogTitle>
          <DialogContent style={{ width: '600px' }}>
            <Grid container spacing={16}>
              <Grid item xs={12}>
                <FormSitterLocation
                  onChange={this.onChange}
                  locations={this.getDefaultValue()}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button variant="flat" onClick={this.onClose}>
              취소
            </Button>
            <Button variant="raised" onClick={this.onSave}>
              저장
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

SitterProfileLocationEditDialog.propTypes = propTypes;
SitterProfileLocationEditDialog.defaultProps = defaultProps;

export default compose(
  connect(
    state => ({
      open: state.sitterProfileLocationEditDialog.open,
      sitterProfile: state.sitterProfile,
    }),
    dispatch => ({
      SitterProfileLocationEditDialogActions: bindActionCreators(sitterProfileLocationEditDialogActions, dispatch),
      SitterProfileActions: bindActionCreators(sitterProfileActions, dispatch),
    }),
  ),
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(SitterProfileLocationEditDialog);
