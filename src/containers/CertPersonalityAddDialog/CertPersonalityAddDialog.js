/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */
import { toast } from 'react-toastify';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import {
  withStyles as withMaterialStyle,
  Button,
  Dialog,
  DialogTitle,
  DialogActions,
  DialogContent,
  TextField,
  Typography,
  Grid,
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
  CircularProgress,
} from '@material-ui/core';
import React from 'react';

import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CertPersonalityAddDialog.css';

import * as certPersonalityAddDialogActions from '../../modules/certPersonalityAddDialog';

const propTypes = {
  open: PropTypes.bool.isRequired,
  pending: PropTypes.bool.isRequired,
  exception: PropTypes.bool.isRequired,
  exceptionMessage: PropTypes.string.isRequired,
  CertPersonalityAddDialogActions: PropTypes.object.isRequired,
};
const defaultProps = {};

class CertPersonalityAddDialog extends React.Component {
  constructor(props) {
    super(props);
    this.sequenceInput = null;
  }

  onClickSave = async () => {
    const sequenceInput = this.sequenceInput;
    if (!sequenceInput) {
      return toast.error('검사ID를 입력해주세요.');
    }
    if (!sequenceInput.value) {
      return toast.error('검사ID를 입력해주세요.');
    }
    const { CertPersonalityAddDialogActions } = this.props;
    await CertPersonalityAddDialogActions.save(sequenceInput.value);
  };

  renderDialogContent = () => {
    const { pending, exception, exceptionMessage } = this.props;
    if (pending) {
      return this.renderPending();
    }

    if (exception) {
      return this.renderException(exceptionMessage);
    }

    return (
      <Grid container spacing={16}>
        <Grid item xs={12}>
          <Typography variant={'caption'}>검사ID(숫자만)</Typography>
        </Grid>
        <Grid item xs={12}>
          <TextField
            fullWidth
            inputRef={(r) => { this.sequenceInput = r; }}
          />
        </Grid>

        <Grid item xs={12}>
          <ExpansionPanel>
            <ExpansionPanelSummary>
              <Typography variant={'body2'}>예시보기</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
              <Grid container spacing={16}>
                <Grid item xs={12}>
                  <Typography variant={'body1'}>메일에 첨부된 검사 ID를 입력해주세요.</Typography>
                </Grid>
                <Grid item xs={12}>
                  <img
                    src={'https://s3.ap-northeast-2.amazonaws.com/momsitter-dev/Screen+Shot+2018-10-01+at+5.28.04+PM.png'}
                  />
                </Grid>
              </Grid>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        </Grid>
      </Grid>
    );
  };

  renderPending = () => {
    return (
      <Grid container spacing={16} justify={'center'} alignItems={'center'} alignContent={'center'}>
        <Grid item xs={12}>
          <CircularProgress size={50} />
        </Grid>
      </Grid>
    );
  };

  renderException = (exceptionMessage) => {
    return (
      <Grid container spacing={16}>
        <Grid item xs={12}>
          <Typography variant={'caption'}>{exceptionMessage}</Typography>
        </Grid>
      </Grid>
    );
  };

  render() {
    const { open, CertPersonalityAddDialogActions } = this.props;
    return (
      <div>
        <Dialog open={open}>
          <DialogTitle>인적성검사 결과 추가</DialogTitle>
          <DialogContent>
            {this.renderDialogContent()}
          </DialogContent>
          <DialogActions>
            <Button variant={'flat'} onClick={CertPersonalityAddDialogActions.close}>취소</Button>
            <Button variant={'raised'} onClick={this.onClickSave}>저장</Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

CertPersonalityAddDialog.propTypes = propTypes;
CertPersonalityAddDialog.defaultProps = defaultProps;

export default compose(
  connect(
    (state) => ({
      open: state.certPersonalityAddDialog.open,
      pending: state.certPersonalityAddDialog.pending,
      exception: state.certPersonalityAddDialog.exception,
      exceptionMessage: state.certPersonalityAddDialog.exceptionMessage,
    }),
    (dispatch) => ({
      CertPersonalityAddDialogActions: bindActionCreators(certPersonalityAddDialogActions, dispatch),
    }),
  ),
  withStyles(s),
  withMaterialStyle(s, { withTheme: true }),
)(CertPersonalityAddDialog);
