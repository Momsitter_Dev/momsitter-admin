import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { compose } from 'recompose';
import React from 'react';
import { Dialog, DialogTitle, DialogActions, DialogContent, Grid, Button } from '@material-ui/core';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ParentProfileActivityEditDialog.css';
import * as parentProfileActivityEditDialogActions from '../../modules/parentProfileActivityEditDialog';
import * as parentProfileActions from '../../modules/parentProfile';
import FormActivity from '../../components/FormActivity';

const propTypes = {
  open: PropTypes.bool.isRequired,
  parentProfile: PropTypes.object.isRequired,
  ParentProfileActions: PropTypes.object.isRequired,
  ParentProfileActivityEditDialogActions: PropTypes.object.isRequired,
};
const defaultProps = {};

class ParentProfileActivityEditDialog extends React.Component {
  constructor(props) {
    super(props);
    this.activitys = this.getDefaultValue();
  }

  onChange = data => {
    console.log(data);
    this.activitys = data.activityIds.map(item => ({
      activityId: Number(item),
    }));
  };

  onClose = () => {
    const { ParentProfileActivityEditDialogActions } = this.props;
    return ParentProfileActivityEditDialogActions.closeDialog();
  };

  onSave = () => {
    const { parentProfile, ParentProfileActions, ParentProfileActivityEditDialogActions } = this.props;

    ParentProfileActions.setParentProfileData({
      ...parentProfile,
      data: {
        ...parentProfile.data,
        parentActivity: [...this.activitys],
      },
    });

    return ParentProfileActivityEditDialogActions.closeDialog();
  };

  getDefaultValue = () => {
    const { parentProfile } = this.props;
    if (!parentProfile) {
      return null;
    }
    if (!parentProfile.data) {
      return null;
    }
    if (!parentProfile.data.parentActivity) {
      return null;
    }
    return parentProfile.data.parentActivity.map(item => item.activityId);
  };

  render() {
    const { open } = this.props;
    return (
      <div className={s.root}>
        <Dialog open={open} onClose={this.onClose}>
          <DialogTitle>부모회원 - 원하는 활동 수정</DialogTitle>
          <DialogContent style={{ width: '600px' }}>
            <Grid container spacing={16}>
              <Grid item xs={12}>
                <FormActivity onChange={this.onChange} initialSelectedIndex={this.getDefaultValue()} />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button variant="flat" onClick={this.onClose}>
              취소
            </Button>
            <Button variant="raised" onClick={this.onSave}>
              저장
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

ParentProfileActivityEditDialog.propTypes = propTypes;
ParentProfileActivityEditDialog.defaultProps = defaultProps;

export default compose(
  connect(
    state => ({
      open: state.parentProfileActivityEditDialog.open,
      parentProfile: state.parentProfile,
    }),
    dispatch => ({
      ParentProfileActivityEditDialogActions: bindActionCreators(parentProfileActivityEditDialogActions, dispatch),
      ParentProfileActions: bindActionCreators(parentProfileActions, dispatch),
    }),
  ),
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(ParentProfileActivityEditDialog);
