/* eslint-disable prettier/prettier */

/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */
import { SheetsRegistry } from 'react-jss/lib/jss';
import JssProvider from 'react-jss/lib/JssProvider';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {
  MuiThemeProvider as NewMuiThemeProvider,
  createGenerateClassName,
} from '@material-ui/core/styles';
import Raven from 'raven';
import cors from 'cors';
import expressJwt, { UnauthorizedError as Jwt401Error } from 'express-jwt';
import compression from 'compression';
import path from 'path';
import express from 'express';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import fetch from 'node-fetch';
import React from 'react';
import ReactDOM from 'react-dom/server';
import PrettyError from 'pretty-error';
import App from './components/App';
import Html from './components/Html';
import { ErrorPageWithoutStyle } from './routes/error/ErrorPage';
import errorPageStyle from './routes/error/ErrorPage.css';
import createFetch from './createFetch';
import passport from './core/momsitter_passport';
import router from './router';
import models from './data/models';
import MomsitterAPI from './api/index';
import chunks from './chunk-manifest.json' // eslint-disable-line import/no-unresolved
import config from './config/config';
import configureStore from './store/configureStore';
import {
  legacy as MaterialTheme,
  current as NewMaterialTheme,
} from './theme';

const app = express();

Raven.config(
  config.sentry.dsn,
  {
    release: config.sentry.release,
    environment: config.sentry.environment,
    captureUnhandledRejections: true,
  },
).install();

const shouldCompress = (req, res) => {
  if (req.headers['x-no-compression']) {
    // don't compress responses with this request header
    return false;
  }
  // fallback to standard filter function
  return compression.filter(req, res);
};
//
// Tell any CSS tooling (such as Material UI) to use all vendor prefixes if the
// user agent is not known.
// -----------------------------------------------------------------------------
global.navigator = global.navigator || {};
global.navigator.userAgent = global.navigator.userAgent || 'all';

// Compress all responses
app.use(compression({ filter: shouldCompress }));

//
// Register Node.js middleware
// -----------------------------------------------------------------------------
app.use(express.static(path.resolve(__dirname, 'public')));
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true, limit: 1024 * 1024 * 50, type: 'application/x-www-form-urlencoded' }));
app.use(bodyParser.json({ limit: 1024 * 1024 * 50, type: 'application/json' }));
app.disable('x-powered-by');

app.use(
  expressJwt({
    secret: 'momsitter-admin',
    credentialsRequired: false,
    getToken: req => req.cookies.id_token,
  }),
);
// Error handler for express-jwt
app.use((err, req, res, next) => {
  // eslint-disable-line no-unused-vars
  if (err instanceof Jwt401Error) {
    console.error('[express-jwt-error]', req.cookies.id_token);
    // `clearCookie`, otherwise user can't use web-app until cookie expires
    res.clearCookie('id_token');
  }
  next(err);
});

app.use(passport.initialize());
app.use(cors());

if (__DEV__) {
  app.enable('trust proxy');
}

app.use(MomsitterAPI);

// Raven Error logging middle ware
app.use(Raven.errorHandler());

//
// Register server-side rendering middleware
// -----------------------------------------------------------------------------
app.get('*', async (req, res, next) => {
  try {
    const css = new Set();

    // Create a new class name generator.
    const generateClassName = createGenerateClassName();

    // Create a sheetsManager instance.
    const sheetsManager = new Map();

    // Create a sheetsRegistry instance.
    const sheetsRegistry = new SheetsRegistry();

    // Redux store
    const initialState = {
      counter: 0,
      user: req.user || null,
    };

    const store = configureStore(initialState);


    const insertCss = (...styles) => {
      // eslint-disable-next-line no-underscore-dangle
      styles.forEach(style => css.add(style._getCss()));
    };

    // Global (context) variables that can be easily accessed from any React component
    // https://facebook.github.io/react/docs/context.html
    const context = {
      // Enables critical path CSS rendering
      // https://github.com/kriasoft/isomorphic-style-loader
      insertCss,
      // Universal HTTP client
      fetch: createFetch(fetch, {
        baseUrl: config.site.domain,
        cookie: req.headers.cookie,
      }),
      // Redux
      store,
      storeSubscription: null, // ?
      // router
      pathname: req.path,
      query: req.query,
    };

    const route = await router.resolve(context);

    if (route.redirect) {
      res.redirect(route.status || 302, route.redirect);
      return;
    }

    const data = { ...route };

    data.children = await ReactDOM.renderToString(
      <JssProvider registry={sheetsRegistry} generateClassName={generateClassName}>
        <NewMuiThemeProvider theme={NewMaterialTheme} sheetsManager={sheetsManager}>
          <MuiThemeProvider muiTheme={MaterialTheme}>
            <App context={context}>{route.component}</App>
          </MuiThemeProvider>
        </NewMuiThemeProvider>
      </JssProvider>
    );
    data.styles = [{ id: 'css', cssText: [...css].join('') }];

    const scripts = new Set();
    const addChunk = chunk => {
      if (chunks[chunk]) {
        chunks[chunk].forEach(asset => scripts.add(asset));
      } else if (__DEV__) {
        throw new Error(`Chunk with name '${chunk}' cannot be found`);
      }
    };
    addChunk('client');
    if (route.chunk) addChunk(route.chunk);
    if (route.chunks) route.chunks.forEach(addChunk);

    data.scripts = Array.from(scripts);
    data.app = {
      apiUrl: config.site.domain,
      state: context.store.getState(),
    };
    data.css = sheetsRegistry.toString();

    // NOTE: client side config
    data.config = {
      domain: config.site.domain,
      port: config.site.port,
      apiServer: config.apiServer,
      addressServer: config.addressServer,
      appServer: config.appServer,
    };

    const html = ReactDOM.renderToStaticMarkup(<Html {...data} />);
    res.status(route.status || 200);
    res.send(`<!doctype html>${html}`);
  } catch (err) {
    next(err);
  }
});

//
// Error handling
// -----------------------------------------------------------------------------
const pe = new PrettyError();
pe.skipNodeFiles();
pe.skipPackage('express');

// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
  console.error(pe.render(err));
  const html = ReactDOM.renderToStaticMarkup(
    <Html
      title="Internal Server Error"
      description={err.message}
      styles={[{ id: 'css', cssText: errorPageStyle._getCss() }]} // eslint-disable-line no-underscore-dangle
    >
      {ReactDOM.renderToString(<ErrorPageWithoutStyle error={err} />)}
    </Html>,
  );
  res.status(err.status || 500);
  res.send(`<!doctype html>${html}`);
});

//
// Launch the server
// -----------------------------------------------------------------------------
const promise = models.sync().catch(err => console.error(err.stack));
if (!module.hot) {
  promise.then(() => {
    app.listen(config.site.port, () => {
      console.info(`The server is running at http://localhost:${config.site.port}/`);
    });
  });
}
//
// Hot Module Replacement
// -----------------------------------------------------------------------------
if (module.hot) {
  app.hot = module.hot;
  module.hot.accept('./router');
}

export default app;
