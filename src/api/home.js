/* eslint-disable prettier/prettier */
import uuid from 'uuid/v4';
import moment from 'moment';
import request from 'request-promise';
import Debug from 'debug';
import express from 'express';
import jwt from 'jsonwebtoken';
import passport from '../core/momsitter_passport';
import AWS from '../core/momsitter_aws';
import {
  User,
  Admin,
  UploadFiles,
  UserProfileImage,
  FaceAnalysis,
  Apply,
  CertificateLabel,
  Coupon, LinkToken,
  LogGeneral, LogMessage, ParentActivity, ParentChildInfo, ParentProfile,
  Payment,
  Point,
  Report, Review,
  SearchIndexParent, SearchIndexSitter,
  SitterActivity, SitterExperience, SitterPreferChildInfo,
  SitterProfile, TimerTaskTarget, UserApplyCount,
  UserCertificate, UserLocation,
  UserOutRequest, UserProduct, UserRole, UserSchedule, UserSnapshot
} from '../data/models/index';
import sequelize from '../data/sequelize';
import ResponseResultTypeGroup from '../util/const/ResponseResultTypeGroup';
import isParent from '../util/user/isParent';
import isSitter from '../util/user/isSitter';
import LocationTypeGroup from '../util/const/LocationTypeGroup';
import ScheduleTypeIdGroup from '../util/const/ScheduleTypeIdGroup';
import ProfileImageTypeGroup from '../util/const/ProfileImageTypeGroup';
import SyncSearchIndex from '../util/SyncSearchIndex';

moment.locale('ko');

const debug = Debug('api');
const app = express.Router();
const S3 = new AWS.S3();

app.post('/account/test', async (req, res) => {
  try {
    const { userKey, userTypeId } = req.body;

    if (!userKey) {
      throw new Error('invalid userKey');
    }

    if (!userTypeId) {
      throw new Error('invalid userTypeId');
    }

    const parent = isParent(userTypeId);
    const sitter = isSitter(userTypeId);

    if (!parent && !sitter) {
      throw new Error('invalid userTypeId');
    }

    // NOTE: userKey is already exist check

    const preExistUser = await User.findOne({
      where: { userKey, },
    });

    if (preExistUser) {
      throw new Error(`${userKey} is already exist`);
    }

    let createdUserId = null;

    await sequelize.transaction(async (transaction) => {
      const { userId } = await User.create({
        userKey,
        userName: '테스터',
        userPassword: sequelize.fn('PASSWORD', '1'),
        userBirthday: '',
        userAge: 27,
        userConnectType: 'email',
        userPhone: '01028505316',
        userGender: 'man',
        userEmail: 'seungjaey@gmail.com',
        userDup: uuid(),
        userNationality: '',
        userAssignmentTos: true,
        userAssignmentPp: true,
        userAssignmentMsg: true,
        userStatus: 'active',
        userSearchable: true,
        contactWay: 'none',
        contactWayDetail: 'none',
        lastLoginDate: new Date(),
        joinType: null,
      }, { transaction });
      debug(userId);
      createdUserId = userId;
      await UserRole.create({
        userId,
        userTypeId,
      }, { transaction });
      await UserLocation.create({
        userId,
        locationType: (parent) ? LocationTypeGroup.PARENT_CARE : LocationTypeGroup.SITTER_HOME,
        jibunAddr: '서울특별시 관악구 신사동',
        roadAddr: '',
        locationLatitude: 37.4856579,
        locationLongitude: 126.91486350000002,
        siNm: '서울특별시',
        sggNm: '관악구',
        emdNm: '신사동',
      }, { transaction });

      const today = moment();

      await UserSchedule.create({
        userId,
        scheduleTypeId: (parent) ? ScheduleTypeIdGroup.PARENT_WANTED : ScheduleTypeIdGroup.SITTER_WANTED,
        scheduleDate: today.format('YYYY-MM-DD 00:00:00'),
        scheduleStartTime: '15:00',
        scheduleEndTime: '21:00',
      }, { transaction });

      await UserProfileImage.create({
        userId,
        fileId: (parent) ? 5 : 50,
        profileImageType: (parent) ? ProfileImageTypeGroup.PARENT_PROFILE_MAIN : ProfileImageTypeGroup.SITTER_PROFILE_MAIN,
        regDate: new Date(),
      }, { transaction });

      await UserProduct.create({
        userId,
        productId: (parent) ? 2 : 11,
        purchaseDate: new Date(),
        expireDate: (parent) ? moment().add(1, 'months').format('YYYY-MM-DD HH:mm:ss') : moment().add(7, 'days').format('YYYY-MM-DD HH:mm:ss'),
        memo: '테스트 계정 생성',
        productStatus: 'active',
      }, { transaction });

      await UserApplyCount.create({
        userId,
        applyToParentCount: (parent) ? 0 : 6,
        applyToSitterCount: (parent) ? 5 : 0,
        lastUpdated: new Date(),
      }, { transaction });

      if (parent) {
        await ParentProfile.create({
          userId,
          viewCount: 0,
          createDate: new Date(),
          updateDate: new Date(),
          profileStatus: 'active',
          profileTitle: '테스트 제목',
          profileDescription: '테스트 설명',
          wantedPayment: 10000,
          wantedInterviewWay: 'phone',
          wantedSitterGender: 'women',
          wantedCareWay: 'alone',
          longTerm: false,
          paymentNegotiable: true,
          durationNegotiable: true,
          weekdayNegotiable: true,
          timeNegotiable: true,
          moreThanThreeMonth: false,
          childCount: 1,
          preferMinAge: 20,
          preferMaxAge: 40,
        }, { transaction });

        await ParentActivity.create({
          userId,
          activityId: 1,
        }, { transaction });

        await ParentChildInfo.create({
          userId,
          childAgeId: 1,
          gender: 'man',
          childBirthDay: moment('2010-03-25').format('YYYY-MM-DD 00:00:00'),
        }, { transaction });
      }

      if (sitter) {
        await SitterProfile.create({
          userId,
          viewCount: 0,
          createDate: new Date(),
          updateDate: new Date(),
          profileStatus: 'active',
          profileTitle: '테스트 제목',
          profileDescription: '테스트 설명',
          wantedPayment: 10000,
          searchable: true,
          cctv: true,
        }, { transaction });

        await SitterActivity.create({
          userId,
          activityId: 1,
        }, { transaction });

        await SitterPreferChildInfo.create({
          userId,
          childAgeId: 1,
        }, { transaction });
      }
    });

    if (!createdUserId) {
      throw new Error('userId is null');
    }

    await SyncSearchIndex(createdUserId, userTypeId);
    return res.json({ result: ResponseResultTypeGroup.SUCCESS });
  } catch (err) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: err.message
      },
    });
  }
});

// 얼굴 사전 감지 API
app.post('/face', async (req, res) => {
  try {
    debug("face API START");
    const { userId } = req.body;
    res.json({
      result: 'success',
    });

    debug('face userId');
    debug(userId);
    if (userId) {
      const userImages = await UserProfileImage.findAndCountAll({
        where: { userId },
        include: [{ model: UploadFiles, as: 'profileFile' }],
      }).catch((err) => {
        debug(err);
        debug('Error in Database');
        return false;
      });

      const previous = await FaceAnalysis.destroy({
        where: { userId },
      }).catch((err) => {
        debug(err);
        return false;
      });

      if (userImages && userImages.rows.length > 0) {
        const faceAnalysisRequest = await Promise.all(userImages.rows.map((item) => {
          return request({
            method: 'POST',
            uri:
              'https://southeastasia.api.cognitive.microsoft.com/face/v1.0/detect?returnFaceId=true&returnFaceLandmarks=false&returnFaceAttributes=age,gender',
            body: JSON.stringify({
              url: item.profileFile.fileUrl,
            }),
            headers: {
              'content-type': 'application/json',
              'Ocp-Apim-Subscription-Key': 'e7f63977ed334b31a4cb72a8aa14b0f5',
            },
          }).then((response) => {
            return response;
          }).catch((err) => {
            debug(err);
            return false;
          });
        }));

        debug("FACE RESULT");
        debug(faceAnalysisRequest);
        const insertResult = await FaceAnalysis.bulkCreate(userImages.rows.map((item, index) => {
          return {
            userId,
            fileId: item.fileId,
            analysisResult: faceAnalysisRequest[index],
            resultSummary: (faceAnalysisRequest[index] && faceAnalysisRequest[index].length > 10) ? true : false,
          };
        })).then(() => {
          return true;
        }).catch(() => {
          return false;
        });

        debug('insert result');
        debug(insertResult);
      } else {
        // TODO: Handle Error
        debug('ERROR FILE IS NOT EXIST[1]');
      }
    } else {
      debug('ERROR User Id is Null');
    }
  } catch (e) {
    debug(e);
  }
});

app.post('/login', async (req, res) => {
  debug('login');
  const {
    userId,
    userPw,
  } = req.body;

  const result = await Admin.findOne({
    where: {
      adminKey: userId,
      adminPassword: userPw,
    },
  }).catch((err) => {
    debug(err);
    return false;
  });

  debug('check login');
  debug(result);
  if (result) {
    const token = jwt.sign({
      adminId: result.adminId,
      adminKey: result.adminKey,
      adminName: result.adminName,
    }, 'momsitter-admin', { algorithm: 'HS256' });
    debug(token);
    res.json({
      result: 'success',
      token,
    });
  } else {
    res.json({ result: 'fail' });
  }
});

app.get('/s3', async (req, res) => {
  const params = {
    Bucket: 'momsitter-user-files-dev',
    Key: 'files/6bbc9b8d99e8355dfd70c869b957640a.png',
  };
  // Key: "files/eb60a46d7f2b065657db58f8aa67c6bc.png"
  debug(params);
  S3.getObject(params, (err, data) => {
    if (err) {
      debug(err, err.stack);
      res.json({
        result: 'fail',
        data: err,
      });
    } else {
      debug(data);
      res.json({
        result: 'success',
        data,
      });
    }
  });
});

app.delete('/destroy/:userId', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const { userId } = req.params;
    if (!userId) {
      throw new Error('invalid userId');
    }
    await UploadFiles.update(
      {
        userId: null,
      },
      {
        where: { userId }
      },
    );
    await UserProfileImage.destroy( {
      where: { userId },
    });
    await Review.destroy({
      where: {
        [sequelize.Op.or]: {
          senderId: userId,
          targetId: userId,
        },
      },
    });
    await Apply.destroy({
      where: {
        [sequelize.Op.or]: {
          applicantId: userId,
          receiverId: userId,
        },
      },
    });
    await User.destroy({
      where: { userId },
    });
    return res.json({ result: ResponseResultTypeGroup.SUCCESS });
  } catch (err) {
    debug(err);
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: err.message,
      },
    });
  }
});

export default app;
