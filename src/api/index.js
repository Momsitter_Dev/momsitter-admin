import express from 'express';
import home from './home';
import manageCoupon from './manage/coupon';
import manageProducts from './manage/products';
import manageUser from './manage/user';
import manageUserReviews from './manage/user/reviews';
import manageUserPayments from './manage/user/payments';
import manageUserApplications from './manage/user/applications';
import manageUserCertifications from './manage/user/certifications';
import manageUserProducts from './manage/user/products';
import manageUserCoupons from './manage/user/coupons';
import manageUserMemos from './manage/user/memos';
import manageUserReports from './manage/user/reports';
import manageFace from './manage/face';
import manageUserOutRequest from './manage/user/outRequest';
import users from './user';
import usersBasicInfo from './users/basicInfo';
import file from './file';
import location from './location';
import userProfile from './users/profile';
import manageFeedback from './manage/feedback';
import statistics from './statistics';
import sitterApplications from './manage/sitter/applications';
import parentApplications from './manage/parent/applications';
import coupon from './coupon';
import points from './points';
import boards from './boards';
import passport from '../core/momsitter_passport';
import messages from './messages';

import getUserCustomLog from './route/customLog/getUserCustomLog';

const app = express.Router();

app.use('/api', home);
app.use('/api/file', file);
app.use('/api/location', location);
app.use('/api/manage/coupons', manageCoupon);
app.use('/api/manage/products', manageProducts);
app.use('/api/manage/face', manageFace);
app.use('/api/manage/feedback', manageFeedback);
app.use('/api/statistics', statistics);

app.use('/api/manage/user/reviews', manageUserReviews);
app.use('/api/manage/user/payments', manageUserPayments);
app.use('/api/manage/user/applications', manageUserApplications);
app.use('/api/manage/user/certifications', manageUserCertifications);
app.use('/api/manage/user/coupons', manageUserCoupons);
app.use('/api/manage/user/products', manageUserProducts);
app.use('/api/manage/user/reports', manageUserReports);
app.use('/api/manage/user/memos', manageUserMemos);
app.use('/api/manage/user', manageUser);
app.use('/api/manage/user/outRequest', manageUserOutRequest);

app.use('/api/users', users);

// TODO Wrap up with /api/users/:userId
app.use('/api/users/', usersBasicInfo);
app.use('/api/users/', userProfile);

app.use('/api/manage/sitter/applications', sitterApplications);
app.use('/api/manage/parent/applications', parentApplications);

app.use('/api/coupon', coupon);
app.use('/api/points', points);
app.use('/api/boards', boards);
app.use('/api/messages', messages);

app.get(
  '/api/log/custom/:userId',
  passport.authenticate('jwt', { session: false }),
  getUserCustomLog,
);

export default app;
