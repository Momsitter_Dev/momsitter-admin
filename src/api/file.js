/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 *
 * 파일에 관련된 API 를 제공하는 컨트롤러
 */
import express from 'express';
import uuidV4 from 'uuid/v4';
import multer from 'multer';
import Debug from 'debug';
import fs from 'fs';
import sharp from 'sharp';
import { UploadFiles } from '../data/models/index';
import mimeTypeToExtension from '../util/file/mimeTypeToExtension';
import fileS3Upload from '../util/file/fileS3Upload';

const debug = Debug('api');
const app = express.Router();
const upload = multer({ dest: 'tmp/' });

app.post('/', upload.single('file'), async (req, res) => {
  const { userId } = req.body;
  const extension = mimeTypeToExtension(req.file.mimetype);

  if (extension) {
    const fileStream = fs.createReadStream(req.file.path).pipe(sharp().rotate());
    const fileUuid = uuidV4();
    const fileAcl = 'public-read'; // TODO: util/constant 로 분리

    let uploadResult;
    try {
      uploadResult = await fileS3Upload(fileUuid, extension, fileStream, fileAcl).catch(() => { throw new Error('Cannot Upload File') });
    } catch (err) {
      debug(err);
      res.json({
        result: 'fail',
        err: {
          message: 'S3 Upload Fail',
        },
      });
    }

    if (uploadResult) {
      let insertResult;
      try {
        insertResult = await UploadFiles.create({
          userId: userId,
          uploadDate: new Date(),
          fileUuid: fileUuid,
          fileUrl: uploadResult.Location,
          fileName: req.file.originalname,
          fileSize: req.file.size,
          mimeType: req.file.mimetype,
          auth: fileAcl,
        });
        res.json({
          result: 'success',
          data: insertResult,
        });
      } catch (err) {
        debug(err);
        res.json({
          result: 'fail',
          err: {
            message: 'file Insert fail',
          },
        });
      }
    }

    fileStream.close();
    fs.unlink(req.file.path, () => {});
  } else {
    res.json({
      result: 'fail',
      err: {
        message: 'Invalid mime type',
      },
    });
  }
});

export default app;
