/* eslint-disable prettier/prettier */
import moment from 'moment';
import express from 'express';
import Debug from 'debug';
import { Point, PointType } from '../data/models/index';
import passport from '../core/momsitter_passport';
import ResponseResultTypeGroup from '../util/const/ResponseResultTypeGroup';
import sequelize from '../data/sequelize';
import PointList from '../data/query/PointList';
import PointListCount from '../data/query/PointListCount';
import parseSearchOptionRawQuery from '../util/dataFormat/parseSearchOptionRawQuery';
import parseOrderOptionRawQuery from '../util/dataFormat/parseOrderOptionRawQuery';
import parseLimitOptionRawQuery from '../util/dataFormat/parseLimitOptionRawQuery';
import ExportCSV from '../util/ExportCSV';

moment.locale('ko');

const debug = Debug('api');
const app = express.Router();

app.get('/types', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    return res.json({
      result: ResponseResultTypeGroup.SUCCESS,
      data: await PointType.findAll(),
    });
  } catch (err) {
    debug(err);
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
    });
  }
});

app.get('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const {
      requestPage,
      searchOptions,
      orderOption,
    } = req.query;
    const page = { maxPage: 0, currentPage: 0 };

    let offset = 0;
    if (requestPage && requestPage > 0) {
      offset = requestPage * 10;
    } else {
      offset = 0;
    }

    const where = parseSearchOptionRawQuery(searchOptions);
    const countQuery = PointListCount(where);
    const count = await sequelize.query(countQuery, { type: sequelize.QueryTypes.SELECT }).then((data) => {
      return data[0]['CNT'];
    });

    if (count === 0) {
      res.json({
        result: 'success',
        data: [],
        page,
      });
    }

    const order = parseOrderOptionRawQuery(orderOption);
    const limit = parseLimitOptionRawQuery(offset);
    const query = PointList(where, order, limit);
    const points = await sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
    page.maxPage = Math.ceil(count / 10);
    page.currentPage = Number(requestPage) || 0;
    res.json({
      result: ResponseResultTypeGroup.SUCCESS,
      data: points,
      page,
    });

  } catch (err) {
    debug(err);
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
    });
  }
});

app.get('/csv', async (req, res) => {
  try {
    const {
      searchOptions,
    } = req.query;
    const where = parseSearchOptionRawQuery(searchOptions);
    const query = PointList(where, '', '');
    const points = await sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
    const fields = [
      'userId',
      'userName',
      'userTypeId',
      'pointId',
      'pointTypeId',
      'pointTypeName',
      'pointPrice',
      'totalPoint',
      'remainPoint',
      'pubDate',
      'expireDate',
      'issueReason',
      'removed',
    ];

    const fieldNames = [
      '회원 번호',
      '회원 이름',
      '회원 타입',
      '적립금 번호',
      '적립금 타입 번호',
      '적립금 타입명',
      '적립금 타입 지급 금액',
      '초기 적립금',
      '잔여 적립금',
      '발급 일자',
      '만료 일자',
      '삭제 사유',
      '삭제 여부',
    ];

    const exportResult = await ExportCSV({
      fields,
      fieldNames,
      data: points,
    });

    if (!exportResult) {
      throw new Error('csv export fail');
    }
    const current = moment(new Date())
      .locale('ko')
      .format('YYYYMMDDhmm');
    res.header('Content-Disposition', `attachment; filename=${current}.csv`);
    res.type('text/csv');
    res.send(200, exportResult);
  } catch (err) {
    debug(err);
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
    });
  }
});

app.delete('/:pointId', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const { pointId } = req.params;
    const { removeReason } = req.body;

    await Point.update({
      removed: true,
      removeReason,
      removedDate: new Date(),
    }, {
      where: {
        pointId,
      }
    });

    return res.json({
      result: ResponseResultTypeGroup.SUCCESS,
    });

  } catch (err) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: err.message
      },
    });
  }
});

app.put('/:pointId', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const { pointId } = req.params;
    const { removeReason } = req.body;

    await Point.update({
      removed: true,
      removeReason,
    }, {
      where: {
        pointId,
      },
    });

    return res.json({
      result: ResponseResultTypeGroup.SUCCESS,
    });

  } catch (err) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: err.message
      },
    });
  }
});

app.post('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const {
      issueReason,
      pointPrice,
      userId,
    } = req.body;

    if (!userId) {
      throw new Error('invalid userId');
    }

    await Point.create({
      userId,
      pointTypeId: 8,
      totalPoint: Number(pointPrice),
      remainPoint: Number(pointPrice),
      pubDate: new Date(),
      expireDate: moment().add(2, 'year').endOf('month').format('YYYY-MM-DD HH:mm:ss'),
      read: false,
      removed: false,
      issueReason,
    });

    return res.json({
      result: ResponseResultTypeGroup.SUCCESS,
    });

  } catch (err) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: err.message
      },
    });
  }
});

export default app;
