import { CustomLog } from '../../../data/appLog/models/index';
import ResponseResultTypeGroup from '../../../util/const/ResponseResultTypeGroup';
import UserAgentPlatformGroup from '../../../util/const/UserAgentPlatformGroup';

export default async function (req, res) {
  try {
    const { userId } = req.params;
    if (!userId) {
      throw new Error('invalid UserId');
    }

    const logs = await Promise.all(
      Object.keys(UserAgentPlatformGroup).map((key) => {
        return CustomLog.findAll({
          where: {
            userId,
            platform: UserAgentPlatformGroup[key],
          },
          group: ['userAgent'],
          order: ['logDate'],
        });
      }),
    ).then((data) => {
      return {
        bot: data[0],
        desktop: data[1],
        phone: data[2],
        tablet: data[3],
        tv: data[4],
      };
    });

    return res.json({
      result: ResponseResultTypeGroup.SUCCESS,
      data: logs,
    });
  } catch (err) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: err.message,
      },
    });
  }
}
