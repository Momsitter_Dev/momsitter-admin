/* eslint-disable prettier/prettier */
import express from 'express';
import Debug from 'debug';
import {
  Apply,
  Admin,
  AdminMemo,
  User,
  UserRole,
  UserType,
  UserProduct,
  UserApplyCount,
  UserBookmark,
  Review,
  Payment,
  Product,
  Report,
  UploadFiles,
  UserCertificate,
  UserPersonalityResult,
  Coupon,
  CouponType,
  CouponCode,
  Point,
  PointType,
  LogUsingPoint,
  UserProfileImage,
  LogMessage,
} from '../data/models/index';
import passport from '../core/momsitter_passport';
import ResponseResultTypeGroup from '../util/const/ResponseResultTypeGroup';
import sequelize from '../data/sequelize';
import MessageListQuery from '../data/query/MessageList';
import CountMessageListQuery from '../data/query/CountMessageList';
import parseSearchOptionRawQuery from '../util/dataFormat/parseSearchOptionRawQuery';
import parseLimitOptionRawQuery from '../util/dataFormat/parseLimitOptionRawQuery';
import CountSitterList from '../data/query/CountSitterList';
import SitterList from '../data/query/SitterList';
import parseOrderOptionRawQuery from '../util/dataFormat/parseOrderOptionRawQuery';
import ReviewTypeGroup from '../util/const/ReviewTypeGroup';

const debug = Debug('api');
const app = express.Router();

app.get('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const { requestPage, searchCategory, searchText, subSearchText, subSearchCategory } = req.query;

  const searchCondition = {
    order: [],
    where: {},
  };
  const page = { maxPage: 0, currentPage: 0 };

  let offset = 0;
  if (requestPage && requestPage > 0) {
    offset = requestPage * 10;
  } else {
    offset = 0;
  }

  searchCondition.main = {};
  searchCondition.sub = null;

  if (searchCategory && searchText) {
    searchCondition.main[searchCategory] = searchText;
  } else {
    searchCondition.main['userId'] = {
      $like: ['%', '%'].join(''),
    };
  }

  if (subSearchCategory && subSearchText) {
    searchCondition.sub = { $and: {} };
    searchCondition.sub['$and'][subSearchCategory] = {
      $like: ['%' + subSearchText + '%'].join(''),
    };
  }

  const users = await User.findAndCountAll({
    offset,
    limit: 10,
    where: {
      ...searchCondition.main,
      ...searchCondition.sub,
    },
    include: [
      {
        model: UserRole,
        as: 'userRole',
        include: [{ model: UserType, as: 'userType' }],
      },
      {
        model: UserProfileImage,
        as: 'userProfileImage',
        include: [{ model: UploadFiles, as: 'profileFile' }],
        where: {
          profileImageType: {
            $in: ['parentProfileMain', 'sitterProfileMain'],
          },
        },
        required: false,
      },
    ],
  }).catch(err => {
    console.error(err);
    return false;
  });

  if (users) {
    page.maxPage = Math.ceil(users.count / 10);
    page.currentPage = Number(requestPage) || 0;

    res.json({
      result: 'success',
      data: users,
      page,
    });
  } else {
    res.json({
      result: 'fail',
      message: 'fetching error',
    });
  }
});

app.get('/:userId/personality', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const userId = req.params.userId;
  if (!userId) {
    return res.status(200).json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: '인적성검사 결과를 조회할 수 없습니다.',
      },
    });
  }
  try {
    const result = await UserPersonalityResult.findOne({
      where: {
        $or: [{ userId }, { name: userId }],
      },
    });
    if (!result) {
      throw new Error('인성검사 데이터가 없음');
    }
    return res.status(200).json({
      result: ResponseResultTypeGroup.SUCCESS,
      data: result,
    });
  } catch (err) {
    return res.status(200).json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: '인적성검사 결과를 조회할 수 없습니다.',
      },
    });
  }
});

app.get('/:userId/reviews', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const { userId } = req.params;
    if (!userId) {
      throw new Error('invalid userId');
    }
    const sendInfo = await Review.findAll({
      order: [['writeDate', 'ASC']],
      where: {
        senderId: userId,
        reviewType: {
          [sequelize.Op.ne]: ReviewTypeGroup.UNFINISHED,
        },
      },
      include: [
        {
          model: User,
          as: 'target',
          include: [
            {
              model: UserProfileImage,
              as: 'userProfileImage',
              where: { profileImageType: { $like: '%Main' } },
              include: [{ model: UploadFiles, as: 'profileFile' }],
            },
          ],
        },
      ],
    });
    const receiveInfo = await Review.findAll({
      where: {
        targetId: userId,
        reviewType: {
          [sequelize.Op.ne]: ReviewTypeGroup.UNFINISHED,
        },
      },
      order: [['writeDate', 'ASC']],
      include: [
        {
          model: User,
          as: 'sender',
          include: [
            {
              model: UserProfileImage,
              as: 'userProfileImage',
              where: { profileImageType: { $like: '%Main' } },
              include: [{ model: UploadFiles, as: 'profileFile' }],
            },
          ],
        },
      ],
    });
    return res.json({
      result: ResponseResultTypeGroup.SUCCESS,
      data: {
        send: sendInfo,
        receive: receiveInfo,
      },
    });
  } catch (err) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: err.message,
      },
    });
  }
});

app.get('/:userId/applications', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const { userId } = req.params;
    if (!userId) {
      throw new Error('invalid userId');
    }
    const sendApplications = await Apply.findAll({
      order: [['applyDate', 'ASC']],
      where: { applicantId: userId },
      include: [
        {
          model: User,
          as: 'receiver',
          include: [
            {
              model: UserProfileImage,
              as: 'userProfileImage',
              where: { profileImageType: { $like: '%Main' } },
              include: [{ model: UploadFiles, as: 'profileFile' }],
            },
          ],
        },
      ],
    });
    const receiveApplications = await Apply.findAll({
      order: [['applyDate', 'ASC']],
      where: { receiverId: userId },
      include: [
        {
          model: User,
          as: 'applicant',
          include: [
            {
              model: UserProfileImage,
              as: 'userProfileImage',
              where: { profileImageType: { $like: '%Main' } },
              include: [{ model: UploadFiles, as: 'profileFile' }],
            },
          ],
        },
      ],
    });
    return res.json({
      result: 'success',
      data: {
        send: sendApplications,
        receive: receiveApplications,
      },
    });
  } catch (err) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: err.message,
      },
    });
  }
});

app.get('/:userId/payments', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const { userId } = req.params;
    if (!userId) {
      throw new Error('invalid userId');
    }
    const payments = await Payment.findAll({
      order: [['paymentRequestDate', 'ASC']],
      where: { userId },
      include: [
        {
          model: Coupon,
          as: 'paymentCoupon',
          include: [
            {
              model: CouponType,
              as: 'couponType',
            },
          ],
        },
        {
          model: Product,
          as: 'paymentProduct',
        },
      ],
    });

    return res.json({
      result: ResponseResultTypeGroup.SUCCESS,
      data: payments,
    });
  } catch (err) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: err.message,
      },
    });
  }
});

app.get('/:userId/products', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const { userId } = req.params;
    if (!userId) {
      throw new Error('invalid userId');
    }
    const result = await UserProduct.findAll({
      order: [['purchaseDate', 'ASC']],
      where: { userId },
      include: [
        {
          model: User,
          as: 'productUser',
          include: [
            {
              model: UserRole,
              as: 'userRole',
              include: { model: UserType, as: 'userType' },
            },
            {
              model: UserApplyCount,
              as: 'userApplyCount',
            },
          ],
        },
        { model: Product, as: 'product' },
        {
          model: Payment,
          as: 'userProductPayment',
          include: [
            {
              model: Coupon,
              as: 'paymentCoupon',
              include: [
                {
                  model: CouponType,
                  as: 'couponType',
                },
              ],
            },
          ],
        },
      ],
    });
    return res.json({
      result: ResponseResultTypeGroup.SUCCESS,
      data: result,
    });
  } catch (err) {
    debug(err);
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: 'server error',
      },
    });
  }
});

app.get('/:userId/reports', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const { userId } = req.params;
    if (!userId) {
      throw new Error('invalid userId');
    }
    const send = await Report.findAll({
      order: [['reportDate', 'ASC']],
      where: { reporterId: userId },
      include: [
        {
          model: User,
          as: 'reportTargetUser',
          include: [
            {
              model: UserProfileImage,
              as: 'userProfileImage',
              where: { profileImageType: { $like: '%Main' } },
              include: [{ model: UploadFiles, as: 'profileFile' }],
            },
          ],
        },
      ],
    });
    const receive = await Report.findAll({
      where: { targetId: userId },
      order: [['reportDate', 'ASC']],
      include: [
        {
          model: User,
          as: 'reportUser',
          include: [
            {
              model: UserProfileImage,
              as: 'userProfileImage',
              where: { profileImageType: { $like: '%Main' } },
              include: [{ model: UploadFiles, as: 'profileFile' }],
            },
          ],
        },
      ],
    });

    return res.json({
      result: ResponseResultTypeGroup.SUCCESS,
      data: { send, receive },
    });
  } catch (err) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: err.message,
      },
    });
  }
});

app.get('/:userId/bookmarks', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const { userId } = req.params;
    if (!userId) {
      throw new Error('invalid userId');
    }
    const send = await UserBookmark.findAll({
      order: [['regDate', 'ASC']],
      where: { subjectId: userId },
      include: [
        {
          model: User,
          as: 'target',
          include: [
            {
              model: UserProfileImage,
              as: 'userProfileImage',
              where: { profileImageType: { $like: '%Main' } },
              include: [{ model: UploadFiles, as: 'profileFile' }],
            },
          ],
        },
      ],
    });
    const receive = await UserBookmark.findAll({
      order: [['regDate', 'ASC']],
      where: { targetUserId: userId },
      include: [
        {
          model: User,
          as: 'subject',
          include: [
            {
              model: UserProfileImage,
              as: 'userProfileImage',
              where: { profileImageType: { $like: '%Main' } },
              include: [{ model: UploadFiles, as: 'profileFile' }],
            },
          ],
        },
      ],
    });
    return res.json({
      result: ResponseResultTypeGroup.SUCCESS,
      data: { send, receive },
    });
  } catch (err) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: err.message,
      },
    });
  }
});

app.get('/:userId/certifications', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const { userId } = req.params;
    if (!userId) {
      throw new Error('invalid userId');
    }
    const result = await UserCertificate.findAll({
      order: [['regDate', 'ASC']],
      where: { userId },
      include: [
        {
          model: User,
          as: 'certUser',
          include: [{ model: UserRole, as: 'userRole' }],
        },
        { model: UploadFiles, as: 'certFile' },
      ],
    });

    return res.json({
      result: ResponseResultTypeGroup.SUCCESS,
      data: result,
    });
  } catch (err) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: err.message,
      },
    });
  }
});

app.get('/:userId/memos', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const { userId } = req.params;
    if (!userId) {
      throw new Error('userId');
    }
    const result = await AdminMemo.findAll({
      order: [['memoRegDate', 'ASC']],
      where: { userId },
      include: [{ model: Admin, as: 'memoAdmin' }],
    });
    return res.json({
      result: ResponseResultTypeGroup.SUCCESS,
      data: result,
    });
  } catch (err) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: err.message,
      },
    });
  }
});

app.put('/:userId/certifications/:certId', passport.authenticate('jwt', { session: false }), async (req, res) => {
  // NOTE: sync 할 필요가 있는지 확인 필요.
  try {
    const { userId, certId } = req.params;
    const { certData } = req.body;

    if (!userId) {
      throw new Error('Invalid userId');
    }

    if (!certId) {
      throw new Error('Invalid certId');
    }

    const parsedData = JSON.parse(certData);
    if (!parsedData || !parsedData.issueDate) {
      return res.json({
        result: ResponseResultTypeGroup.FAIL,
        err: {
          message: '필수 정보 누락',
        },
      });
    }

    await UserCertificate.update(
      {
        certData,
        issueDate: parsedData.issueDate,
      },
      {
        where: {
          userId,
          certId,
        },
      },
    );

    return res.json({
      result: ResponseResultTypeGroup.SUCCESS,
    });
  } catch (err) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: err.message,
      },
    });
  }
});

app.get('/:userId/coupons', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const { userId } = req.params;
    if (!userId) {
      throw new Error('유효하지 않은 회원번호');
    }

    const couponCode = await CouponCode.findOne({
      where: {
        userId,
      },
    });

    const coupons = await Coupon.findAll({
      where: {
        userId,
      },
    });

    return res.json({
      result: ResponseResultTypeGroup.SUCCESS,
      data: {
        coupons,
        couponCode,
      },
    });
  } catch (err) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: err.message,
      },
    });
  }
});

const POINT_TYPE = {
  SPENT: 'spent',
  RESERVED: 'reserved',
  IMPEND: 'impend',
  DELETED: 'deleted',
};

app.get('/:userId/points/:type', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const { userId, type } = req.params;
    if (!userId) {
      throw new Error('invalid userId');
    }
    if (!type) {
      throw new Error('invalid type');
    }

    let data = null;
    if (POINT_TYPE.SPENT === type) {
      data = await LogUsingPoint.findAll({
        order: [['logDate', 'ASC']],
        include: [
          {
            model: Point,
            as: 'logPoint',
            where: {
              userId,
              removed: false,
            },
            include: [
              {
                model: PointType,
                as: 'pointType',
              },
            ],
          },
        ],
      });
    } else if (POINT_TYPE.RESERVED === type) {
      data = await Point.findAll({
        order: [['pubDate', 'ASC']],
        where: { userId, removed: false },
        include: [
          {
            model: PointType,
            as: 'pointType',
          },
        ],
      });
    } else if (POINT_TYPE.IMPEND === type) {
      // TODO: 소멸 에정 포인트 표시 정책 반영 필요
      data = await Point.findAll({
        order: [['pubDate', 'ASC']],
        where: {
          userId,
          removed: false,
          expireDate: {
            $between: [
              sequelize.fn('DATE', sequelize.fn('NOW')),
              sequelize.fn('DATE_ADD', sequelize.fn('NOW'), sequelize.literal('INTERVAL 6 MONTH')),
            ],
          },
        },
        include: [
          {
            model: PointType,
            as: 'pointType',
          },
        ],
      });
    } else if (POINT_TYPE.DELETED === type) {
      data = await Point.findAll({
        order: [['pubDate', 'ASC']],
        where: { userId, removed: true },
        include: [
          {
            model: PointType,
            as: 'pointType',
          },
        ],
      });
    } else {
      throw new Error('invalid point type');
    }

    return res.json({
      result: ResponseResultTypeGroup.SUCCESS,
      data,
    });
  } catch (err) {
    debug(err);
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: err.message,
      },
    });
  }
});

app.get('/:userId/applyCount', async (req, res) => {
  try {
    const { userId } = req.params;
    if (!userId) {
      throw new Error('invalid userId');
    }

    const applyCount = await UserApplyCount.findOne({
      where: { userId },
    });

    return res.json({
      result: ResponseResultTypeGroup.SUCCESS,
      data: applyCount,
    });
  } catch (err) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: err.message,
      },
    });
  }
});

app.put('/:userId/applyCount', async (req, res) => {
  try {
    const { userId } = req.params;
    const { applyToSitterCount, applyToParentCount } = req.body;
    if (!userId) {
      throw new Error('invalid userId');
    }

    const update = {};
    if (applyToSitterCount >= 0) {
      update.applyToSitterCount = applyToSitterCount;
    }
    if (applyToParentCount >= 0) {
      update.applyToParentCount = applyToParentCount;
    }

    await UserApplyCount.update(
      {
        ...update,
        lastUpdated: new Date(),
      },
      {
        where: { userId },
      },
    );

    return res.json({
      result: ResponseResultTypeGroup.SUCCESS,
    });
  } catch (err) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: err.message,
      },
    });
  }
});

export default app;
