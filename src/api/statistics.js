import Debug from 'debug';
import Moment from 'moment';
import { extendMoment } from 'moment-range';
import express from 'express';
import passport from '../core/momsitter_passport';
import sequelize from '../data/sequelize';
import ResponseResultTypeGroup from '../util/const/ResponseResultTypeGroup';
import UserTypeIdGroup from '../util/const/UserTypeIdGroup';
import ApplyStatusGroup from '../util/const/ApplyStatusGroup';
import _ from 'lodash';

const moment = extendMoment(Moment);
const debug = Debug('api');
const app = express.Router();

app.get('/dailyJoinSitter', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const query = "SELECT stat.userTypeId, COUNT(stat.userId) AS y " +
    "FROM ( " +
    "SELECT mu.userId, mu.userSignupDate, mr.userTypeId " +
    "FROM momsitter_user AS mu " +
    "JOIN momsitter_user_role AS mr " +
    "ON mu.userId = mr.userId " +
    `WHERE userSignupDate BETWEEN '${moment().format('YYYY-MM-DD')}' AND '${moment().add(1, 'days').format('YYYY-MM-DD')}' ` +
    ") AS stat " +
    "GROUP BY stat.userTypeId";

  const result = await sequelize.query(query, { type: sequelize.QueryTypes.SELECT});

  const userTypeText = {
    1: '[시터] 대학생 회원',
    2: '[시터] 선생님 회원',
    3: '[시터] 엄마 회원',
    4: '[시터] 일반 회원',
    5: '[부모] 부모회원',
  };
  if (result && result instanceof Array && result.length > 0) {
    res.json({
      result: ResponseResultTypeGroup.SUCCESS,
      data: result.map((item) => {
        return {
          ...item,
          name: userTypeText[item.userTypeId]
        }
      }),
    });
  } else {
    res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: 'fetch fail',
      }
    });
  }
});


app.get('/dailyJoinHistory', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const query = null;
    const getQueryByUserTypeId = (userTypeId) => {
      return 'SELECT perDate, COUNT(*) AS cnt\n' +
        '\tFROM (\n' +
        '\t\tSELECT DATE_FORMAT(userSignUpDate, \'%Y-%m-%d\') AS perDate, userTypeId, userId\n' +
        '\t\t\tFROM (\n' +
        '\t\t\t\tSELECT mu.userId, mu.userSignUpDate, mr.userTypeId\n' +
        '\t\t\t\t\tFROM momsitter_user AS mu\n' +
        '\t\t\t\t\tJOIN momsitter_user_role AS mr\n' +
        '\t\t\t\t\tON mu.userId = mr.userId\n' +
        `\t\t\t\t\tWHERE mr.userTypeId = ${userTypeId}\n` +
        '\t\t\t) AS mw\n' +
        '\t) AS stat\n' +
        '\tGROUP BY perDate';
    };

    const startDate = moment('2016-09-01').format('YYYY-MM-DD');
    const endDate = moment().format('YYYY-MM-DD');
    const dateRange = moment.range(startDate, endDate);
    const range = Array.from(dateRange.by('day'));

    const studentSitter = await sequelize.query(getQueryByUserTypeId(1), { type: sequelize.QueryTypes.SELECT});
    const teacherStiter = await sequelize.query(getQueryByUserTypeId(2), { type: sequelize.QueryTypes.SELECT});
    const momSitter = await sequelize.query(getQueryByUserTypeId(3), { type: sequelize.QueryTypes.SELECT});
    const normalSitter = await sequelize.query(getQueryByUserTypeId(4), { type: sequelize.QueryTypes.SELECT});
    const parent = await sequelize.query(getQueryByUserTypeId(5), { type: sequelize.QueryTypes.SELECT});
    const result = {};
    result[UserTypeIdGroup.COLLEGE] = range.map((item) => {
      let resultData = {};
      const formatDate = item.format('YYYY-MM-DD');
      const targetInd = _.findIndex(studentSitter, (o) => {
        return o.perDate === formatDate;
      });

      if (targetInd !== -1) {
        resultData = { ...studentSitter[targetInd] };
      } else {
        resultData = {
          perDate: formatDate,
          cnt: 0,
        };
      }
      return resultData;
    });
    result[UserTypeIdGroup.TEACHER] = range.map((item) => {
      let resultData = {};
      const formatDate = item.format('YYYY-MM-DD');
      const targetInd = _.findIndex(teacherStiter, (o) => {
        return o.perDate === formatDate;
      });

      if (targetInd !== -1) {
        resultData = { ...teacherStiter[targetInd] };
      } else {
        resultData = {
          perDate: formatDate,
          cnt: 0,
        };
      }
      return resultData;
    });
    result[UserTypeIdGroup.MOM] = range.map((item) => {
      let resultData = {};
      const formatDate = item.format('YYYY-MM-DD');
      const targetInd = _.findIndex(momSitter, (o) => {
        return o.perDate === formatDate;
      });

      if (targetInd !== -1) {
        resultData = { ...momSitter[targetInd] };
      } else {
        resultData = {
          perDate: formatDate,
          cnt: 0,
        };
      }
      return resultData;
    });
    result[UserTypeIdGroup.NORMAL] = range.map((item) => {
      let resultData = {};
      const formatDate = item.format('YYYY-MM-DD');
      const targetInd = _.findIndex(normalSitter, (o) => {
        return o.perDate === formatDate;
      });

      if (targetInd !== -1) {
        resultData = { ...normalSitter[targetInd] };
      } else {
        resultData = {
          perDate: formatDate,
          cnt: 0,
        };
      }
      return resultData;
    });
    result[UserTypeIdGroup.PARENT] = range.map((item) => {
      let resultData = {};
      const formatDate = item.format('YYYY-MM-DD');
      const targetInd = _.findIndex(parent, (o) => {
        return o.perDate === formatDate;
      });

      if (targetInd !== -1) {
        resultData = { ...parent[targetInd] };
      } else {
        resultData = {
          perDate: formatDate,
          cnt: 0,
        };
      }
      return resultData;
    });
    if (result) {
      res.json({
        result: ResponseResultTypeGroup.SUCCESS,
        data: result,
      });
    } else {
      res.json({
        result: ResponseResultTypeGroup.FAIL,
        err: {
          message: 'fetch fail',
        }
      });
    }
  } catch (e) {
    res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: 'fetch fail',
      }
    });
  }
});

app.get('/dailySitterProfileStatus', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const query = "SELECT profileStatus, COUNT(profileId) AS y " +
    "FROM momsitter_sitter_profile " +
    "GROUP BY profileStatus ";

  const result = await sequelize.query(query, { type: sequelize.QueryTypes.SELECT});

  const profileStatusText = {
    active: '활성',
    inactive: '비활성',
  };
  if (result && result instanceof Array && result.length > 0) {
    res.json({
      result: ResponseResultTypeGroup.SUCCESS,
      data: result.map((item) => {
        const label = profileStatusText[item.profileStatus];
        if (label) {
          return {
            ...item,
            name: label,
          }
        }
      }),
    });
  } else {
    res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: 'fetch fail',
      }
    });
  }
});


app.get('/dailyParentProfileStatus', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const query = "SELECT profileStatus, COUNT(profileId) AS y " +
    "FROM momsitter_parent_profile " +
    "GROUP BY profileStatus ";

  const result = await sequelize.query(query, { type: sequelize.QueryTypes.SELECT});

  const profileStatusText = {
    active: '활성',
    inactive: '비활성',
  };
  if (result && result instanceof Array && result.length > 0) {
    res.json({
      result: ResponseResultTypeGroup.SUCCESS,
      data: result.map((item) => {
        const label = profileStatusText[item.profileStatus];
        if (label) {
          return {
            ...item,
            name: label,
          }
        }
      }),
    });
  } else {
    res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: 'fetch fail',
      }
    });
  }
});

app.get('/dailyApplyToParentHistory', async (req, res) => {
  try {
    const totalQuery = 'SELECT applyDate, COUNT(*) AS cnt\n' +
      '\tFROM (\n' +
      '\t\tSELECT applyId, applyType, applyStatus, DATE_FORMAT(applyDate, \'%Y-%m-%d\') AS applyDate\n' +
      '\t\t\tFROM momsitter_apply\n' +
      '\t\t\tWHERE applyType = \'sitterToParent\'\n' +
      '\t) AS stat\n' +
      '\tGROUP BY applyDate';

    const getApplyQuery = (applyStatus) => {
      return 'SELECT applyDate, COUNT(*) AS cnt\n' +
        '\tFROM (\n' +
        '\t\tSELECT applyId, applyType, applyStatus, DATE_FORMAT(applyDate, \'%Y-%m-%d\') AS applyDate\n' +
        '\t\t\tFROM momsitter_apply\n' +
        '\t\t\tWHERE applyType = \'sitterToParent\'\n' +
        `\t\t\tAND applyStatus = '${applyStatus}'\n` +
        '\t) AS stat\n' +
        '\tGROUP BY applyDate';
    };

    const startDate = moment('2017-04-01').format('YYYY-MM-DD');
    const endDate = moment().format('YYYY-MM-DD');
    const dateRange = moment.range(startDate, endDate);
    const range = Array.from(dateRange.by('day'));

    const total = await sequelize.query(totalQuery, { type: sequelize.QueryTypes.SELECT });
    const accept = await sequelize.query(getApplyQuery(ApplyStatusGroup.ACCEPT), { type: sequelize.QueryTypes.SELECT });
    const decline = await sequelize.query(getApplyQuery(ApplyStatusGroup.DECLINE), { type: sequelize.QueryTypes.SELECT });
    const tuning = await sequelize.query(getApplyQuery(ApplyStatusGroup.TUNING), { type: sequelize.QueryTypes.SELECT });
    const request = await sequelize.query(getApplyQuery(ApplyStatusGroup.REQUEST), { type: sequelize.QueryTypes.SELECT });

    const result = {};
    result['total'] = range.map((item) => {
      let resultData = {};
      const formatDate = item.format('YYYY-MM-DD');
      const targetInd = _.findIndex(total, (o) => {
        return o.applyDate === formatDate;
      });

      if (targetInd !== -1) {
        resultData = { ...total[targetInd] };
      } else {
        resultData = {
          applyDate: formatDate,
          cnt: 0,
        };
      }
      return resultData;
    });
    result[ApplyStatusGroup.ACCEPT] = range.map((item) => {
      let resultData = {};
      const formatDate = item.format('YYYY-MM-DD');
      const targetInd = _.findIndex(accept, (o) => {
        return o.applyDate === formatDate;
      });

      if (targetInd !== -1) {
        resultData = { ...accept[targetInd] };
      } else {
        resultData = {
          applyDate: formatDate,
          cnt: 0,
        };
      }
      return resultData;
    });
    result[ApplyStatusGroup.DECLINE] = range.map((item) => {
      let resultData = {};
      const formatDate = item.format('YYYY-MM-DD');
      const targetInd = _.findIndex(decline, (o) => {
        return o.applyDate === formatDate;
      });

      if (targetInd !== -1) {
        resultData = { ...decline[targetInd] };
      } else {
        resultData = {
          applyDate: formatDate,
          cnt: 0,
        };
      }
      return resultData;
    });
    result[ApplyStatusGroup.TUNING] = range.map((item) => {
      let resultData = {};
      const formatDate = item.format('YYYY-MM-DD');
      const targetInd = _.findIndex(tuning, (o) => {
        return o.applyDate === formatDate;
      });

      if (targetInd !== -1) {
        resultData = { ...tuning[targetInd] };
      } else {
        resultData = {
          applyDate: formatDate,
          cnt: 0,
        };
      }
      return resultData;
    });
    result[ApplyStatusGroup.REQUEST] = range.map((item) => {
      let resultData = {};
      const formatDate = item.format('YYYY-MM-DD');
      const targetInd = _.findIndex(request, (o) => {
        return o.applyDate === formatDate;
      });

      if (targetInd !== -1) {
        resultData = { ...request[targetInd] };
      } else {
        resultData = {
          applyDate: formatDate,
          cnt: 0,
        };
      }
      return resultData;
    });

    if (result) {
      res.json({
        result: ResponseResultTypeGroup.SUCCESS,
        data: result
      });
    } else {
      res.json({
        result: ResponseResultTypeGroup.FAIL,
        err: {
          message: 'server error',
        }
      });
    }

  } catch (e) {
    res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: 'fetch fail',
      }
    });
  }
});

app.get('/dailyApplyToSitterHistory', async (req, res) => {
  try {
    const totalQuery = 'SELECT applyDate, COUNT(*) AS cnt\n' +
      '\tFROM (\n' +
      '\t\tSELECT applyId, applyType, applyStatus, DATE_FORMAT(applyDate, \'%Y-%m-%d\') AS applyDate\n' +
      '\t\t\tFROM momsitter_apply\n' +
      '\t\t\tWHERE applyType = \'parentToSitter\'\n' +
      '\t) AS stat\n' +
      '\tGROUP BY applyDate';

    const getApplyQuery = (applyStatus) => {
      return 'SELECT applyDate, COUNT(*) AS cnt\n' +
        '\tFROM (\n' +
        '\t\tSELECT applyId, applyType, applyStatus, DATE_FORMAT(applyDate, \'%Y-%m-%d\') AS applyDate\n' +
        '\t\t\tFROM momsitter_apply\n' +
        '\t\t\tWHERE applyType = \'parentToSitter\'\n' +
        `\t\t\tAND applyStatus = '${applyStatus}'\n` +
        '\t) AS stat\n' +
        '\tGROUP BY applyDate';
    };

    const startDate = moment('2017-04-01').format('YYYY-MM-DD');
    const endDate = moment().format('YYYY-MM-DD');
    const dateRange = moment.range(startDate, endDate);
    const range = Array.from(dateRange.by('day'));

    const total = await sequelize.query(totalQuery, { type: sequelize.QueryTypes.SELECT });
    const accept = await sequelize.query(getApplyQuery(ApplyStatusGroup.ACCEPT), { type: sequelize.QueryTypes.SELECT });
    const decline = await sequelize.query(getApplyQuery(ApplyStatusGroup.DECLINE), { type: sequelize.QueryTypes.SELECT });
    const tuning = await sequelize.query(getApplyQuery(ApplyStatusGroup.TUNING), { type: sequelize.QueryTypes.SELECT });
    const request = await sequelize.query(getApplyQuery(ApplyStatusGroup.REQUEST), { type: sequelize.QueryTypes.SELECT });

    const result = {};
    result['total'] = range.map((item) => {
      let resultData = {};
      const formatDate = item.format('YYYY-MM-DD');
      const targetInd = _.findIndex(total, (o) => {
        return o.applyDate === formatDate;
      });

      if (targetInd !== -1) {
        resultData = { ...total[targetInd] };
      } else {
        resultData = {
          applyDate: formatDate,
          cnt: 0,
        };
      }
      return resultData;
    });
    result[ApplyStatusGroup.ACCEPT] = range.map((item) => {
      let resultData = {};
      const formatDate = item.format('YYYY-MM-DD');
      const targetInd = _.findIndex(accept, (o) => {
        return o.applyDate === formatDate;
      });

      if (targetInd !== -1) {
        resultData = { ...accept[targetInd] };
      } else {
        resultData = {
          applyDate: formatDate,
          cnt: 0,
        };
      }
      return resultData;
    });
    result[ApplyStatusGroup.DECLINE] = range.map((item) => {
      let resultData = {};
      const formatDate = item.format('YYYY-MM-DD');
      const targetInd = _.findIndex(decline, (o) => {
        return o.applyDate === formatDate;
      });

      if (targetInd !== -1) {
        resultData = { ...decline[targetInd] };
      } else {
        resultData = {
          applyDate: formatDate,
          cnt: 0,
        };
      }
      return resultData;
    });
    result[ApplyStatusGroup.TUNING] = range.map((item) => {
      let resultData = {};
      const formatDate = item.format('YYYY-MM-DD');
      const targetInd = _.findIndex(tuning, (o) => {
        return o.applyDate === formatDate;
      });

      if (targetInd !== -1) {
        resultData = { ...tuning[targetInd] };
      } else {
        resultData = {
          applyDate: formatDate,
          cnt: 0,
        };
      }
      return resultData;
    });
    result[ApplyStatusGroup.REQUEST] = range.map((item) => {
      let resultData = {};
      const formatDate = item.format('YYYY-MM-DD');
      const targetInd = _.findIndex(request, (o) => {
        return o.applyDate === formatDate;
      });

      if (targetInd !== -1) {
        resultData = { ...request[targetInd] };
      } else {
        resultData = {
          applyDate: formatDate,
          cnt: 0,
        };
      }
      return resultData;
    });

    if (result) {
      res.json({
        result: ResponseResultTypeGroup.SUCCESS,
        data: result
      });
    } else {
      res.json({
        result: ResponseResultTypeGroup.FAIL,
        err: {
          message: 'server error',
        }
      });
    }

  } catch (e) {
    res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: 'fetch fail',
      }
    });
  }
});


export default app;
