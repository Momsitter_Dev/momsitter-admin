/* eslint-disable prettier/prettier */
import moment from 'moment';
import express from 'express';
import Debug from 'debug';
import {
  User,
  UserRole,
  UserType,
  UserProfileImage,
  UploadFiles,
} from '../../data/models/index';
import passport from '../../core/momsitter_passport';
import SyncSearchIndex from '../../util/SyncSearchIndex';
import ResponseResultTypeGroup from '../../util/const/ResponseResultTypeGroup';
import sequelize from '../../data/sequelize';
import UserStatusGroup from '../../util/const/UserStatusGroup';

const debug = Debug('api');
const app = express.Router();

app.get('/:userId/basicInfo', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const { userId } = req.params;
  if (!userId) {
    return res.status(200).json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: 'Invalid parameter',
      },
    });
  }
  try {
    const result = await User.find({
      where: { userId },
      include: [
        {
          model: UserProfileImage,
          as: 'userProfileImage',
          include: [{ model: UploadFiles, as: 'profileFile' }],
        },
        {
          model: UserRole,
          as: 'userRole',
          include: [{ model: UserType, as: 'userType' }],
        },
      ],
    });
    if (!result) {
      return res.status(200).json({
        result: ResponseResultTypeGroup.FAIL,
        err: {
          message: '정보 조회 오류',
        },
      });
    }
    return res.status(200).json({
      result: 'success',
      data: result,
    });
  } catch (err) {
    return res.status(200).json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: '정보 조회 오류',
      },
    });
  }
});

app.put('/:userId/basicInfo', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const { userId } = req.params;
    const {
      userProfileImage,
      userKey,
      userName,
      userGender,
      userPhone,
      userConnectType,
      userStatus,
      userRole,
      isOwnPhone,
      userBirthday,
    } = req.body;

    if (!userId) {
      return res.status(200).json({
        result: ResponseResultTypeGroup.FAIL,
        err: {
          message: '필수정보가 누락되었습니다.',
        },
      });
    }

    if (!userRole) {
      return res.status(200).json({
        result: ResponseResultTypeGroup.FAIL,
        err: {
          message: '필수정보가 누락되었습니다.',
        },
      });
    }

    if (userRole.length <= 0) {
      return res.status(200).json({
        result: ResponseResultTypeGroup.FAIL,
        err: {
          message: '필수정보가 누락되었습니다.',
        },
      });
    }

    if (!userProfileImage) {
      return res.status(200).json({
        result: ResponseResultTypeGroup.FAIL,
        err: {
          message: '필수정보가 누락되었습니다.',
        },
      });
    }

    await sequelize.transaction(async (transaction) => {
      await User.update({
        userName,
        userKey,
        userGender,
        userPhone,
        userConnectType,
        userStatus,
        isOwnPhone,
        userBirthday,
        userAge: (moment().diff(moment(userBirthday, 'YYYYMMDD'), 'years') + 1),
      }, {
        where: { userId },
        transaction,
      });

      await UserProfileImage.destroy({
        where: { userId },
        transaction,
      });

      await UserProfileImage.bulkCreate(userProfileImage, { transaction });

      await UserRole.destroy({
        where: { userId },
        transaction,
      });

      await UserRole.bulkCreate(userRole.map((item) => {
        return {
          userId,
          userTypeId: item.userTypeId,
        };
      }), { transaction });
    });
    await SyncSearchIndex(userId);
    return res.json({ result: ResponseResultTypeGroup.SUCCESS });
  } catch (err) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: err.message,
      },
    });
  }
});

app.put('/:userId/password', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const userId = req.params.userId;
  const userPassword = req.body.userPassword;
  if (!userId) {
    return res.status(200).json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: 'INVALID_PARAMETER',
      },
    });
  }
  if (!userPassword) {
    return res.status(200).json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: 'INVALID_PARAMETER',
      },
    });
  }

  try {
    const updatePassword = await User.update({
      userPassword: sequelize.fn('PASSWORD', userPassword),
    }, {
      where: { userId }
    }).catch((err) => {
      debug(err);
      return false;
    });
    if (!updatePassword) {
      return res.json({
        result: ResponseResultTypeGroup.FAIL,
        err: {
          message: 'Password update fail',
        },
      });
    }
    return res.json({ result: ResponseResultTypeGroup.SUCCESS });
  } catch (err) {
    return res.status(200).json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: 'Update fail',
      },
    });
  }
});

export default app;
