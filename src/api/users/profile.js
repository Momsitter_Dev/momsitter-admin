import express from 'express';
import Debug from 'debug';
import {
  User,
  UserRole,
  UserType,
  UserSchedule,
  UserLocation,
  SitterProfile,
  SitterActivity,
  ActivityType,
  SitterExperience,
  SitterPreferChildInfo,
  ChildAge,
  ParentProfile,
  ParentActivity,
  ParentChildInfo,
} from '../../data/models/index';
import passport from '../../core/momsitter_passport';
import ScheduleTypeIdGroup from '../../util/const/ScheduleTypeIdGroup';
import isParent from '../../util/user/isParent';
import isSitter from '../../util/user/isSitter';
import ResponseResultTypeGroup from '../../util/const/ResponseResultTypeGroup';
import SyncSearchIndex from '../../util/SyncSearchIndex';
import Sequelize from '../../data/sequelize';

const debug = Debug('api');
const app = express.Router();

app.get('/:userId/profile', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const { userId } = req.params;

  if (!userId) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: '필수정보 누락',
      },
    });
  }

  const profileInfo = await Promise.all([
    User.findOne({
      where: { userId },
    }).catch(err => {
      throw new Error(err);
    }),
    UserSchedule.findAll({
      where: { userId },
    }),
    UserLocation.findAll({
      where: { userId },
    }),
    UserRole.findAll({
      where: { userId },
      include: [{ model: UserType, as: 'userType' }],
    }),
    SitterProfile.findOne({
      where: { userId },
    }),
    SitterActivity.findAll({
      where: { userId },
      include: [{ model: ActivityType, as: 'sitterActivityType' }],
    }),
    SitterExperience.findAll({
      where: { userId },
    }),
    SitterPreferChildInfo.findAll({
      where: { userId },
      include: [{ model: ChildAge, as: 'sitterPreferChildAge' }],
    }),
    ParentProfile.findOne({
      where: { userId },
    }),
    ParentActivity.findAll({
      where: { userId },
      include: [{ model: ActivityType, as: 'parentActivityType' }],
    }),
    ParentChildInfo.findAll({
      where: { userId },
      include: [{ model: ChildAge, as: 'parentChildAge' }],
    }),
  ])
    .then(result => {
      const obj = [
        'user',
        'userSchedule',
        'userLocation',
        'userRole',
        'sitterProfile',
        'sitterActivity',
        'sitterExperience',
        'sitterPreferChildInfo',
        'parentProfile',
        'parentActivity',
        'parentChildInfo',
      ];
      let data = {};
      obj.forEach((key, index) => {
        if (key === 'user') {
          data = {
            ...data,
            ...result[key],
          };
        } else {
          data[key] = result[index];
        }
      });
      return data;
    })
    .catch(err => {
      debug(err);
      return false;
    });

  if (!profileInfo) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: '데이터 조회 오류',
      },
    });
  }

  return res.json({
    result: ResponseResultTypeGroup.SUCCESS,
    data: profileInfo,
  });
});

app.put('/:userId/profile', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const { userId } = req.params;
    const {
      userTypeId,
      sitterProfile,
      sitterActivity,
      sitterExperience,
      sitterPreferChildInfo,
      userSchedule,
      userLocation,
      parentActivity,
      parentChildInfo,
      parentProfile,
    } = req.body;

    if (!userId) {
      throw new Error('필수정보가 누락되었습니다.');
    }

    if (!userTypeId) {
      throw new Error('필수정보가 누락되었습니다.');
    }

    let scheduleType = null;

    if (isParent(userTypeId)) {
      scheduleType = ScheduleTypeIdGroup.PARENT_WANTED;
      await Sequelize.transaction(async transaction => {
        await ParentProfile.update(
          { ...parentProfile },
          {
            where: {
              profileId: parentProfile.profileId,
            },
            transaction,
          },
        );

        await ParentActivity.destroy({
          where: { userId },
          transaction,
        });

        await ParentActivity.bulkCreate(
          parentActivity.map(item => ({
            userId,
            activityId: item.activityId,
          })),
          { transaction },
        );

        await ParentChildInfo.destroy({
          where: { userId },
          transaction,
        });

        await ParentChildInfo.bulkCreate(
          parentChildInfo.map(item => ({
            ...item,
            userId,
          })),
          { transaction },
        );

        await UserSchedule.destroy({
          where: { userId },
          transaction,
        });

        await UserSchedule.bulkCreate(
          userSchedule.map(item => ({
            ...item,
            userId,
            scheduleTypeId: ScheduleTypeIdGroup.PARENT_WANTED,
          })),
          { transaction },
        );

        await UserLocation.destroy({
          where: { userId },
          transaction,
        });

        await UserLocation.bulkCreate(
          userLocation.map(item => ({
            ...item,
            userId,
          })),
          { transaction },
        );
      });
    }

    if (isSitter(userTypeId)) {
      await Sequelize.transaction(async transaction => {
        await SitterProfile.update(
          {
            ...sitterProfile,
          },
          {
            where: { userId },
            transaction,
          },
        );

        await SitterExperience.destroy({
          where: { userId },
          transaction,
        });

        if (sitterExperience) {
          await SitterExperience.bulkCreate(
            sitterExperience.map(item => ({
              ...item,
              userId,
            })),
            { transaction },
          );
        }

        await SitterPreferChildInfo.destroy({
          where: { userId },
          transaction,
        });

        await SitterPreferChildInfo.bulkCreate(
          sitterPreferChildInfo.map(item => ({
            userId,
            childAgeId: item.childAgeId,
          })),
          { transaction },
        );

        await SitterActivity.destroy({
          where: { userId },
          transaction,
        });

        await SitterActivity.bulkCreate(
          sitterActivity.map(item => ({
            userId,
            activityId: item.activityId,
          })),
          { transaction },
        );

        await UserSchedule.destroy({
          where: { userId },
          transaction,
        });

        await UserSchedule.bulkCreate(
          userSchedule.map(item => ({
            ...item,
            userId,
            scheduleTypeId: ScheduleTypeIdGroup.SITTER_WANTED,
          })),
          { transaction },
        );

        await UserLocation.destroy({
          where: { userId },
          transaction,
        });

        await UserLocation.bulkCreate(
          userLocation.map(item => ({
            ...item,
            userId,
          })),
          { transaction },
        );
      });
    }

    try {
      await SyncSearchIndex(userId, userTypeId);
    } catch (err) {
      debug(err);
    }

    return res.json({
      result: ResponseResultTypeGroup.SUCCESS,
    });
  } catch (err) {
    debug(err);
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: err.message,
      },
    });
  }

  // NOTE. 프로필 공개 / 비공개에 따른 예약메세지 세팅
});

export default app;
