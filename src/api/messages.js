import express from 'express';
import parseSearchOptionRawQuery from '../util/dataFormat/parseSearchOptionRawQuery';
import parseLimitOptionRawQuery from '../util/dataFormat/parseLimitOptionRawQuery';
import sequelize from '../data/sequelize';
import CountMessageListQuery from '../data/query/CountMessageList';
import ResponseResultTypeGroup from '../util/const/ResponseResultTypeGroup';
import passport from '../core/momsitter_passport';
import parseOrderOptionRawQuery from '../util/dataFormat/parseOrderOptionRawQuery';
import MessageListQuery from '../data/query/MessageList';

const app = express.Router();

app.get('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const { requestPage, searchOptions, orderOption } = req.query;
    const page = { maxPage: 0, currentPage: 0 };

    let offset = 0;
    if (requestPage && requestPage > 0) {
      offset = requestPage * 10;
    } else {
      offset = 0;
    }
    const where = parseSearchOptionRawQuery(searchOptions);
    const countQuery = CountMessageListQuery(where);
    const count = await sequelize.query(countQuery, { type: sequelize.QueryTypes.SELECT }).then(data => {
      return data[0]['CNT'];
    });
    const order = parseOrderOptionRawQuery(orderOption);
    const limit = parseLimitOptionRawQuery(offset);
    const query = MessageListQuery(where, order, limit);
    const users = await sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
    page.maxPage = Math.ceil(count / 10);
    page.currentPage = Number(requestPage) || 0;
    return res.json({
      result: ResponseResultTypeGroup.SUCCESS,
      data: users,
      page,
    });
  } catch (err) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: err.message,
      },
    });
  }
});

export default app;
