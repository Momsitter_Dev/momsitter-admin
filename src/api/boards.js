/* eslint-disable prettier/prettier */
import Debug from 'debug';
import express from 'express';

import { Notice } from '../data/models';
import parseOrderOption from '../util/dataFormat/parseOrderOption';
import parseSearchOptions from '../util/dataFormat/parseSearchOptions';
import ResponseResultTypeGroup from '../util/const/ResponseResultTypeGroup';

const debug = Debug('api');
const app = express.Router();

app.get('/notice', async (req, res) => {
  try {
    const {
      requestPage,
      searchOptions,
      orderOption,
    } = req.query;
    const page = { maxPage: 0, currentPage: 0 };

    let offset = 0;
    if (requestPage && requestPage > 0) {
      offset = requestPage * 10;
    } else {
      offset = 0;
    }

    const data = await Notice.findAndCountAll({
      offset,
      limit: 10,
      order: parseOrderOption(orderOption),
      where: parseSearchOptions(searchOptions),
    });


    page.maxPage = Math.ceil(data.count / 10);
    page.currentPage = Number(requestPage) || 0;

    return res.json({
      result: ResponseResultTypeGroup.SUCCESS,
      data,
      page,
    });
  } catch (err) {
    debug(err);
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: err.message,
      },
    });
  }
});

app.post('/notice', async (req, res) => {
  try {
    const {
      title,
      content,
      author,
    } = req.body;

    await Notice.create({
      title,
      content,
      author,
      registerDate: new Date(),
      view: 0,
    });

    return res.json({ result: ResponseResultTypeGroup.SUCCESS });
  } catch (err) {
    debug(err);
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: err.message,
      },
    });
  }
});

app.get('/notice/:noticeId', async (req, res) => {
  try {
    const { noticeId } = req.params;
    const article = await Notice.findOne({
      where: { id: noticeId },
    });
    return res.json({
      result: ResponseResultTypeGroup.SUCCESS,
      data: article,
    });
  } catch (err) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: err.message,
      },
    });
  }
});

app.put('/notice/:noticeId', async (req, res) => {
  try {
    const { noticeId } = req.params;
    const {
      title,
      content,
      author,
    } = req.body;

    await Notice.update({
      title,
      content,
      author,
      updateDate: new Date(),
    }, {
      where: { id: noticeId },
    });

    return res.json({ result: ResponseResultTypeGroup.SUCCESS });
  } catch (err) {
    debug(err);
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: err.message,
      },
    });
  }
});

app.delete('/notice/:noticeId', async (req, res) => {
  try {
    const { noticeId } = req.params;

    await Notice.destroy({
      where: { id: noticeId },
    });

    return res.json({ result: ResponseResultTypeGroup.SUCCESS });
  } catch (err) {
    debug(err);
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: err.message,
      },
    });
  }
});

export default app;
