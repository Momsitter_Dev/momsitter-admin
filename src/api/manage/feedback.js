import express from 'express';
import { Product } from '../../data/models/index';
import passport from '../../core/momsitter_passport';
import { Feedback } from '../../data/models/index';
import ResponseResultTypeGroup from '../../util/const/ResponseResultTypeGroup';

const app = express.Router();

app.get('/', async (req, res) => {
  const {
    requestPage,
    searchFilter,
    searchText,
    searchOrderColumn,
    searchOrderType,
  } = req.query;
  const searchCondition = {
    order: [],
    where: {},
  };
  const page = { maxPage: 0, currentPage: 0 };

  let offset = 0;
  if (requestPage && requestPage > 0) {
    offset = requestPage * 10;
  } else {
    offset = 0;
  }

  if (searchFilter && searchText) {
    searchCondition.where[searchFilter] = searchText;
  }

  if (searchOrderColumn && searchOrderType) {
    searchCondition.order = [['postDate', 'DESC'], [searchOrderColumn, searchOrderType]];
  } else {
    searchCondition.order = [['postDate', 'DESC']];
  }

  const users = await Feedback.findAndCountAll({
    offset,
    limit: 10,
    order: searchCondition.order,
    where: searchCondition.where,
  })
    .then(data => data)
    .catch(err => {
      console.error(err);
      return false;
    });

  if (users) {
    page.maxPage = Math.ceil(users.count / 10);
    page.currentPage = Number(requestPage) || 0;

    res.json({
      result: ResponseResultTypeGroup.SUCCESS,
      data: users,
      page,
    });
  } else {
    res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: { message: 'fetching error' }
    });
  }
});

export default app;
