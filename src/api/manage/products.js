import express from 'express';
import { Product } from '../../data/models/index';
import passport from '../../core/momsitter_passport';

const app = express.Router();

/**
 * TODO
 *  1. get Products
 *  2. update Products
 *  3. delete Products
 *  4. insert new products
 */

app.get('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const {
    requestPage,
    searchFilter,
    searchText,
    searchOrderColumn,
    searchOrderType,
  } = req.query;
  const searchCondition = {
    order: [],
    where: {},
  };
  const page = { maxPage: 0, currentPage: 0 };

  let offset = 0;
  if (requestPage && requestPage > 0) {
    offset = requestPage * 10;
  } else {
    offset = 0;
  }

  if (searchFilter && searchText) {
    searchCondition.where[searchFilter] = searchText;
  }

  if (searchOrderColumn && searchOrderType) {
    searchCondition.order = [[searchOrderColumn, searchOrderType]];
  }

  const users = await Product.findAndCountAll({
    offset,
    limit: 10,
    order: searchCondition.order,
    where: searchCondition.where,
  })
    .then(data => data)
    .catch(err => {
      console.error(err);
      return false;
    });

  if (users) {
    page.maxPage = Math.ceil(users.count / 10);
    page.currentPage = Number(requestPage) || 0;

    res.json({
      result: 'success',
      data: users,
      page,
    });
  } else {
    res.json({
      result: 'fail',
      message: 'fetching error',
    });
  }
});

app.post('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const {
    productCode,
    productName,
    productDesc,
    productPrice,
    productType,
    productFor,
    productPeriod,
    productApplyToSitterCount,
    productApplyToParentCount,
  } = req.body;

  // TODO validation checking
  const result = await Product.create({
    productCode,
    productName,
    productDesc,
    productPrice,
    productType,
    productFor,
    productPeriod,
    productApplyToSitterCount,
    productApplyToParentCount,
  }).catch(() => false);

  if (result) {
    res.json({
      result: 'success',
    });
  } else {
    res.json({
      result: 'fail',
      err: {
        message: 'Fail to insert product',
      },
    });
  }
});

app.delete('/:productId', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const { productId } = req.params;
  if (productId) {
    const result = await Product.destroy({
      where: { productId },
    }).then(() => true).catch(() => false);

    if (result) {
      res.json({
        result: 'success',
      });
    } else {
      res.json({
        result: 'fail',
        err: {
          message: 'Fail to delete',
        },
      });
    }
  } else {
    res.json({
      result: 'fail',
      err: {
        message: 'Invalid ProductId',
      },
    });
  }
});

export default app;
