import _ from 'lodash';
import express from 'express';
import Debug from 'debug';
import moment from 'moment';
import {
  User,
  UserRole,
  UserType,
  Apply
} from '../../../data/models/index';
import passport from '../../../core/momsitter_passport';
import ExportCSV from '../../../util/ExportCSV';
import parseSearchOptions from '../../../util/dataFormat/parseSearchOptions';
import parseOrderOption from '../../../util/dataFormat/parseOrderOption';
import sequelize from '../../../data/sequelize';
import ResponseResultTypeGroup from '../../../util/const/ResponseResultTypeGroup';

const debug = Debug('api');
const app = express.Router();

app.get('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const {
    requestPage,
    searchOptions,
    orderOption,
  } = req.query;
  const page = { maxPage: 0, currentPage: 0 };

  let offset = 0;
  if (requestPage && requestPage > 0) {
    offset = requestPage * 10;
  } else {
    offset = 0;
  }
  try {
    const apply = await Apply.findAndCountAll({
      attributes: ['applyId'],
      order: parseOrderOption(orderOption),
      offset,
      limit: 10,
      where: { ...parseSearchOptions(searchOptions), applyType: 'sitterToParent' },
    }).catch((err) => {
      console.error(err);
      return false;
    });

    const applyFullData = await Apply.findAll({
      raw: true,
      where: {
        applyId: {
          $in: apply.rows.map((item) => { return item.applyId; }),
        },
      },
    });

    const receiverUserId = applyFullData.map((item) => {
      return item.receiverId;
    });

    const applicantUserId = applyFullData.map((item) => {
      return item.applicantId;
    });

    const receiver = await User.findAll({
      where: {
        userId: {
          $in: _.uniq(receiverUserId),
        },
      },
      include: [
        {
          model: UserRole,
          as: 'userRole',
          include: [{ model: UserType, as: 'userType' }],
        }
      ],
    });

    const applicant = await User.findAll({
      where: {
        userId: {
          $in: _.uniq(applicantUserId),
        },
      },
      include: [
        {
          model: UserRole,
          as: 'userRole',
          include: [{ model: UserType, as: 'userType' }],
        }
      ],
    });

    // TODO: MAPPING with apply & receiver
    for (let i = 0; i < applyFullData.length; i += 1) {
      for (let j = 0; j < receiver.length; j += 1) {
        if (applyFullData[i].receiverId === receiver[j].userId) {
          applyFullData[i].receiver = receiver[j];
          break;
        }
      }
    }

    // TODO: MAPPING with apply & applicant
    for (let i = 0; i < applyFullData.length; i += 1) {
      for (let j = 0; j < applicant.length; j += 1) {
        if (applyFullData[i].applicantId === applicant[j].userId) {
          applyFullData[i].applicant = applicant[j];
          break;
        }
      }
    }

    page.maxPage = Math.ceil(apply.count / 10);
    page.currentPage = Number(requestPage) || 0;

    return res.json({
      result: ResponseResultTypeGroup.SUCCESS,
      data: {
        rows: applyFullData,
        count: apply.count,
      },
      page,
    });
  } catch (err) {
    debug(err);
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      message: 'fetching error',
    });
  }
});

app.get('/csv', async (req, res) => {
  const {
    searchOptions,
    orderOption,
  } = req.query;

  const users = await Apply.findAndCountAll({
    raw: true,
    attributes: [
      'applyId',
      'applicantId',
      'receiverId',
      [sequelize.fn('DATE_FORMAT', sequelize.col('applyDate'), '%Y-%m-%d %H:%i:%s'), 'applyDate'],
      [sequelize.fn('DATE_FORMAT', sequelize.col('updateDate'), '%Y-%m-%d %H:%i:%s'), 'updateDate'],
      [sequelize.fn('DATE_FORMAT', sequelize.col('responseDate'), '%Y-%m-%d %H:%i:%s'), 'responseDate'],
      [sequelize.fn('DATE_FORMAT', sequelize.col('readDate'), '%Y-%m-%d %H:%i:%s'), 'readDate'],
      'applyStatus',
      'parentData',
      'sitterData',
      'declineReason',
      'declineType',
      'tuningReason',
      'applyType',
      'legacyApplyId'
    ],
    order: parseOrderOption(orderOption),
    where: { ...parseSearchOptions(searchOptions), applyType: 'sitterToParent' },
    include: [
      {
        model: User,
        as: 'applicant',
        include: [
          {
            model: UserRole,
            as: 'userRole',
            include: [{ model: UserType, as: 'userType' }],
          }
        ],
      },
      {
        model: User,
        as: 'receiver',
        include: [
          {
            model: UserRole,
            as: 'userRole',
            include: [{ model: UserType, as: 'userType' }],
          }
        ],
      },
    ],
  }).catch(err => {
    console.error(err);
    return false;
  });

  if (users) {
    const fields = [
      'applyId',
      'applicantId',
      'applicant.userName',
      'applicant.userRole.userType.userDetailTypeDesc',
      'receiverId',
      'receiver.userName',
      'receiver.userRole.userType.userDetailTypeDesc',
      'applyType',
      'applyStatus',
      'declineReason',
      'tuningReason',
      'applyDate',
      'readDate',
      'responseDate',
    ];
    const fieldNames = [
      '회원번호',
      '지원자 번호',
      '지원자 이름',
      '지원자 회원 타입',
      '지원 받은 사람 번호',
      '지원 받은 사람 이름',
      '지원 받은 사람 회원 타입',
      '지원 타입',
      '지원 상태',
      '거절 사유',
      '조율 사유',
      '지원 일자',
      '지원서 확인 일자',
      '응답 일자',
    ];
    const exportResult = await ExportCSV({
      fields,
      fieldNames,
      data: users.rows,
    }).catch((err) => {
      debug(err);
      return false;
    });
    if (exportResult) {
      const current = moment(new Date()).locale('ko').format('YYYYMMDDhmm');
      res.header("Content-Disposition", "attachment; filename=" + current + ".csv");
      res.type("text/csv");
      res.send(200, exportResult);
    } else {
      res.json({
        result: 'fail',
        message: 'CSV EXPORT FAIL',
      });
    }
  } else {
    res.json({
      result: 'fail',
      message: 'fetching error',
    });
  }
});

export default app;
