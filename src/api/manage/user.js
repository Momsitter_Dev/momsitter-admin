import jwt from 'jsonwebtoken';
import express from 'express';
import moment from 'moment';
import Debug from 'debug';
import {
  User,
  UserRole,
  UserType,
  SitterProfile,
  ParentProfile,
  UserProduct,
  UserApplyCount,
  Admin,
  AdminMemo,
  Product,
} from '../../data/models/index';
import sequelize from '../../data/sequelize';
import passport from '../../core/momsitter_passport';
import ResponseResultTypeGroup from '../../util/const/ResponseResultTypeGroup';
import ExportCSV from '../../util/ExportCSV';
import SyncSearchIndex from '../../util/SyncSearchIndex';
import parseSearchOptionRawQuery from '../../util/dataFormat/parseSearchOptionRawQuery';
import parseOrderOptionRawQuery from '../../util/dataFormat/parseOrderOptionRawQuery';
import parseLimitOptionRawQuery from '../../util/dataFormat/parseLimitOptionRawQuery';
import ParentList from '../../data/query/ParentList';
import SitterList from '../../data/query/SitterList';
import CountParentList from '../../data/query/CountParentList';
import CountSitterList from '../../data/query/CountSitterList';

const debug = Debug('api');
const app = express.Router();

app.get('/token', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const { userId } = req.query;

  const user = await User.findOne({
    attributes: ['userId', 'userName', 'userStatus'],
    where: { userId },
    include: [
      {
        model: UserRole,
        as: 'userRole',
        attributes: ['userTypeId'],
      },
    ],
  }).catch(err => {
    debug(err);
  });

  if (user) {
    try {
      const token = jwt.sign(
        {
          userId: user.userId,
          userName: user.userName,
          userStatus: user.userStatus,
          userTypeId: user.userRole[0].userTypeId,
        },
        'momsitter application',
      );

      res.json({
        result: ResponseResultTypeGroup.SUCCESS,
        data: {
          user,
          token,
        },
      });
    } catch (err) {
      debug(err);
      res.json({
        result: ResponseResultTypeGroup.FAIL,
        err: {
          message: 'Token Sign Error',
        },
      });
    }
  } else {
    res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: 'User Not Exist',
      },
    });
  }
});

app.post('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
  // TODO validation checking
  const {
    userName,
    userPhone,
    userKey,
    userPassword,
    userBirthday,
    userConnectType,
    userGender,
    userStatus,
    contactWayDetail,
    userRole,
  } = req.body;
  const userAge = moment().diff(moment(userBirthday).format('YYYY-MM-DD'), 'years');

  const result = await sequelize
    .transaction(transaction =>
      User.create(
        {
          userName,
          userKey,
          userPassword: sequelize.fn('PASSWORD', userPassword),
          userBirthday,
          userPhone,
          userGender,
          userEmail: userKey,
          userAge,
          userStatus,
          userConnectType,
          contactWayDetail,
          userRole: userRole.map(item => {
            return { userTypeId: item };
          }),
        },
        {
          transaction,
          include: [{ model: UserRole, as: 'userRole' }],
        },
      ).catch(() => {
        throw new Error();
      }),
    )
    .catch(err => {
      debug(err);
      return false;
    });

  if (result) {
    res.json({ result: 'success' });
  } else {
    res.json({
      result: 'fail',
      message: 'add user Fail',
    });
  }
});

app.get('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const { requestPage, searchFilter, searchText } = req.query;
  const searchCondition = {
    order: [],
    where: {},
  };
  const page = { maxPage: 0, currentPage: 0 };

  let offset = 0;
  if (requestPage && requestPage > 0) {
    offset = requestPage * 10;
  } else {
    offset = 0;
  }

  if (searchFilter && searchText) {
    searchCondition.where[searchFilter] = searchText;
  }

  const users = await User.findAndCountAll({
    offset,
    limit: 10,
    order: [['userSignUpDate', 'DESC']],
    where: searchCondition.where,
    include: [
      {
        model: UserRole,
        as: 'userRole',
        include: [{ model: UserType, as: 'userType' }],
      },
      { model: SitterProfile, as: 'sitterProfile' },
      { model: ParentProfile, as: 'parentProfile' },
    ],
  }).catch(err => {
    debug(err);
    return false;
  });

  if (users) {
    page.maxPage = Math.ceil(users.count / 10);
    page.currentPage = Number(requestPage) || 0;

    res.json({
      result: 'success',
      data: users,
      page,
    });
  } else {
    res.json({
      result: 'fail',
      message: 'fetching error',
    });
  }
});

app.get('/csv', async (req, res) => {
  const { searchFilter, searchText, searchOrderColumn, searchOrderType } = req.query;
  const searchCondition = {
    order: [],
    where: {},
  };

  if (searchFilter && searchText) {
    searchCondition.where[searchFilter] = searchText;
  }

  if (searchOrderColumn && searchOrderType) {
    searchCondition.order = [[searchOrderColumn, searchOrderType]];
  }

  const users = await User.findAndCountAll({
    order: searchCondition.order,
    where: searchCondition.where,
    raw: true,
    include: [
      {
        model: UserRole,
        as: 'userRole',
        include: [{ model: UserType, as: 'userType' }],
      },
      { model: SitterProfile, as: 'sitterProfile' },
      { model: ParentProfile, as: 'parentProfile' },
    ],
  })
    .then(data => data)
    .catch(err => {
      debug(err);
      return false;
    });

  if (users) {
    const fields = [
      'userId',
      'userKey',
      'userName',
      'userSignUpDate',
      'userBirthday',
      'userAge',
      'userGender',
      'userConnectType',
      'userPhone',
      'userEmail',
      'userStatus',
      'userTypeId',
      'userDetailTypeDesc',
    ];
    const fieldNames = [
      '회원번호',
      '로그인 키',
      '회원이름',
      '가입 일자',
      '생년월일',
      '나이',
      '성별',
      '가입방법',
      '핸드폰 번호',
      '이메일',
      '회원 상태',
      '회원 타입 번호',
      '회원 타입 설명',
    ];
    const exportResult = await {
      fields,
      fieldNames,
      data: users.rows,
    }.catch(err => {
      debug(err);
      return false;
    });
    if (exportResult) {
      const current = moment(new Date())
        .locale('ko')
        .format('YYYYMMDDhmm');
      res.header('Content-Disposition', 'attachment; filename=' + current + '.csv');
      res.type('text/csv');
      res.send(200, exportResult);
    } else {
      res.json({
        result: 'fail',
        message: 'CSV EXPORT FAIL',
      });
    }
  } else {
    res.json({
      result: 'fail',
      message: 'fetching error',
    });
  }
});

app.get('/sitter', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const { requestPage, searchOptions, orderOption } = req.query;
    const page = { maxPage: 0, currentPage: 0 };

    let offset = 0;
    if (requestPage && requestPage > 0) {
      offset = requestPage * 10;
    } else {
      offset = 0;
    }
    const where = parseSearchOptionRawQuery(searchOptions);
    const countQuery = CountSitterList(where);
    const count = await sequelize.query(countQuery, { type: sequelize.QueryTypes.SELECT }).then(data => {
      return data[0]['CNT'];
    });
    const order = parseOrderOptionRawQuery(orderOption);
    const limit = parseLimitOptionRawQuery(offset);
    const query = SitterList(where, order, limit);
    const users = await sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
    page.maxPage = Math.ceil(count / 10);
    page.currentPage = Number(requestPage) || 0;
    return res.json({
      result: ResponseResultTypeGroup.SUCCESS,
      data: users,
      page,
    });
  } catch (err) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: err.message,
      },
    });
  }
});

app.get('/sitter/csv', async (req, res) => {
  try {
    const { searchOptions, orderOption } = req.query;

    const where = parseSearchOptionRawQuery(searchOptions);
    const order = parseOrderOptionRawQuery(orderOption);
    const query = SitterList(where, order, '');
    const users = await sequelize.query(query, { type: sequelize.QueryTypes.SELECT });

    if (!users) {
      throw new Error('cannot fetch parent');
    }

    const fields = [
      'userId',
      'joinType',
      'userKey',
      'userName',
      'userSignUpDate',
      'userBirthday',
      'userAge',
      'userGender',
      'userConnectType',
      'userPhone',
      'userEmail',
      'userStatus',
      'userTypeId',
      'userDetailTypeDesc',
      'profileStatus',
      'createDate',
      'updateDate',
      'isOwnPhone',
    ];
    const fieldNames = [
      '회원번호',
      '가입방법',
      '로그인 키',
      '회원이름',
      '가입 일자',
      '생년월일',
      '나이',
      '성별',
      '가입방법',
      '핸드폰 번호',
      '이메일',
      '회원 상태',
      '회원 타입 번호',
      '회원 타입 설명',
      '프로필 상태',
      '프로필 생성일',
      '프로필 수정일',
      '본인명의 여부',
    ];

    const exportResult = await ExportCSV({
      fields,
      fieldNames,
      data: users,
    });

    if (!exportResult) {
      throw new Error('csv export fail');
    }

    res.header('Content-Disposition', `attachment; filename=${moment().format('YYYYMMDDhmm')}.csv`);
    res.type('text/csv');
    res.status(200).send(exportResult);
  } catch (err) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: err.message,
      },
    });
  }
});

app.get('/parent', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const { requestPage, searchOptions, orderOption } = req.query;
    const page = { maxPage: 0, currentPage: 0 };
    let offset = 0;
    if (requestPage && requestPage > 0) {
      offset = requestPage * 10;
    } else {
      offset = 0;
    }
    const where = parseSearchOptionRawQuery(searchOptions);
    const countQuery = CountParentList(where);
    const count = await sequelize.query(countQuery, { type: sequelize.QueryTypes.SELECT }).then(data => {
      return data[0]['CNT'];
    });
    if (!count) {
      throw new Error('cannot get count of parent');
    }
    const order = parseOrderOptionRawQuery(orderOption);
    const limit = parseLimitOptionRawQuery(offset);
    const query = ParentList(where, order, limit);
    const users = await sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
    page.maxPage = Math.ceil(count / 10);
    page.currentPage = Number(requestPage) || 0;
    return res.json({
      result: ResponseResultTypeGroup.SUCCESS,
      data: users,
      page,
    });
  } catch (err) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: err.message,
      },
    });
  }
});

app.get('/parent/csv', async (req, res) => {
  try {
    const { searchOptions, orderOption } = req.query;
    const where = parseSearchOptionRawQuery(searchOptions);
    const order = parseOrderOptionRawQuery(orderOption);
    const query = ParentList(where, order, '');
    const users = await sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
    if (!users) {
      throw new Error('cannot fetch parent');
    }
    const fields = [
      'userId',
      'joinType',
      'userKey',
      'userName',
      'userSignUpDate',
      'userBirthday',
      'userAge',
      'userGender',
      'userConnectType',
      'userPhone',
      'userEmail',
      'userStatus',
      'userTypeId',
      'userDetailTypeDesc',
      'profileStatus',
      'createDate',
      'updateDate',
      'isOwnPhone',
    ];
    const fieldNames = [
      '회원번호',
      '가입방법',
      '로그인 키',
      '회원이름',
      '가입 일자',
      '생년월일',
      '나이',
      '성별',
      '가입방법',
      '핸드폰 번호',
      '이메일',
      '회원 상태',
      '회원 타입 번호',
      '회원 타입 설명',
      '프로필 상태',
      '신청서 생성일',
      '신청서 수정일',
      '타인 명의 여부',
    ];
    const exportResult = await ExportCSV({
      fields,
      fieldNames,
      data: users,
    });

    if (!exportResult) {
      throw new Error('csv export fail');
    }
    res.header('Content-Disposition', `attachment; filename=${moment().format('YYYYMMDDhmm')}.csv`);
    res.type('text/csv');
    return res.status(200).send(exportResult);
  } catch (err) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: err.message,
      },
    });
  }
});

app.put('/:userId/status', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const { userId } = req.params;
    const { userStatus } = req.body;
    if (!userId) {
      throw new Error('invalid userId');
    }
    const result = await User.update({ userStatus }, { where: { userId } });
    if (!result) {
      throw new Error('update fail');
    }
    await SyncSearchIndex(userId);
    return res.json({
      result: 'success',
    });
  } catch (err) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: err.message,
      },
    });
  }
});

app.put('/:userId/searchable', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const { userId } = req.params;
    const { userSearchable } = req.body;
    if (!userId) {
      throw new Error('invalid userId');
    }
    await SitterProfile.update(
      {
        searchable: userSearchable,
      },
      {
        where: { userId },
      },
    );
    SyncSearchIndex(userId);
    return res.json({
      result: ResponseResultTypeGroup.SUCCESS,
    });
  } catch (err) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: err.message,
      },
    });
  }
});

app.get('/:userId/memos', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const { userId } = req.params;
  if (userId) {
    const result = await User.findAndCountAll({
      where: { userId },
      include: [
        {
          model: AdminMemo,
          as: 'userMemo',
          include: [{ model: Admin, as: 'memoAdmin' }],
        },
      ],
    }).catch(() => false);

    if (result) {
      res.json({
        result: 'success',
        data: result,
      });
    } else {
      res.json({
        result: 'fail',
        message: 'Invalid UserId',
      });
    }
  } else {
    res.json({
      result: 'fail',
      message: 'Invalid UserId',
    });
  }
});

app.post('/:userId/memos', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const { userId } = req.params;
  const { memoResId, memoType, memoClass, memoContent, memoReqMethod, memoStatus } = req.body;
  const sessionUser = req.user;

  if (sessionUser && sessionUser.adminId) {
    if (userId) {
      const createResult = await AdminMemo.create({
        adminId: sessionUser.adminId,
        memoResId,
        memoType,
        memoClass,
        memoContent,
        memoReqMethod,
        memoStatus,
        userId,
      }).catch(err => {
        debug(err);
        return false;
      });

      if (createResult) {
        res.json({
          result: ResponseResultTypeGroup.SUCCESS,
          data: createResult,
        });
      } else {
        res.json({
          result: ResponseResultTypeGroup.FAIL,
          err: {
            message: 'DB 오류입니다..',
          },
        });
      }
    } else {
      res.json({
        result: ResponseResultTypeGroup.FAIL,
        err: {
          message: 'userId가 누락되었습니다.',
        },
      });
    }
  } else {
    res.json({
      result: 'fail',
      err: {
        message: '로그인 후 이용해 주세요.',
      },
    });
  }
});

app.put('/:userId/memos/:memoId', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const { memoId } = req.params;
  const { memoResId, memoType, memoClass, memoContent, memoReqMethod, memoStatus } = req.body;

  if (memoId) {
    const result = await AdminMemo.update(
      {
        memoResId,
        memoType,
        memoContent,
        memoClass,
        memoReqMethod,
        memoStatus,
      },
      { where: { memoId } },
    )
      .then(() => true)
      .catch(err => {
        debug(err);
        return false;
      });

    if (result) {
      res.json({
        result: ResponseResultTypeGroup.SUCCESS,
      });
    } else {
      res.json({
        result: ResponseResultTypeGroup.FAIL,
        err: { message: 'database error[1]' },
      });
    }
  } else {
    res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: { message: 'database error[2]' },
    });
  }
});

app.delete('/:userId/memos/:memoId', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const { userId, memoId } = req.params;
  if (userId && memoId) {
    const result = await AdminMemo.destroy({
      where: { memoId },
    }).catch(() => false);
    if (result) {
      res.json({
        result: 'success',
      });
    } else {
      res.json({
        result: 'fail',
        err: {
          message: 'Database error',
        },
      });
    }
  } else {
    res.json({
      result: 'fail',
      err: {
        message: 'invalid memoId',
      },
    });
  }
});

app.delete('/:userId', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const userId = req.params.userId;
  if (userId) {
    const result = await User.destroy({
      where: { userId },
    })
      .catch(() => false)
      .then(() => true);

    if (result) {
      res.json({
        result: 'success',
        message: `${userId} is deleted`,
      });
      await SyncSearchIndex(userId);
    } else {
      res.json({
        result: 'fail',
        message: 'Database Error',
      });
    }
  } else {
    res.json({
      result: 'fail',
      message: 'Invalid UserId',
    });
  }
});

app.post('/:userId/products', async (req, res) => {
  const { userId } = req.params;
  const { productId, paymentId, purchaseDate, expireDate, memo, productStatus } = req.body;

  if (userId) {
    const productInfo = await Product.findOne({
      where: { productId },
    }).catch(err => {
      debug(err);
      return false;
    });

    if (productInfo) {
      const createProductResult = await UserProduct.create({
        userId,
        productId,
        paymentId,
        purchaseDate,
        expireDate,
        memo,
        productStatus,
      }).catch(err => {
        debug(err);
        return false;
      });

      if (createProductResult) {
        const userApplyCount = await UserApplyCount.findOne({
          where: { userId },
        }).catch(err => {
          debug(err);
          return false;
        });

        const today = moment()
          .set(0, 'hours')
          .set(0, 'minutes')
          .set(0, 'seconds');
        if (moment(expireDate).diff(today, 'hours') > 0 && moment(purchaseDate).diff(today, 'hours') < 0) {
          if (userApplyCount) {
            const updateResult = await UserApplyCount.update(
              {
                userId,
                applyToParentCount: sequelize.literal(
                  `applyToParentCount + ${productInfo.productApplyToParentCount || 0}`,
                ),
                applyToSitterCount: sequelize.literal(
                  `applyToSitterCount + ${productInfo.productApplyToSitterCount || 0}`,
                ),
                lastUpdated: moment().format('YYYY-MM-DD HH:mm'),
              },
              {
                where: { applyCountId: userApplyCount.applyCountId },
              },
            ).catch(err => {
              debug(err);
              return false;
            });

            if (updateResult) {
              res.json({
                result: ResponseResultTypeGroup.SUCCESS,
                data: createProductResult,
              });
            } else {
              res.json({
                result: ResponseResultTypeGroup.FAIL,
                err: {
                  message: '기존에 있던 ApplyCount를 갱신하는데 오.',
                },
              });
            }
          } else {
            const createResult = await UserApplyCount.create({
              userId,
              applyToParentCount: productInfo.productApplyToParentCount,
              applyToSitterCount: productInfo.productApplyToSitterCount,
              lastUpdated: moment().format('YYYY-MM-DD HH:mm'),
            }).catch(err => {
              debug(err);
              return false;
            });

            if (createResult) {
              res.json({
                result: ResponseResultTypeGroup.SUCCESS,
                data: createProductResult,
              });
            } else {
              res.json({
                result: ResponseResultTypeGroup.FAIL,
                err: {
                  message: '새로운 APPLY COUNT를 만드는데 실패함',
                },
              });
            }
          }
        } else {
          res.json({
            result: ResponseResultTypeGroup.SUCCESS,
            data: createProductResult,
          });
        }
      } else {
        res.json({
          result: ResponseResultTypeGroup.FAIL,
          err: {
            message: '네트워크 오류입니다.',
          },
        });
      }
    } else {
      res.json({
        result: ResponseResultTypeGroup.FAIL,
        err: {
          message: '유효하지 않은 이용권정보 입니다.',
        },
      });
    }
  } else {
    res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: '필수정보 누락',
      },
    });
  }
});

export default app;
