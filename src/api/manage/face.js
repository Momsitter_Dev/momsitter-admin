import fs from 'fs';
import request from 'request-promise';
import Debug from 'debug';
import uuid from 'uuid/v4';
import express from 'express';
import {
  FaceAnalysis,
  UserProfileImage,
  UploadFiles,
} from '../../data/models/index';
import passport from '../../core/momsitter_passport';
import ResponseResultTypeGroup from '../../util/const/ResponseResultTypeGroup';

const debug = Debug('api');
const app = express.Router();

app.get('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const {
    requestPage,
    searchFilter,
    searchText,
    searchOrderColumn,
    searchOrderType,
  } = req.query;
  const searchCondition = {
    order: [],
    where: {},
  };
  const page = { maxPage: 0, currentPage: 0 };

  let offset = 0;
  if (requestPage && requestPage > 0) {
    offset = requestPage * 10;
  } else {
    offset = 0;
  }

  if (searchFilter && searchText) {
    if (searchFilter === 'resultSummary') {
      if (searchText === 'true') {
        searchCondition.where[searchFilter] = 1;
      } else {
        searchCondition.where[searchFilter] = 0;
      }
    } else {
      searchCondition.where[searchFilter] = searchText;
    }
  }

  if (searchOrderColumn && searchOrderType) {
    searchCondition.order = [[searchOrderColumn, searchOrderType]];
  }

  const users = await FaceAnalysis.findAndCountAll({
    offset,
    limit: 10,
    order: searchCondition.order,
    where: searchCondition.where,
    include: [{ model: UploadFiles, as: 'faceUpload' }],
  }).catch(err => {
    console.error(err);
    return false;
  });

  if (users) {
    page.maxPage = Math.ceil(users.count / 10);
    page.currentPage = Number(requestPage) || 0;

    res.json({
      result: 'success',
      data: users,
      page,
    });
  } else {
    res.json({
      result: 'fail',
      message: 'fetching error',
    });
  }
});

app.put('/:faceAnalysisId/status', async (req, res) => {
  const { faceAnalysisId } = req.params;
  const { status } = req.body;
  debug({ faceAnalysisId, status });
  if (faceAnalysisId && status) {
    const updateResult = await FaceAnalysis.update({
      status,
    }, {
      where: { faceAnalysisId }
    }).catch((err) => {
      debug(err);
      return false;
    });

    if (updateResult) {
      res.json({
        result: ResponseResultTypeGroup.SUCCESS
      });
    } else {
      res.json({
        result: ResponseResultTypeGroup.FAIL,
        err: {
          message: 'Update Fail',
        }
      });
    }
  } else {
    res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: 'Face Id not exist',
      }
    });
  }
});

export default app;
