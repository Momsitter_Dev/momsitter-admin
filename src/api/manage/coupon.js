import moment from 'moment';
import express from 'express';
import Debug from 'debug';
import { Coupon, CouponType } from '../../data/models/index';
import passport from '../../core/momsitter_passport';
import parseSearchOptions from '../../util/dataFormat/parseSearchOptions';
import parseOrderOption from '../../util/dataFormat/parseOrderOption';
import ResponseResultTypeGroup from '../../util/const/ResponseResultTypeGroup';
import parseOrderOptionRawQuery from '../../util/dataFormat/parseOrderOptionRawQuery';
import parseSearchOptionRawQuery from '../../util/dataFormat/parseSearchOptionRawQuery';
import CouponList from '../../data/query/CouponList';
import sequelize from '../../data/sequelize';
import ExportCSV from '../../util/ExportCSV';

moment.locale('ko');

const debug = Debug('api');
const app = express.Router();

app.get('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const {
      requestPage,
      searchOptions,
      orderOption,
    } = req.query;
    const page = { maxPage: 0, currentPage: 0 };

    let offset = 0;
    if (requestPage && requestPage > 0) {
      offset = requestPage * 10;
    } else {
      offset = 0;
    }

    const users = await Coupon.findAndCountAll({
      offset,
      limit: 10,
      order: parseOrderOption(orderOption),
      where: parseSearchOptions(searchOptions),
      include: [
        {
          model: CouponType,
          as: 'couponType',
        },
      ],
    }).catch(err => {
      console.error(err);
      return false;
    });

    if (!users) {
      return res.json({
        result: ResponseResultTypeGroup.FAIL,
        message: 'fetching error',
      });
    }

    page.maxPage = Math.ceil(users.count / 10);
    page.currentPage = Number(requestPage) || 0;

    return res.json({
      result: ResponseResultTypeGroup.SUCCESS,
      data: users,
      page,
    });
  } catch (err) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      message: 'fetching error',
    });
  }
});

app.get('/csv', async (req, res) => {
  const {
    searchOptions,
    orderOption,
  } = req.query;

  const where = parseSearchOptionRawQuery(searchOptions);
  const order = parseOrderOptionRawQuery(orderOption);
  const query = CouponList(where, order, '');
  const users = await sequelize.query(query, { type: sequelize.QueryTypes.SELECT });

  if (!users) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      message: 'fetching error',
    });
  }

  const fields = [
    'couponId',
    'userId',
    'userName',
    'userTypeId',
    'couponName',
    'couponTypeId',
    'pubDate',
    'useDate',
    'expireDate',
    'couponPrice',
  ];
  const fieldNames = [
    '쿠폰번호',
    '회원번호',
    '회원이름',
    '회원타입번호',
    '쿠폰이름',
    '쿠폰타입번호',
    '발급일자',
    '사용일자',
    '만료일자',
    '쿠폰금액',
  ];

  const exportResult = await ExportCSV({
    fields,
    fieldNames,
    data: users,
  }).catch((err) => {
    debug(err);
    return false;
  });

  if (!exportResult) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: 'CSV EXPORT FAIL',
      },
    });
  }

  const current = moment().format('YYYYMMDDhmm');
  res.header('Content-Disposition', `attachment; filename=${current}.csv`);
  res.type('text/csv');
  res.status(200).send(exportResult);
});

export default app;
