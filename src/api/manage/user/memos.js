import express from 'express';
import {
  User,
  UserRole,
  UserType,
  Admin,
  AdminMemo,
} from '../../../data/models/index';
import passport from '../../../core/momsitter_passport';
import ResponseResultTypeGroup from '../../../util/const/ResponseResultTypeGroup';
import parseSearchOptions from '../../../util/dataFormat/parseSearchOptions';
import parseOrderOption from '../../../util/dataFormat/parseOrderOption';

const app = express.Router();

app.get('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const {
    requestPage,
    searchOptions,
    orderOption,
  } = req.query;
  const page = { maxPage: 0, currentPage: 0 };

  let offset = 0;
  if (requestPage && requestPage > 0) {
    offset = requestPage * 10;
  } else {
    offset = 0;
  }

  const users = await AdminMemo.findAndCountAll({
    offset,
    limit: 10,
    order: parseOrderOption(orderOption),
    where: parseSearchOptions(searchOptions),
    include: [
      {
        model: User,
        as: 'memoUser',
        include: [
          {
            model: UserRole,
            as: 'userRole',
            include: [
              {
                model: UserType,
                as: 'userType'
              },
            ],
          },
        ],
      },
      { model: Admin, as: 'memoAdmin' },
    ],
  }).catch(err => {
    console.error(err);
    return false;
  });

  if (users) {
    page.maxPage = Math.ceil(users.count / 10);
    page.currentPage = Number(requestPage) || 0;

    res.json({
      result: ResponseResultTypeGroup.SUCCESS,
      data: users,
      page,
    });
  } else {
    res.json({
      result: ResponseResultTypeGroup.FAIL,
      message: 'fetching error',
    });
  }
});

export default app;
