import express from 'express';
import Debug from 'debug';
import moment from 'moment';
import {
  User,
  UserRole,
  UserType,
  Review,
} from '../../../data/models/index';
import passport from '../../../core/momsitter_passport';
import ExportCSV from '../../../util/ExportCSV';
import parseSearchOptions from '../../../util/dataFormat/parseSearchOptions';
import parseOrderOption from '../../../util/dataFormat/parseOrderOption';
import sequelize from '../../../data/sequelize';
import ReviewTypeGroup from '../../../util/const/ReviewTypeGroup';

const debug = Debug('api');
const app = express.Router();

app.get('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const {
    requestPage,
    searchOptions,
    orderOption,
  } = req.query;
  const page = { maxPage: 0, currentPage: 0 };

  let offset = 0;
  if (requestPage && requestPage > 0) {
    offset = requestPage * 10;
  } else {
    offset = 0;
  }

  const users = await Review.findAndCountAll({
    offset,
    limit: 10,
    order: parseOrderOption(orderOption),
    where: {
      reviewType: {
        [sequelize.Op.ne]: ReviewTypeGroup.UNFINISHED,
      },
      ...parseSearchOptions(searchOptions),
    },
    include: [
      {
        model: User,
        as: 'sender',
        include: [
          {
            model: UserRole,
            as: 'userRole',
            include: [{ model: UserType, as: 'userType' }]
          },
        ],
      },
      {
        model: User,
        as: 'target',
        include: [
          {
            model: UserRole,
            as: 'userRole',
            include: [{ model: UserType, as: 'userType' }]
          },
        ],
      },
    ],
  }).catch(err => {
    console.error(err);
    return false;
  });

  if (users) {
    page.maxPage = Math.ceil(users.count / 10);
    page.currentPage = Number(requestPage) || 0;

    res.json({
      result: 'success',
      data: users,
      page,
    });
  } else {
    res.json({
      result: 'fail',
      message: 'fetching error',
    });
  }
});

app.get('/csv', async (req, res) => {
  const {
    searchOptions,
    orderOption,
  } = req.query;

  const users = await Review.findAndCountAll({
    order: parseOrderOption(orderOption),
    where: parseSearchOptions(searchOptions),
    raw: true,
    attributes: [
      'reviewId',
      'applyId',
      'senderId',
      'targetId',
      'reviewRate',
      'reviewContent',
      'reviewType',
      [sequelize.fn('DATE_FORMAT', sequelize.col('writeDate'), '%Y-%m-%d %H:%i:%s'), 'writeDate'],
      'interviewWay',
      'status',
    ],
    include: [
      {
        model: User,
        as: 'sender',
        include: [
          {
            model: UserRole,
            as: 'userRole',
            include: [{ model: UserType, as: 'userType' }]
          }
        ],
      },
      {
        model: User,
        as: 'target',
        include: [
          {
            model: UserRole,
            as: 'userRole',
            include: [{ model: UserType, as: 'userType' }]
          },
        ],
      },
    ],
  }).catch(err => {
    console.error(err);
    return false;
  });

  if (users) {
    const fields = [
      'reviewId',
      'applyId',
      'senderId',
      'sender.userName',
      'sender.userRole.userType.userDetailTypeDesc',
      'targetId',
      'target.userName',
      'target.userRole.userType.userDetailTypeDesc',
      'reviewRate',
      'reviewType',
      'writeDate',
      'interviewWay',
      'status',
      'reviewContent',
    ];
    const fieldNames = [
      '후기번호',
      '신청 번호',
      '후기 작성자 번호',
      '후기 작성자 이름',
      '후기 작성자 회원 타입',
      '후기 대상 회원 번호',
      '후기 대상 회원 이름',
      '후기 대상 회원 타입',
      '평점',
      '후기 타입',
      '작성 일자',
      '인터뷰 방법',
      '후기 상태',
      '후기 내용',
    ];
    const exportResult = await ExportCSV({
      fields,
      fieldNames,
      data: users.rows,
    }).catch((err) => {
      debug(err);
      return false;
    });
    if (exportResult) {
      const current = moment(new Date()).locale('ko').format('YYYYMMDDhmm');
      res.header("Content-Disposition", "attachment; filename=" + current + ".csv");
      res.type("text/csv");
      res.send(200, exportResult);
    } else {
      res.json({
        result: 'fail',
        message: 'CSV EXPORT FAIL',
      });
    }
  } else {
    res.json({
      result: 'fail',
      message: 'fetching error',
    });
  }
});

app.put('/:reviewId', passport.authenticate('jwt', { session: false }), async (req, res) => {
  // TODO  reviewId 에 해당하는 리뷰 수정
  // TODO  수정 가능한 항목 확인필요
  debug('edit review');
  debug(req.params);
  debug(req.body);
  const { reviewId } = req.params;
  const { reviewContent, reviewRate } = req.body;
  if (reviewId) {
    const result = await Review.update(
      {
        reviewRate,
        reviewContent,
      },
      { where: { reviewId } },
    )
      .catch(() => false)
      .then(() => true);
    if (result) {
      res.json({
        result: 'success',
      });
    } else {
      res.json({
        result: 'fail',
        message: 'Database Error',
      });
    }
  } else {
    res.json({
      result: 'fail',
      message: 'Invalid reviewId',
    });
  }
});

app.put('/:reviewId/reviewType', passport.authenticate('jwt', { session: false }), async (req, res) => {
  // TODO  reviewId 에 해당하는 리뷰타입 수정
  // TODO  수정 가능한 항목 확인필요
  const { reviewId } = req.params;
  const { reviewType } = req.body;
  if (reviewId && reviewType) {
    const result = await Review.update({ reviewType }, { where: { reviewId } })
      .catch(() => false)
      .then(() => true);

    if (result) {
      res.json({
        result: 'success',
      });
    } else {
      res.json({
        result: 'fail',
        message: 'Database Error',
      });
    }
  } else {
    res.json({
      result: 'fail',
      message: 'Invalid reviewId',
    });
  }
});

app.put('/:reviewId/reviewStatus', passport.authenticate('jwt', { session: false }), async (req, res) => {
  // TODO  reviewId 에 해당하는 리뷰타입 수정
  // TODO  수정 가능한 항목 확인필요
  const { reviewId } = req.params;
  const { reviewStatus } = req.body;
  if (reviewId && reviewStatus) {
    const result = await Review.update(
      { status: reviewStatus },
      { where: { reviewId } },
    )
      .catch(() => false)
      .then(() => true);

    if (result) {
      res.json({
        result: 'success',
      });
    } else {
      res.json({
        result: 'fail',
        message: 'Database Error',
      });
    }
  } else {
    res.json({
      result: 'fail',
      message: 'Invalid reviewId',
    });
  }
});

app.delete('/:reviewId', passport.authenticate('jwt', { session: false }), async (req, res) => {
  // TODO reviewId 에 해당하는 리뷰 삭제
  const { reviewId } = req.params;
  // TODO ADMIN Timeline
  // TODO 반복적으로 사용되는 패턴 (아래와 같음)을 메소드로 분리해볼까나(같은 로직이 들어가는건 맞는데 계속반복되니까 귀찮음)
  if (reviewId) {
    const result = await Review.destroy({
      where: { reviewId },
    })
      .catch(() => false)
      .then(() => true);

    if (result) {
      res.json({
        result: 'success',
      });
    } else {
      res.json({
        result: 'fail',
        message: 'Database Error',
      });
    }
  } else {
    res.json({
      result: 'fail',
      message: 'Invalid reviewId',
    });
  }
});

export default app;
