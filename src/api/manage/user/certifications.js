/* eslint-disable prettier/prettier */
import Debug from 'debug';
import express from 'express';
import moment from 'moment';
import {
  User,
  UserRole,
  UserCertificate,
  CertificateLabel,
  UserType,
  UploadFiles,
  Point,
} from '../../../data/models/index';
import sequelize from '../../../data/sequelize';
import passport from '../../../core/momsitter_passport';
import CertificateTypeIdGroup from '../../../util/const/CertificateTypeIdGroup';
import CertificateStatusGroup from '../../../util/const/CertificateStatusGroup';
import UserTypeIdGroup from '../../../util/const/UserTypeIdGroup';
import FunctionResultTypeGroup from '../../../util/const/FunctionResultTypeGroup';
import ResponseResultTypeGroup from '../../../util/const/ResponseResultTypeGroup';
import SyncSearchIndex from '../../../util/SyncSearchIndex';
import ExportCSV from '../../../util/ExportCSV';
import parseSearchOptions from '../../../util/dataFormat/parseSearchOptions';
import parseOrderOption from '../../../util/dataFormat/parseOrderOption';
import CertDataParser from '../../../util/cert/CertDataParser';
import generateAndSaveLinkToken from '../../../util/message/generateAndSaveLinkToken';
import getLinkTokenPageURL from '../../../util/message/getLinkTokenPageURL';
import isParent from '../../../util/user/isParent';
import CertificateTypeNameGroup from '../../../util/const/CertificateTypeNameGroup';
import SendMessage from '../../../util/SendMessage';
import ATAMessageTemplateCode from '../../../util/const/ATAMessageTemplateCode';
import LMSMessageTemplateCode from '../../../util/const/LMSMessageTemplateCode';
import parseSearchOptionRawQuery from '../../../util/dataFormat/parseSearchOptionRawQuery';
import CountUserCertList from '../../../data/query/CountUserCertList';
import UserCertList from '../../../data/query/UserCertList';
import parseOrderOptionRawQuery from '../../../util/dataFormat/parseOrderOptionRawQuery';
import parseLimitOptionRawQuery from '../../../util/dataFormat/parseLimitOptionRawQuery';

moment.locale('ko');

const debug = Debug('api');
const app = express.Router();

const getExpireDate = () => {
  const base = moment();
  const month = base.get('month');
  return base
    .add(2, 'years')
    .set('month', month)
    .endOf('month')
    .format('YYYY-MM-DD HH:mm:ss');
};

/**
 * 회원의 첫 리뷰인지 확인
 * @param userId
 * @return {Promise<void>}
 */
const isFirstCertification = async userId => {
  try {
    const approvedCertificationCount = await UserCertificate.count({
      where: {
        userId,
        certStatus: CertificateStatusGroup.APPROVED,
      },
    });
    debug('approved cert count');
    debug(approvedCertificationCount);
    if (approvedCertificationCount === 0) {
      return true;
    }
    return false;
  } catch (err) {
    return false;
  }
};

const getReserveTime = () => moment().add(1, 'minutes');

const parseEnrollmentData = certData =>
  new Promise((resolve, reject) => {
    const result = {
      univName: '',
      univMajor: '',
      univStatus: '',
    };
    try {
      if (certData && certData.univ) {
        result.univName = certData.univ;
      }
      if (certData && certData.univName) {
        result.univName = certData.univName;
      }
      if (certData && certData.major) {
        result.univMajor = certData.major;
      }
      if (certData && certData.univStatus) {
        result.univStatus = certData.univStatus;
      }
      if (certData && certData.status) {
        if (String(certData.status) === '1') {
          result.univStatus = '재학';
        }
        if (String(certData.status) === '2') {
          result.univStatus = '휴학';
        }
        if (String(certData.status) === '3') {
          result.univStatus = '졸업';
        }
        if (String(certData.status) === '4') {
          result.univStatus = '퇴학';
        }
      }
      resolve({
        result: FunctionResultTypeGroup.DONE,
        data: result,
      });
    } catch (e) {
      reject('Parse Error');
    }
  });

const childCountToHangul = childCount =>
  new Promise((resolve, reject) => {
    try {
      const hangul = {
        '1': '한',
        '2': '두',
        '3': '세',
        '4': '네',
        '5': '다섯',
        '6': '여섯',
        '7': '일곱',
        '8': '여덣',
        '9': '아홉',
        '10': '열',
      }[String(childCount)];
      if (hangul) {
        resolve(hangul);
      } else {
        reject(false);
      }
    } catch (err) {
      reject(false);
    }
  });

app.get('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const { requestPage, searchOptions, orderOption } = req.query;
    const page = { maxPage: 0, currentPage: 0 };
    let offset = 0;
    if (requestPage && requestPage > 0) {
      offset = requestPage * 10;
    } else {
      offset = 0;
    }
    const where = parseSearchOptionRawQuery(searchOptions);
    const countQuery = CountUserCertList(where);
    const count = await sequelize.query(countQuery, { type: sequelize.QueryTypes.SELECT }).then(data => {
      return data[0]['CNT'];
    });
    const order = parseOrderOptionRawQuery(orderOption);
    const limit = parseLimitOptionRawQuery(offset);
    const query = UserCertList(where, order, limit);
    const users = await sequelize.query(query, { type: sequelize.QueryTypes.SELECT });
    page.maxPage = Math.ceil(count / 10);
    page.currentPage = Number(requestPage) || 0;
    return res.json({
      result: ResponseResultTypeGroup.SUCCESS,
      data: users,
      page,
    });
  } catch (err) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: err.message,
      },
    });
  }
});

app.get('/csv', async (req, res) => {
  try {
    const { searchOptions, orderOption } = req.query;

    const where = parseSearchOptionRawQuery(searchOptions);
    const countQuery = CountUserCertList(where);

    const count = await sequelize.query(countQuery, { type: sequelize.QueryTypes.SELECT }).then(data => {
      return data[0]['CNT'];
    });

    if (!count) {
      throw new Error("데이터가 없습니다.");
    }

    const order = parseOrderOptionRawQuery(orderOption);
    const query = UserCertList(where, order);
    const users = await sequelize.query(query, { type: sequelize.QueryTypes.SELECT });

    const fields = [
      'certId',
      'fileId',
      'certTypeId',
      'userId',
      'userName',
      'userDetailTypeDesc',
      'regDate',
      'certStatus',
      'rejectMsg',
      'issueDate',
      'updateDate',
      'certData',
    ];
    const fieldNames = [
      '인증 번호',
      '파일 번호',
      '인증 타입 번호',
      '인증 회원 번호',
      '인증 회원 이름',
      '인증 회원 타입',
      '등록 일자',
      '인증 상태',
      '반려 사유',
      '발급일자(효력일자)',
      '업데이트 일자',
      '인증 데이터(JSON)',
    ];
    const exportResult = await ExportCSV({
      fields,
      fieldNames,
      data: users,
    });

    if (!exportResult) {
      throw new Error("cannot extract csv");
    }
    const current = moment(new Date())
      .locale('ko')
      .format('YYYYMMDDhmm');
    res.header('Content-Disposition', `attachment; filename=${current}.csv`);
    res.type('text/csv');
    res.send(200, exportResult);
  } catch (err) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: err.message,
      },
    });
  }
});

app.put('/:certId/main/auto', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const { certId } = req.params;
    const { certStatus, certTypeId, userId, certData } = req.body;
    if (!certId) {
      throw new Error('invalid certId');
    }

    const user = await User.findOne({
      where: { userId },
    });

    const certInfo = await UserCertificate.findOne({
      where: { certId },
    });
    if (!certInfo.certId) {
      throw new Error('invalid certId');
    }

    const userRole = await UserRole.findAll({
      where: { userId },
    });
    if (userRole.length === 0) {
      throw new Error('role is empty');
    }

    const userTypeId = userRole[0].userTypeId;
    if (isParent(userTypeId)) {
      throw new Error('parent user not allowed');
    }

    let certLabel = null;
    let certDataParse = null;

    try {
      certDataParse = JSON.parse(certData);
      if (certTypeId === CertificateTypeIdGroup.ENROLLMENT) {
        const certEnrollmentData = await parseEnrollmentData(certDataParse).catch(err => {
          debug(err);
          return { result: FunctionResultTypeGroup.ERROR };
        });
        if (certEnrollmentData.result === FunctionResultTypeGroup.DONE) {
          certLabel = `${certEnrollmentData.data.univName} ${certEnrollmentData.data.univMajor} ${
            certEnrollmentData.data.univStatus
          }생`;
        }
      } else if (certTypeId === CertificateTypeIdGroup.FAMILY_RELATION) {
        const hangul = await childCountToHangul(certDataParse.children.length);
        certLabel = `${hangul} 아이의 엄마`;
      } else if (certTypeId === CertificateTypeIdGroup.CHILDCARE) {
        if (certDataParse.qualificationType) {
          certLabel = certDataParse.qualificationType;
        }
      } else {
        throw new Error('not implemented');
      }

      if (!certLabel) {
        throw new Error('not implemented');
      }
    } catch (error) {
      throw new Error('cannot create main label');
    }

    if (certStatus === CertificateStatusGroup.APPROVED) {
      const isFirstCert = await isFirstCertification(userId);
      await sequelize.transaction(async transaction => {
        await UserCertificate.update(
          {
            certStatus,
            updateDate: new Date(),
          },
          { where: { certId }, transaction },
        );

        await CertificateLabel.destroy({
          where: {
            certTypeId,
            userId,
          },
          transaction,
        });

        await CertificateLabel.create(
          {
            certTypeId,
            userId,
            certLabelName: certLabel,
          },
          { transaction },
        );

        await Point.create({
          pointTypeId: isFirstCert ? 6 : 7,
          userId,
          totalPoint: isFirstCert ? 2000 : 1000,
          remainPoint: isFirstCert ? 2000 : 1000,
          pubDate: new Date(),
          expireDate: getExpireDate(),
          read: 0,
        });
      });

      let link = '/my/point';
      try {
        const token = await generateAndSaveLinkToken(null, userId, link);
        link = getLinkTokenPageURL(token);
      } catch (e) {
        /* continue */
      }

      try {
        if (isFirstCert) {
          await SendMessage({
            templateCode: LMSMessageTemplateCode.SITTER_FIRST_CERT_APPROVED,
            receiverId: userId,
            to: user.userPhone, // TODO: userPhone
            params: {
              '#{url}': link,
            },
          });
        } else {
          await SendMessage({
            templateCode: LMSMessageTemplateCode.SITTER_CERT_APPROVED,
            receiverId: userId,
            to: user.userPhone, // TODO: userPhone
            params: {
              '#{인증이름}': CertificateTypeNameGroup[certTypeId],
              '#{url}': link,
            },
          });
        }
      } catch (e) {
        /* continue */
        console.log(e);
      }

      res.json({
        result: ResponseResultTypeGroup.SUCCESS,
        message: {
          send: true,
        },
      });
    } else if (certStatus === CertificateStatusGroup.REJECTED) {
      await SendMessage({
        templateCode: ATAMessageTemplateCode.SITTER_CERT_REJECTED,
        receiverId: userId,
        to: user.userPhone,
        params: {
          '#{text}': '', // TODO: 거절 사유가 들어가야함.
        },
      });

      await UserCertificate.update(
        {
          certStatus,
          updateDate: new Date(),
        },
        {
          where: { certId },
        },
      );

      res.json({
        result: ResponseResultTypeGroup.SUCCESS,
        message: {
          send: true,
        },
      });
    } else {
      await UserCertificate.update(
        {
          certStatus,
          updateDate: new Date(),
        },
        { where: { certId } },
      );

      res.json({
        result: ResponseResultTypeGroup.SUCCESS,
      });
    }
    SyncSearchIndex(userId);
  } catch (err) {
    console.log(err);
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: 'server error',
      },
    });
  }
});

app.put('/:certId/main', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const { certId } = req.params;
    const { certLabelName, certStatus, certTypeId, userId, certData } = req.body;

    if (!userId) {
      throw new Error('invalid userId');
    }

    const user = await User.findOne({
      where: { userId },
    });

    const isFirstCert = isFirstCertification(userId);

    if (certStatus === CertificateStatusGroup.APPROVED) {
      await sequelize.transaction(async transaction => {
        await UserCertificate.update(
          {
            certStatus,
            updateDate: new Date(),
          },
          {
            where: { certId },
            transaction,
          },
        );
        await CertificateLabel.destroy({
          where: {
            certTypeId,
            userId,
          },
          transaction,
        });
        await CertificateLabel.create(
          {
            certTypeId,
            userId,
            certLabelName,
          },
          { transaction },
        );

        await Point.create({
          userId,
          pointTypeId: isFirstCert ? 6 : 7,
          totalPoint: isFirstCert ? 2000 : 1000,
          remainPoint: isFirstCert ? 2000 : 1000,
          pubDate: new Date(),
          expireDate: getExpireDate(),
        });
      });

      try {
        let link = '/my/point';
        try {
          const token = await generateAndSaveLinkToken(null, userId, link);
          link = getLinkTokenPageURL(token);
        } catch (error) {
          debug(error);
        }

        if (isFirstCert) {
          await SendMessage({
            templateCode: LMSMessageTemplateCode.SITTER_FIRST_CERT_APPROVED,
            receiverId: userId,
            to: user.userPhone, // TODO: userPhone
            params: {
              '#{url}': link,
            },
          });
        } else {
          await SendMessage({
            templateCode: LMSMessageTemplateCode.SITTER_CERT_APPROVED,
            receiverId: userId,
            to: user.userPhone, // TODO: userPhone
            params: {
              '#{인증이름}': CertificateTypeNameGroup[certTypeId],
              '#{url}': link,
            },
          });
        }
      } catch (err) {
        debug(err);
      }
    } else if (certStatus === CertificateStatusGroup.REJECTED) {
      await UserCertificate.update(
        {
          certStatus,
          updateDate: new Date(),
        },
        {
          where: { certId },
        },
      );

      await SendMessage({
        templateCode: ATAMessageTemplateCode.SITTER_CERT_REJECTED,
        receiverId: userId,
        to: user.userPhone,
        params: {
          '#{text}': '', // TODO: 거절 사유가 들어가야함.
        },
      });
    } else {
      await UserCertificate.update(
        {
          certStatus,
          updateDate: new Date(),
        },
        {
          where: { certId },
        },
      );
    }
    await SyncSearchIndex(userId);
    return res.json({
      result: ResponseResultTypeGroup.SUCCESS,
    });
  } catch (err) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: 'server error',
      },
    });
  }
});

app.put('/:certId/status', passport.authenticate('jwt', { session: false }), async (req, res) => {
  // 메인 인증이 아닐경우.
  try {
    const { certId } = req.params;
    const { certStatus, userId, rejectMsg } = req.body;

    if (!(certId && userId && certStatus)) {
      throw new Error('Invalid parameters');
    }

    const user = await User.findOne({
      where: { userId },
    });

    const userRole = await UserRole.findAll({
      where: { userId },
    });

    if (!userRole) {
      throw new Error('Cannot select userRole');
    }

    const certInfo = await UserCertificate.findOne({
      where: { certId },
    });

    if (!certInfo) {
      throw new Error('Cannot select certInfo');
    }

    const userTypeId = userRole[0].userTypeId;
    if (userTypeId === UserTypeIdGroup.PARENT) {
      throw new Error('parent user is not allow to this function');
    }

    const message = {
      type: '',
      send: false,
    };

    if (certStatus === CertificateStatusGroup.APPROVED) {
      const isFirstCert = await isFirstCertification(userId);
      await sequelize.transaction(async transaction => {
        await UserCertificate.update(
          {
            certStatus,
            rejectMsg: '',
            updateDate: new Date(),
          },
          {
            where: { certId },
            transaction,
          },
        );
        // TODO: EXTRACT const
        await Point.create({
          pointTypeId: isFirstCert ? 6 : 7,
          userId,
          totalPoint: isFirstCert ? 2000 : 1000,
          remainPoint: isFirstCert ? 2000 : 1000,
          pubDate: new Date(),
          expireDate: getExpireDate(),
          read: 0,
        });
      });

      message.type = 'APPROVED';
      let link = '/my/point';
      try {
        const token = await generateAndSaveLinkToken(null, userId, link);
        link = getLinkTokenPageURL(token);
      } catch (err) {
        /* continue */
      }
      try {
        if (isFirstCert) {
          await SendMessage({
            templateCode: LMSMessageTemplateCode.SITTER_FIRST_CERT_APPROVED,
            receiverId: userId,
            to: user.userPhone, // TODO: userPhone
            params: {
              '#{url}': link,
            },
          });
        } else {
          await SendMessage({
            templateCode: LMSMessageTemplateCode.SITTER_CERT_APPROVED,
            receiverId: userId,
            to: user.userPhone, // TODO: userPhone
            params: {
              '#{인증이름}': CertificateTypeNameGroup[certInfo.certTypeId],
              '#{url}': link,
            },
          });
        }
        message.send = true;
      } catch (err) {
        message.send = false;
      }

      return res.json({
        result: ResponseResultTypeGroup.SUCCESS,
        message,
      });
    } else if (certStatus === CertificateStatusGroup.REJECTED) {
      await UserCertificate.update(
        {
          certStatus,
          updateDate: new Date(),
          rejectMsg,
        },
        { where: { certId } },
      );
      message.type = 'DECLINE';
      try {
        await SendMessage({
          templateCode: ATAMessageTemplateCode.SITTER_CERT_REJECTED,
          receiverId: userId,
          to: user.userPhone,
          params: {
            '#{text}': rejectMsg || '', // TODO: 거절 사유가 들어가야함.
          },
        });
        message.send = true;
      } catch (err) {
        message.send = false;
      }
      return res.json({
        result: ResponseResultTypeGroup.SUCCESS,
        message,
      });
    } else {
      // NOTE: 승인 / 거절이 아닌경우 데이터만 변경
      await UserCertificate.update(
        {
          certStatus,
          updateDate: new Date(),
          rejectMsg,
        },
        { where: { certId } },
      );
    }
    await SyncSearchIndex(userId);

    return res.json({ result: ResponseResultTypeGroup.SUCCESS });
  } catch (err) {
    debug(err);
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: 'certId is invalid',
      },
    });
  }
});

app.delete('/:certId', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const { certId } = req.params;
    if (!certId) {
      throw new Error('필수정보 누락');
    }
    const certInfo = await UserCertificate.findOne({
      where: { certId },
    });
    if (!certInfo) {
      throw new Error('유효하지 않은 인증정보');
    }
    await UserCertificate.destroy({
      where: { certId },
    });
    await SyncSearchIndex(certInfo.userId);
    return res.json({ result: ResponseResultTypeGroup.SUCCESS });
  } catch (err) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: err.message,
      },
    });
  }
});

app.get('/student', async (req, res) => {
  const query =
    'SELECT *\n' +
    '\tFROM (\n' +
    '\t\tSELECT ma.*, mc.certData, mc.certStatus\n' +
    '\t\t\tFROM momsitter_user_certificate AS mc\n' +
    '\t\t\tRIGHT JOIN (\n' +
    '\t\t\t\t\t\tSELECT wrap.*, mp.profileStatus\n' +
    '\t\t\t\t\t\t\tFROM momsitter_sitter_profile AS mp\n' +
    '\t\t\t\t\t\t\tRIGHT JOIN (\n' +
    '\t\t\t\t\t\t\t\t\tSELECT mw.*, mt.userDetailTypeDesc\n' +
    '\t\t\t\t\t\t\t\t\t\tFROM momsitter_user_type AS mt\n' +
    '\t\t\t\t\t\t\t\t\t\tJOIN (\n' +
    '\t\t\t\t\t\t\t\t\t\t\t\tSELECT mu.*, mr.userTypeId\n' +
    '\t\t\t\t\t\t\t\t\t\t\t\t\tFROM momsitter_user_role AS mr\n' +
    '\t\t\t\t\t\t\t\t\t\t\t\t\tJOIN (\n' +
    '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tSELECT userId, userKey, userName, userSignupDate, userBirthday, userAge, userGender, userConnectType, userPhone, userEmail, userStatus\n' +
    '\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tFROM momsitter_user\n' +
    '\t\t\t\t\t\t\t\t\t\t\t\t) AS mu\n' +
    '\t\t\t\t\t\t\t\t\t\t\t\tON mr.userId = mu.userId\n' +
    '\t\t\t\t\t\t\t\t\t\t) AS mw\n' +
    '\t\t\t\t\t\t\t\t\t\tON mt.userTypeId = mw.userTypeId\n' +
    '\t\t\t\t\t\t\t\t\t\tWHERE mw.userTypeId = 1\n' +
    '\t\t\t\t\t\t\t) AS wrap\n' +
    '\t\t\t\t\t\t\tON mp.userId = wrap.userId\n' +
    '\t\t\t) AS ma\n' +
    '\t\t\tON mc.userId = ma.userId\n' +
    '\t\t\tWHERE mc.certTypeId = 1\n' +
    '\t) AS mwrap\n' +
    '\tLEFT JOIN (\n' +
    '\t\t\tSELECT targetId, COUNT(targetId) AS reportCount\n' +
    '\t\t\t\tFROM momsitter_report\n' +
    '\t\t\t\tGROUP BY targetId\n' +
    '\t) AS mr\n' +
    '\tON mwrap.userId = mr.targetId\n';

  const result = await sequelize.query(query, {
    type: sequelize.QueryTypes.SELECT,
  });
  debug('KEYS');
  debug(Object.keys(result));
  if (result && result instanceof Array && result.length > 0) {
    for (let i = 0; i < result.length; i += 1) {
      const certData = CertDataParser(result[i].certData);
      if (certData) {
        result[i]._univ = certData.univ;
        result[i]._major = certData.major;
        result[i]._degree = certData.degree;
        result[i]._grade = certData.grade;
        result[i]._status = certData.status;
        result[i]._issueDate = certData.issueDate;
      } else {
        result[i]._univ = '';
        result[i]._major = '';
        result[i]._degree = '';
        result[i]._grade = '';
        result[i]._status = '';
        result[i]._issueDate = '';
      }
    }
    const fields = [
      'userId',
      'userKey',
      'userName',
      'userSignupDate',
      'userBirthday',
      'userAge',
      'userGender',
      'userConnectType',
      'userPhone',
      'userEmail',
      'userStatus',
      'userTypeId',
      'userDetailTypeDesc',
      'profileStatus',
      'certData',
      'reportCount',
      '_univ',
      '_major',
      '_degree',
      '_grade',
      '_status',
      '_issueDate',
    ];
    const fieldNames = [
      '회원 번호',
      '회원 로그인 키',
      '회원 이름',
      '회원 가입 일자',
      '생년 월일',
      '나이',
      '성별',
      '가입 방법',
      '핸드폰 번호',
      '이메일',
      '계정 상태',
      '회원 타입 번호',
      '회원 타입',
      '프로필 상태',
      '인증 데이터 원본',
      '신고 받은 횟수',
      '학교명',
      '학과명',
      '학위',
      '학년',
      '학사상태',
      '발급일자',
    ];
    const exportResult = await ExportCSV({
      fields,
      fieldNames,
      data: result,
    }).catch(err => {
      debug(err);
      return false;
    });
    if (exportResult) {
      const current = moment(new Date())
        .locale('ko')
        .format('YYYYMMDDhmm');
      res.header('Content-Disposition', `attachment; filename=${current}.csv`);
      res.type('text/csv');
      res.send(200, exportResult);
    } else {
      res.json({
        result: ResponseResultTypeGroup.FAIL,
        err: {
          message: 'CSV EXPORT FAIL',
        },
      });
    }
  } else {
    res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: '',
      },
    });
  }
});

export default app;
