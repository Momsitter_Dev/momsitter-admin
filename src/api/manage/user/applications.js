import express from 'express';
import Debug from 'debug';
import moment from 'moment';
import {
  User,
  UserRole,
  UserType,
  Apply
} from '../../../data/models/index';
import passport from '../../../core/momsitter_passport';
import ExportCSV from '../../../util/ExportCSV';

const debug = Debug('api');
const app = express.Router();

app.get('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const {
    requestPage,
    searchFilter,
    searchText,
  } = req.query;
  const searchCondition = {
    order: [],
    where: {},
  };
  const page = { maxPage: 0, currentPage: 0 };

  let offset = 0;
  if (requestPage && requestPage > 0) {
    offset = requestPage * 10;
  } else {
    offset = 0;
  }

  if (searchFilter && searchText) {
    searchCondition.where[searchFilter] = searchText;
  }

  const users = await Apply.findAndCountAll({
    offset,
    limit: 10,
    order: [['applyDate', 'DESC']],
    where: searchCondition.where,
    include: [
      {
        model: User,
        as: 'applicant',
        include: [
          {
            model: UserRole,
            as: 'userRole',
            include: [{ model: UserType, as: 'userType' }],
          }
        ],
      },
      {
        model: User,
        as: 'receiver',
        include: [
          {
            model: UserRole,
            as: 'userRole',
            include: [{ model: UserType, as: 'userType' }],
          }
        ],
      },
    ],
  }).catch(err => {
    console.error(err);
    return false;
  });

  if (users) {
    page.maxPage = Math.ceil(users.count / 10);
    page.currentPage = Number(requestPage) || 0;

    res.json({
      result: 'success',
      data: users,
      page,
    });
  } else {
    res.json({
      result: 'fail',
      message: 'fetching error',
    });
  }
});

app.get('/csv', async (req, res) => {
  const {
    searchFilter,
    searchText,
    searchOrderColumn,
    searchOrderType,
  } = req.query;
  const searchCondition = {
    order: [],
    where: {},
  };

  if (searchFilter && searchText) {
    searchCondition.where[searchFilter] = searchText;
  }

  if (searchOrderColumn && searchOrderType) {
    searchCondition.order = [[searchOrderColumn, searchOrderType]];
  }

  const users = await Apply.findAndCountAll({
    raw: true,
    order: searchCondition.order,
    where: searchCondition.where,
    include: [
      {
        model: User,
        as: 'applicant',
        include: [
          {
            model: UserRole,
            as: 'userRole',
            include: [{ model: UserType, as: 'userType' }],
          }
        ],
      },
      {
        model: User,
        as: 'receiver',
        include: [
          {
            model: UserRole,
            as: 'userRole',
            include: [{ model: UserType, as: 'userType' }],
          }
        ],
      },
    ],
  }).catch(err => {
    console.error(err);
    return false;
  });

  if (users) {
    const fields = [
      'applyId',
      'applicantId',
      'applicant.userName',
      'applicant.userRole.userType.userDetailTypeDesc',
      'receiverId',
      'receiver.userName',
      'receiver.userRole.userType.userDetailTypeDesc',
      'applyType',
      'applyStatus',
      'declineReason',
      'tuningReason',
      'applyDate',
      'readDate',
      'responseDate',
    ];
    const fieldNames = [
      '회원번호',
      '지원자 번호',
      '지원자 이름',
      '지원자 회원 타입',
      '지원 받은 사람 번호',
      '지원 받은 사람 이름',
      '지원 받은 사람 회원 타입',
      '지원 타입',
      '지원 상태',
      '거절 사유',
      '조율 사유',
      '지원 일자',
      '신청서 확인 일자',
      '응답 일자',
    ];
    const exportResult = await ExportCSV({
      fields,
      fieldNames,
      data: users.rows,
    }).catch((err) => {
      debug(err);
      return false;
    });
    if (exportResult) {
      const current = moment(new Date()).locale('ko').format('YYYYMMDDhmm');
      res.header("Content-Disposition", "attachment; filename=" + current + ".csv");
      res.type("text/csv");
      res.send(200, exportResult);
    } else {
      res.json({
        result: 'fail',
        message: 'CSV EXPORT FAIL',
      });
    }
  } else {
    res.json({
      result: 'fail',
      message: 'fetching error',
    });
  }
});



export default app;
