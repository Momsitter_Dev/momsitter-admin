/* eslint-disable prettier/prettier */
import Debug from 'debug';
import moment from 'moment';
import express from 'express';
import passport from '../../../core/momsitter_passport';
import ResponseResultTypeGroup from '../../../util/const/ResponseResultTypeGroup';
import PaymentList from '../../../data/query/PaymentList';
import parseOrderOptionRawQuery from '../../../util/dataFormat/parseOrderOptionRawQuery';
import parseSearchOptionRawQuery from '../../../util/dataFormat/parseSearchOptionRawQuery';
import sequelize from '../../../data/sequelize';
import ExportCSV from '../../../util/ExportCSV';
import CountPaymentList from '../../../data/query/CountPaymentList';
import parseLimitOptionRawQuery from '../../../util/dataFormat/parseLimitOptionRawQuery';

const debug = Debug('api');
const app = express.Router();

moment.locale('ko');

app.get('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
  try {
    const {
      requestPage,
      searchOptions,
      orderOption,
    } = req.query;
    const page = { maxPage: 0, currentPage: 0 };

    let offset = 0;
    if (requestPage && requestPage > 0) {
      offset = requestPage * 10;
    } else {
      offset = 0;
    }

    const where = parseSearchOptionRawQuery(searchOptions);
    const countQuery = CountPaymentList(where);
    const count = await sequelize.query(countQuery, { type: sequelize.QueryTypes.SELECT }).then((data) => {
      return data[0]['CNT'];
    }).catch((err) => {
      debug(err);
      return false;
    });

    if (!count) {
      return res.json({
        result: ResponseResultTypeGroup.SUCCESS,
        data: [],
        page: {
          maxPage: 0,
          currentPage: 0,
        },
      });
    }

    if (count === 0) {
      return res.json({
        result: ResponseResultTypeGroup.SUCCESS,
        data: [],
        page: {
          maxPage: 0,
          currentPage: 0,
        },
      });
    }

    const order = parseOrderOptionRawQuery(orderOption);
    const limit = parseLimitOptionRawQuery(offset);
    const query = PaymentList(where, order, limit);
    const users = await sequelize.query(query, { type: sequelize.QueryTypes.SELECT });

    page.maxPage = Math.ceil(count / 10);
    page.currentPage = Number(requestPage) || 0;
    return res.json({
      result: ResponseResultTypeGroup.SUCCESS,
      data: users,
      page,
    });
  } catch (err) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      message: 'fetching error',
    });
  }
});

app.get('/csv', async (req, res) => {
  try {
    const {
      searchOptions,
      orderOption,
    } = req.query;

    const where = parseSearchOptionRawQuery(searchOptions);
    const order = parseOrderOptionRawQuery(orderOption);
    const query = PaymentList(where, order, '');
    const data = await sequelize.query(query, { type: sequelize.QueryTypes.SELECT });

    const fields = [
      'userId',
      'userName',
      'userPhone',
      'userTypeId',
      'paymentId',
      'paymentStatus',
      'paymentRequestDate',
      'paymentDoneDate',
      'paymentPrice',
      'couponDiscount',
      'pointDiscount',
      'card_name',
      'productCode',
      'productName',
      'productPrice',
      'productFor',
      'productPeriod',
    ];
    const fieldNames = [
      '회원번호',
      '회원이름',
      '핸드폰번호',
      '회원타입번호',
      '결제번호',
      '결제상태',
      '결제요청일자',
      '결제완료일자',
      '결제금액',
      '쿠폰 할인금액',
      '적립금 할인금액',
      '결제 카드',
      '상품코드',
      '상품이름',
      '상품가격',
      '상품대상',
      '상품기간(일)',
    ];

    const exportResult = await ExportCSV({
      fields,
      fieldNames,
      data,
    }).catch((err) => {
      debug(err);
      return false;
    });

    if (!exportResult) {
      throw new Error('cannot export');
    }

    const current = moment().format('YYYYMMDDhmm');
    res.header('Content-Disposition', `attachment; filename=${current}.csv`);
    res.type('text/csv');
    return res.status(200).send(exportResult);
  } catch (err) {
    console.log(err);
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      message: 'server error',
    });
  }
});

export default app;
