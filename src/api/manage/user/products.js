import moment from 'moment';
import express from 'express';
import Debug from 'debug';
import {
  User,
  UserRole,
  UserType,
  UserProduct,
  UserApplyCount,
  Product,
} from '../../../data/models/index';
import sequelize from '../../../data/sequelize';
import passport from '../../../core/momsitter_passport';
import ResponseResultTypeGroup from '../../../util/const/ResponseResultTypeGroup';
import ExportCSV from '../../../util/ExportCSV';
import parseSearchOptions from '../../../util/dataFormat/parseSearchOptions';
import parseOrderOption from '../../../util/dataFormat/parseOrderOption';

const debug = Debug('api');
const app = express.Router();

moment.locale('ko');

app.get('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const {
    requestPage,
    searchOptions,
    orderOption,
  } = req.query;
  const page = { maxPage: 0, currentPage: 0 };

  let offset = 0;
  if (requestPage && requestPage > 0) {
    offset = requestPage * 10;
  } else {
    offset = 0;
  }

  const users = await UserProduct.findAndCountAll({
    offset,
    limit: 10,
    order: parseOrderOption(orderOption),
    where: parseSearchOptions(searchOptions),
    include: [
      {
        model: User,
        as: 'productUser',
        include: [
          {
            model: UserRole,
            as: 'userRole',
            include: { model: UserType, as: 'userType' },
          },
          {
            model: UserApplyCount,
            as: 'userApplyCount',
          }
        ]
      },
      {
        model: Product,
        as: 'product',
      }
    ],
  }).catch(err => {
    console.error(err);
    return false;
  });

  if (!users) {
    res.json({
      result: 'fail',
      message: 'fetching error',
    });
    return false;
  }

  page.maxPage = Math.ceil(users.count / 10);
  page.currentPage = Number(requestPage) || 0;

  res.json({
    result: 'success',
    data: users,
    page,
  });
  return false;
});

app.get('/csv', async (req, res) => {
  const {
    searchOptions,
    orderOption,
  } = req.query;

  const users = await UserProduct.findAndCountAll({
    raw: true,
    attributes: [
      'userProductId',
      'userId',
      'productId',
      'paymentId',
      [sequelize.fn('DATE_FORMAT', sequelize.col('purchaseDate'), '%Y-%m-%d %H:%i:%s'), 'purchaseDate'],
      [sequelize.fn('DATE_FORMAT', sequelize.col('expireDate'), '%Y-%m-%d %H:%i:%s'), 'expireDate'],
      'memo',
      'productStatus',
    ],
    order: parseOrderOption(orderOption),
    where: parseSearchOptions(searchOptions),
    include: [
      {
        model: User,
        as: 'productUser',
        include: [
          {
            model: UserRole,
            as: 'userRole',
            include: { model: UserType, as: 'userType' },
          },
          {
            model: UserApplyCount,
            as: 'userApplyCount',
          }
        ]
      },
      {
        model: Product,
        as: 'product',
      }
    ],
  }).catch(err => {
    console.error(err);
    return false;
  });

  if (!users) {
    res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: 'fetching error',
      },
    });
    return false;
  }
  const fields = [
    'userProductId',
    'userId',
    'productUser.userName',
    'productUser.userRole.userType.userDetailTypeDesc',
    'product.productCode',
    'product.productName',
    'productId',
    'purchaseDate',
    'expireDate',
    'memo',
    'productStatus',
  ];
  const fieldNames = [
    '회원 이용권 번호',
    '회원 번호',
    '회원 이름',
    '회원 타입',
    '이용권 번호',
    '이용권 코드',
    '이용권 이름',
    '구매 일자',
    '만료 일자',
    '메모',
    '이용권 상태',
  ];
  const exportResult = await ExportCSV({
    fields,
    fieldNames,
    data: users.rows,
  }).catch((err) => {
    debug(err);
    return false;
  });
  if (!exportResult) {
    res.json({
      result: 'fail',
      message: 'CSV EXPORT FAIL',
    });
    return false;
  }
  const current = moment(new Date()).locale('ko').format('YYYYMMDDhmm');
  res.header("Content-Disposition", "attachment; filename=" + current + ".csv");
  res.type("text/csv");
  res.send(200, exportResult);
  return false;
});

app.put('/:userProductId', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const { userProductId } = req.params;
  const {
    productId,
    paymentId,
    purchaseDate,
    expireDate,
    memo,
    productStatus,
    userId,
    applyCount,
  } = req.body;

  if (!userProductId) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: '필수정보 누락입니다.'
      }
    });
  }

  const userProductInfo = await UserProduct.findOne({
    where: { userProductId },
  }).then((data) => {
    if (!data || !(data.productId)) {
      return new Error(`UserProductInfo is NULL ${userProductId}`);
    }
    return data;
  }).catch((err) => {
    debug(err);
    return new Error(err);
  });

  if (userProductInfo instanceof Error) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: '필수정보 누락입니다.'
      }
    });
  }

  const productInfo = await Product.findOne({
    where: { productId },
  }).then((data) => {
    if (!data) {
      return new Error(`Product is NULL ${productId}`);
    }
    return data;
  }).catch((err) => {
    debug(err);
    return new Error(err);
  });

  if (productInfo instanceof Error) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: '이용권 정보를 찾을수 없습니다.',
      },
    });
  }

  let applyCountQuery = {};
  if (applyCount) {
    if (productInfo.productFor === 'sitter') {
      applyCountQuery.applyToParentCount = Number(applyCount);
    } else {
      applyCountQuery.applyToSitterCount = Number(applyCount);
    }
  }

  const updateTransaction = await sequelize.transaction((transaction) => {
    return UserProduct.update({
      productId,
      paymentId,
      purchaseDate,
      expireDate,
      memo,
      productStatus,
    }, {
      where: { userProductId },
      transaction,
    }).then(() => {
      return UserApplyCount.upsert({
        userId,
        lastUpdated: new Date(),
        ...applyCountQuery,
      },  {
        transaction,
      }).catch((err) => {
        throw new Error(err);
      });
    })
  }).then(() => {
    return true;
  }).catch((err) => {
    return new Error(err);
  });

  if (updateTransaction instanceof Error) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: '트랜잭션 오류.'
      }
    });
  }

  return res.json({ result: ResponseResultTypeGroup.SUCCESS });
});

app.delete('/:userProductId', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const { userProductId } = req.params;
  if (!userProductId) {
    res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: '필수정보가 누락되었습니다.',
      }
    });
    return false;
  }

  const userProductDeleteResult = await UserProduct.destroy({
    where: { userProductId }
  }).catch((err) => {
    debug(err);
  });

  if (!userProductDeleteResult) {
    res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: 'DB 오류입니다.',
      }
    });
    return false;
  }

  res.json({
    result: ResponseResultTypeGroup.SUCCESS,
  });
  return false;
});

export default app;
