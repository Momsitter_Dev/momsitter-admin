import express from 'express';
import moment from 'moment';
import Debug from 'debug';
import {
  User,
  UserRole,
  UserType,
  Report
} from '../../../data/models/index';
import passport from '../../../core/momsitter_passport';
import ExportCSV from '../../../util/ExportCSV';
import parseSearchOptions from '../../../util/dataFormat/parseSearchOptions';
import parseOrderOption from '../../../util/dataFormat/parseOrderOption';
import ResponseResultTypeGroup from '../../../util/const/ResponseResultTypeGroup';

const debug = Debug('api');
const app = express.Router();

app.get('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const {
    requestPage,
    searchOptions,
    orderOption,
  } = req.query;
  const page = { maxPage: 0, currentPage: 0 };

  let offset = 0;
  if (requestPage && requestPage > 0) {
    offset = requestPage * 10;
  } else {
    offset = 0;
  }

  const users = await Report.findAndCountAll({
    offset,
    limit: 10,
    order: parseOrderOption(orderOption),
    where: parseSearchOptions(searchOptions),
    include: [
      {
        model: User,
        as: 'reportUser',
        include: [
          {
            model: UserRole,
            as: 'userRole',
            include: [{ model: UserType, as: 'userType' }],
          }
        ],
      },
      {
        model: User,
        as: 'reportTargetUser',
        include: [
          {
            model: UserRole,
            as: 'userRole',
            include: [{ model: UserType, as: 'userType' }],
          }
        ],
      },
    ],
  }).catch(err => {
    console.error(err);
    return false;
  });

  if (!users) {
    res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: 'fetching error'
      },
    });
    return false;
  }
  page.maxPage = Math.ceil(users.count / 10);
  page.currentPage = Number(requestPage) || 0;

  res.json({
    result: ResponseResultTypeGroup.SUCCESS,
    data: users,
    page,
  });
  return false;
});

app.get('/csv', async (req, res) => {
  const {
    searchOptions,
    orderOption,
  } = req.query;

  const users = await Report.findAndCountAll({
    order: parseOrderOption(orderOption),
    where: parseSearchOptions(searchOptions),
    raw: true,
    include: [
      {
        model: User,
        as: 'reportUser',
        include: [
          {
            model: UserRole,
            as: 'userRole',
            include: [{ model: UserType, as: 'userType' }],
          }
        ],
      },
      {
        model: User,
        as: 'reportTargetUser',
        include: [
          {
            model: UserRole,
            as: 'userRole',
            include: [{ model: UserType, as: 'userType' }],
          }
        ],
      },
    ],
  }).catch(err => {
    console.error(err);
    return false;
  });


  if (!users) {
    res.json({
      result: 'fail',
      message: 'fetching error',
    });
  }

  const fields = [
    'reportId',
    'reporterId',
    'reportUser.userName',
    'reportUser.userRole.userType.userDetailTypeDesc',
    'targetId',
    'reportTargetUser.userName',
    'reportTargetUser.userRole.userType.userDetailTypeDesc',
    'reportType',
    'reportDate',
    'reportContent',
  ];
  const fieldNames = [
    '신고 번호',
    '신고 작성자 번호',
    '신고 작성자 이름',
    '신고 작성자 회원 타입',
    '신고 대상 회원 번호',
    '신고 대상 회원 이름',
    '신고 대상 회원 타입',
    '신고 타입',
    '신고 작성 일자',
    '신고 내용',
  ];
  const exportResult = await ExportCSV({
    fields,
    fieldNames,
    data: users.rows,
  }).catch((err) => {
    debug(err);
    return false;
  });
  if (!exportResult) {
    res.json({
      result: 'fail',
      message: 'CSV EXPORT FAIL',
    });
    return false;
  }
  const current = moment(new Date()).locale('ko').format('YYYYMMDDhmm');
  res.header("Content-Disposition", "attachment; filename=" + current + ".csv");
  res.type("text/csv");
  res.send(200, exportResult);
  return false;
});

app.put('/:reportId', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const { reportId } = req.params;
  const { reportType, reportContent } = req.body;

  if (!reportId) {
    res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: { message: 'Invalid ReportId' },
    });
    return false;
  }
  const result = await Report.update(
    {
      reportType,
      reportContent,
    },
    {
      where: { reportId },
    },
  ).then(() => true)
  .catch(() => false);

  if (!result) {
    res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: { message: 'update fail' }
    });
    return false;
  }
  res.json({ result: ResponseResultTypeGroup.SUCCESS });
  return false;
});

app.delete('/:reportId', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const { reportId } = req.params;
  if (!reportId) {
    res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: { message: 'Invalid ReportId' },
    });
    return false;
  }
  const result = Report.destroy({
    where: { reportId },
  }).then(() => true)
    .catch(() => false);

  if (!result) {
    res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: { message: 'delete fail' },
    });
    return false;
  }
  res.json({ result: ResponseResultTypeGroup.SUCCESS });
  return false;
});

export default app;
