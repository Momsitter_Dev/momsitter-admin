/* eslint-disable prettier/prettier */
import moment from 'moment';
import Debug from 'debug';
import express from 'express';
import passport from '../../../core/momsitter_passport';
import { User, UserRole, UserType, UserOutRequest, UserSnapshot } from '../../../data/models/index';
import ResponseResultTypeGroup from '../../../util/const/ResponseResultTypeGroup';
import parseSearchOptions from '../../../util/dataFormat/parseSearchOptions';
import parseOrderOption from '../../../util/dataFormat/parseOrderOption';
import UserOutRequestStatusTypeGroup from '../../../util/const/UserOutRequestStatusTypeGroup';
import ConstValidator from '../../../util/ConstValidator';
import sequelize from '../../../data/sequelize';
import OutRequestList from '../../../data/query/OutRequestList';
import parseSearchOptionRawQuery from '../../../util/dataFormat/parseSearchOptionRawQuery';
import parseOrderOptionRawQuery from '../../../util/dataFormat/parseOrderOptionRawQuery';
import ExportCSV from '../../../util/ExportCSV';

const debug = Debug('api');
const app = express.Router();

app.get('/', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const {
    requestPage,
    searchOptions,
    orderOption,
  } = req.query;
  const page = { maxPage: 0, currentPage: 0 };

  let offset = 0;
  if (requestPage && requestPage > 0) {
    offset = requestPage * 10;
  } else {
    offset = 0;
  }

  const users = await UserOutRequest.findAndCountAll({
    offset,
    limit: 10,
    order: parseOrderOption(orderOption),
    where: parseSearchOptions(searchOptions),
    include: [
      {
        model: User,
        as: 'outRequestUser',
        include: [
          {
            model: UserRole,
            as: 'userRole',
            include: [
              {
                model: UserType,
                as: 'userType'
              },
            ],
          },
        ],
      },
    ],
  }).catch(err => {
    debug(err);
    return false;
  });

  if (!users) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      message: 'fetching error',
    });
  }

  page.maxPage = Math.ceil(users.count / 10);
  page.currentPage = Number(requestPage) || 0;

  return res.json({
    result: ResponseResultTypeGroup.SUCCESS,
    data: users,
    page,
  });
});

app.get('/csv', async (req, res) => {
  try {
    const {
      searchOptions,
      orderOption,
    } = req.query;

    const where = parseSearchOptionRawQuery(searchOptions);
    const order = parseOrderOptionRawQuery(orderOption);
    const query = OutRequestList(where, order);
    const data = await sequelize.query(query, { type: sequelize.QueryTypes.SELECT });

    const fields = [
      'userId',
      'userName',
      'userStatus',
      'userGender',
      'userSignupDate',
      'userBirthDay',
      'userPhone',
      'outReason',
      'status',
      'regDate',
      'doneDate',
      'userTypeId',
      'sitterProfileStatus',
      'parentProfileStatus',
    ];
    const fieldNames = [
      '회원번호',
      '회원이름',
      '회원상태',
      '성별',
      '회원가입일자',
      '생년월일',
      '핸드폰번호',
      '탈퇴사유',
      '탈퇴상태',
      '탈퇴신청일',
      '탈퇴처리일자',
      '회원타입번호',
      '시터 프로필상태',
      '부모 프로필 상태',
    ];

    const exportResult = await ExportCSV({
      fields,
      fieldNames,
      data,
    });

    const current = moment().format('YYYYMMDDhmm');
    res.header('Content-Disposition', `attachment; filename=${current}.csv`);
    res.type('text/csv');
    res.status(200).send(exportResult);
  } catch (err) {
    console.log(err);
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: '',
      },
    });
  }
  return res.json
});

app.put('/:userId/status', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const userId = req.params.userId;
  const status = await ConstValidator(UserOutRequestStatusTypeGroup, req.body.status).catch((err) => {
    return err;
  });

  if (!userId) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      message: 'invalid user',
    });
  }

  if (status instanceof Error) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      message: 'invalid parameter',
    });
  }

  if (status !== UserOutRequestStatusTypeGroup.REQUEST) {
    const userInfo = await User.findOne({
      where: {userId}
    }).catch((err) => {
      return new Error(err);
    });

    if (userInfo instanceof Error) {
      return res.json({
        result: ResponseResultTypeGroup.FAIL,
        message: 'user data not found',
      });
    }

    const snapshotSaveResult = await sequelize.transaction((transaction) => {
      return UserSnapshot.destroy({
        where: { userId },
        transaction,
      }).then(() => {
        return UserSnapshot.create({
          userId,
          snapshotData: JSON.stringify({
            user: userInfo,
          }),
          regDate: new Date(),
        }, { transaction }).catch((err) => {
          throw new Error(err);
        });
      }).catch((err) => {
        throw new Error(err);
      })
    }).then(() => {
      return true;
    }).catch((err) => {
      debug(err);
      return new Error(error);
    });

    if (snapshotSaveResult instanceof Error) {
      return res.json({
        result: ResponseResultTypeGroup.FAIL,
        message: 'cannot create snapshot',
      });
    }
  }

  const updateResult = await UserOutRequest.update({
    status,
    doneDate: (status !== UserOutRequestStatusTypeGroup.REQUEST) ? new Date() : null,
  }, {
    where: { userId }
  }).catch((err) => {
    return err;
  });

  if (updateResult instanceof Error) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      message: 'update fail',
    });
  }

  if (status === UserOutRequestStatusTypeGroup.CONFIRMED) {
    const updateUserDup = await User.update({
      userDup: sequelize.literal(`CONCAT(userDup, ${sequelize.escape(moment().format('_YYYYMMDDHHmmss'))})`),
    }, {
      where: { userId },
    }).catch((err) => {
      debug(err);
      return false;
    });

    if (!updateUserDup) {
      return res.json({
        result: ResponseResultTypeGroup.FAIL,
        message: 'update userDup fail',
      });
    }
  }

  return res.json({
    result: ResponseResultTypeGroup.SUCCESS,
  });
});

app.delete('/:id/delete', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const id = req.params.id;
  if (!id) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      message: 'err',
    });
  }

  const result = await UserOutRequest.destroy({
    where: { userOutRequestId: id },
  }).catch((err) => {
    return new Error(err);
  });

  debug(result);

  if (result instanceof Error) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: 'DB error',
    });
  }

  return res.json({ result: ResponseResultTypeGroup.SUCCESS });
});

app.put('/:userId/cancel', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const userId = req.params.userId;

  if (!userId) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: '필수정보 누락',
      }
    });
  }

  const data = await UserOutRequest.findOne({
    where: { userId }
  }).then((data) => {
    if (!data.userOutRequestId) {
      return new Error('fetch but no-data');
    }
    return data;
  }).catch((err) => {
    debug(err);
    return new Error(err);
  });

  if (data instanceof Error) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: '정보없음',
      }
    });
  }

  if (data.status === UserOutRequestStatusTypeGroup.CONFIRMED) {
    const userInfo = await User.findOne({
      where: { userId },
    }).catch((err) => {
      return new Error(err);
    });

    if (userInfo instanceof Error) {
      return res.json({
        result: ResponseResultTypeGroup.FAIL,
        err: {
          message: 'UserInfo is NULL',
        }
      });
    }

    const userDupSplit = userInfo.userDup.split('_');
    if (userDupSplit.length < 2) {
      return res.json({
        result: ResponseResultTypeGroup.FAIL,
        err: {
          message: 'invalid userDup format',
        },
      });
    }

    const existUser = await User.findOne({
      where: { userDup: userDupSplit[0] },
    }).then(() => {

    }).catch((err) => {
      return new Error(err);
    });
  }

  if (data.status === UserOutRequestStatusTypeGroup.REQUEST) {

  }

  if (data.status === UserOutRequestStatusTypeGroup.BLACK_LIST) {

  }
});


export default app;
