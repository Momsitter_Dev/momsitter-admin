/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 *
 * 주소 정보에 관한 API 를 제공하는 컨트롤러
 */
import express from 'express';
import request from 'request';
import Debug from 'debug';
import xml2js from 'xml2js';
import passport from '../core/momsitter_passport';

// TODO: 에러 코드를 글로벌하게 관리 할 수 있는 방법 생각해 볼 것
const ERR_CODE = {
  ERR_FETCH_LOCATION: {
    type: 'ERR_FETCH_LOCATION',
    message:
      'Received error response while fetching location info via gov api.',
  },
  ERR_PARSE_LOCATION: {
    type: 'ERR_PARSE_LOCATION',
    message: 'Error occurred while parsing received location data.',
  },
  ERR_INVALID_DATA: {
    type: 'ERR_INVALID_DATA',
    message: 'Invalid location data received from gov.',
  },
};

const debug = Debug('api');
const app = express.Router();

const parseXML = xml =>
  new Promise((resolve, reject) => {
    xml2js.parseString(xml, (err, result) => {
      if (err) reject({ error: ERR_CODE.ERR_PARSE_LOCATION, _raw: err });
      else resolve(result);
    });
  });

const getGovLocation = keyword =>
  new Promise((resolve, reject) => {
    // TODO: 설정값을 나중에 config 로 빼낼 것
    const confmKey = 'U01TX0FVVEgyMDE2MDgxMDIyMzQwNTE0NDM2';
    const apiUrl = 'http://www.juso.go.kr/addrlink/addrLinkApi.do';
    const currentPage = 1;
    const countPerPage = 5;

    request.post(
      {
        url: apiUrl,
        form: { confmKey, currentPage, countPerPage, keyword },
      },
      (err, response, body) => {
        if (err || response.statusCode !== 200) {
          reject({ error: ERR_CODE.ERR_FETCH_LOCATION, _raw: err });
        } else {
          resolve(body);
        }
      },
    );
  });

app.get('/:keyword', passport.authenticate('jwt', { session: false }), (req, res) => {
  getGovLocation(req.params.keyword)
    .then(parseXML)
    .then(result => {
      if (result.results && result.results.juso) {
        res.json({
          result: 'success',
          data: result.results.juso,
        });
      } else {
        debug('%j', result);
        res.json({ error: ERR_CODE.ERR_INVALID_DATA });
      }

      /*
        Note

        siNm, sggNm, emdNm 도 추가적으로 저장해야 할 필요성이 검토되어야 함

        emdNm
          -> 수원시일 경우, 동이름이 출력
          -> 성북구일 경우, 성북동이 출력
        sggNm
          -> 수원시일 경우, 구이름이 출력
          -> 성북구일 경우, 성북구가 출력력

       */
    })
    .catch(err => {
      debug(err);
      res.json(err);
    });
});

export default app;
