
import Debug from 'debug';
import express from 'express';
import { Coupon, CouponCode, CouponType } from '../data/models/index';
import ResponseResultTypeGroup from '../util/const/ResponseResultTypeGroup';

const debug = Debug('api');
const app = express.Router();

app.get('/types', async (req, res) => {
  try {
    const couponTypes = await CouponType.findAll();
    if (!couponTypes) {
      throw new Error('쿠폰 타입 조회 오류');
    }
    return res.json({
      result: ResponseResultTypeGroup.SUCCESS,
      data: {
        couponTypes,
      },
    });
  } catch (err) {
    return res.json({
      result: ResponseResultTypeGroup.FAIL,
      err: {
        message: '쿠폰 타입 조회 오류',
      }
    });
  }
});

export default app;
