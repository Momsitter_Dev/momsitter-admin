/* eslint-disable no-undef */
/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import moment from 'moment';
import FlatButton from 'material-ui/FlatButton';
import RmcPopPicker from 'rmc-date-picker/lib/Popup';
import RmcDatePicker from 'rmc-date-picker/lib/DatePicker';
/* eslint-disable import/no-unresolved,import/first,import/no-webpack-loader-syntax */
import RmcPickerStyle from '!!isomorphic-style-loader!css-loader?modules=false!rmc-picker/assets/index.css';
import RmcPickerPopupStyle from '!!isomorphic-style-loader!css-loader?modules=false!rmc-picker/assets/popup.css';
import RmcDatePickerStyle from '!!isomorphic-style-loader!css-loader?modules=false!rmc-date-picker/assets/index.css';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './TimePicker.mobile.css';
import RmcDataPickerLocale from '../../util/const/RmcDataPickerLocale';
import overwrite from '!!isomorphic-style-loader!css-loader?modules=false!./overwrite.mobile.css';
/* eslint-enable import/no-unresolved,import/first,import/no-webpack-loader-syntax */

const styles = {
  popPickerLabel: {
    fontSize: '14px',
    fontWeight: 400,
    fontFamily: 'Noto Sans KR',
  },
  popPickerActionLabel: {
    fontSize: '15px',
    fontWeight: 400,
    fontFamily: 'Noto Sans KR',
    color: '#ff7000',
  },
};

class TimePicker extends React.Component {
  renderLabel = () => {
    const {
      showLabel,
      labelStyle,
      label,
    } = this.props;
    return showLabel && (<div className={s.label} style={labelStyle}> {label} </div>);
  }
  render() {
    const {
      time,
      maxTime,
      minTime,
      minuteStep,
      timeLabelStyle,
      timeStyle,
      timeLabelFormat,
      style,
    } = this.props;

    return (
      <div className={s.root}>
        <div className={s.container} style={style}>
          {
            this.renderLabel()
          }
          <RmcPopPicker
            datePicker={
              <RmcDatePicker
                rootNativeProps={{ 'data-xx': 'yy' }}
                mode={'time'}
                defaultDate={moment(time)}
                maxDate={moment(maxTime)}
                minDate={moment(minTime)}
                minuteStep={minuteStep}
                locale={RmcDataPickerLocale}
              />
            }
            transitionName="rmc-picker-popup-slide-fade"
            maskTransitionName="rmc-picker-popup-fade"
            onChange={this.props.onChange}
            okText={<span style={styles.popPickerActionLabel}> 확인 </span>}
            dismissText={<span style={styles.popPickerActionLabel}> 취소 </span>}
            title={
              <span
                style={styles.popPickerLabel}
                className={s.popPickerLabel}
              >
                마우스로 드래그하여 원하는 시간을 선택하세요
              </span>
            }
          >
            <FlatButton
              label={moment(time).format(timeLabelFormat)}
              labelStyle={timeLabelStyle}
              style={timeStyle}
            />
          </RmcPopPicker>
        </div>
      </div>
    );
  }
}

TimePicker.propTypes = {
  time: PropTypes.oneOfType([
    PropTypes.instanceOf(Date),
    PropTypes.instanceOf(moment),
  ]),
  maxTime: PropTypes.oneOfType([
    PropTypes.instanceOf(Date),
    PropTypes.instanceOf(moment),
  ]),
  minTime: PropTypes.oneOfType([
    PropTypes.instanceOf(Date),
    PropTypes.instanceOf(moment),
  ]),
  showLabel: PropTypes.bool,
  label: PropTypes.string,
  minuteStep: PropTypes.number,
  labelStyle: PropTypes.node,
  style: stylePropType,
  timeLabelStyle: stylePropType,
  timeStyle: stylePropType,
  timeLabelFormat: PropTypes.string,
  onChange: PropTypes.func,
};

TimePicker.defaultProps = {
  time: moment('12:00', 'HH:mm').toDate(),
  maxTime: moment('23:59', 'HH:mm').toDate(),
  minTime: moment('00:00', 'HH:mm').toDate(),
  showLabel: false,
  label: '',
  minuteStep: 10,
  labelStyle: {},
  style: {},
  timeLabelStyle: {},
  timeStyle: {},
  timeLabelFormat: 'HH:mm',
  onChange: () => {},
};

export default withStyles(
  s,
  RmcPickerStyle,
  RmcDatePickerStyle,
  RmcPickerPopupStyle,
  overwrite,
)(TimePicker);
