/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import React from 'react';
import PropTypes from 'prop-types';
import TimePickerDesktop from './TimePicker.desktop';

class TimePicker extends React.Component {
  render() {
    return (
      <TimePickerDesktop {...this.props} />
    );
  }
}

TimePicker.propTypes = {};
TimePicker.defaultProps = {};

export default TimePicker;
