/* eslint-disable no-undef */
/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import stylePropType from 'react-style-proptype';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './TimePicker.desktop.css';

moment.locale('ko');

class TimePicker extends React.Component {
  getMaxTime = () =>
    (this.props.maxTime instanceof moment ? this.props.maxTime : moment(this.props.maxTime))

  getMinTime = () =>
    (this.props.minTime instanceof moment ? this.props.minTime : moment(this.props.minTime))

  getDefaultTime = () => {
    const time = this.props.time instanceof moment ? this.props.time : moment(this.props.time);
    if (time.isBefore(this.getMinTime())) {
      return this.getMinTime();
    }
    if (time.isAfter(this.getMaxTime())) {
      return this.getMaxTime();
    }
    return time;
  }

  handleChange = (event, key, payload) => {
    this.props.onChange(moment(payload, 'HH:mm'));
  }

  renderTimeList = () => {
    const {
      minuteStep,
      timeLabelFormat,
    } = this.props;
    const maxTime = this.getMaxTime();
    const minTime = this.getMinTime();
    const baseTime = moment(minTime);
    const list = [];
    while (baseTime.isBefore(maxTime) || baseTime.isSame(maxTime)) {
      list.push(
        <MenuItem
          key={baseTime.format('HH:mm')}
          value={baseTime.format('HH:mm')}
          primaryText={baseTime.format(timeLabelFormat)}
        />,
      );
      baseTime.add(minuteStep, 'minutes');
    }
    return list;
  }

  render() {
    const {
      maxHeight,
      showLabel,
      label,
      labelStyle,
      selectFieldStyle,
    } = this.props;
    return (
      <div className={s.root}>
        <div className={s.container}>
          {
            showLabel && <div className={s.label} style={labelStyle}> {label} </div>
          }
          <SelectField
            value={this.getDefaultTime().format('HH:mm')}
            maxHeight={maxHeight}
            style={{
              padding: 0,
              width: '100%',
              ...selectFieldStyle,
            }}
            iconStyle={{
              display: 'none',
            }}
            hintStyle={{
              textAlign: 'center',
              fontSize: '14px',
              fontWeight: 600,
              color: '#ff4500',
              width: 70,
              marginLeft: -35,
              left: '50%',
            }}
            inputStyle={{
              textAlign: 'center',
              fontSize: '14px',
              fontWeight: 600,
            }}
            menuItemStyle={{
              fontWeight: 400,
            }}
            selectedMenuItemStyle={{
              color: '#ff4500',
            }}
            labelStyle={{
              color: '#ff4500',
              padding: 0,
              width: '100%',
            }}
            underlineShow={false}
            onChange={this.handleChange}
          >
            {this.renderTimeList()}
          </SelectField>
        </div>
      </div>
    );
  }
}

TimePicker.propTypes = {
  time: PropTypes.oneOfType([
    PropTypes.instanceOf(Date),
    PropTypes.instanceOf(moment),
  ]),
  maxTime: PropTypes.oneOfType([
    PropTypes.instanceOf(Date),
    PropTypes.instanceOf(moment),
  ]),
  minTime: PropTypes.oneOfType([
    PropTypes.instanceOf(Date),
    PropTypes.instanceOf(moment),
  ]),
  maxHeight: PropTypes.number,
  minuteStep: PropTypes.number,
  showLabel: PropTypes.bool,
  label: PropTypes.string,
  labelStyle: stylePropType,
  selectFieldStyle: stylePropType,
  timeLabelFormat: PropTypes.string,
  onChange: PropTypes.func,
};

TimePicker.defaultProps = {
  time: moment('12:00', 'HH:mm'),
  maxTime: moment('23:59', 'HH:mm'),
  minTime: moment('00:00', 'HH:mm'),
  maxHeight: 200,
  minuteStep: 30,
  showLabel: false,
  label: '',
  labelStyle: {},
  selectFieldStyle: {},
  timeLabelFormat: 'A HH:mm',
  onChange: () => {},
};

export default withStyles(s)(TimePicker);
