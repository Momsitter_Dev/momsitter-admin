import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './MemoClass.css';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import MemoReqMethodGroup from '../../../util/const/MemoReqMethodGroup';

class MemoClass extends React.Component {
  onChange = (data) => {
    this.props.onChange(data);
  };
  render() {
    const { value } = this.props;
    return (
      <SelectField
        value={value}
        onChange={(event, index, data) => this.onChange(data)}
      >
        <MenuItem value={MemoReqMethodGroup.CALL} primaryText="전화" />
        <MenuItem value={MemoReqMethodGroup.KAKAO} primaryText="카카오톡" />
        <MenuItem value={MemoReqMethodGroup.MAIL} primaryText="이메일" />
        <MenuItem value={MemoReqMethodGroup.SMS} primaryText="문자" />
      </SelectField>
    );
  }
}

MemoClass.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
};

MemoClass.defaultProps = {};

export default withStyles(s)(MemoClass);
