import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ReportType.css';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import ReportTypeGroup from '../../../util/const/ReportTypeGroup';
import ReportTypeDescriptionGroup from '../../../util/const/ReportTypeDescriptionGroup';

class ReportType extends React.Component {
  onChange = (value) => {
    this.props.onChange(value);
  };

  render() {
    const { value } = this.props;
    return (
      <SelectField
        value={value}
        onChange={(event, index, value) => this.onChange(value)}
      >
        <MenuItem value={ReportTypeGroup.IMPROPER_CONTENT} primaryText={ReportTypeDescriptionGroup[ReportTypeGroup.IMPROPER_CONTENT]} />
        <MenuItem value={ReportTypeGroup.IMPROPER_IMAGE} primaryText={ReportTypeDescriptionGroup[ReportTypeGroup.IMPROPER_IMAGE]} />
        <MenuItem value={ReportTypeGroup.IMPROPER_USER} primaryText={ReportTypeDescriptionGroup[ReportTypeGroup.IMPROPER_USER]} />
        <MenuItem value={ReportTypeGroup.VIOLATE_REGULATION} primaryText={ReportTypeDescriptionGroup[ReportTypeGroup.VIOLATE_REGULATION]} />
        <MenuItem value={ReportTypeGroup.WRONG_INFORMATION} primaryText={ReportTypeDescriptionGroup[ReportTypeGroup.WRONG_INFORMATION]} />
        <MenuItem value={ReportTypeGroup.OTHER} primaryText={ReportTypeDescriptionGroup[ReportTypeGroup.OTHER]} />
      </SelectField>
    );
  }
}

ReportType.propTypes = {
  value: PropTypes.object,
  onChange: PropTypes.func.isRequired,
};

ReportType.defaultProps = {};

export default withStyles(s)(ReportType);
