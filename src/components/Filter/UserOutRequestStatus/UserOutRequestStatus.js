import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './UserOutRequestStatus.css';
import UserOutRequestStatusTypeGroup from '../../../util/const/UserOutRequestStatusTypeGroup'

const propTypes = {
  onChange: PropTypes.func,
  value: PropTypes.string,
};
const defaultProps = {
  onChange: () => {},
  value: '',
};

class UserOutRequestStatus extends React.Component {
  onChange = (event, index, value) => {
    this.props.onChange(value);
  };

  render() {
    return (
      <SelectField
        value={this.props.value}
        onChange={this.onChange}
      >
        <MenuItem value={UserOutRequestStatusTypeGroup.REQUEST} primaryText="요청" />
        <MenuItem value={UserOutRequestStatusTypeGroup.HOLD} primaryText="보류" />
        <MenuItem value={UserOutRequestStatusTypeGroup.CONFIRMED} primaryText="본인탈퇴처리" />
        <MenuItem value={UserOutRequestStatusTypeGroup.BLACK_LIST} primaryText="블랙리스트" />
      </SelectField>
    );
  }
}

UserOutRequestStatus.propTypes = propTypes;
UserOutRequestStatus.defaultProps = defaultProps;

export default withStyles(s)(UserOutRequestStatus);
