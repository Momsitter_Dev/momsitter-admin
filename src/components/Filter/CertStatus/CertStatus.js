import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CertStatus.css';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import CertificateStatusGroup from '../../../util/const/CertificateStatusGroup';

class CertStatus extends React.Component {
  onChange = (data) => {
    this.props.onChange(data);
  };
  render() {
    const { value } = this.props;
    return (
      <SelectField
        value={value}
        onChange={(event, index, data) => this.onChange(data)}
      >
        <MenuItem value={CertificateStatusGroup.APPROVED} primaryText="승인" />
        <MenuItem value={CertificateStatusGroup.NONE} primaryText="없음" />
        <MenuItem value={CertificateStatusGroup.REJECTED} primaryText="거절" />
        <MenuItem value={CertificateStatusGroup.WAITING} primaryText="인증 대기" />
      </SelectField>
    );
  }
}

CertStatus.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
};

CertStatus.defaultProps = {};

export default withStyles(s)(CertStatus);
