import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './MemoType.css';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import MemoTypeGroup from '../../../util/const/MemoTypeGroup';

class MemoType extends React.Component {
  onChange = (data) => {
    this.props.onChange(data);
  };
  render() {
    const { value } = this.props;
    return (
      <SelectField
        value={value}
        onChange={(event, index, data) => this.onChange(data)}
      >
        <MenuItem value={MemoTypeGroup.REQUEST} primaryText="요청" />
        <MenuItem value={MemoTypeGroup.COMPLAIN} primaryText="불만" />
        <MenuItem value={MemoTypeGroup.QUESTION} primaryText="문의" />
        <MenuItem value={MemoTypeGroup.ADMIN} primaryText="관리자" />
      </SelectField>
    );
  }
}

MemoType.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
};

MemoType.defaultProps = {};

export default withStyles(s)(MemoType);
