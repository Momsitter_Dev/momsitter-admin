import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './UserType.css';
import UserTypeIdGroup from '../../../util/const/UserTypeIdGroup';

const propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
};
const defaultProps = {
  value: 1,
};

class UserType extends React.Component {
  onChange = (event) => {
    return this.props.onChange(event.target.value);
  };

  render() {
    const { value } = this.props;
    return (
      <FormControl>
        <Select
          value={value}
          onChange={this.onChange}
        >
          <MenuItem value={UserTypeIdGroup.COLLEGE}> 시터 - 대학생 </MenuItem>
          <MenuItem value={UserTypeIdGroup.TEACHER}> 시터 - 선생님 </MenuItem>
          <MenuItem value={UserTypeIdGroup.MOM}> 시터 - 엄마 </MenuItem>
          <MenuItem value={UserTypeIdGroup.NORMAL}> 시터 - 일반 </MenuItem>
          <MenuItem value={UserTypeIdGroup.PARENT}> 부모회원 </MenuItem>
        </Select>
      </FormControl>
    );
  }
}

UserType.propTypes = propTypes;
UserType.defaultProps = defaultProps;

export default withStyles(s)(UserType);
