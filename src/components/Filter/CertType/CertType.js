import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CertType.css';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import CertificateTypeIdGroup from '../../../util/const/CertificateTypeIdGroup';

class CertType extends React.Component {
  onChange = (data) => {
    this.props.onChange(data);
  };
  render() {
    const { value } = this.props;
    return (
      <SelectField
        value={value}
        onChange={(event, index, data) => this.onChange(data)}
      >
        <MenuItem value={CertificateTypeIdGroup.ENROLLMENT} primaryText="학교 인증" />
        <MenuItem value={CertificateTypeIdGroup.CHILDCARE} primaryText="자격증 인증" />
        <MenuItem value={CertificateTypeIdGroup.FAMILY_RELATION} primaryText="가족관계 인증" />
        <MenuItem value={CertificateTypeIdGroup.MEDICAL_EXAMINATION} primaryText="건강 인증" />
        <MenuItem value={CertificateTypeIdGroup.RESIDENT_REGISTRATION} primaryText="등초본 인증" />
        <MenuItem value={CertificateTypeIdGroup.PERSONALITY} primaryText="인성 인증" />
      </SelectField>
    );
  }
}

CertType.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
};

CertType.defaultProps = {};

export default withStyles(s)(CertType);
