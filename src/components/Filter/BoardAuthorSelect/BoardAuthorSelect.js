import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './BoardAuthorSelect.css';

const propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
};
const defaultProps = {
  value: '',
  onChange: () => {},
};

class BoardAuthorSelect extends React.Component {
  onChange = (data) => {
    return this.props.onChange(data);
  };

  render() {
    const { value } = this.props;
    return (
      <Select
        value={value}
        onChange={(event) => this.onChange(event.target.value)}
      >
        <MenuItem value={'고객관리팀'}> 고객관리팀 </MenuItem>
        <MenuItem value={'개발팀'}> 개발팀 </MenuItem>
        <MenuItem value={'기획팀'}> 기획팀 </MenuItem>
      </Select>
    );
  }
}

BoardAuthorSelect.propTypes = propTypes;
BoardAuthorSelect.defaultProps = defaultProps;

export default withStyles(s)(BoardAuthorSelect);
