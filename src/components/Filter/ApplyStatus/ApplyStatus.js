import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ApplyStatus.css';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import ApplyStatusGroup from '../../../util/const/ApplyStatusGroup';

class ApplyStatus extends React.Component {
  onChange = (data) => {
    this.props.onChange(data);
  };

  render() {
    const { value } = this.props;
    return (
      <SelectField
        value={value}
        onChange={(event, index, data) => this.onChange(data)}
      >
        <MenuItem value={ApplyStatusGroup.REQUEST} primaryText="요청" />
        <MenuItem value={ApplyStatusGroup.ACCEPT} primaryText="승인" />
        <MenuItem value={ApplyStatusGroup.DECLINE} primaryText="거절" />
        <MenuItem value={ApplyStatusGroup.TUNING} primaryText="조율" />
      </SelectField>
    );
  }
}

ApplyStatus.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
};

ApplyStatus.defaultProps = {};

export default withStyles(s)(ApplyStatus);
