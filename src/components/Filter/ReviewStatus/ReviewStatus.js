import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ReviewStatus.css';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import ReviewStatusGroup from '../../../util/const/ReviewStatusGroup';

class ReviewStatus extends React.Component {
  onChange = (data) => {
    this.props.onChange(data);
  };
  render() {
    const { value } = this.props;
    return (
      <SelectField
        value={value}
        onChange={(event, index, data) => this.onChange(data)}
      >
        <MenuItem value={ReviewStatusGroup.OPEN} primaryText="열림" />
        <MenuItem value={ReviewStatusGroup.CLOSE} primaryText="닫힘" />
      </SelectField>
    );
  }
}

ReviewStatus.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
};

ReviewStatus.defaultProps = {};

export default withStyles(s)(ReviewStatus);
