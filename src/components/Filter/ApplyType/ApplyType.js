import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ApplyType.css';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import ApplyTypeGroup from '../../../util/const/ApplyTypeGroup';

class ApplyType extends React.Component {
  onChange = (data) => {
    this.props.onChange(data);
  };
  render() {
    const { value } = this.props;
    return (
      <SelectField
        value={value}
        onChange={(event, index, data) => this.onChange(data)}
      >
        <MenuItem value={ApplyTypeGroup.PARENT_TO_SITTER} primaryText="부모->시터" />
        <MenuItem value={ApplyTypeGroup.SITTER_TO_PARENT} primaryText="시터->부모" />
      </SelectField>
    );
  }
}

ApplyType.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
};

ApplyType.defaultProps = {};

export default withStyles(s)(ApplyType);
