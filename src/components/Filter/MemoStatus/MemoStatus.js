import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './MemoStatus.css';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import MemoStatusGroup from '../../../util/const/MemoStatusGroup';

class MemoStatus extends React.Component {
  onChange = (data) => {
    this.props.onChange(data);
  };
  render() {
    const { value } = this.props;
    return (
      <SelectField
        value={value}
        onChange={(event, index, data) => this.onChange(data)}
      >
        <MenuItem value={MemoStatusGroup.NEW} primaryText="신규" />
        <MenuItem value={MemoStatusGroup.DONE} primaryText="완료" />
      </SelectField>
    );
  }
}

MemoStatus.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
};

MemoStatus.defaultProps = {};

export default withStyles(s)(MemoStatus);
