import React from 'react';
import { Select, MenuItem } from '@material-ui/core';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './UserOwnPhone.css';
import UserOwnPhoneTypeGroup from '../../../util/const/UserOwnPhoneTypeGroup';

const propTypes = {
  value: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
};
const defaultProps = {
  value: false,
};

class UserOwnPhone extends React.Component {
  onChange = value => {
    return this.props.onChange(value);
  };

  render() {
    const { value } = this.props;
    return (
      <Select value={value} onChange={(event) => this.onChange(event.target.value)}>
        <MenuItem value={UserOwnPhoneTypeGroup.OWN_PHONE}> 본인명의 </MenuItem>
        <MenuItem value={UserOwnPhoneTypeGroup.NOT_OWN_PHONE}> 본인명의 아님 </MenuItem>
      </Select>
    );
  }
}

UserOwnPhone.propTypes = propTypes;
UserOwnPhone.defaultProps = defaultProps;

export default withStyles(s)(UserOwnPhone);
