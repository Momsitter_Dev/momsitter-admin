import DatePicker from 'react-datepicker';
import moment from 'moment';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './DateRangePickerWrapper.css';
import datePickerStyle from '!!isomorphic-style-loader!css-loader?modules=false!./overwrite.css'

moment.locale('ko');

const propTypes = {
  onDatesChange: PropTypes.func,
};

const defaultProps = {
  onDatesChange: () => {},
};

class DateRangePickerWrapper extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: null,
      endDate: null,
    };
  }

  onChangeStartDate = (value) => {
    return this.setState({
      startDate: value
    }, () => {
      return this.props.onDatesChange(this.state);
    });
  };

  onChangeEndDate = (value) => {
    return this.setState({
      endDate: value
    }, () => {
      return this.props.onDatesChange(this.state);
    });
  };

  render() {
    return (
      <div className={s.root}>
        <DatePicker selected={this.state.startDate} onChange={this.onChangeStartDate} />
        <DatePicker selected={this.state.endDate} onChange={this.onChangeEndDate} />
      </div>
    );
  }
}

DateRangePickerWrapper.propTypes = propTypes;
DateRangePickerWrapper.defaultProps = defaultProps;

export default withStyles(s, datePickerStyle)(DateRangePickerWrapper);
