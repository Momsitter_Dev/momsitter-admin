import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ReviewType.css';
import ReviewTypeGroup from '../../../util/const/ReviewTypeGroup';

class ReviewType extends React.Component {
  onChange = (data) => {
    this.props.onChange(data);
  };

  render() {
    const { value } = this.props;
    return (
      <SelectField
        value={value}
        onChange={(event, index, data) => this.onChange(data)}
      >
        <MenuItem value={ReviewTypeGroup.INTERVIEW} primaryText="인터뷰" />
        <MenuItem value={ReviewTypeGroup.RECRUIT} primaryText="채용" />
        <MenuItem value={ReviewTypeGroup.FAILURE} primaryText="실패" />
      </SelectField>
    );
  }
}

ReviewType.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
};

ReviewType.defaultProps = {};

export default withStyles(s)(ReviewType);
