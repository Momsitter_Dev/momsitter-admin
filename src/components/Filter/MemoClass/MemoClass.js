import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './MemoClass.css';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import MemoClassGroup from '../../../util/const/MemoClassGroup';

class MemoClass extends React.Component {
  onChange = (data) => {
    this.props.onChange(data);
  };
  render() {
    const { value } = this.props;
    return (
      <SelectField
        value={value}
        onChange={(event, index, data) => this.onChange(data)}
      >
        <MenuItem value={MemoClassGroup.INFO_ERROR} primaryText="정보 오류" />
        <MenuItem value={MemoClassGroup.PROFILE_ERROR} primaryText="사진 오류" />
        <MenuItem value={MemoClassGroup.OTHER} primaryText="기타" />
        <MenuItem value={MemoClassGroup.NOT_IN_USE} primaryText="미사용" />
      </SelectField>
    );
  }
}

MemoClass.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
};

MemoClass.defaultProps = {};

export default withStyles(s)(MemoClass);
