import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './UserStatus.css';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import UserStatusGroup from '../../../util/const/UserStatusGroup'

class UserStatus extends React.Component {
  onChange = (data) => {
    this.props.onChange(data);
  };

  render() {
    const { value } = this.props;
    return (
      <SelectField
        value={value}
        onChange={(event, index, data) => this.onChange(data)}
      >
        <MenuItem value={UserStatusGroup.ACTIVE} primaryText="활성" />
        <MenuItem value={UserStatusGroup.INACTIVE} primaryText="비활성" />
        <MenuItem value={UserStatusGroup.OUT} primaryText="탈퇴" />
      </SelectField>
    );
  }
}

UserStatus.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
};

UserStatus.defaultProps = {};

export default withStyles(s)(UserStatus);
