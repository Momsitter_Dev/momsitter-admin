import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './JoinType.css';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import JoinTypeIdGroup from '../../../util/const/JoinTypeIdGroup';
import JoinTypeDescGroup from '../../../util/const/JoinTypeDescGroup';

const propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
};
const defaultProps = {};

class JoinType extends React.Component {
  onChange = (data) => {
    this.props.onChange(data);
  };
  render() {
    return (
      <SelectField
        value={this.props.value}
        onChange={(event, index, data) => this.onChange(data)}
      >
        <MenuItem value={JoinTypeIdGroup.BRIEF} primaryText={JoinTypeDescGroup[JoinTypeIdGroup.BRIEF]} />
        <MenuItem value={JoinTypeIdGroup.DETAIL} primaryText={JoinTypeDescGroup[JoinTypeIdGroup.DETAIL]} />
      </SelectField>
    );
  }
}

JoinType.propTypes = propTypes;
JoinType.defaultProps = defaultProps;

export default withStyles(s)(JoinType);
