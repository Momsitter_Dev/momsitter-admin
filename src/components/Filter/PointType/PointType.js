import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './PointType.css';

const propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
};
const defaultProps = {
  value: 1,
};

// TODO: point Type as const or async
class PointType extends React.Component {
  onChange = (event) => {
    return this.props.onChange(event.target.value);
  };

  render() {
    const { value } = this.props;
    return (
      <FormControl>
        <Select
          value={value}
          onChange={this.onChange}
        >
          <MenuItem value={1}> 친구 초대 감사 </MenuItem>
          <MenuItem value={2}> 첫 후기 작성 </MenuItem>
          <MenuItem value={3}> 후기 작성 </MenuItem>
          <MenuItem value={4}> 후기 작성 </MenuItem>
          <MenuItem value={5}> 3시간 이내 응답 </MenuItem>
          <MenuItem value={6}> 첫 인증 완료 </MenuItem>
          <MenuItem value={7}> 인증 완료 </MenuItem>
        </Select>
      </FormControl>
    );
  }
}

PointType.propTypes = propTypes;
PointType.defaultProps = defaultProps;

export default withStyles(s)(PointType);
