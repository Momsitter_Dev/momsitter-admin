import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './PaymentStatus.css';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import PaymentStatusGroup from '../../../util/const/PaymentStatusGroup';

class PaymentStatus extends React.Component {
  onChange = (data) => {
    this.props.onChange(data);
  };
  render() {
    const { value } = this.props;
    return (
      <SelectField
        value={value}
        onChange={(event, index, data) => this.onChange(data)}
      >
        <MenuItem value={PaymentStatusGroup.WAITING} primaryText="결제 대기" />
        <MenuItem value={PaymentStatusGroup.ADMIN_ABORT} primaryText="관리자 취소" />
        <MenuItem value={PaymentStatusGroup.DONE} primaryText="결제 완료" />
        <MenuItem value={PaymentStatusGroup.REQUEST} primaryText="결제 요청" />
        <MenuItem value={PaymentStatusGroup.USER_ABORT} primaryText="사용자 취소" />
      </SelectField>
    );
  }
}

PaymentStatus.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
};

PaymentStatus.defaultProps = {};

export default withStyles(s)(PaymentStatus);
