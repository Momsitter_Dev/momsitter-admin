import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './UserGender.css';
import UserGenderGroup from '../../../util/const/UserGenderGroup'

class UserGender extends React.Component {
  onChange = (data) => {
    this.props.onChange(data);
  };

  render() {
    const { value } = this.props;
    return (
      <SelectField
        value={value}
        onChange={(event, index, data) => this.onChange(data)}
      >
        <MenuItem value={UserGenderGroup.MAN} primaryText="남자" />
        <MenuItem value={UserGenderGroup.WOMEN} primaryText="여자" />
      </SelectField>
    );
  }
}

UserGender.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
};

UserGender.defaultProps = {};

export default withStyles(s)(UserGender);
