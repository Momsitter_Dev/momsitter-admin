import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ProductIdSelect.css';

const propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
};
const defaultProps = {
  value: 1,
};

class ProductIdSelect extends React.Component {
  onChange = (event) => {
    return this.props.onChange(event.target.value);
  };

  render() {
    const { value } = this.props;
    return (
      <FormControl>
        <Select
          value={value}
          onChange={this.onChange}
        >
          <MenuItem value={1}> 맘시터 이용권 체험권 </MenuItem>
          <MenuItem value={2}> 맘시터 이용권 (1개월) </MenuItem>
          <MenuItem value={3}> 맘시터 이용권 (3개월) </MenuItem>
          <MenuItem value={4}> 맘시터 이용권 (6개월) </MenuItem>
          <MenuItem value={5}> 맘시터 이용권 체험권 (2일) </MenuItem>
          <MenuItem value={6}> 맘시터 이용권 (3일) </MenuItem>
          <MenuItem value={7}> 맘시터 이용권 (7일) </MenuItem>
          <MenuItem value={8}> 맘시터 이용권 (28일) </MenuItem>
          <MenuItem value={9}> 인성검사 </MenuItem>
          <MenuItem value={10}> 테스트용 지원권 </MenuItem>
          <MenuItem value={11}> 맘시터 지원권 (7일) </MenuItem>
          <MenuItem value={12}> 맘시터 지원권 (30일) </MenuItem>
          <MenuItem value={14}> 시터 상품 테스트 </MenuItem>
          <MenuItem value={15}> 부모 상품 테스트 </MenuItem>
          <MenuItem value={16}> 맘시터 이용권 (14일) </MenuItem>
        </Select>
      </FormControl>
    );
  }
}

ProductIdSelect.propTypes = propTypes;
ProductIdSelect.defaultProps = defaultProps;

export default withStyles(s)(ProductIdSelect);
