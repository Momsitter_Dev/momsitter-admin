import EnhancedButton from 'material-ui/internal/EnhancedButton';
import ArrowUpIcon from 'material-ui/svg-icons/navigation/arrow-upward';
import ArrowDownIcon from 'material-ui/svg-icons/navigation/arrow-downward';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './TableHeaderRow.css';

const iconStyle = {
  position: 'absolute',
  top: '25%',
  right: '-5px',
  width: '15px',
  height: '15px',
};

class TableHeaderRow extends React.Component {
  onChangeOrderColumn = (columnId) => {
    this.props.onChangeOrderColumn(columnId);
  };

  renderArrow = (columnId) => {
    const { orderOption } = this.props;
    let ui = null;
    if (columnId === orderOption.column) {
      if (orderOption.method === 'DESC') {
        ui = (
          <ArrowDownIcon style={iconStyle} />
        );
      }
      if (orderOption.method === 'ASC') {
        ui = (
          <ArrowUpIcon style={iconStyle} />
        );
      }
    }
    return ui;
  };

  renderHeaderItems = () => {
    const { data } = this.props;
    let ui = null;
    if (data && data instanceof Array && data.length > 0) {
      ui = data.map((item, index) => {
        // sortable
        if (item.sortable) {
          return (
            <th key={index}>
              <EnhancedButton
                onClick={() => this.onChangeOrderColumn(item.id)}
                style={{ width: '100%', zIndex: '0' }}
              >
                <div>
                  <span>{item.desc}</span>
                  <span>{this.renderArrow(item.id)}</span>
                </div>
              </EnhancedButton>
            </th>
          );
        } else {
          return (
            <th key={index}>
              <span style={{ color: '#919191'}}>{item.desc}</span>
            </th>
          );
        }
      });
    } else {
      ui = (<div> 오 류 !</div>)
    }
    return ui;
  };

  render() {
    return (
      <tr>
        {this.renderHeaderItems()}
      </tr>
    );
  }
}

TableHeaderRow.propTypes = {
  data: PropTypes.array.isRequired,
  orderOption: PropTypes.object,
  onChangeOrderColumn: PropTypes.func.isRequired,
  openNotificationDialog: PropTypes.func.isRequired,
};

TableHeaderRow.defaultProps = {};

export default withStyles(s)(TableHeaderRow);
