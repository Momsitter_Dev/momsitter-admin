/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */
import { compose } from 'recompose';
import {
  Grid,
  withStyles as withMaterialStyles
} from '@material-ui/core';
import { ToastContainer } from 'react-toastify';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import rs from 'react-toastify/dist/ReactToastify.css';
import s from './Layout.css';
import Container from '../../containers/Container';

const propTypes = {
  children: PropTypes.node.isRequired,
};

const defaultProps = {};

class Layout extends React.Component {
  render() {
    return (
      <Grid container spacing={16}>
        <Grid item xs={12}>
          <Container>{this.props.children}</Container>
        </Grid>
        {/* toast 메세지 컨테이너 toast.success('message') */}
        <ToastContainer toastClassName={s.toast} />
      </Grid>
    );
  }
}

Layout.propTypes = propTypes;
Layout.defaultProps = defaultProps;

export default compose(
  withStyles(s, rs),
  withMaterialStyles(s, { withTheme: true }),
)(Layout);
