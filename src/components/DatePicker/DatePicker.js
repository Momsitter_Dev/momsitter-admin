/* eslint-disable no-undef */
/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import moment from 'moment';
import MaterialDatePicker from 'material-ui/DatePicker';
import areIntlLocalesSupported from 'intl-locales-supported';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './DatePicker.css';

let DateTimeFormat;

if (areIntlLocalesSupported(['ko', 'ko-KR'])) {
  DateTimeFormat = global.Intl.DateTimeFormat;
} else {
  const IntlPolyfill = require('intl');
  DateTimeFormat = IntlPolyfill.DateTimeFormat;
  require('intl/locale-data/jsonp/ko');
  require('intl/locale-data/jsonp/ko-KR');
}

const DEFAULT_MIN_DATE = '1950-01-01';

const styles = {
  dateLabelStyle: {
    verticalAlign: 'top',
  },
  disabledLabelStyle: {
    color: '#bfbfbf',
  },
  disabledDateLabelStyle: {
    color: '#bfbfbf',
  },
};

class DatePicker extends React.Component {
  getLabelStyle = () => {
    const { labelStyle, disabled } = this.props;

    let style = labelStyle;

    if (disabled) {
      style = {
        ...style,
        ...styles.disabledLabelStyle,
      };
    }

    return style;
  };

  getMaxDate = () => {
    const { maxDate } = this.props;
    if (maxDate) {
      return maxDate instanceof moment ? maxDate.toDate() : maxDate;
    }
    return null;
  };

  getMinDate = () => {
    const { minDate } = this.props;
    if (minDate) {
      return minDate instanceof moment ? minDate.toDate() : minDate;
    }
    return moment(DEFAULT_MIN_DATE).toDate();
  };

  getDefaultDate = () => {
    const { date } = this.props;
    if (date) {
      return date instanceof moment ? date.toDate() : date;
    }
    return null;
  };

  handleChange = (event, date) => {
    this.props.onChange(moment(date));
  };

  render() {
    const {
      hintText,
      showLabel,
      label,
      style,
      rootStyle,
      pickerStyle,
      textFieldStyle,
      inputStyle,
      hintStyle,
      disabled,
      openToYearSelection,
    } = this.props;

    return (
      <div className={s.root} style={rootStyle}>
        <div className={s.container} style={style}>
          {showLabel && (
            <div className={s.label} style={this.getLabelStyle()}>
              {' '}
              {label}{' '}
            </div>
          )}
          <MaterialDatePicker
            DateTimeFormat={DateTimeFormat}
            className={s.datePicker}
            locale="ko-KR"
            minDate={this.getMinDate()}
            maxDate={this.getMaxDate()}
            value={this.getDefaultDate() || null}
            container={'inline'}
            cancelLabel={'취소'}
            okLabel={'확인'}
            hintText={hintText}
            style={pickerStyle}
            dialogContainerStyle={null}
            textFieldStyle={{
              width: '100%',
              cursor: 'pointer',
              ...textFieldStyle,
            }}
            inputStyle={{
              textAlign: 'center',
              fontSize: '14px',
              fontWeight: 600,
              color: '#ff4500',
              ...inputStyle,
            }}
            hintStyle={{
              textAlign: 'center',
              fontSize: '14px',
              fontWeight: 600,
              color: '#ff4500',
              width: 70,
              marginLeft: -35,
              left: '50%',
              ...hintStyle,
            }}
            underlineShow={false}
            disabled={disabled}
            openToYearSelection={openToYearSelection}
            onChange={this.handleChange}
          />
        </div>
      </div>
    );
  }
}

DatePicker.propTypes = {
  date: PropTypes.oneOfType([PropTypes.instanceOf(Date), PropTypes.instanceOf(moment)]),
  maxDate: PropTypes.oneOfType([PropTypes.instanceOf(Date), PropTypes.instanceOf(moment)]),
  minDate: PropTypes.oneOfType([PropTypes.instanceOf(Date), PropTypes.instanceOf(moment)]),
  showLabel: PropTypes.bool,
  label: PropTypes.string,
  labelStyle: stylePropType,
  hintText: PropTypes.string,
  style: stylePropType,
  rootStyle: stylePropType,
  pickerStyle: stylePropType,
  textFieldStyle: stylePropType,
  inputStyle: stylePropType,
  hintStyle: stylePropType,
  disabled: PropTypes.bool,
  openToYearSelection: PropTypes.bool,
  onChange: PropTypes.func,
};

DatePicker.defaultProps = {
  date: null,
  maxDate: null,
  minDate: null,
  showLabel: false,
  label: '',
  labelStyle: {},
  hintText: '날짜선택',
  style: {},
  rootStyle: {},
  pickerStyle: {},
  textFieldStyle: {},
  inputStyle: {},
  hintStyle: {},
  disabled: false,
  openToYearSelection: false,
  onChange: () => {},
};

export default withStyles(s)(DatePicker);
