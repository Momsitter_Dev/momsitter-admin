import { compose } from 'recompose';
import React from 'react';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles/index';
import Button from '@material-ui/core/Button';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import StarIcon from '@material-ui/icons/Star';
import LocationIcon from '@material-ui/icons/LocationOn';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ProfileSitterLocation.css';

const iconStyle = {
  width: '15px',
  height: '15px',
  fill: '#fff',
  marginRight: '5px',
};

const propTypes = {
  title: PropTypes.string.isRequired,
  locations: PropTypes.arrayOf(
    PropTypes.shape({
      locationId: PropTypes.number,
      locationType: PropTypes.string,
      userId: PropTypes.number,
      jibunAddr: PropTypes.string,
      roadAddr: PropTypes.string,
      locationLatitude: PropTypes.number,
      locationLongitude: PropTypes.number,
      siNm: PropTypes.string,
      sggNm: PropTypes.string,
      emdNm: PropTypes.string,
      detailAddr: PropTypes.string,
      postcode: PropTypes.string,
      zonecode: PropTypes.string,
    }),
  ),
  onEdit: PropTypes.func,
};
const defaultProps = {
  locations: [],
  onEdit: () => {},
};

class ProfileSitterLocation extends React.Component {
  onClickEdit = () => {
    return this.props.onEdit();
  };

  renderEmpty = () => {
    return <Typography variant={'caption'}>표시할 정보가 없습니다.</Typography>;
  };

  renderLocationItems = () => {
    const { locations } = this.props;
    if (!locations) {
      return this.renderEmpty();
    }
    if (locations.length === 0) {
      return this.renderEmpty();
    }

    return locations.map((item, index) => {
      let divider = null;
      if (locations[index + 1]) {
        divider = <Divider style={{ backgroundColor: '#e0e0e0' }} />;
      }
      return (
        <div key={`location-item-${index}`}>
          <div className={s.item}>
            <div className={index === 0 ? s.chip : s.subChip}>
              {index === 0 ? <StarIcon style={iconStyle} /> : <LocationIcon style={iconStyle} />}
              <div> {index + 1} 순위 </div>
            </div>
            <div> {`${item.siNm || ''} ${item.sggNm || ''} ${item.emdNm || ''}`.trim()} </div>
          </div>
          {divider}
        </div>
      );
    });
  };

  render() {
    const { title } = this.props;
    return (
      <ExpansionPanel>
        <ExpansionPanelSummary>
          <Typography variant={'body1'}> {title} </Typography>
        </ExpansionPanelSummary>

        <ExpansionPanelDetails style={{ paddingTop: '0', paddingBottom: '0' }}>
          <Grid container spacing={16}>
            <Grid item xs={12}>
              {this.renderLocationItems()}
            </Grid>
          </Grid>
        </ExpansionPanelDetails>
        <ExpansionPanelActions>
          <Button variant={'flat'} onClick={this.onClickEdit}>수정하기</Button>
        </ExpansionPanelActions>
      </ExpansionPanel>
    );
  }
}

ProfileSitterLocation.propTypes = propTypes;
ProfileSitterLocation.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(ProfileSitterLocation);
