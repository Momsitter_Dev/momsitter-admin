/* eslint-disable no-undef */
/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@mfort.co.kr
 */

import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import FlatButton from 'material-ui/FlatButton';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './FormGenderPicker.css';
import GenderTypeGroup from '../../util/const/GenderTypeGroup';

const propTypes = {
  style: stylePropType,
  // 디폴트 성별값
  defaultGender: PropTypes.oneOf([GenderTypeGroup.WOMEN, GenderTypeGroup.MAN, GenderTypeGroup.IDC]),
  onChange: PropTypes.func,
};
const defaultProps = {
  style: {},
  defaultGender: GenderTypeGroup.WOMEN,
  onChange: () => {},
};

class FormGenderPicker extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      gender: props.defaultGender,
    };
  }

  handleChange = value => {
    this.setState(
      {
        gender: value,
      },
      () => this.props.onChange(value),
    );
  };

  render() {
    const { style } = this.props;
    const { gender } = this.state;
    return (
      <div className={s.root}>
        <div className={s.container} style={style}>
          <FlatButton
            className={gender === GenderTypeGroup.WOMEN ? s.buttonActive : s.buttonInactive}
            label={'여자'}
            onClick={() => this.handleChange(GenderTypeGroup.WOMEN)}
          />
          <FlatButton
            className={gender === GenderTypeGroup.MAN ? s.buttonActive : s.buttonInactive}
            label={'남자'}
            onClick={() => this.handleChange(GenderTypeGroup.MAN)}
          />
          <FlatButton
            className={gender === GenderTypeGroup.IDC ? s.buttonActive : s.buttonInactive}
            label={'무관'}
            onClick={() => this.handleChange(GenderTypeGroup.IDC)}
          />
        </div>
      </div>
    );
  }
}

FormGenderPicker.propTypes = propTypes;
FormGenderPicker.defaultProps = defaultProps;

export default withStyles(s)(FormGenderPicker);
