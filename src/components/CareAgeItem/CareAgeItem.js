/* eslint-disable no-undef */
import EnhancedButton from 'material-ui/internal/EnhancedButton';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CareAgeItem.css';

const propTypes = {
  id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  label: PropTypes.string.isRequired,
  active: PropTypes.bool,
  iconY: PropTypes.string.isRequired,
  iconN: PropTypes.string.isRequired,
  onClickItem: PropTypes.func,
};
const defaultProps = {
  active: false,
  iconY: 'join-study-s.svg',
  iconN: 'join-study-n.svg',
  onClickItem: () => {},
};

const S3 = 'https://s3.ap-northeast-2.amazonaws.com/momsitter-service/momsitter-app/static/public/form/';

class CareAgeItem extends React.Component {
  onClickItem = () => this.props.onClickItem(this.props.id);

  renderIcon = () => {
    if (this.props.active) {
      return (
        <div className={s.iconWrap}>
          <img alt={this.props.label} src={`${S3}${this.props.iconY}`} className={s.icon} />
          <img alt={'checked'} src={`${S3}check-icon.svg`} className={s.checkIcon} />
        </div>
      );
    }
    return (
      <div className={s.iconWrap}>
        <img alt={this.props.label} src={`${S3}${this.props.iconN}`} className={s.icon} />
      </div>
    );
  };

  renderLabel = () => {
    if (this.props.active) {
      return (
        <div style={{ color: '#e45600' }} className={s.label}>
          {' '}
          {this.props.label}{' '}
        </div>
      );
    }
    return <div className={s.label}> {this.props.label} </div>;
  };

  render() {
    return (
      <div className={s.root}>
        <EnhancedButton onClick={this.onClickItem} style={{ padding: '15px' }}>
          {this.renderIcon()}
          {this.renderLabel()}
        </EnhancedButton>
      </div>
    );
  }
}

CareAgeItem.propTypes = propTypes;
CareAgeItem.defaultProps = defaultProps;

export default withStyles(s)(CareAgeItem);
