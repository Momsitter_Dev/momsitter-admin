import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CheckboxItem.css';


class CheckboxItem extends React.Component {
  constructor(props){
    super(props);

    this._handleToggle = this._handleToggle.bind(this);
  }

  _handleToggle(event, data){
    // 다른 체크된 것이 있는지 확인하고 해당 엘리먼트의 체크를 푼다
    this.props.__handleChildToggle(null, {
      index: this.props.index,
      checked: this.props.checked ? true : false,
      value: this.props.value,
    });
  }

  render() {
    return (
      <div className={s.wrap} onClick={this._handleToggle} style={ this.props.checked ? {...this.props.style, ...this.props.activeStyle} : {...this.props.style} } >
        <input type="checkbox" name={this.props.name} className={s.input} checked={this.props.checked} value={this.props.value} />
        <div className={s.container}>
          <div className={s.iconWrap} style={ this.props.checked ? {...this.props.iconWrapStyle, ...this.props.activeIconWrapStyle} : {...this.props.iconWrapStyle} }>
            {
              (this.props.checked) ? (this.props.checkedIcon) ? this.props.checkedIcon : (null) : (this.props.unCheckedIcon) ? this.props.unCheckedIcon : (null)
            }
          </div>
          <div className={s.contentWrap}>
            <div className={s.labelWrap} style={ this.props.checked ? {...this.props.labelWrapStyle, ...this.props.activeLabelWrapStyle} : {...this.props.labelWrapStyle} }>
              <div className={s.mainLabel} style={ this.props.checked ? {...this.props.mainLabelStyle, ...this.props.activeMainLabelStyle} : {...this.props.mainLabelStyle} }>
                {this.props.mainLabel}
              </div>
              <div className={s.subLabel} style={ this.props.checked ? {...this.props.subLabelStyle, ...this.props.activeSubLabelStyle} : {...this.props.subLabelStyle} }>
                {this.props.subLabel}
              </div>
            </div>
            <div className={s.description} style={ this.props.checked ? {...this.props.descriptionStyle, ...this.props.activeDescriptionStyle} : {...this.props.descriptionStyle} }>
              {this.props.description}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

CheckboxItem.propTyeps = {
  data: PropTypes.object,
  index: PropTypes.number,
  name: PropTypes.string.isRequired,
  __handleChildToggle: PropTypes.func,

  unCheckedIcon: PropTypes.element,
  checkedIcon: PropTypes.element,

  iconWrapStyle: PropTypes.object,
  labelWrapStyle: PropTypes.object,
  mainLabelStyle: PropTypes.object,
  subLabelStyle: PropTypes.object,
  descriptionStyle: PropTypes.object,
  style: PropTypes.object,

  activeIconWrapStyle: PropTypes.object,
  activeStyle: PropTypes.object,
  activeLabelWrapStyle: PropTypes.object,
  activeMainLabelStyle: PropTypes.object,
  activeSubLabelStyle: PropTypes.object,
  activeDescriptionStyle: PropTypes.object,
};

CheckboxItem.propTyeps = {
  mainLabelStyle: {},
  subLabelStyle: {},
  descriptionStyle: {},
  style: {},
};


export default withStyles(s)(CheckboxItem);
