import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CheckboxMarkdown.css';


class CheckboxMarkdown extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      expanded: this.props.expanded,
    };
  };

  _handleToggle = (event, data) => {
    // 다른 체크된 것이 있는지 확인하고 해당 엘리먼트의 체크를 푼다
    this.props.__handleChildToggle(null, {
      index: this.props.index,
      checked: this.props.checked ? true : false,
      value: this.props.value,
    });
  };

  _handleExpand = (event, data) => {
    event.stopPropagation();
    this.setState({expanded: !(this.state.expanded)});
  };

  render() {
    return (
      <div>
        <div className={s.wrap} onClick={this._handleToggle} style={ this.props.checked ? {...this.props.style, ...this.props.activeStyle} : {...this.props.style} } >
          <input type="checkbox" name={this.props.name} className={s.input} checked={this.props.checked} value={this.props.value} />
          <div className={s.container}>
            <div className={s.iconWrap} style={ this.props.checked ? {...this.props.iconWrapStyle, ...this.props.activeIconWrapStyle} : {...this.props.iconWrapStyle} }>
              {
                (this.props.checked) ? (this.props.checkedIcon) ? this.props.checkedIcon : (null) : (this.props.unCheckedIcon) ? this.props.unCheckedIcon : (null)
              }
            </div>
            <div className={s.contentWrap}>
              <div className={s.labelWrap} style={ this.props.checked ? {...this.props.labelWrapStyle, ...this.props.activeLabelWrapStyle} : {...this.props.labelWrapStyle} }>
                <div className={s.mainLabel} style={ this.props.checked ? {...this.props.mainLabelStyle, ...this.props.activeMainLabelStyle} : {...this.props.mainLabelStyle} }>
                  {this.props.mainLabel}
                </div>
                <div className={s.subLabel} style={ this.props.checked ? {...this.props.subLabelStyle, ...this.props.activeSubLabelStyle} : {...this.props.subLabelStyle} }>
                  {this.props.subLabel}
                </div>
              </div>
            </div>
            <div onClick={this._handleExpand} style={ this.props.expandedIconStyle }>
              {
                (this.state.expanded) ? (this.props.unExpandedIcon) ? this.props.unExpandedIcon : null : (this.props.expandedIcon) ? this.props.expandedIcon : null
              }
            </div>
          </div>
        </div>
        <div style={ this.state.expanded ? {display: 'block'} : {display: 'none'} }>안보이지롱</div>
      </div>
    );
  }
}

CheckboxMarkdown.propTyeps = {
  data: PropTypes.object,
  index: PropTypes.number,
  name: PropTypes.string.isRequired,
  __handleChildToggle: PropTypes.func,
  markdownUrl: PropTypes.string,
  expaned: PropTypes.bool,

  unCheckedIcon: PropTypes.element,
  checkedIcon: PropTypes.element,

  expandedIcon: PropTypes.element,
  unExpandedIcon: PropTypes.element,

  iconWrapStyle: PropTypes.object,
  expandedIconStyle: PropTypes.object,
  labelWrapStyle: PropTypes.object,
  mainLabelStyle: PropTypes.object,
  subLabelStyle: PropTypes.object,
  style: PropTypes.object,

  activeIconWrapStyle: PropTypes.object,
  activeStyle: PropTypes.object,
  activeLabelWrapStyle: PropTypes.object,
  activeMainLabelStyle: PropTypes.object,
  activeSubLabelStyle: PropTypes.object,
};

CheckboxMarkdown.propTyeps = {
  mainLabelStyle: {},
  subLabelStyle: {},
  style: {},
  expanded: false,
};


export default withStyles(s)(CheckboxMarkdown);
