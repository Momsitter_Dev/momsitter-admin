import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CheckboxGroup.css';

import IconCheckbox from 'material-ui/svg-icons/toggle/check-box';
import IconCheckboxBlank from 'material-ui/svg-icons/toggle/check-box-outline-blank';

/**
 * @class SwitchGroup
 */
class CheckboxGroup extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      activeItem: [],
    };

    if ( this.props.children instanceof Array ) {
      this.props.children.forEach((child) => {
        if ( child.props.checked ) {
          this.state.activeItem.push(child.props.value);
        }
      });
    }

    this._handleChildToggle = this._handleChildToggle.bind(this);
    this._handleChildChange = this._handleChildChange.bind(this);
  }

  _handleChildToggle(event, data) {
    // if data.checked == true -> pop ActiveItem(data.index);
    if ( data.checked ) {
      let matchedIndex = this.state.activeItem.findIndex((item) => {
        if ( item == data.value ){
          return true;
        }
      });
      if ( matchedIndex === -1 ){
        // 이럴수가있남... ERROR
      } else {
        this.state.activeItem.splice(matchedIndex, 1);
        this.setState({activeItem: this.state.activeItem});
        if ( this.props.onChange ){
          this.props.onChange(null, this.state.activeItem);
        }
      }
    } else {
      this.state.activeItem.push(data.value);
      this.setState({activeItem: this.state.activeItem});

      if ( this.props.onChange ){
        this.props.onChange(null, this.state.activeItem);
      }
    }
  }

  _handleChildChange() {
  }

  render() {
    return (
      <div style={this.props.style}>
        {
          React.Children.map(this.props.children, (child, index) => {
            let ind = this.state.activeItem.findIndex((item) => {
              if( item == child.props.value ){
                return true;
              }
            });
            if ( ind != -1 ) {
              return React.cloneElement(child, { __handleChildToggle: this._handleChildToggle, checked: true, name: this.props.name })
            } else {
              return React.cloneElement(child, { __handleChildToggle: this._handleChildToggle, checked: false, name: this.props.name })
            }
          })
        }
      </div>
    );
  }
}

CheckboxGroup.propTypes = {
  name: PropTypes.string.isRequired,
  defaultSelected: PropTypes.number,
  onChange: PropTypes.func,
  style: PropTypes.object,
};

CheckboxGroup.defaultProps = {
  isMultiple: true,
  name: '',
  checkIcon: IconCheckbox,
  uncheckedIcon: IconCheckboxBlank,
  style: {},
};


export default withStyles(s)(CheckboxGroup);
