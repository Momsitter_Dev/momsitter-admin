/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import PropTypes from 'prop-types';
import serialize from 'serialize-javascript';
import serverConfig from '../config/config';

/* eslint-disable react/no-danger */
class Html extends React.Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    styles: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        cssText: PropTypes.string.isRequired,
      }).isRequired,
    ),
    css: PropTypes.string.isRequired,
    scripts: PropTypes.arrayOf(PropTypes.string.isRequired),
    app: PropTypes.object, // eslint-disable-line
    children: PropTypes.string.isRequired,
    config: PropTypes.string.isRequired,
  };

  static defaultProps = {
    styles: [],
    scripts: [],
  };

  render() {
    const { title, description, styles, css, scripts, app, config, children } = this.props;
    return (
      <html
        className="no-js wf-roboto-n1-active wf-roboto-n3-active wf-roboto-n4-active wf-roboto-i4-active wf-roboto-n5-active wf-roboto-n7-active wf-active"
        lang="en"
      >
        <head>
          <meta charSet="utf-8" />
          <meta httpEquiv="x-ua-compatible" content="ie=edge" />
          <title>{title}</title>
          <meta name="description" content={description} />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          {scripts.map(script => (
            <link key={script} rel="preload" href={script} as="script" />
          ))}
          <link rel="apple-touch-icon" href="apple-touch-icon.png" />
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/8.0.0/normalize.css" />
          <link rel="stylesheet" href="/dist/bootstrap.css" />
          <link rel="stylesheet" href="/dist/layout.css" />
          <link rel="stylesheet" href="/dist/theme.css" />
          <link rel="stylesheet" href="/dist/ui.css" />
          <link rel="stylesheet" href="/dist/app.css" />
          <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" />
          <link rel="stylesheet" href="/dist/font.css" />

          {styles.map(style => (
            <style key={style.id} id={style.id} dangerouslySetInnerHTML={{ __html: style.cssText }} />
          ))}
        </head>
        <body>
          <section id="app-container" dangerouslySetInnerHTML={{ __html: children }} />
          <style id="jss-server-side" dangerouslySetInnerHTML={{ __html: css }} />
          <script dangerouslySetInnerHTML={{ __html: `window.APP_CONFIG=${serialize(config)}` }} />
          <script dangerouslySetInnerHTML={{ __html: `window.App=${serialize(app)}` }} />
          {serverConfig.api.google.key && (
            <script
              src={`https://maps.googleapis.com/maps/api/js?key=${serverConfig.api.google.key}&libraries=places`}
            />
          )}
          {scripts.map(script => (
            <script key={script} src={script} />
          ))}
        </body>
      </html>
    );
  }
}

export default Html;
