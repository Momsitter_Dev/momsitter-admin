import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ProgressCard.css';
import Paper from 'material-ui/Paper';
import CircularProgress from 'material-ui/CircularProgress';

class ProgressCard extends React.Component {
  render() {
    const { text } = this.props;
    return (
      <Paper zDepth={2}>
        <div className={s.root}>
          <div className={s.progress}>
            <CircularProgress />
          </div>
          <div className={text}>
            {text}
          </div>
        </div>
      </Paper>
    );
  }
}

ProgressCard.propTypes = {
  text: PropTypes.string,
};

ProgressCard.defaultProps = {
  text: '잠시만 기다려 주세요.',
};

export default withStyles(s)(ProgressCard);
