import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './TextField.css';

class TextField extends React.Component {
  onChange = (event) => {
    this.props.onChange(event.target.value);
  };

  onKeyPress = (event) => {
    if (this.props.onKeyPress instanceof Function) {
      this.props.onKeyPress(event);
    }
  };

  render() {
    const { disabled, value } = this.props;
    return (
      <textarea
        onChange={this.onChange}
        value={value}
        disabled={disabled}
        className={s.textField}
        onKeyPress={this.onKeyPress}
      />
    );
  }
}

TextField.propTypes = {
  onChange: PropTypes.func.isRequired,
  onKeyPress: PropTypes.func,
  value: PropTypes.string,
  disabled: PropTypes.bool,
};

TextField.defaultProps = {
  value: '',
  disabled: false,
};

export default withStyles(s)(TextField);
