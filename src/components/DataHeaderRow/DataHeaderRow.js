import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './DataHeaderRow.css';

const propTypes = {};
const defaultProps = {};

class DataHeaderRow extends React.Component {
  render() {
    return (
      <div className={s.root}>
        <div className={s.container}>

        </div>
      </div>
    );
  }
}

DataHeaderRow.propTypes = propTypes;
DataHeaderRow.defaultProps = defaultProps;

export default withStyles(s)(DataHeaderRow);
