import moment from 'moment';
import { compose } from 'recompose';
import React from 'react';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles/index';
import Button from '@material-ui/core/Button';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ProfileSitterExperience.css';

moment.locale('ko');

const propTypes = {
  title: PropTypes.string.isRequired,
  sitterExperience: PropTypes.arrayOf(
    PropTypes.shape({
      startDate: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Date)]),
      endDate: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Date)]),
      experienceTitle: PropTypes.string,
    }),
  ),
  onEdit: PropTypes.func,
};
const defaultProps = {
  sitterExperience: [],
  onEdit: () => {},
};

class ProfileSitterExperience extends React.Component {
  onClickEdit = () => {
    return this.props.onEdit();
  };

  renderExperiences = () => {
    const { sitterExperience } = this.props;
    return sitterExperience.map((item, index) => (
      <div className={s.itemWrap} key={`expreience-item-${index}`}>
        <div className={s.itemTitle}>
          <div className={s.bullet} />
          <div> {item.experienceTitle} </div>
        </div>
        <div className={s.itemDuration}>
          {`${moment(item.startDate).format('YYYY.MM.DD')} ~ ${moment(item.endDate).format('YYYY.MM.DD')}`}
        </div>
      </div>
    ));
  };

  renderEmpty = () => {
    return <Typography variant={'caption'}>데이터가 없습니다.</Typography>;
  };

  renderContent = () => {
    const { sitterExperience } = this.props;
    if (!sitterExperience) {
      return this.renderEmpty();
    }
    if (sitterExperience.length === 0) {
      return this.renderEmpty();
    }
    return this.renderExperiences();
  };

  render() {
    const { title } = this.props;
    return (
      <ExpansionPanel>
        <ExpansionPanelSummary>
          <Typography variant={'body1'}> {title} </Typography>
        </ExpansionPanelSummary>

        <ExpansionPanelDetails style={{ paddingTop: '0', paddingBottom: '0' }}>
          <Grid container spacing={16}>
            <Grid item xs={12}>
              <div className={s.experienceWrap}>{this.renderContent()}</div>
            </Grid>
          </Grid>
        </ExpansionPanelDetails>
        <ExpansionPanelActions>
          <Button variant={'raised'} onClick={this.onClickEdit}>
            수정하기
          </Button>
        </ExpansionPanelActions>
      </ExpansionPanel>
    );
  }
}

ProfileSitterExperience.propTypes = propTypes;
ProfileSitterExperience.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(ProfileSitterExperience);
