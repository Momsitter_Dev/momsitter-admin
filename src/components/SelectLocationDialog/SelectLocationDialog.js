import { compose } from 'recompose';

import React from 'react';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './SelectLocationDialog.css';
import SearchLocationPicker from '../SearchLocationPicker';

const propTypes = {
  open: PropTypes.bool,
};
const defaultProps = {
  open: false,
};

class SelectLocationDialog extends React.Component {
  render() {
    const { open } = this.props;
    return (
      <div className={s.root}>
        <Dialog open={open}>
          <DialogTitle> 장소를 선택해 주세요. </DialogTitle>
          <DialogContent style={{ width: '500px' }}>
            <DialogContentText> 변경할 장소를 선택해 주세요. </DialogContentText>
            <SearchLocationPicker />
          </DialogContent>
          <DialogActions>
            <Button variant={'flat'}> 닫기 </Button>
            <Button variant={'raised'} color={'primary'}> 저장 </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

SelectLocationDialog.propTypes = propTypes;
SelectLocationDialog.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(SelectLocationDialog);
