import styleProps from 'react-style-proptype';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './BarProgress.css';

const propTypes = {
  height: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  bgColor: PropTypes.string,
  valueColor: PropTypes.string,
  avg: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  showAvgLine: PropTypes.bool,
  showLabel: PropTypes.string,
  showCross: PropTypes.bool,
};
const defaultProps = {
  height: '50px',
  value: 0,
  avg: 10,
  bgColor: '#d3d3d3',
  valueColor: 'blue',
  showAvgLine: false,
  showLabel: true,
  showCross: false,
};

class BarProgress extends React.Component {
  render() {
    return (
      <div className={s.root}>
        <div
          className={s.barWrap}
          style={{
            backgroundColor: this.props.bgColor,
            height: this.props.height,
          }}
        >
          <div
            className={s.cross}
            style={{
              top: (parseInt(this.props.height, 10) / 2),
              borderColor: this.props.valueColor,
              display: this.props.showCross ? 'block' : 'none',
            }}
          />

          <div
            className={s.fillArea}
            style={{
              width: `${this.props.value}%`,
              height: this.props.height,
              backgroundColor: this.props.valueColor,
            }}
          />
          <div
            className={s.label}
            style={{
              display: (this.props.showLabel) ? 'block' : 'none',
              left: `${parseInt(this.props.value, 10) + 2}%`,
              color: this.props.valueColor,
              fontSize: '0.9rem',
              fontWeight: '500',
            }}
          >
            {this.props.value}
          </div>
          <div
            className={s.line}
            style={{
              left: `${this.props.avg}%`,
              height: this.props.height,
              display: (this.props.showAvgLine) ? 'block' : 'none',
            }}
          >
            <div className={s.top} />
            <div className={s.bottom} />
          </div>
        </div>
      </div>
    );
  }
}

BarProgress.propTypes = propTypes;
BarProgress.defaultProps = defaultProps;

export default withStyles(s)(BarProgress);
