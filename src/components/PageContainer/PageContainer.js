import { compose } from 'recompose';
import { Grid, withStyles as withMaterialStyles } from '@material-ui/core';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './PageContainer.css';

class PageContainer extends React.Component {
  render() {
    return (
      <div>
        {this.props.children}
      </div>
    );
  }
}

PageContainer.propTypes = {
  children: PropTypes.node.isRequired,
};
PageContainer.defaultProps = {};

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(PageContainer);
