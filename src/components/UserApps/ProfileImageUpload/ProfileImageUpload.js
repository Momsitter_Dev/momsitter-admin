import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ProfileImageUpload.css';
import CircularProgress from 'material-ui/CircularProgress';
import ProfileImageItem from './ProfileImageItem/ProfileImageItem';
import fetch from 'isomorphic-fetch';
import Paper from 'material-ui/Paper';
import Request from '../../../util/Request';
import uuid from 'uuid/v4';
import _ from 'lodash';

class ProfileImageUpload extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      uploading: false,
      images: props.profileImages.map(item => ({
        ...item,
        generatedKey: uuid(),
      })),
    };
    this.handleAddProfileImage = this.handleAddProfileImage.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      uploading: false,
      images: nextProps.profileImages.map(item => ({
        ...item,
        generatedKey: uuid(),
      })),
    });
  }

  handleClickItem = (event, genKey) => {
    const { images } = this.state;
    const { userTypeId } = this.props;
    const targetIndex = _.findIndex(images, o => o.generatedKey === genKey);
    const userProfileType = {
      basic: '',
      main: '',
    };
    if (userTypeId === 5) {
      userProfileType.basic = 'parentProfile';
      userProfileType.main = 'parentProfileMain';
    } else {
      userProfileType.basic = 'sitterProfile';
      userProfileType.main = 'sitterProfileMain';
    }
    // 메인 이미지 초기화
    for (let i = 0; i < images.length; i += 1) {
      images[i].profileImageType = userProfileType.basic;
    }
    if (targetIndex !== -1) {
      images[targetIndex].profileImageType = userProfileType.main;
      this.setState({ images: Object.assign([], images) });
      this.props.onChange(null, images);
    } else {
      // TODO Handling Error
      console.log('DEBUG : THIS CASE IS ERROR');
    }
  };

  handleRemoveProfileImage = (event, genKey) => {
    const { images } = this.state;
    _.remove(images, o => o.generatedKey === genKey);
    // images를 다시 재할당하는 것과 무슨 차이가 있는가 ? GC는 재대로 되고 있을까?
    this.setState({ images: Object.assign([], images) });
    this.props.onChange(null, images);
  };

  handleAddProfileImage = async function () {
    const { userTypeId, userId } = this.props;

    if (!this.state.uploading) {
      this.setState({ uploading: true });
      const formData = new FormData();
      const file = document.getElementById('file').files[0];
      let userProfileImageType = '';

      if (userTypeId === 5) {
        userProfileImageType = 'parentProfile';
      } else {
        userProfileImageType = 'sitterProfile';
      }

      if (file) {
        formData.append('file', file);
        formData.append('userId', userId);
        try {
          const { images } = this.state;
          const response = await Request('api/file', {
            method: 'POST',
            data: formData,
            header: null,
            modeType: 'no-cors',
            dataParse: true,
          });
          if (response.result === 'success') {
            const { data } = response;
            images.push({
              generatedKey: uuid(),
              fileId: data.fileId,
              profileImageType: userProfileImageType,
              profileFile: {
                auth: data.auth,
                fileId: data.fileId,
                fileName: data.fileName,
                fileSize: data.fileSize,
                fileUrl: data.fileUrl,
                fileUuid: data.fileUuid,
                mimeType: data.mimeType,
                uploadDate: data.uploadDate,
                userId: data.userId,
              },
              userId,
            });
            this.setState(
              {
                images,
                uploading: false,
              },
              () => this.props.onChange(null, images),
            );
          } else {
            alert('파일 업로드 중 에러 발생!');
            this.setState({ uploading: false });
          }
        } catch (err) {
          alert('파일 업로드 중 에러 발생!');
          console.error(err);
          this.setState({ uploading: false });
        }
      }
    }
  };

  handleAddButtonClick = () => {
    this.fileElement.click();
  };

  render() {
    const { images, uploading } = this.state;
    return (
      <Paper zDepth={2}>
        <div className={s.wrap}>
          {images &&
            images.length > 0 &&
            images.map((item, index) =>
              <ProfileImageItem
                key={index}
                index={index}
                image={item}
                clickItemCallback={this.handleClickItem}
                removeItemCallback={this.handleRemoveProfileImage}
                itemStyle={this.props.itemStyle}
                activeBorderStyle={this.props.activeBorderStyle}
                style={this.props.style}
              />,
            )}
          <div className={s.profileAddWrap}>
            <input
              id="file"
              type="file"
              onChange={this.handleAddProfileImage.bind(this)}
              ref={input => (this.fileElement = input)}
            />
            <CircularProgress
              className={uploading ? s.isLoading : s.isNotLoading}
              size={80}
              thickness={5}
              style={{ position: 'absolute', top: '10%', left: '10%' }}
            />
          </div>
        </div>
      </Paper>
    );
  }
}

ProfileImageUpload.propTypes = {
  profileImages: PropTypes.array.isRequired,
  activeBorderStyle: PropTypes.object,
  itemStyle: PropTypes.object,
  removeButtonStyle: PropTypes.object,
  style: PropTypes.object,
  onChange: PropTypes.func,
  userTypeId: PropTypes.number,
  userId: PropTypes.number,
};

ProfileImageUpload.defaultProps = {
  profileImages: [],
  activeBorderStyle: {},
  style: {},
  onChange: () => {},
};

export default withStyles(s)(ProfileImageUpload);
