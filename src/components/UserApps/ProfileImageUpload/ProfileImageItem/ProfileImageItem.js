import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ProfileImageItem.css';
import RaisedButton from 'material-ui/RaisedButton';

class ProfileImageItem extends React.Component {
  _handleClickItem = event => {
    event.stopPropagation();
    const { image } = this.props;
    this.props.clickItemCallback(null, image.generatedKey);
  };

  _handleClickCancel = event => {
    event.stopPropagation();
    const { image } = this.props;
    this.props.removeItemCallback(null, image.generatedKey);
  };

  // TODO fileUrl = null 인 경우 기존 이미지를 제거해야한다.
  getFileUrl = () => {
    const image = this.props.image;
    if (!image) {
      return null;
    }
    if (!image.profileFile) {
      return null;
    }
    if (!image.profileFile.fileUrl) {
      return null;
    }
    return image.profileFile.fileUrl;
  };

  render() {
    const image = this.props.image;
    let isMain = false;

    if (image.profileImageType) {
      const type = image.profileImageType;
      if (type === 'sitterProfileMain') {
        isMain = true;
      }
      if (type === 'parentProfileMain') {
        isMain = true;
      }
    }

    return (
      <div className={s.item} onClick={this._handleClickItem}>
        {isMain ? <div className={s.mainTag}> 메인 </div> : null}
        <img width="100" height="100" src={this.getFileUrl()} />
        <RaisedButton
          onClick={this._handleClickCancel}
          fullWidth
          label="삭제"
        />
      </div>
    );
  }
}

ProfileImageItem.propTypes = {
  image: PropTypes.object.isRequired,
  clickItemCallback: PropTypes.func.isRequired,
  removeItemCallback: PropTypes.func.isRequired,
  index: PropTypes.number.isRequired,
  activeBorderStyle: PropTypes.object,
  itemStyle: PropTypes.object,
  style: PropTypes.object,
  removeButtonStyle: PropTypes.object,
};

export default withStyles(s)(ProfileImageItem);
