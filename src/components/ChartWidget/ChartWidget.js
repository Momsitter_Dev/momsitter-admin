import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ChartWidget.css';
import Paper from 'material-ui/Paper';

class ChartWidget extends React.Component {
  render() {
    return (
      <Paper zDepth={1}>
        <div className={s.root}>
          {this.props.children}
        </div>
      </Paper>
    );
  }
}

ChartWidget.propTypes = {};

ChartWidget.defaultProps = {};

export default withStyles(s)(ChartWidget);
