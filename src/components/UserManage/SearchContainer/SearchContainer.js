import { compose } from 'recompose';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';

import WarningIcon from 'material-ui/svg-icons/alert/warning';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './SearchContainer.css';
import SearchForm from '../SearchForm';
import SearchResult from '../SearchResult';
import Request from '../../../util/Request';
import AddUserDialog from '../../Dialog/AddUserDialog';
import AddTestUserDialog from '../../AddTestUserDialog';

const propTypes = {
  onClickCard: PropTypes.func,
  userId: PropTypes.string,
};
const defaultProps = {
  onClickCard: () => {},
  userId: null,
};

class SearchContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userList: [],
      addDialog: false,
      addTestUserDialog: false,
      onLoading: true,
      onError: false,
    };
    this.searchOptions = {};
    this.pageInfo = {};
    this.doSearch = this.doSearch.bind(this);
  }

  componentDidMount() {
    const { userId } = this.props;
    this.doSearch({
      searchText: userId || '',
      searchCategory: 'userId',
      subSearchCategory: '',
      subSearchText: '',
    });
    this.setState({ onLoading: false });
  }

  /**
   * @param data
   * @param data.searchText
   * @param data.searchCategory
   */
  onSearch = data => {
    const { onLoading } = this.state;
    if (onLoading) {
      return false;
    }
    this.setState({ onLoading: true });
    this.doSearch(data);
    this.setState({ onLoading: false });
  };

  onClickSeeMore = () => {
    const { onLoading } = this.state;
    if (onLoading) {
      return false;
    }
    this.setState({ onLoading: true });
    this.doSearch(this.searchOptions, 'seeMore');
    this.setState({ onLoading: false });
  };

  doSearch = async function (data, type) {
    this.searchOptions = data;
    const {
      searchText,
      searchCategory,
      subSearchText,
      subSearchCategory,
    } = data;

    if (type) {
      const { currentPage } = this.pageInfo;
      const response = await Request(
        `api/users?searchText=${searchText}&subSearchText=${subSearchText}&searchCategory=${searchCategory}&subSearchCategory=${subSearchCategory}&requestPage=${currentPage +
          1}`,
        { method: 'GET' },
      );
      if (response && response.result === 'success') {
        const { userList } = this.state;
        if (response.data.rows.length > 0) {
          this.pageInfo = response.page;
        }
        this.setState({ userList: userList.concat(response.data.rows) });
      }
    } else {
      const response = await Request(
        `api/users?searchText=${searchText}&subSearchText=${subSearchText}&searchCategory=${searchCategory}&subSearchCategory=${subSearchCategory}`,
        { method: 'GET' },
      );
      if (response && response.result === 'success') {
        if (response.data.rows.length > 0) {
          this.pageInfo = response.page;
        }
        this.setState({ userList: response.data.rows });
      }
    }
  };

  onClickCard = (userId, userTypeId) => {
    this.props.onClickCard(userId, userTypeId);
  };

  onClickAddUser = () => {
    const { addDialog } = this.state;
    if (!addDialog) {
      this.setState({ addDialog: true });
    }
  };

  onOpenAddTestUserDialog = () => {
    return this.setState({
      addTestUserDialog: true,
    });
  };

  onCloseAddTestUserDialog = () => {
    return this.setState({
      addTestUserDialog: false,
    });
  };

  onCloseAddDialog = () => {
    this.setState({ addDialog: false });
  };

  renderSearchResult = () => {
    const { userList, onLoading, onError } = this.state;
    if (onError) {
      return (
        <Paper>
          <div className={s.paperWrap}>
            <div> 오류가 발생했습니다. </div>
            <div> 다시 시도해 주세요. </div>
          </div>
        </Paper>
      );
    }

    if (onLoading) {
      return (
        <Paper>
          <div className={s.paperWrap}>
            <CircularProgress size={50} />
            <div> 데이터를 불러오는 중입니다 ... </div>
          </div>
        </Paper>
      );
    }

    if (userList && userList instanceof Array && userList.length > 0) {
      return (
        <SearchResult
          searchResult={userList}
          searchResultPageInfo={this.pageInfo}
          onClickCard={this.onClickCard}
          onClickSeeMore={this.onClickSeeMore}
        />
      );
    }

    if (userList && userList instanceof Array && userList.length === 0) {
      return (
        <Paper>
          <div className={s.paperWrap}>
            <div>
              <WarningIcon style={{ width: '100px', height: '100px', marginBottom: '10px' }} />
            </div>
            <div>
              <div> 검색 결과가 없습니다. </div>
            </div>
          </div>
        </Paper>
      );
    }

    // ERROR CASE
    return (
      <Paper>
        <div className={s.paperWrap}>
          오류 입니다. 다시 시도해 주세요.
        </div>
      </Paper>
    );
  };

  render() {
    const { addDialog, addTestUserDialog } = this.state;
    return (
      <div>
        <Grid container spacing={16}>
          <Grid item xs={12}>
            <Button variant={'raised'} onClick={this.onClickAddUser}> 회원 추가 </Button>
            <Button variant={'raised'} onClick={this.onOpenAddTestUserDialog}> 테스트 계정 추가 </Button>
          </Grid>
          <Grid item xs={12}>
            <SearchForm onSearch={this.onSearch} />
          </Grid>
          <Grid item xs={12}>
            {this.renderSearchResult()}
          </Grid>
        </Grid>
        <AddUserDialog open={addDialog} onClose={this.onCloseAddDialog} />
        <AddTestUserDialog open={addTestUserDialog} onClose={this.onCloseAddTestUserDialog} />
      </div>
    );
  }
}

SearchContainer.propTypes = propTypes;
SearchContainer.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(SearchContainer);
