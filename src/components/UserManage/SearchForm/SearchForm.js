import uuid from 'uuid/v4';
import Paper from 'material-ui/Paper';
import MenuItem from 'material-ui/MenuItem';
import SelectField from 'material-ui/SelectField';
import RaisedButton from 'material-ui/RaisedButton';
import SearchIcon from 'material-ui/svg-icons/action/search';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './SearchForm.css';
import InputField from '../../InputField';

class SearchForm extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      searchText: '',
      searchCategory: 'userId',
      subSearchText: '',
      subSearchCategory: '',
    };
  }

  onChangeSearchText = (type, value) => {
    if (type === 'main') {
      this.setState({ searchText: value });
    } else {
      this.setState({ subSearchText: value });
    }
  };

  onChangeSearchCategory = (type, value) => {
    if (type === 'main') {
      this.setState({ searchCategory: value });
    } else {
      this.setState({ subSearchCategory: value });
    }
  };

  onClickSearch = () => {
    // TODO Validate input value
    this.props.onSearch(this.state);
  };

  onKeyPressEnter = (event) => {
    if (event.key === 'Enter') {
      event.preventDefault();
      this.props.onSearch(this.state);
    }
  };

  render() {
    const { searchCategory, searchText, subSearchText, subSearchCategory } = this.state;
    return (
      <Paper zDepth={1}>
        <div className={s.root}>
          <div className="box-header bg-color-primary">회원 검색</div>
          <div className={s.formWrap}>
            <div className={s.formRow}>
              <div className={s.formItem}>
                <SelectField
                  fullWidth
                  onChange={(event, index, value) => this.onChangeSearchCategory('main', value)}
                  value={searchCategory}
                >
                  <MenuItem primaryText="회원 번호" value="userId" />
                  <MenuItem primaryText="이름" value="userName" />
                  <MenuItem primaryText="휴대폰 번호" value="userPhone" />
                </SelectField>
              </div>
              <div className={s.formItem}>
                <InputField
                  id={uuid()}
                  value={searchText}
                  onChange={(value) => this.onChangeSearchText('main', value)}
                  onKeyPress={this.onKeyPressEnter}
                  style={{ width: '100%' }}
                />
              </div>
            </div>
            <div className={s.formRow}>
              <SelectField
                fullWidth
                onChange={(event, index, value) => this.onChangeSearchCategory('sub', value)}
                value={subSearchCategory}
              >
                <MenuItem primaryText="선택 안함" value="" />
                <MenuItem primaryText="회원 번호" value="userId" />
                <MenuItem primaryText="이름" value="userName" />
                <MenuItem primaryText="휴대폰 번호" value="userPhone" />
              </SelectField>
              <InputField
                id={uuid()}
                value={subSearchText}
                onChange={(value) => this.onChangeSearchText('sub', value)}
                onKeyPress={this.onKeyPressEnter}
                style={{ width: '100%' }}
              />
            </div>
            <div className={s.buttonWrap}>
              <RaisedButton
                fullWidth
                onClick={this.onClickSearch}
                icon={<SearchIcon />}
                primary={true}
              />
            </div>
          </div>
        </div>
      </Paper>
    );
  }
}

SearchForm.propTypes = {
  onSearch: PropTypes.func.isRequired,
};

SearchForm.defaultProps = {
};

export default withStyles(s)(SearchForm);
