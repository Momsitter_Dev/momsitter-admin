import { compose } from 'recompose';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './UserManage.css';
import SearchContainer from './SearchContainer';
import WidgetContainer from './WidgetContainer';

const propTypes = {
  userId: PropTypes.string,
};
const defaultProps = {
  userId: null,
};

class UserManage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentUserId: null,
      currentUserType: null,
    };
  }

  onClickCard = (userId, userTypeId) => {
    const { currentUserId } = this.state;
    if (currentUserId !== userId) {
      return this.setState({
        currentUserId: userId,
        currentUserType: userTypeId,
      });
    }
    return null;
  };

  render() {
    const { userId } = this.props;
    const { currentUserId, currentUserType } = this.state;
    return (
      <div>
        <Grid container spacing={16}>
          <Grid item xs={12} sm={3}>
            <SearchContainer
              userId={userId}
              onClickCard={this.onClickCard}
            />
          </Grid>
          <Grid item xs={12} sm={9}>
            <WidgetContainer
              userId={currentUserId}
              userTypeId={currentUserType}
            />
          </Grid>
        </Grid>
      </div>
    );
  }
}

UserManage.propTypes = propTypes;
UserManage.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(UserManage);
