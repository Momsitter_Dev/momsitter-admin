/* eslint-disable prettier/prettier */
import moment from 'moment';
import uuid from 'uuid/v4';
import { compose } from 'recompose';
import {
  Grid,
  Typography,
  Button,
  Select,
  MenuItem,
  CircularProgress,
  Paper,
  withStyles as withMaterialStyles,
  TextField,
  IconButton,
} from '@material-ui/core';
import WarningIcon from 'material-ui/svg-icons/alert/warning';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './BasicInfo.css';
import Request from '../../../../util/Request';
import InputField from '../../../InputField';
import ProfileImageUpload from '../../../UserApps/ProfileImageUpload';
import UserConnectType from '../../../../util/const/UserConnectType';
import SimpleDateFormat from '../../../../util/dataFormat/SimpleDateFormat';
import EditPasswordDialog from '../../../Dialog/EditPasswordDialog';
import ResponseResultTypeGroup from '../../../../util/const/ResponseResultTypeGroup';
import clientConfig from '../../../../config/clientConfig';
import Widget from '../../../Widget';
import UserStatusGroup from '../../../../util/const/UserStatusGroup';
import UserOwnPhoneTypeGroup from '../../../../util/const/UserOwnPhoneTypeGroup';

const propTypes = {
  userId: PropTypes.number.isRequired,
  userTypeId: PropTypes.number.isRequired,
  title: PropTypes.string,
  openNotificationDialog: PropTypes.func.isRequired,
};
const defaultProps = {
  title: '',
};

class BasicInfo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      basicInfo: null,
      onLoading: true,
      onError: false,
      editPasswordDialog: false,
      editPasswordData: '',
      editPasswordDialogLoading: false,
    };
    this.modified = {};
    this.originInfo = null;

    this.getBasicInfo = this.getBasicInfo.bind(this);
    this.onRefresh = this.onRefresh.bind(this);
    this.onClickBackdoor = this.onClickBackdoor.bind(this);
    this.onSave = this.onSave.bind(this);
    this.onRefresh = this.onRefresh.bind(this);
    this.onSaveEditPassword = this.onSaveEditPassword.bind(this);
  }

  async componentDidMount() {
    await this.getBasicInfo(this.props.userId);
  }

  async componentWillReceiveProps(nextProps) {
    if (this.props.userId !== nextProps.userId) {
      await this.getBasicInfo(nextProps.userId);
    }
  }

  getBasicInfo = async function(userId) {
    this.setState({
      onLoading: true,
    });
    const response = await Request(`api/users/${userId}/basicInfo`, { method: 'GET' });
    if (response && response.result === 'success') {
      this.originInfo = response.data;
      if (typeof window !== 'undefined') {
        const self = this;
        const timerIndex = window.setTimeout(() => {
          self.setState({
            basicInfo: Object.assign({}, response.data),
            onLoading: false,
          });
          window.clearTimeout(timerIndex);
        }, 700);
      } else {
        this.setState({
          basicInfo: Object.assign({}, response.data),
          onLoading: false,
        });
      }
    } else {
      this.setState({
        onError: true,
        onLoading: false,
      });
    }
  };

  onClickBackdoor = async function(userId) {
    if (userId) {
      try {
        const response = await Request(`api/manage/user/token?userId=${userId}`, {
          method: 'GET',
        });

        if (response.result === ResponseResultTypeGroup.SUCCESS) {
          this.props.openNotificationDialog(
            '알림',
            <div>
              <div> 새창에서 가상 로그인이 진행됩니다. (팝업을 허용해 주세요.) </div>
              <div>{JSON.stringify(response)}</div>
            </div>,
          );
          window.open(`${clientConfig.apiServer}/api/token/redirect?token=${response.data.token}`);
          return true;
        }
        throw new Error('request fail');
      } catch (err) {
        console.log(err);
        return this.props.openNotificationDialog(
          '오류',
          '해당 회원으로 로그인 하기 위해 필요한 정보를 생성할 수 없습니다.',
        );
      }
    }
    return null;
  };

  onChangeProfileImage = (event, data) => {
    const { basicInfo } = this.state;
    basicInfo.userProfileImage = data;
    this.setState({ basicInfo });
  };

  onSave = async function() {
    const { userId } = this.props;
    const { basicInfo } = this.state;
    this.setState({ onLoading: true });

    const response = await Request(`api/users/${userId}/basicInfo`, {
      method: 'PUT',
      data: basicInfo,
    });
    if (response && response.result === 'success') {
      this.props.openNotificationDialog('알림', '저장되었습니다.');
      this.onRefresh();
    } else {
      this.props.openNotificationDialog('오류.', '네트워크 오류입니다.');
      this.setState({ onLoading: false, onError: true });
    }
  };

  onChangeUserBasicInfo = (type, value) => {
    const { basicInfo } = this.state;
    if (type === 'userRole') {
      basicInfo[type] = value.map(item => {
        return {
          userTypeId: item,
        };
      });
    } else {
      basicInfo[type] = value;
    }
    return this.setState({ basicInfo });
  };

  onRefresh = async function() {
    const { userId } = this.props;
    await this.getBasicInfo(userId);
  };

  onClickChangePassword = () => {
    return this.openEditPasswordDialog();
  };

  onClickWarning = () => {
    if (typeof window !== 'undefined') {
      if (window.confirm('정보를 다시 로드하시겠습니까?')) {
        this.setState(
          {
            onError: false,
          },
          () => {
            this.onRefresh();
          },
        );
      }
    }
  };

  openEditPasswordDialog = () => {
    return this.setState({
      editPasswordData: '',
      editPasswordDialog: true,
      editPasswordDialogLoading: false,
    });
  };

  closeEditPasswordDialog = () => {
    return this.setState({
      editPasswordData: '',
      editPasswordDialog: false,
      editPasswordDialogLoading: false,
    });
  };

  changeEditPasswordDialogData = data => {
    return this.setState({
      editPasswordData: data,
    });
  };

  onSaveEditPassword = async function() {
    const { editPasswordData } = this.state;
    const { userId } = this.props;
    const response = await Request(`api/users/${userId}/password`, {
      method: 'PUT',
      data: { userPassword: editPasswordData },
    }).catch(err => {
      console.log(err);
      return false;
    });
    if (response && response.result === ResponseResultTypeGroup.SUCCESS) {
      this.closeEditPasswordDialog();
      this.props.openNotificationDialog('저장되었습니다.');
    } else {
      this.props.openNotificationDialog('네트워크 오류입니다.');
    }
  };

  renderIsOwnPhone = isOwnPhone => {
    const buttons = Object.values(UserOwnPhoneTypeGroup).map((item, index) => {
      return (
        <Grid key={`user-status-button-${index}`} item xs={6}>
          <Button
            fullWidth
            variant={'raised'}
            color={isOwnPhone === item ? 'primary' : 'default'}
            onClick={() => this.onChangeUserBasicInfo('isOwnPhone', item)}
          >
            {item ? '본인명의' : '본인명의 아님'}
          </Button>
        </Grid>
      );
    });

    return (
      <Grid container spacing={0}>
        {buttons}
      </Grid>
    );
  };

  renderUserStatus = userStatus => {
    const userStatusLabels = {
      [UserStatusGroup.ACTIVE]: '활성',
      [UserStatusGroup.INACTIVE]: '비활성',
      [UserStatusGroup.OUT]: '탈퇴',
    };

    const buttons = Object.values(UserStatusGroup).map((item, index) => {
      return (
        <Grid key={`user-status-button-${index}`} item xs={4}>
          <Button
            fullWidth
            variant={'raised'}
            color={userStatus === item ? 'primary' : 'default'}
            onClick={() => this.onChangeUserBasicInfo('userStatus', item)}
          >
            {userStatusLabels[item]}
          </Button>
        </Grid>
      );
    });

    return (
      <Grid container spacing={0}>
        {buttons}
      </Grid>
    );
  };

  renderBasicInfo = (onError, onLoading, userId, userTypeId, basicInfo) => {
    if (onError) {
      return (
        <div style={{ textAlign: 'center' }}>
          <IconButton onClick={this.onClickWarning}>
            <WarningIcon />
          </IconButton>
          <div style={{ marginTop: '10px' }}> 오류가 발생했습니다.</div>
        </div>
      );
    }
    if (onLoading) {
      return (
        <div style={{ textAlign: 'center' }}>
          <CircularProgress size={80} />
          <div style={{ marginTop: '10px' }}> 데이터를 불러오는 중입니다.</div>
        </div>
      );
    }

    return (
      <Grid container spacing={16} alignItems={'center'}>
        <Grid item xs={12}>
          <Typography variant={'caption'}> 회원번호 </Typography>
        </Grid>
        <Grid item xs={12}>
          {' '}
          {userId}{' '}
        </Grid>

        <Grid item xs={12}>
          <Typography variant={'caption'}> 가입일자 </Typography>
        </Grid>
        <Grid item xs={12}>
          {SimpleDateFormat(basicInfo.userSignUpDate)}
        </Grid>

        <Grid item xs={12}>
          <Typography variant={'caption'}> 생년월일 </Typography>
        </Grid>
        <Grid item xs={12}>
          <TextField
            type={'date'}
            fullWidth
            defaultValue={moment(basicInfo.userBirthday, 'YYYYMMDD').format('YYYY-MM-DD')}
            onChange={event => this.onChangeUserBasicInfo('userBirthday', moment(event.target.value, 'YYYY-MM-DD').format('YYYYMMDD'))}
          />
        </Grid>

        <Grid item xs={12}>
          <Typography variant={'caption'}> 이름 </Typography>
        </Grid>
        <Grid item xs={12}>
          <InputField
            style={{ width: '100%' }}
            id={uuid()}
            value={basicInfo.userName || ''}
            onChange={value => this.onChangeUserBasicInfo('userName', value)}
          />
        </Grid>

        <Grid item xs={12}>
          <Typography variant={'caption'}> 상태 </Typography>
        </Grid>
        <Grid item xs={12}>
          {this.renderUserStatus(basicInfo.userStatus)}
        </Grid>

        <Grid item xs={12}>
          <Typography variant={'caption'}> 본인명의 가입여부 </Typography>
        </Grid>
        <Grid item xs={12}>
          {this.renderIsOwnPhone(basicInfo.isOwnPhone)}
        </Grid>

        <Grid item xs={12}>
          <Typography variant={'caption'}> 프로필 사진 </Typography>
        </Grid>
        <Grid item xs={12}>
          <div className={s.profileImageWrap}>
            <ProfileImageUpload
              userId={userId}
              userTypeId={userTypeId}
              profileImages={basicInfo.userProfileImage}
              onChange={this.onChangeProfileImage}
            />
          </div>
        </Grid>

        <Grid item xs={12}>
          <Typography variant={'caption'}> 가입방법 </Typography>
        </Grid>
        <Grid item xs={12}>
          <Select
            id={uuid()}
            style={{ width: '100%' }}
            value={basicInfo.userConnectType}
            onChange={event => this.onChangeUserBasicInfo('userConnectType', event.target.value)}
          >
            <MenuItem value={UserConnectType.NAVER}> 네이버 </MenuItem>
            <MenuItem value={UserConnectType.KAKAO}> 카카오 </MenuItem>
            <MenuItem value={UserConnectType.FACEBOOK}> 페이스북 </MenuItem>
            <MenuItem value={UserConnectType.EMAIL}> 이메일 </MenuItem>
          </Select>
        </Grid>

        <Grid item xs={12}>
          <Typography variant={'caption'}> 로그인 ID </Typography>
        </Grid>
        <Grid item xs={12}>
          <InputField
            style={{ width: '100%' }}
            id={uuid()}
            value={basicInfo.userKey || ''}
            onChange={value => this.onChangeUserBasicInfo('userKey', value)}
          />
        </Grid>

        <Grid item xs={12}>
          <Typography variant={'caption'}> 비밀번호 </Typography>
        </Grid>
        <Grid item xs={12}>
          <Grid container spacing={16}>
            <Grid item xs={12}>
              {' '}
              {basicInfo.userPassword}{' '}
            </Grid>
            <Grid item xs={12}>
              <Button fullWidth variant={'raised'} onClick={this.onClickChangePassword}>
                변경하기
              </Button>
            </Grid>
          </Grid>
        </Grid>

        <Grid item xs={12}>
          <Typography variant={'caption'}> 휴대폰번호 </Typography>
        </Grid>
        <Grid item xs={12}>
          <InputField
            style={{ width: '100%' }}
            id={uuid()}
            value={basicInfo.userPhone}
            onChange={value => this.onChangeUserBasicInfo('userPhone', value)}
          />
        </Grid>

        <Grid item xs={12}>
          <Typography variant={'caption'}> 성별 </Typography>
        </Grid>
        <Grid item xs={12}>
          <Select
            id={uuid()}
            style={{ width: '100%' }}
            value={basicInfo.userGender}
            onChange={event => this.onChangeUserBasicInfo('userGender', event.target.value)}
          >
            <MenuItem value="man"> 남자 </MenuItem>
            <MenuItem value="women"> 여자 </MenuItem>
          </Select>
        </Grid>

        <Grid item xs={12}>
          <Typography variant={'caption'}> 회원타입 </Typography>
        </Grid>
        <Grid item xs={12}>
          <Select
            id={uuid()}
            multiple
            style={{ width: '100%' }}
            value={basicInfo.userRole.map(item => {
              return item.userTypeId;
            })}
            onChange={event => this.onChangeUserBasicInfo('userRole', event.target.value)}
          >
            <MenuItem value={1}> 시터 - 대학생 </MenuItem>
            <MenuItem value={2}> 시터 - 선생님 </MenuItem>
            <MenuItem value={3}> 시터 - 엄마 </MenuItem>
            <MenuItem value={4}> 시터 - 일반 </MenuItem>
            <MenuItem value={5}> 부모 회원 </MenuItem>
          </Select>
        </Grid>
      </Grid>
    );
  };

  render() {
    const { userTypeId, userId, title } = this.props;
    const {
      onError,
      onLoading,
      basicInfo,
      editPasswordData,
      editPasswordDialog,
      editPasswordDialogLoading,
    } = this.state;
    return (
      <Widget
        title={title}
        onRefresh={this.onRefresh}
        actions={[
          <Button
            key={'widget-basic-info-btn-save'}
            variant={'raised'}
            color={'primary'}
            onClick={this.onSave}
            style={{ marginRight: '10px' }}
          >
            저장
          </Button>,
          <Button
            key={'widget-basic-info-btn-backdoor'}
            variant={'raised'}
            color={'primary'}
            onClick={() => this.onClickBackdoor(userId)}
            style={{ marginRight: '10px' }}
          >
            가상로그인
          </Button>,
        ]}
      >
        {this.renderBasicInfo(onError, onLoading, userId, userTypeId, basicInfo)}
        <EditPasswordDialog
          open={editPasswordDialog}
          data={editPasswordData}
          onLoading={editPasswordDialogLoading}
          onChange={this.changeEditPasswordDialogData}
          onClose={this.closeEditPasswordDialog}
          onSave={this.onSaveEditPassword}
        />
      </Widget>
    );
  }
}

BasicInfo.propTypes = propTypes;
BasicInfo.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(BasicInfo);
