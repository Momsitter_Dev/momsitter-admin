import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Applications.css';
import Widget from '../Widget';
import {Tabs, Tab} from 'material-ui/Tabs';
import ApplicationItem from './ApplicationItem';
import Request from '../../../../util/Request';
import Paper from 'material-ui/Paper';
import classnames from 'classnames';
import CircularProgress from 'material-ui/CircularProgress';
import WarningIcon from 'material-ui/svg-icons/alert/warning';
import IconButton from 'material-ui/IconButton';

class Applications extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      applications: null,
      onLoading: true,
      onError: false,
    };
    this.getApplications = this.getApplications.bind(this);
    this.onRefresh = this.onRefresh.bind(this);
  }

  async componentDidMount() {
    await this.getApplications(this.props.userId);
  }

  async componentWillReceiveProps(nextProps) {
    if (this.props.userId !== nextProps.userId) {
      await this.getApplications(nextProps.userId);
    }
  }

  getApplications = async function (userId) {
    this.setState({
      onLoading: true,
    });
    const response = await Request(`api/users/${userId}/applications`, { method: 'GET' });
    if (response) {
      if (typeof window !== 'undefined') {
        const self = this;
        const timerIndex = window.setTimeout(() => {
          self.setState({
            applications: response.data,
            onLoading: false,
          });
          window.clearTimeout(timerIndex);
        }, 700);
      } else {
        this.setState({
          applications: response.data,
          onLoading: false,
        });
      }
    } else {
      this.setState({
        onError: true,
        onLoading: false,
      });
    }
  };

  onClickWarning = () => {
    if (typeof window !== 'undefined') {
      if (window.confirm('정보를 다시 로드하시겠습니까?')) {
        this.setState({
          onError: false
        }, () => {
          this.onRefresh();
        });
      }
    }
  };

  onRefresh = async function () {
    await this.getApplications(this.props.userId);
  };

  renderSendedApplications = (onError, onLoading, applications) => {
    let ui = null;
    if (onError) {
      ui = (
        <Paper style={{ textAlign: 'center' }}>
          <IconButton onClick={this.onClickWarning}>
            <WarningIcon />
          </IconButton>
          <div style={{ marginTop: '10px' }}> 오류가 발생했습니다. </div>
        </Paper>
      );
    } else {
      if (onLoading) {
        ui = (
          <Paper style={{ textAlign: 'center' }}>
            <CircularProgress size={80} thickness={5} />
            <div style={{ marginTop: '10px' }}> 데이터를 불러오는 중입니다. </div>
          </Paper>
        );
      } else {
        if (applications && applications.send && applications.send.length > 0) {
          ui = applications.send.map((item, index) => {
            return (
              <ApplicationItem
                key={index}
                type="receiver"
                applicationData={item}
              />);
          });
        } else {
          ui = (
            <Paper>
              <div className={classnames(s.no_content)}>해당 회원이 지원한 이력이 없습니다.</div>
            </Paper>
          );
        }
      }
    }
    return ui;
  };

  renderReceivedApplications = (onError, onLoading, applications) => {
    let ui = null;
    if (onError) {
      ui = (
        <Paper style={{ textAlign: 'center' }}>
          <IconButton onClick={this.onClickWarning}>
            <WarningIcon />
          </IconButton>
          <div style={{ marginTop: '10px' }}> 오류가 발생했습니다. </div>
        </Paper>
      );
    } else {
      if (onLoading) {
        ui = (
          <Paper style={{ textAlign: 'center' }}>
            <CircularProgress size={80} thickness={5} />
            <div style={{ marginTop: '10px' }}> 데이터를 불러오는 중입니다. </div>
          </Paper>
        );
      } else {
        if (applications && applications.receive && applications.receive.length > 0) {
          ui = applications.receive.map((item, index) => {
            return (
              <ApplicationItem
                key={index}
                type="applicant"
                applicationData={item}
              />);
          });
        } else {
          ui = (
            <Paper>
              <div className={classnames(s.no_content)}>해당 회원에게 지원한 신청서가 없습니다.</div>
            </Paper>
          );
        }
      }
    }
    return ui;
  };

  render() {
    const { tabTitle } = this.props;
    const { onError, onLoading, applications } = this.state;
    return (
      <Widget
        title={this.props.title}
        onRefresh={this.onRefresh}
      >
        <Tabs>
          <Tab label={tabTitle[0]}>{this.renderSendedApplications(onError, onLoading, applications)}</Tab>
          <Tab label={tabTitle[1]}>{this.renderReceivedApplications(onError, onLoading, applications)}</Tab>
        </Tabs>
      </Widget>
    );
  }
}

Applications.propTypes = {
  userId: PropTypes.number,
  title: PropTypes.string,
  tabTitle: PropTypes.array.isRequired,
};

Applications.defaultProps = {
  tabTitle: ['내가 지원한', '내게 신청한'],
};

export default withStyles(s)(Applications);
