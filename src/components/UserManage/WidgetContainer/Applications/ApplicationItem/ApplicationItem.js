import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ApplicationItem.css';
import Paper from 'material-ui/Paper';
import classnames from 'classnames';
import Avatar from 'material-ui/Avatar';
import ApplyStatusGroup from '../../../../../util/const/ApplyStatusGroup';
import ApplyDeclineTypeTextGroup from '../../../../../util/const/ApplyDeclineTypeTextGroup';
import SimpleDateFormat from '../../../../../util/dataFormat/SimpleDateFormat';
import EnhancedButton from 'material-ui/internal/EnhancedButton';

class ApplicationItem extends React.Component {
  onClickItem = (userId) => {
    window.open('/users?userId=' + userId);
  };

  renderApplicationStatus = (data) => {
    let applyStatusStr = null;
    let declineUI = null;
    if (data.applyStatus === ApplyStatusGroup.REQUEST) {
      applyStatusStr = '요청';
    }
    if (data.applyStatus === ApplyStatusGroup.DECLINE) {
      applyStatusStr = '거절';
      declineUI = (
        <div className={s.contentRow}>
          <label> 거절 사유 </label>
          <span>{ApplyDeclineTypeTextGroup[data.declineType] || '없음'}</span>
        </div>
      );
    }
    if (data.applyStatus === ApplyStatusGroup.ACCEPT) {
      applyStatusStr = '승인';
    }
    if (data.applyStatus === ApplyStatusGroup.TUNING) {
      applyStatusStr = '조율';
    }

    return (
      <div>
        <div className={s.contentRow}>
          <label> 신청 상태 </label>
          <span>{applyStatusStr}</span>
        </div>
        {declineUI}
      </div>
    );
  };

  renderAvatar = () => {
    const { applicationData, type } = this.props;
    try {
      return (
        <Avatar
          size={70}
          src={applicationData[type].userProfileImage[0].profileFile.fileUrl}
        />
      );
    } catch (err) {
      return (<div style={{ width: '70px', height: '70px'}}> 프로필 이미지 없음 </div>);
    }
  };

  render() {
    const { applicationData, type } = this.props;
    return (
      <EnhancedButton
        style={{ width: '100%' }}
        onClick={() => this.onClickItem(applicationData[type + 'Id'])}
      >
        <Paper zDepth={3}>
          <div className={classnames(s.wrap)}>
            <div className={s.contentWrap}>
              <div className={classnames(s.itemUserInfo)}>
                <div> {this.renderAvatar()} </div>
                <div className={s.userName}>
                  <span>{applicationData[type] ? applicationData[type].userName : null}</span>
                </div>
              </div>
              <div className={s.content}>
                <div className={s.contentRow}>
                  <label> 회원 번호 </label>
                  <span>{applicationData[type + 'Id']} </span>
                </div>
                {this.renderApplicationStatus(applicationData)}
                <div className={s.contentRow}>
                  <label> 지원 일시 </label>
                  <span>{SimpleDateFormat(applicationData.applyDate)}</span>
                </div>
                <div className={s.contentRow}>
                  <label> 응답 일시 </label>
                  <span>{SimpleDateFormat(applicationData.updateDate)}</span>
                </div>
              </div>
            </div>
          </div>
        </Paper>
      </EnhancedButton>
    );
  }
}

ApplicationItem.propTypes = {
  applicationData: PropTypes.object,
  type: PropTypes.string,
};

ApplicationItem.defaultProps = {};

export default withStyles(s)(ApplicationItem);
