import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CertificationItem.css';
import Paper from 'material-ui/Paper';
import FlatButton from 'material-ui/FlatButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import CertificateTypeIdGroup from '../../../../../util/const/CertificateTypeIdGroup';
import CertificateStatusGroup from '../../../../../util/const/CertificateStatusGroup';
import SimpleDateFormat from '../../../../../util/dataFormat/SimpleDateFormat';

class CertificationItem extends React.Component {
  renderStatusDateLabel = (certStatus) => {
    let label = null;
    if (certStatus === CertificateStatusGroup.REJECTED) {
      label = '거절 일자';
    }
    if (certStatus === CertificateStatusGroup.APPROVED ) {
      label = '승인 일자';
    }
    return label;
  };

  renderCertData = (certData) => {
    let ui = '없음';
    if (certData) {
      ui = certData;
    }
    return (
      <div className={s.formRow}>
        <label>인증 데이터</label>
        <div className={s.formValue}>{ui}</div>
      </div>
    )
  };

  renderCertFileImage = (certFile) => {
    let ui = '없음';
    if (certFile && certFile.fileUrl && certFile.fileName) {
      ui = (
        <a target="_blank" href={certFile.fileUrl}>{certFile.fileName}</a>
      );
    }
    return (
      <div className={s.formRow}>
        <label>인증 파일</label>
        <div className={s.formValue}>{ui}</div>
      </div>
    );
  };

  render() {
    const { data } = this.props;
    const {
      certData,
      certId,
      certStatus,
      certTypeId,
      fileId,
      issueDate,
      regDate,
      updateDate,
      userId,
      certFile,
      certUser,
    } = data;
    return (
      <Paper zDepth={2}>
        <div className={s.root}>
          <div className={s.formRow}>
            <label>인증 번호</label>
            <div className={s.formValue}>{certId}</div>
          </div>
          <div className={s.formRow}>
            <label>인증 타입</label>
            <SelectField
              value={certTypeId}
              underlineStyle={{
                borderColor: '#d3d3d3',
              }}
              labelStyle={{ textAlign: 'right' }}
              style={{ width: '100%' }}
              menuItemStyle={{ textAlign: 'right' }}
            >
              <MenuItem value={CertificateTypeIdGroup.ENROLLMENT} primaryText="학교 인증" />
              <MenuItem value={CertificateTypeIdGroup.MEDICAL_EXAMINATION} primaryText="건강 인증" />
              <MenuItem value={CertificateTypeIdGroup.FAMILY_RELATION} primaryText="가족관계 인증" />
              <MenuItem value={CertificateTypeIdGroup.CHILDCARE} primaryText="자격증 인증" />
              <MenuItem value={CertificateTypeIdGroup.RESIDENT_REGISTRATION} primaryText="등초본 인증" />
              <MenuItem value={CertificateTypeIdGroup.PERSONALITY} primaryText="인성 인증" />
            </SelectField>
          </div>
          <div className={s.formRow}>
            <label>인증 상태</label>
            <SelectField
              value={certStatus}
              underlineStyle={{
                borderColor: '#d3d3d3',
              }}
              labelStyle={{ textAlign: 'right' }}
              style={{ width: '100%' }}
              menuItemStyle={{ textAlign: 'right' }}
            >
              <MenuItem value={CertificateStatusGroup.WAITING} primaryText="대기" />
              <MenuItem value={CertificateStatusGroup.APPROVED} primaryText="승인" />
              <MenuItem value={CertificateStatusGroup.REJECTED} primaryText="거절" />
              <MenuItem value={CertificateStatusGroup.NONE} primaryText="미 선택" />
            </SelectField>
          </div>
          {this.renderCertFileImage(certFile)}
          {/*this.renderCertData(certUser)*/}
          <div className={s.formRow}>
            <label>요청일자</label>
            <div className={s.formValue}>
              {SimpleDateFormat(regDate)}
            </div>
          </div>
          <div className={s.formRow}>
            <label>{this.renderStatusDateLabel(certStatus)}</label>
            <div className={s.formValue}>
              {SimpleDateFormat(updateDate)}
            </div>
          </div>
        </div>
      </Paper>
    );
  }
}

CertificationItem.propTypes = {
  data: PropTypes.object.isRequired,
};

CertificationItem.defaultProps = {};

export default withStyles(s)(CertificationItem);
