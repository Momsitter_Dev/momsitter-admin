import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import uuid from 'uuid/v4';
import CircularProgress from 'material-ui/CircularProgress';
import WarningIcon from 'material-ui/svg-icons/alert/warning';
import IconButton from 'material-ui/IconButton';
import s from './Certifications.css';
import Widget from '../Widget';
import Request from '../../../../util/Request';
import RaisedButton from 'material-ui/RaisedButton';
import CertificationItem from './CertificationItem';

class Certifications extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      certification: null,
      onLoading: true,
      onError: false,
    };
    this.getCertification = this.getCertification.bind(this);
    this.onRefresh = this.onRefresh.bind(this);
  }

  async componentDidMount() {
    await this.getCertification(this.props.userId);
  }

  async componentWillReceiveProps(nextProps) {
    if (this.props.userId !== nextProps.userId) {
      await this.getCertification(nextProps.userId);
    }
  }

  getCertification = async function (userId) {
    this.setState({ onLoading: true });
    const response = await Request(`api/users/${userId}/certifications`, { method: 'GET' });
    if (response) {
      if (typeof window !== 'undefined') {
        const self = this;
        const timerIndex = window.setTimeout(() => {
          self.setState({
            onLoading: false,
            certification: response.data
          });
          window.clearTimeout(timerIndex);
        }, 500);
      } else {
        this.setState({
          onLoading: false,
          certification: response.data
        });
      }
    } else {
      this.setState({
        onError: true,
        onLoading: false,
      });
    }
  };

  onRefresh = async function () {
    await this.getCertification(this.props.userId);
  };

  onClickWarning = () => {
    if (typeof window !== 'undefined') {
      if (window.confirm('정보를 다시 로드하시겠습니까?')) {
        this.setState({
          onError: false
        }, () => {
          this.onRefresh();
        });
      }
    }
  };

  onClickAddCert = () => {
    console.log("DEBUG : ON CLICK ADD CERT");
    //this.props.openNotificationDialog('알림', '개발중인 기능입니담.');
  };

  renderCertificationList = (onError, onLoading, certification) => {
    let ui = null;
    if (onError) {
      ui = (
        <div style={{ textAlign: 'center' }}>
          <IconButton onClick={this.onClickWarning}>
            <WarningIcon />
          </IconButton>
          <div style={{ marginTop: '10px' }}> 오류가 발생했습니다. </div>
        </div>
      );
    } else {
      if (onLoading) {
        ui = (
          <div style={{ textAlign: 'center' }}>
            <CircularProgress size={80} thickness={5} />
            <div style={{ marginTop: '10px' }}> 데이터를 불러오는 중입니다. </div>
          </div>
        );
      } else {
        if (certification && certification.length > 0) {
          ui = certification.map((item) => {
            return (
              <CertificationItem
                key={uuid()}
                data={item}
              />
            );
          });
        } else {
          ui = (
            <div>인증 내역이 없습니다.</div>
          );
        }
      }
    }
    return ui;
  };

  render() {
    const { onError, onLoading, certification } = this.state;
    return (
      <Widget
        title={this.props.title}
        onRefresh={this.onRefresh}
      >
        <div className={s.actions}>
          <RaisedButton
            label="인증 추가"
            onClick={this.onClickAddCert}
          />
        </div>
        <div>
          {this.renderCertificationList(onError, onLoading, certification)}
        </div>
      </Widget>
    );
  }
}

Certifications.propTypes = {
  title: PropTypes.string,
  userId: PropTypes.number,
  openNotificationDialog: PropTypes.func.isRequired,
};

Certifications.defaultProps = {};

export default withStyles(s)(Certifications);
