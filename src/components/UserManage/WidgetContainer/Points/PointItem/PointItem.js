import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './PointItem.css';

const propTypes = {};
const defaultProps = {};

class PointItem extends React.Component {
  render() {
    return (
      <div className={s.root}>
        <div className={s.container}>

        </div>
      </div>
    );
  }
}

PointItem.propTypes = propTypes;
PointItem.defaultProps = defaultProps;

export default withStyles(s)(PointItem);
