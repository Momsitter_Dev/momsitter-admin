import uuid from 'uuid/v4';
import moment from 'moment';

import { compose } from 'recompose';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import AppBar from '@material-ui/core/AppBar';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import Grow from '@material-ui/core/Grow';
import Zoom from '@material-ui/core/Zoom';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import ErrorIcon from '@material-ui/icons/Error';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import AddIcon from '@material-ui/icons/Add';

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Points.css';
import Widget from '../../../Widget';
import Request from '../../../../util/Request';
import ResponseResultTypeGroup from '../../../../util/const/ResponseResultTypeGroup';
import getFormattedMoney from '../../../../util/dataFormat/getFormattedMoney';

moment.locale('ko');

const styles = theme => ({
  fab: {
    position: 'absolute',
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 2,
  },
});

const POINT_TYPE = {
  SPENT: 'spent',
  RESERVED: 'reserved',
  IMPEND: 'impend',
  DELETED: 'deleted',
};

const propTypes = {
  title: PropTypes.string,
  userId: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]).isRequired,
  openNotificationDialog: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
};

const defaultProps = {
  title: '적립금 현황',
  children: null,
};

class Points extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      points: {
        reserved: [],
        spent: [],
        impend: [],
        deleted: [],
      },
      pending: true,
      error: false,
      currentTab: POINT_TYPE.RESERVED,
      deleteDialog: false,
      deleteDialogData: null,
      editDialog: false,
      editDialogData: null,
      addDialog: false,
      addDialogData: null,
      snackBar: {
        open: false,
        message: '',
      },
    };
    this.onRefresh = this.onRefresh.bind(this);
    this.onDeletePoint = this.onDeletePoint.bind(this);
    this.onEditPoint = this.onEditPoint.bind(this);
    this.onAddPoint = this.onAddPoint.bind(this);
    this.deleteInput = null;
    this.editInput = null;
    this.addPointPriceInput = null;
    this.addIssueReasonInput = null;
  }

  async componentDidMount() {
    try {
      const reserved = await this.getReservedPoint();
      const spent = await this.getSpentPoint();
      const impend = await this.getImpendPoint();
      const deleted = await this.getDeletedPoint();
      this.setState({
        points: {
          reserved,
          spent,
          impend,
          deleted,
        },
        pending: false,
        error: false,
      });
    } catch (err) {
      console.log(err);
      // TODO: pending & error ui
      this.setState({
        pending: false,
        error: true,
      });
    }
  }

  onRefresh = async function () {
    try {
      return this.setState({
        pending: true,
      }, async function () {
        const reserved = await this.getReservedPoint();
        const spent = await this.getSpentPoint();
        const impend = await this.getImpendPoint();
        const deleted = await this.getDeletedPoint();
        this.setState({
          points: {
            reserved,
            spent,
            impend,
            deleted,
          },
          pending: false,
          error: false,
        });
      });
    } catch (err) {
      return this.setState({
        error: true,
      });
    }
  };

  onClickDeletePoint = (deleteDialogData) => {
    return this.openDeleteDialog(deleteDialogData);
  };

  onClickEditRemoveReason = (editDialogData) => {
    return this.openEditDialog(editDialogData);
  };

  onChangeTab = (e, value) => {
    return this.setState({ currentTab: value });
  };

  onDeletePoint = async function () {
    try {
      const { deleteDialogData } = this.state;
      const removeReason = this.deleteInput.value;
      if (!removeReason) {
        // TODO: NOTIFICATION
        return this.openSnackBar('삭제 사유를 입력해 주세요.');
      }
      const response = await Request(`api/points/${deleteDialogData.pointId}`, {
        method: 'DELETE',
        data: {
          removeReason,
        },
      });

      if (response.result === ResponseResultTypeGroup.SUCCESS) {
        this.closeDeleteDialog();
        this.openSnackBar('삭제되었습니다.');
        await this.onRefresh();
        return true;
      }
      throw new Error('서버 오류');
    } catch (err) {
      console.log(err);
      this.closeDeleteDialog();
      this.openSnackBar('오류가 발생하였습니다.');
      return false;
    }
  };

  onEditPoint = async function () {
    try {
      const { editDialogData } = this.state;
      const removeReason = this.editInput.value;
      if (!removeReason) {
        // TODO: NOTIFICATION
        return this.openSnackBar('삭제 사유를 입력해 주세요.');
      }
      const response = await Request(`api/points/${editDialogData.pointId}`, {
        method: 'PUT',
        data: {
          removeReason,
        },
      });

      if (response.result === ResponseResultTypeGroup.SUCCESS) {
        this.closeEditDialog();
        this.openSnackBar('수정되었습니다.');
        await this.onRefresh();
        return true;
      }
      throw new Error('서버 오류');
    } catch (err) {
      console.log(err);
      this.closeEditDialog();
      this.openSnackBar('오류가 발생하였습니다.');
      return false;
    }
  };

  onAddPoint = async function () {
    try {
      const { userId } = this.props;
      const pointPrice = this.addPointPriceInput.value;
      const issueReason = this.addIssueReasonInput.value;
      const response = await Request('api/points', {
        method: 'POST',
        data: {
          userId,
          pointPrice,
          issueReason,
        },
      });

      if (response.result === ResponseResultTypeGroup.SUCCESS) {
        this.closeAddDialog();
        this.openSnackBar('지급되었습니다.');
        await this.onRefresh();
        return true;
      }
      throw new Error('서버 오류');
    } catch (err) {
      console.log(err);
      this.closeAddDialog();
      this.openSnackBar('오류가 발생하였습니다.');
      return false;
    }
  };

  openDeleteDialog = (deleteDialogData) => {
    return this.setState({
      deleteDialog: true,
      deleteDialogData,
    });
  };

  closeDeleteDialog = () => {
    return this.setState({
      deleteDialog: false,
    });
  };

  openEditDialog = (editDialogData) => {
    try {
      return this.setState({
        editDialog: true,
        editDialogData,
      });
      if (this.editInput) {
        this.editInput.value = editDialogData.removeReason;
      }
    } catch (err) {
      console.log(err);
    }
  };

  closeEditDialog = () => {
    return this.setState({
      editDialog: false,
    });
  };

  onClickAddPoint = () => {
    return this.openAddDialog();
  };

  openAddDialog = () => {
    return this.setState({
      addDialog: true,
    });
  };

  closeAddDialog = () => {
    return this.setState({
      addDialog: false,
    });
  };

  openSnackBar = (message) => {
    return this.setState({
      snackBar: {
        open: true,
        message,
      },
    });
  };

  closeSnackBar = () => {
    return this.setState({
      snackBar: {
        open: false,
        message: null,
      },
    });
  };

  getPointByType = async function (type) {
    const { userId } = this.props;
    try {
      const response = await Request(`api/users/${userId}/points/${type}`, { method: 'GET' });
      if (response.result === ResponseResultTypeGroup.SUCCESS) {
        return response.data;
      }
      throw new Error('request fail');
    } catch (err) {
      throw new Error(err);
    }
  };

  getImpendPoint = async function () {
    const DEFAULT = [];
    try {
      return await this.getPointByType(POINT_TYPE.IMPEND);
    } catch (err) {
      // TODO: notification
      console.log(err);
      return DEFAULT;
    }
  };

  getDeletedPoint = async function () {
    const DEFAULT = [];
    try {
      return await this.getPointByType(POINT_TYPE.DELETED);
    } catch (err) {
      // TODO: notification
      console.log(err);
      return DEFAULT;
    }
  };

  getReservedPoint = async function () {
    const DEFAULT = [];
    try {
      return await this.getPointByType(POINT_TYPE.RESERVED);
    } catch (err) {
      // TODO: notification
      console.log(err);
      return DEFAULT;
    }
  };

  getSpentPoint = async function () {
    const DEFAULT = [];
    try {
      return await this.getPointByType(POINT_TYPE.SPENT);
    } catch (err) {
      // TODO: notification
      console.log(err);
      return DEFAULT;
    }
  };

  renderEmptyPoint = () => {
    return (
      <Grow in>
        <Paper style={{ padding: '1.25rem', backgroundColor: '#f5f8fB' }}>
          <div className={s.flexRow}>
            <ErrorOutlineIcon style={{ width: '25px', height: '25px', marginRight: '5px' }} />
            <Typography variant="body1"> 표시할 적립금이 없습니다. </Typography>
          </div>
        </Paper>
      </Grow>
    );
  };

  renderReservedPoints = () => {
    try {
      const { reserved } = this.state.points;
      if (reserved.length === 0) {
        return this.renderEmptyPoint();
      }
      return reserved.map((item, index) => {
        return (
          <Grow key={`reserved-point-item-${index}`} in>
            <Paper style={{ padding: '1.25rem', backgroundColor: '#f5f8fB' }}>
              <div className={s.reservedPointCard}>
                <div className={s.flexColumn}>
                  <div>
                    <Typography variant="subheading"> {item.pointType.pointTypeName}</Typography>
                  </div>
                  <div>
                    <Typography variant="caption"> 적립일시 {moment(item.pubDate).format('YYYY년 MM월 DD일 HH시 mm분')} </Typography>
                  </div>
                </div>
                <div className={s.flexRow}>
                  <Typography variant="body2"> + </Typography>
                  <Typography variant="subheading"> {getFormattedMoney(item.totalPoint)} </Typography>
                  <Typography variant="body2"> 원 </Typography>
                </div>
                <div>
                  <IconButton aria-label="delete" onClick={() => this.onClickDeletePoint(item)}>
                    <DeleteIcon />
                  </IconButton>
                </div>
              </div>
            </Paper>
          </Grow>
        );
      });
    } catch (err) {
      return (
        <div> 정보를 표시할 수 없습니다. </div>
      );
    }
  };

  renderSpentPoint = () => {
    try {
      const { spent } = this.state.points;
      if (spent.length === 0) {
        return this.renderEmptyPoint();
      }
      return spent.map((item, index) => {
        return (
          <Grow key={`spent-point-item-${index}`} in>
            <Paper style={{ padding: '1.25rem', backgroundColor: '#f5f8fB' }} key={uuid()}>
              <div className={s.reservedPointCard}>
                <div className={s.flexColumn}>
                  <div>
                    <Typography variant="subheading"> {item.logPoint.pointType.pointTypeName}</Typography>
                  </div>
                  <div>
                    <Typography variant="caption"> 사용일시 {moment(item.logDate).format('YYYY년 MM월 DD일 HH시 mm분')} </Typography>
                  </div>
                </div>
                <div className={s.flexRow}>
                  <Typography variant="body2"> - </Typography>
                  <Typography variant="subheading"> {getFormattedMoney(item.spentPoint)} </Typography>
                  <Typography variant="body2"> 원 </Typography>
                </div>
              </div>
            </Paper>
          </Grow>
        );
      });
    } catch (err) {
      console.log(err);
      return null;
    }
  };

  renderImpendPoint = () => {
    try {
      const { impend } = this.state.points;
      if (impend.length === 0) {
        return this.renderEmptyPoint();
      }
      return impend.map((item, index) => {
        return (
          <Grow key={`impend-point-item-${index}`} in>
            <Paper style={{ padding: '1.25rem', backgroundColor: '#f5f8fB' }} key={uuid()}>
              <div className={s.reservedPointCard}>
                <div className={s.flexColumn}>
                  <div>
                    <Typography variant="subheading"> {item.pointType.pointTypeName}</Typography>
                  </div>
                  <div>
                    <Typography variant="caption"> 소멸예정일 {moment(item.expireDate).format('YYYY년 MM월 DD일 HH시 mm분')} </Typography>
                  </div>
                </div>
                <div className={s.flexRow}>
                  <Typography variant="body2"> + </Typography>
                  <Typography variant="subheading"> {getFormattedMoney(item.totalPoint)} </Typography>
                  <Typography variant="body2"> 원 </Typography>
                </div>
              </div>
            </Paper>
          </Grow>
        );
      });
    } catch (err) {
      return (
        <div> 정보를 표시할 수 없습니다. </div>
      );
    }
  };

  renderDeletedPoints = () => {
    try {
      const { deleted } = this.state.points;
      if (deleted.length === 0) {
        return this.renderEmptyPoint();
      }
      return deleted.map((item, index) => {
        return (
          <Grow key={`deleted-point-item-${index}`} in>
            <Paper style={{ padding: '1.25rem', backgroundColor: '#f5f8fB' }} key={uuid()}>
              <div className={s.reservedPointCard}>
                <div className={s.flexColumn} style={{ flexBasis: '70%' }}>
                  <div>
                    <Typography variant="subheading"> {item.pointType.pointTypeName}</Typography>
                  </div>
                  <div className={s.flexRow}>
                    <Typography variant="body1"> 삭제사유 : </Typography>
                    <Typography variant="body1"> {item.removeReason || '없음'} </Typography>
                  </div>
                  <div>
                    <Typography variant="caption"> 삭제일시 : {moment(item.removedDate).format('YYYY년 MM월 DD일 HH시 mm분')} </Typography>
                  </div>
                </div>
                <div className={s.flexRow} style={{ flexBasis: '20%' }}>
                  <Typography variant="body2"> + </Typography>
                  <Typography variant="subheading"> {getFormattedMoney(item.totalPoint)} </Typography>
                  <Typography variant="body2"> 원 </Typography>
                </div>
                <div style={{ flexBasis: '10%' }}>
                  <IconButton aria-label="edit" onClick={() => this.onClickEditRemoveReason(item)}>
                    <EditIcon />
                  </IconButton>
                </div>
              </div>
            </Paper>
          </Grow>
        );
      });
    } catch (err) {
      return (
        <div> 정보를 표시할 수 없습니다. </div>
      );
    }
  };

  renderContent = () => {
    const { pending, error } = this.state;
    try {
      if (pending) {
        return this.renderPendingUI();
      }
      if (error) {
        return this.renderErrorUI();
      }
      return this.renderTabContent();
    } catch (err) {
      return this.renderErrorUI();
    }
  };

  renderPendingUI = () => {
    return (
      <Grow in>
        <Paper style={{ padding: '1.25rem', backgroundColor: '#f5f8fB' }}>
          <div className={s.flexRow}>
            <CircularProgress />
            <Typography variant="body1"> 데이터를 불러오고 있습니다. </Typography>
          </div>
        </Paper>
      </Grow>
    );
  };

  renderErrorUI = () => {
    return (
      <Grow in>
        <Paper style={{ padding: '1.25rem', backgroundColor: '#f5f8fB' }}>
          <div className={s.flexRow}>
            <ErrorIcon style={{ width: '25px', height: '25px' }} />
            <Typography variant="body1"> 오류가 발생했습니다. 다시 시도해 주세요. </Typography>
          </div>
        </Paper>
      </Grow>
    );
  };

  renderTabContent = () => {
    const { currentTab } = this.state;
    if (currentTab === POINT_TYPE.RESERVED) {
      return this.renderReservedPoints();
    } else if (currentTab === POINT_TYPE.SPENT) {
      return this.renderSpentPoint();
    } else if (currentTab === POINT_TYPE.IMPEND) {
      return this.renderImpendPoint();
    } else if (currentTab === POINT_TYPE.DELETED) {
      return this.renderDeletedPoints();
    } else {
      return null;
    }
    return null;
  };

  render() {
    const { currentTab, deleteDialog, editDialog, addDialog, snackBar } = this.state;
    const { title, classes } = this.props;
    return (
      <Widget
        title={title}
        onRefresh={this.onRefresh}
        actions={
          <Zoom in>
            <Button
              variant="raised"
              color="primary"
              className={classes.button}
              onClick={this.onClickAddPoint}
              style={{ marginRight: '10px' }}
            >
              적립금 추가 <AddIcon />
            </Button>
          </Zoom>
        }
      >
        <div className={s.root}>
          <AppBar
            position="static"
            style={{
              backgroundColor: '#fff',
              color: '#000',
            }}
          >
            <Tabs
              value={currentTab}
              onChange={this.onChangeTab}
              centered
            >
              <Tab label="적립 내역" value={POINT_TYPE.RESERVED} />
              <Tab label="사용 내역" value={POINT_TYPE.SPENT} />
              <Tab label="소멸 예정" value={POINT_TYPE.IMPEND} />
              <Tab label="삭제 내역" value={POINT_TYPE.DELETED} />
            </Tabs>
          </AppBar>
          <div className={s.content}>
            {this.renderContent()}
          </div>
        </div>
        <Dialog
          open={deleteDialog}
          onClose={this.closeDeleteDialog}
        >
          <DialogTitle> 적립금 삭제 </DialogTitle>
          <DialogContent>
            <DialogContentText> 삭제 사유를 입력해 주세요. </DialogContentText>
            <TextField inputRef={(r) => { this.deleteInput = r; }} />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.closeDeleteDialog}>
              취소
            </Button>
            <Button variant="raised" color="primary" onClick={this.onDeletePoint}>
              삭제
            </Button>
          </DialogActions>
        </Dialog>
        <Dialog
          open={editDialog}
          onClose={this.closeEditDialog}
        >
          <DialogTitle> 삭제 사유 수정 </DialogTitle>
          <DialogContent>
            <DialogContentText> 삭제사유를 입력해 주세요. </DialogContentText>
            <TextField inputRef={(r) => { this.editInput = r; }} />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.closeEditDialog}>
              취소
            </Button>
            <Button variant="raised" color="primary" onClick={this.onEditPoint}>
              수정
            </Button>
          </DialogActions>
        </Dialog>
        <Dialog
          open={addDialog}
          onClose={this.closeAddDialog}
        >
          <DialogTitle> 포인트 지급 </DialogTitle>
          <DialogContent>
            <DialogContentText> 액수 </DialogContentText>
            <TextField inputRef={(r) => { this.addPointPriceInput = r; }} />
            <DialogContentText> 사유 </DialogContentText>
            <TextField inputRef={(r) => { this.addIssueReasonInput = r; }} />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.closeAddDialog}>
              취소
            </Button>
            <Button variant="raised" color="primary" onClick={this.onAddPoint}>
              지급
            </Button>
          </DialogActions>
        </Dialog>
        <Snackbar
          open={snackBar.open}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
          onClose={this.closeSnackBar}
          message={snackBar.message}
        />
      </Widget>
    );
  }
}

Points.propTypes = propTypes;
Points.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(styles, { withTheme: true }),
)(Points);
