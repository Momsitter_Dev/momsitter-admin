import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Widget.css';
import ExpandLessIcon from 'material-ui/svg-icons/navigation/expand-less';
import ExpandMoreIcon from 'material-ui/svg-icons/navigation/expand-more';
import SaveIcon from 'material-ui/svg-icons/content/save';
import AddIcon from 'material-ui/svg-icons/content/add';
import IconButton from 'material-ui/IconButton';
import Paper from 'material-ui/Paper';
import classnames from 'classnames';
import RefreshIcon from 'material-ui/svg-icons/navigation/refresh';

class Widget extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expand: true,
      actionShow: false,
      actionType: null,
    };
  }

  toggleExpand = () => {
    const { expand } = this.state;
    this.setState({ expand: !expand });
  };

  onClickSave = () => {
    this.props.onSave();
  };

  onClickRefresh = () => {
    this.props.onRefresh();
  };

  onClickAddItem = () => {
    this.props.onAddItem();
  };

  renderAddItemButton = () => {
    let ui = null;
    if (this.props.onAddItem instanceof Function) {
      ui = (
        <IconButton
          onClick={this.onClickAddItem}
          iconStyle={{ color: '#fff' }}
          tooltip="추가하기"
        >
          <AddIcon />
        </IconButton>
      );
    }
    return ui;
  };

  renderSaveButton = () => {
    let ui = null;
    if (this.props.onSave instanceof Function) {
      ui = (
        <IconButton
          onClick={this.onClickSave}
          iconStyle={{ color: '#fff' }}
          tooltip="저장하기"
        >
          <SaveIcon />
        </IconButton>
      );
    }
    return ui;
  };

  render() {
    const { isModified } = this.props;
    const expandStyle = {};
    let expandIcon = null;
    let modifiedStyle = null;

    if (isModified) {
      modifiedStyle = 'bg-color-success';
    } else {
      modifiedStyle = 'bg-color-primary';
    }

    if (this.state.expand) {
      expandIcon = <ExpandLessIcon />;
      expandStyle.height = 'auto';
    } else {
      expandIcon = <ExpandMoreIcon />;
      expandStyle.height = '0px';
      expandStyle.padding = '0px';
    }

    return (
      <Paper zDepth={1} className={s.root}>
        <div
          className={classnames('box-header', modifiedStyle, s.widgetHeader)}
        >
          <span>
            {this.props.title}
            {this.props.isModified ? '- [수정됨]' : ''}
          </span>
          <span>
            {this.renderAddItemButton()}
            {this.renderSaveButton()}
            <IconButton
              onClick={this.onClickRefresh}
              iconStyle={{ color: '#fff' }}
              tooltip="데이터 다시 불러오기"
            >
              <RefreshIcon />
            </IconButton>
            <IconButton onClick={this.toggleExpand} iconStyle={{ color: '#fff' }}>
              {expandIcon}
            </IconButton>
          </span>
        </div>
        <div className={classnames('box-body', s.content)} style={expandStyle}>
          {this.props.children}
        </div>
      </Paper>
    );
  }
}

Widget.propTypes = {
  title: PropTypes.string,
  onAddItem: PropTypes.func,
  onSave: PropTypes.func,
  onRefresh: PropTypes.func,
  onDelete: PropTypes.func,
  isModified: PropTypes.bool,
};

Widget.defaultProps = {
  onAddItem: null,
  onSave: null,
  onRefresh: () => {},
  onDelete: () => {},
  isModified: false,
};

export default withStyles(s)(Widget);
