import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './MemoItem.css';
import SimpleDateFormat from '../../../../../util/dataFormat/SimpleDateFormat';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import MemoTypeGroup from '../../../../../util/const/MemoTypeGroup';
import MemoStatusGroup from '../../../../../util/const/MemoStatusGroup';
import MemoClassGroup from '../../../../../util/const/MemoClassGroup';
import MemoReqMethodGroup from '../../../../../util/const/MemoReqMethodGroup';

const commonRadioButtonGroupStyle = {
  marginBottom: '20px',
  paddingLeft: '15px',
};
const commonRadioButtonStyle = {
  display: 'inline-block',
  maxWidth: '125px',
};

class MemoItem extends React.Component {
  getAdminInfoText = (memoAdmin) => {
    let text = '정보없음';
    if (memoAdmin) {
      text = memoAdmin.adminName;
    }
    return text;
  };

  onChangeMemo = (type, value) => {
    const { memoId } = this.props.data;
    this.props.onChangeMemo(memoId, type, value);
  };

  onClickDeleteMemo = (userId, memoId) => {
    this.props.onDeleteMemo(userId, memoId);
  };

  onClickEditMemo = (memoId) => {
    this.props.onEditMemo(memoId);
  };

  onClickMemoId = (userId, memoId) => {
    this.props.onClickMemoId({
      userId,
      memoResId: memoId,
    });
  };

  onClickMemoResId = (memoData) => {
    this.props.onClickMemoResId('memoResId', memoData)
  };

  onClickMemoContent = (memoData) => {
    this.props.onClickMemoContent('memoContent', memoData);
  };

  renderMemoClass = () => {
    const { memoClass, memoType } = this.props.data;
    let ui = null;
    if (memoType && memoType === MemoTypeGroup.ADMIN) {
      ui = (
        <div className={s.dataRow}>
          <label>메모 구분</label>
          <RadioButtonGroup
            name="memoClassGroup"
            defaultSelected={memoClass ? memoClass : 'info_error'}
            onChange={(event, value) => this.onChangeMemo('memoClass', value)}
            style={commonRadioButtonGroupStyle}
          >
            <RadioButton style={commonRadioButtonStyle} value={MemoClassGroup.INFO_ERROR} label="정보오류"/>
            <RadioButton style={commonRadioButtonStyle} value={MemoClassGroup.PROFILE_ERROR} label="사진오류"/>
            <RadioButton style={commonRadioButtonStyle} value={MemoClassGroup.OTHER} label="기타"/>
            <RadioButton style={commonRadioButtonStyle} value={MemoClassGroup.NOT_IN_USE} label="미사용"/>
          </RadioButtonGroup>
        </div>
      );
    }
    return ui;
  };

  renderMemoReqMethod = () => {
    const { memoReqMethod, memoType } = this.props.data;
    let ui = null;
    if (memoType && memoType !== MemoTypeGroup.ADMIN) {
      ui = (
        <div className={s.dataRow}>
          <label className={s.memoContentLabel}>메모 요청 방법</label>
          <RadioButtonGroup
            name="memoReqMethod"
            defaultSelected={memoReqMethod ? memoReqMethod : 'call'}
            onChange={(event, value) => this.onChangeMemo('memoReqMethod', value)}
            style={commonRadioButtonGroupStyle}
          >
            <RadioButton value={MemoReqMethodGroup.CALL} label="전화" style={commonRadioButtonStyle} />
            <RadioButton value={MemoReqMethodGroup.KAKAO} label="카카오톡" style={commonRadioButtonStyle} />
            <RadioButton value={MemoReqMethodGroup.MAIL} label="메일" style={commonRadioButtonStyle} />
            <RadioButton value={MemoReqMethodGroup.SMS} label="문자" style={commonRadioButtonStyle} />
          </RadioButtonGroup>
        </div>
      );
    }
    return ui;
  };

  renderResId = (memoResId, data) => {
    let ui = null;
    let buttonLabel = '없음';

    if (memoResId && data) {
      buttonLabel = memoResId;
    }
    ui = (
      <RaisedButton
        label={buttonLabel}
        onClick={() => this.onClickMemoResId(data)}
      />
    );
    return ui;
  };


  render() {
    const { data } = this.props;
    const {
      userId,
      memoResId,
      memoId,
      memoType,
      memoContent,
      memoClass,
      memoRegDate,
      memoStatus,
      memoReqMethod,
      memoAdmin,
    } = data;
    return (
      <Paper zDepth={2}>
        <div className={s.root}>
          <div className={s.header}>
            <RaisedButton
              label={memoId}
              onClick={() => this.onClickMemoId(userId, memoId)}
            />
            <div>
              <RaisedButton
                label="수정"
                onClick={() => this.onClickEditMemo(memoId)}
              />
              <RaisedButton
                label="삭제"
                onClick={() => this.onClickDeleteMemo(userId, memoId)}
              />
            </div>
          </div>
          <div className={s.dataRow}>
            <label className={s.memoContentLabel}>등록일자</label>
            <span className={s.memoRegDate}>{SimpleDateFormat(memoRegDate)}</span>
          </div>
          <div className={s.dataRow}>
            <label className={s.memoContentLabel}>연관 메모 번호</label>
            {this.renderResId(memoResId, data)}
          </div>
          <div className={s.dataRow}>
            <label className={s.memoContentLabel}>메모 등록자</label>
            <div style={{ textAlign: 'left', paddingLeft: '15px' }}>{this.getAdminInfoText(memoAdmin)}</div>
          </div>
          <div className={s.dataRow}>
            <label> 메모 타입 </label>
            <RadioButtonGroup
              name="memoTypeGroup"
              defaultSelected={memoType ? memoType : MemoTypeGroup.QUESTION}
              onChange={(event, value) => this.onChangeMemo('memoType', value)}
              style={commonRadioButtonGroupStyle}
            >
              <RadioButton
                style={commonRadioButtonStyle}
                value={MemoTypeGroup.QUESTION}
                label="문의"
              />
              <RadioButton
                style={commonRadioButtonStyle}
                value={MemoTypeGroup.REQUEST}
                label="요청"
              />
              <RadioButton
                style={commonRadioButtonStyle}
                value={MemoTypeGroup.COMPLAIN}
                label="불만"
              />
              <RadioButton
                style={commonRadioButtonStyle}
                value={MemoTypeGroup.ADMIN}
                label="관리자"
              />
            </RadioButtonGroup>
          </div>
          {this.renderMemoClass()}
          {this.renderMemoReqMethod()}
          <div className={s.dataRow}>
            <label>메모 상태</label>
            <RadioButtonGroup
              name="memoStatusGroup"
              defaultSelected={memoStatus ? memoStatus : MemoStatusGroup.NEW}
              onChange={(event, value) => this.onChangeMemo('memoStatus', value)}
              style={commonRadioButtonGroupStyle}
            >
              <RadioButton
                style={commonRadioButtonStyle}
                value={MemoStatusGroup.NEW}
                label="신규"
              />
              <RadioButton
                style={commonRadioButtonStyle}
                value={MemoStatusGroup.DONE}
                label="완료"
              />
            </RadioButtonGroup>
          </div>
          <div className={s.memoContentRow}>
            <label className={s.memoContentLabel}>메모 내용</label>
            <RaisedButton
              label={memoContent}
              style={{ height: '100%' }}
              labelStyle={{ lineHeight: '1.6em' }}
              onClick={() => this.onClickMemoContent(data)}
            />
          </div>
        </div>
      </Paper>
    );
  }
}

MemoItem.propTypes = {
  data: PropTypes.object.isRequired,
  onDeleteMemo: PropTypes.func.isRequired,
  onEditMemo: PropTypes.func.isRequired,
  onChangeMemo: PropTypes.func.isRequired,
  onClickMemoId: PropTypes.func.isRequired,
  onClickMemoResId: PropTypes.func.isRequired,
  onClickMemoContent: PropTypes.func.isRequired,
};

MemoItem.defaultProps = {};

export default withStyles(s)(MemoItem);
