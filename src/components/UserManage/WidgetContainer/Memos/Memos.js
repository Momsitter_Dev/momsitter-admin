/* eslint-disable prettier/prettier */
import _ from 'lodash';
import uuid from 'uuid/v4';

import { compose } from 'recompose';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import WarningIcon from '@material-ui/icons/Warning';
import IconButton from '@material-ui/core/IconButton';
import CircularProgress from '@material-ui/core/CircularProgress';

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Memos.css';
import Widget from '../Widget';
import Request from '../../../../util/Request';
import MemoItem from './MemoItem';
import AddMemoDialog from '../../../Manage/Memos/AddMemoDialog';
import ResponseResultTypeGroup from '../../../../util/const/ResponseResultTypeGroup';
import FormDialog from '../../../Dialog/MemoFormDialog';

import TextField from '../../../TextField';
import MemoStatusGroup from '../../../../util/const/MemoStatusGroup';
import MemoClassGroup from '../../../../util/const/MemoClassGroup';
import MemoTypeGroup from '../../../../util/const/MemoTypeGroup';

const propTypes = {
  title: PropTypes.string,
  userId: PropTypes.number,
  openNotificationDialog: PropTypes.func.isRequired,
};
const defaultProps = {
  title: '',
  userId: 0,
};

class Memos extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      memos: null,
      memosHierarchy: null,
      formDialog: false,
      formDialogTitle: null,
      formDialogData: {
        userId: props.userId,
        memoResId: null,
        memoType: MemoTypeGroup.QUESTION,
        memoClass: MemoClassGroup.INFO_ERROR,
        memoStatus: MemoStatusGroup.NEW,
        memoContent: '',
        memoReqMethod: 'call',
      },
      simpleFormDialog: false,
      simpleFormDialogValue: '',
      simpleFormDialogType: null,
      simpleFormDialogData: null,
      simpleFormDialogTitle: '',
      addMemoDialog: false,
      addMemoDialogData: {
        userId: props.userId,
        memoResId: null,
        memoType: MemoTypeGroup.QUESTION,
        memoClass: MemoClassGroup.INFO_ERROR,
        memoStatus: MemoStatusGroup.NEW,
        memoContent: '',
        memoReqMethod: 'call',
      },
      onLoading: true,
      onError: false,
    };
    this.getMemos = this.getMemos.bind(this);
    this.onRefresh = this.onRefresh.bind(this);
    this.onSaveAddMemo = this.onSaveAddMemo.bind(this);
    this.onCreateMemo = this.onCreateMemo.bind(this);
    this.onEditMemo = this.onEditMemo.bind(this);
    this.onDeleteMemo = this.onDeleteMemo.bind(this);
  }

  async componentDidMount() {
    await this.getMemos(this.props.userId);
  }

  async componentWillReceiveProps(nextProps) {
    if (this.props.userId !== nextProps.userId) {
      await this.getMemos(nextProps.userId);
    }
  }

  getMemos = async function (userId) {
    this.setState({ onLoading: true });
    const response = await Request(`api/users/${userId}/memos`, { method: 'GET' });
    if (response) {
      if (typeof window !== 'undefined') {
        const self = this;
        const timerIndex = window.setTimeout(() => {
          self.setState({
            onLoading: false,
            memos: response.data,
            memosHierarchy: this.generateMemoHierarchy(response.data),
          });
          window.clearTimeout(timerIndex);
        }, 500);
      } else {
        this.setState({
          onLoading: false,
          memos: response.data,
          memosHierarchy: this.generateMemoHierarchy(response.data),
        });
      }
    } else {
      this.setState({
        onError: true,
        onLoading: false,
      });
    }
  };

  generateMemoHierarchy = (list) => {
    let result = Object.assign([], list);
    if (result && result.length > 0) {
      for (let i = 0; i < result.length; i += 1) {
        result[i].childMemo = this.getChildHierarchy(result[i], list);
      }
    }
    return _.filter(result, (o) => {
      return o.memoResId === null
    });
  };

  getChildHierarchy = (memo, list) => {
    if (memo && memo.memoId) {
      return memo.childMemo = _.filter(list, (o) => {
        return o.memoResId === memo.memoId;
      });
    } else {
      return null;
    }
  };

  openFormDialog = () => {
    this.setState({
      formDialog: true,
      formDialogTitle: '새로운 메모 등록',
    });
  };

  closeFormDialog = () => {
    this.setState({
      formDialog: false,
      formDialogTitle: '',
      formDialogData: {
        memoResId: null,
        memoType: MemoTypeGroup.QUESTION,
        memoClass: MemoClassGroup.INFO_ERROR,
        memoStatus: MemoStatusGroup.NEW,
        memoContent: '',
        memoReqMethod: 'call',
      }
    });
  };

  onRefresh = async function () {
    await this.getMemos(this.props.userId);
  };

  onClickEditData = (type, memoData) => {
    this.setState({
      simpleFormDialogType: type,
      simpleFormDialogTitle: type,
      simpleFormDialogValue: memoData[type],
      simpleFormDialogData: memoData,
    });
    this.openSimpleFormDialog();
  };

  saveSimpleFormDialog = () => {
    const {
      simpleFormDialogType,
      simpleFormDialogValue,
      simpleFormDialogData,
      memos,
      memosHierarchy,
    } = this.state;
    // TODO: FIND MEMO AND SET DATA;
    const targetMemoId = simpleFormDialogData.memoId;
    if (targetMemoId) {
      const targetInd = _.findIndex(memos, (o) => {
        return o.memoId === targetMemoId;
      });

      if (targetInd !== -1) {
        let targetMemo = memos[targetInd];
        targetMemo[simpleFormDialogType] = simpleFormDialogValue;
        memos[targetInd] = targetMemo;
        this.setState({
          memos: Object.assign([], memos),
          memosHierarchy: this.generateMemoHierarchy(Object.assign([], memos))
        });
        this.props.openNotificationDialog('저장되었습니다.');
        this.closeSimpleFormDialog();
      } else {
        this.props.openNotificationDialog('오류', '메모 정보를 찾을 수 없습니다.\n 새로고침 후 이용해 주세요.');
        this.closeSimpleFormDialog();
      }
    } else {
      this.props.openNotificationDialog('오류', '메모 정보를 찾을 수 없습니다.\n 새로고침 후 이용해 주세요.');
      this.closeSimpleFormDialog();
    }
  };

  changeSimpleFormDialogData = (value) => {
    this.setState({
      simpleFormDialogValue: value,
    });
  };

  openSimpleFormDialog = () => {
    this.setState({
      simpleFormDialog: true,
    });
  };

  closeSimpleFormDialog = () => {
    this.setState({
      simpleFormDialog: false,

    });
  };

  onClickAddMemo = () => {
    const { userId } = this.props;
    const { addMemoDialogData } = this.state;
    this.setState({
      addMemoDialog: true,
      addMemoDialogData: {
        ...addMemoDialogData,
        userId,
      },
    });
  };

  onChangeAddMemoData = (type, value) => {
    const { addMemoDialogData } = this.state;
    let newMemoData = Object.assign({}, addMemoDialogData);
    newMemoData[type] = value;
    this.setState({
      addMemoDialogData: newMemoData,
    });
  };

  checkAddMemoDataValid = (data) => {
    let result = true;
    if (data) {
      for (let key in data) {
        if (key === 'userId' && !data[key]) {
          this.props.openNotificationDialog('알림', '회원번호를 입력해 주세요.');
          return false;
        }
        if (key === 'memoType' && !data[key]) {
          this.props.openNotificationDialog('알림', '메모 타입을 입력해 주세요.');
          return false;
        }
        if (key === 'memoClass' && !data[key]) {
          this.props.openNotificationDialog('알림', '메모 구분을 입력해 주세요.');
          return false;
        }
        if (key === 'memoContent' && !data[key]) {
          this.props.openNotificationDialog('알림', '메모 내용을 입력해 주세요.');
          return false;
        }
        if (key === 'memoReqMethod' && !data[key]) {
          this.props.openNotificationDialog('알림', '메모 요청방법을 입력해 주세요.');
          return false;
        }
        if (key === 'memoStatus' && !data[key]) {
          this.props.openNotificationDialog('알림', '메모 상태를 입력해 주세요.');
          return false;
        }
      }
    } else {
      result = false;
      this.props.openNotificationDialog('오류', '정보가 누락되어 진행할 수 없습니다.');
    }
    return result;
  };

  onSaveAddMemo = async function () {
    const { addMemoDialogData, memos } = this.state;
    if (this.checkAddMemoDataValid(addMemoDialogData)) {
      const response = await Request(`api/manage/user/${addMemoDialogData.userId}/memos`, {
        method: 'POST',
        data: {
          ...addMemoDialogData,
        }
      });
      if (response && response.result === ResponseResultTypeGroup.SUCCESS) {
        this.onCloseAddMemoDialog();
        this.props.openNotificationDialog('알림', '저장되었습니다.');
        let newUserMemo = Object.assign([], memos);
        newUserMemo = [response.data, ...newUserMemo];
        this.setState({
          memos: newUserMemo,
          memosHierarchy: this.generateMemoHierarchy(newUserMemo),
        });
      } else {
        this.props.openNotificationDialog('오류', '네트워크 오류입니다.');
      }
    }
  };

  onCreateMemo = async function () {
    const { formDialogData, memos } = this.state;
    const { userId } = this.props;
    if (userId) {
      const response = await Request(`api/manage/user/${userId}/memos`, {
        method: 'POST',
        data: {
          ...formDialogData,
          userId,
        }
      });
      if (response && response.result === ResponseResultTypeGroup.SUCCESS) {
        this.closeFormDialog();
        this.props.openNotificationDialog('알림', '저장되었습니다.');
        let newUserMemo = Object.assign([], memos);
        newUserMemo.push(response.data);
        this.setState({
          memos: newUserMemo,
          memosHierarchy: this.generateMemoHierarchy(newUserMemo),
        });
      } else {
        this.props.openNotificationDialog('오류', '네트워크 오류입니다.');
      }
    } else {
      this.props.openNotificationDialog('오류', '회원정보를 찾을 수 없습니다.');
    }
  };

  onEditMemo = async function (memoId) {
    const { memos } = this.state;
    const targetMemoId = _.findIndex(memos, (o) => {
      return o.memoId === memoId;
    });
    if (targetMemoId !== -1) {
      const targetMemoData = memos[targetMemoId];
      if (targetMemoData) {
        const response = await Request(`api/manage/user/${targetMemoData.userId}/memos/${targetMemoData.memoId}`, {
          method: 'PUT',
          data: targetMemoData,
        });
        if (response && response.result === ResponseResultTypeGroup.SUCCESS) {
          this.props.openNotificationDialog('알림', '수정되었습니다.');
        } else {
          this.props.openNotificationDialog('오류', '네트워크 오류입니다.');
        }
      } else {
        this.props.openNotificationDialog('오류', '메모 정보를 찾을 수 없습니다. 개발자에게 문의해 주세요.');
      }
    } else {
      this.props.openNotificationDialog('오류', '메모 정보를 찾을 수 없습니다. 개발자에게 문의해 주세요.');
    }
  };

  onDeleteMemo = async function (userId, memoId) {
    const { memos } = this.state;
    const response = await Request(
      `api/manage/user/${userId}/memos/${memoId}`,
      { method: 'DELETE' },
    ).catch(() => false);
    if (response && response.result === 'success') {
      this.props.openNotificationDialog('알림', '메모가 삭제되었습니다.');
      const targetMemoDataIndex = _.findIndex(memos, (o) => {
        return o.memoId === memoId
      });
      if (targetMemoDataIndex !== -1) {
        const newUserMemo = _.filter(memos, (o) => {
          return o.memoId !== memoId;
        });
        this.props.openNotificationDialog('알림', '삭제되었습니다.');
        this.setState({
          memos: newUserMemo,
          memosHierarchy: this.generateMemoHierarchy(newUserMemo),
        });
      } else {
        this.props.openNotificationDialog('알림', '새로고침 후에 이용해 주세요.');
      }
    } else {
      this.props.openNotificationDialog('오류', '네트워크 오류입니다.');
    }
  };

  onCloseAddMemoDialog = () => {
    this.setState({
      addMemoDialog: false,
      addMemoDialogData: {
        userId: this.props.userId,
        memoResId: null,
        memoType: MemoTypeGroup.QUESTION,
        memoClass: MemoClassGroup.INFO_ERROR,
        memoStatus: MemoStatusGroup.NEW,
        memoContent: '',
        memoReqMethod: 'call',
      },
    });
  };

  onChangeMemo = (memoId, type, value) => {
    const { memos } = this.state;
    const targetMemoInd = _.findIndex(memos, (o) => {
      return o.memoId === memoId;
    });

    if (targetMemoInd !== -1) {
      const targetMemoData = Object.assign({}, memos[targetMemoInd]);
      targetMemoData[type] = value;

      const newUserMemo = Object.assign([], memos);
      newUserMemo[targetMemoInd] = targetMemoData;
      this.setState({
        memos: newUserMemo,
        memosHierarchy: this.generateMemoHierarchy(newUserMemo),
      });
    } else {
      this.props.openNotificationDialog('오류', '메모정보를 찾을 수 없습니다. 개발자에게 문의해주세요.');
    }
  };

  onChangeNewMemo = (type, value) => {
    const { formDialogData } = this.state;
    let newMemoData = Object.assign({}, formDialogData);
    newMemoData[type] = value;
    this.setState({
      formDialogData: newMemoData,
    });
  };

  onClickAddNewMemo = (data) => {
    if (data) {
      this.setState({
        formDialogData: {
          ...this.state.formDialogData,
          ...data,
        },
      }) ;
    }
    this.openFormDialog();
  };

  renderMemoList = (onError, onLoading, memos) => {
    let ui = null;
    if (onError) {
      ui = (
        <div style={{ textAlign: 'center' }}>
          <IconButton onClick={this.onClickWarning}>
            <WarningIcon />
          </IconButton>
          <div style={{ marginTop: '10px' }}> 오류가 발생했습니다. </div>
        </div>
      );
    } else {
      if (onLoading) {
        ui = (
          <div style={{ textAlign: 'center' }}>
            <CircularProgress size={80} thickness={5} />
            <div style={{ marginTop: '10px' }}> 데이터를 불러오는 중입니다. </div>
          </div>
        );
      } else {
        if (memos && memos.length > 0) {
          ui = memos.map((item) => {
            return (
              <div key={uuid()}>
                <MemoItem
                  data={item}
                  openNotificationDialog={this.props.openNotificationDialog}
                  onDeleteMemo={this.onDeleteMemo}
                  onEditMemo={this.onEditMemo}
                  onChangeMemo={this.onChangeMemo}
                  onClickMemoId={this.onClickAddNewMemo}
                  onClickMemoResId={this.onClickEditData}
                  onClickMemoContent={this.onClickEditData}
                />
                {this.renderChildMemoListUI(item.childMemo, 1)}
              </div>
            );
          });
        } else {
          ui = (<div>작성된 메모가 없습니다.</div>);
        }
      }
    }
    return  ui;
  };

  renderChildMemoListUI = (memo, depth) => {
    let ui = null;
    if (memo && memo.length > 0) {
      ui = memo.map((item) => {
        return (
          <div style={{ paddingLeft: String(20 * depth) + 'px' }} key={uuid()}>
            <MemoItem
              key={uuid()}
              data={item}
              openNotificationDialog={this.props.openNotificationDialog}
              onDeleteMemo={this.onDeleteMemo}
              onEditMemo={this.onEditMemo}
              onChangeMemo={this.onChangeMemo}
              onClickMemoId={this.onClickAddNewMemo}
              onClickMemoResId={this.onClickEditData}
              onClickMemoContent={this.onClickEditData}
            />
            {this.renderChildMemoListUI(item.childMemo, depth + 1)}
          </div>
        );
      });
    }
    return ui;
  };

  render() {
    const { title } = this.props;
    const {
      memos,
      memosHierarchy,
      addMemoDialog,
      addMemoDialogData,
      formDialog,
      formDialogTitle,
      formDialogData,
      simpleFormDialog,
      simpleFormDialogData,
      simpleFormDialogValue,
      simpleFormDialogTitle,
      onError,
      onLoading,
    } = this.state;

    return (
      <Widget
        title={title}
        onRefresh={this.onRefresh}
        onAddItem={this.onClickAddMemo}
      >
        <div>
          {this.renderMemoList(onError, onLoading, memosHierarchy)}
        </div>
        <Dialog
          open={simpleFormDialog}
          onClose={this.closeSimpleFormDialog}
          fullWidth
          maxWidth="md"
        >
          <DialogTitle> {simpleFormDialogTitle} </DialogTitle>
          <DialogContent>
            <Grid container spacing={16}>
              <Grid item xs={12}>
                <TextField
                  onChange={(value) => this.changeSimpleFormDialogData(value)}
                  value={simpleFormDialogValue}
                  style={{ width: '100%' }}
                  multiLine
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button variant={'flat'} onClick={this.closeSimpleFormDialog}> 취소 </Button>
            <Button variant={'raised'} onClick={this.saveSimpleFormDialog}> 저장 </Button>
          </DialogActions>
        </Dialog>
        <AddMemoDialog
          title="메모 추가하기"
          open={addMemoDialog}
          data={addMemoDialogData}
          onCreateMemo={this.onSaveAddMemo}
          onChangeNewMemo={this.onChangeAddMemoData}
          closeDialog={this.onCloseAddMemoDialog}
        />
        <FormDialog
          modal={true}
          open={formDialog}
          title={formDialogTitle}
          closeDialog={this.closeFormDialog}
          onCreateMemo={this.onCreateMemo}
          onChangeNewMemo={this.onChangeNewMemo}
          data={formDialogData}
        />
      </Widget>
    );
  }
}

Memos.propTypes = propTypes;
Memos.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true })
)(Memos);
