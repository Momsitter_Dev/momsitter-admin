import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Reports.css';
import Widget from '../Widget';
import { Tabs, Tab } from 'material-ui/Tabs';
import ReportItem from './ReportItem';
import Request from '../../../../util/Request';
import Paper from 'material-ui/Paper';
import CircularProgress from 'material-ui/CircularProgress';
import WarningIcon from 'material-ui/svg-icons/alert/warning';
import IconButton from 'material-ui/IconButton';

class Reports extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      reports: {
        send: [],
        receive: [],
      },
      onLoading: true,
      onError: false,
    };
    this.getReports = this.getReports.bind(this);
    this.onRefresh = this.onRefresh.bind(this);
  }

  async componentDidMount() {
    await this.getReports(this.props.userId);
  }

  async componentWillReceiveProps(nextProps) {
    if (this.props.userId !== nextProps.userId) {
      await this.getReports(nextProps.userId);
    }
  }

  getReports = async function (userId) {
    this.setState({ onLoading: true });
    const response = await Request(`api/users/${userId}/reports`, { method: 'GET'});
    if (response) {
      if (typeof window !== 'undefined') {
        const self = this;
        const timerIndex = window.setTimeout(() => {
          self.setState({
            reports: response.data,
            onLoading: false,
          });
          window.clearTimeout(timerIndex);
        }, 500);
      } else {
        this.setState({ reports: response.data });
      }
    } else {
      this.setState({
        onLoading: false,
        onError: true,
      });
    }
  };

  onRefresh = async function () {
    await this.getReports(this.props.userId);
  };


  onClickWarning = () => {
    if (typeof window !== 'undefined') {
      if (window.confirm('정보를 다시 로드하시겠습니까?')) {
        this.setState({
          onError: false
        }, () => {
          this.onRefresh();
        });
      }
    }
  };

  renderSendedReports = (onError, onLoading, reports) => {
    let ui = null;
    if (onError) {
      ui = (
        <Paper style={{ textAlign: 'center' }}>
          <IconButton onClick={this.onClickWarning}>
            <WarningIcon />
          </IconButton>
          <div style={{ marginTop: '10px' }}> 오류가 발생했습니다. </div>
        </Paper>
      );
    } else {
      if (onLoading) {
        ui = (
          <Paper style={{ textAlign: 'center' }}>
            <CircularProgress size={80} thickness={5} />
            <div style={{ marginTop: '10px' }}> 데이터를 불러오는 중입니다. </div>
          </Paper>
        );
      } else {
        if (reports.send && reports.send.length > 0) {
          ui = reports.send.map((item, index) =>
            <ReportItem key={index} type="reportTargetUser" reportData={item} />,
          );
        } else {
          ui = (
            <Paper>
              <div className={s.no_content}>해당 회원이 신고한 내역이 없습니다.</div>
            </Paper>
          );
        }
      }
    }
    return ui;
  };

  renderReceivedReports = (onError, onLoading, reports) => {
    let ui = null;
    if (onError) {
      ui = (
        <Paper style={{ textAlign: 'center' }}>
          <IconButton onClick={this.onClickWarning}>
            <WarningIcon />
          </IconButton>
          <div style={{ marginTop: '10px' }}> 오류가 발생했습니다. </div>
        </Paper>
      );
    } else {
      if (onLoading) {
        ui = (
          <Paper style={{ textAlign: 'center' }}>
            <CircularProgress size={80} thickness={5} />
            <div style={{ marginTop: '10px' }}> 데이터를 불러오는 중입니다. </div>
          </Paper>
        );
      } else {
        if (reports && reports.receive && reports.receive.length > 0) {
          ui = reports.receive.map((item, index) =>
            <ReportItem key={index} type="reportUser" reportData={item} />,
          );
        } else {
          ui = (
            <Paper>
              <div className={s.no_content}>해당 회원에게 작성된 신고가 없습니다.</div>
            </Paper>
          );
        }
      }
    }
    return ui;
  };

  render() {
    const { onError, onLoading, reports } = this.state;
    return (
      <Widget title={this.props.title} onRefresh={this.onRefresh}>
        <Tabs>
          <Tab label="작성한 신고">
            {this.renderSendedReports(onError, onLoading, reports)}
          </Tab>
          <Tab label="받은 신고">
            {this.renderReceivedReports(onError, onLoading, reports)}
          </Tab>
        </Tabs>
      </Widget>
    );
  }
}

Reports.propTypes = {
  userId: PropTypes.number,
  title: PropTypes.string,
};

Reports.defaultProps = {};

export default withStyles(s)(Reports);
