import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ReportItem.css';
import Paper from 'material-ui/Paper';
import Avatar from 'material-ui/Avatar';
import classnames from 'classnames';
import SimpleDateFormat from '../../../../../util/dataFormat/SimpleDateFormat';
import EnhancedButton from 'material-ui/internal/EnhancedButton';

class ReportItem extends React.Component {
  onClickItem = (userId) => {
    window.open(`/users?userId=${userId}`);
  };

  renderAvatar = () => {
    const defaultUI = (<div style={{ width: '70px', height: '70px'}}> 프로필 이미지 없음 </div>);
    const { reportData, type } = this.props;
    if (!reportData[type]) {
      return defaultUI
    }
    if (!reportData[type].userProfileImage) {
      return defaultUI;
    }

    if (reportData[type].userProfileImage.length <= 0) {
      return defaultUI;
    }

    return (
      <Avatar
        size={70}
        src={reportData[type].userProfileImage[0].profileFile.fileUrl}
      />
    );
  };

  render() {
    const { reportData, type } = this.props;
    return (
      <EnhancedButton
        onClick={() => this.onClickItem(reportData[type].userId)}
        style={{ width: '100%' }}
      >
        <Paper zDepth={3}>
          <div className={s.wrap}>
            <div className={classnames(s.itemUserInfo)}>
              <div>{this.renderAvatar()}</div>
              <div>
                {reportData[type] ? reportData[type].userName : null}
              </div>
            </div>
            <div className={s.contentWrap}>
              <div className={s.contentRow}>
                <label> 신고 번호 </label>
                <span>
                  {reportData.reportId}
                </span>
              </div>
              <div className={s.contentRow}>
                <label> 신고 타입 </label>
                <span>
                  {reportData.reportType}
                </span>
              </div>
              <div className={s.contentRow}>
                <label> 신고 일자 </label>
                <span>
                  {SimpleDateFormat(reportData.reportDate)}
                </span>
              </div>
              <div className={s.contentRow}>
                <label> 신고내용 </label>
                <span>
                  {reportData.reportContent}
                </span>
              </div>
            </div>
          </div>
        </Paper>
      </EnhancedButton>
    );
  }
}

ReportItem.propTypes = {
  reportData: PropTypes.object,
  type: PropTypes.string,
};

ReportItem.defaultProps = {};

export default withStyles(s)(ReportItem);
