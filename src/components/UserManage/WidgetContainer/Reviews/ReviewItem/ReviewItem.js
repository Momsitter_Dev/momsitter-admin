import classnames from 'classnames';
import Avatar from 'material-ui/Avatar';
import Paper from 'material-ui/Paper';
import EnhancedButton from 'material-ui/internal/EnhancedButton';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ReviewItem.css';
import SimpleDateFormat from '../../../../../util/dataFormat/SimpleDateFormat';
import ReviewTypeGroup from '../../../../../util/const/ReviewTypeGroup';
import ReviewStatusGroup from '../../../../../util/const/ReviewStatusGroup';

const propTypes = {
  reviewData: PropTypes.shape({
    applyId: PropTypes.number,
    readDate: PropTypes.string,
    reviewContent: PropTypes.string,
    reviewId: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]),
    reviewRate: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]),
    reviewType: PropTypes.string,
    sender: PropTypes.object,
    senderId: PropTypes.number,
    status: PropTypes.string,
    targetId: PropTypes.number,
    writeDate: PropTypes.string,
  }).isRequired,
  type: PropTypes.string.isRequired,
};

const defaultProps = {};

class ReviewItem extends React.Component {
  onClickItem = (userId) => {
    window.open(`/users?userId=${userId}`);
  };

  getReviewStatus = (reviewStatus) => {
    if (reviewStatus === ReviewStatusGroup.OPEN) {
      return '열람';
    }
    if (reviewStatus === ReviewStatusGroup.CLOSE) {
      return '미열람';
    }
    return '알수없음';
  };

  getReviewType = (reviewType) => {
    if (reviewType === ReviewTypeGroup.FAILURE) {
      return '실패';
    }
    if (reviewType === ReviewTypeGroup.INTERVIEW) {
      return '인터뷰';
    }
    if (reviewType === ReviewTypeGroup.RECRUIT) {
      return '채용';
    }
    return '알수없음';
  };

  renderAvatar = () => {
    const defaultUI = (<div style={{ width: '70px', height: '70px'}}> 프로필 이미지 없음 </div>);
    const { reviewData, type } = this.props;
    if (!reviewData[type]) {
      return defaultUI;
    }
    if (!reviewData[type].userProfileImage) {
      return defaultUI;
    }

    if (reviewData[type].userProfileImage.length <= 0) {
      return defaultUI;
    }

    return (
      <Avatar
        size={70}
        src={reviewData[type].userProfileImage[0].profileFile.fileUrl}
      />
    );
  };

  render() {
    const { reviewData, type } = this.props;

    return (
      <EnhancedButton
        onClick={() => this.onClickItem(reviewData[`${type}Id`])}
        style={{ width: '100%' }}
      >
        <Paper zDepth={3}>
          <div className={classnames(s.wrap)}>
            <div className={classnames(s.itemUserInfo)}>
              <div> {this.renderAvatar()} </div>
              <div className={s.userName}>
                {reviewData[type] ? reviewData[type].userName : null}
              </div>
            </div>
            <div className={s.reviewContentWrap}>
              <div className={s.reviewContentRow}>
                <label> 회원 번호 </label>
                <span>{reviewData[type + 'Id']}</span>
              </div>
              <div className={s.reviewContentRow}>
                <label> 후기 작성일자 </label>
                <span>
                  {SimpleDateFormat(reviewData.writeDate)}
                </span>
              </div>
              <div className={s.reviewContentRow}>
                <label> 후기 타입 </label>
                <span> {this.getReviewType(reviewData.reviewType)} </span>
              </div>
              <div className={s.reviewContentRow}>
                <label> 후기 상태 </label>
                <span> {this.getReviewStatus(reviewData.status)} </span>
              </div>
              <div className={s.reviewContentRow}>
                <label> 후기 내용 </label>
                <span>
                  {reviewData.reviewContent}
                </span>
              </div>
            </div>
          </div>
        </Paper>
      </EnhancedButton>
    );
  }
}

ReviewItem.propTypes = propTypes;
ReviewItem.defaultProps = defaultProps;

export default withStyles(s)(ReviewItem);
