import Paper from 'material-ui/Paper';
import CircularProgress from 'material-ui/CircularProgress';
import WarningIcon from 'material-ui/svg-icons/alert/warning';
import IconButton from 'material-ui/IconButton';
import { Tabs, Tab } from 'material-ui/Tabs';

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Reviews.css';
import Widget from '../Widget';
import ReviewItem from './ReviewItem';
import Request from '../../../../util/Request';

class Reviews extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      reviews: {
        send: [],
        receive: [],
      },
      onLoading: true,
      onError: false,
    };
    this.getReviews = this.getReviews.bind(this);
    this.onRefresh = this.onRefresh.bind(this);
  }

  async componentDidMount() {
    await this.getReviews(this.props.userId);
  }

  async componentWillReceiveProps(nextProps) {
    if (this.props.userId !== nextProps.userId) {
      await this.getReviews(nextProps.userId);
    }
  }

  getReviews = async function (userId) {
    this.setState({ onLoading: true });
    const response = await Request(`api/users/${userId}/reviews`, { method: 'GET' });
    if (response) {
      if (typeof window !== 'undefined') {
        const self = this;
        const timerIndex = window.setTimeout(() => {
          self.setState({
            reviews: response.data,
            onLoading: false,
          });
          window.clearTimeout(timerIndex);
        }, 500);
      } else {
        this.setState({
          reviews: response.data,
          onLoading: false,
        });
      }
    } else {
      this.setState({
        onError: true,
        onLoading: false,
      });
    }
  };

  onRefresh = async function () {
    await this.getReviews(this.props.userId);
  };

  onClickWarning = () => {
    if (typeof window !== 'undefined') {
      if (window.confirm('정보를 다시 로드하시겠습니까?')) {
        this.setState({
          onError: false
        }, () => {
          this.onRefresh();
        });
      }
    }
  };

  renderSendedReview = (onError, onLoading, reviews) => {
    if (onError) {
      return (
        <Paper style={{ textAlign: 'center' }}>
          <IconButton onClick={this.onClickWarning}>
            <WarningIcon />
          </IconButton>
          <div style={{ marginTop: '10px' }}> 오류가 발생했습니다. </div>
        </Paper>
      );
    } else {
      if (onLoading) {
        return (
          <Paper style={{ textAlign: 'center' }}>
            <CircularProgress size={80} thickness={5} />
            <div style={{ marginTop: '10px' }}> 데이터를 불러오는 중입니다. </div>
          </Paper>
        );
      } else {
        if (reviews && reviews.send && reviews.send.length > 0) {
          return reviews.send.map((item, index) => {
            return (
              <ReviewItem
                key={`review-send-item-${index}`}
                type="target"
                reviewData={item}
              />);
          });
        } else {
          return (
            <Paper zDepth={2}>
              <div className={s.no_content}>해당 회원이 작성한 후기가 없습니다.</div>
            </Paper>
          );
        }
      }
    }
  };

  renderReceivedReview = (onError, onLoading, reviews) => {
    if (onError) {
      return (
        <Paper style={{ textAlign: 'center' }}>
          <IconButton onClick={this.onClickWarning}>
            <WarningIcon />
          </IconButton>
          <div style={{ marginTop: '10px' }}> 오류가 발생했습니다. </div>
        </Paper>
      );
    } else {
      if (onLoading) {
        return (
          <Paper style={{ textAlign: 'center' }}>
            <CircularProgress size={80} thickness={5} />
            <div style={{ marginTop: '10px' }}> 데이터를 불러오는 중입니다. </div>
          </Paper>
        );
      } else {
        if (reviews && reviews.receive && reviews.receive.length > 0) {
          return reviews.receive.map((item, index) => {
            return (
              <ReviewItem
                key={`review-received-item-${index}`}
                type="sender"
                reviewData={item}
              />);
          });
        } else {
          return (
            <Paper zDepth={2}>
              <div className={s.no_content}>해당 회원에게 작성된 후기가 없습니다.</div>
            </Paper>
          );
        }
      }
    }
  };

  render() {
    const {
      onError,
      onLoading,
      reviews
    } = this.state;
    return (
      <Widget title={this.props.title} onRefresh={this.onRefresh}>
        <Tabs>
          <Tab label="작성한 후기"> {this.renderSendedReview(onError, onLoading, reviews)} </Tab>
          <Tab label="받은 후기"> {this.renderReceivedReview(onError, onLoading, reviews)} </Tab>
        </Tabs>
      </Widget>
    );
  }
}

Reviews.propTypes = {
  userId: PropTypes.number,
  title: PropTypes.string,
};

Reviews.defaultProps = {};

export default withStyles(s)(Reviews);
