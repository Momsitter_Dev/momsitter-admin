import uuid from 'uuid/v4';
import classnames from 'classnames';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './PaymentItem.css';
import SimpleDateFormat from '../../../../../util/dataFormat/SimpleDateFormat';
import PaymentStatusGroup from '../../../../../util/const/PaymentStatusGroup';
import getFormattedMoney from '../../../../../util/dataFormat/getFormattedMoney';

const propTypes = {
  paymentData: PropTypes.shape({
    card_name: PropTypes.string,
    couponDiscount: PropTypes.number,
    pointDiscount: PropTypes.number,
    csturl: PropTypes.string,
    goodname: PropTypes.string,
    memo: PropTypes.string,
    mul_no: PropTypes.string,
    pay_addr: PropTypes.string,
    pay_date: PropTypes.string,
    pay_memo: PropTypes.string,
    pay_state: PropTypes.number,
    pay_type: PropTypes.number,
    paymentDoneDate: PropTypes.string,
    paymentId: PropTypes.number,
    paymentPrice: PropTypes.number,
    paymentRequestDate: PropTypes.string,
    payurl: PropTypes.string,
    paymentStatus: PropTypes.string,
    price: PropTypes.number,
    productCode: PropTypes.string,
    recvphone: PropTypes.string,
    reqaddr: PropTypes.string,
    reqdate: PropTypes.string,
    userId: PropTypes.number,
    var1: PropTypes.string,
    var2: PropTypes.string,
    vbank: PropTypes.string,
    vbankno: PropTypes.string,
    paymentCoupon: PropTypes.arrayOf(
      PropTypes.shape({
        codeId: PropTypes.number,
        couponId: PropTypes.number,
        couponTypeId: PropTypes.number,
        expireDate: PropTypes.string,
        paymentId: PropTypes.number,
        pubDate: PropTypes.string,
        read: PropTypes.bool,
        status: PropTypes.string,
        useDate: PropTypes.string,
        userId: PropTypes.number,
        couponType: PropTypes.shape({
          couponDate: PropTypes.number,
          couponName: PropTypes.string,
          couponPrice: PropTypes.number,
          couponTypeId: PropTypes.number,
          registerDate: PropTypes.string,
          status: PropTypes.string,
          target: PropTypes.string,
        }),
      }),
    ),
    paymentProduct: PropTypes.shape({
      productApplyToParentCount: PropTypes.number,
      productApplyToSitterCount: PropTypes.number,
      productCode: PropTypes.string,
      productDesc: PropTypes.string,
      productFor: PropTypes.string,
      productId: PropTypes.number,
      productName: PropTypes.string,
      productPeriod: PropTypes.number,
      productPrice: PropTypes.number,
      productType: PropTypes.string,
    }),
  }),
};
const defaultProps = {
  paymentData: null,
};

class PaymentItem extends React.Component {
  getPaymentStatusText = (status) => {
    if (status === PaymentStatusGroup.ADMIN_ABORT) {
      return '관리자 취소';
    }
    if (status === PaymentStatusGroup.DONE){
      return '결제 완료';
    }
    if (status === PaymentStatusGroup.REQUEST){
      return '결제 요청';
    }
    if (status === PaymentStatusGroup.USER_ABORT){
      return '회원 취소';
    }
    if (status === PaymentStatusGroup.WAITING) {
      return '결제 대기';
    }
    return status;
  };

  renderBillButton = () => {
    const { csturl } = this.props.paymentData;
    if (csturl) {
      return (
        <RaisedButton
          label="영수증 보기"
          onClick={() => { window.open(csturl); }}
          primary
          fullWidth
        />
      );
    }
    return null;
  };

  renderActualPaymentPrice = () => {
    const {
      paymentCoupon,
      couponDiscount,
      pointDiscount,
      paymentPrice,
      paymentProduct,
    } = this.props.paymentData;
    const ADDTIANL_UI = {
      coupon: null,
      point: null,
    };

    try {
      let price = paymentProduct.productPrice;
      if (couponDiscount) {
        ADDTIANL_UI.coupon = paymentCoupon.map((item) => {
          price -= item.couponType.couponPrice;
          return (
            <div className={s.couponRow} key={uuid()}>
              <label> {item.couponType.couponName} </label>
              <div> - {getFormattedMoney(item.couponType.couponPrice)} 원 </div>
            </div>
          );
        });
      }
      if (pointDiscount) {
        ADDTIANL_UI.point = (
          <div className={s.couponRow} key={uuid()}>
            <label> 포인트 사용 </label>
            <div> - {getFormattedMoney(pointDiscount)} 원 </div>
          </div>
        );
      }
    } catch (err) {
      return null;
    }

    return (
      <div>
        <div className={s.couponRow} style={{ color: '#000' }}>
          <label> 상품 가격 </label>
          <div> {getFormattedMoney(paymentProduct.productPrice)} 원 </div>
        </div>
        {ADDTIANL_UI.coupon}
        {ADDTIANL_UI.point}
        <div className={s.couponRow} style={{ color: '#000' }}>
          <label> 최종 결제 금액 </label>
          <div style={{ fontWeight: 'bold' }}>
            {getFormattedMoney(paymentPrice)} 원
          </div>
        </div>
      </div>
    );
  };

  render() {
    const { paymentData } = this.props;
    return (
      <Paper zDepth={3} className={classnames(s.wrap, 'col-xl-12')}>
        <div className={s.headerRow}>
          <div> {SimpleDateFormat(paymentData.paymentRequestDate)} </div>
          <div style={{ color: '#d3d3d3' }}> {paymentData.paymentId} </div>
        </div>
        <div className={s.wrap}>
          <div className={s.contentRow}>
            <label> 결제 상태 </label>
            <span> {this.getPaymentStatusText(paymentData.paymentStatus)} </span>
          </div>
          <div className={s.contentRow}>
            <label> 상품 코드 </label>
            <span> {paymentData.productCode} </span>
          </div>
          <div className={s.contentRow}>
            <label style={{ marginBottom: '10px' }}> 실제 결제 금액 </label>
            {this.renderActualPaymentPrice()}
          </div>
          {this.renderBillButton()}
        </div>
      </Paper>
    );
  }
}

PaymentItem.propTypes = propTypes;
PaymentItem.defaultProps = defaultProps;

export default withStyles(s)(PaymentItem);
