import IconButton from 'material-ui/IconButton';
import CircularProgress from 'material-ui/CircularProgress';
import WarningIcon from 'material-ui/svg-icons/alert/warning';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Payments.css';
import PaymentItem from './PaymentItem';
import Widget from '../Widget';
import Request from '../../../../util/Request';

const propTypes = {
  title: PropTypes.string,
  userId: PropTypes.number.isRequired,
};
const defaultProps = {
  title: '결제현황',
};

class Payments extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      payments: null,
      onLoading: true,
      onError: false,
    };
    this.getPayments = this.getPayments.bind(this);
    this.onRefresh = this.onRefresh.bind(this);
  }

  async componentDidMount() {
    await this.getPayments(this.props.userId);
  }

  async componentWillReceiveProps(nextProps) {
    if (this.props.userId !== nextProps.userId) {
      await this.getPayments(nextProps.userId);
    }
  }

  getPayments = async function (userId) {
    this.setState({ onLoading: true });
    const response = await Request(`api/users/${userId}/payments`, { method: 'GET' });
    if (response) {
      if (typeof window !== 'undefined') {
        const self = this;
        const timerIndex = window.setTimeout(() => {
          self.setState({
            onLoading: false,
            payments: response.data,
          });
          window.clearTimeout(timerIndex);
        }, 500);
      } else {
        this.setState({
          onLoading: false,
          payments: response.data,
        });
      }
    } else {
      this.setState({
        onError: true,
        onLoading: false,
      });
    }
  };

  onRefresh = async function () {
    await this.getPayments(this.props.userId);
  };

  renderPayments = (onError, onLoading, payments) => {
    if (onError) {
      return (
        <div style={{ textAlign: 'center' }}>
          <IconButton onClick={this.onClickWarning}>
            <WarningIcon />
          </IconButton>
          <div style={{ marginTop: '10px' }}> 오류가 발생했습니다. </div>
        </div>
      );
    } else {
      if (onLoading) {
        return (
          <div style={{ textAlign: 'center' }}>
            <CircularProgress size={80} thickness={5} />
            <div style={{ marginTop: '10px' }}> 데이터를 불러오는 중입니다. </div>
          </div>
        );
      } else {
        if (payments && payments.length > 0) {
          return (
            <div>
              {payments.map((item, index) =>
                <PaymentItem key={`payment-item-${index}`} paymentData={item} />,
              )}
            </div>
          );
        } else {
          return <div>결제 내역이 없습니다.</div>;
        }
      }
    }
  };

  render() {
    const { onError, onLoading, payments } = this.state;
    return (
      <Widget
        title={this.props.title}
        onRefresh={this.onRefresh}
      >
        {this.renderPayments(onError, onLoading, payments)}
      </Widget>
    );
  }
}

Payments.propTypes = propTypes;
Payments.defaultProps = defaultProps;

export default withStyles(s)(Payments);
