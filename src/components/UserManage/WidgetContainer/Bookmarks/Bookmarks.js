import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Bookmarks.css';
import Widget from '../Widget';
import Request from '../../../../util/Request';
import BookmarkItem from './BookmarkItem';
import {Tabs, Tab} from 'material-ui/Tabs';
import Paper from 'material-ui/Paper';

class Bookmarks extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      bookmarks: {
        send: [],
        receive: [],
      },
    };

    this.getBookmarks = this.getBookmarks.bind(this);
    this.onRefresh = this.onRefresh.bind(this);
  }

  async componentDidMount() {
    await this.getBookmarks(this.props.userId);
  }

  async componentWillReceiveProps(nextProps) {
    if (this.props.userId !== nextProps.userId) {
      await this.getBookmarks(nextProps.userId);
    }
  }

  getBookmarks = async function (userId) {
    const response = await Request(`api/users/${userId}/bookmarks`, { method: 'GET' });
    if (response) {
      this.setState({ reports: response.data });
    } else {
      console.log('fail to fetch');
    }
  };

  onRefresh = async function () {
    await this.getBookmarks(this.props.userId);
  };

  render() {
    const { bookmarks } = this.state;
    let sendUI = [];
    let receiveUI = [];

    if (bookmarks) {
      if (bookmarks.send && bookmarks.send.length > 0) {
        sendUI = bookmarks.send.map((item, index) => {
          return (
            <BookmarkItem
              key={index}
              bookmarkData={item}
            />);
        });
      } else {
        sendUI = (
          <Paper>
            <div className={s.no_content}>해당 회원이 관심표시한 회원이 없습니다.</div>
          </Paper>
        );
      }

      if (bookmarks.receive && bookmarks.receive.length > 0) {
        receiveUI = bookmarks.receive.map((item, index) => {
          return (
            <BookmarkItem
              key={index}
              bookmarkData={item}
            />);
        });
      } else {
        receiveUI = (
          <Paper>
            <div className={s.no_content}>해당 회원에게 관심을 표한 회원이 없습니다.</div>
          </Paper>
        );
      }
    }

    return (
      <Widget
        title={this.props.title}
        onRefresh={this.onRefresh}
      >
        <Tabs>
          <Tab label="표현한 관심">{sendUI}</Tab>
          <Tab label="받은 관심">{receiveUI}</Tab>
        </Tabs>
      </Widget>
    );
  }
}

Bookmarks.propTypes = {
  title: PropTypes.string,
  userId: PropTypes.number,
};

Bookmarks.defaultProps = {};

export default withStyles(s)(Bookmarks);
