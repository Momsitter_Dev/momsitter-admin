import moment from 'moment';
import { compose } from 'recompose';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Grow from '@material-ui/core/Grow';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './UserAgent.css';
import Widget from '../../../Widget';
import Request from '../../../../util/Request';
import ResponseResultTypeGroup from '../../../../util/const/ResponseResultTypeGroup';
import UserAgentPlatformGroup from '../../../../util/const/UserAgentPlatformGroup';
import UserAgentPlatformNameGroup from '../../../../util/const/UserAgentPlatformNameGroup';

moment.locale('ko');

const propTypes = {
  userId: PropTypes.number.isRequired,
};
const defaultProps = {};

class UserAgent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      pending: true,
      exception: false,
      currentTab: null,
    };
    this.getUserAgent = this.getUserAgent.bind(this);
    this.renderContent = this.renderContent.bind(this);
  }

  async componentDidMount() {
    const data = await this.getUserAgent();
    this.setState({
      pending: false,
      data,
    });
  }

  async componentWillReceiveProps(nextProps) {
    if (this.props.userId !== nextProps.userId) {
      const data = await this.getUserAgent();
      this.setState({
        pending: false,
        data,
      });
    }
  }

  onChangeTab = (event, value) => {
    return this.setState({
      currentTab: value,
    });
  };

  getUserAgent = async function () {
    try {
      const { userId } = this.props;
      const response = await Request(`api/log/custom/${userId}`, {
        method: 'GET',
      });
      if (response.result === ResponseResultTypeGroup.SUCCESS) {
        return response.data;
      }
      throw new Error('request fail');
    } catch (err) {
      console.log(err);
      return null;
    }
  };

  renderPending = () => {
    return (
      <div>
        <CircularProgress size={50} />
      </div>
    );
  };

  renderUserAgent = () => {
    const { currentTab, data } = this.state;
    const values = Object.values(UserAgentPlatformGroup);
    const tabs = values.map((item, index) => {
      if (data[item] && data[item].length > 0) {
        return (
          <Tab
            key={`tab-item-${index}`}
            label={UserAgentPlatformNameGroup[item]}
            value={item}
          />
        );
      }
      return null;
    });

    return (
      <div className={s.container}>
        <AppBar position="static" color="default">
          <Tabs
            value={currentTab}
            onChange={this.onChangeTab}
          >
            {tabs}
          </Tabs>
        </AppBar>
        <div>
          {this.renderTabItems(currentTab)}
        </div>
      </div>
    );
  };

  renderTabItems = (tabKey) => {
    const { data, currentTab } = this.state;
    const currentData = data[tabKey];
    if (!currentTab) {
      return (
        <Paper style={{ padding: '15px', backgroundColor: '#F4F8FB' }}>
          <div> 탭을 선택해 주세요. </div>
        </Paper>
      );
    }
    if (!currentData) {
      return (
        <Paper style={{ padding: '15px', backgroundColor: '#F4F8FB' }}>
          <div> 표시할 정보가 없습니다. </div>
        </Paper>
      );
    }
    if (currentData.length === 0) {
      return (
        <Paper style={{ padding: '15px', backgroundColor: '#F4F8FB' }}>
          <div> 표시할 정보가 없습니다. </div>
        </Paper>
      );
    }
    return currentData.map((item, index) => {
      return (
        <Grow in key={`user-agent-tab-item-${tabKey}-${index}`}>
          <Paper style={{ padding: '15px', backgroundColor: '#F4F8FB' }}>
            <div>
              <Grid container spacing={16}>
                <Grid item xs={2}>
                  <Typography variant={'caption'}> 운영체제 </Typography>
                </Grid>
                <Grid item xs={2}>
                  <Typography variant={'caption'}> 브라우저 </Typography>
                </Grid>
                <Grid item xs={2}>
                  <Typography variant={'caption'}> 장치 </Typography>
                </Grid>
                <Grid item xs={4}>
                  <Typography variant={'caption'}> 접속일자 </Typography>
                </Grid>
                <Grid item xs={2} />
              </Grid>
              <Grid container spacing={16}>
                <Grid item xs={2}> {item.os} {item.osVersion} </Grid>
                <Grid item xs={2}> {item.browser} {item.browserVersion} </Grid>
                <Grid item xs={2}> {item.device} {item.deviceVersion} </Grid>
                <Grid item xs={4}> {moment(item.logDate).format('YYYY년 MM월 DD일 HH시 mm분')} </Grid>
                <Grid item xs={2} />
              </Grid>
            </div>
          </Paper>
        </Grow>
      );
    });
  };

  renderContent = () => {
    const { pending, data } = this.state;
    if (pending) {
      return this.renderPending();
    }
    if (data) {
      return this.renderUserAgent();
    }
    return (
      <div> 표시할 데이터가 없습니다. </div>
    );
  };

  render() {
    return (
      <Widget
        title={'사용자 환경'}
        defaultExpanded={false}
      >
        <div className={s.root}>
          {this.renderContent()}
        </div>
      </Widget>
    );
  }
}

UserAgent.propTypes = propTypes;
UserAgent.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(UserAgent);
