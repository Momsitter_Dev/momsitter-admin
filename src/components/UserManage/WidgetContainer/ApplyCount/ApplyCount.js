import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Typography from '@material-ui/core/Typography';
import Snackbar from '@material-ui/core/Snackbar';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ApplyCount.css';
import Request from '../../../../util/Request';
import ResponseResultTypeGroup from '../../../../util/const/ResponseResultTypeGroup';

const propTypes = {
  userId: PropTypes.number.isRequired,
};
const defaultProps = {};

class ApplyCount extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pending: false,
      exception: false,
      applyToSitterCount: 0,
      applyToParentCount: 0,
      open: false,
      snackbar: false,
      snackbarMessage: '',
    };
    this.onRefresh = this.onRefresh.bind(this);
    this.onSaveApplyCount = this.onSaveApplyCount.bind(this);
    this.onUpdateApplyCount = this.onUpdateApplyCount.bind(this);
    this.getUserApplyCount = this.getUserApplyCount.bind(this);
    this.onClickApplyCount = this.onClickApplyCount.bind(this);
    this.applyCountType = 'applyToSitterCount';
    this.input = null;
  }

  async componentDidMount() {
    try {
      const { applyToSitterCount, applyToParentCount } = await this.getUserApplyCount();
      return this.setState({
        applyToSitterCount,
        applyToParentCount,
      });
    } catch (err) {
      console.log(err);
    }
  }

  async componentWillReceiveProps(nextProps) {
    try {
      if (this.props.userId !== nextProps.userId) {
        const { applyToSitterCount, applyToParentCount } = await this.getUserApplyCount(nextProps.userId);
        return this.setState({
          applyToSitterCount,
          applyToParentCount,
        });
      }
    } catch (err) {
      console.log(err);
    }
  }

  onRefresh = async () => {
    try {
      const { applyToSitterCount, applyToParentCount } = await this.getUserApplyCount();
      return this.setState({
        applyToSitterCount,
        applyToParentCount,
      });
    } catch (err) {
      console.log(err);
      return null;
    }
  };

  onUpdateApplyCount = async function (type, value) {
    try {
      const { userId } = this.props;
      const body = {};
      body[type] = value;
      const response = await Request(`api/users/${userId}/applyCount`, {
        method: 'PUT',
        data: body,
      });
      if (response.result === ResponseResultTypeGroup.SUCCESS) {
        return true;
      }
      throw new Error('request fail');
    } catch (err) {
      console.log(err);
      return false;
    }
  };

  onSaveApplyCount = async function () {
    const result = await this.onUpdateApplyCount(this.applyCountType, this.input.value);
    if (result) {
      this.openSnackbar('저장되었습니다.');
      this.closeDialog();
      const state = {};
      state[this.applyCountType] = this.input.value;
      this.setState({
        ...state,
      });
      return true;
    }
    this.openSnackbar('오류가 발생했습니다.');
    return false;
  };

  onClickApplyCount = (type) => {
    this.applyCountType = type;
    if (this.input) {
      this.input.value = this.state[type];
    }
    return this.openDialog();
  };

  closeDialog = () => {
    return this.setState({
      open: false,
    });
  };

  openDialog = () => {
    return this.setState({
      open: true,
    });
  };

  openSnackbar = (message) => {
    return this.setState({
      snackbar: true,
      snackbarMessage: message,
    });
  };

  closeSnackbar = () => {
    return this.setState({
      snackbar: false,
    });
  };

  getUserApplyCount = async function (id) {
    try {
      const { userId } = this.props;
      const response = await Request(`api/users/${id || userId}/applyCount`, { method: 'GET' });
      if (response.result === ResponseResultTypeGroup.SUCCESS) {
        return response.data;
      }
    } catch (err) {
      console.log(err);
      return null;
    }
  };

  getApplyCountType = () => {
    if (this.applyCountType === 'applyToSitterCount') {
      return '신청';
    }
    return '지원';
  };

  render() {
    const {
      applyToParentCount,
      applyToSitterCount,
      open,
      snackbar,
      snackbarMessage,
    } = this.state;
    return (
      <div className={s.root}>
        <Grid container spacing={16}>
          <Grid item xs={2}>
            <Button
              variant={'raised'}
              onClick={() => this.onClickApplyCount('applyToParentCount')}
              style={{ backgroundColor: '#fff', borderRadius: '5px' }}
            >
              <Typography variant={'caption'} style={{ marginRight: '5px' }}> 지원횟수 </Typography>
              <Typography variant={'button'} style={{ color: '#6974E4' }}> {applyToParentCount || 0} </Typography>
            </Button>
          </Grid>
          <Grid item xs={2}>
            <Button
              variant={'raised'}
              onClick={() => this.onClickApplyCount('applyToSitterCount')}
              style={{ backgroundColor: '#fff', borderRadius: '5px' }}
            >
              <Typography variant={'caption'} style={{ marginRight: '5px' }}> 신청횟수 </Typography>
              <Typography variant={'button'} style={{ color: '#6974E4' }}> {applyToSitterCount || 0} </Typography>
            </Button>
          </Grid>
        </Grid>
        <Dialog open={open}>
          <DialogTitle> {this.getApplyCountType()} 횟수 수정 </DialogTitle>
          <DialogContent>
            <DialogContentText style={{ marginBottom: '15px' }}> 회원이 보유하고 있는 이용권에 관계없이 {this.getApplyCountType()} 횟수가 갱신됩니다. </DialogContentText>
            <Grid container>
              <Grid item xs={3}>
                {this.getApplyCountType()} 횟수
              </Grid>
              <Grid item xs={9}>
                <TextField fullWidth inputRef={(r) => { this.input = r; }} />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button variant={'flat'} onClick={this.closeDialog}> 취소 </Button>
            <Button variant={'raised'} color={'primary'} onClick={this.onSaveApplyCount}> 저장 </Button>
          </DialogActions>
        </Dialog>
        <Snackbar
          open={snackbar}
          message={<span> {snackbarMessage} </span>}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          onClose={this.closeSnackbar}
        />
      </div>
    );
  }
}

ApplyCount.propTypes = propTypes;
ApplyCount.defaultProps = defaultProps;

export default withStyles(s)(ApplyCount);
