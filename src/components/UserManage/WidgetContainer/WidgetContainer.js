/* eslint-disable prettier/prettier */
import { toast } from 'react-toastify';
import { compose } from 'recompose';
import { Button, Grid, Paper, Typography, withStyles as withMaterialStyles } from '@material-ui/core';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './WidgetContainer.css';
import BasicInfo from './BasicInfo';
import Reviews from './Reviews';
import Applications from './Applications';
import Reports from './Reports';
import Products from './Products';
import Payments from './Payments';
import Certifications from './Certifications';
import Memos from './Memos';
import isParent from '../../../util/user/isParent';
import isSitter from '../../../util/user/isSitter';
import Points from './Points';
import Request from '../../../util/Request';
import ResponseResultTypeGroup from '../../../util/const/ResponseResultTypeGroup';
import history from '../../../history';
import ApplyCount from './ApplyCount';
import UserAgent from './UserAgent';
import WidgetMessages from '../../../containers/WidgetMessages';

import ParentProfile from '../../../containers/ParentProfile';
import SitterProfile from '../../../containers/SitterProfile';
import WidgetCoupon from '../../../containers/WidgetCoupon';

const propTypes = {
  userId: PropTypes.number,
  userTypeId: PropTypes.number,
};

const defaultProps = {
  userId: null,
  userTypeId: null,
};

class WidgetContainer extends React.Component {
  constructor(props) {
    super(props);
    this.onClickDeleteUser = this.onClickDeleteUser.bind(this);
  }
  onClickDeleteUser = async function() {
    const { userId } = this.props;
    if (confirm(`${userId} 회원의 모든 정보가 삭제됩니다. 계속 진행하시겠습니까?`)) {
      try {
        const response = await Request(`api/destroy/${userId}`, { method: 'DELETE' });
        if (response && response.result === ResponseResultTypeGroup.SUCCESS) {
          toast.success(`${userId} 회원 정보가 모두 제거되었습니다.`);
          return setTimeout(() => {
            // NOTE: 새로고침과 같은 효과를 줘야함 (모든 위젯이 재 로딩되어야함)
            return history.go('/users');
          }, 1000)
        }
        throw new Error('request fail');
      } catch (err) {
        return toast.error(`${userId} 회원 정보를 삭제할 수 없습니다.`);
      }
    }
    return null;
  };

  openNotificationDialog = (title, message) => {
    return toast.info(`[${title}] : ${message}`);
  };

  renderApplications = (userId, userTypeId) => {
    const title = '신청/지원 현황';
    const tabTitle = [];
    if (isParent(userTypeId)) {
      tabTitle.push('내가 신청한');
      tabTitle.push('내게 지원한');
    } else {
      tabTitle.push('내가 지원한');
      tabTitle.push('내게 신청한');
    }
    return <Applications title={title} tabTitle={tabTitle} userId={userId} />;
  };

  renderProfile = () => {
    const { userId, userTypeId } = this.props;
    if (isParent(userTypeId)) {
      return <ParentProfile userId={userId} userTypeId={userTypeId} />;
    }
    if (isSitter(userTypeId)) {
      return <SitterProfile userId={userId} userTypeId={userTypeId} />;
    }
    return <Typography> 유효하지 않은 회원 타입입니다. {userTypeId} </Typography>;
  };

  render() {
    const { userId, userTypeId } = this.props;
    if (!userId) {
      return (
        <Grid container spacing={16}>
          <Grid item xs={12}>
            <Paper style={{ padding: '15px' }}>
              <Typography variant={'caption'}> 회원을 선택해 주세요! </Typography>
            </Paper>
          </Grid>
        </Grid>
      );
    }
    return (
      <Grid container spacing={16}>
        <Grid item xs={12}>
          <ApplyCount userId={userId} />
        </Grid>
        <Grid item xs={12}>
          <UserAgent userId={userId} />
        </Grid>
        <Grid item xs={12} sm={12} md={6}>
          <BasicInfo
            title="기본 정보"
            userId={userId}
            userTypeId={userTypeId}
            openNotificationDialog={this.openNotificationDialog}
          />
        </Grid>
        <Grid item xs={12} sm={12} md={6}>
          {this.renderProfile()}
        </Grid>
        <Grid item xs={12} sm={12} md={6}>
          {this.renderApplications(userId, userTypeId)}
        </Grid>
        <Grid item xs={12} sm={12} md={6}>
          <Products title="이용권 현황" userId={userId} openNotificationDialog={this.openNotificationDialog} />
        </Grid>
        <Grid item xs={12} sm={12} md={6}>
          <Reviews title="후기 현황" userId={userId} openNotificationDialog={this.openNotificationDialog} />
        </Grid>
        <Grid item xs={12} sm={12} md={6}>
          <Reports title="신고 현황" userId={userId} openNotificationDialog={this.openNotificationDialog} />
        </Grid>
        <Grid item xs={12} sm={12} md={6}>
          <Payments title="결제 내역" userId={userId} openNotificationDialog={this.openNotificationDialog} />
        </Grid>
        <Grid item xs={12} sm={12} md={6}>
          <Certifications title="인증 내역" userId={userId} openNotificationDialog={this.openNotificationDialog} />
        </Grid>
        <Grid item xs={12} sm={12} md={6}>
          <WidgetMessages userId={userId} />
        </Grid>
        <Grid item xs={12} sm={12} md={6}>
          <WidgetCoupon userId={userId} />
        </Grid>
        <Grid item xs={12} sm={12} md={12}>
          <Points userId={userId} openNotificationDialog={this.openNotificationDialog} />
        </Grid>
        <Grid item xs={12} sm={12} md={12}>
          <Memos title="메모 내역" userId={userId} openNotificationDialog={this.openNotificationDialog} />
        </Grid>
        <Grid item xs={12}>
          <Button variant={'raised'} onClick={this.onClickDeleteUser}>
            회원 정보 삭제
          </Button>
        </Grid>
      </Grid>
    );
  }
}

WidgetContainer.propTypes = propTypes;
WidgetContainer.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(WidgetContainer);
