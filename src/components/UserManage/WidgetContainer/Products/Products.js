import _ from 'lodash';
import uuid from 'uuid/v4';
import moment from 'moment';
import CircularProgress from 'material-ui/CircularProgress';
import WarningIcon from 'material-ui/svg-icons/alert/warning';
import IconButton from 'material-ui/IconButton';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Products.css';
import Widget from '../Widget';
import ProductItem from './ProductItem';
import Request from '../../../../util/Request';
import EditProductDialog from '../../../Dialog/EditProductDialog';
import AddProductDialog from '../../../Dialog/AddProductDialog';
import ResponseResultTypeGroup from '../../../../util/const/ResponseResultTypeGroup';

class Products extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      products: null,
      onLoading: true,
      onError: false,
      addProductDialog: false,
      editProductDialog: false,
      addProductDialogData: {
        userId: props.userId,
        productId: '',
        paymentId: null,
        purchaseDate: null,
        expireDate: null,
        memo: '',
      },
      editProductDialogData: {
        userId: props.userId,
        productId: '',
        paymentId: null,
        purchaseDate: null,
        expireDate: null,
        memo: '',
      },
    };
    this.getProducts = this.getProducts.bind(this);
    this.onRefresh = this.onRefresh.bind(this);
    this.onSubmitAddProduct = this.onSubmitAddProduct.bind(this);
    this.onSaveEditDialog = this.onSaveEditDialog.bind(this);
    this.onDeleteProduct = this.onDeleteProduct.bind(this);
  }

  async componentDidMount() {
    await this.getProducts(this.props.userId);
  }

  async componentWillReceiveProps(nextProps) {
    if (this.props.userId !== nextProps.userId) {
      await this.getProducts(nextProps.userId);
    }
  }

  getProducts = async function (userId) {
    this.setState({
      onLoading: true,
    });
    const response = await Request(`api/users/${userId}/products`, { method: 'GET' });
    if (response) {
      if (typeof window !== 'undefined') {
        const self = this;
        const timerIndex = window.setTimeout(() => {
          self.setState({
            products: response.data,
            onLoading: false,
          });
          window.clearTimeout(timerIndex);
        }, 500);
      } else {
        this.setState({
          products: response.data,
          onLoading: false,
        });
      }
    } else {
      this.setState({
        onError: true,
        onLoading: false,
      });
    }
  };

  onRefresh = async function () {
    await this.getProducts(this.props.userId);
  };

  onClickWarning = () => {
    if (typeof window !== 'undefined') {
      if (window.confirm('정보를 다시 로드하시겠습니까?')) {
        this.setState({
          onError: false
        }, () => {
          this.onRefresh();
        });
      }
    }
  };

  onClickAddUserProduct = () => {
    const { userId } = this.props;
    const { addProductDialogData } = this.state;
    this.setState({
      addProductDialog: true,
      addProductDialogData: {
        ...addProductDialogData,
        userId,
      },
    });
  };

  onChangeAddProduct = (type, value) => {
    const { addProductDialogData } = this.state;
    let newData = Object.assign({}, addProductDialogData);
    newData[type] = value;
    this.setState({
      addProductDialogData: newData,
    });
  };

  onSubmitAddProduct = async function () {
    const { addProductDialogData, products } = this.state;
    if (this.validateAddProductDialogData(addProductDialogData)) {
      const response = await Request(`api/manage/user/${addProductDialogData.userId}/products`, {
        method: 'POST',
        data: addProductDialogData
      }).catch((err) => {
        console.log(err);
        return false;
      });

      if (response && response.result === ResponseResultTypeGroup.SUCCESS) {
        if (response.data) {
          let newList = Object.assign([], products);
          newList = [response.data, ...products];
          this.setState({
            list: newList,
          });
        }
        this.props.openNotificationDialog('알림', '저장되었습니다.');
        this.onCloseAddDialog();
      } else {
        this.props.openNotificationDialog('오류', '네트워크 오류입니다.');
      }
    }
  };

  onCloseAddDialog = () => {
    const { userId } = this.props;
    this.setState({
      addProductDialog: false,
      addProductDialogData: {
        userId,
        productId: '',
        paymentId: null,
        purchaseDate: null,
        expireDate: null,
        memo: '',
      },
    });
  };

  onClickEdit = (data) => {
    this.setState({
      editProductDialog: true,
      editProductDialogData: {
        ...data,
        purchaseDate: moment(data.purchaseDate).toDate(),
        expireDate: moment(data.expireDate).toDate(),
      },
    });
  };

  onChangeEditProductDialog = (type, value) => {
    const { editProductDialogData } = this.state;
    let newData = Object.assign({}, editProductDialogData);
    newData[type] = value;
    this.setState({
      editProductDialogData: newData,
    });
  };

  onSaveEditDialog = async function () {
    const { editProductDialogData, products } = this.state;

    if (!this.validateAddProductDialogData(editProductDialogData)) {
      return false;
    }
    const response = await Request(`api/manage/user/products/${editProductDialogData.userProductId}`, {
      method: 'PUT',
      data: editProductDialogData,
    }).catch((err) => {
      console.log(err);
    });

    if (!(response) || response.result === ResponseResultTypeGroup.FAIL) {
      this.closeEditProductDialog();
      this.props.openNotificationDialog('오류', '네트워크 오류입니다.');
      return false;
    }

    if (response && response.result === ResponseResultTypeGroup.SUCCESS) {
      this.closeEditProductDialog();
      this.props.openNotificationDialog('알림', '수정되었습니다.');
      this.findAndReplaceProductItem(editProductDialogData);
    }

    return false;
  };

  closeEditProductDialog = () => {
    const { userId } = this.props;
    this.setState({
      editProductDialog: false,
      editProductDialogData: {
        userId,
        productId: '',
        paymentId: null,
        purchaseDate: null,
        expireDate: null,
        memo: '',
      },
    });
  };

  onDeleteProduct = async function (userProductId) {
    if (typeof window !== 'undefined') {
      if (userProductId) {
        if (window.confirm('정말 삭제하시겠습니까?')) {
          const response = await Request(`api/manage/user/products/${userProductId}`, {
            method: 'DELETE',
          }).catch((err) => {
            console.log(err);
            return false;
          });
          if (response && response.result === ResponseResultTypeGroup.SUCCESS) {
            this.props.openNotificationDialog('알림', '삭제되었습니다.');
            this.findAndDeleteProductItemByUserProductId(userProductId);
          } else {
            this.props.openNotificationDialog('오류', '네트워크 오류입니다.');
          }
        }
      } else {
        this.props.openNotificationDialog('오류', '필수정보가 누락되었습니다.');
      }
    } else {
      this.props.openNotificationDialog('오류', '브라우저 환경에서 이용해 주세요.');
    }
  };

  findAndDeleteProductItemByUserProductId = (userProductId) => {
    const { products } = this.state;
    const targetInd = _.findIndex(products, (o) => {
      return o.userProductId === userProductId
    });
    if (targetInd !== -1) {
      const newList = _.filter(products, (o, index) => {
        return index !== targetInd;
      });
      this.setState({
        products: newList,
      });
    } else {
      //TODO: Handle Error
    }
  };

  findAndReplaceProductItem = (data) => {
    const { products } = this.state;
    if (data && data.userProductId) {
      try {
        const targetInd = _.findIndex(products, (o) => {
          return o.userProductId === data.userProductId;
        });
        if (targetInd !== -1) {
          let newList = Object.assign([], products);
          if (targetInd > 0) {
            const forward = newList.slice(targetInd - 1);
            const backward = newList.slice(targetInd + 1);
            newList = [...forward, data, backward];
          } else {
            newList[targetInd] = data;
          }
          this.setState({
            products: newList,
          });
        }
      } catch (e) {
        console.log(e);
      }
    }
  };

  validateAddProductDialogData = (data) => {
    let result = true;
    if (data) {
      for (let key in data) {
        if (key === 'userId' && !data[key]) {
          this.props.openNotificationDialog('오류', '사용자 번호를 입력해 주세요.');
          return false;
        }
        if (key === 'productId' && !data[key]) {
          this.props.openNotificationDialog('오류', '이용권 번호를 입력해 주세요.');
          return false;
        }
        if (key === 'purchaseDate' && !data[key]) {
          this.props.openNotificationDialog('오류', '이용권 만료일자를 입력해 주세요.');
          return false;
        }
        if (key === 'expireDate' && !data[key]) {
          this.props.openNotificationDialog('오류', '이용권 만료일자를 입력해 주세요.');
          return false;
        }
      }
    } else {
      result = false;
    }
    return result;
  };

  renderProductItems = (onError, onLoading, products) => {
    let ui = null;
    if (onError) {
      ui = (
        <div style={{ textAlign: 'center' }}>
          <IconButton onClick={this.onClickWarning}>
            <WarningIcon />
          </IconButton>
          <div style={{ marginTop: '10px' }}> 오류가 발생했습니다. </div>
        </div>
      );
    } else {
      if (onLoading) {
        ui = (
          <div style={{ textAlign: 'center' }}>
            <CircularProgress size={80} thickness={5} />
            <div style={{ marginTop: '10px' }}> 데이터를 불러오는 중입니다. </div>
          </div>
        );
      } else {
        if (products && products.length > 0) {
          ui = products.map((item) => {
            return (
              <ProductItem
                key={uuid()}
                productData={item}
                onClickEdit={this.onClickEdit}
                onClickDelete={this.onDeleteProduct}
              />
            );
          });
        } else {
          ui = (<div> 보유하고있는 이용권이 없습니다. </div>)
        }
      }
    }
    return ui;
  };

  render() {
    const {
      products,
      addProductDialog,
      addProductDialogData,
      editProductDialog,
      editProductDialogData,
      onError,
      onLoading,
    } = this.state;

    return (
      <Widget
        title={this.props.title}
        onRefresh={this.onRefresh}
        onAddItem={this.onClickAddUserProduct}
      >
        <div>
          {this.renderProductItems(onError, onLoading, products)}
        </div>
        <EditProductDialog
          title="이용권 수정"
          open={editProductDialog}
          data={editProductDialogData}
          onChange={this.onChangeEditProductDialog}
          onSubmit={this.onSaveEditDialog}
          closeDialog={this.closeEditProductDialog}
        />
        <AddProductDialog
          title="이용권 추가"
          open={addProductDialog}
          data={addProductDialogData}
          onChange={this.onChangeAddProduct}
          onCreate={this.onSubmitAddProduct}
          closeDialog={this.onCloseAddDialog}
        />
      </Widget>
    );
  }
}

Products.propTypes = {
  userId: PropTypes.number,
  title: PropTypes.string,
  openNotificationDialog: PropTypes.func.isRequired,
};

Products.defaultProps = {};

export default withStyles(s)(Products);
