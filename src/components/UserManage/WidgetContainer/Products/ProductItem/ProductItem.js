import uuid from 'uuid/v4';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ProductItem.css';
import SimpleDateFormat from '../../../../../util/dataFormat/SimpleDateFormat';
import getFormattedMoney from '../../../../../util/dataFormat/getFormattedMoney';

const propTypes = {
  productData: PropTypes.object,
  onClickEdit: PropTypes.func.isRequired,
  onClickDelete: PropTypes.func.isRequired,
};
const defaultProps = {
  productData: null,
};

class ProductItem extends React.Component {
  onClickEdit = (productData) => {
    this.props.onClickEdit(productData);
  };

  onDelete = (userProductId) => {
    this.props.onClickDelete(userProductId);
  };

  renderActualPaymentPrice = () => {
    const { userProductPayment, product, paymentId } = this.props.productData;
    try {
      if (!userProductPayment) {
        return '결제 정보를 찾을 수 없습니다.';
      }
      if (!product) {
        return '상품 정보를 찾을 수 없습니다.';
      }
      let ADDTIONAL_UI = {
        coupon: null,
        point: null,
      };
      let price = product.productPrice;
      const { paymentCoupon, couponDiscount, pointDiscount, paymentPrice } = userProductPayment;
      if (couponDiscount) {
        ADDTIONAL_UI.coupon = paymentCoupon.map((item) => {
          price -= item.couponType.couponPrice;
          return (
            <div className={s.couponRow} key={uuid()}>
              <label> {item.couponType.couponName} </label>
              <div> - {getFormattedMoney(item.couponType.couponPrice)} 원 </div>
            </div>
          );
        });
      }
      if (pointDiscount) {
        ADDTIONAL_UI.point = (
          <div className={s.couponRow} key={uuid()}>
            <label> 포인트 사용 </label>
            <div> - {getFormattedMoney(pointDiscount)} 원 </div>
          </div>
        );
      }

      return (
        <div>
          <div style={{ marginBottom: '10px', color: '#a5a5a5' }}> 결제 금액 </div>
          <div className={s.couponRow} style={{ color: '#000' }}>
            <label> 상품 가격 </label>
            <div> {getFormattedMoney(product.productPrice)} 원 </div>
          </div>
          {ADDTIONAL_UI.coupon}
          {ADDTIONAL_UI.point}
          <div className={s.couponRow} style={{ color: '#000' }}>
            <label> 최종 결제 금액 </label>
            <div style={{ fontWeight: 'bold' }}> {getFormattedMoney(paymentPrice)} 원 </div>
          </div>
        </div>
      )

    } catch (err) {
      return (
        <span> 금액을 표시할 수 없습니다. </span>
      );
    }
  };

  render() {
    const { productData } = this.props;
    return (
      <Paper zDepth={3}>
        <div className={s.headerRow}>
          {productData.userProductId}
        </div>
        <div className={s.wrap}>
          <div className={s.itemWrap}>
            <div className={s.itemRow}>
              <label> 이용권코드 </label>
              <span>{productData.product.productCode}</span>
            </div>
            <div className={s.itemRow}>
              <label> 이용권 이름 </label>
              <span>{productData.product.productName}</span>
            </div>
            <div className={s.itemRow}>
              <label> 구매일자 </label>
              <span>{SimpleDateFormat(productData.purchaseDate, 'YYYY년 MM월 DD일 HH시 mm분')}</span>
            </div>
            <div className={s.itemRow}>
              <label> 만료일자 </label>
              <span>{SimpleDateFormat(productData.expireDate, 'YYYY년 MM월 DD일 HH시 mm분')}</span>
            </div>
            <div className={s.itemRow}>
              <label> 결제번호 </label>
              <span> {productData.paymentId || '결제 정보를 찾을 수 없음'} </span>
            </div>
            {this.renderActualPaymentPrice()}
            <div className={s.itemRow}>
              <label> 메모 </label>
              <p>{productData.memo || '메모없음'}</p>
            </div>
          </div>
          <div className={s.actionWrap}>
            <RaisedButton
              label="수정"
              primary
              onClick={() => this.onClickEdit(productData)}
            />
            <RaisedButton
              label="삭제"
              secondary
              onClick={() => this.onDelete(productData.userProductId)}
            />
          </div>
        </div>
      </Paper>
    );
  }
}

ProductItem.propTypes = propTypes;
ProductItem.defaultProps = defaultProps;

export default withStyles(s)(ProductItem);
