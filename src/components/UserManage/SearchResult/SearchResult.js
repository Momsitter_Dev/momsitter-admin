import classnames from 'classnames';
import Paper from 'material-ui/Paper';
import FlatButton from 'material-ui/FlatButton';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './SearchResult.css';
import UserInfoCard from '../../UserInfoCard';

const propTypes = {
  searchResult: PropTypes.array,
  searchResultPageInfo: PropTypes.object,
  onClickCard: PropTypes.func,
  onClickSeeMore: PropTypes.func,
};
const defaultProps = {};

class SearchResult extends React.Component {
  onClickCard = (userId, userTypeId) => {
    this.props.onClickCard(userId, userTypeId);
  };

  onClickSeeMore = () => {
    this.props.onClickSeeMore();
  };

  render() {
    const { searchResultPageInfo, searchResult } = this.props;
    let searchResultUI = null;
    let seeMoreUI = null;

    if (searchResult && searchResult.length > 0) {
      searchResultUI = searchResult.map((item, index) =>
        <UserInfoCard
          key={`search-result-item-${index}`}
          userData={item}
          onClickCard={this.onClickCard}
        />,
      );
    } else {
      searchResultUI = (<div>검색 결과가 없습니다.</div>);
    }

    if (searchResultPageInfo) {
      seeMoreUI = (
        <div>
          <FlatButton
            label={`더보기 [${searchResultPageInfo.currentPage + 1} / ${searchResultPageInfo.maxPage}]`}
            fullWidth
            onClick={this.onClickSeeMore}
          />
        </div>
      );
    }

    return (
      <Paper zDepth={1}>
        <div className="box-header bg-color-primary">검색 결과</div>
        <div className={classnames('box-body', s.resultList)}>
          <div>{searchResultUI}</div>
          <div>{seeMoreUI}</div>
        </div>
      </Paper>
    );
  }
}

SearchResult.propTypes = propTypes;
SearchResult.defaultProps = defaultProps;

export default withStyles(s)(SearchResult);
