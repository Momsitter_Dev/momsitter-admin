import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ProductAddDialog.css';

const propTypes = {
  open: PropTypes.bool,
  onSave: PropTypes.func,
  onClose: PropTypes.func,
};
const defaultProps = {
  open: false,
  onSave: () => {},
  onClose: () => {},
};

class ProductAddDialog extends React.Component {
  constructor(props) {
    super(props);
    this.inputUserId = null;

  }

  onClickClose = () => {

  };

  onClickSave = () => {

  };

  render() {
    const { open } = this.props;
    return (
      <div className={s.root}>
        <Dialog open={open}>
          <DialogTitle id={'form-dialog-title'}> 이용/지원권 추가 </DialogTitle>
          <DialogContent>
            <DialogContentText> 이용/지원 횟수는 이용/지원권이 추가가 된 이후에 진행할 수 있습니다. </DialogContentText>
            { /* TODO: 회원 번호를 바로 검색할 수 있도록 */ }
            <Grid container alignItems={'center'}>
              <Grid item xs={3}>
                <Typography variant={'subheading'} align={'right'}> 회원번호 </Typography>
              </Grid>
              <Grid item xs={1} />
              <Grid item xs={8}>
                <TextField
                  fullWidth
                  id={'product-add-dialog-input-userId'}
                  inputRef={(r) => { this.inputUserId = r; }}
                />
              </Grid>
            </Grid>
            <Grid container alignItems={'center'}>
              <Grid item xs={3}>
                <Typography variant={'subheading'} align={'right'}> 이용/지원권 </Typography>
              </Grid>
              <Grid item xs={1} />
              <Grid item xs={8}>
                <TextField
                  fullWidth
                  id={'product-add-dialog-input-userId'}
                  inputRef={(r) => { this.input = r; }}
                />
              </Grid>
            </Grid>
            <Grid container alignItems={'center'}>
              <Grid item xs={3}>
                <Typography variant={'subheading'} align={'right'}> 구매일자 </Typography>
              </Grid>
              <Grid item xs={1} />
              <Grid item xs={8}>
                <TextField
                  fullWidth
                  id={'product-add-dialog-input-userId'}
                  inputRef={(r) => { this.input = r; }}
                />
              </Grid>
            </Grid>
            <Grid container alignItems={'center'}>
              <Grid item xs={3}>
                <Typography variant={'subheading'} align={'right'}> 만료일자 </Typography>
              </Grid>
              <Grid item xs={1} />
              <Grid item xs={8}>
                <TextField
                  fullWidth
                  id={'product-add-dialog-input-userId'}
                  inputRef={(r) => { this.input = r; }}
                />
              </Grid>
            </Grid>
            <Grid container alignItems={'center'}>
              <Grid item xs={3}>
                <Typography variant={'subheading'} align={'right'}> 메모 </Typography>
              </Grid>
              <Grid item xs={1} />
              <Grid item xs={8}>
                <TextField
                  fullWidth
                  id={'product-add-dialog-input-userId'}
                  inputRef={(r) => { this.input = r; }}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.onClickClose} color={'primary'}>
              취소
            </Button>
            <Button onClick={this.onClickSave} color={'primary'}>
              저장
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

ProductAddDialog.propTypes = propTypes;
ProductAddDialog.defaultProps = defaultProps;

export default withStyles(s)(ProductAddDialog);
