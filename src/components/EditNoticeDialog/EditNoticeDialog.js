import Paper from '@material-ui/icons/Paper';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Grow from '@material-ui/core/Grow';
import Slide from '@material-ui/core/Slide';
import draftToHtml from 'draftjs-to-html';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import htmlToDraft from 'html-to-draftjs';
import { Editor } from 'react-draft-wysiwyg';
import EditorStyle from '!!isomorphic-style-loader!css-loader?modules=false!react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './EditNoticeDialog.css';
import BoardAuthorSelect from '../../components/Filter/BoardAuthorSelect';
import Request from '../../util/Request';
import ResponseResultTypeGroup from '../../util/const/ResponseResultTypeGroup';

const propTypes = {
  open: PropTypes.bool,
  noticeId: PropTypes.number,
  onClose: PropTypes.func,
};
const defaultProps = {
  open: false,
  noticeId: null,
  onClose: () => {},
};

class EditNoticeDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      author: '',
      title: '',
      content: '',
      editorState: EditorState.createEmpty(),
      registerDate: '',
      updateDate: '',
      view: '',
    };
    this.onChangeAuthor = this.onChangeAuthor.bind(this);
    this.onChangeContent = this.onChangeContent.bind(this);
    this.onClickClose = this.onClickClose.bind(this);
    this.getBoardInfo = this.getBoardInfo.bind(this);
  }

  async componentDidMount() {
    try {
      const { noticeId } = this.props;
      if (!noticeId) {
        throw new Error('noticeId is null');
      }
      const data = await this.getBoardInfo(noticeId);
      if (data) {
        this.setState({
          author: data.author,
          title: data.title,
          content: htmlToDraft(data.content),
          registerDate: data.registerDate,
          updateDate: data.updateDate,
          view: data.view,
        });
      }
    } catch (err) {
      console.log(err);
    }
    this.inputTitle = null;
    this.inputContent = null;
    this.inputRegisterDate = null;
    this.onChangeEditState = this.onChangeEditState.bind(this);
  };

  async componentWillReceiveProps(props) {
    try {
      const { noticeId } = props;
      if (!noticeId) {
        throw new Error('noticeId is null');
      }
      const data = await this.getBoardInfo(noticeId);
      if (data) {
        this.setState({
          author: data.author,
          title: data.title,
          content: htmlToDraft(data.content),
          registerDate: data.registerDate,
          updateDate: data.updateDate,
          view: data.view,
        });
      }
    } catch (err) {
      console.log('willReceiveProps err');
      console.log(err);
    }
  };

  onChangeAuthor = (value) => {
    return this.setState({
      author: value,
    });
  };

  onChangeEditState = (editorState) => {
    return this.setState({
      editorState,
    });
  };

  onChangeContent = (contentState) => {
    this.inputContent = contentState;
    return true;
  };

  onClickClose = () => {
    return this.props.onClose();
  };

  getBoardInfo = async function (id) {
    try {
      const response = await Request(`api/boards/notice/${id}`, {
        method: 'GET',
      });

      if (response.result === ResponseResultTypeGroup.SUCCESS) {
        return response.data;
      }
    } catch (err) {
      console.log(err);
      return null;
    }
  };

  render() {
    const {
      author,
      title,
      content,
      registerDate,
      editorState,
    } = this.state;

    return (
      <div className={s.root}>
        <Paper>
          <div className={s.contentWrap}>
            <Grow in>
              <Grid container spacing={16}>
                <Grid container spacing={16} style={{ marginBottom: '10px' }}>
                  <Grid item xs={1}>
                    <Typography variant={'subheading'} style={{ textAlign: 'right' }}> 작성자 </Typography>
                  </Grid>
                  <Grid item xs={11}>
                    <BoardAuthorSelect value={author} onChange={this.onChangeAuthor} />
                  </Grid>
                </Grid>
                <Grid container spacing={16} style={{ marginBottom: '10px' }}>
                  <Grid item xs={1}>
                    <Typography variant={'subheading'} style={{ textAlign: 'right' }}> 작성일자 </Typography>
                  </Grid>
                  <Grid item xs={11}>
                    <TextField
                      fullWidth
                      type={'date'}
                      inputRef={(r) => { this.inputRegisterDate = r; }}
                    />
                  </Grid>
                </Grid>
                <Grid container spacing={16} style={{ marginBottom: '10px' }}>
                  <Grid item xs={1}>
                    <Typography variant={'subheading'} style={{ textAlign: 'right' }}> 제목 </Typography>
                  </Grid>
                  <Grid item xs={11}>
                    <TextField
                      defaultValue={title}
                      fullWidth
                      inputRef={(r) => { this.inputTitle = r; }}
                    />
                  </Grid>
                </Grid>
                <Grid container spacing={16} style={{ marginBottom: '10px' }}>
                  <Grid item xs={1}>
                    <Typography variant={'subheading'} style={{ textAlign: 'right' }}> 내용 </Typography>
                  </Grid>
                  <Grid item xs={11}>
                    <Editor
                      editorState={editorState}
                      onEditorStateChange={this.onChangeEditState}
                      editorClassName={s.editor}
                    />
                  </Grid>
                </Grid>
              </Grid>
            </Grow>
          </div>
          <div>
            <Button variant={'flat'} onClick={this.onClickClose}> 취소 </Button>
            <Button variant={'raised'} onClick={this.onClickSave}> 저장 </Button>
          </div>
        </Paper>
      </div>
    );
  }
}

EditNoticeDialog.propTypes = propTypes;
EditNoticeDialog.defaultProps = defaultProps;

export default withStyles(s, EditorStyle)(EditNoticeDialog);
