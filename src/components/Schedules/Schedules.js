import DayPicker from 'react-day-picker';
import moment from 'moment';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import dayPickerStyle from '!!isomorphic-style-loader!css-loader?modules=false!./dayPickerStyle.css';
import s from './Schedules.css';
import ScheduleCellContainer from '../Scheduler/ScheduleCellContainer';
import DateRow from '../Scheduler/DateRow';
import ParentScheduleTypeGroup from '../../util/const/ParentScheduleTypeGroup';

moment.locale('ko');

const propTypes = {
  schedules: PropTypes.arrayOf(
    PropTypes.shape({
      scheduleId: PropTypes.number,
      scheduleTypeId: PropTypes.number,
      userId: PropTypes.number,
      scheduleDate: PropTypes.string,
      scheduleStartTime: PropTypes.string,
      scheduleEndTime: PropTypes.string,
      scheduleLocation: PropTypes.string,
    }),
  ),
  scheduleType: PropTypes.oneOf([
    ParentScheduleTypeGroup.SCHEDULE_TYPE_REGULAR,
    ParentScheduleTypeGroup.SCHEDULE_TYPE_UNPLANNED,
    ParentScheduleTypeGroup.SCHEDULE_TYPE_SHORT_TERM,
  ]),
  duration: PropTypes.number,
  startTime: PropTypes.string,
  endTime: PropTypes.string,
  unitTime: PropTypes.number,
  unitHeight: PropTypes.number,
  unitWidth: PropTypes.number,
};
const defaultProps = {
  schedules: [],
  duration: 120,
  startTime: '00:00',
  endTime: '24:00',
  unitTime: 120,
  unitHeight: 30,
  unitWidth: 50,
  scheduleType: ParentScheduleTypeGroup.SCHEDULE_TYPE_REGULAR,
};

class Schedules extends React.Component {

  getTimeColumnRender = () => {
    const {
      startTime,
      endTime,
      unitTime,
      unitHeight,
    } = this.props;
    const start = moment(startTime, 'HH:mm');
    const end = moment(endTime, 'HH:mm');
    const render = [];
    let timeDiff = end.diff(start, 'minutes') + unitTime;
    let index = 0;
    while (timeDiff >= unitTime) {
      render.push(
        <div key={index} className={s.timeCell} style={{ height: unitHeight, lineHeight: `${unitHeight}px` }}>
          {start.clone().format('HH:00')}
        </div>,
      );
      start.add(unitTime, 'minutes');
      timeDiff -= unitTime;
      index += 1;
    }
    return render;
  };

  getGuideLines = () => {
    const { startTime, endTime, unitTime, unitHeight, unitWidth, duration } = this.props;
    const start = moment(startTime, 'HH:mm');
    const end = moment(endTime, 'HH:mm');
    const render = [];
    const timeDiff = end.diff(start, 'minutes') + unitTime;
    const guideLineCount = timeDiff / unitTime;
    const guideLineWidth = unitWidth * duration;
    for (let i = 0; i < guideLineCount; i += 1) {
      render.push(
        <div
          key={i}
          style={{ marginBottom: unitHeight - 1, width: guideLineWidth }}
          className={s.guideLine}
        />,
      );
    }
    return render;
  };

  renderRegularSchedule = () => {
    const { schedules } = this.props;
    const {
      duration,
      unitWidth,
    } = this.props;
    const timeColumnRender = this.getTimeColumnRender();
    const guideLineRender = this.getGuideLines();

    return (
      <div
        className={s.container}
        style={{
          display: 'block',
        }}
        ref={tableContainer => this.tableContainer = tableContainer}
      >
        <DateRow duration={duration} unitWidth={unitWidth} />
        <div className={s.timeColumn}>
          {timeColumnRender}
        </div>
        <div className={s.scheduleCellContainer}>
          {guideLineRender}
          <ScheduleCellContainer
            schedules={
              schedules.map((item) => {
                return {
                  ...item,
                  scheduleDate: moment(item.scheduleDate).toDate(),
                };
              })
            }
            duration={duration}
          />
        </div>
      </div>
    );
  };

  renderShortTermSchedule = () => {
    const { schedules } = this.props;
    return (
      <div>
        <div style={{ margin: '25px 0', borderBottom: '1px solid #eaeaea' }}>
          <DayPicker
            selectedDays={
              schedules.map((item) => {
                return moment(item.scheduleDate).toDate();
              })
            }
            modifiers={{
              disabled: {
                before: new Date(),
              },
            }}
            months={['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']}
            labels={{ nextMonth: '다음달', previousMonth: '이전달' }}
            weekdaysShort={['일', '월', '화', '수', '목', '금', '토']}
            enableOutsideDays
          />
        </div>
        <div className={s.shortTermTime}>
          <div className={s.timeItem}>
            <div className={s.timeLabel}> 시작시간 </div>
            <div> {schedules[0]['scheduleStartTime']} </div>
          </div>
          <div>
            <div className={s.rightGreenArrow} />
          </div>
          <div className={s.timeItem}>
            <div className={s.timeLabel}> 종료시간 </div>
            <div> {schedules[0]['scheduleEndTime']} </div>
          </div>
        </div>
      </div>
    );
  };

  renderUnplannedSchedule = () => {
    const { schedules } = this.props;
    const targetDate = schedules[0];

    return (
      <div>
        <div style={{ margin: '25px 0', borderBottom: '1px solid #eaeaea' }}>
          <DayPicker
            selectedDays={
              schedules.map((item) => {
                return moment(item.scheduleDate).toDate();
              })
            }
            modifiers={{
              disabled: {
                before: new Date(),
              },
            }}
            months={['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']}
            labels={{ nextMonth: '다음달', previousMonth: '이전달' }}
            weekdaysShort={['일', '월', '화', '수', '목', '금', '토']}
            enableOutsideDays
          />
        </div>
        <div style={{ marginBottom: '30px', textAlign: 'center' }}>
          <div className={s.unplannedFooter}> {targetDate.scheduleStartTime} 부터 {targetDate.scheduleEndTime} 사이에 </div>
          <div className={s.unplannedFooter}> 만나보고 싶어요. </div>
        </div>
      </div>
    );
  };

  renderContent = () => {
    const { schedules, scheduleType } = this.props;
    if (schedules.length === 0) {
      return this.renderEmpty();
    }
    if (scheduleType === ParentScheduleTypeGroup.SCHEDULE_TYPE_REGULAR) {
      return this.renderRegularSchedule();
    }
    if (scheduleType === ParentScheduleTypeGroup.SCHEDULE_TYPE_SHORT_TERM) {
      return this.renderShortTermSchedule();
    }
    if (scheduleType === ParentScheduleTypeGroup.SCHEDULE_TYPE_UNPLANNED) {
      return this.renderUnplannedSchedule();
    }
    return (
      <div> 표시할 수 없는 스케줄 타입입니다. </div>
    );
  };

  renderEmpty = () => {
    return (
      <div> 표시할 정보가 없습니다. </div>
    );
  };

  render() {
    return (
      <div className={s.root}>
        {this.renderContent()}
      </div>
    );
  }
}

Schedules.propTypes = propTypes;
Schedules.defaultProps = defaultProps;

export default withStyles(s, dayPickerStyle)(Schedules);
