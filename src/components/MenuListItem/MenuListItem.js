import { Group as GroupIcon } from '@material-ui/icons';
import { compose } from 'recompose';
import React from 'react';
import { withStyles as withMaterialStyles, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './MenuListItem.css';
import history from '../../history';

const propTypes = {
  icon: PropTypes.node,
  label: PropTypes.string.isRequired,
  link: PropTypes.string.isRequired,
};
const defaultProps = {
  icon: <GroupIcon />,
};

class MenuListItem extends React.Component {
  onClick = () => {
    const { link } = this.props;
    return history.push(link);
  };

  render() {
    const { icon, label } = this.props;
    return (
      <ListItem onClick={this.onClick} button>
        <ListItemIcon>
          {icon}
        </ListItemIcon>
        <ListItemText primary={label} />
      </ListItem>
    );
  }
}

MenuListItem.propTypes = propTypes;
MenuListItem.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(MenuListItem);
