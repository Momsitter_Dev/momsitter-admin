import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './FormParentChildAndPayment.css';
import StaticFileDomainGroup from '../../util/const/StaticFileDomainGroup';
import ChildAgeIdGroup from '../../util/const/ChildAgeIdGroup';
import WantedPaymentGroup from '../../util/const/WantedPaymentGroup';
import getFormattedMoney from '../../util/dataFormat/getFormattedMoney';
import isParentWantedPaymentValid from '../../util/validation/isParentWantedPaymentValid';

const DEFAULT_PAYMENT_ONE_CHILD = 8000; // 아이가 1명일 때 시급 기본값
const DEFAULT_PAYMENT_TWO_CHILD = 12000; // 아이가 2명일 때 시급 기본값

const CHILD_TYPE = {
  [ChildAgeIdGroup.NEW]: {
    url: `${StaticFileDomainGroup.AWS_S3}/momsitter-app/static/public/joinNew/p-membership-5-newbornbaby-#{type}.svg`,
    title: '신생아',
  },
  [ChildAgeIdGroup.YONG]: {
    url: `${StaticFileDomainGroup.AWS_S3}/momsitter-app/static/public/joinNew/p-membership-5-baby-#{type}.svg`,
    title: '영아',
  },
  [ChildAgeIdGroup.CHILD]: {
    url: `${StaticFileDomainGroup.AWS_S3}/momsitter-app/static/public/joinNew/p-membership-5-child-#{type}.svg`,
    title: '유아',
  },
  [ChildAgeIdGroup.ELEMENT]: {
    url: `${StaticFileDomainGroup.AWS_S3}/momsitter-app/static/public/joinNew/p-membership-5-schoolchild-#{type}.svg`,
    title: '초등학생',
  },
};

const propTypes = {
  wantedPayment: PropTypes.number,
  paymentNegotiable: PropTypes.bool,
  parentChildInfo: PropTypes.array,
  onChange: PropTypes.func,
};
const defaultProps = {
  onChange: () => {},
};

class FormParentChildAndPayment extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      numberOfChildren: null,
      // 아이 정보 리스트
      children: [],
      // 원하는 시급 정보
      wantedPayment: 0,
      // 시급 협의 가능 여부
      paymentNegotiable: false,
    };
  }

  async componentDidMount() {
    try {
      if (this.props.parentChildInfo) {
        this.setState({
          numberOfChildren: this.props.parentChildInfo.length,
          children: this.props.parentChildInfo,
          paymentNegotiable: this.props.paymentNegotiable,
          wantedPayment: this.props.wantedPayment,
        });
      }
    } catch (err) {
      console.log(err);
    }
  }

  onChange = () => {
    const { children, paymentNegotiable, wantedPayment } = this.state;

    return this.props.onChange({
      parentChildInfo: children,
      paymentNegotiable,
      wantedPayment,
    });
  };

  // Note. 아이 유형 아이콘 주소를 반환하는 함수
  getChildTypeIconUrl = (type, on = false) => {
    if (!type) return null;
    return CHILD_TYPE[type].url.replace('#{type}', on ? 's' : 'n');
  };

  /*
  handleClickSaveButton = async () => {
    if (this.canSaveForm()) {
      const {
        wantedPayment,
        paymentNegotiable,
        children,
      } = this.state;
      // Note. 업데이트 데이터
      const data = {
        wantedPayment,
        paymentNegotiable,
      };
      try {
        const fileId = this.state.profile.userProfileImageDetail[0].fileId;
        // Note.
        // 부모가 디폴트 이미지를 사용중이라면,
        // 디폴트 이미지는 아이 정보를 기반으로 정해지므로, 아이 정보 수정과 함께 디폴트 이미지를 수정한다.
        if (isDefaultParentProfileImage(fileId) || !fileId) {
          data.fileId = getDefaultParentProfileImage(children).fileId;
        }
      } catch (err) {
        console.error(err);
      }
      const result = await this.updateParentProfile({
        childInfo: children,
        data,
      });
      if (result) {
        // Note. 업데이트 완료 후, 신청서 알리기
        try {
          requestAreacast(getAccessToken());
        } catch (err) {
          // continue
        }
        return history.push('/my/profile');
      }
      return alert('오류가 발생하였습니다. 다시 시도해 주세요');
    }
    return false;
  };
  */

  // Note. 아이 정보가 모두 유효한지 확인하는 함수
  isChildrenValid = () => {
    const { numberOfChildren, children } = this.state;
    if (!numberOfChildren) return false;
    for (let i = 0; i < numberOfChildren; i += 1) {
      if (!children[i] || !children[i].childAgeId) return false;
    }
    return true;
  };

  canSaveForm = () =>
    this.state.numberOfChildren &&
    this.isChildrenValid() &&
    isParentWantedPaymentValid(this.state.wantedPayment, this.state.numberOfChildren);

  // Note. 매개변수로 인덱스를 넘겨, 해당 인덱스의 아이의 유형번호가 파라미터로 넘겨진 유형번호와 일치하는지 확인하는 함수
  isEqualNthChildType = (index, childAgeId) =>
    this.state.children[index] && this.state.children[index].childAgeId === childAgeId;

  // Note. 돌볼 자녀수가 변경되었을 때의 이벤트 핸들러
  handleChildrenButtonClick = numberOfChildren => {
    const { children } = this.state;
    let newChildren;
    // Note. 돌봄 자녀수 변경시, 자녀수에 따른 최소 희망시급으로 변경
    let wantedPayment = DEFAULT_PAYMENT_ONE_CHILD;
    if (numberOfChildren === 1) {
      newChildren = [children[0]];
    } else if (numberOfChildren === 2) {
      wantedPayment = DEFAULT_PAYMENT_TWO_CHILD;
      newChildren = children;
    }
    this.setState(
      {
        numberOfChildren,
        wantedPayment,
        children: newChildren,
      },
      () => this.onChange(),
    );
  };

  // Note. 아이 유형을 선택할 때의 이벤트 핸들러
  handleChildTypeClick = (index, type) => {
    const { children } = this.state;
    if (!children[index]) {
      children[index] = {};
    }
    children[index].childAgeId = type;
    this.setState(
      {
        children,
      },
      () => this.onChange(),
    );
  };

  handlePaymentChange = event => {
    const wantedPayment = event.target.value;
    this.setState(
      {
        wantedPayment: parseInt(wantedPayment.replace(',', ''), 10) || 0,
      },
      () => this.onChange(),
    );
  };

  renderNumberOfChildrenButton = () => {
    const { numberOfChildren } = this.state;
    // TODO: 아이 수 셀렉하는 부분 별도 콤포넌트로 분리
    return (
      <div className={s.wrapChildrenButton}>
        <FlatButton
          label="1명"
          className={numberOfChildren === 1 ? s.buttonChildrenOn : s.buttonChildrenOff}
          onClick={() => this.handleChildrenButtonClick(1)}
        />
        <FlatButton
          label="2명"
          className={numberOfChildren === 2 ? s.buttonChildrenOn : s.buttonChildrenOff}
          onClick={() => this.handleChildrenButtonClick(2)}
        />
      </div>
    );
  };

  renderChildrenTypeButton = () => {
    const { numberOfChildren } = this.state;
    const buttons = [];
    const renderOrderChildType = [
      ChildAgeIdGroup.NEW,
      ChildAgeIdGroup.YONG,
      ChildAgeIdGroup.CHILD,
      ChildAgeIdGroup.ELEMENT,
    ];
    if (numberOfChildren) {
      for (let i = 0; i < numberOfChildren; i += 1) {
        // TODO: 아이 연령 선택하는 레디오 버튼들 별도 콤포넌트로 분리
        buttons.push(
          <div key={`child-age-type-item-${i}`}>
            <h4 className={s.step2SubTitle}>
              {numberOfChildren === 1 && '아이 연령을 선택해주세요.'}
              {numberOfChildren !== 1 && i === 0 && '첫째 아이 연령을 선택해주세요.'}
              {numberOfChildren !== 1 && i === 1 && '둘째 아이 연령을 선택해주세요.'}
            </h4>
            <div className={s.wrapChildTypeButton}>
              {renderOrderChildType.map((item, index) => (
                <FlatButton
                  // eslint-disable-next-line react/no-array-index-key
                  key={`child-item-${index}`}
                  label={
                    <div style={{ textAlign: 'center' }}>
                      <img
                        src={this.getChildTypeIconUrl(item, this.isEqualNthChildType(i, item))}
                        alt={CHILD_TYPE[item].title}
                        className={s.childTypeIcon}
                      />
                      <div className={this.isEqualNthChildType(i, item) ? s.childTypeLabelOn : s.childTypeLabelOff}>
                        {CHILD_TYPE[item].title}
                      </div>
                    </div>
                  }
                  onClick={() => this.handleChildTypeClick(i, item)}
                  className={this.isEqualNthChildType(i, item) ? s.buttonChildTypeOn : s.buttonChildTypeOff}
                />
              ))}
            </div>
          </div>,
        );
      }
    }
    return (
      <div className={numberOfChildren ? s.showContents : s.hideContents}>
        <div className={s.wrapChildrenTypeButton}>{buttons}</div>
      </div>
    );
  };

  renderPaymentForm = () => {
    const { wantedPayment, paymentNegotiable } = this.state;
    return (
      <div>
        <h4 className={s.step2SubTitle} style={{ textAlign: 'left' }}>
          맘시터에게 지급할 희망시급을 적어주세요
        </h4>
        {/* TODO: 시급입력 폼 별도 콤포넌트로 배치할 것 */}
        <div className={s.wrapPaymentInput}>
          <input
            value={getFormattedMoney(wantedPayment)}
            onChange={this.handlePaymentChange}
            style={{
              color: isParentWantedPaymentValid(wantedPayment) ? '#2b2b2b' : '#ff4500',
            }}
            className={s.paymentInput}
          />
          <span className={s.paymentPostFix}>원</span>
        </div>
        <div
          role="button"
          className={s.buttonNegotiable}
          style={{ marginTop: 15, marginBottom: 14 }}
          onClick={() => this.setState({ paymentNegotiable: !paymentNegotiable }, () => this.onChange())}
        >
          <span
            className={paymentNegotiable ? s.negotiableCheckIconOn : s.negotiableCheckIcon}
            style={{ marginRight: 8 }}
          />
          <span className={s.negotiableLabel}> 시급협의 가능 </span>
        </div>
      </div>
    );
  };

  // Note. 시급이 적절하지 않을 경우, 안내 문구를 렌더링하는 함수
  renderPaymentWarning = () => {
    const { wantedPayment, numberOfChildren } = this.state;
    if (isParentWantedPaymentValid(wantedPayment, numberOfChildren)) return null;
    if (numberOfChildren === 1) {
      return (
        <p className={s.paymentWarning}>
          최저시급(
          {getFormattedMoney(WantedPaymentGroup.MINIMUM)}
          원) 이상의 시급을 입력해주세요.
        </p>
      );
    } else if (numberOfChildren === 2) {
      return (
        <p className={s.paymentWarning}>
          아이 2명을 맡기는 경우,
          <br />
          최저시급 {getFormattedMoney(WantedPaymentGroup.MINIMUM)}
          원x
          {WantedPaymentGroup.TWO_CHILD_FACTOR}배 =&nbsp;
          {getFormattedMoney(WantedPaymentGroup.MINIMUM * WantedPaymentGroup.TWO_CHILD_FACTOR)}원 이상 필수
        </p>
      );
    }
    return null;
  };

  render() {
    return (
      <div className={s.root}>
        <div className={s.stepStyle}>
          {this.renderNumberOfChildrenButton()}
          <div>{this.renderChildrenTypeButton()}</div>
          <div className={s.warpPayment}>
            {this.renderPaymentForm()}
            {this.renderPaymentWarning()}
          </div>
        </div>
      </div>
    );
  }
}

FormParentChildAndPayment.propTypes = propTypes;
FormParentChildAndPayment.defaultProps = defaultProps;

export default withStyles(s)(FormParentChildAndPayment);
