/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@mfort.co.kr
 */

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Step.css';

class Step extends React.Component {
  render() {
    const { children, stepStyle, stepContainerStyle } = this.props;

    return (
      <div className={s.root} style={stepStyle}>
        <div className={s.container} style={stepContainerStyle}>
          {children}
        </div>
      </div>
    );
  }
}

Step.propTypes = {
  stepStyle: PropTypes.object,
  stepContainerStyle: PropTypes.object,
};

Step.defaultProps = {
  stepStyle: {},
  stepContainerStyle: {},
};

export default withStyles(s)(Step);
