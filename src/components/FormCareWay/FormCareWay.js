/* eslint-disable no-undef */
/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@mfort.co.kr
 */

import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './FormCareWay.css';
import CareWayGroup from '../../util/const/CareWayGroup';

const propTypes = {
  style: stylePropType,
  defaultCareWay: PropTypes.oneOf([
    CareWayGroup.ALONE,
    CareWayGroup.WITH_HELPER,
    CareWayGroup.WITH_ELDER,
    CareWayGroup.WITH_PARENT,
  ]),
  onChange: PropTypes.func,
};
const defaultProps = {
  style: {},
  defaultCareWay: CareWayGroup.ALONE,
  onChange: () => {},
};

class FormCareWay extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      careWay: props.defaultCareWay,
    };
  }

  handleCareWayChange = (event, value = CareWayGroup.ALONE) => {
    this.setState(
      {
        careWay: value,
      },
      () => this.props.onChange(value),
    );
  };

  render() {
    const { style } = this.props;
    const { careWay } = this.state;
    return (
      <div className={s.root}>
        <div className={s.container} style={style}>
          <RadioButtonGroup name="careWay" onChange={this.handleCareWayChange} defaultSelected={careWay}>
            <RadioButton className={s.radio} value={CareWayGroup.ALONE} label="부모 대신 혼자 돌봐주세요." />
            <RadioButton className={s.radio} value={CareWayGroup.WITH_PARENT} label="엄마 아빠와 함께 돌봐주세요." />
            <RadioButton
              className={s.radio}
              value={CareWayGroup.WITH_ELDER}
              label="할머니(할아버지)와 함께 돌봐주세요."
            />
            <RadioButton
              className={s.radio}
              value={CareWayGroup.WITH_HELPER}
              label="이모님(도우미)과 함께 돌봐주세요."
            />
          </RadioButtonGroup>
        </div>
      </div>
    );
  }
}

FormCareWay.propTypes = propTypes;
FormCareWay.defaultProps = defaultProps;

export default withStyles(s)(FormCareWay);
