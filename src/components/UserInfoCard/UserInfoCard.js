import uuid from 'uuid/v4';
import moment from 'moment';
import Chip from 'material-ui/Chip';
import Paper from 'material-ui/Paper';
import Avatar from 'material-ui/Avatar';
import EnhancedButton from 'material-ui/internal/EnhancedButton';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './UserInfoCard.css';
import isParent from '../../util/user/isParent';
import isSitter from '../../util/user/isSitter';
import ProfileImageTypeGroup from '../../util/const/ProfileImageTypeGroup';

const propTypes = {
  userData: PropTypes.object,
  onClickCard: PropTypes.func,
};
const defaultProps = {
  userData: {},
  onClickCard: () => {},
};

class UserInfoCard extends React.Component {
  onClickCard = () => {
    // TODO 권한을 선택해 주세요.
    const { userData, onClickCard } = this.props;
    if (userData.userRole.length >= 2) {
      console.log('Chip을 통해서만 정보를 볼 수 있음');
    } else {
      onClickCard(userData.userId, userData.userRole[0].userTypeId);
    }
  };

  onClickUserRole = (event, userTypeId) => {
    event.stopPropagation();
    return this.props.onClickCard(this.props.userData.userId, userTypeId);
  };

  renderProfileImage = () => {
    const defaultUI = (<span style={{ height: '70px' }}> 이미지 없음 </span>);
    try {
      const userProfileImage = this.props.userData.userProfileImage;
      if (!userProfileImage) {
        return defaultUI;
      }
      if (userProfileImage.length <= 0) {
        return defaultUI;
      }
      let url = null;
      for (let i = 0; i < userProfileImage.length; i += 1) {
        if (userProfileImage[i].profileImageType === ProfileImageTypeGroup.SITTER_PROFILE_MAIN || userProfileImage[i].profileImageType === ProfileImageTypeGroup.PARENT_PROFILE_MAIN) {
          url = userProfileImage[i].profileFile.fileUrl;
          break;
        }
      }
      return (<Avatar src={url} size={70} />);
    } catch (err) {
      return defaultUI;
    }
  };

  renderUserRole = () => {
    const defaultUI = (<span>ERROR</span>);
    try {
      const userRole = this.props.userData.userRole;
      if (!userRole) {
        return defaultUI;
      }
      if (userRole.length <= 0) {
        return defaultUI;
      }
      return userRole.map((item) => {
        if (isParent(item.userTypeId)) {
          return (
            <Chip
              key={uuid()}
              className={s.userRole}
              onClick={(e) => { this.onClickUserRole(e, item.userTypeId);}}
              backgroundColor="#2196f3"
              labelColor="#fff"
              style={{ boxShadow: '1.5px 1.5px #d3d3d3' }}
            >
              {item.userType.userTypeDesc}
            </Chip>
          );
        }
        if (isSitter(item.userTypeId)) {
          return (
            <Chip
              key={uuid()}
              className={s.userRole}
              onClick={(e) => { this.onClickUserRole(e, item.userTypeId);}}
              backgroundColor="#fff"
              labelColor="#2196f3"
              style={{ boxShadow: '1.5px 1.5px #d3d3d3' }}
            >
              <Avatar
                size={27}
                backgroundColor="#2196f3"
                color="#fff"
                style={{ fontSize: '12px', verticalAlign: 'middle', }}
              >
                시터
              </Avatar>
              {item.userType.userDetailTypeDesc}
            </Chip>
          );
        }
        return null;
      });
    } catch (err) {
      return defaultUI;
    }
  };

  render() {
    const style = {
      marginBottom: '15px',
    };
    const buttonStyle = {
      display: 'block',
      width: '100%',
    };
    const {
      userId,
      userName,
      userPhone,
      userBirthday,
    } = this.props.userData;

    return (
      <Paper style={style} zDepth={2}>
        <EnhancedButton onClick={this.onClickCard} style={buttonStyle}>
          <div className={s.wrap}>
            <div className={s.row} style={{ justifyContent: 'flex-start' }}>
              {this.renderUserRole()}
            </div>
            <div className={s.row} style={{ justifyContent: 'center' }}>
              {this.renderProfileImage()}
            </div>
            <div className={s.row}>
              <span className={s.label}> no.{userId} </span>
              <span className={s.value}> {userName} </span>
            </div>
            <div className={s.row}>
              <span className={s.label}>핸드폰: </span>
              <span className={s.value}> {userPhone} </span>
            </div>
            <div className={s.row}>
              <span className={s.label}>생년월일: </span>
              <span className={s.value}> {moment(userBirthday, 'YYYYMMDD').format('YYYY-MM-DD')} </span>
            </div>
          </div>
        </EnhancedButton>
      </Paper>
    );
  }
}

UserInfoCard.propTypes = propTypes;
UserInfoCard.defaultProps = defaultProps;

export default withStyles(s)(UserInfoCard);
