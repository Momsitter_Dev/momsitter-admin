import styleProps from 'react-style-proptype';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './SearchLocationList.css';
import SearchLocationItem from '../SearchLocationItem';

const propTypes = {
  list: PropTypes.array.isRequired,
  dataKey: PropTypes.string.isRequired,
  onClickItem: PropTypes.func,
  selectedItem: PropTypes.string,
  style: styleProps,
  height: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]),
  total: PropTypes.object,
  totalShow: PropTypes.bool,
};
const defaultProps = {
  onClickItem: () => {},
  selectedItem: null,
  style: {},
  height: null,
  total: null,
  totalShow: true,
};

class SearchLocationList extends React.Component {
  onClickItem = (data) => {
    const { dataKey } = this.props;
    return this.props.onClickItem(dataKey, data);
  };

  getTotalItem = () => {
    const { dataKey, total, selectedItem } = this.props;
    return (
      <SearchLocationItem
        key={`search-location-item-${dataKey}-total`}
        data={{
          main: total.main,
          sub: (dataKey === 'sub') ? '전체' : total.sub,
          detail: (dataKey === 'detail') ? '전체' : total.detail,
          locationLatitude: total.locationLatitude,
          locationLongitude: total.locationLongitude,
        }}
        dataKey={dataKey}
        onClick={this.onClickItem}
        selectedItem={selectedItem}
      />
    );
  };

  renderItems = () => {
    const { totalShow, dataKey, list, selectedItem } = this.props;
    let items = [];
    if (!list) {
      return null;
    }

    if (list.length === 0 && (dataKey === 'sub' || dataKey === 'detail')) {
      return null;
    }

    if (totalShow && (dataKey === 'sub' || dataKey === 'detail')) {
      items.push(this.getTotalItem());
    }

    items = [
      ...items,
      list.map((item, index) => {
        return (
          <SearchLocationItem
            key={`search-location-item-${dataKey}-${index}`}
            data={item}
            dataKey={dataKey}
            onClick={this.onClickItem}
            selectedItem={selectedItem}
          />
        );
      }),
    ];
    return items;
  };

  render() {
    const { style, height } = this.props;
    if (height) {
      style.height = height;
    }
    return (
      <div
        className={s.root}
        style={{
          ...style,
        }}
      >
        {this.renderItems()}
      </div>
    );
  }
}

SearchLocationList.propTypes = propTypes;
SearchLocationList.defaultProps = defaultProps;

export default withStyles(s)(SearchLocationList);
