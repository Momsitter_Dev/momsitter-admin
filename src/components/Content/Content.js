import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Content.css';
import BrushIcon from 'material-ui/svg-icons/image/brush';

class Content extends React.Component {
  render() {
    return (
      <div className="app-content-wrapper">
        <div className="app-content">
          <div className="full-height">
            <section className="container-fluid" style={{ padding: '15px' }}>
              {this.props.children}
            </section>
          </div>
        </div>
        <section className="app-footer">
          <div className="container-fluid">
            <span className="float-left">
              <span>
                {/* Copyright © <a className="brand" target="_blank" href="#"> Momsitter </a> 2017 */}
                Copyright © Momsitter 2017
              </span>
            </span>
            <span className="float-right">
              <span>
                Momsitter Developers{' '}
                <BrushIcon style={{ width: '12px', height: '12px' }} />
              </span>
            </span>
          </div>
        </section>
      </div>
    );
  }
}

Content.propTypes = {
  children: PropTypes.node.isRequired,
};

Content.defaultProps = {};

export default withStyles(s)(Content);
