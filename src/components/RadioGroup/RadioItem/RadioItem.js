/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import IconRadio from 'material-ui/svg-icons/toggle/radio-button-checked';
import IconRadioBlank from 'material-ui/svg-icons/toggle/radio-button-unchecked';

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './RadioItem.css';

class RadioItem extends React.Component {
  constructor(props){
    super(props);
  }

  getStyles() {
    const {checked, style, activeStyle, inputStyle, containerStyle, iconWrapStyle, activeIconWrapStyle,
      labelWrapStyle, activeLabelWrapStyle, mainLabelStyle, activeMainLabelStyle,
      subLabelStyle, activeSubLabelStyle, descriptionStyle, activeDescriptionStyle} = this.props;
    return {
      wrapStyle: checked ? Object.assign(style, activeStyle) : style,
      inputStyle, containerStyle,
      iconWrapStyle: checked ? Object.assign(iconWrapStyle, activeIconWrapStyle) : iconWrapStyle,
      labelWrapStyle: checked ? Object.assign(labelWrapStyle, activeLabelWrapStyle) : labelWrapStyle,
      mainLabelStyle: checked ? Object.assign(mainLabelStyle, activeMainLabelStyle) : mainLabelStyle,
      subLabelStyle: checked ? Object.assign(subLabelStyle, activeSubLabelStyle) : subLabelStyle,
      descriptionStyle: checked ? Object.assign(descriptionStyle, activeDescriptionStyle) : descriptionStyle,
    }
  }

  getIcon() {
    const {checked, checkedIcon, unCheckedIcon} = this.props;
    return checked ? checkedIcon : unCheckedIcon;
  }

  handleToggle = (event) => {
    this.props.onToggle(event, {
      index: this.props.index,
      checked: this.props.checked ? true : false,
      value: this.props.value,
    });
  };

  render() {

    const {checked, value, name, mainLabel, subLabel, description} = this.props;

    const styles = this.getStyles();

    return (
      <div className={s.wrap} onClick={this.handleToggle} style={styles.wrapStyle} >

        <input type="radio" name={name} className={s.input} checked={checked} value={value} style={styles.inputStyle} readOnly />

        <div className={s.container} style={styles.containerStyle}>
          <div className={s.iconWrap} style={styles.iconWrapStyle}>
            { this.getIcon() }
          </div>

          <div className={s.contentWrap}>

            <div className={s.labelWrap} style={styles.labelWrapStyle}>
              <div className={s.mainLabel} style={styles.mainLabelStyle}>
                {mainLabel}
              </div>
              <div className={s.subLabel} style={styles.subLabelStyle}>
                {subLabel}
              </div>
            </div>

            <div className={s.description} style={styles.descriptionStyle}>
              {description}
            </div>

          </div>

        </div>
      </div>
    );
  }
}

RadioItem.propTyeps = {
  name: PropTypes.string.isRequired,
  data: PropTypes.object,
  index: PropTypes.number,
  onToggle: PropTypes.func,
  checked: PropTypes.bool,
  unCheckedIcon: PropTypes.element,
  checkedIcon: PropTypes.element,
  style: PropTypes.object,
  iconWrapStyle: PropTypes.object,
  labelWrapStyle: PropTypes.object,
  mainLabelStyle: PropTypes.object,
  subLabelStyle: PropTypes.object,
  descriptionStyle: PropTypes.object,
  inputStyle: PropTypes.object,
  containerStyle: PropTypes.object,
  activeIconWrapStyle: PropTypes.object,
  activeStyle: PropTypes.object,
  activeLabelWrapStyle: PropTypes.object,
  activeMainLabelStyle: PropTypes.object,
  activeSubLabelStyle: PropTypes.object,
  activeDescriptionStyle: PropTypes.object,
};

RadioItem.defaultProps = {
  unCheckedIcon: <IconRadio/>,
  checkedIcon: <IconRadioBlank/>,
  style: {},
  iconWrapStyle: {},
  labelWrapStyle: {},
  mainLabelStyle: {},
  subLabelStyle: {},
  descriptionStyle: {},
  inputStyle: {},
  containerStyle: {},
  activeIconWrapStyle: {},
  activeStyle: {},
  activeLabelWrapStyle: {},
  activeMainLabelStyle: {},
  activeSubLabelStyle: {},
  activeDescriptionStyle: {},
};

export default withStyles(s)(RadioItem);
