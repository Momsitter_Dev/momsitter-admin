/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './RadioGroup.css';

import IconCheckbox from 'material-ui/svg-icons/toggle/check-box';
import IconCheckboxBlank from 'material-ui/svg-icons/toggle/check-box-outline-blank';

class RadioGroup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: props.defaultSelected,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps) {
      this.setState({ selected: nextProps.defaultSelected });
    }
  }

  handleChildToggle = (event, value) => {
    this.setState({selected: value.value});
    this.props.onChange(null, value);
  };

  render() {
    const { selected } = this.state;
    const { style, children } = this.props;
    return (
      <div style={style}>
        {
          React.Children.map(children, child => {
            return React.cloneElement(child, {onToggle: this.handleChildToggle, checked: child.props.value === selected, name: this.props.name })
          })
        }
      </div>
    );
  }
}

RadioGroup.propTypes = {
  name: PropTypes.string.isRequired,
  defaultSelected: PropTypes.string, // TODO: defaultSelected 값 number 도 지원할 수 있도록 수정
  onChange: PropTypes.func,
  style: PropTypes.object,
};

RadioGroup.defaultProps = {
  checkIcon: <IconCheckbox/>,
  uncheckedIcon: <IconCheckboxBlank/>,
  onChange: () => {},
  style: {},
};

export default withStyles(s)(RadioGroup);
