import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Grow from '@material-ui/core/Grow';
import Slide from '@material-ui/core/Slide';
import draftToHtml from 'draftjs-to-html';
import { Editor } from 'react-draft-wysiwyg';
import EditorStyle from '!!isomorphic-style-loader!css-loader?modules=false!react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './AddNoticeDialog.css';
import BoardAuthorSelect from '../../components/Filter/BoardAuthorSelect';

const propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func,
  onSave: PropTypes.func,
};
const defaultProps = {
  open: false,
  onClose: () => {},
  onSave: () => {},
};

const transition = (props) => {
  return (
    <Slide direction={'up'} {...props} />
  );
};

class AddNoticeDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      author: '',
    };
    this.inputTitle = null;
    this.inputContent = null;
  }

  onChangeContent = (contentState) => {
    this.inputContent = contentState;
    return true;
  };

  onClickClose = () => {
    return this.props.onClose();
  };

  onClickSave = () => {
    if (this.checkValidation()) {
      return this.props.onSave({
        title: this.inputTitle.value,
        author: this.state.author,
        content: draftToHtml(this.inputContent),
      });
    }
  };

  onChangeAuthor = (value) => {
    return this.setState({
      author: value,
    });
  };

  checkValidation = () => {
    try {
      const { author } = this.state;
      const title = this.inputTitle.value;
      const content = draftToHtml(this.inputContent);

      if (!author) {
        throw new Error('author required');
      }

      if (!title) {
        throw new Error('title required');
      }

      if (!content) {
        throw new Error('content is required');
      }

      return true;
    } catch (err) {
      console.log(err);
      return false;
    }
  };

  render() {
    const { author } = this.state;
    const { open } = this.props;
    return (
      <div className={s.root}>
        <Dialog
          open={open}
          fullScreen
          onClose={this.onClickClose}
          TransitionComponent={transition}
        >
          <DialogTitle> 공지사항 - 게시글 추가</DialogTitle>
          <DialogContent>
            <DialogContentText> 공지사항 게시판에 나타날 게시글을 작성합니다. </DialogContentText>
            <div className={s.contentWrap}>
              <Grow in>
                <Grid container spacing={16}>
                  <Grid container spacing={16} style={{ marginBottom: '10px' }}>
                    <Grid item xs={1}>
                      <Typography variant={'subheading'} style={{ textAlign: 'right' }}> 작성자 </Typography>
                    </Grid>
                    <Grid item xs={11}>
                      <BoardAuthorSelect value={author} onChange={this.onChangeAuthor} />
                    </Grid>
                  </Grid>
                  <Grid container spacing={16} style={{ marginBottom: '10px' }}>
                    <Grid item xs={1}>
                      <Typography variant={'subheading'} style={{ textAlign: 'right' }}> 제목 </Typography>
                    </Grid>
                    <Grid item xs={11}>
                      <TextField fullWidth inputRef={(r) => { this.inputTitle = r; }} />
                    </Grid>
                  </Grid>
                  <Grid container spacing={16} style={{ marginBottom: '10px' }}>
                    <Grid item xs={1}>
                      <Typography variant={'subheading'} style={{ textAlign: 'right' }}> 내용 </Typography>
                    </Grid>
                    <Grid item xs={11}>
                      <Editor
                        onContentStateChange={this.onChangeContent}
                        editorClassName={s.editor}
                        wrapperClassName="demo-wrapper"
                      />
                    </Grid>
                  </Grid>
                </Grid>
              </Grow>
            </div>
          </DialogContent>
          <DialogActions>
            <Button variant={'flat'} onClick={this.onClickClose}> 취소 </Button>
            <Button variant={'raised'} onClick={this.onClickSave}> 저장 </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

AddNoticeDialog.propTypes = propTypes;
AddNoticeDialog.defaultProps = defaultProps;

export default withStyles(s, EditorStyle)(AddNoticeDialog);
