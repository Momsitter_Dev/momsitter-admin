/* eslint-disable no-undef */
/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@mfort.co.kr
 */

import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './StepLocationPicker.css';
import Step from '../Step';
import SearchLocationMultiplePicker from '../SearchLocationMultiplePicker';

const propTypes = {
  stepStyle: stylePropType,
  title: PropTypes.node,
  showTitle: PropTypes.bool,
  selectedLocations: PropTypes.arrayOf(
    PropTypes.shape({
      si: PropTypes.string,
      sgg: PropTypes.string,
      emd: PropTypes.string,
      locationLatitude: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
      locationLongitude: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    }),
  ),
  onChange: PropTypes.func,
};

const defaultProps = {
  stepStyle: {},
  title: (
    <h2 className={s.title}>
      <span style={{ color: '#ff4500' }}>활동 가능한 지역</span>을 선택해주세요.
    </h2>
  ),
  showTitle: true,
  selectedLocations: [],
  onChange: () => {},
};

class StepLocationPicker extends React.Component {
  handleSelectedLocationChange = value => {
    if (value && value.length > 0) {
      return this.props.onChange(
        null,
        value.map(item => {
          return {
            ...item,
            jibunAddr: item.address,
            roadAddr: item.address,
            locationLatitude: item.locationLatitude,
            locationLongitude: item.locationLongitude,
            siNm: item.main,
            sggNm: item.sub,
            emdNm: item.detail === '전체' ? '' : item.detail,
          };
        }),
      );
    }
    return this.props.onChange(null, []);
  };

  render() {
    const { stepStyle, title, showTitle, selectedLocations } = this.props;

    return (
      <Step
        stepStyle={stepStyle}
        stepContainerStyle={{
          padding: 0,
        }}
      >
        <div className={s.root}>
          <div className={s.container}>
            {showTitle && (
              <div className={s.wrapTitle}>
                {title}
                <p className={s.sub}>최대 3개까지 선택 가능합니다.</p>
              </div>
            )}
            <SearchLocationMultiplePicker
              header
              selectedLocations={selectedLocations.map(item => {
                return {
                  ...item,
                  main: item.siNm,
                  sub: item.sggNm,
                  detail: item.emdNm === '' ? '' : item.emdNm,
                  locationLatitude: item.locationLatitude,
                  locationLongitude: item.locationLongitude,
                  address: item.jibunAddr,
                };
              })}
              onChange={this.handleSelectedLocationChange}
            />
          </div>
        </div>
      </Step>
    );
  }
}

StepLocationPicker.propTypes = propTypes;
StepLocationPicker.defaultProps = defaultProps;

export default withStyles(s)(StepLocationPicker);
