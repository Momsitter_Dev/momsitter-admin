/* eslint-disable no-undef,react/no-did-mount-set-state,max-len */
/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import uuid from 'uuid/v4';
import React from 'react';
import PropTypes from 'prop-types';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './FormActivity.css';
import ActivityTypeIdGroup from '../../util/const/ActivityTypeIdGroup';
import ActivityItem from '../ActivityItem';

const propTypes = {
  initialSelectedIndex: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.number, PropTypes.string])),
  max: PropTypes.number,
  onChange: PropTypes.func,
};
const defaultProps = {
  initialSelectedIndex: null,
  max: null,
  onChange: () => {},
};

const ACTIVITIES = [
  {
    id: ActivityTypeIdGroup.IN_DOOR_ACTIVITY,
    label: '실내놀이',
    iconY: 'join-indoorplay-s.svg',
    iconN: 'join-indoorplay-n.svg',
  },
  {
    id: ActivityTypeIdGroup.KOREAN_STUDY,
    label: '한글놀이',
    iconY: 'p-membership-2-koreanicon-s-2.svg',
    iconN: 'p-membership-2-koreanicon-n-2.svg',
  },
  {
    id: ActivityTypeIdGroup.SIMPLE_CLEANING,
    label: '간단 청소',
    iconY: 'join-cleaning-s.svg',
    iconN: 'join-cleaning-n.svg',
  },
  {
    id: ActivityTypeIdGroup.LONG_RESIDENT_SITTER,
    label: '장기입주',
    iconY: 'join-longterm-s.svg',
    iconN: 'join-longterm-n.svg',
  },
  {
    id: ActivityTypeIdGroup.SCHOOL_COMPANION,
    label: '등하원돕기',
    iconY: 'join-walk-s@2x.png',
    iconN: 'join-walk-n@2x.png',
  },
  {
    id: ActivityTypeIdGroup.ENGLISH_PLAY,
    label: '영어놀이',
    iconY: 'join-english-s.svg',
    iconN: 'join-english-n.svg',
  },
  {
    id: ActivityTypeIdGroup.SIMPLE_COOKING,
    label: '밥 챙겨주기',
    iconY: 'join-cook-s.svg',
    iconN: 'join-cook-n.svg',
  },
  {
    id: ActivityTypeIdGroup.SHORT_RESIDENT_SITTER,
    label: '단기입주',
    iconY: 'join-shortperide-s.svg',
    iconN: 'join-shortperide-n.svg',
  },
  {
    id: ActivityTypeIdGroup.STORY_TELLING,
    label: '책읽기',
    iconY: 'join-read-s.svg',
    iconN: 'join-read-n.svg',
  },
  {
    id: ActivityTypeIdGroup.STUDY_GUIDE,
    label: '학습지도',
    iconY: 'join-specially-s.svg',
    iconN: 'join-specially-n.svg',
  },
  {
    id: ActivityTypeIdGroup.SIMPLE_DISH_WASHING,
    label: '간단 설거지',
    iconY: 'join-washing-s.svg',
    iconN: 'join-washing-n.svg',
  },
  {
    id: ActivityTypeIdGroup.OUT_DOOR_ACTIVITY,
    label: '야외활동',
    iconY: 'join-outdooractivities-s.svg',
    iconN: 'join-outdooractivities-n.svg',
  },
  {
    id: ActivityTypeIdGroup.ATHLETIC_PLAY,
    label: '체육놀이',
    iconY: 'join-pe-s.svg',
    iconN: 'join-pe-n.svg',
  },
];

const ROW_PER_ITEM = [4, 4, 3, 2];

class FormActivity extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      activities: {},
    };
  }

  componentDidMount() {
    if (this.props.initialSelectedIndex) {
      const activities = {};
      this.props.initialSelectedIndex.forEach(item => {
        activities[item] = true;
      });
      this.setState({ activities });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.initialSelectedIndex) {
      const activities = {};
      nextProps.initialSelectedIndex.forEach(item => {
        activities[item] = true;
      });
      this.setState({ activities });
    }
  }

  onChange = () => {
    const activityIds = this.getSelectedActivityIds();
    let isValid = true;
    if (!activityIds) {
      isValid = false;
    }
    if (activityIds.length <= 0) {
      isValid = false;
    }
    return this.props.onChange({
      activityIds,
      isValid,
    });
  };

  onClickItem = id => {
    const activities = this.state.activities;
    if (activities[id]) {
      activities[id] = false;
    } else {
      if (this.props.max) {
        const activityIds = this.getSelectedActivityIds();
        if (activityIds.length >= this.props.max) {
          return alert(`최대 ${this.props.max}개만 선택가능합니다.`);
        }
      }
      activities[id] = true;
    }
    return this.setState(
      {
        activities: Object.assign({}, activities),
      },
      () => this.onChange(),
    );
  };

  getSelectedActivityIds = () => Object.keys(this.state.activities).filter(item => this.state.activities[item]);

  renderItems = () => {
    const point = {
      start: 0,
      end: 0,
    };
    return ROW_PER_ITEM.map((item, index) => {
      if (index === 0) {
        point.start = index;
        point.end = item - 1;
      } else {
        point.start = point.end + 1;
        point.end += item;
      }
      return <div key={uuid()}>{this.renderItemByRow(point.start, point.end)}</div>;
    });
  };

  renderItemByRow = (start, end) =>
    ACTIVITIES.filter((item, index) => index >= start && index <= end).map(item => {
      if (this.state.activities[item.id]) {
        return <ActivityItem {...item} onClickItem={this.onClickItem} active key={uuid()} />;
      }
      return <ActivityItem {...item} onClickItem={this.onClickItem} key={uuid()} />;
    });

  render() {
    return (
      <div className={s.root}>
        <div className={s.warpActivity}>{this.renderItems()}</div>
        <Dialog
          modal
          open={this.state.dialogOpen}
          actions={<FlatButton label="확인" primary onClick={() => this.setState({ dialogOpen: false })} />}
          onRequestClose={() => this.setState({ dialogOpen: false })}
        >
          <p className={s.dialogMessage}>{this.state.dialogMessage}</p>
        </Dialog>
      </div>
    );
  }
}

FormActivity.propTypes = propTypes;
FormActivity.defaultProps = defaultProps;

export default withStyles(s)(FormActivity);
