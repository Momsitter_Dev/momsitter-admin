import { compose } from 'recompose';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles/index';
import Button from '@material-ui/core/Button';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ProfileSitterSchedule.css';
import Schedules from '../Schedules';

const propTypes = {
  title: PropTypes.string.isRequired,
  schedules: PropTypes.arrayOf(
    PropTypes.shape({
      scheduleId: PropTypes.number,
      scheduleTypeId: PropTypes.number,
      userId: PropTypes.number,
      scheduleDate: PropTypes.string,
      scheduleStartTime: PropTypes.string,
      scheduleEndTime: PropTypes.string,
      scheduleLocation: PropTypes.string,
    }),
  ),
  onEdit: PropTypes.func,
};
const defaultProps = {
  schedules: [],
  onEdit: () => {},
};

class ProfileSitterSchedule extends React.Component {
  onClickEdit = () => {
    return this.props.onEdit();
  };

  render() {
    const { title } = this.props;
    return (
      <ExpansionPanel>
        <ExpansionPanelSummary>
          <Typography variant={'body1'}> {title} </Typography>
        </ExpansionPanelSummary>

        <ExpansionPanelDetails style={{ paddingTop: '0', paddingBottom: '0' }}>
          <Grid container spacing={16}>
            <Grid item xs={12}>
              <Schedules schedules={this.props.schedules} />
            </Grid>
          </Grid>
        </ExpansionPanelDetails>
        <ExpansionPanelActions>
          <Button variant={'raised'} onClick={this.onClickEdit}>
            수정하기
          </Button>
        </ExpansionPanelActions>
      </ExpansionPanel>
    );
  }
}

ProfileSitterSchedule.propTypes = propTypes;
ProfileSitterSchedule.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(ProfileSitterSchedule);
