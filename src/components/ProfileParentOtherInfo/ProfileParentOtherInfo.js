/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */
import { compose } from 'recompose';
import { withStyles as withMaterialStyle } from '@material-ui/core/styles';
import React from 'react';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ProfileParentOtherInfo.css';

import CareWayGroup from '../../util/const/CareWayGroup';
import InterviewWayGroup from '../../util/const/InterviewWayGroup';
import GenderTypeGroup from '../../util/const/GenderTypeGroup';

const propTypes = {
  title: PropTypes.string.isRequired,
  wantedInterviewWay: PropTypes.string,
  wantedCareWay: PropTypes.string,
  wantedSitterGender: PropTypes.string,
  preferMinAge: PropTypes.number,
  preferMaxAge: PropTypes.number,
  onEdit: PropTypes.func,
};
const defaultProps = {
  wantedInterviewWay: null,
  wantedCareWay: null,
  wantedSitterGender: null,
  preferMinAge: null,
  preferMaxAge: null,
  onEdit: () => {},
};

class ProfileParentOtherInfo extends React.Component {
  onClickEdit = () => {
    return this.props.onEdit();
  };

  getGender = () => {
    const { wantedSitterGender } = this.props;
    switch (wantedSitterGender) {
      case GenderTypeGroup.MAN:
        return '남자';
      case GenderTypeGroup.WOMEN:
        return '여자';
      case GenderTypeGroup.IDC:
        return '무관';
      default:
        return '입력하지 않음';
    }
  };

  getSitterAge = () => {
    const { preferMinAge, preferMaxAge } = this.props;
    if (preferMinAge && preferMaxAge) {
      return `${preferMinAge} ~ ${preferMaxAge}`;
    }
    return '입력하지 않음';
  };

  getWantedInterviewWay = () => {
    const { wantedInterviewWay } = this.props;
    switch (wantedInterviewWay) {
      case InterviewWayGroup.DEMO:
        return '시험채용';
      case InterviewWayGroup.FACE:
        return '대면 인터뷰';
      case InterviewWayGroup.PHONE:
        return '전화 인터뷰';
      default:
        return '입력하지 않음';
    }
  };

  getWantedCareWay = () => {
    const { wantedCareWay } = this.props;
    switch (wantedCareWay) {
      case CareWayGroup.ALONE:
        return '부모 대신 혼자 돌봐주세요.';
      case CareWayGroup.WITH_PARENT:
        return '엄마 아빠와 함께 돌봐주세요.';
      case CareWayGroup.WITH_ELDER:
        return '할머니(할아버지)와 함께 돌봐주세요.';
      case CareWayGroup.WITH_HELPER:
        return '이모님(도우미)과 함께 돌봐주세요.';
      default:
        return '입력하지 않음';
    }
  };

  render() {
    const { title } = this.props;
    return (
      <ExpansionPanel>
        <ExpansionPanelSummary>
          <Typography variant="body1">{title}</Typography>
        </ExpansionPanelSummary>

        <ExpansionPanelDetails style={{ paddingTop: '0', paddingBottom: '0' }}>
          <Grid container spacing={16}>
            <Grid item xs={3}>
              <Typography variant={'caption'}>원하는 맘시터 성별</Typography>
            </Grid>
            <Grid item xs={9}>
              {this.getGender()}
            </Grid>

            <Grid item xs={3}>
              <Typography variant={'caption'}>원하는 맘시터 연령대</Typography>
            </Grid>
            <Grid item xs={9}>
              {this.getSitterAge()}
            </Grid>

            <Grid item xs={3}>
              <Typography variant={'caption'}>인터뷰 방식</Typography>
            </Grid>
            <Grid item xs={9}>
              {this.getWantedInterviewWay()}
            </Grid>

            <Grid item xs={3}>
              <Typography variant={'caption'}>돌보는 방식</Typography>
            </Grid>
            <Grid item xs={9}>
              {this.getWantedCareWay()}
            </Grid>
          </Grid>
        </ExpansionPanelDetails>
        <ExpansionPanelActions>
          <Button variant={'raised'} onClick={this.onClickEdit}>
            수정하기
          </Button>
        </ExpansionPanelActions>
      </ExpansionPanel>
    );
  }
}

ProfileParentOtherInfo.propTypes = propTypes;
ProfileParentOtherInfo.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyle(s, { withTheme: true }),
)(ProfileParentOtherInfo);
