import { compose } from 'recompose';
import React from 'react';
import {
  withStyles as withMaterialStyles,
  Grid,
} from '@material-ui/core';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './FormParentOtherInfo.css';
import FormGenderPicker from '../FormGenderPicker';
import FormInterviewWayPicker from '../FormInterviewWayPicker';
import FormCareWay from '../FormCareWay';
import GenderTypeGroup from '../../util/const/GenderTypeGroup';
import InterviewWayGroup from '../../util/const/InterviewWayGroup';
import CareWayGroup from '../../util/const/CareWayGroup';
import AgeRangePicker from '../AgeRangePicker';

const propTypes = {
  defaultWantedSitterGender: PropTypes.oneOf([GenderTypeGroup.WOMEN, GenderTypeGroup.MAN, GenderTypeGroup.IDC]),
  defaultPreferMinAge: PropTypes.number,
  defaultPreferMaxAge: PropTypes.number,
  defaultWantedInterviewWay: PropTypes.oneOf([InterviewWayGroup.DEMO, InterviewWayGroup.FACE, InterviewWayGroup.PHONE]),
  defaultWantedCareWay: PropTypes.oneOf([
    CareWayGroup.ALONE,
    CareWayGroup.WITH_HELPER,
    CareWayGroup.WITH_ELDER,
    CareWayGroup.WITH_PARENT,
  ]),
  onChange: PropTypes.func,
};
const defaultProps = {
  defaultWantedSitterGender: GenderTypeGroup.WOMEN,
  defaultPreferMinAge: 20,
  defaultPreferMaxAge: 30,
  defaultWantedInterviewWay: InterviewWayGroup.PHONE,
  defaultWantedCareWay: CareWayGroup.ALONE,
  onChange: () => {},
};

class FormParentOtherInfo extends React.Component {
  constructor(props) {
    super(props);
    this.formData = {
      wantedSitterGender: null,
      preferMinAge: null,
      preferMaxAge: null,
      wantedInterviewWay: null,
      wantedCareWay: null,
    };
  }

  componentDidMount() {
    const {
      defaultWantedSitterGender,
      defaultPreferMinAge,
      defaultPreferMaxAge,
      defaultWantedInterviewWay,
      defaultWantedCareWay,
    } = this.props;

    this.formData.wantedSitterGender = defaultWantedSitterGender;
    this.formData.preferMinAge = defaultPreferMinAge;
    this.formData.preferMaxAge = defaultPreferMaxAge;
    this.formData.wantedInterviewWay = defaultWantedInterviewWay;
    this.formData.wantedCareWay = defaultWantedCareWay;
  }

  handleChange = () => {
    return this.props.onChange(this.formData);
  };

  handleFormGenerPickerChange = value => {
    this.formData.wantedSitterGender = value;
    this.handleChange();
  };

  handleAgeRangePickerChange = value => {
    this.formData.preferMinAge = value[0];
    this.formData.preferMaxAge = value[1];
    this.handleChange();
  };

  handleFormInterviewWayPickerChange = value => {
    this.formData.wantedInterviewWay = value;
    this.handleChange();
  };

  handleFormCareWayChange = value => {
    console.log(`DEBUG : CARE WAY CHANGE : ${value}`);
    this.formData.wantedCareWay = value;
    this.handleChange();
  };

  render() {
    const {
      defaultWantedSitterGender,
      defaultPreferMinAge,
      defaultPreferMaxAge,
      defaultWantedInterviewWay,
      defaultWantedCareWay,
    } = this.props;
    return (
      <div className={s.root}>
        <Grid container spacing={16}>
          <Grid item xs={12}>
            <h3 className={s.title}> 원하는 맘시터 성별 </h3>
          </Grid>
          <Grid item xs={12}>
            <FormGenderPicker defaultGender={defaultWantedSitterGender} onChange={this.handleFormGenerPickerChange} />
          </Grid>

          <Grid item xs={12}>
            <h3 className={s.title}> 원하는 맘시터 연령대 </h3>
          </Grid>
          <Grid item xs={12}>
            <AgeRangePicker
              defaultMin={defaultPreferMinAge}
              defaultMax={defaultPreferMaxAge}
              onChange={this.handleAgeRangePickerChange}
            />
          </Grid>

          <Grid item xs={12}>
            <h3 className={s.title}>인터뷰 방식</h3>
          </Grid>
          <Grid item xs={12}>
            <FormInterviewWayPicker
              defaultInterviewWay={defaultWantedInterviewWay}
              onChange={this.handleFormInterviewWayPickerChange}
            />
          </Grid>

          <Grid item xs={12}>
            <h3 className={s.title}> 돌보는 방식 </h3>
          </Grid>
          <Grid item xs={12}>
            <FormCareWay defaultCareWay={defaultWantedCareWay} onChange={this.handleFormCareWayChange} />
          </Grid>
        </Grid>
      </div>
    );
  }
}

FormParentOtherInfo.propTypes = propTypes;
FormParentOtherInfo.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(FormParentOtherInfo);
