import moment from 'moment';
import { compose } from 'recompose';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './BoardNoticeItem.css';

const propTypes = {
  data: PropTypes.shape({
    id: PropTypes.number,
    title: PropTypes.string,
    content: PropTypes.string,
    author: PropTypes.string,
    registerDate: PropTypes.string,
    updateDate: PropTypes.string,
    view: PropTypes.number,
  }).isRequired,
  onClickEdit: PropTypes.func,
  onClickDelete: PropTypes.func,
};
const defaultProps = {
  onClickEdit: () => {},
  onClickDelete: () => {},
};

class BoardNoticeItem extends React.Component {
  onClickEdit = () => {
    const { id } = this.props.data;
    return this.props.onClickEdit(id);
  };

  onClickDelete = () => {
    const { id } = this.props.data;
    return this.props.onClickDelete(id);
  };

  render() {
    const { data } = this.props;
    const { id, title, author, registerDate, updateDate, view } = data;
    return (
      <div className={s.root}>
        <Grow in>
          <Paper style={{ padding: '0.8rem' }}>
            <Grid
              container
              spacing={16}
              alignItems="center"
              justify="center"
              direction="row"
            >
              <Grid item xs={1}>
                <Checkbox />
              </Grid>
              <Grid item xs={1} style={{ textAlign: 'center' }}>
                <Typography> {id} </Typography>
              </Grid>
              <Grid item xs={1} style={{ textAlign: 'center' }}>
                <Typography> {author} </Typography>
              </Grid>
              <Grid item xs={3} style={{ textAlign: 'center' }}>
                <Typography style={{ overflow: 'hidden', textOverflow: 'ellipsis', whiteSpace: 'nowrap' }}> {title} </Typography>
              </Grid>
              <Grid item xs={2} style={{ textAlign: 'center' }}>
                <Typography> {registerDate ? moment(registerDate).format('YYYY년 MM월 DD일 HH시 mm분') : ''} </Typography>
              </Grid>
              <Grid item xs={2} style={{ textAlign: 'center' }}>
                <Typography> {updateDate ?moment(updateDate).format('YYYY년 MM월 DD일 HH시 mm분') : ''} </Typography>
              </Grid>
              <Grid item xs={1} style={{ textAlign: 'center' }}>
                <Typography> {view} </Typography>
              </Grid>
              <Grid item xs={1} style={{ textAlign: 'center' }}>
                <IconButton onClick={this.onClickEdit}>
                  <EditIcon style={{ width: '20px', height: '20px' }} />
                </IconButton>
                <IconButton onClick={this.onClickDelete}>
                  <DeleteIcon style={{ width: '20px', height: '20px' }} />
                </IconButton>
              </Grid>
            </Grid>
          </Paper>
        </Grow>
      </div>
    );
  }
}

BoardNoticeItem.propTypes = propTypes;
BoardNoticeItem.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(BoardNoticeItem);
