import { compose } from 'recompose';
import React from 'react';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles/index';
import Button from '@material-ui/core/Button';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import LocationIcon from '@material-ui/icons/LocationOn';

import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ProfileParentLocation.css';

const propTypes = {
  title: PropTypes.string.isRequired,
  locations: PropTypes.arrayOf(
    PropTypes.shape({
      locationId: PropTypes.number,
      locationType: PropTypes.string,
      userId: PropTypes.number,
      jibunAddr: PropTypes.string,
      roadAddr: PropTypes.string,
      locationLatitude: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
      locationLongitude: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
      siNm: PropTypes.string,
      sggNm: PropTypes.string,
      emdNm: PropTypes.string,
      detailAddr: PropTypes.string,
      postcode: PropTypes.string,
      zonecode: PropTypes.string,
    }),
  ),
  onEdit: PropTypes.func,
};
const defaultProps = {
  locations: [],
  onEdit: () => {},
};

class ProfileParentLocation extends React.Component {
  onClickEdit = () => {
    return this.props.onEdit();
  };

  renderLocations = () => {
    const { locations } = this.props;
    if (locations.length === 0) {
      return this.renderEmpty();
    }
    return locations.map((item, index) => {
      // Note. 주소 포맷에 맞추어서 노출
      return (
        <div className={s.locationItem} key={`location-item-${index}`}>
          <LocationIcon style={{ width: '20px', height: '25px', fill: '#11b38f', marginRight: '5px' }} />
          <div className={s.locationLabel}> {`${item.siNm || ''} ${item.sggNm || ''} ${item.emdNm || ''}`.trim()} </div>
        </div>
      );
    });
  };

  renderEmpty = () => {
    return <div> 표시할 장소가 없습니다. </div>;
  };

  render() {
    const { title } = this.props;
    return (
      <ExpansionPanel>
        <ExpansionPanelSummary>
          <Typography variant={'body1'}>{title}</Typography>
        </ExpansionPanelSummary>

        <ExpansionPanelDetails style={{ paddingTop: '0', paddingBottom: '0' }}>
          <Grid container spacing={16}>
            <Grid item xs={12}>
              {this.renderLocations()}
            </Grid>
          </Grid>
        </ExpansionPanelDetails>
        <ExpansionPanelActions>
          <Button variant={'raised'} onClick={this.onClickEdit}>
            수정하기
          </Button>
        </ExpansionPanelActions>
      </ExpansionPanel>
    );
  }
}

ProfileParentLocation.propTypes = propTypes;
ProfileParentLocation.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(ProfileParentLocation);
