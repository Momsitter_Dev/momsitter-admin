import Moment from 'moment';
import { extendMoment } from 'moment-range';

import React from 'react';
import DayPicker, { DateUtils } from 'react-day-picker';
import Button from '@material-ui/core/Button';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import dayPickerStyle from '!!isomorphic-style-loader!css-loader?modules=false!./dayPickerStyle.css';
import s from './FormParentSchedule.css';

import ParentScheduleTypeGroup from '../../util/const/ParentScheduleTypeGroup';
import getWeekdayCheck from '../../util/schdule/getWeekdayCheck';
import baseSunToBaseMonMoment from '../../util/schdule/baseSunToBaseMonMoment';

import TimePicker from '../TimePicker';
import DatePicker from '../DatePicker';

const moment = extendMoment(Moment);

moment.locale('ko');

const WEEK_DAYS = ['월요일', '화요일', '수요일', '목요일', '금요일', '토요일', '일요일'];

const MOMENT_WEEK_DAY_INDEX = {
  0: 6,
  1: 0,
  2: 1,
  3: 2,
  4: 3,
  5: 4,
  6: 5,
};

const propTypes = {
  schedules: PropTypes.array,
  scheduleType: PropTypes.string,
  scheduleNegotiable: PropTypes.bool,
  onChange: PropTypes.func,
};
const defaultProps = {
  schedules: [],
  scheduleType: ParentScheduleTypeGroup.SCHEDULE_TYPE_REGULAR,
  scheduleNegotiable: false,
  onChange: () => {},
};

class FormParentSchedule extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // Note. 선택된 스케쥴 유형 (정기적, 특정날, 무계획)
      scheduleType: null,
      // Note. 정기적 스케쥴일 경우의 시작 날짜
      scheduleStartDate: null,
      // Note. 정기적 스케쥴일 경우의 선택 요일
      scheduleWeekday: [
        false, // 월
        false, // 화
        false, // 수
        false, // 목
        false, // 금
        false, // 토
        false, // 일
      ],
      scheduleWeekDayTime: {
        0: { start: moment('12:00', 'HH:mm').toDate(), end: moment('22:00', 'HH:mm').toDate() },
        1: { start: moment('12:00', 'HH:mm').toDate(), end: moment('22:00', 'HH:mm').toDate() },
        2: { start: moment('12:00', 'HH:mm').toDate(), end: moment('22:00', 'HH:mm').toDate() },
        3: { start: moment('12:00', 'HH:mm').toDate(), end: moment('22:00', 'HH:mm').toDate() },
        4: { start: moment('12:00', 'HH:mm').toDate(), end: moment('22:00', 'HH:mm').toDate() },
        5: { start: moment('12:00', 'HH:mm').toDate(), end: moment('22:00', 'HH:mm').toDate() },
        6: { start: moment('12:00', 'HH:mm').toDate(), end: moment('22:00', 'HH:mm').toDate() },
      },
      // Note. 스케쥴의 시작시간, 종료시간
      scheduleStartTime: moment('12:00', 'HH:mm').toDate(),
      scheduleEndTime: moment('22:00', 'HH:mm').toDate(),
      // Note. 스케쥴 조율 가능 여부
      scheduleNegotiable: false,
      // Note. 특정날이나 무계획일 경우 디폴트로 선택된 날짜
      selectedDays: [
        moment()
          .add(6, 'days')
          .toDate(),
      ],
    };
  }

  async componentDidMount() {
    try {
      const { schedules, scheduleType, scheduleNegotiable } = this.props;
      if (scheduleType === ParentScheduleTypeGroup.SCHEDULE_TYPE_REGULAR) {
        this.setState({
          scheduleType,
          scheduleNegotiable,
          scheduleWeekDayTime: this.getWeekdayTimeRange(schedules),
          scheduleStartDate: moment
            .min(
              schedules.map(item => {
                return moment(item.scheduleDate);
              }),
            )
            .toDate(),
          scheduleWeekday: getWeekdayCheck(schedules),
        });
      } else if (scheduleType === ParentScheduleTypeGroup.SCHEDULE_TYPE_SHORT_TERM) {
        this.setState({
          scheduleType,
          scheduleNegotiable,
          scheduleStartDate: moment
            .min(
              schedules.map(item => {
                return moment(item.scheduleDate);
              }),
            )
            .toDate(),
          selectedDays: schedules.map(item => {
            return moment(item.scheduleDate).toDate();
          }),
          scheduleStartTime: moment(schedules[0].scheduleStartTime, 'HH:mm').toDate(),
          scheduleEndTime: moment(schedules[0].scheduleEndTime, 'HH:mm').toDate(),
        });
      } else if (scheduleType === ParentScheduleTypeGroup.SCHEDULE_TYPE_UNPLANNED) {
        this.setState({
          scheduleType: ParentScheduleTypeGroup.SCHEDULE_TYPE_REGULAR,
          scheduleNegotiable,
        });
      }
    } catch (err) {
      console.log(err);
    }
  }

  onChange = () => {
    return this.props.onChange(this.getScheduleData());
  };

  onChangeTime = (type, value) => {
    // TODO: 요일별 시간 변경
    const { scheduleStartTime, scheduleEndTime } = this.state;

    if (type === 'start') {
      return this.setState(
        {
          scheduleStartTime: value,
          scheduleWeekDayTime: {
            0: { start: value, end: scheduleEndTime },
            1: { start: value, end: scheduleEndTime },
            2: { start: value, end: scheduleEndTime },
            3: { start: value, end: scheduleEndTime },
            4: { start: value, end: scheduleEndTime },
            5: { start: value, end: scheduleEndTime },
            6: { start: value, end: scheduleEndTime },
          },
        },
        () => this.onChange(),
      );
    } else {
      return this.setState(
        {
          scheduleEndTime: value,
          scheduleWeekDayTime: {
            0: { start: scheduleStartTime, end: value },
            1: { start: scheduleStartTime, end: value },
            2: { start: scheduleStartTime, end: value },
            3: { start: scheduleStartTime, end: value },
            4: { start: scheduleStartTime, end: value },
            5: { start: scheduleStartTime, end: value },
            6: { start: scheduleStartTime, end: value },
          },
        },
        () => this.onChange(),
      );
    }
  };

  onChangeDateTime = (index, type, value) => {
    const { scheduleWeekDayTime } = this.state;
    const datetime = {};
    datetime[index] = {
      start: null,
      end: null,
      ...scheduleWeekDayTime[index],
    };
    datetime[index][type] = value;
    return this.setState(
      {
        scheduleWeekDayTime: {
          ...scheduleWeekDayTime,
          ...datetime,
        },
      },
      () => this.onChange(),
    );
  };

  getScheduleData = () => {
    // TODO: 현재 form 이 valid 한지 검사.
    const {
      scheduleType,
      scheduleWeekDayTime,
      scheduleWeekday,
      scheduleNegotiable,
      selectedDays,
      scheduleStartTime,
      scheduleEndTime,
    } = this.state;
    const DATE_RANGE = moment.range(moment(), moment().add(6, 'months'));
    if (scheduleType === ParentScheduleTypeGroup.SCHEDULE_TYPE_REGULAR) {
      const schedules = Array.from(DATE_RANGE.by('day'));
      const SELECTED_DATE = WEEK_DAYS.reduce((acc, item, index) => {
        acc[item] = !!scheduleWeekday[index];
        return acc;
      }, {});
      const filteredDate = schedules
        .filter(item => {
          return SELECTED_DATE[item.format('dddd')];
        })
        .map(item => {
          const target = scheduleWeekDayTime[MOMENT_WEEK_DAY_INDEX[item.format('e')]];
          const { start, end } = target;
          return {
            scheduleDate: item.format('YYYY-MM-DD 00:00:00'),
            scheduleStartTime: moment(start).format('HH:mm'),
            scheduleEndTime: moment(end).format('HH:mm'),
          };
        });
      return {
        schedules: filteredDate,
        data: {
          scheduleType,
          scheduleNegotiable,
        },
      };
    }
    if (scheduleType === ParentScheduleTypeGroup.SCHEDULE_TYPE_SHORT_TERM) {
      return {
        schedules: selectedDays.map(item => {
          return {
            scheduleDate: moment(item).format('YYYY-MM-DD 00:00:00'),
            scheduleStartTime: moment(scheduleStartTime).format('HH:mm'),
            scheduleEndTime: moment(scheduleEndTime).format('HH:mm'),
          };
        }),
        data: {
          scheduleType,
          scheduleNegotiable,
        },
      };
    }
    throw new Error(`cannot create data with scheduleType: ${scheduleType}`);
  };

  // Note. 정기적 스케쥴의 경우, 요일중에 하나라도 선택이 되었는지 확인하는 함수
  isAnyScheduleWeekdaySelected = () => this.state.scheduleWeekday.find(item => item === true);

  // Note. 특정날 혹은 무계획 스케쥴의 경우, 하나라도 선택된 날이 있는지 확인하는 함수
  isAnyDaySelected = () => this.state.selectedDays.length > 0;

  // Note. 스케쥴 유형이 선택했을 때의 이벤트 핸들러
  handleScheduleTypeButton = scheduleType => {
    const scheduleStartDate =
      scheduleType === ParentScheduleTypeGroup.SCHEDULE_TYPE_REGULAR
        ? moment()
            .add(6, 'days')
            .toDate()
        : null;
    return this.setState(
      {
        scheduleType,
        scheduleStartDate,
      },
      () => this.onChange(),
    );
  };

  // Note. 정기적 스케쥴의 시작날짜가 변경되었을 때 이벤트 핸들러
  handleScheduleStartDateChange = value => {
    this.setState(
      {
        scheduleStartDate: value.toDate(),
      },
      () => this.onChange(),
    );
  };

  // Note. 정기적 스케쥴에서 요일이 선택되었을 때
  handleWeekdayClick = weekdayIndex => {
    const { scheduleWeekday } = this.state;
    scheduleWeekday[weekdayIndex] = !scheduleWeekday[weekdayIndex];
    this.setState(
      {
        scheduleWeekday,
      },
      () => this.onChange(),
    );
  };

  // Note. 특정날, 무계획 스케쥴인 경우 날짜를 선택했을 때 이벤트 핸들러
  handleDayClick = (day, { selected }) => {
    const { selectedDays } = this.state;
    // Note. 오늘 기준 이전 날짜 데이터는 무시
    if (moment(day).isBefore(new Date())) return;
    // Note. 이미 선택된 날짜는 삭제하고, 선택되지 않은 날짜는 선택으로 추가
    if (selected) {
      const selectedIndex = selectedDays.findIndex(item => DateUtils.isSameDay(item, day));
      selectedDays.splice(selectedIndex, 1);
    } else {
      selectedDays.push(day);
    }
    this.setState(
      {
        selectedDays,
      },
      () => this.onChange(),
    );
  };

  getWeekdayTimeRange = schedules => {
    const { scheduleWeekDayTime } = this.state;
    const weekdayParsed = schedules.reduce((acc, item) => {
      const current = moment(item.scheduleDate);
      const currentWeek = baseSunToBaseMonMoment(current);
      if (!acc[currentWeek]) {
        acc[currentWeek] = {
          start: moment(item.scheduleStartTime, 'HH:mm').toDate(),
          end: moment(item.scheduleEndTime, 'HH:mm').toDate(),
        };
      }
      return acc;
    }, {});

    console.log(weekdayParsed);

    return {
      ...scheduleWeekDayTime,
      ...weekdayParsed,
    };
  };

  // Note. 스케쥴 조율 가능 여부를 렌더링 하는 버튼
  renderButtonScheduleNegotiable = () => {
    const { scheduleNegotiable } = this.state;
    return (
      <div
        role={'button'}
        className={s.buttonNegotiable}
        style={{ marginTop: 15, marginBottom: 14 }}
        onClick={() => this.setState({ scheduleNegotiable: !scheduleNegotiable }, () => this.onChange())}
      >
        <span
          className={scheduleNegotiable ? s.negotiableCheckIconOn : s.negotiableCheckIcon}
          style={{ marginRight: 8 }}
        />
        <span className={s.negotiableLabel}>본 일정은 맘시터에 맞춰서 얼마든지 조정할 수 있어요.</span>
      </div>
    );
  };

  // TODO: 요일 선택 콤포넌트 분리 구현할 것
  // Note. 정기적 스케쥴 입력에서 돌봄 요일 선택 UI
  renderWeekdayCheckbox = () => {
    const { scheduleWeekday } = this.state;
    const label = ['월', '화', '수', '목', '금', '토', '일'];
    return (
      <div className={s.wrapWeekday}>
        {label.map((item, index) => (
          <Button
            key={item}
            className={scheduleWeekday[index] ? s.buttonWeekdayOn : s.buttonWeekday}
            onClick={() => this.handleWeekdayClick(index)}
          >
            {item}
          </Button>
        ))}
      </div>
    );
  };

  // TODO 시작-종료시간을 선택할 수 있는 레인지형 콤포넌트 구현
  // Note. 시작, 종료 시간을 선택할 수 있는 UI
  renderTimeRange = () => {
    const { scheduleStartTime, scheduleEndTime } = this.state;
    return (
      <div className={s.wrapRegularScheduleTimeRange}>
        <div>
          <div className={s.labelScheduleForm}>시작시간</div>
          <TimePicker
            time={scheduleStartTime}
            maxTime={moment(scheduleEndTime).subtract(30, 'minutes')}
            style={{
              width: 101,
              height: 37,
              border: '1px solid #ffcfb8',
              backgroundColor: '#fff0e8',
              marginTop: 6,
            }}
            timeStyle={{
              width: 101,
              height: 37,
            }}
            timeLabelStyle={{
              fontSize: '0.9375rem',
              fontWeight: 500,
              color: '#ff5300',
              verticalAlign: 'top',
              padding: 0,
            }}
            timeLabelFormat={'A HH:mm'}
            // Note. 데스크탑 버전 메뉴 스타일
            selectFieldStyle={{
              width: 135,
              border: '1px solid #ffcfb8',
              backgroundColor: '#fff0e8',
              marginTop: 6,
            }}
            onChange={value => this.onChangeTime('start', value.toDate())}
          />
        </div>
        <div style={{ borderRight: '1px solid #efefef' }} />
        <div>
          <div className={s.labelScheduleForm}>종료시간</div>
          <TimePicker
            time={scheduleEndTime}
            minTime={moment(scheduleStartTime).add(30, 'minutes')}
            style={{
              width: 101,
              height: 37,
              border: '1px solid #ffcfb8',
              backgroundColor: '#fff0e8',
              marginTop: 6,
            }}
            timeStyle={{
              width: 101,
              height: 37,
            }}
            timeLabelStyle={{
              fontSize: '0.9375rem',
              fontWeight: 500,
              color: '#ff5300',
              verticalAlign: 'top',
              padding: 0,
            }}
            timeLabelFormat={'A HH:mm'}
            // Note. 데스크탑 버전 메뉴 스타일
            selectFieldStyle={{
              width: 135,
              border: '1px solid #ffcfb8',
              backgroundColor: '#fff0e8',
              marginTop: 6,
            }}
            onChange={value => this.onChangeTime('end', value.toDate())}
          />
        </div>
      </div>
    );
  };

  renderDateTimeRange = () => {
    const { scheduleWeekday, scheduleWeekDayTime } = this.state;
    return scheduleWeekday.map((item, index) => {
      if (!item) {
        return null;
      }
      return (
        <div key={`date-time-item-${index}`} className={s.dateTimeItem}>
          <div
            style={{
              fontSize: '0.9rem',
              fontWeight: '400',
              color: '#646464',
            }}
          >
            {' '}
            {['월', '화', '수', '목', '금', '토', '일'][index]}{' '}
          </div>
          <div>
            <TimePicker
              time={scheduleWeekDayTime[index].start}
              maxTime={moment(scheduleWeekDayTime[index].end).subtract(30, 'minutes')}
              onChange={value => this.onChangeDateTime(index, 'start', value)}
            />
          </div>
          <div>
            <TimePicker
              time={scheduleWeekDayTime[index].end}
              minTime={moment(scheduleWeekDayTime[index].start).add(30, 'minutes')}
              onChange={value => this.onChangeDateTime(index, 'end', value)}
            />
          </div>
        </div>
      );
    });
  };

  // Note. 한줄 버전의 시간 선택 UI
  renderTimeRangeInline = () => {
    const { scheduleStartTime, scheduleEndTime } = this.state;
    return (
      <div className={s.wrapUnplannedScheduleTimeRange}>
        <TimePicker
          time={scheduleStartTime}
          maxTime={moment(scheduleEndTime).subtract(30, 'minutes')}
          style={{
            width: 70,
            height: 25,
            minWidth: 0,
            border: '1px solid #ffcfb8',
            backgroundColor: '#fff0e8',
          }}
          timeStyle={{
            width: 70,
            height: 25,
            minWidth: 0,
          }}
          timeLabelStyle={{
            fontSize: '0.85rem',
            fontWeight: 500,
            color: '#ff5300',
            verticalAlign: 'top',
            padding: 0,
            lineHeight: '25px',
          }}
          timeLabelFormat={'A HH:mm'}
          // Note. 데스크탑 버전 메뉴 스타일
          selectFieldStyle={{
            width: 135,
            border: '1px solid #ffcfb8',
            backgroundColor: '#fff0e8',
            marginTop: 6,
          }}
          onChange={value => this.setState({ scheduleStartTime: value.toDate() }, () => this.onChange())}
        />
        <span className={s.inlineTimeText}>&nbsp;&nbsp;부터&nbsp;&nbsp;</span>
        <TimePicker
          time={scheduleEndTime}
          minTime={moment(scheduleStartTime).add(30, 'minutes')}
          style={{
            width: 70,
            height: 25,
            minWidth: 0,
            border: '1px solid #ffcfb8',
            backgroundColor: '#fff0e8',
          }}
          timeStyle={{
            width: 70,
            height: 25,
            minWidth: 0,
          }}
          timeLabelStyle={{
            fontSize: '0.85rem',
            fontWeight: 500,
            color: '#ff5300',
            verticalAlign: 'top',
            padding: 0,
            lineHeight: '25px',
          }}
          timeLabelFormat={'A HH:mm'}
          // Note. 데스크탑 버전 메뉴 스타일
          selectFieldStyle={{
            width: 135,
            border: '1px solid #ffcfb8',
            backgroundColor: '#fff0e8',
            marginTop: 6,
          }}
          onChange={value => this.setState({ scheduleEndTime: value.toDate() }, () => this.onChange())}
        />
        <span className={s.inlineTimeText}>&nbsp;사이에 만나볼게요. </span>
      </div>
    );
  };

  // Note. 특정한날 혹은 계획이 없을 때의 스케쥴 입력 UI
  renderShortTermOrUnplannedScheduleForm = () => {
    const { scheduleType, selectedDays } = this.state;
    return (
      <div className={s.wrapDayPickerForm}>
        <DayPicker
          selectedDays={selectedDays}
          modifiers={{
            disabled: {
              before: new Date(),
            },
          }}
          onDayClick={this.handleDayClick}
          months={['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']}
          labels={{ nextMonth: '다음달', previousMonth: '이전달' }}
          weekdaysShort={['일', '월', '화', '수', '목', '금', '토']}
          enableOutsideDays
        />
        <hr style={{ borderColor: '#efefef', margin: '8px 0' }} />
        {scheduleType === ParentScheduleTypeGroup.SCHEDULE_TYPE_SHORT_TERM && this.renderTimeRange()}
        <div style={{ margin: '0 20px' }}>
          {scheduleType === ParentScheduleTypeGroup.SCHEDULE_TYPE_SHORT_TERM && this.renderButtonScheduleNegotiable()}
        </div>
        {scheduleType === ParentScheduleTypeGroup.SCHEDULE_TYPE_UNPLANNED && (
          <div>
            {this.renderTimeRangeInline()}
            <hr style={{ borderColor: '#efefef', margin: '8px 0' }} />
          </div>
        )}
      </div>
    );
  };

  // Note. 정기적인 스케쥴 입력 UI
  renderRegularScheduleForm = () => {
    const { scheduleStartDate } = this.state;
    return (
      <div className={s.wrapRegularScheduleForm}>
        <div className={s.wrapRegularStartDate}>
          <div className={s.labelScheduleForm}>돌봄 시작일</div>
          <DatePicker
            date={scheduleStartDate}
            minDate={new Date()}
            maxDate={moment()
              .add(90, 'days')
              .toDate()}
            style={{
              border: '1px solid #ffcfb8',
              borderRadius: 2,
              backgroundColor: '#fff0e8',
              width: 101,
              height: 37,
            }}
            inputStyle={{
              height: 37,
            }}
            textFieldStyle={{
              height: 37,
            }}
            dateLabelStyle={{
              fontSize: '0.9375rem',
              fontWeight: 500,
              color: '#ff5300',
            }}
            onChange={this.handleScheduleStartDateChange}
          />
        </div>
        <hr style={{ borderColor: '#efefef', margin: '8px 0' }} />
        <div>
          <div className={s.labelScheduleForm}>돌봄 요일</div>
          {/* TODO: 요일을 선택하는 신규 콤포넌트 분리 구현할것 */}
          {this.renderWeekdayCheckbox()}
        </div>
        <hr style={{ borderColor: '#efefef', margin: '8px 0' }} />
        <div className={this.isAnyScheduleWeekdaySelected() ? s.showContents : s.hideContents}>
          {this.renderTimeRange()}
          {this.renderDateTimeRange()}
          {this.renderButtonScheduleNegotiable()}
        </div>
      </div>
    );
  };

  // Note. 현재 스케쥴 유형에 따른 입력 UI 를 렌더링
  renderScheduleForm = () => {
    const { scheduleType } = this.state;
    return (
      <div>
        <div className={s.wrapScheduleForm}>
          {scheduleType === ParentScheduleTypeGroup.SCHEDULE_TYPE_REGULAR && this.renderRegularScheduleForm()}
          {/* eslint-disable max-len */}
          {scheduleType !== ParentScheduleTypeGroup.SCHEDULE_TYPE_REGULAR &&
            this.renderShortTermOrUnplannedScheduleForm()}
          {/* eslint-enable max-len */}
        </div>
      </div>
    );
  };

  renderScheduleTypeButtons = () => {
    const { scheduleType } = this.state;
    return (
      <div>
        <div className={s.wrapScheduleTypeButtons}>
          <Button
            className={
              scheduleType === ParentScheduleTypeGroup.SCHEDULE_TYPE_REGULAR
                ? s.buttonScheduleTypeOn
                : s.buttonScheduleType
            }
            onClick={() => this.handleScheduleTypeButton(ParentScheduleTypeGroup.SCHEDULE_TYPE_REGULAR)}
          >
            정기적으로
          </Button>
          <Button
            className={
              scheduleType === ParentScheduleTypeGroup.SCHEDULE_TYPE_SHORT_TERM
                ? s.buttonScheduleTypeOn
                : s.buttonScheduleType
            }
            onClick={() => this.handleScheduleTypeButton(ParentScheduleTypeGroup.SCHEDULE_TYPE_SHORT_TERM)}
          >
            특정 날에만
          </Button>
        </div>
      </div>
    );
  };

  renderEditUI = () => {
    return (
      <div className={s.wrap}>
        <div className={s.stepStyle}>
          {this.renderScheduleTypeButtons()}
          {this.renderScheduleForm()}
        </div>
      </div>
    );
  };

  // eslint-disable-next-line no-undef
  renderContent = () => {
    return this.renderEditUI();
  };

  render() {
    return <div className={s.root}>{this.renderContent()}</div>;
  }
}

FormParentSchedule.propTypes = propTypes;
FormParentSchedule.defaultProps = defaultProps;

export default withStyles(s, dayPickerStyle)(FormParentSchedule);
