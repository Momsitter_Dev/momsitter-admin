/* eslint-disable no-undef,react/no-did-mount-set-state,max-len */
/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@mfort.co.kr
 */
import uuid from 'uuid/v4';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './FormCareAge.css';
import ChildAgeIdGroup from '../../util/const/ChildAgeIdGroup';
import CareAgeItem from '../CareAgeItem';

const propTypes = {
  onChange: PropTypes.func,
  initialSelectedIndex: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.number, PropTypes.string])),
};
const defaultProps = {
  onChange: () => {},
  initialSelectedIndex: null,
};

const CARE_AGE = [
  {
    id: ChildAgeIdGroup.NEW,
    label: '신생아',
    iconY: 'sitter-newborn-baby-s.svg',
    iconN: 'sitter-newborn-baby-n.svg',
  },
  {
    id: ChildAgeIdGroup.YONG,
    label: '영아',
    iconY: 'sitter-toddler-s.svg',
    iconN: 'sitter-toddler-n.svg',
  },
  {
    id: ChildAgeIdGroup.CHILD,
    label: '유아',
    iconY: 'sitter-child-s.svg',
    iconN: 'sitter-child-n.svg',
  },
  {
    id: ChildAgeIdGroup.ELEMENT,
    label: '초등학생',
    iconY: 'sitter-schoolchild-s.svg',
    iconN: 'sitter-schoolchild-n.svg',
  },
];

class FormCareAge extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      careAges: {},
    };
  }

  componentDidMount() {
    if (this.props.initialSelectedIndex) {
      const careAges = {};
      this.props.initialSelectedIndex.forEach(item => {
        careAges[item] = true;
      });
      this.setState({ careAges });
    }
  }

  onChange = () => {
    const ageIds = Object.keys(this.state.careAges).filter(item => {
      return this.state.careAges[item];
    });
    let isValid = true;
    if (!ageIds) {
      isValid = false;
    }
    if (ageIds.length <= 0) {
      isValid = false;
    }
    return this.props.onChange({
      isValid,
      ageIds,
    });
  };

  onClickItem = id => {
    const { careAges } = this.state;
    if (careAges[id]) {
      careAges[id] = false;
    } else {
      careAges[id] = true;
    }
    return this.setState(
      {
        careAges: Object.assign({}, careAges),
      },
      () => {
        return this.onChange();
      },
    );
  };

  renderItems = () => {
    return CARE_AGE.map(item => {
      if (this.state.careAges[item.id]) {
        return <CareAgeItem {...item} onClickItem={this.onClickItem} active key={uuid()} />;
      }
      return <CareAgeItem {...item} onClickItem={this.onClickItem} key={uuid()} />;
    });
  };

  render() {
    return <div className={s.root}>{this.renderItems()}</div>;
  }
}

FormCareAge.propTypes = propTypes;
FormCareAge.defaultProps = defaultProps;

export default withStyles(s)(FormCareAge);
