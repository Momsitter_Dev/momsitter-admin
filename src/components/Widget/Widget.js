import { compose } from 'recompose';
import { Paper, AppBar, Toolbar, Typography, Button, IconButton, Collapse, Grow } from '@material-ui/core';
import {
  Refresh as RefreshIcon,
  KeyboardArrowDown as ArrowDownIcon,
  KeyboardArrowUp as ArrowUpIcon,
} from '@material-ui/icons';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Widget.css';

const propTypes = {
  title: PropTypes.string,
  onRefresh: PropTypes.func,
  children: PropTypes.node,
  actions: PropTypes.node,
  defaultExpanded: PropTypes.bool,
};
const defaultProps = {
  title: 'untitled',
  onRefresh: null,
  actions: null,
  children: null,
  defaultExpanded: true,
};

class Widget extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: true,
    };
  }

  componentDidMount() {
    this.setState({
      expanded: this.props.defaultExpanded,
    });
  }

  onClickRefresh = () => {
    return this.props.onRefresh();
  };

  toggleExpand = () => {
    const { expanded } = this.state;
    return this.setState({
      expanded: !expanded,
    });
  };

  renderArrowIcon = () => {
    const { expanded } = this.state;
    if (expanded) {
      return <ArrowUpIcon />;
    }
    return <ArrowDownIcon />;
  };

  renderActionBar = () => {
    const { actions, onRefresh } = this.props;
    const { expanded } = this.state;
    if (!expanded) {
      return null;
    }
    if (actions || onRefresh) {
      return (
        <Grow in>
          <Toolbar style={{ justifyContent: 'flex-end' }}>
            {actions}
            {this.renderRefreshButton()}
          </Toolbar>
        </Grow>
      );
    }
    return null;
  };

  renderRefreshButton = () => {
    const { onRefresh } = this.props;
    if (onRefresh) {
      return (
        <Button variant="raised" color="primary" onClick={this.onClickRefresh}>
          새로고침
          <RefreshIcon style={{ marginLeft: '5px', width: '15px', height: '15px' }} />
        </Button>
      );
    }
    return null;
  };

  render() {
    const { title, children } = this.props;
    const { expanded } = this.state;
    return (
      <Paper
        elevation={1}
        style={{
          borderRadius: '5px',
          overflow: 'hidden',
          marginBottom: '15px',
        }}
      >
        <AppBar
          position="static"
          style={{
            backgroundColor: '#fff',
          }}
        >
          <Toolbar style={{ justifyContent: 'space-between' }}>
            <Typography variant="title" style={{ color: '#8a93a7' }}>
              {title}
            </Typography>
            <IconButton onClick={this.toggleExpand}>{this.renderArrowIcon()}</IconButton>
          </Toolbar>
          {this.renderActionBar()}
        </AppBar>
        <Collapse in={expanded}>
          <Paper style={{ padding: '10px', backgroundColor: '#f4f8fb' }}>{children}</Paper>
        </Collapse>
      </Paper>
    );
  }
}

Widget.propTypes = propTypes;
Widget.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(Widget);
