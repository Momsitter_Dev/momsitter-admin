import DoneIcon from 'material-ui/svg-icons/action/done';
import FlatButton from 'material-ui/FlatButton';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './SearchLocationItem.css';

const propTypes = {
  data: PropTypes.object.isRequired,
  dataKey: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  selectedItem: PropTypes.string,
};
const defaultProps = {
  onClick: () => {},
  selectedItem: '',
};

class SearchLocationItem extends React.Component {
  onClick = () => {
    const { data } = this.props;
    return this.props.onClick(data);
  };

  renderContent = () => {
    const { data, dataKey, selectedItem } = this.props;
    if (selectedItem === data[dataKey]) {
      return this.renderCheckedButton();
    }
    return this.renderButton();
  };

  renderButton = () => {
    const { data, dataKey } = this.props;
    return (
      <FlatButton
        fullWidth
        label={data[dataKey]}
        labelStyle={{
          fontSize: '0.9rem',
          fontWeight: '400',
          color: '#1e1e1e',
        }}
        style={{
          borderBottom: '1px solid #f0f0f0',
          height: '45px',
        }}
        onClick={this.onClick}
      />
    );
  };

  renderCheckedButton = () => {
    const { data, dataKey } = this.props;
    return (
      <FlatButton
        fullWidth
        icon={(dataKey === 'detail' || data[dataKey] === '전체') ? <DoneIcon style={{ width: '15px', height: '15px', fill: '#ff5300' }} /> : null}
        label={data[dataKey]}
        onClick={this.onClick}
        backgroundColor="#fff8f2"
        style={{
          borderBottom: '1px solid #f0f0f0',
          height: '45px',
        }}
        labelStyle={{
          fontSize: '0.9rem',
          fontWeight: 'bold',
          color: '#ff7100',
        }}
      />
    );
  };

  render() {
    return (
      <div className={s.root}>
        {this.renderContent()}
      </div>
    );
  }
}

SearchLocationItem.propTypes = propTypes;
SearchLocationItem.defaultProps = defaultProps;

export default withStyles(s)(SearchLocationItem);
