import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './FormSitterLocation.css';
import StepLocationPicker from '../StepLocationPicker';

const propTypes = {
  onChange: PropTypes.func,
  locations: PropTypes.arrayOf(
    PropTypes.shape({
      si: PropTypes.string,
      sgg: PropTypes.string,
      emd: PropTypes.string,
      locationLatitude: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
      locationLongitude: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    }),
  ),
};
const defaultProps = {
  onChange: () => {},
  locations: [],
};

class FormSitterLocation extends React.Component {
  onChange = (event, data) => {
    if (!data || !(data instanceof Array) || data.length === 0) {
      return this.props.onChange([]);
    }
    return this.props.onChange(data);
  };

  render() {
    return (
      <div className={s.root}>
        <div className={s.container}>
          <StepLocationPicker selectedLocations={this.props.locations} onChange={this.onChange} />
        </div>
      </div>
    );
  }
}

FormSitterLocation.propTypes = propTypes;
FormSitterLocation.defaultProps = defaultProps;

export default withStyles(s)(FormSitterLocation);
