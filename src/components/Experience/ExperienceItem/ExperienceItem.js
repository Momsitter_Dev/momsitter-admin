/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@mfort.co.kr
 */
import moment from 'moment';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ExperienceItem.css';
import DatePicker from '../../../components/DatePicker';

class ExperienceItem extends React.Component {
  getStyles = () => {
    return {
      textField: {
        style: {
          width: '100%',
          fontSize: '13px',
          fontWeight: '300',
        },
      },
      deleteButton: {
        labelStyle: {
          fontSize: '11px',
          fontWeight: 300,
          verticalAlign: 'top',
          paddingLeft: 0,
          paddingRight: 0,
          lineHeight: '30px',
        },
        style: {
          border: '1px solid #ddd',
          minWidth: '50px',
          width: 50,
          float: 'right',
          marginTop: '-5px',
          marginRight: '10px',
          height: 30,
        },
      },
    };
  };

  onTitleChange = (event, experienceTitle) => {
    const { index, startDate, endDate } = this.props;
    this.props.onChange(event, { index, experienceTitle, startDate, endDate });
  };

  onStartDateChange = startDate => {
    const { index, experienceTitle, endDate } = this.props;
    this.props.onChange(event, {
      index,
      experienceTitle,
      startDate: startDate.toDate(),
      endDate,
    });
  };

  onEndDateChange = endDate => {
    const { index, experienceTitle, startDate } = this.props;
    this.props.onChange(event, {
      index,
      experienceTitle,
      startDate,
      endDate: endDate.toDate(),
    });
  };

  render() {
    const { experienceTitle, startDate, endDate } = this.props;
    const styles = this.getStyles();

    return (
      <div className={s.root}>
        <div className={s.container}>
          <FlatButton
            label="삭제"
            labelStyle={styles.deleteButton.labelStyle}
            style={styles.deleteButton.style}
            onClick={() => this.props.onDelete(null, this.props.index)}
          />

          <div className={s.label}> 경험 내용 </div>
          <div>
            <TextField
              hintText="예) 키즈카페, 교육기관, 봉사활동"
              style={styles.textField.style}
              defaultValue={experienceTitle}
              onChange={this.onTitleChange}
            />
          </div>

          <br />

          <div className={s.label}> 경험 기간 </div>
          <div className={s.flex}>
            <div className={s.flexItem}>
              <DatePicker
                maxDate={new Date()}
                date={startDate}
                hintText="시작일자"
                openToYearSelection
                onChange={this.onStartDateChange}
                textFieldStyle={{ borderBottom: '1px solid #e0e0e0' }}
                inputStyle={{ color: '#000', fontSize: '16px', fontWeight: '300' }}
              />
            </div>
            <div className={s.flexDivider}> ㅡ </div>
            <div className={s.flexItem}>
              <DatePicker
                maxDate={new Date()}
                date={endDate}
                hintText="종료일자"
                openToYearSelection
                onChange={this.onEndDateChange}
                textFieldStyle={{ borderBottom: '1px solid #e0e0e0' }}
                inputStyle={{ color: '#000', fontSize: '16px', fontWeight: '300' }}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ExperienceItem.propTypes = {
  index: PropTypes.number,
  experienceTitle: PropTypes.string,
  startDate: PropTypes.instanceOf(Date),
  endDate: PropTypes.instanceOf(Date),
  onChange: PropTypes.func,
  onDelete: PropTypes.func,
};

ExperienceItem.defaultProps = {
  index: -1,
  experienceTitle: '',
  startDate: moment('2017-01-01', 'YYYY-MM-DD').toDate(),
  endDate: moment().toDate(),
  onChange: () => {},
  onDelete: () => {},
};

export default withStyles(s)(ExperienceItem);
