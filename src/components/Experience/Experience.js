/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@mfort.co.kr
 */

import uuid from 'uuid';
import FlatButton from 'material-ui/FlatButton';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Experience.css';
import ExperienceItem from './ExperienceItem';

class Experience extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      experiences: props.experiences
        ? props.experiences.map(item => {
            let startDate = item.startDate;
            let endDate = item.endDate;
            if (!(startDate instanceof Date)) {
              startDate = new Date(startDate);
            }
            if (!(endDate instanceof Date)) {
              endDate = new Date(endDate);
            }
            return {
              ...item,
              key: uuid(),
              startDate,
              endDate,
            };
          })
        : [],
    };
  }

  getStyles = () => {
    return {
      addButton: {
        style: {
          border: '1px solid #ff4500',
          height: 50,
        },
        labelStyle: {
          fontSize: '14px',
          fontWeight: 400,
          verticalAlign: 'top',
          color: '#ff4500',
        },
      },
    };
  };

  getExperienceItemRender = () => {
    const experiences = this.state.experiences;
    if (experiences.length <= 0) {
      return null;
    }
    return experiences.map((item, index) => {
      return (
        <ExperienceItem
          key={item.key}
          index={index}
          experienceTitle={item.experienceTitle}
          startDate={item.startDate}
          endDate={item.endDate}
          onChange={this.handleChange}
          onDelete={this.handleDelete}
        />
      );
    });
  };

  handleAdd = () => {
    const experiences = this.state.experiences;
    experiences.push({
      key: uuid(),
    });
    this.setState({
      experiences,
    });
    this.props.onChange(null, experiences);
  };

  handleChange = (event, value) => {
    const experiences = this.state.experiences;
    const { index } = value;
    experiences[index] = {
      ...experiences[index],
      ...value,
    };
    this.setState({ experiences });
    this.props.onChange(event, experiences);
  };

  handleDelete = (event, value) => {
    const experiences = this.state.experiences;
    experiences.splice(value, 1);
    this.setState({ experiences });
    return this.props.onChange(event, experiences);
  };

  render() {
    const styles = this.getStyles();
    return (
      <div className={s.root}>
        <div className={s.container}>
          {this.getExperienceItemRender()}
          <FlatButton
            label="+ 경험 내용 추가하기"
            fullWidth
            style={styles.addButton.style}
            labelStyle={styles.addButton.labelStyle}
            onClick={this.handleAdd}
          />
        </div>
      </div>
    );
  }
}

Experience.propTypes = {
  experiences: PropTypes.arrayOf(
    PropTypes.shape({
      experienceTitle: PropTypes.string,
      startDate: PropTypes.oneOfType([PropTypes.instanceOf(Date), PropTypes.string]),
      endDate: PropTypes.oneOfType([PropTypes.instanceOf(Date), PropTypes.string]),
    }),
  ),
  onChange: PropTypes.func,
};

Experience.defaultProps = {
  experiences: [],
  onChange: () => {},
};

export default withStyles(s)(Experience);
