import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './LoginLayout.css';
import QueueAnim from 'rc-queue-anim';

class LoginLayout extends React.Component {
  render() {
    return (
      <div className="page-login">
        <div className="main-body">
          <QueueAnim type="bottom" className="ui-animate">
            <div key="1">
              {this.props.children}
            </div>
          </QueueAnim>
        </div>
      </div>
    );
  }
}

LoginLayout.propTypes = {
  children: PropTypes.node,
};

LoginLayout.defaultProps = {
};

export default withStyles(s)(LoginLayout);
