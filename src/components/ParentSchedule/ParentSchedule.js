import { compose } from 'recompose';
import React from 'react';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Grid from '@material-ui/core/Grid';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ParentSchedule.css';
import Schedules from '../Schedules';
import ParentSchedulesHeader from '../ParentSchedulesHeader';
import ParentScheduleTypeGroup from '../../util/const/ParentScheduleTypeGroup';

const propTypes = {
  schedules: PropTypes.arrayOf(
    PropTypes.shape({
      scheduleDate: PropTypes.oneOfType([PropTypes.string, PropTypes.instanceOf(Date)]),
      scheduleStartTime: PropTypes.string,
      scheduleEndTime: PropTypes.string,
      scheduleLocation: PropTypes.string,
    }),
  ),
  scheduleType: PropTypes.oneOf([
    ParentScheduleTypeGroup.SCHEDULE_TYPE_REGULAR,
    ParentScheduleTypeGroup.SCHEDULE_TYPE_UNPLANNED,
    ParentScheduleTypeGroup.SCHEDULE_TYPE_SHORT_TERM,
  ]),
  scheduleNegotiable: PropTypes.bool,
  onEdit: PropTypes.func,
};
const defaultProps = {
  onEdit: () => {},
  schedules: [],
  scheduleType: ParentScheduleTypeGroup.SCHEDULE_TYPE_REGULAR,
  scheduleNegotiable: false,
};

class ParentSchedule extends React.Component {
  onClickEdit = () => {
    return this.props.onEdit();
  };

  render() {
    return (
      <div className={s.root}>
        <ExpansionPanel>
          <ExpansionPanelSummary>
            <Typography variant={'body1'}>스케줄 정보</Typography>
          </ExpansionPanelSummary>

          <ExpansionPanelDetails style={{ paddingTop: '0', paddingBottom: '0' }}>
            <Grid container spacing={16}>
              <Grid item xs={12}>
                <ParentSchedulesHeader
                  schedules={this.props.schedules}
                  scheduleType={this.props.scheduleType}
                  scheduleNegotiable={this.props.scheduleNegotiable}
                />
              </Grid>
              <Grid item xs={12}>
                <Schedules schedules={this.props.schedules} scheduleType={this.props.scheduleType} />
              </Grid>
            </Grid>
          </ExpansionPanelDetails>

          <ExpansionPanelActions>
            <Button variant={'raised'} onClick={this.onClickEdit}>
              수정하기
            </Button>
          </ExpansionPanelActions>
        </ExpansionPanel>
      </div>
    );
  }
}

ParentSchedule.propTypes = propTypes;
ParentSchedule.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(ParentSchedule);
