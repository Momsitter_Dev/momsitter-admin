import styleProps from 'react-style-proptype';
import CircularProgress from 'material-ui/CircularProgress';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './SearchLocationPicker.css';
import SearchLocationList from '../SearchLocationList';
import Request from '../../util/Request';
import clientConfig from '../../config/clientConfig';

const propTypes = {
  defaultLocation: PropTypes.shape({
    main: PropTypes.string,
    sub: PropTypes.string,
    detail: PropTypes.string,
  }),
  onChange: PropTypes.func,
  style: styleProps,
  height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  totalShow: PropTypes.bool,
};
const defaultProps = {
  onChange: () => {},
  defaultLocation: null,
  height: null,
  totalShow: true,
};

class SearchLocationPicker extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      main: [],
      sub: [],
      detail: [],
      selected: {
        main: '',
        sub: '',
        detail: '',
      },
      pending: false,
    };
  }

  async componentDidMount() {
    try {
      const { defaultLocation } = this.props;
      const main = await this.getLocation();
      if (defaultLocation) {
        const sub = defaultLocation.main ? await this.getLocation({ main: defaultLocation.main }) : [];
        const detail = defaultLocation.sub
          ? await this.getLocation({ main: defaultLocation.main, sub: defaultLocation.sub })
          : [];
        this.setState({
          selected: defaultLocation,
          main,
          sub,
          detail,
          pending: false,
        });
      } else {
        this.setState({
          main,
          sub: [],
          detail: [],
          pending: false,
        });
      }
    } catch (err) {
      this.setState({
        pending: false,
      });
    }

    const { defaultLocation } = this.props;
    if (defaultLocation) {
      const temp = {};
      temp.sub = await this.getLocation({ main: defaultLocation.main });
      temp.detail = await this.getLocation({ main: defaultLocation.main, sub: defaultLocation.sub });
      this.setState({
        selected: {
          ...defaultLocation,
        },
        sub: temp.sub,
        detail: temp.detail,
      });
    }
  }

  async componentWillReceiveProps(props) {
    try {
      if (props.defaultLocation) {
        const temp = {};
        temp.sub = await this.getLocation({ main: props.defaultLocation.main });
        temp.detail = await this.getLocation({ main: props.defaultLocation.main, sub: props.defaultLocation.sub });
        this.setState({
          selected: {
            ...props.defaultLocation,
          },
          sub: temp.sub,
          detail: temp.detail,
        });
      }
    } catch (err) {
      console.log(err);
    }
  }

  onSelectItem = (dataKey, data) => {
    return this.setState(
      {
        selected: {
          ...data,
        },
      },
      async () => {
        const location = await this.getLocation({
          ...this.state.selected,
        });
        if (dataKey === 'main') {
          this.setState({
            sub: location,
            detail: [],
          });
        }
        if (dataKey === 'sub') {
          if (data[dataKey] === '전체') {
            this.setState({
              detail: [],
            });
            return this.props.onChange({
              siNm: this.state.selected.main,
              sggNm: '',
              emdNm: '',
              main: this.state.selected.main,
              sub: '전체',
              detail: '',
              locationLatitude: data.locationLatitude,
              locationLongitude: data.locationLongitude,
            });
          }
          return this.setState({
            detail: location,
          });
        }
        if (dataKey === 'detail') {
          if (data[dataKey] === '전체') {
            return this.props.onChange({
              siNm: this.state.selected.main,
              sggNm: this.state.selected.sub,
              emdNm: '',
              main: this.state.selected.main,
              sub: this.state.selected.sub,
              detail: '전체',
              locationLatitude: data.locationLatitude,
              locationLongitude: data.locationLongitude,
            });
          }
          return this.props.onChange({
            siNm: this.state.selected.main,
            sggNm: this.state.selected.sub,
            emdNm: this.state.selected.detail,
            main: this.state.selected.main,
            sub: this.state.selected.sub,
            detail: this.state.selected.detail,
            locationLatitude: data.locationLatitude,
            locationLongitude: data.locationLongitude,
          });
        }
      },
    );
  };

  getLocation = async (options = {}) => {
    try {
      const main = options.main ? encodeURIComponent(options.main) : '';
      const sub = options.sub ? encodeURIComponent(options.sub) : '';
      const detail = options.detail ? encodeURIComponent(options.detail) : '';

      const response = await Request(`${clientConfig.addressServer}?main=${main}&sub=${sub}&detail=${detail}`, {
        method: 'GET',
      });
      return response;
    } catch (err) {
      return null;
    }
  };

  renderContent = () => {
    const { pending } = this.state;
    if (pending) {
      return this.renderPendingUI();
    }
    return this.renderLocationList();
  };

  renderLocationList = () => {
    const { main, sub, detail, selected } = this.state;
    const ui = [];
    if (main) {
      ui.push(this.renderMainList());
    }
    if (sub) {
      ui.push(this.renderSubList());
    }
    if (detail && selected.sub !== '전체') {
      ui.push(this.renderDetailList());
    }
    return <div className={s.container}>{ui}</div>;
  };

  renderMainList = () => {
    const { height } = this.props;
    const { main, selected } = this.state;
    return (
      <SearchLocationList
        key={'search-location-list-main'}
        list={main}
        dataKey="main"
        onClickItem={this.onSelectItem}
        selectedItem={selected.main}
        height={height}
      />
    );
  };

  renderSubList = () => {
    const { height, totalShow } = this.props;
    const { sub, selected } = this.state;
    return (
      <SearchLocationList
        key={'search-location-list-sub'}
        list={sub.filter(item => {
          return item.sub;
        })}
        dataKey="sub"
        onClickItem={this.onSelectItem}
        selectedItem={selected.sub}
        height={height}
        total={selected}
        totalShow={totalShow}
      />
    );
  };

  renderDetailList = () => {
    const { height, totalShow } = this.props;
    const { detail, selected } = this.state;
    return (
      <SearchLocationList
        key={'search-location-list-detail'}
        list={detail.filter(item => {
          return item.detail;
        })}
        dataKey="detail"
        onClickItem={this.onSelectItem}
        selectedItem={selected.detail}
        height={height}
        total={selected}
        totalShow={totalShow}
      />
    );
  };

  renderPendingUI = () => {
    return (
      <div>
        <CircularProgress size={50} />
      </div>
    );
  };

  render() {
    const { style } = this.props;
    return (
      <div className={s.root} style={style}>
        {this.renderContent()}
      </div>
    );
  }
}

SearchLocationPicker.propTypes = propTypes;
SearchLocationPicker.defaultProps = defaultProps;

export default withStyles(s)(SearchLocationPicker);
