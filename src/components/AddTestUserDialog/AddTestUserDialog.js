import { toast } from 'react-toastify';
import { compose } from 'recompose';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import SyncIcon from '@material-ui/icons/Sync';
import Grid from '@material-ui/core/Grid';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './AddTestUserDialog.css';

import ResponseResultTypeGroup from '../../util/const/ResponseResultTypeGroup';
import Request from '../../util/Request';
import UserType from '../Filter/UserType';

const propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func,
};
const defaultProps = {
  open: false,
  onClose: () => {},
};

class AddTestUserDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userTypeId: '',
      saving: false,
    };
    this.userKeyInput = null;
    this.onClose = this.onClose.bind(this);
    this.onChangeUserTypeId = this.onChangeUserTypeId.bind(this);
    this.onSave = this.onSave.bind(this);
  }

  onChangeUserTypeId = (value) => {
    return this.setState({
      userTypeId: value,
    });
  };

  onClose = () => {
    return this.props.onClose();
  };

  onSave = async function () {
    const { userTypeId } = this.state;
    this.setState({ saving: true });

    const data = {
      userKey: this.userKeyInput.value,
      userTypeId,
    };
    try {
      const response = await Request('api/account/test', {
        method: 'POST',
        data,
      });
      if (response.result === ResponseResultTypeGroup.SUCCESS) {
        this.onClose();
        toast.success(`${data.userKey} 로 회원 정보를 생성하였습니다.`);
        this.setState({ saving: false });
        return true;
      }
      throw new Error('request fail');
    } catch (err) {
      toast.error(`${data.userKey}로 테스트 계정을 생성할 수 없습니다.`);
      this.setState({ saving: false });
      return false;
    }
  };

  renderSyncIcon = () => {
    const { saving } = this.state;
    if (saving) {
      return (
        <SyncIcon style={{ width: '15px', height: '15px' }}  className={s.rotating} />
      );
    }
    return null;
  };

  render() {
    const { open } = this.props;
    const { userTypeId, saving } = this.state;

    return (
      <div className={s.root}>
        <Dialog
          open={open}
          onClose={this.onClose}
        >
          <DialogTitle> 테스트 계정 생성 </DialogTitle>
          <DialogContent>
            <DialogContentText style={{ marginBottom: '15px' }}>
              로그인시 아래 입력하는 '로그인 ID' 비밀번호 1로 로그인 가능합니다.
            </DialogContentText>
            <Grid container spacing={16} alignItems={'flex-start'}>
              <Grid item xs={3}>
                <Typography align={'right'} variant={'caption'}> 로그인 ID </Typography>
              </Grid>
              <Grid item xs={9}>
                <TextField type={"text"} inputRef={(r) => { this.userKeyInput = r; }} />
              </Grid>

              <Grid item xs={3}>
                <Typography align={'right'} variant={'caption'}> 회원타입 </Typography>
              </Grid>
              <Grid item xs={9}>
                <UserType value={userTypeId} onChange={this.onChangeUserTypeId} />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button variant={'flat'} onClick={this.onClose}> 취소 </Button>
            <Button variant={'raised'} onClick={this.onSave} disabled={saving}>
              저장
              {this.renderSyncIcon()}
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

AddTestUserDialog.propTypes = propTypes;
AddTestUserDialog.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(AddTestUserDialog);
