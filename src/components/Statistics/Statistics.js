import { compose } from 'recompose';
import React from 'react';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Statistics.css';
import DailyJoinSitter from './DailyJoinSitter';
import DailySitterProfileStatus from './DailySitterProfileStatus';
import DailyParentProfileStatus from './DailyParentProfileStatus';
import DailyJoinUserHistory from './DailyJoinUserHistory';
import DailyApplyToParentHistory from './DailyApplyToParentHistory';
import DailyApplyToSitterHistory from './DailyApplyToSitterHistory';

class Statistics extends React.Component {
  render() {
    return (
      <div className={s.root}>
        <Grid container spacing={16}>
          <Grid item xs={12}>
            <DailyJoinUserHistory />
          </Grid>
          <Grid item xs={12}>
            <DailyApplyToParentHistory />
          </Grid>
          <Grid item xs={12}>
            <DailyApplyToSitterHistory />
          </Grid>
          <Grid item xs={4}>
            <DailyJoinSitter />
          </Grid>
          <Grid item xs={4}>
            <DailySitterProfileStatus />
          </Grid>
          <Grid item xs={4}>
            <DailyParentProfileStatus />
          </Grid>
        </Grid>
      </div>
    );
  }
}

Statistics.propTypes = {};
Statistics.defaultProps = {};

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(Statistics);
