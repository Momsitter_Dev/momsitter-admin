import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './DailyJoinSitter.css';
import ChartWidget from '../../ChartWidget';
import Request from '../../../util/Request';
import ResponseResultTypeGroup from '../../../util/const/ResponseResultTypeGroup';
import ReactHighcharts from 'react-highcharts';
import LoadingSpinner from '../../LoadingSpinner';

class DailyJoinSitter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      onLoading: true,
    };
  }

  async componentDidMount () {
    const data = await this.getData();
    this.setState({ data });
  };

  getData = () => {
    const self = this;
    return new Promise(async (resolve, reject) => {
      self.setState({
        onLoading: true,
      });
      const response = await Request(`api/statistics/dailyJoinSitter`, {
        method: 'GET',
      }).catch((err) => {
        console.log(err);
      });

      self.setState({
        onLoading: false,
      });
      if (response && response.result === ResponseResultTypeGroup.SUCCESS) {
        resolve(response.data);
      } else {
        reject('Fetch Fail');
      }
    });
  };

  renderChart = (onLoading, data) => {
    let ui = null;
    if (onLoading) {
      ui = (
        <LoadingSpinner />
      );
    } else {
      if (data && data.length > 0) {
        ui = (
          <ReactHighcharts
            config={{
              chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
              },
              tooltip: {
                pointFormat: '{series.name}: <b>{point.y}명</b>'
              },
              title: {
                text: '오늘 전체 회원 가입 현황',
              },
              plotOptions: {
                pie: {
                  allowPointSelect: true,
                  cursor: 'pointer',
                  dataLabels: {
                    enabled: false
                  },
                  showInLegend: true
                }
              },
              series: [{
                colorByPoint: true,
                name: '가입 회원 수',
                data,
              }]
            }}
          />
        );
      } else {
        ui = (
          <div>표시할 데이터가 없습니다.</div>
        );
      }
    }
    return ui;
  };

  render() {
    const { data, onLoading } = this.state;
    return (
      <ChartWidget>
        {this.renderChart(onLoading, data)}
      </ChartWidget>
    );
  }
}

DailyJoinSitter.propTypes = {};

DailyJoinSitter.defaultProps = {};

export default withStyles(s)(DailyJoinSitter);
