import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './DailyJoinUserHistory.css';
import ChartWidget from '../../ChartWidget';
import Request from '../../../util/Request';
import ResponseResultTypeGroup from '../../../util/const/ResponseResultTypeGroup';
import LoadingSpinner from '../../LoadingSpinner';
import ReactHighcharts from 'react-highcharts';
import UserTypeIdGroup from '../../../util/const/UserTypeIdGroup';

class DailyJoinUserHistory extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      onLoading: true,
    };
  }

  async componentDidMount () {
    const data = await this.getData();
    this.setState({ data });
  };

  getData = () => {
    const self = this;
    return new Promise(async (resolve, reject) => {
      self.setState({
        onLoading: true,
      });
      const response = await Request(`api/statistics/dailyJoinHistory`, {
        method: 'GET',
      }).catch((err) => {
        console.log(err);
      });

      self.setState({
        onLoading: false,
      });
      if (response && response.result === ResponseResultTypeGroup.SUCCESS) {
        resolve(response.data);
      } else {
        reject('Fetch Fail');
      }
    });
  };

  renderChart = (onLoading, data) => {
    let ui = null;
    if (onLoading) {
      ui = (
        <LoadingSpinner size={100} />
      );
    } else {
      if (data) {
        ui = (
          <ReactHighcharts
            config={{
              chart: {
                zoomType: 'x',
              },
              title: {
                text: '회원 타입별 가입 추이'
              },

              xAxis: {
                categories: data[UserTypeIdGroup.COLLEGE].map((item) => {
                  return item.perDate;
                })
              },

              yAxis: {
                title: {
                  text: '가입자 수'
                }
              },
              legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
              },

              plotOptions: {
                series: {
                  label: {
                    connectorAllowed: false
                  },
                }
              },

              series: [{
                name: '대학생 시터',
                data: data[UserTypeIdGroup.COLLEGE].map((item) => {
                  return item.cnt;
                }),
              }, {
                name: '선생님 시터',
                data: data[UserTypeIdGroup.TEACHER].map((item) => {
                  return item.cnt;
                }),
              }, {
                name: '부모 시터',
                data: data[UserTypeIdGroup.MOM].map((item) => {
                  return item.cnt;
                }),
              }, {
                name: '일반 시터',
                data: data[UserTypeIdGroup.NORMAL].map((item) => {
                  return item.cnt;
                }),
              }, {
                name: '부모 회원',
                data: data[UserTypeIdGroup.PARENT].map((item) => {
                  return item.cnt;
                }),
              }],

              responsive: {
                rules: [{
                  condition: {
                    maxWidth: 500
                  },
                  chartOptions: {
                    legend: {
                      layout: 'horizontal',
                      align: 'center',
                      verticalAlign: 'bottom'
                    }
                  }
                }]
              }
            }}
          />
        );
      } else {
        ui = (
          <div>표시할 데이터가 없습니다.</div>
        );
      }
    }
    return ui;
  };

  render() {
    const { data, onLoading } = this.state;
    return (
      <ChartWidget>
        {this.renderChart(onLoading, data)}
      </ChartWidget>
    );
  }
}

DailyJoinUserHistory.propTypes = {};

DailyJoinUserHistory.defaultProps = {};

export default withStyles(s)(DailyJoinUserHistory);
