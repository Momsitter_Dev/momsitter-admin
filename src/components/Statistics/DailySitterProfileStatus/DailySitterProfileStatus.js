import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import LoadingSpinner from '../../LoadingSpinner';
import ReactHighcharts from 'react-highcharts';
import s from './DailySitterProfileStatus.css';
import Request from '../../../util/Request';
import ResponseResultTypeGroup from '../../../util/const/ResponseResultTypeGroup';
import ChartWidget from '../../ChartWidget';

class DailySitterProfileStatus extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      onLoading: true,
    };
  }

  async componentDidMount () {
    const data = await this.getData();
    this.setState({ data });
  };

  getData = () => {
    const self = this;
    return new Promise(async (resolve, reject) => {
      self.setState({
        onLoading: true,
      });
      const response = await Request(`api/statistics/dailySitterProfileStatus`, {
        method: 'GET',
      }).catch((err) => {
        console.log(err);
      });

      self.setState({
        onLoading: false,
      });
      if (response && response.result === ResponseResultTypeGroup.SUCCESS) {
        resolve(response.data);
      } else {
        reject('Fetch Fail');
      }
    });
  };

  renderChart = (onLoading, data) => {
    let ui = null;
    if (onLoading) {
      ui = (
        <LoadingSpinner size={100} />
      );
    } else {
      if (data && data.length > 0) {
        ui = (
          <ReactHighcharts
            config={{
              chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
              },
              tooltip: {
                pointFormat: '{series.name}: <b>{point.y}명</b>'
              },
              title: {
                text: '시터 프로필 상태 현황',
              },
              plotOptions: {
                pie: {
                  allowPointSelect: true,
                  cursor: 'pointer',
                  dataLabels: {
                    enabled: false
                  },
                  showInLegend: true
                }
              },
              series: [{
                colorByPoint: true,
                name: '프로필 상태',
                data: data.filter((item) => {
                  if (item) {
                    return item;
                  }
                }),
              }]
            }}
          />
        );
      } else {
        ui = (
          <div>표시할 데이터가 없습니다.</div>
        );
      }
    }
    return ui;
  };

  render() {
    const { data, onLoading } = this.state;
    return (
      <ChartWidget>
        {this.renderChart(onLoading, data)}
      </ChartWidget>
    );
  }
}

DailySitterProfileStatus.propTypes = {};

DailySitterProfileStatus.defaultProps = {};

export default withStyles(s)(DailySitterProfileStatus);
