import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './DailyApplyToSitterHistory.css';
import ChartWidget from '../../ChartWidget';
import Request from '../../../util/Request';
import ResponseResultTypeGroup from '../../../util/const/ResponseResultTypeGroup';
import LoadingSpinner from '../../LoadingSpinner';
import ReactHighcharts from 'react-highcharts';
import ApplyStatusGroup from '../../../util/const/ApplyStatusGroup';

class DailyApplyToSitterHistory extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      onLoading: true,
    };
  }

  async componentDidMount () {
    const data = await this.getData();
    this.setState({ data });
  };

  getData = () => {
    const self = this;
    return new Promise(async (resolve, reject) => {
      self.setState({
        onLoading: true,
      });
      const response = await Request(`api/statistics/dailyApplyToSitterHistory`, {
        method: 'GET',
      }).catch((err) => {
        console.log(err);
      });

      self.setState({
        onLoading: false,
      });
      if (response && response.result === ResponseResultTypeGroup.SUCCESS) {
        resolve(response.data);
      } else {
        reject('Fetch Fail');
      }
    });
  };

  renderChart = (onLoading, data) => {
    let ui = null;
    if (onLoading) {
      ui = (
        <LoadingSpinner size={100} />
      );
    } else {
      if (data) {
        ui = (
          <ReactHighcharts
            config={{
              chart: {
                zoomType: 'x',
              },
              title: {
                text: '부모 -> 시터에게 신청 현황'
              },

              xAxis: {
                categories: data['total'].map((item) => {
                  return item.applyDate;
                })
              },

              yAxis: {
                title: {
                  text: '신청수'
                }
              },
              legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'middle'
              },

              plotOptions: {
                series: {
                  label: {
                    connectorAllowed: false
                  },
                }
              },

              series: [{
                name: '전체',
                data: data['total'].map((item) => {
                  return item.cnt;
                }),
              }, {
                name: '수락',
                data: data[ApplyStatusGroup.ACCEPT].map((item) => {
                  return item.cnt;
                }),
              }, {
                name: '거절',
                data: data[ApplyStatusGroup.DECLINE].map((item) => {
                  return item.cnt;
                }),
              }, {
                name: '요청',
                data: data[ApplyStatusGroup.REQUEST].map((item) => {
                  return item.cnt;
                }),
              }, {
                name: '조율',
                data: data[ApplyStatusGroup.TUNING].map((item) => {
                  return item.cnt;
                }),
              }],

              responsive: {
                rules: [{
                  condition: {
                    maxWidth: 500
                  },
                  chartOptions: {
                    legend: {
                      layout: 'horizontal',
                      align: 'center',
                      verticalAlign: 'bottom'
                    }
                  }
                }]
              }
            }}
          />
        );
      } else {
        ui = (
          <div>표시할 데이터가 없습니다.</div>
        );
      }
    }
    return ui;
  };

  render() {
    const { data, onLoading } = this.state;
    return (
      <ChartWidget>
        {this.renderChart(onLoading, data)}
      </ChartWidget>
    );
  }
}

DailyApplyToSitterHistory.propTypes = {};

DailyApplyToSitterHistory.defaultProps = {};

export default withStyles(s)(DailyApplyToSitterHistory);
