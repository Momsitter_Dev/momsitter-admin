/* eslint-disable no-undef */
/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import EnhancedButton from 'material-ui/internal/EnhancedButton';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ActivityItem.css';

const propTypes = {
  id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  label: PropTypes.string.isRequired,
  active: PropTypes.bool,
  iconY: PropTypes.string.isRequired,
  iconN: PropTypes.string.isRequired,
  onClickItem: PropTypes.func,
};
const defaultProps = {
  active: false,
  onClickItem: () => {},
};

const S3 = 'https://s3.ap-northeast-2.amazonaws.com/momsitter-service/momsitter-app/static/public/form/';

class ActivityItem extends React.Component {
  onClickItem = () => this.props.onClickItem(this.props.id);

  renderIcon = () => {
    if (this.props.active) {
      return (
        <div className={s.iconWrap}>
          <img alt={this.props.label} src={`${S3}${this.props.iconY}`} className={s.icon} />
          <img alt={'checked'} src={`${S3}check-icon.svg`} className={s.checkIcon} />
        </div>
      );
    }
    return (
      <div className={s.iconWrap}>
        <img alt={this.props.label} src={`${S3}${this.props.iconN}`} className={s.icon} />
      </div>
    );
  };

  renderLabel = () => {
    if (this.props.active) {
      return (
        <div style={{ color: '#e45600' }} className={s.label}>
          {' '}
          {this.props.label}{' '}
        </div>
      );
    }
    return <div className={s.label}> {this.props.label} </div>;
  };

  render() {
    return (
      <div className={s.root}>
        <EnhancedButton onClick={this.onClickItem} style={{ padding: '15px 0' }}>
          {this.renderIcon()}
          {this.renderLabel()}
        </EnhancedButton>
      </div>
    );
  }
}

ActivityItem.propTypes = propTypes;
ActivityItem.defaultProps = defaultProps;

export default withStyles(s)(ActivityItem);
