import RoomIcon from 'material-ui/svg-icons/action/room';
import styleProps from 'react-style-proptype';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './SearchLocationInput.css';
import SearchLocationPicker from '../SearchLocationPicker';

const propTypes = {
  inputStyle: styleProps,
  onChange: PropTypes.func,
  defaultPlace: PropTypes.object,
};
const defaultProps = {
  inputStyle: {},
  onChange: () => {},
  defaultPlace: null,
};

class SearchLocationInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show: false,
    };
    this.input = null;
  }

  componentDidMount() {
    try {
      if (this.input) {
        const { defaultPlace } = this.props;
        this.input.value = `${defaultPlace.main || ''} ${defaultPlace.sub || ''} ${defaultPlace.detail || ''}`;
      }
    } catch (err) {
      console.log(err);
    }
  }

  componentWillReceiveProps() {
    try {
      if (this.input) {
        const { defaultPlace } = this.props;
        this.input.value = `${defaultPlace.main || ''} ${defaultPlace.sub || ''} ${defaultPlace.detail || ''}`;
      }
    } catch (err) {
      console.log(err);
    }
  }

  /**
   * @param data.main
   * @param data.sub
   * @param data.detail
   */
  onSelectLocation = (data) => {
    return this.setState({
      show: false,
    }, () => {
      this.input.value = `${data.main} ${data.sub} ${data.detail}`;
      return this.props.onChange(data);
    });
  };

  onFocusInput = () => {
    return this.setState({ show: true });
  };

  onClickOverlay = (e) => {
    e.stopPropagation();
    return this.setState({ show: false });
  };

  render() {
    const { inputStyle, defaultPlace } = this.props;
    const { show } = this.state;
    return (
      <div className={s.root}>
        <RoomIcon
          style={{
            width: '20px',
            height: '20px',
            fill: '#ff5300',
            position: 'absolute',
            left: '10px',
            top: '10px',
          }}
        />
        <input
          type="text"
          onFocus={this.onFocusInput}
          style={{
            textIndent: '25px',
            ...inputStyle,
          }}
          readOnly
          ref={(r) => { this.input = r; }}
        />
        <div
          onClick={this.onClickOverlay}
          className={s.overlay}
          style={{
            display: show ? 'block' : 'none',
          }}
        />
        <div className={s.picker}>
          <SearchLocationPicker
            height="400px"
            style={{
              display: show ? 'block' : 'none',
            }}
            onChange={this.onSelectLocation}
            defaultLocation={defaultPlace}
          />
        </div>
      </div>
    );
  }
}

SearchLocationInput.propTypes = propTypes;
SearchLocationInput.defaultProps = defaultProps;

export default withStyles(s)(SearchLocationInput);
