/* eslint-disable no-undef */
/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@mfort.co.kr
 */

import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import FlatButton from 'material-ui/FlatButton';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './FormInterviewWayPicker.css';
import InterviewWayGroup from '../../util/const/InterviewWayGroup';

const propTypes = {
  style: stylePropType,
  // 디폴트 인터뷰 유형 선택 값
  defaultInterviewWay: PropTypes.oneOf([InterviewWayGroup.PHONE, InterviewWayGroup.FACE, InterviewWayGroup.DEMO]),
  onChange: PropTypes.func,
};
const defaultProps = {
  style: {},
  defaultInterviewWay: InterviewWayGroup.PHONE,
  onChange: () => {},
};

class FormInterviewWayPicker extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      interviewWay: props.defaultInterviewWay,
    };
  }

  handleChange = value => {
    this.setState(
      {
        interviewWay: value,
      },
      () => this.props.onChange(value),
    );
  };

  render() {
    const { style } = this.props;
    const { interviewWay } = this.state;
    return (
      <div className={s.root}>
        <div className={s.container} style={style}>
          <FlatButton
            className={interviewWay === InterviewWayGroup.PHONE ? s.buttonActive : s.buttonInactive}
            label={'전화 인터뷰'}
            onClick={() => this.handleChange(InterviewWayGroup.PHONE)}
          />
          <FlatButton
            className={interviewWay === InterviewWayGroup.FACE ? s.buttonActive : s.buttonInactive}
            label={
              <div className={s.labelBox}>
                대면 인터뷰 <br />
                <span className={s.subLabel}> (비용 현장지급) </span>
              </div>
            }
            onClick={() => this.handleChange(InterviewWayGroup.FACE)}
          />
          <FlatButton
            className={interviewWay === InterviewWayGroup.DEMO ? s.buttonActive : s.buttonInactive}
            label={
              <div className={s.labelBox}>
                시범채용 <br />
                <span className={s.subLabel}> (비용 현장지급) </span>
              </div>
            }
            onClick={() => this.handleChange(InterviewWayGroup.DEMO)}
          />
        </div>
      </div>
    );
  }
}

FormInterviewWayPicker.propTypes = propTypes;
FormInterviewWayPicker.defaultProps = defaultProps;

export default withStyles(s)(FormInterviewWayPicker);
