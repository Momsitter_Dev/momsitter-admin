import { compose } from 'recompose';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './NotificationDialog.css';

const propTypes = {
  open: PropTypes.bool.isRequired,
  title: PropTypes.string,
  onClose: PropTypes.func.isRequired,
  message: PropTypes.string,
};
const defaultProps = {
  title: '알림',
  message: '알림',
};

class NotificationDialog extends React.Component {
  onClose = () => {
    return this.props.onClose();
  };

  render() {
    const { open, title, message } = this.props;
    return (
      <Dialog open={open} onClose={this.onClose}>
        <DialogTitle> {title} </DialogTitle>
        <DialogContent>
          <Typography variant={'subheading'}> {message} </Typography>
        </DialogContent>
        <DialogActions>
          <Button variant={'raised'} onClick={this.onClose}>
            확인
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

NotificationDialog.propTypes = propTypes;
NotificationDialog.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(NotificationDialog);
