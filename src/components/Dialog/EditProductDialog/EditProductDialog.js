/* eslint-disable prettier/prettier */
import uuid from 'uuid/v4';

import { compose } from 'recompose';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import DatePicker from 'material-ui/DatePicker';

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './EditProductDialog.css';
import TextField from '../../TextField';
import InputField from '../../InputField';
import isParent from '../../../util/user/isParent';
import isSitter from '../../../util/user/isSitter';

const propTypes = {
  open: PropTypes.bool,
  title: PropTypes.string,
  closeDialog: PropTypes.func,
  data: PropTypes.object,
  onSubmit: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
};
const defaultProps = {
  open: false,
  title: '',
  data: {
    userId: '',
    productId: '',
    paymentId: '',
    purchaseDate: '',
    expireDate: '',
    memo: '',
    productStatus: '',
  },
};
/***
 * TODO: 상위 컴포넌트에서 이벤트를 관리하기때문에 코드가 번잡해지는 것 같음,
 * 상위 컴포넌트에서는 모달을 띄우고 닫는 역할만 하고 저장 + Ajax 요청은 해당 다이얼로그 컴포넌트에서 처리하는 식으로 개선 필요.
 */
class EditProductDialog extends React.Component {
  constructor(props) {
    super(props);
    this.onClickSubmit = this.onClickSubmit.bind(this);
  }

  getApplyCount = productUser => {
    const DEFAULT_TEXT = '없음';
    if (!productUser) {
      return DEFAULT_TEXT;
    }
    if (!productUser.userApplyCount) {
      return DEFAULT_TEXT;
    }
    if (!(productUser.userRole instanceof Array) && productUser.length === 0) {
      return DEFAULT_TEXT;
    }

    const userRole = productUser.userRole[0];
    if (isParent(userRole.userTypeId)) {
      return productUser.userApplyCount.applyToSitterCount;
    }

    if (isSitter(userRole.userTypeId)) {
      return productUser.userApplyCount.applyToParentCount;
    }

    return DEFAULT_TEXT;
  };

  onClickSubmit = () => {
    return this.props.onSubmit();
  };

  onChangeForm = (type, value) => {
    return this.props.onChange(type, value);
  };

  onCloseDialog = () => {
    return this.props.closeDialog();
  };

  render() {
    const {
      title,
      open,
    } = this.props;

    const {
      userId,
      productId,
      purchaseDate,
      expireDate,
      memo,
      applyCount,
      productUser,
    } = this.props.data;

    return (
      <Dialog
        title={title}
        open={open}
        onClose={this.onCloseDialog}
      >
        <DialogTitle> {title} </DialogTitle>
        <DialogContent>
          <Grid container={12} spacing={16}>
            <Grid item xs={3}>
              <Typography variant={'caption'}> 회원 번호 </Typography>
            </Grid>
            <Grid item xs={9}>
              <InputField
                id={uuid()}
                value={userId}
                onChange={(value) => this.onChangeForm('userId', value)}
              />
            </Grid>

            <Grid item xs={3}>
              <Typography variant={'caption'}> 이용권 번호 </Typography>
            </Grid>
            <Grid item xs={9}>
              <InputField
                id={uuid()}
                value={productId}
                onChange={(value) => this.onChangeForm('productId', value)}
              />
            </Grid>

            <Grid item xs={3}>
              <Typography variant={'caption'}> 이용권 시작 일자 </Typography>
            </Grid>
            <Grid item xs={9}>
              <DatePicker
                id={uuid()}
                value={purchaseDate || new Date()}
                onChange={(event, value) => this.onChangeForm('purchaseDate', value)}
              />
            </Grid>

            <Grid item xs={3}>
              <Typography variant={'caption'}> 이용권 만료 일자 </Typography>
            </Grid>
            <Grid item xs={9}>
              <DatePicker
                id={uuid()}
                value={expireDate || new Date()}
                onChange={(event, value) => this.onChangeForm('expireDate', value)}
              />
            </Grid>

            <Grid item xs={3}>
              <Typography variant={'caption'}> 이용권 메모 </Typography>
            </Grid>
            <Grid item xs={9}>
              <TextField
                id={uuid()}
                value={memo}
                onChange={(value) => this.onChangeForm('memo', value)}
              />
            </Grid>

            <Grid item xs={3}>
              <Typography variant={'caption'}> 당일 지원(신청) 가능 횟수 </Typography>
            </Grid>
            <Grid item xs={9}>
              <InputField
                id={uuid()}
                value={applyCount || this.getApplyCount(productUser)}
                onChange={(value) => this.onChangeForm('applyCount', value)}
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button variant={'flat'} onClick={this.onCloseDialog}> 취소 </Button>
          <Button variant={'raised'} onClick={this.onClickSubmit}> 저장 </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

EditProductDialog.propTypes = propTypes;
EditProductDialog.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(EditProductDialog);
