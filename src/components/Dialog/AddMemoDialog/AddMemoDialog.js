/* eslint-disable prettier/prettier */
import { compose } from 'recompose';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './AddMemoDialog.css';
import TextField from '../../TextField';

const propTypes = {
  open: PropTypes.bool,
  title: PropTypes.string,
  closeDialog: PropTypes.func,
  data: PropTypes.object,
  onCreateMemo: PropTypes.func.isRequired,
  onChangeNewMemo: PropTypes.func.isRequired,
};
const defaultProps = {
  open: false,
  title: '메모 추가',
  closeDialog: () => {},
  data: {},
};
class AddMemoDialog extends React.Component {
  constructor(props) {
    super(props);
    this.onClickSubmit = this.onClickSubmit.bind(this);
  }

  onClickSubmit = async function () {
    return this.props.onCreateMemo();
  };

  onChangeForm = (type, value) => {
    return this.props.onChangeNewMemo(type, value);
  };

  onCloseDialog = () => {
    return this.props.closeDialog();
  };

  render() {
    const {
      title,
      open,
    } = this.props;

    const {
      userId,
      memoType,
      memoClass,
      memoContent,
      memoReqMethod,
      memoStatus,
    } = this.props.data;

    return (
      <div>
        <Dialog
          open={open}
          onClose={this.onCloseDialog}
        >
          <DialogTitle> {title} </DialogTitle>
          <DialogContent>
            <DialogContentText style={{ marginBottom: '15px' }}>
              메모를 추가합니다.
            </DialogContentText>
            <Grid container>
              <Grid item xs={3}>
                <Typography variant={'caption'}> 회원 번호 </Typography>
              </Grid>
              <Grid item xs={9}>
                <TextField
                  value={userId}
                  onChange={(value) => this.onChangeForm('userId', value)}
                />
              </Grid>

              <Grid item xs={3}>
                <Typography variant={'caption'}> 메모 타입 </Typography>
              </Grid>
              <Grid item xs={9}>
                <Select
                  value={memoType}
                  onChange={(event) => this.onChangeForm('memoType', event.target.value)}
                >
                  <MenuItem value="문의"> 문의 </MenuItem>
                  <MenuItem value="요청"> 요청 </MenuItem>
                  <MenuItem value="불만"> 불만 </MenuItem>
                  <MenuItem value="기타"> 기타 </MenuItem>
                </Select>
              </Grid>

              <Grid item xs={3}>
                <Typography variant={'caption'}> 메모 상태 </Typography>
              </Grid>
              <Grid item xs={9}>
                <Select
                  value={memoStatus}
                  onChange={(event) => this.onChangeForm('memoStatus', event.target.value)}
                >
                  <MenuItem value="done"> 완료 </MenuItem>
                  <MenuItem value="new"> 신규 </MenuItem>
                </Select>
              </Grid>

              <Grid item xs={3}>
                <Typography variant={'caption'}> 메모 구분 </Typography>
              </Grid>
              <Grid item xs={9}>
                <Select
                  value={memoClass}
                  onChange={(event) => this.onChangeForm('memoClass', event.target.value)}
                >
                  <MenuItem value="req"> 요청 </MenuItem>
                  <MenuItem value="res"> 응답 </MenuItem>
                </Select>
              </Grid>

              <Grid item xs={3}>
                <Typography variant={'caption'}> 메모 요청 방법 </Typography>
              </Grid>
              <Grid item xs={9}>
                <Select
                  value={memoReqMethod}
                  onChange={(event) => this.onChangeForm('memoReqMethod', event.target.value)}
                >
                  <MenuItem value="call"> 전화 </MenuItem>
                </Select>
              </Grid>

              <Grid item xs={3}>
                <Typography variant={'caption'}> 메모 내용 </Typography>
              </Grid>
              <Grid item xs={9}>
                <TextField
                  id="inputMemoType"
                  value={memoContent}
                  onChange={(value) => this.onChangeForm('memoContent', value)}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button variant={'flat'} onClick={this.onCloseDialog}>
              취소
            </Button>
            <Button variant={'raised'} onClick={this.onClickSubmit}>
              저장
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

AddMemoDialog.propTypes = propTypes;
AddMemoDialog.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(AddMemoDialog);
