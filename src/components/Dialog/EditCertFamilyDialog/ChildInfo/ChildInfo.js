import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ChildInfo.css';
import uuid from 'uuid/v4';
import DatePicker from 'material-ui/DatePicker';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import getDateStrAsDate from '../../../../util/date/getDateStrAsDate';
import FlatButton from 'material-ui/FlatButton';

class ChildInfo extends React.Component {
  onChangeChildInfo = (type, value) => {
    const { generatedKey } = this.props.data;
    this.props.onChange(generatedKey, type, value);
  };

  onClickDeleteChildInfo = () => {
    const { generatedKey } = this.props.data;
    this.props.onDelete(generatedKey);
  };

  render() {
    const { birthday, gender } = this.props.data;
    return (
      <div className={s.root}>
        <div style={{ textAlign: 'right' }}>
          <FlatButton
            label="삭제"
            onClick={this.onClickDeleteChildInfo}
          />
        </div>
        <div className={s.item}>
          <label>성별</label>
          <SelectField
            value={gender}
            onChange={(event, index, value) => this.onChangeChildInfo('gender', value)}
            style={{ width: '100%' }}
          >
            <MenuItem value="women" primaryText="여자" />
            <MenuItem value="man" primaryText="남자" />
          </SelectField>
        </div>
        <div className={s.item}>
          <label>생년월일</label>
          <DatePicker
            id={uuid()}
            value={getDateStrAsDate(birthday) || null}
            onChange={(event, value) => this.onChangeChildInfo('birthday', value)}
            style={{ width: '100%' }}
          />
        </div>
      </div>
    );
  }
}

ChildInfo.propTypes = {
  data: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
};

ChildInfo.defaultProps = {};

export default withStyles(s)(ChildInfo);
