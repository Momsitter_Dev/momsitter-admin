/* eslint-disable prettier/prettier */
import _ from 'lodash';
import uuid from 'uuid/v4';

import { compose } from 'recompose';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import DatePicker from 'material-ui/DatePicker';

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './EditCertFamilyDialog.css';
import ChildInfo from './ChildInfo';
import getDateStrAsDate from '../../../util/date/getDateStrAsDate';

const propTypes = {
  open: PropTypes.bool.isRequired,
  data: PropTypes.object,
  onChange: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
  onLoading: PropTypes.bool.isRequired,
};
const defaultProps = {
  data: {},
};

class EditCertFamilyDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      childCount: 0,
      children: [],
      issueDate: '',
    };
    if (props && props.data) {
      this.state.children = props.data.children.map((item) => {
        return {
          ...item,
          generatedKey: uuid(),
        };
      });
      this.state.childCount = props.data.children.length;
      this.state.issueDate = props.data.issueDate;
    }
  }

  onClose = () => {
    return this.props.onClose();
  };

  onChange = (type, value) => {
    return this.props.onChange(type, value);
  };

  onSave = () => {
    return this.props.onSave();
  };

  onClickAddChild = () => {
    const { children } = this.state;
    children.push({
      generatedKey: uuid(),
      gender: '',
      birthday: null,
    });
    this.setState({
      childCount: children.length,
      children,
    });
    this.props.onChange('children', children);
    this.props.onChange('childCount', children.length);
  };

  onChangeChildInfo = (generatedKey, type, value) => {
    const { children } = this.state;
    const targetInd = _.findIndex(children, (o) => {
      return o.generatedKey === generatedKey;
    });
    if (targetInd !== -1) {
      children[targetInd][type] = value;
      this.setState({ children });

      this.props.onChange('children', children);
      this.props.onChange('childCount', children.length);
    }
  };

  onDeleteChildInfo = (generatedKey) => {
    const { children } = this.state;
    _.remove(children, (o) => {
      return o.generatedKey === generatedKey;
    });
    const childCount = children.length;
    this.setState({
      children,
      childCount,
    });

    this.props.onChange('children', children);
    this.props.onChange('childCount', childCount);
  };

  onChangeIssueDate = (value) => {
    this.setState({ issueDate: value });
    this.props.onChange('issueDate', value);
  };

  renderUI = () => {
    const { issueDate } = this.state;
    return (
      <div>
        <div>
          <label>발급일자</label>
          <DatePicker
            style={{ width: '100%' }}
            value={issueDate ? getDateStrAsDate(issueDate) : null}
            onChange={(event, value) => this.onChangeIssueDate(value)}
          />
        </div>
        <div>
          <label>아이정보</label>
          <Button
            variant={'raised'}
            onClick={this.onClickAddChild}
          > 아이정보 추가하기 </Button>
          <div>
            {this.renderChildrenInfo()}
          </div>
        </div>
      </div>
    );
  };

  renderChildrenInfo = () => {
    const { children } = this.state;
    if (children && children instanceof Array) {
        return children.map((item, index) => {
          return (
            <ChildInfo
              key={index}
              data={item}
              onChange={this.onChangeChildInfo}
              onDelete={this.onDeleteChildInfo}
            />
          )
        });
    }
    return null;
  };

  render() {
    const { data, open, onLoading } = this.props;
    return (
      <Dialog
        open={open}
        onClose={this.onClose}
      >
        <DialogTitle> 엄마(가족관계증명서) 인증정보 수정 </DialogTitle>
        <DialogContent>
          <div className={s.wrap}>{this.renderUI(onLoading, data)}</div>
        </DialogContent>
        <DialogActions>
          <Button variant={'flat'} onClick={this.onClose}> 취소 </Button>
          <Button variant={'raised'} onClick={this.onSave}> 저장 </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

EditCertFamilyDialog.propTypes = propTypes;
EditCertFamilyDialog.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(EditCertFamilyDialog);
