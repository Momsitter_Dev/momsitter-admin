/* eslint-disable prettier/prettier */
import uuid from 'uuid/v4';
import moment from 'moment';

import { compose } from 'recompose';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import DatePicker from 'material-ui/DatePicker';

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './EditCertEnrollmentDialog.css';
import UnivDegreeTypeGroup from '../../../util/const/UnivDegreeTypeGroup';
import UnivStatusTypeGroup from '../../../util/const/UnivStatusTypeGroup';
import InputField from '../../InputField';

moment.locale('ko');

const propTypes = {
  open: PropTypes.bool.isRequired,
  data: PropTypes.object,
  onChange: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
  onLoading: PropTypes.bool.isRequired,
};
const defaultProps = {
  data: {},
};

class EditCertEnrollmentDialog extends React.Component {
  onClose = () => {
    this.props.onClose();
  };

  onChange = (type, value) => {
    this.props.onChange(type, value);
  };

  onSave = () => {
    this.props.onSave();
  };

  getUnivNameValue = (parsedData) => {
    let value = null;
    if (parsedData.univ) {
      value = parsedData.univ;
    }
    if (parsedData.univName) {
      value = parsedData.univName;
    }
    return value;
  };

  getUnivGradeValue = (parsedData) => {
    let value = null;
    if (parsedData.univGrade) {
      // old version
      value = parsedData.univGrade;
    }
    if (parsedData.grade) {
      value = parsedData.grade;
    }
    return value;
  };

  getUnivStatusValue = (parsedData) => {
    let value = null;
    if (parsedData.univStatus) {
      // old version
      const degree = parsedData.univStatus;
      if (degree === "재학") {
        value = UnivStatusTypeGroup.ATTENDING;
      }
      if (degree === "휴학") {
        value = UnivStatusTypeGroup.LEAVE;
      }
      if (degree === "퇴학") {
        value = UnivStatusTypeGroup.WITHDRAW;
      }
      if (degree === "졸업") {
        value = UnivStatusTypeGroup.GRADUATE;
      }
    } else if (parsedData.status) {
      value = Number(parsedData.status);
    } else {
      value = null;
    }
    return value;
  };

  getUnivDegreeValue = (parsedData) => {
    let value = null;
    if (parsedData.univDegree) {
      // old version
      const degree = parsedData.univDegree;
      if (degree === "학사") {
        value = UnivDegreeTypeGroup.BACHELOR;
      }
      if (degree === "석사") {
        value = UnivDegreeTypeGroup.MASTER;
      }
      if (degree === "박사") {
        value = UnivDegreeTypeGroup.DOCTOR;
      }
    }
    if (parsedData.degree) {
      const degree = parsedData.degree;
      if (degree) {
        value = degree;
      }
    }
    return value;
  };

  getUnivStatusText = (status) => {
    status = Number(status);
    let text = '';
    if (status === UnivStatusTypeGroup.ATTENDING) {
      text = '재학';
    }
    if (status === UnivStatusTypeGroup.GRADUATE) {
      text = '졸업';
    }
    if (status === UnivStatusTypeGroup.LEAVE) {
      text = '휴학';
    }
    if (status === UnivStatusTypeGroup.WITHDRAW) {
      text = '퇴학';
    }
    return text;
  };

  getGraduateDateValue = (data) => {
    let value = '';
    if (data) {
      const { status, graduateDate } = data;
      if (status && status === UnivStatusTypeGroup.GRADUATE) {
        value = graduateDate;
      }
    }
    return value;
  };

  getExpelledDateValue = (data) => {
    const defaultValue = '';
    if (data && data.expelledDate) {
      return data.expelledDate;
    }
    return defaultValue;
  };

  getDateStrAsDate = (value) => {
    let result = null;
    if (value) {
      if (value instanceof Date) {
        result = value;
      } else {
        result = moment(value).toDate();
      }
    }
    return result;
  };

  renderLoadingUI = (onLoading) => {
    if (onLoading) {
      return (
        <CircularProgress size={25} />
      );
    }
    return null;
  };

  renderUI = (data) => {
    if (!data) {
      return null;
    }
    const {
      major,
      issueDate
    } = data;
    return (
      <div>
        <div className={s.dataRow}>
          <label>학교 이름</label>
          <InputField
            id={uuid()}
            style={{ width: '100%' }}
            onChange={(value) => this.onChange('univ', value)}
            value={this.getUnivNameValue(data)}
          />
        </div>
        <div className={s.dataRow}>
          <label>전공</label>
          <InputField
            id={uuid()}
            style={{ width: '100%' }}
            value={major}
            onChange={(value) => this.onChange('major', value)}
          />
        </div>
        <div className={s.dataRow}>
          <label> 학위 </label>
          <Select
            id={uuid()}
            value={this.getUnivDegreeValue(data)}
            onChange={(event) => this.onChange('degree', event.target.value)}
            style={{ width: '100%', textAlign: 'right' }}
            menuItemStyle={{ textAlign: 'right' }}
          >
            <MenuItem value={UnivDegreeTypeGroup.BACHELOR}> 학사 </MenuItem>
            <MenuItem value={UnivDegreeTypeGroup.MASTER}> 석사 </MenuItem>
            <MenuItem value={UnivDegreeTypeGroup.DOCTOR}> 박사 </MenuItem>
          </Select>
        </div>
        <div className={s.dataRow}>
          <label> 학년 </label>
          <Select
            id={uuid()}
            value={this.getUnivGradeValue(data)}
            onChange={(event) => this.onChange('grade', event.target.value)}
            style={{ width: '100%', textAlign: 'right' }}
            menuItemStyle={{ textAlign: 'right' }}
          >
            <MenuItem value={1}> 1학년 </MenuItem>
            <MenuItem value={2}> 2학년 </MenuItem>
            <MenuItem value={3}> 3학년 </MenuItem>
            <MenuItem value={4}> 4학년 </MenuItem>
          </Select>
        </div>
        <div className={s.dataRow}>
          <label>학사 상태</label>
          <Select
            id={uuid()}
            value={this.getUnivStatusValue(data)}
            onChange={(event) => this.onChange('status', event.target.value)}
            style={{ width: '100%', textAlign: 'right' }}
            menuItemStyle={{ textAlign: 'right' }}
          >
            <MenuItem value={UnivStatusTypeGroup.ATTENDING}> 재학 </MenuItem>
            <MenuItem value={UnivStatusTypeGroup.LEAVE}> 휴학 </MenuItem>
            <MenuItem value={UnivStatusTypeGroup.GRADUATE}> 졸업 </MenuItem>
            <MenuItem value={UnivStatusTypeGroup.WITHDRAW}> 퇴학 </MenuItem>
            <MenuItem value={UnivStatusTypeGroup.EXPELLED}> 제적 </MenuItem>
          </Select>
        </div>
        <div className={s.dataRow}>
          <label>발급일자</label>
          <DatePicker
            id={uuid()}
            value={this.getDateStrAsDate(issueDate)}
            onChange={(event, value) => this.onChange('issueDate', value)}
            style={{ width: '100%', textAlign: 'right' }}
          />
        </div>
        <div className={s.dataRow}>
          <label>졸업 일자</label>
          <DatePicker
            id={uuid()}
            value={this.getDateStrAsDate(this.getGraduateDateValue(data))}
            onChange={(event, value) => this.onChange('graduateDate', value)}
            style={{ width: '100%', textAlign: 'right' }}
          />
        </div>
        <div className={s.dataRow}>
          <label>제적 일자</label>
          <DatePicker
            id={uuid()}
            value={this.getDateStrAsDate(this.getExpelledDateValue(data))}
            onChange={(event, value) => this.onChange('expelledDate', value)}
            style={{ width: '100%', textAlign: 'right' }}
          />
        </div>
      </div>
    );
  };
  render() {
    const { data, open, onLoading } = this.props;
    return (
      <Dialog
        open={open}
        title="학교 인증정보 수정"
        onClose={this.onClose}
      >
        <DialogTitle> 학교 인증정보 수정 </DialogTitle>
        <DialogContent>
          {this.renderLoadingUI(onLoading)}
          {this.renderUI(data)}
        </DialogContent>
        <DialogActions>
          <Button variant={'flat'} onClick={this.onClose}> 취소 </Button>
          <Button variant={'raised'} onClick={this.onSave}> 저장 </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

EditCertEnrollmentDialog.propTypes = propTypes;
EditCertEnrollmentDialog.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(EditCertEnrollmentDialog);
