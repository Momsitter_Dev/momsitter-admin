/* eslint-disable prettier/prettier */
import uuid from 'uuid/v4';

import { compose } from 'recompose';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './AddTimerDialog.css';
import TextField from '../../TextField';
import TimerTaskRuleIdGroup from '../../../util/timer/constant/TimerTaskRuleIdGroup';

const propTypes = {
  open: PropTypes.bool.isRequired,
  data: PropTypes.object,
  onChange: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
};
const defaultProps = {
  data: {},
};

class AddTimerDialog extends React.Component {
  onSave = () => {
    this.props.onSave();
  };

  onChange = (type, value) => {
    this.props.onChange(type, value);
  };

  onClose = () => {
    this.props.onClose();
  };

  getTimerRuleId = (data) => {
    let result = '';
    if (data && data.timerRuleId) {
      result = data.timerRuleId;
    }
    return result;
  };

  getTimerTargetUserId = (data) => {
    let result = '';
    if (data && data.timerTargetUserId) {
      result = data.timerTargetUserId;
    }
    return result;
  };

  getReservedDate = (data) => {
    let result = '';
    if (data && data.reservedDate) {
      result = data.reservedDate;
    }
    return result;
  };

  render() {
    const {
      open,
      data,
    } = this.props;

    return (
      <Dialog
        open={open}
        onClose={this.onClose}
      >
        <DialogTitle> 예약메세지 추가 </DialogTitle>
        <DialogContent>
          <Grid container spacing={16}>
            <Grid item xs={3}>
              <Typography variant={'caption'}> 예약메세지 종류 </Typography>
            </Grid>
            <Grid item xs={9}>
              <Select
                value={this.getTimerRuleId(data)}
                onChange={(e) => this.onChange('timerRuleId', e.target.value)}
              >
                <MenuItem value={TimerTaskRuleIdGroup.ADMIN_PARENT_PRODUCT_EXTENSION}>
                  [관리자] 부모 회원 이용권 연장 처리 안내 문자
                </MenuItem>
                <MenuItem value={TimerTaskRuleIdGroup.ADMIN_SITTER_PROFILE_ERROR_FIRST}>
                  [관리자] 시터 프로필 오류
                </MenuItem>
                <MenuItem value={TimerTaskRuleIdGroup.ADMIN_SITTER_PROFILE_ERROR_SECOND}>
                  [관리자] 시터 프로필 오류 3일간 유지
                </MenuItem>
                <MenuItem value={TimerTaskRuleIdGroup.ADMIN_SITTER_PROFILE_ERROR_THIRD}>
                  [관리자] 프로필 오류 오류 7일간 유지
                </MenuItem>
                <MenuItem value={TimerTaskRuleIdGroup.ADMIN_PARENT_PROFILE_ERROR_FIRST}>
                  [관리자] 부모 프로필 오류
                </MenuItem>
                <MenuItem value={TimerTaskRuleIdGroup.ADMIN_PARENT_PROFILE_ERROR_SECOND}>
                  [관리자] 부모 프로필 오류 3일간 유지
                </MenuItem>
                <MenuItem value={TimerTaskRuleIdGroup.ADMIN_PARENT_PROFILE_ERROR_THIRD}>
                  [관리자] 부모 프로필 오류 7일간 유지
                </MenuItem>
                <MenuItem value={TimerTaskRuleIdGroup.ADMIN_USER_ISSUE_OUT}>
                  [관리자] 이슈 회원 탈퇴 처리
                </MenuItem>
                <MenuItem value={TimerTaskRuleIdGroup.ADMIN_USER_ISSUE_WARNING_FIRST}>
                  [관리자] 이슈 회원 발견 1차 경고
                </MenuItem>
                <MenuItem value={TimerTaskRuleIdGroup.ADMIN_USER_ISSUE_WARNING_SECOND}>
                  [관리자] 이슈 회원 발견 2차 경고
                </MenuItem>
                <MenuItem value={TimerTaskRuleIdGroup.ADMIN_USER_ISSUE_WARNING_THIRD}>
                  [관리자 이슈 회원 발견 3차 경고
                </MenuItem>
                <MenuItem value={TimerTaskRuleIdGroup.ADMIN_SITTER_CERT_DECLINE}>
                  [관리자] 시터 인증 반려
                </MenuItem>
                <MenuItem value={TimerTaskRuleIdGroup.ADMIN_SITTER_RESIDENT_REGISTRATION_CERT_COMPLETE_TARGET_STUDENT}>
                  [관리자] 시터 등초본 인증 완료 (대학생 시터)
                </MenuItem>
                <MenuItem value={TimerTaskRuleIdGroup.ADMIN_SITTER_RESIDENT_REGISTRATION_CERT_COMPLETE_TARGET_NORMAL}>
                  [관리자] 시터 등초본 인증 완료 (일반)
                </MenuItem>
                <MenuItem value={TimerTaskRuleIdGroup.ADMIN_SITTER_PERSONALITY_CERT_COMPLETE}>
                  [관리자] 시터 인성 인증 완료
                </MenuItem>
                <MenuItem value={TimerTaskRuleIdGroup.ADMIN_SITTER_MEDICAL_EXAMINATION_CERT_COMPLETE}>
                  [관리자] 시터 건강 인증 완료
                </MenuItem>
                <MenuItem value={TimerTaskRuleIdGroup.ADMIN_SITTER_CHILD_CARE_CERT_COMPLETE}>
                  [관리자] 시터 자격증 인증 완료
                </MenuItem>
                <MenuItem value={TimerTaskRuleIdGroup.ADMIN_SITTER_FAMILY_RELATION_CERT_COMPLETE}>
                  [관리자] 시터 가족관계증명서 인증 완료
                </MenuItem>
                <MenuItem value={TimerTaskRuleIdGroup.ADMIN_SITTER_ENROLLMENT_CERT_COMPLETE_TARGET_STUDENT}>
                  [관리자] 시터 학교 인증 완료 (대학생)
                </MenuItem>
                <MenuItem value={TimerTaskRuleIdGroup.ADMIN_SITTER_ENROLLMENT_CERT_COMPLETE_TARGET_NORMAL}>
                  [관리자] 시터 학교 인증 완료 (일반)
                </MenuItem>
              </Select>
            </Grid>

            <Grid item xs={3}>
              <Typography variant={'caption'}> 실행될 시간 </Typography>
            </Grid>
            <Grid item xs={9}>
              <TextField
                id={uuid()}
                value={this.getReservedDate(data)}
                onChange={(value) => this.onChange('reservedDate', value)}
              />
            </Grid>

            <Grid item xs={3}>
              <Typography variant={'caption'}> 타이머 대상 사용자 </Typography>
            </Grid>
            <Grid item xs={9}>
              <TextField
                id={uuid()}
                value={this.getTimerTargetUserId(data)}
                onChange={(value) => {this.onChange('timerTargetUserId', value)}}
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button variant={'flat'} onClick={this.onClose}>
            취소
          </Button>
          <Button variant={'raised'} onClick={this.onSave}>
            저장
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

AddTimerDialog.propTypes = propTypes;
AddTimerDialog.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(AddTimerDialog);
