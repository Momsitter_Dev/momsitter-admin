/* eslint-disable prettier/prettier */
import uuid from 'uuid/v4';

import { compose } from 'recompose';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './EditPasswordDialog.css';
import TextField from '../../TextField';
import LoadingSpinner from '../../LoadingSpinner';

const propTypes = {
  open: PropTypes.bool.isRequired,
  data: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
  onLoading: PropTypes.bool.isRequired,
};
const defaultProps = {
  data: '',
};

class EditPasswordDialog extends React.Component {
  onChange = (value) => {
    return this.props.onChange(value);
  };

  onClose = () => {
    return this.props.onClose();
  };

  onSave = () => {
    return this.props.onSave();
  };

  renderUI = (onLoading, data) => {
    if (onLoading) {
      return (
        <LoadingSpinner />
      );
    }
    return (
      <div className={s.row}>
        <label>비밀번호</label>
        <TextField
          id={uuid()}
          value={data}
          onChange={(value) => {this.onChange(value)}}
        />
      </div>
    );
  };

  render() {
    const { data, open, onLoading } = this.props;
    return (
      <Dialog
        open={open}
        onClose={this.onClose}
      >
        <DialogTitle> 비밀번호 수정 </DialogTitle>
        <DialogContent>
          <div className={s.wrap}> {this.renderUI(onLoading, data)} </div>
        </DialogContent>
        <DialogActions>
          <Button variant={'flat'} onClick={this.onClose}> 취소 </Button>
          <Button variant={'raised'} onClick={this.onSave}> 저장 </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

EditPasswordDialog.propTypes = propTypes;
EditPasswordDialog.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(EditPasswordDialog);
