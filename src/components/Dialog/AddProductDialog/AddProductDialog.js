import uuid from 'uuid/v4';
import { compose } from 'recompose';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContentText from '@material-ui/core/DialogContentText';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import DatePicker from 'material-ui/DatePicker';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './AddProductDialog.css';
import InputField from '../../InputField';
import TextField from '../../TextField';
import ProductIdSelect from '../../Filter/ProductIdSelect';

const propTypes = {
  open: PropTypes.bool,
  title: PropTypes.string,
  closeDialog: PropTypes.func,
  data: PropTypes.object,
  onCreate: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
};
const defaultProps = {
  open: false,
  title: '',
  closeDialog: () => {},
};

class AddProductDialog extends React.Component {
  onClickSubmit = () => {
    return this.props.onCreate();
  };

  onChangeForm = (type, value) => {
    return this.props.onChange(type, value);
  };

  onCloseDialog = () => {
    return this.props.closeDialog();
  };

  render() {
    const { open } = this.props;
    const {
      userId,
      productId,
      purchaseDate,
      expireDate,
      memo,
    } = this.props.data;

    return (
      <Dialog
        open={open}
      >
        <DialogTitle> 이용권 추가 </DialogTitle>
        <DialogContent>
          <DialogContentText style={{ marginBottom: '15px' }}> 회원에게 이용권 발급 </DialogContentText>
          <Grid container spacing={16}>
            <Grid item xs={3}>
              <Typography variant="subheading"> 회원 번호 </Typography>
            </Grid>
            <Grid item xs={9}>
              <InputField
                id={uuid()}
                value={userId || ''}
                onChange={(value) => this.onChangeForm('userId', value)}
              />
            </Grid>
          </Grid>
          <Grid container spacing={16}>
            <Grid item xs={3}>
              <Typography variant="subheading"> 이용권 번호 </Typography>
            </Grid>
            <Grid item xs={9}>
              <ProductIdSelect
                value={productId}
                onChange={(value) => this.onChangeForm('productId', value)}
              />
            </Grid>
          </Grid>
          <Grid container spacing={16}>
            <Grid item xs={3}>
              <Typography variant="subheading"> 이용권 시작 일자 </Typography>
            </Grid>
            <Grid item xs={9}>
              <DatePicker
                id={uuid()}
                value={purchaseDate}
                onChange={(event, value) => this.onChangeForm('purchaseDate', value)}
              />
            </Grid>
          </Grid>
          <Grid container spacing={16}>
            <Grid item xs={3}>
              <Typography variant="subheading"> 이용권 만료 일자 </Typography>
            </Grid>
            <Grid item xs={9}>
              <DatePicker
                id={uuid()}
                value={expireDate}
                onChange={(event, value) => this.onChangeForm('expireDate', value)}
              />
            </Grid>
          </Grid>
          <Grid container spacing={16}>
            <Grid item xs={3}>
              <Typography variant="subheading"> 이용권 메모 </Typography>
            </Grid>
            <Grid item xs={9}>
              <TextField
                id={uuid()}
                value={memo}
                onChange={(value) => this.onChangeForm('memo', value)}
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button
            variant={'flat'}
            onClick={this.onCloseDialog}
          >
            취소
          </Button>
          <Button
            variant={'raised'}
            color={'primary'}
            onClick={this.onClickSubmit}
          >
            저장
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

AddProductDialog.propTypes = propTypes;
AddProductDialog.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(AddProductDialog);
