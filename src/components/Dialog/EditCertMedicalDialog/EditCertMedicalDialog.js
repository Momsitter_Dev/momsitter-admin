import uuid from 'uuid/v4';

import { compose } from 'recompose';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import DatePicker from 'material-ui/DatePicker';

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './EditCertMedicalDialog.css';
import getDateStrAsDate from '../../../util/date/getDateStrAsDate';

const propTypes = {
  open: PropTypes.bool.isRequired,
  data: PropTypes.object,
  onChange: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
  onLoading: PropTypes.bool.isRequired,
};
const defaultProps = {
  data: {},
};

class EditCertMedicalDialog extends React.Component {
  onClose = () => {
    return this.props.onClose();
  };

  onChange = (type, value) => {
    return this.props.onChange(type, value);
  };

  onSave = () => {
    return this.props.onSave();
  };

  renderUI = () => {
    const { data } = this.props;
    if (data) {
      const { issueDate } = data;
      return (
        <div>
          <div className={s.dataRow}>
            <label> 검진일자 </label>
            <DatePicker
              id={uuid()}
              value={getDateStrAsDate(issueDate) || null}
              onChange={(event, value) => this.onChange('issueDate', value)}
              style={{ width: '100%' }}
            />
          </div>
        </div>
      );
    }
    return null;
  };

  render() {
    const { data, open, onLoading } = this.props;
    return (
      <Dialog
        open={open}
        onClose={this.onClose}
      >
        <DialogTitle> 건강 인증정보 수정 </DialogTitle>
        <DialogContent>
          <div className={s.wrap}>{this.renderUI(onLoading, data)}</div>
        </DialogContent>
        <DialogActions>
          <Button variant={'flat'} onClick={this.onClose}> 취소 </Button>
          <Button variant={'raised'} onClick={this.onSave}> 저장 </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

EditCertMedicalDialog.propTypes = propTypes;
EditCertMedicalDialog.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(EditCertMedicalDialog);
