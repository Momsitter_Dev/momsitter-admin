import { compose } from 'recompose';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Button from '@material-ui/core/Button';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import uuid from 'uuid/v4';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './RejectCertDialog.css';
import TextField from '../../TextField';

class RejectCertDialog extends React.Component {
  onSave = () => {
    this.props.onSave();
  };

  onClose = () => {
    this.props.onClose();
  };

  onChange = (value) => {
    this.props.onChange(value);
  };

  render() {
    const { title, data, open } = this.props;
    const { rejectMsg } = data;

    return (
      <Dialog
        open={open}
        title={title}
        onClose={this.onClose}
      >
        <DialogTitle> {title} </DialogTitle>
        <DialogContent>
          <div className={s.wrap}>
            <div className={s.row}>
              <label className={s.label}>거절 사유</label>
              <TextField
                id={uuid()}
                value={rejectMsg || ''}
                onChange={this.onChange}
              />
            </div>
          </div>
        </DialogContent>
        <DialogActions>
          <Button variant={"flat"} onClick={this.onClose}> 취소 </Button>
          <Button variant={"raised"} onClick={this.onSave}> 저장 </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

RejectCertDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  title: PropTypes.string,
  data: PropTypes.object,
  onClose: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
};

RejectCertDialog.defaultProps = {
  title: '인증 거절(반려) 사유 입력'
};

export default compose(
  withMaterialStyles(s, { withTheme: true }),
  withStyles(s),
)(RejectCertDialog);
