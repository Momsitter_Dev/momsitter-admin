/* eslint-disable prettier/prettier */
import uuid from 'uuid/v4';

import { compose } from 'recompose';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './AddUserDialog.css';
import TextField from '../../TextField';
import Request from '../../../util/Request';

const propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func,
};
const defaultProps = {
  open: false,
  onClose: () => {},
};

class AddUserDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: '',
      userPhone: '',
      userKey: '',
      userPassword: '',
      userBirthday: '',
      userConnectType: 'email',
      userGender: 'women',
      userStatus: 'active',
      contactWayDetail: 'admin',
      userRole: [],
    };
    this.onSave = this.onSave.bind(this);
  }

  onChangeForm = (type, value) => {
    const form = {};
    form[type] = value;
    return this.setState(form);
  };

  onClose = () => {
    return this.props.onClose();
  };

  onSave = async function () {
    try {
      const response = await Request('api/manage/user', {
        method: 'POST',
        data: this.state,
      });
      if (response && response.result === 'success') {
        console.log('SUCCESS');
      }
      throw new Error('request fail');
    } catch (err) {
      console.log(err);
    }
  };

  render() {
    const { open } = this.props;
    const {
      userName,
      userPhone,
      userKey,
      userPassword,
      userBirthday,
      userGender,
      userStatus,
      userRole
    } = this.state;

    return (
      <Dialog
        open={open}
        onClose={this.onClose}
      >
        <DialogTitle> 회원 추가 </DialogTitle>
        <DialogContent>
          <Grid container spacing={16}>
            <Grid item xs={3}>
              <Typography variant={'caption'}> 이름 </Typography>
            </Grid>
            <Grid item xs={9}>
              <TextField
                id={uuid()}
                value={userName}
                onChange={(value) => this.onChangeForm('userName', value)}
              />
            </Grid>

            <Grid item xs={3}>
              <Typography variant={'caption'}> 이메일 </Typography>
            </Grid>
            <Grid item xs={9}>
              <TextField
                id={uuid()}
                value={userKey}
                onChange={(value) => this.onChangeForm('userKey', value)}
              />
            </Grid>

            <Grid item xs={3}>
              <Typography variant={'caption'}> 패스워드 </Typography>
            </Grid>
            <Grid item xs={9}>
              <TextField
                id={uuid()}
                value={userPassword}
                onChange={(value) => this.onChangeForm('userPassword', value)}
              />
            </Grid>

            <Grid item xs={3}>
              <Typography variant={'caption'}> 생년월일 </Typography>
            </Grid>
            <Grid item xs={9}>
              <TextField
                id={uuid()}
                value={userBirthday}
                onChange={(value) => this.onChangeForm('userBirthday', value)}
              />
            </Grid>

            <Grid item xs={3}>
              <Typography variant={'caption'}> 핸드폰 번호 </Typography>
            </Grid>
            <Grid item xs={9}>
              <TextField
                id={uuid()}
                value={userPhone}
                onChange={(value) => this.onChangeForm('userPhone', value)}
              />
            </Grid>

            <Grid item xs={3}>
              <Typography variant={'caption'}> 성별 </Typography>
            </Grid>
            <Grid item xs={9}>
              <Select
                id={uuid()}
                value={userGender}
                onChange={(event) => this.onChangeForm('userGender', event.target.value)}
              >
                <MenuItem value="women"> 여자 </MenuItem>
                <MenuItem value="man"> 남자 </MenuItem>
              </Select>
            </Grid>

            <Grid item xs={3}>
              <Typography variant={'caption'}> 상태 </Typography>
            </Grid>
            <Grid item xs={9}>
              <Select
                id={uuid()}
                value={userStatus}
                onChange={(event) => this.onChangeForm('userStatus', event.target.value)}
              >
                <MenuItem value="active"> 활성 </MenuItem>
                <MenuItem value="inactive"> 비활성 </MenuItem>
                <MenuItem value="out"> 탈퇴 </MenuItem>
              </Select>
            </Grid>

            <Grid item xs={3}>
              <Typography variant={'caption'}> 권한 </Typography>
            </Grid>
            <Grid item xs={9}>
              <Select
                id={uuid()}
                value={userRole}
                multiple
                onChange={(event) => this.onChangeForm('userRole', event.target.value)}
              >
                <MenuItem value="1"> 시터 - 대학생 </MenuItem>
                <MenuItem value="2"> 시터 - 선생님 </MenuItem>
                <MenuItem value="3"> 시터 - 엄마 </MenuItem>
                <MenuItem value="4"> 시터 - 일반 </MenuItem>
                <MenuItem value="5"> 부모 </MenuItem>
              </Select>
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button variant={'flat'} onClick={this.onClose}> 취소 </Button>
          <Button variant={'raised'} onClick={this.onSave}> 저장 </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

AddUserDialog.propTypes = propTypes;
AddUserDialog.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(AddUserDialog);
