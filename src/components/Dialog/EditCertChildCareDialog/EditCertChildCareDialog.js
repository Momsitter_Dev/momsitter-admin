/* eslint-disable prettier/prettier */
import uuid from 'uuid/v4';
import { compose } from 'recompose';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import DatePicker from 'material-ui/DatePicker';

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './EditCertChildCareDialog.css';
import CertChildCareTypeGroup from '../../../util/const/CertChildCareTypeGroup';
import getDateStrAsDate from '../../../util/date/getDateStrAsDate';

const propTypes = {
  open: PropTypes.bool.isRequired,
  data: PropTypes.object,
  onChange: PropTypes.func.isRequired,
  onClose: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
  onLoading: PropTypes.bool.isRequired,
};
const defaultProps = {
  data: {},
};

class EditCertChildCareDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      careType: '',
      issueDate: '',
    };
    if (props && props.data) {
      this.state.careType = props.data.qualificationType;
      this.state.issueDate = props.data.issueDate;
    }
  }

  onClose = () => {
    return this.props.onClose();
  };
  onChange = (type, value) => {
    return this.props.onChange(type, value);
  };
  onSave = () => {
    return this.props.onSave();
  };

  onChangeCareType = (value) => {
    this.setState({ careType: value });
    return this.onChange('qualificationType', value);
  };

  onChangeIssueDate = (value) => {
    this.setState({ issueDate: value });
    return this.onChange('issueDate', value);
  };

  renderUI = () => {
    const { issueDate, careType } = this.state;
    return (
      <div>
        <Grid container spacing={16}>
          <Grid item xs={3}>
            <Typography variant={'caption'}> 자격증 종류 </Typography>
          </Grid>
          <Grid item xs={9}>
            <Select
              value={careType}
              style={{ width: '100%' }}
              onChange={(event) => this.onChangeCareType(event.target.value)}
            >
              {this.renderCertChildCareSelectItem()}
            </Select>
          </Grid>

          <Grid item xs={3}>
            <Typography variant={'caption'}> 발급 일자 </Typography>
          </Grid>
          <Grid item xs={9}>
            <DatePicker
              id={uuid()}
              value={getDateStrAsDate(issueDate) || null}
              style={{ width: '100%' }}
              onChange={(event, value) => this.onChangeIssueDate(value)}
            />
          </Grid>
        </Grid>
      </div>
    );
  };

  renderCertChildCareSelectItem = () => {
    const keys = Object.keys(CertChildCareTypeGroup);
    return keys.map((item, index) => {
      return (
        <MenuItem key={index} value={CertChildCareTypeGroup[item]}>
          {CertChildCareTypeGroup[item]}
        </MenuItem>
      )
    });
  };

  render() {
    const { data, open, onLoading } = this.props;
    return (
      <Dialog
        open={open}
        onClose={this.onClose}
      >
        <DialogTitle> 자격증 인증정보 수정 </DialogTitle>
        <DialogContent>
          <div className={s.wrap}>{this.renderUI(onLoading, data)}</div>
        </DialogContent>
        <DialogActions>
          <Button variant={'flat'} onClick={this.onClose}> 취소 </Button>
          <Button variant={'raised'} onClick={this.onSave}> 저장 </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

EditCertChildCareDialog.propTypes = propTypes;
EditCertChildCareDialog.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(EditCertChildCareDialog);
