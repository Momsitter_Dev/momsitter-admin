/* eslint-disable prettier/prettier */
import { compose } from 'recompose';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './MainCertLabelDialog.css';
import TextField from '../../TextField';

const propTypes = {
  open: PropTypes.bool.isRequired,
  onCloseMainCertLabelDialog: PropTypes.func.isRequired,
  onSaveMainCertLabelDialog: PropTypes.func.isRequired,
};
const defaultProps = {};

class MainCertLabelDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mainCertLabel: '',
    };
  }

  onChangeCertLabel = (value) => {
    this.setState({
      mainCertLabel: value,
    });
  };

  onSaveMainCertLabelDialog = () => {
    const { mainCertLabel } = this.state;
    this.props.onSaveMainCertLabelDialog(mainCertLabel);
  };

  render() {
    const { mainCertLabel } = this.state;
    const {
      open,
      onCloseMainCertLabelDialog,
    } = this.props;
    return (
      <Dialog
        open={open}
        onClose={onCloseMainCertLabelDialog}
      >
        <DialogTitle> 메인 인증 대표 문장 </DialogTitle>
        <DialogContent>
          <div className={s.formRow}>
            <label> 대표 문장 </label>
            <TextField
              value={mainCertLabel}
              onChange={this.onChangeCertLabel}
            />
          </div>
        </DialogContent>
        <DialogActions>
          <Button variant={'flat'} onClick={onCloseMainCertLabelDialog}> 취소 </Button>
          <Button variant={'raised'} onClick={this.onSaveMainCertLabelDialog}> 저장 </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

MainCertLabelDialog.propTypes = propTypes;
MainCertLabelDialog.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(MainCertLabelDialog);
