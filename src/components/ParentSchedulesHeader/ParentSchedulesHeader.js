import moment from 'moment';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ParentSchedulesHeader.css';
import ParentScheduleTypeGroup from '../../util/const/ParentScheduleTypeGroup';

moment.locale('ko');

const propTypes = {
  schedules: PropTypes.arrayOf(
    PropTypes.shape({
      scheduleId: PropTypes.number,
      scheduleTypeId: PropTypes.number,
      userId: PropTypes.number,
      scheduleDate: PropTypes.string,
      scheduleStartTime: PropTypes.string,
      scheduleEndTime: PropTypes.string,
      scheduleLocation: PropTypes.string,
    }),
  ),
  scheduleType: PropTypes.oneOf([
    ParentScheduleTypeGroup.SCHEDULE_TYPE_REGULAR,
    ParentScheduleTypeGroup.SCHEDULE_TYPE_UNPLANNED,
    ParentScheduleTypeGroup.SCHEDULE_TYPE_SHORT_TERM,
  ]),
  scheduleNegotiable: PropTypes.bool,
};
const defaultProps = {
  schedules: [],
  scheduleType: ParentScheduleTypeGroup.SCHEDULE_TYPE_REGULAR,
  scheduleNegotiable: false,
};

class ParentSchedulesHeader extends React.Component {
  getBackgroundColor = () => {
    const { scheduleType } = this.props;
    if (scheduleType === ParentScheduleTypeGroup.SCHEDULE_TYPE_REGULAR) {
      return '#e9f2f1';
    }
    if (scheduleType === ParentScheduleTypeGroup.SCHEDULE_TYPE_SHORT_TERM) {
      return '#e1f3ff';
    }
    if (scheduleType === ParentScheduleTypeGroup.SCHEDULE_TYPE_UNPLANNED) {
      return '#faf1fd';
    }
    return '#e9f2f1';
  };

  getScheduleStart = () => {
    const { schedules } = this.props;
    return moment.min(schedules.map((item) => {
      return moment(item);
    })).format('YYYY.MM.DD');
  };

  renderScheduleNegotiable = () => {
    const { scheduleNegotiable } = this.props;
    if (scheduleNegotiable) {
      return (
        <div style={{ marginTop: '10px' }}>
          <div> * 이 일정은 맘시터에 맞춰서 얼마든지 </div>
          <div style={{ marginLeft: '7px' }}> 조정할 수 있어요. </div>
        </div>
      );
    }
    return null;
  };

  renderScheduleInfo = () => {
    const { scheduleType } = this.props;
    if (scheduleType === ParentScheduleTypeGroup.SCHEDULE_TYPE_REGULAR) {
      return (
        <div>
          <div> * {this.getScheduleStart()}부터</div>
          <div style={{ marginLeft: '7px' }}> 정기적으로 돌봐주세요. </div>
        </div>
      );
    }

    if (scheduleType === ParentScheduleTypeGroup.SCHEDULE_TYPE_SHORT_TERM) {
      return (
        <div>
          <div> * 아래 표시된 날에만 돌봐주세요. </div>
        </div>
      );
    }

    if (scheduleType === ParentScheduleTypeGroup.SCHEDULE_TYPE_UNPLANNED) {
      return (
        <div>
          <div className={s.unplannedText} style={{ color: '#cda0d7' }}> 아직, 정확한 시간계획은 없어요. </div>
          <div className={s.unplannedText}> 아래 표시된 날짜 중에 맘시터를 </div>
          <div className={s.unplannedText}> 한번 만나보고 결정하고 싶어요. </div>
        </div>
      );
    }
    return null;
  };

  renderHeader = () => {
    const { schedules } = this.props;
    if (schedules.length === 0) {
      return this.renderEmpty();
    }
    return (
      <div className={s.header}>
        <div className={s.avatarWrap}>
          <img
            alt={'avatar'}
            src={'https://s3.ap-northeast-2.amazonaws.com/momsitter-service/momsitter-app/static/public/p-details-1-parentsimage.svg'}
            width={54}
            height={58}
          />
        </div>
        <div
          className={s.scheduleDescription}
          style={{ backgroundColor: this.getBackgroundColor() }}
        >
          <div className={s.arrowLeft} />
          {this.renderScheduleInfo()}
          {this.renderScheduleNegotiable()}
        </div>
      </div>
    );
  };

  renderEmpty = () => {
    return null;
  };

  render() {
    return (
      <div className={s.root}>
        {this.renderHeader()}
      </div>
    );
  }
}

ParentSchedulesHeader.propTypes = propTypes;
ParentSchedulesHeader.defaultProps = defaultProps;

export default withStyles(s)(ParentSchedulesHeader);
