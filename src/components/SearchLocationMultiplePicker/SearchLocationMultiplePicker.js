import styleProps from 'react-style-proptype';
import CircularProgress from 'material-ui/CircularProgress';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './SearchLocationMultiplePicker.css';
import Request from '../../util/Request';
import clientConfig from '../../config/clientConfig';
import SearchLocationMultipleList from '../SearchLocationMultipleList';
import SearchLocationPickerHeader from '../SearchLocationPickerHeader';

const propTypes = {
  selectedLocations: PropTypes.array,
  onChange: PropTypes.func,
  limit: PropTypes.number,
  header: PropTypes.bool,
  headerOrdered: PropTypes.bool,
  style: styleProps,
  wrapStyle: styleProps,
  totalShow: PropTypes.bool,
  currentSelected: PropTypes.object,
};
const defaultProps = {
  selectedLocations: [],
  onChange: () => {},
  limit: 3,
  header: false,
  headerOrdered: false,
  style: {},
  wrapStyle: {},
  totalShow: true,
  currentSelected: null,
};

const ADDRESS_TYPE = {
  MAIN: 'main',
  SUB: 'sub',
  DETAIL: 'detail',
};

class SearchLocationMultiplePicker extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pending: false,
      main: [],
      sub: [],
      detail: [],
      selected: [],
      currentlySelected: this.props.currentSelected,
    };
  }

  async componentDidMount() {
    try {
      const { selectedLocations } = this.props;
      const main = await this.getLocation();
      this.setState({
        main,
        sub: [],
        detail: [],
        pending: false,
        selected: selectedLocations,
      });
    } catch (err) {
      this.setState({
        pending: false,
      });
    }
  }

  async componentWillReceiveProps(nextProps) {
    try {
      const { selectedLocations } = nextProps;
      this.setState({
        selected: selectedLocations,
      });
    } catch (err) {
      this.setState({
        selected: [],
      });
    }
  }

  onDeleteItem = data => {
    const { selected } = this.state;
    if (!data.addressId) {
      const targetIndex = selected.findIndex(item => {
        return item.main === data.main && item.sub === data.sub && item.detail === data.detail;
      });
      if (targetIndex !== -1) {
        return this.onChange(
          selected.filter((item, index) => {
            return index !== targetIndex;
          }),
        );
      }
    }
    const targetIndex = selected.findIndex(item => {
      return data.addressId === item.addressId;
    });
    if (targetIndex === -1) {
      return this.onChange(
        selected.filter(item => {
          return data.main !== item.main && data.sub !== item.sub && data.detail !== item.detail;
        }),
      );
    }
    return this.onChange(
      selected.filter(item => {
        return item.addressId !== data.addressId;
      }),
    );
  };

  onChange = (data, additionalData) => {
    const { limit } = this.props;
    if (additionalData) {
      if (limit < data.length + 1) {
        alert(`최대 ${limit}개 선택가능합니다.`);
        return false;
      }
      return this.setState(
        {
          selected: [
            ...data,
            {
              ...additionalData,
            },
          ],
        },
        () => {
          this.props.onChange(this.state.selected);
        },
      );
    }
    return this.setState(
      {
        selected: data,
      },
      () => {
        this.props.onChange(this.state.selected);
      },
    );
  };

  onClickItem = async (type, data) => {
    const { currentlySelected } = this.state;
    if (type === ADDRESS_TYPE.MAIN) {
      const sub = await this.getLocation({
        main: data.main,
      });
      return this.setState({
        currentlySelected: {
          main: data.main,
          sub: '',
          detail: '',
          locationLatitude: data.locationLatitude,
          locationLongitude: data.locationLongitude,
        },
        sub,
        detail: [],
      });
    }
    if (type === ADDRESS_TYPE.SUB) {
      const detail = await this.getLocation({
        main: data.main,
        sub: data.sub,
      });
      return this.setState({
        detail,
        currentlySelected: {
          ...currentlySelected,
          sub: data.sub,
          detail: '',
          locationLatitude: data.locationLatitude,
          locationLongitude: data.locationLongitude,
        },
      });
    }
    if (type === ADDRESS_TYPE.DETAIL) {
      // TODO: 기존에 있는 주소인지 확인
      const { selected } = this.state;
      const alreadySelectedIndex = selected.findIndex(item => {
        return item.main === data.main && item.sub === data.sub && item.detail === data.detail;
      });
      if (alreadySelectedIndex !== -1) {
        // 기존 항목을 삭제한다.
        return this.onChange([
          ...selected.filter((item, index) => {
            return index !== alreadySelectedIndex;
          }),
        ]);
      } else {
        return this.onChange(selected, data);
      }
    }
    return false;
  };

  getLocation = async (options = {}) => {
    try {
      const main = options.main ? encodeURIComponent(options.main) : '';
      const sub = options.sub ? encodeURIComponent(options.sub) : '';
      const detail = options.detail ? encodeURIComponent(options.detail) : '';

      const response = await Request(`${clientConfig.addressServer}?main=${main}&sub=${sub}&detail=${detail}`, {
        method: 'GET',
      });
      return response;
    } catch (err) {
      return null;
    }
  };

  renderLocationList = type => {
    const { totalShow } = this.props;
    const { main, sub, detail, selected, currentlySelected } = this.state;
    if (type === ADDRESS_TYPE.MAIN) {
      return (
        <SearchLocationMultipleList
          addressType={type}
          list={main}
          onClickItem={this.onClickItem}
          selectedItems={selected}
          currentSelected={currentlySelected}
        />
      );
    }
    if (type === ADDRESS_TYPE.SUB) {
      return (
        <SearchLocationMultipleList
          addressType={type}
          list={sub}
          onClickItem={this.onClickItem}
          selectedItems={selected}
          currentSelected={currentlySelected}
        />
      );
    }
    if (type === ADDRESS_TYPE.DETAIL) {
      return (
        <SearchLocationMultipleList
          totalShow={totalShow}
          addressType={type}
          list={detail}
          onClickItem={this.onClickItem}
          selectedItems={selected}
          currentSelected={currentlySelected}
        />
      );
    }
    return null;
  };

  renderContent = () => {
    const { pending } = this.state;
    if (pending) {
      return (
        <div className={s.container}>
          <CircularProgress size={50} />
        </div>
      );
    }
    return (
      <div className={s.container}>
        {this.renderLocationList(ADDRESS_TYPE.MAIN)}
        {this.renderLocationList(ADDRESS_TYPE.SUB)}
        {this.renderLocationList(ADDRESS_TYPE.DETAIL)}
      </div>
    );
  };

  renderHeader = () => {
    const { header, headerOrdered } = this.props;
    const { selected } = this.state;
    if (!header) {
      return null;
    }
    return <SearchLocationPickerHeader onDelete={this.onDeleteItem} selected={selected} ordered={headerOrdered} />;
  };

  render() {
    const { style, wrapStyle } = this.props;
    return (
      <div className={s.root} style={style}>
        {this.renderHeader()}
        <div className={s.wrap} style={wrapStyle}>
          {this.renderContent()}
        </div>
      </div>
    );
  }
}

SearchLocationMultiplePicker.propTypes = propTypes;
SearchLocationMultiplePicker.defaultProps = defaultProps;

export default withStyles(s)(SearchLocationMultiplePicker);
