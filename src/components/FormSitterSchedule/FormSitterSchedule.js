import Moment from 'moment';
import { extendMoment } from 'moment-range';
import React from 'react';
import FlatButton from 'material-ui/FlatButton';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './FormSitterSchedule.css';
import TimePicker from '../TimePicker';
import DatePicker from '../DatePicker';
import getWeekdayCheck from '../../util/schdule/getWeekdayCheck';
import baseSunToBaseMonMoment from '../../util/schdule/baseSunToBaseMonMoment';

const moment = extendMoment(Moment);

moment.locale('ko');

const WEEK_DAYS = ['월요일', '화요일', '수요일', '목요일', '금요일', '토요일', '일요일'];

const MOMENT_WEEK_DAY_INDEX = {
  0: 6,
  1: 0,
  2: 1,
  3: 2,
  4: 3,
  5: 4,
  6: 5,
};

const styles = {
  datePickerStyle: {
    border: '1px solid #ffcfb8',
    borderRadius: 2,
    backgroundColor: '#fff0e8',
    width: '100%',
    height: 43,
  },
  datePickerInputStyle: {
    height: 43,
  },
  datePickerTextFieldStyle: {
    height: 43,
  },
  datePickerDateLabelStyle: {
    fontSize: '0.9375rem',
    fontWeight: 500,
    color: '#ff5300',
  },
  timePickerStyle: {
    width: 101,
    height: 37,
    border: '1px solid #ffcfb8',
    backgroundColor: '#fff0e8',
    marginTop: 6,
  },
  timePickerTimeStyle: {
    width: 101,
    height: 37,
  },
  timePickerLabelStyle: {
    fontSize: '0.9375rem',
    fontWeight: 500,
    color: '#ff5300',
    verticalAlign: 'top',
    padding: 0,
  },
  timePickerSelectFieldStyle: {
    width: 135,
    border: '1px solid #ffcfb8',
    backgroundColor: '#fff0e8',
    marginTop: 6,
  },
};
// 스케쥴 기간 옵션 리스트
const scheduleRangeOption = [
  {
    value: 7, // 1주일 이상
    label: '1주일',
  },
  {
    value: 30, // 1주일 이상
    label: '1개월',
  },
  {
    value: 90, // 1주일 이상
    label: '3개월',
  },
  {
    value: 180, // 1주일 이상
    label: '6개월',
  },
];

const propTypes = {
  schedules: PropTypes.arrayOf(
    PropTypes.shape({
      scheduleDate: PropTypes.instanceOf(Date),
      scheduleStartTime: PropTypes.string,
      scheduleEndTime: PropTypes.string,
      scheduleLocation: PropTypes.string,
    }),
  ),
  onChange: PropTypes.func,
};
const defaultProps = {
  schedules: [],
  onChange: () => {},
};

class FormSitterSchedule extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // Note. 시작 날짜
      scheduleStartDate: new Date(),
      // Note. 선택 요일
      scheduleWeekday: [
        false, // 월
        false, // 화
        false, // 수
        false, // 목
        false, // 금
        false, // 토
        false, // 일
      ],
      scheduleWeekDayTime: {
        0: { start: moment('12:00', 'HH:mm').toDate(), end: moment('22:00', 'HH:mm').toDate() },
        1: { start: moment('12:00', 'HH:mm').toDate(), end: moment('22:00', 'HH:mm').toDate() },
        2: { start: moment('12:00', 'HH:mm').toDate(), end: moment('22:00', 'HH:mm').toDate() },
        3: { start: moment('12:00', 'HH:mm').toDate(), end: moment('22:00', 'HH:mm').toDate() },
        4: { start: moment('12:00', 'HH:mm').toDate(), end: moment('22:00', 'HH:mm').toDate() },
        5: { start: moment('12:00', 'HH:mm').toDate(), end: moment('22:00', 'HH:mm').toDate() },
        6: { start: moment('12:00', 'HH:mm').toDate(), end: moment('22:00', 'HH:mm').toDate() },
      },

      // Note. 스케쥴의 시작시간, 종료시간
      scheduleStartTime: moment('12:00', 'HH:mm').toDate(),
      scheduleEndTime: moment('22:00', 'HH:mm').toDate(),

      // Note. 스케쥴의 기간
      scheduleRange: scheduleRangeOption[scheduleRangeOption.length - 1].value, // 디폴트 6개월
    };
  }

  async componentDidMount() {
    try {
      const { schedules } = this.props;

      // eslint-disable-next-line react/no-did-mount-set-state
      return this.setState(
        {
          schedules,
          scheduleStartDate: moment
            .min(
              schedules.map(item => {
                return moment(item.scheduleDate);
              }),
            )
            .toDate(),
          scheduleWeekday: getWeekdayCheck(schedules),
          scheduleWeekDayTime: this.getWeekdayTimeRange(schedules),
        },
        () => this.onChange(),
      );
    } catch (err) {
      console.error(err);
      // eslint-disable-next-line react/no-did-mount-set-state
    }
  }

  onChange = () => {
    const { schedules } = this.getScheduleData();
    return this.props.onChange(schedules);
  };

  onChangeDateTime = (index, type, value) => {
    const { scheduleWeekDayTime } = this.state;
    const datetime = {};
    datetime[index] = {
      start: null,
      end: null,
      ...scheduleWeekDayTime[index],
    };
    datetime[index][type] = value;
    return this.setState(
      {
        scheduleWeekDayTime: {
          ...scheduleWeekDayTime,
          ...datetime,
        },
      },
      () => this.onChange(),
    );
  };

  onChangeTime = (type, value) => {
    // TODO: 요일별 시간 변경
    const { scheduleStartTime, scheduleEndTime } = this.state;

    if (type === 'start') {
      return this.setState(
        {
          scheduleStartTime: value,
          scheduleWeekDayTime: {
            0: { start: value, end: scheduleEndTime },
            1: { start: value, end: scheduleEndTime },
            2: { start: value, end: scheduleEndTime },
            3: { start: value, end: scheduleEndTime },
            4: { start: value, end: scheduleEndTime },
            5: { start: value, end: scheduleEndTime },
            6: { start: value, end: scheduleEndTime },
          },
        },
        () => this.onChange(),
      );
    } else {
      return this.setState(
        {
          scheduleEndTime: value,
          scheduleWeekDayTime: {
            0: { start: scheduleStartTime, end: value },
            1: { start: scheduleStartTime, end: value },
            2: { start: scheduleStartTime, end: value },
            3: { start: scheduleStartTime, end: value },
            4: { start: scheduleStartTime, end: value },
            5: { start: scheduleStartTime, end: value },
            6: { start: scheduleStartTime, end: value },
          },
        },
        () => this.onChange(),
      );
    }
  };

  // Note. 요일중에 하나라도 선택이 되었는지 확인하는 함수
  isAnyScheduleWeekdaySelected = () => this.state.scheduleWeekday.find(item => item === true);

  getScheduleData = () => {
    // TODO: 현재 form 이 valid 한지 검사.
    const { scheduleStartDate, scheduleWeekDayTime, scheduleWeekday, scheduleRange } = this.state;
    const DATE_RANGE = moment.range(moment(scheduleStartDate), moment(scheduleStartDate).add(scheduleRange, 'days'));
    const schedules = Array.from(DATE_RANGE.by('day'));
    const SELECTED_DATE = WEEK_DAYS.reduce((acc, item, index) => {
      acc[item] = !!scheduleWeekday[index];
      return acc;
    }, {});
    const filteredDate = schedules
      .filter(item => {
        return SELECTED_DATE[item.format('dddd')];
      })
      .map(item => {
        const target = scheduleWeekDayTime[MOMENT_WEEK_DAY_INDEX[item.format('e')]];
        const { start, end } = target;
        return {
          scheduleDate: item.format('YYYY-MM-DD 00:00:00'),
          scheduleStartTime: moment(start).format('HH:mm'),
          scheduleEndTime: moment(end).format('HH:mm'),
        };
      });
    return {
      schedules: filteredDate,
    };
  };

  // Note. 활동 시작일 값이 변경되었을 때의 핸들링 함수
  handleScheduleStartDateChange = value => {
    this.setState(
      {
        scheduleStartDate: value.toDate(),
      },
      () => this.onChange(),
    );
  };

  // Note. 활동 요일이 선택되었을 때
  handleWeekdayClick = weekdayIndex => {
    const { scheduleWeekday } = this.state;
    scheduleWeekday[weekdayIndex] = !scheduleWeekday[weekdayIndex];
    this.setState(
      {
        scheduleWeekday,
      },
      () => this.onChange(),
    );
  };

  getWeekdayTimeRange = schedules => {
    const { scheduleWeekDayTime } = this.state;
    const weekdayParsed = schedules.reduce((acc, item) => {
      const current = moment(item.scheduleDate);
      const currentWeek = baseSunToBaseMonMoment(current);
      if (!acc[currentWeek]) {
        acc[currentWeek] = {
          start: moment(item.scheduleStartTime, 'HH:mm').toDate(),
          end: moment(item.scheduleEndTime, 'HH:mm').toDate(),
        };
      }
      return acc;
    }, {});

    return {
      ...scheduleWeekDayTime,
      ...weekdayParsed,
    };
  };

  // Note. 정기적 스케쥴 입력에서 돌봄 요일 선택 UI
  renderWeekdayCheckbox = () => {
    const { scheduleWeekday } = this.state;
    const label = ['월', '화', '수', '목', '금', '토', '일'];
    return (
      <div className={s.wrapWeekday}>
        {label.map((item, index) => (
          <FlatButton
            key={`week-day-label-${item}`}
            label={item}
            className={scheduleWeekday[index] ? s.buttonWeekdayOn : s.buttonWeekday}
            onClick={() => this.handleWeekdayClick(index)}
          />
        ))}
      </div>
    );
  };

  // Note. 시작시간 종료시간을 선택할 수 있는 UI 를 렌더링하는 함수
  renderTimeRange = () => {
    const { scheduleStartTime, scheduleEndTime } = this.state;
    return (
      <div className={s.wrapTimeRange}>
        <div>
          <h5 className={s.label} style={{ textAlign: 'center' }}>
            시작시간
          </h5>
          <TimePicker
            time={scheduleStartTime}
            style={styles.timePickerStyle}
            timeStyle={styles.timePickerTimeStyle}
            timeLabelStyle={styles.timePickerLabelStyle}
            selectFieldStyle={styles.timePickerSelectFieldStyle}
            onChange={value => this.onChangeTime('start', value.toDate())}
          />
        </div>
        <div className={s.rangeDivider} />
        <div>
          <h5 className={s.label} style={{ textAlign: 'center' }}>
            종료시간
          </h5>
          <TimePicker
            time={scheduleEndTime}
            style={styles.timePickerStyle}
            timeStyle={styles.timePickerTimeStyle}
            timeLabelStyle={styles.timePickerLabelStyle}
            selectFieldStyle={styles.timePickerSelectFieldStyle}
            onChange={value => this.onChangeTime('end', value.toDate())}
          />
        </div>
      </div>
    );
  };

  // Note. 일정 기간을 선택할 수 있는 UI 를 렌더링하는 함수
  renderScheduleRange = () => {
    const { scheduleRange } = this.state;
    return (
      <div className={s.wrapScheduleRangeButton}>
        {scheduleRangeOption.map((item, index) => (
          <FlatButton
            key={`schedule-range-option-${index}`}
            label={`${item.label} 이상`}
            className={scheduleRange === item.value ? s.buttonScheduleRangeActive : s.buttonScheduleRange}
            onClick={() => this.setState({ scheduleRange: item.value }, () => this.onChange())}
          />
        ))}
      </div>
    );
  };

  renderDateTimeRange = () => {
    const { scheduleWeekday, scheduleWeekDayTime } = this.state;
    return scheduleWeekday.map((item, index) => {
      if (!item) {
        return null;
      }
      return (
        <div key={`date-time-item-${index}`} className={s.dateTimeItem}>
          <div className={s.dateTimeLabel}>
            <div
              style={{
                width: '7px',
                height: '7px',
                borderRadius: '100px',
                backgroundColor: '#ff7928',
                marginRight: '15px',
              }}
            />
            <div
              style={{
                fontSize: '0.9rem',
                fontWeight: '400',
                color: '#646464',
              }}
            >
              {['월', '화', '수', '목', '금', '토', '일'][index]}
            </div>
          </div>
          <div>
            <TimePicker
              time={scheduleWeekDayTime[index].start}
              maxTime={moment(scheduleWeekDayTime[index].end).subtract(30, 'minutes')}
              onChange={value => this.onChangeDateTime(index, 'start', value)}
              style={{
                backgroundColor: '#fff0e8',
                border: '1px solid #ffcfb8',
                borderRadius: '2px',
                height: '38px',
              }}
              timeLabelStyle={{
                color: '#ff5300',
                fontSize: '0.9rem',
                fontWeight: '500',
              }}
            />
          </div>
          <div> ~ </div>
          <div>
            <TimePicker
              time={scheduleWeekDayTime[index].end}
              minTime={moment(scheduleWeekDayTime[index].start).add(30, 'minutes')}
              onChange={value => this.onChangeDateTime(index, 'end', value)}
              style={{
                backgroundColor: '#fff0e8',
                border: '1px solid #ffcfb8',
                borderRadius: '2px',
                height: '38px',
              }}
              timeLabelStyle={{
                color: '#ff5300',
                fontSize: '0.9rem',
                fontWeight: '500',
              }}
            />
          </div>
        </div>
      );
    });
  };

  renderContent = () => {
    const { scheduleStartDate, scheduleRange } = this.state;

    return (
      <div>
        <div className={s.stepStyle}>
          <div>
            <h5 className={s.label}>활동 시작일</h5>
            <DatePicker
              date={scheduleStartDate}
              minDate={new Date()}
              maxDate={moment()
                .add(90, 'days')
                .toDate()}
              style={styles.datePickerStyle}
              inputStyle={styles.datePickerInputStyle}
              textFieldStyle={styles.datePickerTextFieldStyle}
              dateLabelStyle={styles.datePickerDateLabelStyle}
              onChange={this.handleScheduleStartDateChange}
            />
          </div>
          <hr className={s.divider} />

          <div>{this.renderTimeRange()}</div>
          <hr className={s.divider} />

          <div>
            <h5 className={s.label}>활동 요일</h5>
            <div style={{ marginBottom: '30px' }}>{this.renderWeekdayCheckbox()}</div>
            <div>{this.renderDateTimeRange()}</div>
          </div>
          <hr className={s.divider} />

          <div>
            <h5 className={s.label}>이 일정으로 얼마동안 일할 수 있나요?</h5>
            {this.renderScheduleRange()}
            <p className={s.guideInactivate}>
              활동 시작일로 부터&nbsp;
              {scheduleRangeOption.find(item => item.value === scheduleRange).label}
              &nbsp; 후, 내 프로필이 비공개로 변경됩니다.
            </p>
          </div>
        </div>
      </div>
    );
  };

  render() {
    return <div>{this.renderContent()}</div>;
  }
}

FormSitterSchedule.propTypes = propTypes;
FormSitterSchedule.defaultProps = defaultProps;

export default withStyles(s)(FormSitterSchedule);
