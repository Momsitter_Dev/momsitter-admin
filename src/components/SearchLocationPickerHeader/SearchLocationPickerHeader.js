import IconButton from 'material-ui/IconButton';
import CloseIcon from 'material-ui/svg-icons/navigation/close';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './SearchLocationPickerHeader.css';

const propTypes = {
  ordered: PropTypes.bool,
  selected: PropTypes.array,
  onDelete: PropTypes.func,
};
const defaultProps = {
  ordered: false,
  selected: [],
  onDelete: () => {},
};

class SearchLocationPickerHeader extends React.Component {
  onClickDelete = item => {
    return this.props.onDelete(item);
  };

  getItemLabel = item => {
    if (item.detail === '전체' || item.detail === '') {
      return `${item.sub} 전체`;
    }
    return item.detail;
  };

  renderOrderedSelectedItems = () => {
    const { selected } = this.props;
    if (!selected) {
      return null;
    }
    return selected.map((item, index) => {
      return (
        <div className={s.item} key={`search-location-picker-header-item-${index}`} style={{ height: '30px' }}>
          <div className={s.itemLabel} style={{ paddingLeft: '5px' }}>
            {' '}
            {`${index + 1}`}
            순위 {this.getItemLabel(item)}{' '}
          </div>
          <div>
            <IconButton
              onClick={() => this.onClickDelete(item)}
              iconStyle={{ width: '25px', height: '25px', fill: '#ff4500' }}
              style={{ width: '30px', height: '30px', padding: 0 }}
            >
              <CloseIcon />
            </IconButton>
          </div>
        </div>
      );
    });
  };

  renderSelectedItems = () => {
    const { selected } = this.props;
    if (!selected) {
      return null;
    }
    return selected.map((item, index) => {
      return (
        <div className={s.item} key={`search-location-picker-header-item-${index}`}>
          <div className={s.itemId}> {index + 1} </div>
          <div className={s.itemLabel}> {this.getItemLabel(item)} </div>
          <div>
            <IconButton
              onClick={() => this.onClickDelete(item)}
              iconStyle={{ width: '25px', height: '25px', fill: '#ff4500' }}
              style={{ width: '30px', height: '30px', padding: 0 }}
            >
              <CloseIcon />
            </IconButton>
          </div>
        </div>
      );
    });
  };

  renderContent = () => {
    const { selected, ordered } = this.props;
    if (!selected) {
      return null;
    }
    if (ordered) {
      return this.renderOrderedSelectedItems();
    }
    return this.renderSelectedItems();
  };

  render() {
    return (
      <div className={s.root}>
        <div className={s.itemWrap}>{this.renderContent()}</div>
        <div className={s.header}>
          <div className={s.headerItem}> 시/도 </div>
          <div className={s.headerItem}> 시/군/구 </div>
          <div className={s.headerItem}> 동/읍/면 </div>
        </div>
      </div>
    );
  }
}

SearchLocationPickerHeader.propTypes = propTypes;
SearchLocationPickerHeader.defaultProps = defaultProps;

export default withStyles(s)(SearchLocationPickerHeader);
