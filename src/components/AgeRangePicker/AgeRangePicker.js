/* eslint-disable no-undef */
/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@mfort.co.kr
 */

import React from 'react';
import PropTypes from 'prop-types';
import stylePropType from 'react-style-proptype';
import { Range } from 'rc-slider';
/* eslint-disable import/no-unresolved, import/no-webpack-loader-syntax */
import SliderStyle from '!!isomorphic-style-loader!css-loader?modules=false!rc-slider/dist/rc-slider.min.css';
import RangeStyle from '!!isomorphic-style-loader!css-loader?modules=false!./RangeStyle.css';
/* eslint-enable import/no-unresolved, import/no-webpack-loader-syntax */
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './AgeRangePicker.css';

// Note. 설정가능한 최소/최대 나이
const MIN_LIMIT = 20;
const MAX_LIMIT = 60;
// Note. 설정가능한 나이 단위
const STEP = 10;

const propTypes = {
  style: stylePropType,
  // 최소 연령대 디폴트 값
  defaultMin: PropTypes.number,
  // 최대 연령대 디폴트 값
  defaultMax: PropTypes.number,
  onChange: PropTypes.func,
};
const defaultProps = {
  style: {},
  defaultMin: 30,
  defaultMax: 40,
  onChange: () => {},
};

class AgeRangePicker extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      min: props.defaultMin,
      max: props.defaultMax,
    };
  }

  handleAgeRangeChange = value => {
    if (value) {
      this.setState(
        {
          min: value[0],
          max: value[1],
        },
        () => this.props.onChange(value),
      );
    }
  };

  renderLabel = () => {
    const render = [];
    for (let i = MIN_LIMIT; i <= MAX_LIMIT; i += 10) {
      render.push(
        <div className={s.label} key={`ageRange-${i}`}>
          {' '}
          {i}대{' '}
        </div>,
      );
    }
    return render;
  };

  render() {
    const { style } = this.props;
    const { min, max } = this.state;
    return (
      <div className={s.root}>
        <div className={s.container} style={style}>
          <Range
            min={MIN_LIMIT}
            max={MAX_LIMIT}
            defaultValue={[min, max]}
            step={STEP}
            dots
            tipFormatter={value => `${value}대`}
            onChange={this.handleAgeRangeChange}
          />
          <div className={s.wrapLabel}>{this.renderLabel()}</div>
        </div>
      </div>
    );
  }
}

AgeRangePicker.propTypes = propTypes;
AgeRangePicker.defaultProps = defaultProps;

export default withStyles(s, SliderStyle, RangeStyle)(AgeRangePicker);
