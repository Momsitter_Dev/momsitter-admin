import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Applicant.css';
import Paper from 'material-ui/Paper';

class Applicant extends React.Component {
  render() {
    return (
      <Paper zDepth={3}>
        <label>신청한 회원 정보</label>
        <div className={s.row}>
          <label className={s.label}></label>
          <span className={s.value}></span>
        </div>
      </Paper>
    );
  }
}

Applicant.propTypes = {
  data: PropTypes.object,
};

Applicant.defaultProps = {};

export default withStyles(s)(Applicant);
