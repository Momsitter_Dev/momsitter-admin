import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Receiver.css';
import Paper from 'material-ui/Paper';

class Receiver extends React.Component {
  render() {
    return (
      <Paper zDepth={3}>
        <label>신청 받은 회원 정보</label>
        <div className={s.row}>
          <label className={s.label}></label>
          <span className={s.value}></span>
        </div>
      </Paper>
    );
  }
}

Receiver.propTypes = {};

Receiver.defaultProps = {};

export default withStyles(s)(Receiver);
