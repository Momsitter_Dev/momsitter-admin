import { compose } from 'recompose';
import React from 'react';
import Paper from 'material-ui/Paper';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ApplicationItem.css';
import ApplyStatusGroup from '../../../../util/const/ApplyStatusGroup';
import Applicant from './Applicant';
import Receiver from './Receiver';

const propTypes = {
  data: PropTypes.object.isRequired,
};
const defaultProps = {};

class ApplicationItem extends React.Component {

  render() {
    const { data } = this.props;
    const {
      applyId,
      applyStatus,
      applicant,
      applyDate,
      applyType,
      declineReason,
      declineType,
      legacyApplyId,
      readDate,
      receiver,
      receiverId,
      tuningReason,
      updateDate,
    } = data;
    return (
      <Paper zDepth={2}>
        <div className={s.root}>
          <div className={s.applyInfo}>
            <div className={s.row}>
              <label className={s.label}>신청번호</label>
              <span className={s.value}>{applyId}</span>
            </div>
            <div className={s.row}>
              <label className={s.label}>요청상태</label>
              <SelectField
                value={applyStatus}
                disabled={true}
                underlineDisabledStyle={{ borderBottom: '1px solid #d3d3d3' }}
                style={{ width: '100%' }}
                labelStyle={{ textAlign: 'right' }}
                menuItemStyle={{ textAlign: 'right'}}
              >
                <MenuItem value={ApplyStatusGroup.REQUEST} primaryText="요청" />
                <MenuItem value={ApplyStatusGroup.ACCEPT} primaryText="수락" />
                <MenuItem value={ApplyStatusGroup.DECLINE} primaryText="거절" />
                <MenuItem value={ApplyStatusGroup.TUNING} primaryText="조율" />
              </SelectField>
            </div>
            <div className={s.row}>
              <label className={s.label}></label>
              <span className={s.value}></span>
            </div>
          </div>

          <div className={s.userInfo}>
            <Grid container spacing={16}>
              <Grid item xs={6}>
                <Applicant
                  data={applicant}
                />
              </Grid>
              <Grid item xs={6}>
                <Receiver
                  data={receiver}
                />
              </Grid>
            </Grid>
          </div>
        </div>
      </Paper>
    );
  }
}

ApplicationItem.propTypes = propTypes;
ApplicationItem.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(ApplicationItem);
