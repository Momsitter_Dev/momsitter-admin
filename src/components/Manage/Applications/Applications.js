import qs from 'qs';
import classnames from 'classnames';
import uuid from 'uuid/v4';
import IconButton from 'material-ui/IconButton';
import CloudDownloadIcon from 'material-ui/svg-icons/file/cloud-download';
import FlatPagination from 'material-ui-flat-pagination';

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Applications.css';
import SearchBar from './SearchBar';
import TableHeaderRows from '../Common/TableHeaderRows';
import Request from '../../../util/Request';
import TableRow from './TableRow';
import ApplicationItem from './ApplicationItem';
import NotificationDialog from '../../Dialog/NotificationDialog';

class Applications extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      page: 0,
      maxPage: 0,
      notificationDialog: false,
      notificationDialogTitle: '',
      notificationDialogMessage: '',
    };
    this.cachedOption = {
      searchFilter: 'applyId',
      searchText: '',
      searchOrderColumn: '',
      searchOrderType: '',
    };
    this.searchOptions = {
      requestPage: 0,
      searchFilter: 'applyId',
      searchText: '',
      searchOrderColumn: '',
      searchOrderType: '',
    };
    this.onClickSearch = this.onClickSearch.bind(this);
    this.onExportCSV = this.onExportCSV.bind(this);
  }

  async componentDidMount() {
    const queryString = qs.stringify(this.searchOptions);
    await this.doSearch(queryString)
      .then(data => {
        this.setState({
          list: data.data.rows,
          page: data.page.currentPage,
          maxPage: data.page.maxPage,
        });
      })
      .catch(() => {
        this.setState({ list: [] });
      });
  }

  doSearch = query =>
    new Promise(async (resolve, reject) => {
      // Cache Update
      this.cachedOption = Object.assign({}, this.searchOptions);

      const searchResult = await Request(
        `api/manage/user/applications?${query}`,
        { method: 'GET' },
      ).catch(() => false);
      if (searchResult && searchResult.result === 'success') {
        resolve(searchResult);
      } else {
        reject('2');
      }
    });

  onChangeSearchFilter = (event, index, value) => {
    this.searchOptions.searchFilter = value;
  };

  onChangeSearchText = (value) => {
    this.searchOptions.searchText = value;
  };

  onClickSearch = async function () {
    // 페이지를 1로 초기화
    this.searchOptions.requestPage = 0;
    const queryString = qs.stringify(this.searchOptions);
    await this.doSearch(queryString)
      .then(data => {
        this.setState({
          list: data.data.rows,
          page: data.page.currentPage,
          maxPage: data.page.maxPage,
        });
      })
      .catch(() => {
        this.setState({ list: [] });
      });
  };

  onClickPage = value => {
    this.cachedOption.requestPage = value;
    const queryString = qs.stringify(this.cachedOption);
    this.doSearch(queryString)
      .then(data => {
        this.setState({
          list: data.data.rows,
          page: data.page.currentPage,
          maxPage: data.page.maxPage,
        });
      })
      .catch(() => {
        this.setState({ list: [] });
      });
  };

  onChangeOrderColumn = () => {
    this.openNotificationDialog('알림', '해당 기능은 구현중입니다.');
  };

  openNotificationDialog = (title, message) => {
    this.setState({
      notificationDialog: true,
      notificationDialogTitle: title || '알림',
      notificationDialogMessage: message,
    });
  };

  onClickCloseNotificationDialog = () => {
    this.setState({
      notificationDialog: false,
    });
  };

  onExportCSV = async function () {
    const queryString = qs.stringify(this.cachedOption);
    window.open(`/api/manage/user/applications/csv?${queryString}`);
  };

  getApplicationListUI = (list) => {
    let ui = null;
    if (list && list.length > 0) {
      ui = list.map((item) => {
        return (
          <TableRow
            key={uuid()}
            data={item}
            openNotificationDialog={this.openNotificationDialog}
          />);
      });
    }
    return ui;
  };

  renderApplicationList = (list) => {
    let ui = null;
    if (list && list.length > 0) {
      ui = list.map((item) => {
        return (
          <ApplicationItem
            key={uuid()}
            data={item}
          />
        );
      });
    }
    return ui;
  };

  render() {
    const {
      list,
      page,
      maxPage,
      notificationDialog,
      notificationDialogTitle,
      notificationDialogMessage,
    } = this.state;
    const headers = [
      { id: 'applyId', desc: '신청번호' },
      { id: 'applicantId', desc: '지원자 번호' },
      { id: 'applicantName', desc: '지원자 이름' },
      { id: 'applicantRole', desc: '지원자 권한' },
      { id: 'receiverId', desc: '피 지원자 번호' },
      { id: 'receiverName', desc: '피 지원자 이름' },
      { id: 'receiverRole', desc: '피 지원자 권한' },
      { id: 'applyStatus', desc: '요청상태' },
      { id: 'applyType', desc: '지원 타입' },
      { id: 'applyDate', desc: '지원 일자' },
      { id: 'readDate', desc: '신청서 확인 일자' },
    ];
    const filter = [
      { name: '신청 번호', value: 'applyId', type: 'text' },
      { name: '지원자 번호', value: 'applicantId', type: 'text' },
      { name: '피 지원자 번호', value: 'receiverId', type: 'text' },
      { name: '지원 타입', value: 'applyType', type: 'applyType' },
      { name: '요청 상태', value: 'applyStatus', type: 'applyStatus' }
    ];
    return (
      <div className={classnames(s.root, ' box box-default')}>
        <SearchBar
          onChangeSearchFilter={this.onChangeSearchFilter}
          onChangeSearchText={this.onChangeSearchText}
          onClickSearch={this.onClickSearch}
          filter={filter}
        >
          <IconButton onClick={this.onExportCSV} tooltip={"CSV 내려받기"}>
            <CloudDownloadIcon/>
          </IconButton>
        </SearchBar>
        <div className={classnames("box-body", "padding-sm", s.tableWrap)}>
          <table
            className={classnames('table', s.table)}
            cellPadding="0px"
          >
            <thead>
              <TableHeaderRows
                onChangeOrderColumn={this.onChangeOrderColumn}
                headers={headers}
              />
            </thead>
            <tbody>
              {this.getApplicationListUI(list)}
            </tbody>
          </table>
          <div>
            <FlatPagination
              offset={page}
              limit={1}
              total={maxPage}
              onClick={(e, offset) => this.onClickPage(offset)}
            />
          </div>
        </div>
        <NotificationDialog
          open={notificationDialog}
          title={notificationDialogTitle}
          message={notificationDialogMessage}
          onClose={this.onClickCloseNotificationDialog}
        />
      </div>
    );
  }
}

Applications.propTypes = {};
Applications.defaultProps = {};

export default withStyles(s)(Applications);
