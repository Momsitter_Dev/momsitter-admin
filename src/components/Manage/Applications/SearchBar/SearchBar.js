import uuid from 'uuid/v4';
import { Toolbar, ToolbarGroup, ToolbarSeparator } from 'material-ui/Toolbar';
import MenuItem from 'material-ui/MenuItem';
import DropDownMenu from 'material-ui/DropDownMenu';
import RaisedButton from 'material-ui/RaisedButton';
import SelectField from 'material-ui/SelectField';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './SearchBar.css';
import TextField from '../../../TextField';
import ApplyTypeFilter from '../../../Filter/ApplyType';
import ApplyStatusFilter from '../../../Filter/ApplyStatus';

class SearchBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filterValue: '',
      filterText: '',
      filterType: '',
    };
  }

  onChangeFilter = (event, index, value) => {
    const filter = this.props.filter.find(item => {
      if (item.value === value) {
        return true;
      }
    });
    let filterType = '';
    if (filter) {
      if (filter.type) {
        filterType = filter.type;
      } else {
        filterType = 'text';
      }
    } else {
      // TODO Error filter not found
    }
    this.setState({
      filterText: '',
      filterValue: value,
      filterType,
    });
    this.props.onChangeSearchFilter(null, null, value);
  };

  onChangeFilterText = (value) => {
    this.setState({ filterText: value });
    this.props.onChangeSearchText(value);
  };

  onClickSearch = () => {
    this.props.onClickSearch();
  };

  onPressEnter = ev => {
    if (ev.key === 'Enter') {
      ev.preventDefault();
      this.onClickSearch();
    }
  };

  renderFilter = (filterType, filterText) => {
    let DEFAULT = (<div>검색 조건을 선택해 주세요.</div>);

    if (!filterType) {
      return DEFAULT;
    }

    if (filterType === 'text') {
      return (
        <TextField
          id={uuid()}
          onChange={(value) => this.onChangeFilterText(value)}
          value={filterText}
        />
      );
    }
    if (filterType === 'applyType') {
      return (
        <ApplyTypeFilter
          id={uuid()}
          onChange={this.onChangeFilterText}
          value={filterText}
        />
      );
    }
    if (filterType === 'applyStatus') {
      return (
        <ApplyStatusFilter
          id={uuid()}
          onChange={this.onChangeFilterText}
          value={filterText}
        />
      );
    }

    return DEFAULT;
  };

  renderFilterMenu = (filter) => {
    let ui = null;
    if (filter && filter.length > 0) {
      ui = filter.map((item, index) =>
        <MenuItem
          key={index}
          value={item.value}
          primaryText={item.name}
        />,
      );
    }
    return ui;
  }

  render() {
    const { filter } = this.props;
    const {
      filterValue,
      filterType,
      filterText,
    } = this.state;
    return (
      <Toolbar>
        <ToolbarGroup>
          <SelectField
            value={filterValue}
            onChange={this.onChangeFilter}
          >
            {this.renderFilterMenu(filter)}
          </SelectField>
          {this.renderFilter(filterType, filterText)}
          <RaisedButton
            label="search"
            primary
            onClick={this.onClickSearch}
          />
          <ToolbarSeparator />
          {this.props.children}
        </ToolbarGroup>
      </Toolbar>
    );
  }
}

SearchBar.propTypes = {
  onChangeSearchFilter: PropTypes.func,
  onChangeSearchText: PropTypes.func,
  onClickSearch: PropTypes.func,
  filter: PropTypes.array,
  children: PropTypes.node,
};

SearchBar.defaultProps = {};

export default withStyles(s)(SearchBar);
