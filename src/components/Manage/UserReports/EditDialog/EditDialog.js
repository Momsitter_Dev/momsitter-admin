/* eslint-disable prettier/prettier */
import { compose } from 'recompose';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './EditDialog.css';
import TextField from '../../../TextField';
import Request from '../../../../util/Request';

import ReportTypeGroup from '../../../../util/const/ReportTypeGroup';
import ReportTypeDescriptionGroup from '../../../../util/const/ReportTypeDescriptionGroup';
import ResponseResultTypeGroup from "../../../../util/const/ResponseResultTypeGroup";

const propTypes = {
  open: PropTypes.bool,
  data: PropTypes.shape({
    reportType: PropTypes.string,
    reportContent: PropTypes.string,
    reportId: PropTypes.number,
  }),
  onCloseDialog: PropTypes.func,
  onSaveDialog: PropTypes.func,
};
const defaultProps = {
  open: false,
  data: null,
  onCloseDialog: () => {},
  onSaveDialog: () => {},
};
class EditDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      reportType: props.data.reportType,
      reportContent: props.data.reportContent,
    };
    this.onSaveDialog = this.onSaveDialog.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      reportType: nextProps.data.reportType,
      reportContent: nextProps.data.reportContent,
    });
  }

  onChangeForm = (type, data) => {
    const state = {};
    state[type] = data;
    return this.setState(state);
  };

  onCloseDialog = () => {
    return this.props.onCloseDialog();
  };

  onSaveDialog = async function () {
    const { reportId } = this.props.data;
    const { reportType, reportContent } = this.state;
    if (reportId) {
      try {
        const response = await Request(
          `api/manage/user/reports/${reportId}`,
          {
            method: 'PUT',
            data: {
              reportId,
              reportType,
              reportContent,
            },
          },
        );
        if (response && response.result === ResponseResultTypeGroup.SUCCESS) {
          return this.onCloseDialog();
        }
        throw new Error('request fail');
      } catch (err) {
        console.log(err);
      }
    }
    return null;
  };

  render() {
    return (
      <div className={s.root}>
        <Dialog
          open={this.props.open}
          onClose={this.onCloseDialog}
        >
          <DialogTitle> 신고 정보 수정 </DialogTitle>
          <DialogContent>
            <Grid container spacing={16} alignItems={'flex-start'}>
              <Grid item xs={2}>
                <Typography variant={'caption'}> 신고 타입 </Typography>
              </Grid>
              <Grid item xs={10}>
                <Select
                  value={this.state.reportType}
                  onChange={(event) =>
                    this.onChangeForm('reportType', event.target.value)}
                >
                  <MenuItem value={ReportTypeGroup.IMPROPER_CONTENT}>
                    {ReportTypeDescriptionGroup[ReportTypeGroup.IMPROPER_CONTENT]}
                  </MenuItem>
                  <MenuItem value={ReportTypeGroup.IMPROPER_IMAGE}>
                    {ReportTypeDescriptionGroup[ReportTypeGroup.IMPROPER_IMAGE]}
                  </MenuItem>
                  <MenuItem value={ReportTypeGroup.IMPROPER_USER}>
                    {ReportTypeDescriptionGroup[ReportTypeGroup.IMPROPER_USER]}
                  </MenuItem>
                  <MenuItem value={ReportTypeGroup.VIOLATE_REGULATION}>
                    {ReportTypeDescriptionGroup[ReportTypeGroup.VIOLATE_REGULATION]}
                  </MenuItem>
                  <MenuItem value={ReportTypeGroup.WRONG_INFORMATION}>
                    {ReportTypeDescriptionGroup[ReportTypeGroup.WRONG_INFORMATION]}
                  </MenuItem>
                  <MenuItem value={ReportTypeGroup.OTHER}>
                    {ReportTypeDescriptionGroup[ReportTypeGroup.OTHER]}
                  </MenuItem>
                </Select>
              </Grid>
              <Grid item xs={2}>
                <Typography variant={'caption'}> 신고 내용 </Typography>
              </Grid>
              <Grid item xs={10}>
                <TextField
                  hintText="신고내용"
                  floatingLabelText="신고내용"
                  floatingLabelFixed
                  multiLine
                  rows={2}
                  value={this.state.reportContent}
                  onChange={(value) => this.onChangeForm('reportContent', value)}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button variant={"flat"} onClick={this.onCloseDialog}> 취소 </Button>
            <Button variant={"raised"} onClick={this.onSaveDialog}> 저장 </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

EditDialog.propTypes = propTypes;
EditDialog.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(EditDialog);
