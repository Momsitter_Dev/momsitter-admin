import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './TableRow.css';
import RaisedButton from 'material-ui/RaisedButton';
import Request from '../../../../util/Request';
import SimpleDateFormat from '../../../../util/dataFormat/SimpleDateFormat';
import ReportTypeDescriptionGroup from '../../../../util/const/ReportTypeDescriptionGroup';
import EnhancedButton from 'material-ui/internal/EnhancedButton';
import getUserNameText from '../../../../util/user/getUserNameText';
import getUserRoleText from '../../../../util/user/getUserRoleText';

const buttonStyle = {
  zIndex: '0',
};

class TableRow extends React.Component {
  constructor(props) {
    super(props);
    this.onClickDeleteReport = this.onClickDeleteReport.bind(this);
  }
  onClickReportContent = (reportContent) => {
    if (reportContent) {
      this.props.openNotificationDialog('신고 내용', reportContent);
    } else {
      this.props.openNotificationDialog('신고 내용', '작성된 신고 내용이 없습니다.');
    }
  };

  onClickUserId = (userId) => {
    window.open('/users?userId=' + userId);
  };

  onClickEditReport = () => {
    this.props.onClickEditReport(this.props.data);
  };

  onClickDeleteReport = async function () {
    // TODO: 이 함수는 상위 컴포넌트로 옮겨야함
    const { reportId } = this.props.data;
    if (confirm('삭제하시겠습니까?')) {
      if (reportId) {
        const response = await Request(
          `api/manage/user/reports/${reportId}`,
          { method: 'DELETE' },
        ).catch(() => false);
        if (response && response.result === 'success') {
          // this.setState({ reviewType: value });
          // TODO Reload list
        } else {
          console.log('error');
        }
      }
    }
  };

  getReportTypeText = (reportType) => {
    let text = '없음';
    if (reportType) {
      text = ReportTypeDescriptionGroup[reportType];
      if (!text) {
        text = '없음';
      }
    }
    return text;
  };

  render() {
    const { data } = this.props;
    const {
      reportId,
      reporterId,
      targetId,
      reportType,
      reportContent,
      reportDate,
      reportUser,
      reportTargetUser,
    } = data;
    return (
      <tr className={s.row}>
        <td>
          {reportId}
        </td>
        <td>
          <RaisedButton
            label={reporterId}
            onClick={() => this.onClickUserId(reporterId)}
            buttonStyle={buttonStyle}
          />
        </td>
        <td>{getUserNameText(reportUser)}</td>
        <td>{getUserRoleText(reportUser)}</td>
        <td>
          <RaisedButton
            label={targetId}
            onClick={() => this.onClickUserId(targetId)}
            buttonStyle={buttonStyle}
          />
        </td>
        <td>{getUserNameText(reportTargetUser)}</td>
        <td>{getUserRoleText(reportTargetUser)}</td>
        <td>
          {this.getReportTypeText(reportType)}
        </td>
        <td>
          <EnhancedButton
            onClick={() => this.onClickReportContent(reportContent)}
            style={{ width: '100%', ...buttonStyle }}
          >
            <p className={s.reportContent}>{reportContent}</p>
          </EnhancedButton>
        </td>
        <td>
          {SimpleDateFormat(reportDate)}
        </td>
        <td>
          <RaisedButton
            label="수정"
            onClick={this.onClickEditReport}
            buttonStyle={buttonStyle}
          />
          <RaisedButton
            label="삭제"
            onClick={this.onClickDeleteReport}
            buttonStyle={buttonStyle}
          />
        </td>
      </tr>
    );
  }
}

TableRow.propTypes = {
  data: PropTypes.object.isRequired,
  onClickEditReport: PropTypes.func,
  openNotificationDialog: PropTypes.func.isRequired,
};

TableRow.defaultProps = {};

export default withStyles(s)(TableRow);
