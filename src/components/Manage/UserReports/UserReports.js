import qs from 'qs';
import uuid from 'uuid/v4';
import _ from 'lodash';
import classnames from 'classnames';
import FlatPagination from 'material-ui-flat-pagination';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './UserReports.css';
import SearchBar from '../../SearchBar';
import TableHeaderRow from '../../TableHeaderRow';
import TableRow from './TableRow';
import Request from '../../../util/Request';
import EditDialog from './EditDialog';
import ResponseResultTypeGroup from '../../../util/const/ResponseResultTypeGroup';
import FilterDataTypeGroup from '../../../util/const/FilterDataTypeGroup';
import NotificationDialog from '../../Dialog/NotificationDialog';

class UserReports extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      page: 0,
      maxPage: 0,
      editDialog: false,
      editData: {},
      notificationDialog: false,
      notificationDialogTitle: '',
      notificationDialogMessage: '',
      searchOptions: [],
      orderOption: {
        column: '',
        method: '',
      },
    };
    this.cachedOption = {};
    this.onClickSearch = this.onClickSearch.bind(this);
    this.onChangeOrderColumn = this.onChangeOrderColumn.bind(this);
    this.onExportCSV = this.onExportCSV.bind(this);
  }

  async componentDidMount() {
    await this.doSearch({}).then(data => {
      this.setState({
        list: data.data.rows,
        page: data.page.currentPage,
        maxPage: data.page.maxPage,
      });
    }).catch(() => {
      this.setState({ list: [] });
    });
  }

  doSearch = (options) =>
    new Promise(async (resolve, reject) => {
      const query = qs.stringify(options);
      this.cachedOption = Object.assign({}, options);

      const searchResult = await Request(
        `api/manage/user/reports?${query}`,
        { method: 'GET' },
      ).catch(() => false);
      if (searchResult && searchResult.result === ResponseResultTypeGroup.SUCCESS) {
        resolve(searchResult);
      } else {
        reject();
      }
    });

  onClickSearch = async function () {
    const { searchOptions, orderOption } = this.state;
    await this.doSearch({
      searchOptions,
      orderOption,
      requestPage: 0,
    }).then(data => {
      this.setState({
        list: data.data.rows,
        page: data.page.currentPage,
        maxPage: data.page.maxPage,
      });
    }).catch(() => {
      this.setState({ list: [] });
    });
  };

  onClickPage = value => {
    this.cachedOption.requestPage = value;
    this.doSearch(this.cachedOption).then(data => {
      this.setState({
        list: data.data.rows,
        page: data.page.currentPage,
        maxPage: data.page.maxPage,
      });
    }).catch(() => {
      this.setState({ list: [] });
    });
  };

  onChangeOrderColumn = async function (columnId) {
    const { orderOption, searchOptions } = this.state;
    if (orderOption.column !== columnId) {
      orderOption.method = '';
    }
    orderOption.column = columnId;
    if (orderOption.method === 'DESC') {
      orderOption.method = 'ASC';
    } else if (orderOption.method === 'ASC') {
      orderOption.method = '';
    } else {
      orderOption.method = 'DESC'
    }
    this.setState({ orderOption });

    await this.doSearch({
      searchOptions,
      orderOption,
      requestPage: 0,
    }).then(data => {
      this.setState({
        list: data.data.rows,
        page: data.page.currentPage,
        maxPage: data.page.maxPage,
      });
    }).catch(() => {
      this.setState({ list: [] });
    });
  };

  onExportCSV = async function () {
    const queryString = qs.stringify(this.cachedOption);
    window.open(`/api/manage/user/reports/csv?${queryString}`);
  };

  openEditDialog = () => {
    this.setState({ editDialog: true });
  };

  closeEditDialog = () => {
    this.setState({ editDialog: false });
  };

  onClickEditReport = (data) => {
    this.setState({ editData: data });
    this.openEditDialog();
  };

  openNotificationDialog = (title, message) => {
    this.setState({
      notificationDialog: true,
      notificationDialogTitle: title || '알림',
      notificationDialogMessage: message,
    });
  };

  onClickCloseNotificationDialog = () => {
    this.setState({
      notificationDialog: false,
    });
  };

  onAddSearchOption = () => {
    const { searchOptions } = this.state;
    searchOptions.push({
      generatedKey: uuid(),
      filterType: '',
      filterValue: '',
      filterDataType: '',
    });
    this.setState({ searchOptions });
  };

  onDeleteSearchOption = (generatedKey) => {
    const { searchOptions } = this.state;
    _.remove(searchOptions, (o) => {
      return o.generatedKey === generatedKey;
    });
    this.setState({ searchOptions });
  };

  onChangeSearchOption = (generatedKey, type, value, dataType) => {
    const { searchOptions } = this.state;
    const targetIndex = _.findIndex(searchOptions, (o) => {
      return o.generatedKey === generatedKey;
    });
    if (targetIndex !== -1) {
      searchOptions[targetIndex] = {
        ...searchOptions[targetIndex],
        filterType: type,
        filterValue: value,
        filterDataType: dataType,
      };
      this.setState({ searchOptions });
    } else {
      console.log('DEBUG : ON CHANGE BUT NOT FOUND : ' + generatedKey);
    }
  };

  renderReportItems = (list) => {
    let ui = null;
    if (list && list.length > 0) {
      ui = list.map((item, index) =>
        <TableRow
          key={index}
          data={item}
          onClickEditReport={this.onClickEditReport}
          openNotificationDialog={this.openNotificationDialog}
        />,
      );
    }
    return ui;
  };

  render() {
    const {
      list,
      notificationDialog,
      notificationDialogTitle,
      notificationDialogMessage,
      orderOption,
      searchOptions,
      page,
      maxPage,
      editData,
      editDialog,
    } = this.state;
    const headers = [
      { id: 'reportId', desc: '신고 번호', sortable: true },
      { id: 'reporterId', desc: '신고자 번호', sortable: true },
      { id: 'reporterId', desc: '신고자 이름', sortable: false },
      { id: 'reporterId', desc: '신고자 타입', sortable: false },
      { id: 'targetId', desc: '신고대상 번호', sortable: true },
      { id: 'targetId', desc: '신고대상 이름', sortable: false },
      { id: 'targetId', desc: '신고대상 타입', sortable: false },
      { id: 'reportType', desc: '신고타입', sortable: true },
      { id: 'reportContent', desc: '신고내용', sortable: true },
      { id: 'reportDate', desc: '신고일자', sortable: true },
      { id: 'reportId', desc: ' ', sortable: false }
    ];
    const filter = [
      { name: '신고 번호', type: 'reportId', dataType: FilterDataTypeGroup.UNIQUE_TEXT },
      { name: '신고자 번호', type: 'reporterId', dataType: FilterDataTypeGroup.UNIQUE_TEXT },
      { name: '신고대상 번호', type: 'targetId', dataType: FilterDataTypeGroup.UNIQUE_TEXT },
      { name: '신고 타입', type: 'reportType', dataType: FilterDataTypeGroup.REPORT_TYPE },
      { name: '신고 내용', type: 'reportContent', dataType: FilterDataTypeGroup.TEXT },
      { name: '신고 일자', type: 'reportDate', dataType: FilterDataTypeGroup.DATE_RANGE },
    ];

    return (
      <div className={classnames(s.root, ' box box-default')}>
        <SearchBar
          filter={filter}
          searchOptions={searchOptions}
          onClickSearch={this.onClickSearch}
          openNotificationDialog={this.openNotificationDialog}
          onExportCSV={this.onExportCSV}
          onClickAddSearchOption={this.onAddSearchOption}
          onClickDeleteSearchOption={this.onDeleteSearchOption}
          onChangeSearchOption={this.onChangeSearchOption}
        />
        <div className="box-body padding-sm">
          <table className={classnames('table')}>
            <thead>
              <TableHeaderRow
                data={headers}
                orderOption={orderOption}
                onChangeOrderColumn={this.onChangeOrderColumn}
                openNotificationDialog={this.openNotificationDialog}
              />
            </thead>
            <tbody>{this.renderReportItems(list)}</tbody>
          </table>
          <div>
            <FlatPagination
              offset={page}
              limit={1}
              total={maxPage}
              onClick={(e, offset) => this.onClickPage(offset)}
            />
          </div>
        </div>
        <EditDialog
          data={editData}
          open={editDialog}
          onCloseDialog={this.closeEditDialog}
        />
        <NotificationDialog
          open={notificationDialog}
          title={notificationDialogTitle}
          message={notificationDialogMessage}
          onClose={this.onClickCloseNotificationDialog}
        />
      </div>
    );
  }
}

UserReports.propTypes = {};

UserReports.defaultProps = {};

export default withStyles(s)(UserReports);
