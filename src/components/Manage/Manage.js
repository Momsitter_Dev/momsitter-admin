import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Manage.css';
import Pagination from 'material-ui-pagination';
import DataTable from './DataTable';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';
import MenuItem from 'material-ui/MenuItem';
import DropDownMenu from 'material-ui/DropDownMenu';
import TextField from '../TextField';
import RaisedButton from 'material-ui/RaisedButton';

class Manage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 1,
    };
  }

  handleChange = (event, index, value) => this.setState({value});
  onPageChange = (value) => {
    console.log('onPageChange' + value);
  };

  render() {
    return (
      <div className={s.root}>
        <Toolbar>
          <ToolbarGroup>
            <ToolbarTitle text={this.props.title} />
            <ToolbarSeparator />
            <DropDownMenu value={this.state.value} onChange={this.handleChange}>
              <MenuItem value={1} primaryText="All Broadcasts" />
              <MenuItem value={2} primaryText="All Voice" />
              <MenuItem value={3} primaryText="All Text" />
              <MenuItem value={4} primaryText="Complete Voice" />
              <MenuItem value={5} primaryText="Complete Text" />
              <MenuItem value={6} primaryText="Active Voice" />
              <MenuItem value={7} primaryText="Active Text" />
            </DropDownMenu>
            <TextField hintText="Hint Text" />
            <RaisedButton label="search" primary={true}/>
          </ToolbarGroup>
        </Toolbar>
        <DataTable />
        <div>
          <Pagination total={5}
                      current={1}
                      display={5}
                      onChange={this.onPageChange}
          />
        </div>
      </div>
    );
  }
}

Manage.propTypes = {
  title: PropTypes.string.isRequired,
};

Manage.defaultProps = {
  title: '제목',
};

export default withStyles(s)(Manage);
