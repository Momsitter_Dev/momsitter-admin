import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './TableRow.css';
import MenuItem from 'material-ui/MenuItem';
import DropDownMenu from 'material-ui/DropDownMenu';
import RaisedButton from 'material-ui/RaisedButton';
import moment from 'moment';
import Request from '../../../../util/Request';
import SimpleDateFormat from '../../../../util/dataFormat/SimpleDateFormat';

class TableRow extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      reviewType: this.props.data.reviewType,
      reviewStatus: this.props.data.status,
    };
    this.onClickEditReview = this.onClickEditReview.bind(this);
    this.onClickRemoveReview = this.onClickRemoveReview.bind(this);
    this.onChangeReviewStatus = this.onChangeReviewStatus.bind(this);
    this.onChangeReviewType = this.onChangeReviewType.bind(this);
  }

  onClickUserId = (userId) => {
    window.open('/users?userId=' + userId);
  };

  onClickEditReview = async function (event, data) {
    this.props.onClickEdit(this.props.data);
  };

  onClickRemoveReview = async function () {
    if (typeof window !== 'undefined') {
      const { reviewId } = this.props.data;
      if (window.confirm('삭제하시겠습니까?')) {
        if (reviewId) {
          const response = await Request(
            `api/manage/user/reviews/${reviewId}`,
            { method: 'DELETE' },
          ).catch(() => false);
          if (response && response.result === 'success') {
            this.props.openNotificationDialog('알림', '삭제되었습니다.');
          } else {
            this.props.openNotificationDialog('오류', '네트워크 오류입니다.');
          }
        }
      }
    } else {
      this.props.openNotificationDialog('오류', '비 정상적인 접근입니다.');
    }
  };

  onChangeReviewStatus = async function (event, data, value) {
    const { reviewId } = this.props.data;
    const { reviewStatus } = this.state;
    if (reviewStatus !== value) {
      const response = await Request(
        `api/manage/user/reviews/${reviewId}/reviewStatus`,
        {
          method: 'PUT',
          data: { reviewStatus: value },
        },
      ).catch(() => false);
      if (response && response.result === 'success') {
        this.setState({ reviewStatus: value });
        this.props.openNotificationDialog('알림', '리뷰상태가 변경되었습니다.');
      } else {
        this.props.openNotificationDialog('오류', '네트워크 오류입니다.');
      }
    }
  };

  onChangeReviewType = async function (event, data, value) {
    const { reviewId } = this.props.data;
    const { reviewType } = this.state;
    if (reviewType !== value) {
      const response = await Request(
        `api/manage/user/reviews/${reviewId}/reviewType`,
        {
          method: 'PUT',
          data: { reviewType: value },
        },
      ).catch(() => false);
      if (response && response.result === 'success') {
        this.setState({ reviewType: value });
        this.props.openNotificationDialog('알림', '리뷰타입이 변경되었습니다.');
      } else {
        this.props.openNotificationDialog('오류', '네트워크 오류입니다.');
      }
    }
  };

  shouldComponentUpdate() {
    return true;
  }

  render() {
    const { data } = this.props;
    const {
      reviewId,
      applyId,
      senderId,
      targetId,
      reviewRate,
      reviewContent,
      reviewType,
      reviewStatus,
      writeDate
    } = data;
    return (
      <tr>
        <td>
          {reviewId}
        </td>
        <td>
          {applyId || '없음'}
        </td>
        <td>
          <RaisedButton
            label={senderId}
            onClick={() => this.onClickUserId(senderId)}
          />
        </td>
        <td>
          <RaisedButton
            label={targetId}
            onClick={() => this.onClickUserId(targetId)}
          />
        </td>
        <td>
          {reviewRate}
        </td>
        <td>
          {reviewContent}
        </td>
        <td>
          <DropDownMenu
            value={reviewType}
            onChange={this.onChangeReviewType}
          >
            <MenuItem value="interviewReview" primaryText="인터뷰 리뷰" />
            <MenuItem value="recruitReview" primaryText="채용 리뷰" />
          </DropDownMenu>
        </td>
        <td>
          {SimpleDateFormat(writeDate)}
        </td>
        <td>
          <DropDownMenu
            value={reviewStatus}
            onChange={this.onChangeReviewStatus}
          >
            <MenuItem value="open" primaryText="오픈됨" />
            <MenuItem value="closed" primaryText="닫힘" />
          </DropDownMenu>
        </td>
        <td>
          <RaisedButton label="수정" onClick={this.onClickEditReview} />
          <RaisedButton label="삭제" onClick={this.onClickRemoveReview} />
        </td>
      </tr>
    );
  }
}

TableRow.propTypes = {
  data: PropTypes.object.isRequired,
  onClickEdit: PropTypes.func,
  onClickEditSave: PropTypes.func,
  openNotificationDialog: PropTypes.func.isRequired,
};

TableRow.defaultProps = {};

export default withStyles(s)(TableRow);
