import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './TableHeaderRows.css';
import ArrowUpIcon from 'material-ui/svg-icons/navigation/arrow-upward';
import ArrowDownIcon from 'material-ui/svg-icons/navigation/arrow-downward';
import EnhancedButton from 'material-ui/internal/EnhancedButton';

class TableHeaderRows extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      orderColumn: '',
      orderType: '',
    };
  }

  onChangeOrderColumn = (value) => {
    const { orderColumn, orderType } = this.state;
    if (orderColumn !== value) {
      this.setState({ orderColumn: value, orderType: 'ASC' });
    } else if (orderType === 'ASC') {
      this.setState({ orderType: 'DESC' });
    } else {
      this.setState({ orderType: 'ASC' });
    }
    this.props.onChangeOrderColumn({
      orderType: this.state.orderType,
      orderColumn: this.state.orderColumn,
    });
  };

  renderArrow = (id, state) => {
    let ui = null;
    const iconStyle = {
      position: 'absolute',
      top: '25%',
      right: '-5px',
      width: '15px',
      height: '15px',
    };
    if (id) {
      if (state.orderColumn === id) {
        if (state.orderType === 'ASC') {
          ui = <ArrowUpIcon style={iconStyle} />;
        }
        if (state.orderType === 'DESC') {
          ui = <ArrowDownIcon style={iconStyle} />;
        }
      }
    }
    return ui;
  };

  render() {
    const { headers } = this.props;
    return (
      <tr className={s.row}>
        {headers.map((item, index) =>
          <th key={index}>
            <EnhancedButton
              onClick={() => this.onChangeOrderColumn(item.id)}
              style={{ width: '100%' }}
            >
              <div>
                <span>{this.renderArrow(item.id, this.state)}</span>
                <span>{item.desc}</span>
              </div>
            </EnhancedButton>
          </th>,
        )}
      </tr>
    );
  }
}

TableHeaderRows.propTypes = {
  onChangeOrderColumn: PropTypes.func,
  headers: PropTypes.array,
};

TableHeaderRows.defaultProps = {};

export default withStyles(s)(TableHeaderRows);
