import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './SearchBar.css';
import { Toolbar, ToolbarGroup, ToolbarSeparator } from 'material-ui/Toolbar';
import MenuItem from 'material-ui/MenuItem';
import DropDownMenu from 'material-ui/DropDownMenu';
import TextField from '../../../TextField';
import RaisedButton from 'material-ui/RaisedButton';
import uuid from 'uuid/v4';

class SearchBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filterValue: '',
      filterText: '',
      filterType: '',
    };
  }

  onChangeFilter = (event, index, value) => {
    // TODO Find value in filter
    const filter = this.props.filter.find(item => {
      if (item.value === value) {
        return true;
      }
    });
    let filterType = '';
    if (filter) {
      if (filter.type) {
        filterType = filter.type;
      } else {
        filterType = 'text';
      }
    } else {
      // TODO Error filter not found
    }
    this.setState({
      filterValue: value,
      filterType,
    });
    this.props.onChangeSearchFilter(null, null, value);
  };

  onChangeSearchText = (value) => {
    console.log(`DEBUG : ${value}`);
    return this.setState({ filterText: value }, () => {
      return this.props.onChangeSearchText(null, value);
    });
  };

  onClickSearch = () => {
    this.props.onClickSearch();
  };

  onPressEnter = ev => {
    if (ev.key === 'Enter') {
      ev.preventDefault();
      this.onClickSearch();
    }
  };

  renderFilter = (type) => {
    let ui = null;
    if (type) {
      if (type === 'text') {
        ui = (
          <TextField
            id={"text_input"}
            onChange={this.onChangeSearchText}
          />
        );
      }
    } else {
      ui = (
        <TextField
          id={"text_input"}
          onChange={this.onChangeSearchText}
        />
      );
    }
    return ui;
  };

  render() {
    const { filter } = this.props;
    const {
      filterValue,
      filterType,
    } = this.state;
    return (
      <Toolbar>
        <ToolbarGroup>
          <DropDownMenu
            value={filterValue}
            onChange={this.onChangeFilter}
          >
            {filter.map((item, index) =>
              <MenuItem
                key={index}
                value={item.value}
                primaryText={item.name}
              />,
            )}
          </DropDownMenu>
          {this.renderFilter(filterType)}
          <RaisedButton
            label="search"
            primary
            onClick={this.onClickSearch}
          />
          <ToolbarSeparator />
          {this.props.children}
        </ToolbarGroup>
      </Toolbar>
    );
  }
}

SearchBar.propTypes = {
  onChangeSearchFilter: PropTypes.func,
  onChangeSearchText: PropTypes.func,
  onClickSearch: PropTypes.func,
  filter: PropTypes.array,
  children: PropTypes.node,
};

SearchBar.defaultProps = {};

export default withStyles(s)(SearchBar);
