/* eslint-disable prettier/prettier */
import { compose } from 'recompose';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './EditDialog.css';
import TextField from '../../../TextField';

const propTypes = {
  open: PropTypes.bool,
  data: PropTypes.shape({
    reviewId: PropTypes.number,
    reviewRate: PropTypes.number,
    reviewContent: PropTypes.string,
  }),
  onCloseDialog: PropTypes.func,
  onSaveDialog: PropTypes.func,
};
const defaultProps = {
  open: false,
  data: {},
  onCloseDialog: () => {},
  onSaveDialog: () => {},
};

class EditDialog extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      reviewId: props.data.reviewId,
      reviewRate: props.data.reviewRate,
      reviewContent: props.data.reviewContent,
    };
  }

  componentWillReceiveProps (nextProps) {
    this.setState({
      reviewId: nextProps.data.reviewId,
      reviewRate: nextProps.data.reviewRate,
      reviewContent: nextProps.data.reviewContent,
    });
  }

  onCloseDialog = () => {
    return this.props.onCloseDialog();
  };

  onSaveDialog = () => {
    return this.props.onSaveDialog({
      reviewId: this.state.reviewId,
      reviewRate: this.state.reviewRate,
      reviewContent: this.state.reviewContent,
    });
  };

  onChangeReviewRate = (event) => {
    return this.setState({ reviewRate: event.target.value });
  };

  onChangeReviewContent = (value) => {
    return this.setState({ reviewContent: value });
  };

  render() {
    const { reviewRate, reviewContent } = this.state;
    const { open } = this.props;
    return (
      <div className={s.root}>
        <Dialog
          title="후기 수정"
          open={open}
          onClose={this.onCloseDialog}
        >
          <DialogTitle> 후기 수정 </DialogTitle>
          <DialogContent>
            <Grid container spacing={16} alignItems={'flex-start'}>
              <Grid item xs={2}>
                <Typography variant={'caption'}> 후기 점수 </Typography>
              </Grid>
              <Grid item xs={10}>
                <Select
                  value={reviewRate}
                  onChange={this.onChangeReviewRate}
                >
                  <MenuItem value={1}> 1 점 </MenuItem>
                  <MenuItem value={2}> 2 점 </MenuItem>
                  <MenuItem value={3}> 3 점 </MenuItem>
                  <MenuItem value={4}> 4 점 </MenuItem>
                  <MenuItem value={5}> 5 점 </MenuItem>
                </Select>
              </Grid>

              <Grid item xs={2}>
                <Typography variant={'caption'}> 후기 내용 </Typography>
              </Grid>
              <Grid item xs={10}>
                <TextField
                  value={reviewContent}
                  onChange={this.onChangeReviewContent}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button variant={'flat'} onClick={this.onCloseDialog}> 취소 </Button>
            <Button variant={'raised'} onClick={this.onSaveDialog}> 저장 </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

EditDialog.propTypes = propTypes;
EditDialog.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(EditDialog);
