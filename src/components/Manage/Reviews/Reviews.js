/* eslint-disable prettier/prettier */
import _ from 'lodash';
import qs from 'qs';
import uuid from 'uuid/v4';
import classnames from 'classnames';
import FlatPagination from 'material-ui-flat-pagination';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Reviews.css';
import SearchBar from '../../SearchBar';
import TableHeaderRow from '../../TableHeaderRow';
import TableRow from './TableRow';
import EditDialog from './EditDialog';
import Request from '../../../util/Request';
import ResponseResultTypeGroup from '../../../util/const/ResponseResultTypeGroup';
import FilterDataTypeGroup from '../../../util/const/FilterDataTypeGroup';
import NotificationDialog from '../../Dialog/NotificationDialog';

class Reviews extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      page: 0,
      maxPage: 0,
      dialogOpen: false,
      editData: {},
      notificationDialog: false,
      notificationDialogTitle: '',
      notificationDialogMessage: '',
      searchOptions: [],
      orderOption: {
        column: '',
        method: '',
      },
    };
    this.cachedOption = {};
    this.onClickSearch = this.onClickSearch.bind(this);
    this.onChangeOrderColumn = this.onChangeOrderColumn.bind(this);
    this.onClickRemoveReview = this.onClickRemoveReview.bind(this);
    this.onExportCSV = this.onExportCSV.bind(this);
  }

  async componentDidMount() {
    await this.doSearch({}).then(data => {
      this.setState({
        list: data.data.rows,
        page: data.page.currentPage,
        maxPage: data.page.maxPage,
      });
    }).catch(() => {
      this.setState({ list: [] });
    });
  }

  doSearch = (options) =>
    new Promise(async (resolve, reject) => {
      const query = qs.stringify(options);
      this.cachedOption = Object.assign({}, options);

      const searchResult = await Request(
        `api/manage/user/reviews?${query}`,
        { method: 'GET' },
      ).catch(() => false);
      if (searchResult && searchResult.result === ResponseResultTypeGroup.SUCCESS) {
        resolve(searchResult);
      } else {
        reject();
      }
    });

  onClickSearch = async function () {
    const { searchOptions, orderOption } = this.state;
    await this.doSearch({
      searchOptions,
      orderOption,
      requestPage: 0,
    }).then(data => {
      this.setState({
        list: data.data.rows,
        page: data.page.currentPage,
        maxPage: data.page.maxPage,
      });
    }).catch(() => {
      this.setState({ list: [] });
    });
  };

  onClickPage = value => {
    this.cachedOption.requestPage = value;
    this.doSearch(this.cachedOption).then(data => {
      this.setState({
        list: data.data.rows,
        page: data.page.currentPage,
        maxPage: data.page.maxPage,
      });
    }).catch(() => {
      this.setState({ list: [] });
    });
  };

  onChangeOrderColumn = async function (columnId) {
    const { orderOption, searchOptions } = this.state;
    if (orderOption.column !== columnId) {
      orderOption.method = '';
    }
    orderOption.column = columnId;
    if (orderOption.method === 'DESC') {
      orderOption.method = 'ASC';
    } else if (orderOption.method === 'ASC') {
      orderOption.method = '';
    } else {
      orderOption.method = 'DESC'
    }
    this.setState({ orderOption });

    await this.doSearch({
      searchOptions,
      orderOption,
      requestPage: 0,
    }).then(data => {
      this.setState({
        list: data.data.rows,
        page: data.page.currentPage,
        maxPage: data.page.maxPage,
      });
    }).catch(() => {
      this.setState({ list: [] });
    });
  };

  onCloseDialog = () => {
    this.setState({ dialogOpen: false });
  };

  onSaveDialog = async data => {
    const { reviewId } = data;
    const searchResult = await Request(
      `api/manage/user/reviews/${reviewId}`,
      { method: 'PUT', data: data, },
    ).catch(() => false);

    if (searchResult && searchResult.result === ResponseResultTypeGroup.SUCCESS) {
      const { list } = this.state;
      const targetInd = _.findIndex(list, (o) => {
        return o.reviewId = reviewId;
      });

      if (targetInd !== -1) {
        let newList = Object.assign([], list);
        let targetData = list[targetInd];
        newList[targetInd] = {...targetData, ...data};
        this.setState({
          list: newList,
        });
      }
      this.openNotificationDialog('알림', '저장되었습니다.');
      this.onCloseDialog();
    } else {
      this.openNotificationDialog('오류', '저장할 수 없습니다.');
    }
  };

  onClickEditReview = data => {
    this.setState({
      dialogOpen: true,
      editData: data,
    });
  };

  onClickRemoveReview = async function (reviewId) {
    const { list } = this.state;
    if (reviewId) {
      const response = await Request(`api/manage/user/reviews/${reviewId}`,
        { method: 'DELETE' },
      ).catch(() => false);

      if (response && response.result === ResponseResultTypeGroup.SUCCESS) {
        const targetInd = _.findIndex(list, (o) => {
          return o.reviewId === reviewId;
        });
        if (targetInd !== -1) {
          let newList = Object.assign([], list);
          newList.slice(targetInd, 1);
          this.setState({
            list: newList,
          });
          this.openNotificationDialog('알림', '삭제되었습니다.');
        } else {
          this.openNotificationDialog('오류', '유효하지 않은 후기 ID 입니다.');
        }
      } else {
        this.openNotificationDialog('오류', '네트워크 오류입니다.');
      }
    } else {
      this.openNotificationDialog('오류', '유효하지 않은 후기 ID 입니다.');
    }
  };

  openNotificationDialog = (title, message) => {
    this.setState({
      notificationDialog: true,
      notificationDialogTitle: title || '알림',
      notificationDialogMessage: message,
    });
  };

  onExportCSV = async function () {
    const queryString = qs.stringify(this.cachedOption);
    window.open(`/api/manage/user/reviews/csv?${queryString}`);
  };

  onClickCloseNotificationDialog = () => {
    this.setState({
      notificationDialog: false,
    });
  };

  onAddSearchOption = () => {
    const { searchOptions } = this.state;
    searchOptions.push({
      generatedKey: uuid(),
      filterType: '',
      filterValue: '',
      filterDataType: '',
    });
    this.setState({ searchOptions });
  };

  onDeleteSearchOption = (generatedKey) => {
    const { searchOptions } = this.state;
    _.remove(searchOptions, (o) => {
      return o.generatedKey === generatedKey;
    });
    this.setState({ searchOptions });
  };

  onChangeSearchOption = (generatedKey, type, value, dataType) => {
    const { searchOptions } = this.state;
    const targetIndex = _.findIndex(searchOptions, (o) => {
      return o.generatedKey === generatedKey;
    });
    if (targetIndex !== -1) {
      searchOptions[targetIndex] = {
        ...searchOptions[targetIndex],
        filterType: type,
        filterValue: value,
        filterDataType: dataType,
      };
      this.setState({ searchOptions });
    } else {
      console.log('DEBUG : ON CHANGE BUT NOT FOUND : ' + generatedKey);
    }
  };

  renderReviewItem = () => {
    const { list } = this.state;
    let ui = null;
    if (list && list instanceof Array) {
      ui = list.map((item, index) => {
        return (
          <TableRow
            key={index}
            data={item}
            onClickEdit={this.onClickEditReview}
            onClickRemove={this.onClickRemoveReview}
            openNotificationDialog={this.openNotificationDialog}
          />
        );
      });
    }
    return ui;
  };

  render() {
    const {
      notificationDialog,
      notificationDialogTitle,
      notificationDialogMessage,
      orderOption,
      searchOptions,
      page,
      maxPage,
    } = this.state;
    const headers = [
      { id: 'reviewId', desc: '후기번호', sortable: true },
      { id: 'applyId', desc: '신청번호', sortable: true },
      { id: 'senderId', desc: '작성자 번호', sortable: true },
      { id: 'senderId', desc: '작성자 이름', sortable: false },
      { id: 'senderId', desc: '작성자 권한', sortable: false },
      { id: 'targetId', desc: '후기 대상 번호', sortable: true },
      { id: 'targetId', desc: '후기 대상 이름', sortable: false },
      { id: 'targetId', desc: '후기 대상 권한', sortable: false },
      { id: 'reviewRate', desc: '평점', sortable: true },
      { id: 'reviewContent', desc: '후기내용', sortable: true },
      { id: 'reviewType', desc: '후기타입', sortable: true },
      { id: 'status', desc: '후기상태', sortable: true },
      { id: 'writeDate', desc: '작성일자', sortable: true },
      { id: 'actions', desc: ' ', sortable: false },
    ];
    const filter = [
      { name: '후기 번호', type: 'reviewId', dataType: FilterDataTypeGroup.UNIQUE_TEXT },
      { name: '지원 번호', type: 'applyId', dataType: FilterDataTypeGroup.UNIQUE_TEXT },
      { name: '작성자 번호', type: 'senderId', dataType: FilterDataTypeGroup.UNIQUE_TEXT },
      { name: '후기 대상 번호', type: 'targetId', dataType: FilterDataTypeGroup.UNIQUE_TEXT },
      { name: '후기 타입', type: 'reviewType', dataType: FilterDataTypeGroup.REVIEW_TYPE },
      { name: '후기 상태', type: 'status', dataType: FilterDataTypeGroup.REVIEW_STATUS },
      { name: '후기 내용', type: 'reviewContent', dataType: FilterDataTypeGroup.TEXT },
      { name: '작성 일자', type: 'writeDate', dataType: FilterDataTypeGroup.DATE_RANGE }
    ];

    return (
      <div className={classnames(s.root, ' box box-default')}>
        <SearchBar
          filter={filter}
          searchOptions={searchOptions}
          onClickSearch={this.onClickSearch}
          openNotificationDialog={this.openNotificationDialog}
          onExportCSV={this.onExportCSV}
          onClickAddSearchOption={this.onAddSearchOption}
          onClickDeleteSearchOption={this.onDeleteSearchOption}
          onChangeSearchOption={this.onChangeSearchOption}
        />
        <div className="box-body padding-sm">
          <table className={classnames('table', s.table)}>
            <thead>
              <TableHeaderRow
                data={headers}
                orderOption={orderOption}
                onChangeOrderColumn={this.onChangeOrderColumn}
                openNotificationDialog={this.openNotificationDialog}
              />
            </thead>
            <tbody>{this.renderReviewItem()}</tbody>
          </table>
          <div>
            <FlatPagination
              offset={page}
              limit={1}
              total={maxPage}
              onClick={(e, offset) => this.onClickPage(offset)}
            />
          </div>
          <EditDialog
            open={this.state.dialogOpen}
            onCloseDialog={this.onCloseDialog}
            onSaveDialog={this.onSaveDialog}
            data={this.state.editData}
          />
        </div>

        <NotificationDialog
          open={notificationDialog}
          title={notificationDialogTitle}
          message={notificationDialogMessage}
          onClose={this.onClickCloseNotificationDialog}
        />
      </div>
    );
  }
}

Reviews.propTypes = {};

Reviews.defaultProps = {};

export default withStyles(s)(Reviews);
