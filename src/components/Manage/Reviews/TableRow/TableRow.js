import EnhancedButton from 'material-ui/internal/EnhancedButton';
import RaisedButton from 'material-ui/RaisedButton';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './TableRow.css';
import SimpleDateFormat from '../../../../util/dataFormat/SimpleDateFormat';
import ReviewTypeGroup from '../../../../util/const/ReviewTypeGroup';
import ReviewStatusGroup from '../../../../util/const/ReviewStatusGroup';
import getUserNameText from '../../../../util/user/getUserNameText';
import getUserRoleText from '../../../../util/user/getUserRoleText';

const buttonStyle = {
  zIndex: '0',
};

class TableRow extends React.Component {
  getReviewTypeText = (reviewType) => {
    let text = '없음';
    if (reviewType && reviewType === ReviewTypeGroup.RECRUIT) {
      text = '채용';
    }
    if (reviewType && reviewType === ReviewTypeGroup.INTERVIEW) {
      text = '인터뷰';
    }
    if (reviewType && reviewType === ReviewTypeGroup.FAILURE) {
      text = '실패';
    }
    return text;
  };

  getReviewStatusText = (status) => {
    let text = '없음';
    if (status && status === ReviewStatusGroup.CLOSE) {
      text = '닫힘';
    }
    if (status && status === ReviewStatusGroup.OPEN) {
       text = '열림';
    }
    return text;
  };

  onClickEditReview = (data) => {
    this.props.onClickEdit(data);
  };

  onClickRemoveReview = (reviewId) => {
    this.props.onClickRemove(reviewId);
  };

  onClickReviewContent = (reviewContent) => {
    if (reviewContent) {
      this.props.openNotificationDialog('후기 내용', reviewContent);
    } else {
      this.props.openNotificationDialog('후기 내용', '입력된 후기 내용이 없습니다.');
    }
  };

  onClickUserId = (userId) => {
    window.open(`/users?userId=${userId}`);
  };

  render() {
    const { data } = this.props;
    const {
      reviewId,
      applyId,
      senderId,
      targetId,
      reviewRate,
      reviewContent,
      reviewType,
      writeDate,
      status,
      sender,
      target,
    } = data;
    return (
      <tr className={s.row}>
        <td>{reviewId}</td>
        <td>{applyId || '없음'}</td>
        <td>
          <RaisedButton
            label={senderId}
            onClick={() => this.onClickUserId(senderId)}
            buttonStyle={buttonStyle}
          />
        </td>
        <td>{getUserNameText(sender)}</td>
        <td>{getUserRoleText(sender)}</td>
        <td>
          <RaisedButton
            label={targetId}
            onClick={() => this.onClickUserId(targetId)}
            buttonStyle={buttonStyle}
          />
        </td>
        <td>{getUserNameText(target)}</td>
        <td>{getUserRoleText(target)}</td>
        <td>{reviewRate}</td>
        <td>
          <EnhancedButton
            onClick={() => this.onClickReviewContent(reviewContent)}
            style={buttonStyle}
          >
            <p className={s.reviewContent}>{reviewContent}</p>
          </EnhancedButton>
        </td>
        <td>{this.getReviewTypeText(reviewType)}</td>
        <td>{this.getReviewStatusText(status)}</td>
        <td>{SimpleDateFormat(writeDate)}</td>
        <td>
          <RaisedButton
            label="수정"
            onClick={() => this.onClickEditReview(data)}
            buttonStyle={buttonStyle}
          />
          <RaisedButton
            label="삭제"
            onClick={() => this.onClickRemoveReview(reviewId)}
            buttonStyle={buttonStyle}
          />
        </td>
      </tr>
    );
  }
}

TableRow.propTypes = {
  data: PropTypes.object.isRequired,
  onClickEdit: PropTypes.func.isRequired,
  onClickRemove: PropTypes.func.isRequired,
  openNotificationDialog: PropTypes.func.isRequired,
};

TableRow.defaultProps = {};

export default withStyles(s)(TableRow);
