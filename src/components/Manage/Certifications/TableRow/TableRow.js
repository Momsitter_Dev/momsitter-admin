import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './TableRow.css';
import MenuItem from 'material-ui/MenuItem';
import DropDownMenu from 'material-ui/DropDownMenu';
import SimpleDateFormat from '../../../../util/dataFormat/SimpleDateFormat';
import CertificateTypeIdGroup from '../../../../util/const/CertificateTypeIdGroup';
import CertificateStatusGroup from '../../../../util/const/CertificateStatusGroup';
import UnivDegreeTypeGroup from '../../../../util/const/UnivDegreeTypeGroup';
import UnivStatusTypeGroup from '../../../../util/const/UnivStatusTypeGroup';
import RaisedButton from 'material-ui/RaisedButton';
import getUserNameText from '../../../../util/user/getUserNameText';
import getUserRoleTexts from '../../../../util/user/getUserRoleText';

const buttonStyle = {
  zIndex: '0',
};

class TableRow extends React.Component {
  constructor(props) {
    super(props);
    this.onChangeCertStatus = this.onChangeCertStatus.bind(this);
  }
  shouldComponentUpdate(nextProps) {
    return true;
  }

  onChangeCertStatus = async function (event, index, value) {
    const { certId, userId } = this.props.data;
    this.props.onChangeCertStatus(certId, value, userId);
  };

  onClickUserId = (userId) => {
    window.open(`/users?userId=${userId}`);
  };

  onClickRemoveCert = (certId, userId) => {
    if (typeof window !== 'undefined') {
      if (window.confirm('정말 삭제하시겠습니까?')) {
        if (certId && userId) {
          this.props.onRemove(certId, userId);
        } else {
          this.props.openNotificationDialog('오류', '인증정보를 찾을 수 없습니다.');
        }
      }
    } else {
      this.props.openNotificationDialog('오류', '브라우저 환경에서 이용해 주세요.');
    }
  };

  getCertTypeText = certTypeId => {
    let certTypeText = '없음';
    if (certTypeId === CertificateTypeIdGroup.ENROLLMENT) {
      certTypeText = '학교';
    } else if (certTypeId === CertificateTypeIdGroup.MEDICAL_EXAMINATION) {
      certTypeText = '건강';
    } else if (certTypeId === CertificateTypeIdGroup.FAMILY_RELATION) {
      certTypeText = '엄마';
    } else if (certTypeId === CertificateTypeIdGroup.CHILDCARE) {
      certTypeText = '자격증';
    } else if (certTypeId === CertificateTypeIdGroup.RESIDENT_REGISTRATION) {
      certTypeText = '등초본';
    } else if (certTypeId === CertificateTypeIdGroup.PERSONALITY) {
      certTypeText = '인성';
    }
    return certTypeText;
  };

  getUnivDegreeText = (degree) => {
    degree = Number(degree);
    let text = '';
    if (degree === UnivDegreeTypeGroup.BACHELOR) {
      text = '학사';
    }
    if (degree === UnivDegreeTypeGroup.MASTER) {
      text = '석사';
    }
    if (degree === UnivDegreeTypeGroup.DOCTOR) {
      text = '박사';
    }
    return text;
  };

  getUnivStatusText = (status) => {
    status = Number(status);
    let text = '';
    if (status === UnivStatusTypeGroup.ATTENDING) {
      text = '재학';
    }
    if (status === UnivStatusTypeGroup.GRADUATE) {
      text = '졸업';
    }
    if (status === UnivStatusTypeGroup.LEAVE) {
      text = '휴학';
    }
    if (status === UnivStatusTypeGroup.WITHDRAW) {
      text = '퇴학';
    }
    return text;
  };

  getChildCountText = (children) => {
    let text = '';
    if (children && children.length > 0) {
      text = children.length + '명';
    } else {
      text = '없음';
    }
    return text;
  };

  getGraduateDate = (data) => {
    let ui = null;
    if (data) {
      const { status, graduateDate } = data;
      if (status && status === UnivStatusTypeGroup.GRADUATE) {
        ui = (
          <div className={s.innerRow}>
            <label>졸업일자</label>
            <span style={{ fontSize: '0.8em' }}>{SimpleDateFormat(graduateDate)}</span>
          </div>
        );
      }
    }
    return ui;
  };

  render() {
    const { data } = this.props;
    const {
      certId,
      fileId,
      certTypeId,
      userId,
      regDate,
      issueDate,
      certFile,
      certStatus,
      updateDate,
      certData,
      certUser,
    } = data;

    let updateDateStr = '없음';
    if (updateDate) {
      updateDateStr = SimpleDateFormat(updateDate);
    }

    let fileInfo = '없음';
    // TODO: 이것도 미리 볼 수 있는 UI가 되어야 하지 않을까...?
    if (fileId && certFile) {
      fileInfo = (
        <a href={certFile.fileUrl} target="_blank">
          {fileId}
        </a>
      );
    }

    return (
      <tr className={s.row}>
        <td>{certId}</td>
        <td>
          <RaisedButton
            label={userId}
            onClick={() => this.onClickUserId(userId)}
            buttonStyle={buttonStyle}
            style={{ width: 'auto', minWidth: 'auto' }}
          />
        </td>
        <td>{getUserNameText(certUser)}</td>
        <td>{getUserRoleTexts(certUser)}</td>
        <td>{fileInfo}</td>
        <td> {this.getCertTypeText(certTypeId)} </td>
        <td>{SimpleDateFormat(regDate)}</td>
        <td>{SimpleDateFormat(issueDate)}</td>
        <td>{updateDateStr}</td>
        <td>
          <DropDownMenu value={certStatus} onChange={this.onChangeCertStatus}>
            <MenuItem value={CertificateStatusGroup.WAITING} primaryText="인증대기" />
            <MenuItem value={CertificateStatusGroup.APPROVED} primaryText="승인" />
            <MenuItem value={CertificateStatusGroup.REJECTED} primaryText="거절" />
            <MenuItem value={CertificateStatusGroup.NONE} primaryText="없음" />
          </DropDownMenu>
        </td>
        <td>
          <RaisedButton
            label="삭제"
            secondary={true}
            onClick={() => this.onClickRemoveCert(certId, userId)}
            buttonStyle={buttonStyle}
          />
        </td>
      </tr>
    );
  }
}

TableRow.propTypes = {
  data: PropTypes.object.isRequired,
  openNotificationDialog: PropTypes.func.isRequired,
  openMainCertLabelDialog: PropTypes.func.isRequired,
  onChangeCertStatus: PropTypes.func.isRequired,
  onRemove: PropTypes.func.isRequired,
};

TableRow.defaultProps = {};

export default withStyles(s)(TableRow);
