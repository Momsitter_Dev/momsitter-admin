import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CertificateItem.css';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import SimpleDateFormat from '../../../../util/dataFormat/SimpleDateFormat';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import CertificateStatusGroup from '../../../../util/const/CertificateStatusGroup';
import CertificateTypeIdGroup from '../../../../util/const/CertificateTypeIdGroup';
import UnivStatusTypeGroup from '../../../../util/const/UnivStatusTypeGroup';
import UnivDegreeTypeGroup from '../../../../util/const/UnivDegreeTypeGroup';

class CertificateItem extends React.Component {
  constructor(props) {
    super(props);
    this.onChangeCertStatus = this.onChangeCertStatus.bind(this);
  }

  shouldComponentUpdate() {
    return true;
  }

  onChangeCertStatus = async function (event, index, value) {
    const { certId, userId } = this.props.data;
    this.props.onChangeCertStatus(certId, value, userId);
  };

  onClickUserId = (userId) => {
    window.open(`/users?userId=${userId}`);
  };

  getCertTypeText = certTypeId => {
    let certTypeText = '없음';
    if (certTypeId === CertificateTypeIdGroup.ENROLLMENT) {
      certTypeText = '학교';
    } else if (certTypeId === CertificateTypeIdGroup.MEDICAL_EXAMINATION) {
      certTypeText = '건강';
    } else if (certTypeId === CertificateTypeIdGroup.FAMILY_RELATION) {
      certTypeText = '엄마';
    } else if (certTypeId === CertificateTypeIdGroup.CHILDCARE) {
      certTypeText = '자격증';
    } else if (certTypeId === CertificateTypeIdGroup.RESIDENT_REGISTRATION) {
      certTypeText = '등초본';
    } else if (certTypeId === CertificateTypeIdGroup.PERSONALITY) {
      certTypeText = '인성';
    }
    return certTypeText;
  };

  getUnivDegreeText = (degree) => {
    degree = Number(degree);
    let text = '';
    if (degree === UnivDegreeTypeGroup.BACHELOR) {
      text = '학사';
    }
    if (degree === UnivDegreeTypeGroup.MASTER) {
      text = '석사';
    }
    if (degree === UnivDegreeTypeGroup.DOCTOR) {
      text = '박사';
    }
    return text;
  };

  getUnivStatusText = (status) => {
    status = Number(status);
    let text = '';
    if (status === UnivStatusTypeGroup.ATTENDING) {
      text = '재학';
    }
    if (status === UnivStatusTypeGroup.GRADUATE) {
      text = '졸업';
    }
    if (status === UnivStatusTypeGroup.LEAVE) {
      text = '휴학';
    }
    if (status === UnivStatusTypeGroup.WITHDRAW) {
      text = '퇴학';
    }
    return text;
  };

  getChildCountText = (children) => {
    let text = '';
    if (children && children.length > 0) {
      text = children.length + '명';
    } else {
      text = '없음';
    }
    return text;
  };

  renderCertData = (data) => {
    let ui = null;
    const {
      certStatus,
      certData,
      certTypeId,
    } = data;

    try {
      const parsedData = JSON.parse(certData);
      if (certStatus && certData && certTypeId) {
        if (certTypeId === CertificateTypeIdGroup.ENROLLMENT) {
          ui = (
            <div className={s.innerRowWrap}>
              <div className={s.innerRow}>
                <label>학교명</label>
                <span>{parsedData.univ || parsedData.univName}</span>
              </div>
              <div className={s.innerRow}>
                <label>학과</label>
                <span>{parsedData.major}</span>
              </div>
              <div className={s.innerRow}>
                <label>학위</label>
                <span>{parsedData.univDegree || this.getUnivDegreeText(parsedData.degree)}</span>
              </div>
              <div className={s.innerRow}>
                <label>학년</label>
                <span>{parsedData.univGrade || (parsedData.grade + '학년')}</span>
              </div>
              <div className={s.innerRow}>
                <label>학사상태</label>
                <span>{parsedData.univStatus || this.getUnivStatusText(parsedData.status)}</span>
              </div>
              <div className={s.innerRow}>
                <label>학사상태</label>
                <span style={{ fontSize: '0.8em' }}>{SimpleDateFormat(parsedData.issueDate)}</span>
              </div>
            </div>
          );
        }

        if (certTypeId === CertificateTypeIdGroup.FAMILY_RELATION) {
          ui = (
            <div className={s.innerRowWrap}>
              <div className={s.innerRow}>
                <label>아이수</label>
                <span>{parsedData.childCount || this.getChildCountText(parsedData.children)}</span>
              </div>
            </div>
          );
        }

        if (certTypeId === CertificateTypeIdGroup.CHILDCARE) {
          ui = (
            <div className={s.innerRowWrap}>
              <div className={s.innerRow}>
                <label>자격증 이름</label>
                <span>{parsedData.qualificationType || '확인필요'}</span>
              </div>
            </div>
          );
        }
      } else {
        ui = (
          <span>오류!</span>
        );
      }
    } catch (e) {
      ui = (
        <span>오류!</span>
      );
    }
    return ui;
  };

  render() {
    const { data } = this.props;
    const {
      certId,
      fileId,
      certTypeId,
      userId,
      regDate,
      issueDate,
      certFile,
      certStatus,
      updateDate,
      certData,
    } = data;

    let updateDateStr = '없음';
    if (updateDate) {
      updateDateStr = SimpleDateFormat(updateDate);
    }
    let fileInfo = '없음';
    // TODO: 이것도 미리 볼 수 있는 UI가 되어야 하지 않을까...?
    if (fileId && certFile) {
      fileInfo = (
        <a href={certFile.fileUrl} target="_blank">
          {fileId}
        </a>
      );
    }

    return (
      <Paper>
        <div className={s.root}>
          <div className={s.dataRow}>
            <label className={s.datalabel}>인증번호</label>
            <span classname={s.dataValue}>{certId}</span>
          </div>
          <div className={s.dataRow}>
            <label className={s.datalabel}>사용자 번호</label>
            <span classname={s.dataValue}>
              <RaisedButton
                label={userId}
                onClick={() => this.onClickUserId(userId)}
              />
            </span>
          </div>
          <div className={s.dataRow}>
            <label className={s.datalabel}>인증상태</label>
            <SelectField value={certStatus} onChange={this.onChangeCertStatus}>
              <MenuItem value={CertificateStatusGroup.WAITING} primaryText="인증대기" />
              <MenuItem value={CertificateStatusGroup.APPROVED} primaryText="승인" />
              <MenuItem value={CertificateStatusGroup.REJECTED} primaryText="거절" />
              <MenuItem value={CertificateStatusGroup.NONE} primaryText="없음" />
            </SelectField>
          </div>
          <div className={s.dataRow}>
            <label className={s.datalabel}>인증타입</label>
            <span classname={s.dataValue}>{this.getCertTypeText(certTypeId)}</span>
          </div>
          <div className={s.dataRow}>
            <label className={s.datalabel}>등록일자</label>
            <span classname={s.dataValue}>{SimpleDateFormat(regDate)}</span>
          </div>
          <div className={s.dataRow}>
            <label className={s.datalabel}>발급일자</label>
            <span classname={s.dataValue}>{SimpleDateFormat(issueDate)}</span>
          </div>
          <div className={s.dataRow}>
            <label className={s.datalabel}>갱신일자</label>
            <span classname={s.dataValue}>{updateDateStr}</span>
          </div>
          <Paper>
            <div className={s.certDataWrap}>
              {this.renderCertData(data)}
            </div>
          </Paper>
        </div>
      </Paper>
    );
  }
}

CertificateItem.propTypes = {
  data: PropTypes.object.isRequired,
  openNotificationDialog: PropTypes.func.isRequired,
  openMainCertLabelDialog: PropTypes.func.isRequired,
  onChangeCertStatus: PropTypes.func.isRequired,
};

CertificateItem.defaultProps = {};

export default withStyles(s)(CertificateItem);
