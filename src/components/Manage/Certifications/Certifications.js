import qs from 'qs';
import uuid from 'uuid/v4';
import _ from 'lodash';
import moment from 'moment';
import classnames from 'classnames';
import FlatPagination from 'material-ui-flat-pagination';
import RaisedButton from 'material-ui/RaisedButton';
import Drawer from 'material-ui/Drawer';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Certifications.css';
import SearchBar from '../../SearchBar';
import TableHeaderRow from '../../TableHeaderRow';
import Request from '../../../util/Request';
import TableRow from './TableRow';
import CertInfoRow from './CertInfoRow';
import CertificateStatusGroup from '../../../util/const/CertificateStatusGroup';
import UserMainCertTypeIdGroup from '../../../util/const/UserMainCertTypeIdGroup';
import ResponseResultTypeGroup from '../../../util/const/ResponseResultTypeGroup';
import RejectCertDialog from '../../Dialog/RejectCertDialog';
import EditCertChildCareDialog from '../../Dialog/EditCertChildCareDialog';
import EditCertEnrollmentDialog from '../../Dialog/EditCertEnrollmentDialog';
import EditCertFamilyDialog from '../../Dialog/EditCertFamilyDialog';
import EditCertMedicalDialog from '../../Dialog/EditCertMedicalDialog';
import CertificateTypeIdGroup from '../../../util/const/CertificateTypeIdGroup';
import FilterDataTypeGroup from '../../../util/const/FilterDataTypeGroup';
import MainCertLabelDialog from '../../Dialog/MainCertLabelDialog';
import PersonalityResult from '../../PersonalityResult';
import NotificationDialog from '../../Dialog/NotificationDialog';

class Certifications extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      page: 0,
      maxPage: 0,
      notificationDialog: false,
      notificationDialogTitle: '',
      notificationDialogMessage: '',
      rejectDialog: false,
      rejectDialogData: {},
      mainCertLabelDialog: false,
      actionCertInfo: null,
      personality: null,
      personalityDrawer: false,
      editCertData: {
        open: false,
        data: null,
        certId: null,
        certData: null,
        onLoading: false,
      },
      searchOptions: [],
      orderOption: {
        column: '',
        method: '',
      },
    };
    this.cachedOption = {};

    this.onClickSearch = this.onClickSearch.bind(this);
    this.onRemoveCertItem = this.onRemoveCertItem.bind(this);
    this.certRemoveRequest = this.certRemoveRequest.bind(this);
    this.onChangeCertStatus = this.onChangeCertStatus.bind(this);
    this.changeCertStatusRequest = this.changeCertStatusRequest.bind(this);
    this.saveRejectCertDialog = this.saveRejectCertDialog.bind(this);
    this.onExportCSV = this.onExportCSV.bind(this);
    this.onSaveCertData = this.onSaveCertData.bind(this);
    this.onChangeOrderColumn = this.onChangeOrderColumn.bind(this);

    this.openDrawer = this.openDrawer.bind(this);
    this.closeDrawer = this.closeDrawer.bind(this);
    this.onClickPersonalityResult = this.onClickPersonalityResult.bind(this);
    this.onSaveMainCertLabelDialog = this.onSaveMainCertLabelDialog.bind(this);
    this.changeMainCertRequest = this.changeMainCertRequest.bind(this);
    this.currentUserId = null;
  }

  async componentDidMount() {
    await this.doSearch({})
      .then(data => {
        this.setState({
          list: data.data.rows,
          page: data.page.currentPage,
          maxPage: data.page.maxPage,
        });
      })
      .catch(() => {
        this.setState({ list: [] });
      });
  }

  onClickPersonalityResult = async function (userId) {
    if (!userId) {
      return alert('인성검사 결과를 표시할 수 없습니다.[1]');
    }
    this.currentUserId = userId;
    return this.openDrawer();
  };

  openDrawer = () => {
    return this.setState({ personalityDrawer: true });
  };

  closeDrawer = () => {
    return this.setState({ personalityDrawer: false });
  };

  doSearch = options =>
    new Promise(async (resolve, reject) => {
      const query = qs.stringify(options);
      this.cachedOption = Object.assign({}, options);

      const searchResult = await Request(
        `api/manage/user/certifications?${query}`,
        { method: 'GET' },
      ).catch(() => false);
      if (
        searchResult &&
        searchResult.result === ResponseResultTypeGroup.SUCCESS
      ) {
        resolve(searchResult);
      } else {
        reject();
      }
    });

  onClickSearch = async function () {
    const { searchOptions, orderOption } = this.state;
    await this.doSearch({
      searchOptions,
      orderOption,
      requestPage: 0,
    })
      .then(data => {
        this.setState({
          list: data.data.rows,
          page: data.page.currentPage,
          maxPage: data.page.maxPage,
        });
      })
      .catch(() => {
        this.setState({ list: [] });
      });
  };

  onClickPage = value => {
    this.cachedOption.requestPage = value;
    this.doSearch(this.cachedOption)
      .then(data => {
        this.setState({
          list: data.data.rows,
          page: data.page.currentPage,
          maxPage: data.page.maxPage,
        });
      })
      .catch(() => {
        this.setState({ list: [] });
      });
  };

  onChangeOrderColumn = async function (columnId) {
    const { orderOption, searchOptions } = this.state;
    if (orderOption.column !== columnId) {
      orderOption.method = '';
    }
    orderOption.column = columnId;
    if (orderOption.method === 'DESC') {
      orderOption.method = 'ASC';
    } else if (orderOption.method === 'ASC') {
      orderOption.method = '';
    } else {
      orderOption.method = 'DESC';
    }
    this.setState({ orderOption });

    await this.doSearch({
      searchOptions,
      orderOption,
      requestPage: 0,
    })
      .then(data => {
        this.setState({
          list: data.data.rows,
          page: data.page.currentPage,
          maxPage: data.page.maxPage,
        });
      })
      .catch(() => {
        this.setState({ list: [] });
      });
  };

  onRemoveCertItem = async function (certId, userId) {
    if (certId) {
      const { list } = this.state;
      const response = await this.certRemoveRequest(certId, userId);
      if (response && response.result === ResponseResultTypeGroup.SUCCESS) {
        const targetIndex = _.findIndex(list, o => o.certId === certId);
        if (targetIndex !== -1) {
          list.splice(targetIndex, 1);
          this.setState({ list });
        }
        this.openNotificationDialog('알림', '삭제되었습니다.');
      } else {
        this.openNotificationDialog('오류', '인증 정보를 지울 수 없습니다.');
      }
    } else {
      this.openNotificationDialog('오류', '인증 정보를 찾을 수 없습니다.');
    }
  };

  certRemoveRequest = async function (certId, userId) {
    return await Request(`api/manage/user/certifications/${certId}`, {
      method: 'DELETE',
      body: {
        userId,
      },
    }).catch(err => {
      console.log(err);
      return false;
    });
  };

  openNotificationDialog = (title, message) => {
    this.setState({
      notificationDialog: true,
      notificationDialogTitle: title || '알림',
      notificationDialogMessage: message,
    });
  };

  onClickCloseNotificationDialog = () => {
    this.setState({
      notificationDialog: false,
    });
  };

  openMainCertLabelDialog = certData => {
    this.setState({
      mainCertLabelDialog: true,
      actionCertInfo: certData,
    });
  };

  onSaveMainCertLabelDialog = async function (labelText) {
    const { actionCertInfo, list } = this.state;
    if (actionCertInfo) {
      const response = await this.changeMainCertRequest(
        Object.assign({}, actionCertInfo, { certLabelName: labelText }),
      );
      if (response && response.result === 'success') {
        const targetInd = _.findIndex(
          list,
          o => o.certId === actionCertInfo.certId,
        );
        list[targetInd].certStatus = actionCertInfo.certStatus;
        list[targetInd].updateDate = moment();
        this.setState({ list: Object.assign([], list) });
        this.openNotificationDialog(
          '알림',
          <div>
            <div> 메인 인증이 반영되었습니다. </div>
          </div>,
        );
        this.onCloseMainCertLabelDialog();
      } else {
        this.openNotificationDialog('오류', '메인 인증 - 네트워크 오류입니다.');
      }
    } else {
      this.openNotificationDialog('오류', 'actionInfo가 Null 입니다.');
    }
  };

  changeMainCertRequest = async certData =>
    await Request(`api/manage/user/certifications/${certData.certId}/main`, {
      method: 'PUT',
      data: certData,
    }).catch(() => false);

  changeMainCertRequestByAuto = async certData =>
    await Request(
      `api/manage/user/certifications/${certData.certId}/main/auto`,
      {
        method: 'PUT',
        data: certData,
      },
    ).catch(() => false);

  onCloseMainCertLabelDialog = () => {
    this.setState({
      mainCertLabelDialog: false,
      actionCertInfo: null,
    });
  };

  onChangeCertStatus = async function (certId, value, userId) {
    const { list } = this.state;
    const targetInd = _.findIndex(list, o => o.certId === certId);
    if (targetInd !== -1) {
      try {
        const targetCertInfo = list[targetInd];
        if (targetCertInfo.certStatus !== value) {
          const isUserMainCert = await this.isMainCertOfUser(targetCertInfo);
          if (isUserMainCert instanceof Error) {
            this.openNotificationDialog(
              '오류',
              '회원 타입을 확인 할 수 없습니다.[1]',
            );
          } else if (confirm('수정하시겠습니까?')) {
            if (isUserMainCert && value === CertificateStatusGroup.APPROVED) {
              if (targetCertInfo.certData) {
                const response = await this.changeMainCertRequestByAuto({
                  ...targetCertInfo,
                  certStatus: value,
                });
                if (response && response.result === 'success') {
                  const message = response.message;
                  const newList = Object.assign([], list);
                  newList[targetInd].certStatus = value;
                  newList[targetInd].updateDate = moment();
                  this.setState({
                    list: newList,
                  });
                  if (message) {
                    this.openNotificationDialog(
                      '알림',
                      '저장하고 메세지를 보냈습니다.',
                    );
                  } else {
                    this.openNotificationDialog('알림', '저장되었습니다.');
                  }
                } else if (response && response.err && response.err.message) {
                  this.openNotificationDialog('오류', response.err.message);
                } else {
                  this.openNotificationDialog('오류', '네트워크 오류입니다.');
                }
              } else {
                this.openMainCertLabelDialog({
                  userId: targetCertInfo.userId,
                  certId,
                  certTypeId: targetCertInfo.certTypeId,
                  certStatus: value,
                });
              }
            } else if (value === CertificateStatusGroup.REJECTED) {
              this.openRejectCertDialog({
                certId,
                userId,
              });
            } else {
              const response = await this.changeCertStatusRequest(
                certId,
                value,
                userId,
              );
              if (response && response.result === 'success') {
                const responseMessage = response.message;
                const newList = Object.assign([], list);
                newList[targetInd].certStatus = value;
                newList[targetInd].updateDate = moment();
                this.setState({
                  list: newList,
                });
                if (responseMessage) {
                  if (responseMessage.send) {
                    this.openNotificationDialog(
                      '알림',
                      '저장하고 회원에게 메세지를 발송하였습니다.',
                    );
                  } else {
                    this.openNotificationDialog('알림', '저장되었습니다.');
                  }
                } else {
                  this.openNotificationDialog('알림', '저장되었습니다.');
                }
              } else {
                this.openNotificationDialog('오류', '네트워크 오류입니다.');
              }
            }
          }
        }
      } catch (err) {
        this.openNotificationDialog('오류', '오류가 발생하였습니다.');
      }
    } else {
      this.openNotificationDialog('오류', 'certId를 찾을 수 없습니다.');
    }
  };

  changeCertStatusRequest = async function (certId, status, userId, rejectMsg) {
    return await Request(
      `api/manage/user/certifications/${certId}/status`,
      {
        method: 'PUT',
        data: {
          certStatus: status,
          userId,
          rejectMsg,
        },
      },
    ).catch(() => false);
  };

  isMainCertOfUser = data =>
    new Promise((resolve, reject) => {
      let isMain = false;
      const { certTypeId, certUser } = data;
      try {
        if (certUser && certUser.userRole) {
          const userRole = certUser.userRole;
          for (let i = 0; i < userRole.length; i += 1) {
            if (
              certTypeId === UserMainCertTypeIdGroup[userRole[i].userTypeId]
            ) {
              isMain = true;
              break;
            }
          }
          resolve(isMain);
        } else {
          reject('회원 타입을 확인할 수 없습니다.');
        }
      } catch (err) {
        reject(new Error('개발자를 호출해 주세요.'));
      }
    });

  onEditCertData = data => {
    this.openCertDataEditDialog(data);
  };

  openCertDataEditDialog = data => {
    const { editCertData } = this.state;
    let parsedData = null;
    try {
      parsedData = JSON.parse(data.certData);
      this.setState({
        editCertData: {
          ...editCertData,
          open: true,
          data,
          onLoading: false,
          certId: data.certId,
          certData: parsedData,
        },
      });
    } catch (e) {
      console.log(e);
    }
  };

  closeCertDataEditDialog = () => {
    const { editCertData } = this.state;
    this.setState({
      editCertData: {
        ...editCertData,
        open: false,
        data: null,
        certId: null,
        certData: null,
        onLoading: false,
      },
    });
  };

  openRejectCertDialog = data => {
    this.setState({
      rejectDialog: true,
      rejectDialogData: {
        rejectMsg: '',
        userId: data.userId,
        certId: data.certId,
      },
    });
  };

  closeRejectCertDialog = () => {
    this.setState({
      rejectDialog: false,
      rejectDialogData: {},
    });
  };

  onChangeRejectDialogValue = value => {
    const { rejectDialogData } = this.state;
    this.setState({
      rejectDialogData: {
        ...rejectDialogData,
        rejectMsg: value,
      },
    });
  };

  saveRejectCertDialog = async function () {
    const { list, rejectDialogData } = this.state;
    const response = await this.changeCertStatusRequest(
      rejectDialogData.certId,
      CertificateStatusGroup.REJECTED,
      rejectDialogData.userId,
      rejectDialogData.rejectMsg,
    );
    if (response && response.result === ResponseResultTypeGroup.SUCCESS) {
      try {
        const newList = Object.assign([], list);
        const targetInd = _.findIndex(
          list,
          o => o.certId === rejectDialogData.certId,
        );
        newList[targetInd].certStatus = CertificateStatusGroup.REJECTED;
        newList[targetInd].updateDate = moment();
        this.setState({
          list: newList,
        });
      } catch (e) {
        console.log(e);
      }
      this.openNotificationDialog('알림', '저장되었습니다.');
      this.closeRejectCertDialog();
    } else {
      this.openNotificationDialog('오류', '네트워크 오류입니다.');
      this.closeRejectCertDialog();
    }
  };

  onExportCSV = async function () {
    const queryString = qs.stringify(this.cachedOption);
    window.open(`/api/manage/user/certifications/csv?${queryString}`);
  };

  onChangeCertData = (type, value) => {
    const { editCertData } = this.state;
    if (!editCertData || !editCertData.certData) {
      return false;
    }
    const certData = editCertData.certData;

    if (!certData) {
      return false;
    }

    certData[type] = value;
    this.setState({
      editCertData: {
        ...editCertData,
        certData,
      },
    });
  };

  onSaveCertData = async function () {
    const { editCertData } = this.state;
    const { list } = this.state;
    this.setState({
      editCertData: {
        ...editCertData,
        onLoading: true,
      },
    });

    if (editCertData && editCertData.data) {
      const { data, certData } = editCertData;
      const certId = data.certId;
      const userId = data.userId;
      const targetInd = _.findIndex(list, o => o.certId === certId);

      if (targetInd !== -1) {
        const makeCleanCertData = this.getCertDataTransformByCertTypeId(
          list[targetInd].certTypeId,
          certData,
        );
        if (makeCleanCertData) {
          const response = await Request(
            `api/users/${userId}/certifications/${certId}`,
            {
              method: 'PUT',
              data: {
                ...data,
                certData: JSON.stringify(makeCleanCertData),
              },
            },
          ).catch(err => {
            console.log(err);
            return false;
          });
          if (response && response.result === ResponseResultTypeGroup.SUCCESS) {
            this.openNotificationDialog('알림', '변경사항이 저장되었습니다.');
            this.closeCertDataEditDialog();
            list[targetInd].certData = JSON.stringify(makeCleanCertData);
            this.setState({ list });
          } else {
            this.openNotificationDialog('오류', '네트워크 오류입니다.');
          }
        } else {
          this.openNotificationDialog(
            '오류',
            '인증 데이터를 생성할 수 없습니다.',
          );
        }
      } else {
        this.openNotificationDialog(
          '오류',
          '인증 정보를 찾을 수 없습니다... 다시 시도해 주세요.',
        );
      }
    } else {
      this.openNotificationDialog(
        '오류',
        '인증 정보를 찾을 수 없습니다... 다시 시도해 주세요.',
      );
    }
  };

  getCertDataTransformByCertTypeId = (certTypeId, certData) => {
    let result = null;
    if (certTypeId && certData) {
      if (certTypeId === CertificateTypeIdGroup.ENROLLMENT) {
        result = this.makeEnrollmentData(certData);
      } else if (certTypeId === CertificateTypeIdGroup.MEDICAL_EXAMINATION) {
        result = this.makeMedicalExaminationData(certData);
      } else if (certTypeId === CertificateTypeIdGroup.CHILDCARE) {
        result = this.makeChildCareData(certData);
      } else if (certTypeId === CertificateTypeIdGroup.FAMILY_RELATION) {
        result = this.makeFamilyData(certData);
      } else if (certTypeId === CertificateTypeIdGroup.RESIDENT_REGISTRATION) {
        result = this.makeResidentRegistrationData(certData);
      } else {
        result = null;
      }
    }
    return result;
  };

  makeEnrollmentData = certData => {
    /*
    as is : {"univName":"백석예술대학교/본교","univDegree":"학사","univGrade":4,"univStatus":"졸업","major":"사회복지학과","issueDate":"2015-02-10 00:00:00"}
    to be : {"univ":"가천대학교","major":"유아교육학과","degree":1,"status":1,"grade":4,"issueDate":"2017-01-04T23:14:28.871Z","graduateDate":null}
     */
    let result = null;
    try {
      result = {
        univ: certData.univName,
        major: certData.major,
        degree: certData.univDegree,
        grade: certData.univGrade,
        status: certData.univStatus,
        issueDate: certData.issueDate,
        graduateDate: certData.graduateDate,
        expelledDate: certData.expelledDate,
      };
      // 기존 데이터 포멧이 맞춰서 테이터를 세팅한 후, 바뀐포멧으로 덮어씌움
      if (certData.univ) {
        result.univ = certData.univ;
      }
      if (certData.degree) {
        result.degree = certData.degree;
      }
      if (certData.status) {
        result.status = certData.status;
      }
      if (certData.grade) {
        result.grade = certData.grade;
      }
    } catch (e) {
      console.log(e);
      result = null;
    }
    return result;
  };

  makeMedicalExaminationData = certData => {
    /*
    {"issueDate":"2016-09-19 00:00:00"}
     */
    let result = null;
    try {
      result = {
        issueDate: certData.issueDate,
      };
    } catch (e) {
      console.log(e);
      result = null;
    }
    return result;
  };

  makeChildCareData = certData => {
    /*
    {"issueDate":"2008-03-12T14:02:53.320Z","qualificationType":"보육교사1급"}
    */
    let result = null;
    try {
      result = {
        issueDate: certData.issueDate,
        qualificationType: certData.qualificationType,
      };
    } catch (e) {
      console.log(e);
      result = null;
    }
    return result;
  };

  makeFamilyData = certData => {
    /*
    as is : {"issueDate":"2016-11-30T15:00:00.000Z","children":[{"gender":"women","birthday":"2008-09-06T15:00:00.000Z"},{"gender":"man","birthday":"2013-01-01T15:00:00.000Z"}]}
    to bd : {"childCount":3,"issueDate":"2017-11-15T05:46:34.911Z","children":[{"gender":"women","birthday":"2002-12-28T05:47:20.674Z"},{"gender":"women","birthday":"2004-09-21T05:47:31.760Z"},{"gender":"man","birthday":"2008-01-21T05:47:42.479Z"}]}
    */
    let result = null;
    try {
      result = {
        issueDate: certData.issueDate,
        children: certData.children,
        childCount: 0,
      };
      if (result.children instanceof Array) {
        result.childCount = result.children.length;
      }
    } catch (e) {
      console.log(e);
      result = null;
    }
    return result;
  };

  makeResidentRegistrationData = certData => {
    /*
    {"issueDate":"2017-11-13T08:57:42.294Z","location":{"jibunAddr":"서울특별시 성동구 행당동 117-52","roadAddr":"서울특별시 성동구 고산자로8가길 17 (행당동)","siNm":"서울특별시","sggNm":"성동구","emdNm":"행당동","locationLatitude":37.5574387,"locationLongitude":127.03610000000003}}
    */
    let result = null;
    try {
      result = {
        issueDate: certData.issueDate,
        location: {},
      };
      if (certData.location) {
        result.location = {
          jibunAddr: certData.location.jibunAddr,
          roadAddr: certData.location.roadAddr,
          siNm: certData.location.siNm,
          sggNm: certData.location.sggNm,
          emdNm: certData.location.emdNm,
          locationLatitude: certData.location.locationLatitude,
          locationLongitude: certData.location.locationLongitude,
        };
      } else {
        throw new Error();
      }
    } catch (e) {
      console.log(e);
      result = null;
    }
    return result;
  };

  onClickDownloadCert = () => {
    return window.open(`/api/manage/user/certifications/student`);
  };

  onAddSearchOption = () => {
    const { searchOptions } = this.state;
    searchOptions.push({
      generatedKey: uuid(),
      filterType: '',
      filterValue: '',
      filterDataType: '',
    });
    this.setState({ searchOptions });
  };

  onDeleteSearchOption = generatedKey => {
    const { searchOptions } = this.state;
    _.remove(searchOptions, o => o.generatedKey === generatedKey);
    this.setState({ searchOptions });
  };

  onChangeSearchOption = (generatedKey, type, value, dataType) => {
    const { searchOptions } = this.state;
    const targetIndex = _.findIndex(
      searchOptions,
      o => o.generatedKey === generatedKey,
    );
    if (targetIndex !== -1) {
      searchOptions[targetIndex] = {
        ...searchOptions[targetIndex],
        filterType: type,
        filterValue: value,
        filterDataType: dataType,
      };
      this.setState({ searchOptions });
    } else {
      console.log(`DEBUG : ON CHANGE BUT NOT FOUND : ${generatedKey}`);
    }
  };

  getCertListUI = () => {
    let ui = null;
    const { list } = this.state;
    if (list && list.length) {
      ui = list.map((item, index) => [
        <TableRow
          key={`certification-item-${index}`}
          data={item}
          onRemove={this.onRemoveCertItem}
          onChangeCertStatus={this.onChangeCertStatus}
          changeCertStatusRequest={this.changeCertStatusRequest}
          openNotificationDialog={this.openNotificationDialog}
          openMainCertLabelDialog={this.openMainCertLabelDialog}
        />,
        <CertInfoRow
          data={item}
          key={`certification-info-item-${index}`}
          onChange={this.onChangeCertData}
          onEdit={this.onEditCertData}
          openNotificationDialog={this.openNotificationDialog}
          onClickPersonalityResult={this.onClickPersonalityResult}
        />,
      ]);
    }
    return ui;
  };

  renderCertDialog = () => {
    const { editCertData } = this.state;
    let ui = null;
    if (editCertData && editCertData.open) {
      const { data } = editCertData;
      if (data) {
        const { certTypeId } = data;
        if (certTypeId === CertificateTypeIdGroup.ENROLLMENT) {
          ui = (
            <EditCertEnrollmentDialog
              open={editCertData.open}
              data={editCertData.certData || {}}
              onLoading={false}
              onClose={this.closeCertDataEditDialog}
              onChange={this.onChangeCertData}
              onSave={this.onSaveCertData}
            />
          );
        }
        if (certTypeId === CertificateTypeIdGroup.FAMILY_RELATION) {
          ui = (
            <EditCertFamilyDialog
              open={editCertData.open}
              data={editCertData.certData || {}}
              onLoading={false}
              onClose={this.closeCertDataEditDialog}
              onChange={this.onChangeCertData}
              onSave={this.onSaveCertData}
            />
          );
        }
        if (certTypeId === CertificateTypeIdGroup.CHILDCARE) {
          ui = (
            <EditCertChildCareDialog
              open={editCertData.open}
              data={editCertData.certData || {}}
              onLoading={false}
              onClose={this.closeCertDataEditDialog}
              onChange={this.onChangeCertData}
              onSave={this.onSaveCertData}
            />
          );
        }
        if (certTypeId === CertificateTypeIdGroup.MEDICAL_EXAMINATION) {
          ui = (
            <EditCertMedicalDialog
              open={editCertData.open}
              data={editCertData.certData || {}}
              onLoading={false}
              onClose={this.closeCertDataEditDialog}
              onChange={this.onChangeCertData}
              onSave={this.onSaveCertData}
            />
          );
        }
        if (certTypeId === CertificateTypeIdGroup.RESIDENT_REGISTRATION) {
          this.openNotificationDialog('알림', '미구현 항목입니다.');
        }
      } else {
        // this.openNotificationDialog('오류', '인증 정보를 찾을 수 없습니다.');
      }
    } else {
      // this.openNotificationDialog('오류', '인증 정보를 찾을 수 없습니다.');
    }
    return ui;
  };

  renderPersonalityResult = () => {
    if (!this.currentUserId) {
      return null;
    }
    return (
      <PersonalityResult
        userId={this.currentUserId}
        onClose={this.closeDrawer}
      />
    );
  };

  render() {
    const {
      mainCertLabelDialog,
      notificationDialog,
      notificationDialogTitle,
      notificationDialogMessage,
      list,
      rejectDialog,
      rejectDialogData,
      orderOption,
      searchOptions,
    } = this.state;
    const headers = [
      { id: 'certId', desc: '인증번호', sortable: true },
      { id: 'userId', desc: '회원번호', sortable: true },
      { id: 'userName', desc: '회원이름', sortable: false },
      { id: 'userType', desc: '회원타입', sortable: false },
      { id: 'fileId', desc: '파일', sortable: true },
      { id: 'certTypeId', desc: '인증타입', sortable: true },
      { id: 'regDate', desc: '등록일자', sortable: true },
      { id: 'updateDate', desc: '갱신일자', sortable: true },
      { id: 'issueDate', desc: '발급일자', sortable: true },
      { id: 'certStatus', desc: '인증상태', sortable: true },
      { id: '', desc: '', sortable: false },
    ];
    const filter = [
      {
        name: '신청번호',
        type: 'certId',
        dataType: FilterDataTypeGroup.UNIQUE_TEXT,
      },
      {
        name: '사용자번호',
        type: 'userId',
        dataType: FilterDataTypeGroup.UNIQUE_TEXT,
      },
      {
        name: '인증 타입',
        type: 'certTypeId',
        dataType: FilterDataTypeGroup.CERT_TYPE,
      },
      {
        name: '인증 상태',
        type: 'certStatus',
        dataType: FilterDataTypeGroup.CERT_STATUS,
      },
      {
        name: '갱신일자',
        type: 'updateDate',
        dataType: FilterDataTypeGroup.DATE_RANGE,
      },
      {
        name: '발급일자',
        type: 'issueDate',
        dataType: FilterDataTypeGroup.DATE_RANGE,
      },
      {
        name: '등록일자',
        type: 'regDate',
        dataType: FilterDataTypeGroup.DATE_RANGE,
      },
    ];
    return (
      <div className={classnames(s.root, ' box box-default')}>
        <SearchBar
          filter={filter}
          searchOptions={searchOptions}
          onClickSearch={this.onClickSearch}
          openNotificationDialog={this.openNotificationDialog}
          onExportCSV={this.onExportCSV}
          onClickAddSearchOption={this.onAddSearchOption}
          onClickDeleteSearchOption={this.onDeleteSearchOption}
          onChangeSearchOption={this.onChangeSearchOption}
        >
          <RaisedButton label="대학생 인증 데이터" onClick={this.onClickDownloadCert} />
        </SearchBar>
        <div className="box-body padding-sm">
          <table className={classnames('table', s.table)}>
            <thead>
              <TableHeaderRow
                data={headers}
                orderOption={orderOption}
                onChangeOrderColumn={this.onChangeOrderColumn}
                openNotificationDialog={this.openNotificationDialog}
              />
            </thead>
            <tbody>{this.getCertListUI()}</tbody>
          </table>
          <div>
            <FlatPagination
              offset={this.state.page}
              limit={1}
              total={this.state.maxPage}
              onClick={(e, offset) => this.onClickPage(offset)}
            />
          </div>
        </div>
        <MainCertLabelDialog
          open={mainCertLabelDialog}
          onSaveMainCertLabelDialog={this.onSaveMainCertLabelDialog}
          onCloseMainCertLabelDialog={this.onCloseMainCertLabelDialog}
        />
        <RejectCertDialog
          open={rejectDialog}
          data={rejectDialogData}
          onSave={this.saveRejectCertDialog}
          onClose={this.closeRejectCertDialog}
          onChange={this.onChangeRejectDialogValue}
        />
        {this.renderCertDialog()}
        <NotificationDialog
          open={notificationDialog}
          title={notificationDialogTitle}
          message={notificationDialogMessage}
          onClose={this.onClickCloseNotificationDialog}
        />
        <Drawer open={this.state.personalityDrawer} openSecondary width={600}>
          {this.renderPersonalityResult()}
        </Drawer>
      </div>
    );
  }
}

Certifications.propTypes = {};
Certifications.defaultProps = {};

export default withStyles(s)(Certifications);
