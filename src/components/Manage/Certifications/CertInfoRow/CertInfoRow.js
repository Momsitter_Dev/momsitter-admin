import uuid from 'uuid/v4';
import Paper from 'material-ui/Paper';
import MenuItem from 'material-ui/MenuItem';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './CertInfoRow.css';
import SimpleDateFormat from '../../../../util/dataFormat/SimpleDateFormat';
import CertificateTypeIdGroup from '../../../../util/const/CertificateTypeIdGroup';
import UnivDegreeTypeGroup from '../../../../util/const/UnivDegreeTypeGroup';
import UnivStatusTypeGroup from '../../../../util/const/UnivStatusTypeGroup';
import CertChildCareTypeGroup from '../../../../util/const/CertChildCareTypeGroup';

const propTypes = {
  data: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  openNotificationDialog: PropTypes.func.isRequired,
  onClickPersonalityResult: PropTypes.func,
};
const defaultProps = {

};

class CertInfoRow extends React.Component {

  onClickPersonalityResult = () => {
    return this.props.onClickPersonalityResult(this.props.data.userId);
  };

  getExpendText = (expend) => {
    let text = '';
    if (!expend) {
      text = '인증 데이터 상세 보기';
    } else {
      text = '상세 정보 닫기';
    }
    return text;
  };

  getUnivNameValue = (parsedData) => {
    let value = null;
    if (parsedData.univ) {
      value = parsedData.univ;
    }
    if (parsedData.univName) {
      value = parsedData.univName;
    }
    return value;
  };

  getUnivGradeValue = (parsedData) => {
    let value = null;
    if (parsedData.univGrade) {
      // old version
      value = parsedData.univGrade;
    }
    if (parsedData.grade) {
      value = parsedData.grade;
    }
    return value;
  };

  getUnivStatusValue = (parsedData) => {
    let value = null;
    if (parsedData.univStatus) {
      // old version
      const degree = parsedData.univStatus;
      if (degree === "재학") {
        value = UnivStatusTypeGroup.ATTENDING;
      }
      if (degree === "휴학") {
        value = UnivStatusTypeGroup.LEAVE;
      }
      if (degree === "퇴학") {
        value = UnivStatusTypeGroup.WITHDRAW;
      }
      if (degree === "졸업") {
        value = UnivStatusTypeGroup.GRADUATE;
      }
    }
    if (parsedData.status) {
      value = parsedData.status;
    }
    return value;
  };

  getUnivDegreeValue = (parsedData) => {
    let value = null;
    if (parsedData.univDegree) {
      // old version
      const degree = parsedData.univDegree;
      if (degree === "학사") {
        value = UnivDegreeTypeGroup.BACHELOR;
      }
      if (degree === "석사") {
        value = UnivDegreeTypeGroup.MASTER;
      }
      if (degree === "박사") {
        value = UnivDegreeTypeGroup.DOCTOR;
      }
    }
    if (parsedData.degree) {
      const degree = parsedData.degree;
      if (degree) {
        value = degree;
      }
    }
    return value;
  };

  getUnivDegreeText = (degree) => {
    let text = '';
    degree = Number(degree);
    if (degree === UnivDegreeTypeGroup.BACHELOR) {
      text = "학사";
    }
    if (degree === UnivDegreeTypeGroup.MASTER) {
      text = "석사";
    }
    if (degree === UnivDegreeTypeGroup.DOCTOR) {
      text = "박사";
    }
    return text;
  };

  getUnivStatusText = (status) => {
    status = Number(status);
    const defaultText = '';
    if (status === UnivStatusTypeGroup.ATTENDING) {
      return '재학';
    }
    if (status === UnivStatusTypeGroup.GRADUATE) {
      return '졸업';
    }
    if (status === UnivStatusTypeGroup.LEAVE) {
      return '휴학';
    }
    if (status === UnivStatusTypeGroup.WITHDRAW) {
      return '퇴학';
    }
    if (status === UnivStatusTypeGroup.EXPELLED) {
      return '제적';
    }
    return defaultText;
  };

  getChildCountText = (children) => {
    let text = '';
    if (children && children.length > 0) {
      text = children.length + '명';
    } else {
      text = '없음';
    }
    return text;
  };

  getGraduateDateValue = (data) => {
    let value = '';
    if (data) {
      const { status, graduateDate } = data;
      if (status && status === UnivStatusTypeGroup.GRADUATE) {
        value = graduateDate;
      }
    }
    return value;
  };

  getExpelledDateValue = (data) => {
    let defaultValue = '';
    if (!data) {
      return defaultValue;
    }
    const expelledDate = data.expelledDate;
    if (!expelledDate) {
      return defaultValue;
    }
    return expelledDate;
  };

  onChangeCertData = (type, value) => {
    const { data } = this.props;
    const { certId } = data;
    this.props.onChange(certId, type, value);
  };

  onClickCertFileDownLoad = (url) => {
    if (typeof window !== 'undefined') {
      window.open(url);
    }
  };

  onClickCertFile = (data) => {
    if (data && data.certFile) {
      this.props.openNotificationDialog('인증 파일 미리보기', (
        <div>
          <div style={{ marginTop: '15px', width: '100%', height: '500px', backgroundImage: 'url(' + data.certFile.fileUrl + ')', backgroundSize: 'cover'}}/>
          <RaisedButton
            label="인증 파일 다운로드"
            onClick={() => this.onClickCertFileDownLoad(data.certFile.fileUrl)}
            fullWidth={true}
            primary={true}
            style={{ margin: '15px 0' }}
          />
        </div>
      ));
    } else {
      this.props.openNotificationDialog('알림', '인증 파일이 없습니다.');
    }
  };

  onClickEditCertData = (data) => {
    this.props.onEdit(data);
  };

  renderCertData = (data) => {
    const defaultUI = (<span>오류!</span>);
    try {
      const {
        certData,
        certTypeId,
      } = data;
      const parsedData = JSON.parse(certData);
      // if (certStatus && parsedData && certTypeId) {
      if (certTypeId === CertificateTypeIdGroup.ENROLLMENT) {
        return this.renderCertEnrollment(parsedData);
      }
      if (certTypeId === CertificateTypeIdGroup.FAMILY_RELATION) {
        return this.renderCertFamilyRelation(parsedData);
      }
      if (certTypeId === CertificateTypeIdGroup.CHILDCARE) {
        return this.renderCertChildCare(parsedData);
      }
      if (certTypeId === CertificateTypeIdGroup.MEDICAL_EXAMINATION) {
        return this.renderCertMedicalExamination(parsedData);
      }
      if (certTypeId === CertificateTypeIdGroup.RESIDENT_REGISTRATION) {
        return this.renderCertResidentRegistration(parsedData);
      }

      if (certTypeId === CertificateTypeIdGroup.PERSONALITY) {
        return this.renderCertPersonality();
      }
      return defaultUI;
    } catch (err) {
      return defaultUI;
    }
  };

  renderCertEnrollment = (parsedData) => {
    let ui = null;
    if (parsedData) {
      ui = (
        <table>
          <thead>
            <tr>
              <th>학교명</th>
              <th>학과</th>
              <th>학위</th>
              <th>학년</th>
              <th>학사상태</th>
              <th>발급일자</th>
              <th>졸업일자</th>
              <th>제적일자</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>{this.getUnivNameValue(parsedData)}</td>
              <td>{parsedData.major}</td>
              <td>{this.getUnivDegreeText(this.getUnivDegreeValue(parsedData))}</td>
              <td>{this.getUnivGradeValue(parsedData)}학년</td>
              <td>{this.getUnivStatusText(this.getUnivStatusValue(parsedData))}</td>
              <td>{SimpleDateFormat(parsedData.issueDate)}</td>
              <td>{SimpleDateFormat(this.getGraduateDateValue(parsedData) || null)}</td>
              <td>{SimpleDateFormat(this.getExpelledDateValue(parsedData) || null)}</td>
            </tr>
          </tbody>
        </table>
      );
    }
    return ui;
  };

  renderCertMedicalExamination = (parsedData) => {
    if (!parsedData) {
      return (
        <div className={s.certData}>
          <div className={s.certDataItem}>
            <label> 검진일자 </label>
            <div>없음</div>
          </div>
        </div>
      );
    }
    const issueDate = parsedData.issueDate;
    return (
      <div className={s.certData}>
        <div className={s.certDataItem}>
          <label> 검진일자 </label>
          <div>{SimpleDateFormat(issueDate)}</div>
        </div>
      </div>
    );
  };

  renderCertFamilyRelation = (parsedData) => {
  /*
    {
      "childCount":2,
      "issueDate":"2017-11-14T06:02:15.556Z",
      "children":[
        {"gender":"women","birthday":"2000-02-01T06:02:30.882Z"},
        {"gender":"man","birthday":"2000-02-01T06:02:48.108Z"}
      ]
    }
  */
    let ui = null;
    if (parsedData) {
      let childrenInfo = parsedData.children.map((item) => {
        return (
          <div key={uuid()} className={s.certChildItem}>
            <div className={s.certDataItem}>
              <label>성별</label>
              <div>{item.gender}</div>
            </div>
            <div className={s.certDataItem}>
              <label>생년월일</label>
              <div>{SimpleDateFormat(item.birthday)}</div>
            </div>
          </div>
        );
      });
      ui = (
        <div className={s.certData}>
          <div className={s.certDataItem}>
            <label>아이수</label>
            <div>{this.getChildCountText(parsedData.children)}</div>
          </div>
          <label className={s.label}>아이 정보</label>
          <div className={s.childInfoWrap}>{childrenInfo}</div>
        </div>
      );
    }
    return ui;
  };

  renderCertChildCare = (parsedData) => {
  /*
    {"issueDate":"2017-02-17T06:33:58.324Z","qualificationType":"특수학교 정교사"}
  */
    let ui = null;
    if (parsedData) {
      ui = (
        <div className={s.certData}>
          <div className={s.certDataItem}>
            <label>자격증 이름</label>
            <div>{parsedData.qualificationType || '없음'}</div>
          </div>
          <div className={s.certDataItem}>
            <label>자격증 발급일</label>
            <div>{SimpleDateFormat(parsedData.issueDate)}</div>
          </div>
        </div>
      );
    } else {
      ui = (
        <div>인증 데이터 없음</div>
      );
    }
    return ui;
  };

  renderCertChildCareSelectItem = () => {
    const keys = Object.keys(CertChildCareTypeGroup);
    return keys.map((item) => {
      return (
        <MenuItem primaryText={CertChildCareTypeGroup[item]} value={CertChildCareTypeGroup[item]} />
      )
    });
  };

  renderCertResidentRegistration = (parsedData) => {
  /*
   {"issueDate":"2016-08-15T15:00:00.000Z",
      "location":{
          "jibunAddr":"서울특별시 서대문구 창천동  57-55",
          "roadAddr":"서울특별시 서대문구 연세로5가길 20 (창천동)",
          "siNm":"서울특별시",
          "sggNm":"서대문구",
          "emdNm":"창천동",
          "locationLatitude":37.5573656,
          "locationLongitude":126.9355971
      }
    }
  */
    if (!parsedData) {
      return null;
    }

    if (typeof parsedData.location === 'string') {
      return (
        <div className={s.certData}>
          <div className={s.certDataItem}>
            <label> 주소 </label>
            <div> {parsedData.location} </div>
          </div>
          <div className={s.certDataItem}>
            <label>등록일자</label>
            <div> {SimpleDateFormat(parsedData.issueDate)} </div>
          </div>
        </div>
      );
    }

    return (
      <div className={s.certData}>
        <div className={s.certDataItem}>
          <label>지번</label>
          <div>{parsedData && parsedData.location && parsedData.location.jibunAddr}</div>
        </div>
        <div className={s.certDataItem}>
          <label>도로명</label>
          <div>{parsedData && parsedData.location && parsedData.location.roadAddr}</div>
        </div>
        <div className={s.certDataItem}>
          <label>등록일자</label>
          <div>{SimpleDateFormat(parsedData.issueDate)}</div>
        </div>
      </div>
    );
  };

  renderCertPersonality = () => {
    return (
      <div>
        <FlatButton label="인성검사 결과보기" onClick={this.onClickPersonalityResult} fullWidth />
      </div>
    );
  };

  renderCertDataWrap = (data) => {
    const defaultUI = (<div> 인증 데이터를 표시할 수 없습니다. </div>);
    try {
      const certTypeId = data.certTypeId;

      if (certTypeId === CertificateTypeIdGroup.PERSONALITY) {
        return (
          <Paper zDepth={2}>
            <div className={s.wrap}>
              <div>{this.renderCertData(data)}</div>
            </div>
          </Paper>
        );
      }

      return (
        <div>
          <Paper zDepth={2}>
            <div className={s.wrap}>
              <div>{this.renderCertData(data)}</div>
            </div>
            <div className={s.actions}>
              <RaisedButton
                label="인증 파일 미리보기"
                onClick={() => this.onClickCertFile(data)}
              />
              <RaisedButton
                label="인증 데이터 수정하기"
                onClick={() => this.onClickEditCertData(data)}
              />
            </div>
          </Paper>
        </div>
      );
    } catch (err) {
      return defaultUI;
    }
  };

  render() {
    const { data } = this.props;
    return (
      <tr className={s.row}>
        <td colSpan={11}>
          {this.renderCertDataWrap(data)}
        </td>
      </tr>
    );
  }
}

CertInfoRow.propTypes = propTypes;
CertInfoRow.defaultProps = defaultProps;

export default withStyles(s)(CertInfoRow);
