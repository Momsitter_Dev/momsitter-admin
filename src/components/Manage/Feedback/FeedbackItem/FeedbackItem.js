import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './FeedbackItem.css';
import Paper from 'material-ui/Paper';
import SimpleDateFormat from '../../../../util/dataFormat/SimpleDateFormat';
import classnames from 'classnames';

class FeedbackItem extends React.Component {
  render() {
    const {
      feedbackId,
      postDate,
      feedbackContent,
    } = this.props.data;
    return (
      <div className={classnames(s.root)}>
        <div className={s.header}>
          <div className={s.feedbackId}>{feedbackId}</div>
          <div className={s.postDate}>{SimpleDateFormat(postDate)}</div>
        </div>
        <p className={s.content}>{feedbackContent}</p>
      </div>
    );
  }
}

FeedbackItem.propTypes = {
  data: PropTypes.object.isRequired,
};

FeedbackItem.defaultProps = {};

export default withStyles(s)(FeedbackItem);
