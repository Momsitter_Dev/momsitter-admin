import FlatPagination from 'material-ui-flat-pagination';
import Paper from 'material-ui/Paper';
import uuid from 'uuid/v4';
import qs from 'qs';
import classnames from 'classnames';
import IconButton from 'material-ui/IconButton';
import AddIcon from 'material-ui/svg-icons/action/note-add';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Feedback.css';
import SearchBar from '../Common/SearchBar';
import Request from '../../../util/Request';
import FeedbackItem from './FeedbackItem';
import NotificationDialog from '../../Dialog/NotificationDialog';

class Feedback extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      page: 0,
      maxPage: 0,
      notificationDialog: false,
      notificationDialogTitle: '',
      notificationDialogMessage: '',
    };
    this.cachedOption = {
      searchFilter: 'userId',
      searchText: '',
      searchOrderColumn: '',
      searchOrderType: '',
    };
    this.searchOptions = {
      requestPage: 0,
      searchFilter: 'userId',
      searchText: '',
      searchOrderColumn: '',
      searchOrderType: '',
    };
    this.onClickSearch = this.onClickSearch.bind(this);
  }

  async componentDidMount() {
    const queryString = qs.stringify(this.searchOptions);
    await this.doSearch(queryString)
      .then(data => {
        this.setState({
          list: data.data.rows,
          page: data.page.currentPage,
          maxPage: data.page.maxPage,
        });
      })
      .catch(() => {
        this.setState({ list: [] });
      });
  }

  openNotificationDialog = (title, message) => {
    this.setState({
      notificationDialog: true,
      notificationDialogTitle: title || '알림',
      notificationDialogMessage: message,
    });
  };

  onClickCloseNotificationDialog = () => {
    this.setState({
      notificationDialog: false,
    });
  };

  doSearch = query =>
    new Promise(async (resolve, reject) => {
      // Cache Update
      this.cachedOption = Object.assign({}, this.searchOptions);
      const searchResult = await Request(
        `api/manage/feedback?${query}`,
        { method: 'GET' },
      ).catch(() => false);
      if (searchResult && searchResult.result === 'success') {
        resolve(searchResult);
      } else {
        reject('1');
      }
    });

  onChangeSearchFilter = (event, index, value) => {
    this.searchOptions.searchFilter = value;
  };

  onChangeSearchText = (event, value) => {
    this.searchOptions.searchText = value;
  };

  onClickSearch = async function (event, index, value) {
    // 페이지를 1로 초기화
    this.searchOptions.requestPage = 0;
    const queryString = qs.stringify(this.searchOptions);
    await this.doSearch(queryString)
      .then(data => {
        this.setState({
          list: data.data.rows,
          page: data.page.currentPage,
          maxPage: data.page.maxPage,
        });
      })
      .catch(() => {
        this.setState({ list: [] });
      });
  };

  onClickPage = value => {
    this.cachedOption.requestPage = value;
    const queryString = qs.stringify(this.cachedOption);
    this.doSearch(queryString)
      .then(data => {
        this.setState({
          list: data.data.rows,
          page: data.page.currentPage,
          maxPage: data.page.maxPage,
        });
      })
      .catch(err => {
        this.setState({ list: [] });
      });
  };

  onChangeOrderColumn = data => {
    this.openNotificationDialog('알림', '구현 준비중입니다.')
  };

  render() {
    const {
      list,
      page,
      maxPage,
      notificationDialog,
      notificationDialogTitle,
      notificationDialogMessage,
    } = this.state;
    const filter = [
      { name: '피드백 번호', value: 'feedbackId' },
    ];

    let searchResultUI = null;
    if (list && list.length > 0) {
      searchResultUI = list.map((item) => {
        return (
          <FeedbackItem key={uuid()} data={item} />
        )
      });
    } else {
      searchResultUI = (
        <Paper zDepth={1}>
          <div>
            검색 결과가 없습니다.
          </div>
        </Paper>
      );
    }

    return (
      <div className={classnames(s.root, ' box box-default')}>
        <SearchBar
          onChangeSearchFilter={this.onChangeSearchFilter}
          onChangeSearchText={this.onChangeSearchText}
          onClickSearch={this.onClickSearch}
          filter={filter}
        >
          <IconButton onClick={this.onClickAddCoupon}>
            <AddIcon />
          </IconButton>
        </SearchBar>
        <div className="box-body padding-sm">
          <div className={classnames("row", s.resultWrap)}>
            {searchResultUI}
          </div>
          <div>
            <FlatPagination
              offset={page}
              limit={1}
              total={maxPage}
              onClick={(e, offset) => this.onClickPage(offset)}
            />
          </div>
        </div>
        <NotificationDialog
          open={notificationDialog}
          title={notificationDialogTitle}
          message={notificationDialogMessage}
          onClose={this.onClickCloseNotificationDialog}
        />
      </div>
    );
  }
}

Feedback.propTypes = {};
Feedback.defaultProps = {};

export default withStyles(s)(Feedback);
