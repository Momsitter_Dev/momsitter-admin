import RaisedButton from 'material-ui/RaisedButton';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './TableRow.css';
import SimpleDateFormat from '../../../../util/dataFormat/SimpleDateFormat';
import PaymentStatusGroup from '../../../../util/const/PaymentStatusGroup';
import getFormattedMoney from '../../../../util/dataFormat/getFormattedMoney';
import UserTypeNameGroup from '../../../../util/const/UserTypeNameGroup';

const buttonStyle = {
  zIndex: '0',
};

const propTypes = {
  data: PropTypes.shape({
    card_name: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string,
    ]),
    couponDiscount: PropTypes.oneOf([
      PropTypes.number,
      PropTypes.string,
    ]),
    paymentDoneDate: PropTypes.string,
    paymentId: PropTypes.number,
    paymentPrice: PropTypes.number,
    paymentRequestDate: PropTypes.string,
    paymentStatus: PropTypes.string,
    pointDiscount: PropTypes.oneOf([
      PropTypes.number,
      PropTypes.string,
    ]),
    productCode: PropTypes.string,
    productFor: PropTypes.string,
    productName: PropTypes.string,
    productPeriod: PropTypes.oneOf([
      PropTypes.number,
      PropTypes.string,
    ]),
    productPrice: PropTypes.number,
    userId: PropTypes.number,
    userName: PropTypes.string,
    userPhone: PropTypes.string,
    userTypeId: PropTypes.number,
  }),
};
const defaultProps = {};

class TableRow extends React.Component {
  getDateText = (data) => {
    let text = '없음';
    try {
      if (data) {
        text = SimpleDateFormat(data);
      }
    } catch (err) {
      console.log(err);
    }
    return text;
  };

  getPaymentStatus = (status) => {
    let text = '상태없음';
    try {
      if (status) {
        if (status === PaymentStatusGroup.DONE) {
          text = '완료';
        }
        if (status === PaymentStatusGroup.REQUEST) {
          text = '요청';
        }
        if (status === PaymentStatusGroup.WAITING) {
          text = '결제 대기';
        }
        if (status === PaymentStatusGroup.ADMIN_ABORT) {
          text = '관리자 취소';
        }
        if (status === PaymentStatusGroup.USER_ABORT) {
          text = '사용자 취소';
        }
      }
    } catch (err) {
      console.log(err);
    }
    return text;
  };

  onClickUserId = (userId) => {
    window.open(`/users?userId=${userId}`)
  };

  render() {
    const { data } = this.props;
    const {
      card_name,
      couponDiscount,
      paymentId,
      paymentPrice,
      paymentRequestDate,
      paymentDoneDate,
      paymentStatus,
      pointDiscount,
      productCode,
      productFor,
      productName,
      productPeriod,
      productPrice,
      userId,
      userName,
      userPhone,
      userTypeId,
    } = data;
    return (
      <tr>
        <td>{paymentId}</td>
        <td>
          <RaisedButton
            label={userId}
            onClick={() => this.onClickUserId(userId)}
            buttonStyle={buttonStyle}
          />
        </td>
        <td>{userName}</td>
        <td>{UserTypeNameGroup[userTypeId]}</td>
        <td>{productCode}</td>
        <td>{productName}</td>
        <td>{this.getPaymentStatus(paymentStatus)}</td>
        <td>{getFormattedMoney(paymentPrice)} 원 </td>
        <td> {couponDiscount ? getFormattedMoney(couponDiscount) : '0'} 원 </td>
        <td> {pointDiscount ? getFormattedMoney(pointDiscount) : '0'} 원 </td>
        <td>{this.getDateText(paymentRequestDate)}</td>
        <td>{this.getDateText(paymentDoneDate)}</td>
      </tr>
    );
  }
}

TableRow.propTypes = propTypes;
TableRow.defaultProps = defaultProps;

export default withStyles(s)(TableRow);
