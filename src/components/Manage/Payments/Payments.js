/* eslint-disable prettier/prettier */
import _ from 'lodash';
import uuid from 'uuid/v4';
import qs from 'qs';
import classnames from 'classnames';
import FlatPagination from 'material-ui-flat-pagination';
import CircularProgress from 'material-ui/CircularProgress';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Payments.css';
import SearchBar from '../../SearchBar';
import TableHeaderRow from '../../TableHeaderRow';
import TableRow from './TableRow';
import Request from '../../../util/Request';
import ResponseResultTypeGroup from '../../../util/const/ResponseResultTypeGroup';
import FilterDataTypeGroup from '../../../util/const/FilterDataTypeGroup';

class Payments extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      onLoading: true,
      onError: false,
      list: [],
      page: 0,
      maxPage: 0,
      searchOptions: [],
      orderOption: {
        column: '',
        method: '',
      },
    };
    this.cachedOption = {};
    this.onClickSearch = this.onClickSearch.bind(this);
    this.onChangeOrderColumn = this.onChangeOrderColumn.bind(this);
  }

  async componentDidMount() {
    await this.doSearch({}).then(data => {
      this.setState({
        onLoading: false,
        list: data.data,
        page: data.page.currentPage,
        maxPage: data.page.maxPage,
      });
    }).catch(() => {
      this.setState({ list: [] });
    });
  }

  onClickExportCSV = () => {
    const queryString = qs.stringify(this.cachedOption);
    return window.open(`/api/manage/user/payments/csv?${queryString}`);
  };

  doSearch = options =>
    new Promise(async (resolve, reject) => {
      this.setState({ onLoading: true });
      // Cache Update
      const query = qs.stringify(options);
      this.cachedOption = Object.assign({}, options);

      const searchResult = await Request(
        `api/manage/user/payments?${query}`,
        { method: 'GET' },
      ).catch(() => false);
      if (searchResult && searchResult.result === ResponseResultTypeGroup.SUCCESS) {
        resolve(searchResult);
      } else {
        reject();
      }
    });

  onClickSearch = async function () {
    const { searchOptions, orderOption } = this.state;
    await this.doSearch({
      searchOptions,
      orderOption,
      requestPage: 0,
    }).then(data => {
      this.setState({
        onLoading: false,
        list: data.data,
        page: data.page.currentPage,
        maxPage: data.page.maxPage,
      });
    }).catch(() => {
      this.setState({ list: [] });
    });
  };

  onClickPage = value => {
    this.cachedOption.requestPage = value;
    this.doSearch(this.cachedOption).then(data => {
      this.setState({
        onLoading: false,
        list: data.data,
        page: data.page.currentPage,
        maxPage: data.page.maxPage,
      });
    }).catch(() => {
      this.setState({ list: [] });
    });
  };

  onChangeOrderColumn = async function (columnId) {
    const { orderOption, searchOptions } = this.state;
    if (orderOption.column !== columnId) {
      orderOption.method = '';
    }
    orderOption.column = columnId;
    if (orderOption.method === 'DESC') {
      orderOption.method = 'ASC';
    } else if (orderOption.method === 'ASC') {
      orderOption.method = '';
    } else {
      orderOption.method = 'DESC'
    }
    this.setState({ orderOption });

    await this.doSearch({
      searchOptions,
      orderOption,
      requestPage: 0,
    }).then(data => {
      this.setState({
        onLoading: false,
        list: data.data,
        page: data.page.currentPage,
        maxPage: data.page.maxPage,
      });
    }).catch(() => {
      this.setState({ list: [] });
    });
  };

  onAddSearchOption = () => {
    const { searchOptions } = this.state;
    searchOptions.push({
      generatedKey: uuid(),
      filterType: '',
      filterValue: '',
      filterDataType: '',
    });
    this.setState({ searchOptions });
  };

  onDeleteSearchOption = (generatedKey) => {
    const { searchOptions } = this.state;
    _.remove(searchOptions, (o) => {
      return o.generatedKey === generatedKey;
    });
    this.setState({ searchOptions });
  };

  onChangeSearchOption = (generatedKey, type, value, dataType) => {
    const { searchOptions } = this.state;
    const targetIndex = _.findIndex(searchOptions, (o) => {
      return o.generatedKey === generatedKey;
    });
    if (targetIndex !== -1) {
      searchOptions[targetIndex] = {
        ...searchOptions[targetIndex],
        filterType: type,
        filterValue: value,
        filterDataType: dataType,
      };
      this.setState({ searchOptions });
    } else {
      console.log('DEBUG : ON CHANGE BUT NOT FOUND : ' + generatedKey);
    }
  };

  renderPaymentList = () => {
    const { list, onLoading, onError } = this.state;
    let ui = null;
    if (onError) {
      ui = (
        <tr>
          <td colSpan={11}>[오류] 새로고침 후에 이용해 주세요.</td>
        </tr>
      );
    } else {
      if (onLoading) {
        ui = (
          <tr>
            <td colSpan={11}>
              <CircularProgress />
              <div>데이터를 조회중입니다.</div>
            </td>
          </tr>
        );
      } else {
        ui = list.map((item, index) =>
          <TableRow key={`payment-item-${index}`} data={item} />,
        );
      }
    }
    return ui;
  };

  render() {
    const {
      page,
      maxPage,
      orderOption,
      searchOptions,
    } = this.state;
    const headers = [
      { id: 'paymentId', desc: '결제번호', sortable: true },
      { id: 'userId', desc: '회원번호', sortable: true },
      { id: 'userName', desc: '회원이름', sortable: false },
      { id: 'userTypeName', desc: '회원타입', sortable: false },
      { id: 'productCode', desc: '상품코드', sortable: true },
      { id: 'productName', desc: '상품명', sortable: false },
      { id: 'paymentStatus', desc: '결제상태', sortable: true },
      { id: 'paymentPrice', desc: '결제금액', sortable: true },
      { id: 'couponDiscount', desc: '쿠폰사용', sortable: true },
      { id: 'pointDiscount', desc: '포인트사용', sortable: true },
      { id: 'paymentRequestDate', desc: '결제요청일자', sortable: true },
      { id: 'paymentDoneDate', desc: '결제완료일자', sortable: true },
    ];
    const filter = [
      { name: '결제번호', type: 'paymentId', dataType: FilterDataTypeGroup.UNIQUE_TEXT },
      { name: '회원번호', type: 'userId', dataType: FilterDataTypeGroup.UNIQUE_TEXT },
      { name: '상품코드', type: 'productCode', dataType: FilterDataTypeGroup.UNIQUE_TEXT },
      { name: '결제상태', type: 'paymentStatus', dataType: FilterDataTypeGroup.PAYMENT_STATUS },
      { name: '결제요청일자', type: 'paymentRequestDate', dataType: FilterDataTypeGroup.DATE_RANGE },
      { name: '결제완료일자', type: 'paymentDoneDate', dataType: FilterDataTypeGroup.DATE_RANGE },
    ];

    return (
      <div className={classnames(s.root, ' box box-default')}>
        <SearchBar
          filter={filter}
          searchOptions={searchOptions}
          onClickSearch={this.onClickSearch}
          openNotificationDialog={this.openNotificationDialog}
          onClickAddSearchOption={this.onAddSearchOption}
          onClickDeleteSearchOption={this.onDeleteSearchOption}
          onChangeSearchOption={this.onChangeSearchOption}
          onExportCSV={this.onClickExportCSV}
        />
        <div className="box-body padding-sm">
          <table className="table">
            <thead>
              <TableHeaderRow
                data={headers}
                orderOption={orderOption}
                onChangeOrderColumn={this.onChangeOrderColumn}
                openNotificationDialog={this.openNotificationDialog}
              />
            </thead>
            <tbody>{this.renderPaymentList()}</tbody>
          </table>
          <div>
            <FlatPagination
              offset={page}
              limit={1}
              total={maxPage}
              onClick={(e, offset) => this.onClickPage(offset)}
            />
          </div>
        </div>
      </div>
    );
  }
}

Payments.propTypes = {};

Payments.defaultProps = {};

export default withStyles(s)(Payments);
