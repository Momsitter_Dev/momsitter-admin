import RaisedButton from 'material-ui/RaisedButton';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './TableRow.css';
import Request from '../../../../util/Request';

const buttonStyle = {
  zIndex: '0',
};

class TableRow extends React.Component {
  constructor(props) {
    super(props);
    this.onClickDeleteProduct = this.onClickDeleteProduct.bind(this);
  }
  onClickDeleteProduct = async function () {
    const { productId } = this.props.data;
    if (confirm('삭제하시겠습니까?')) {
      if (productId) {
        const response = await Request(
          `api/manage/products/${productId}`,
          { method: 'DELETE' },
        ).catch(() => false);
        if (response && response.result === 'success') {
          // this.setState({ reviewType: value });
          // TODO Reload list
        } else {
          console.log('error');
        }
      }
    }
  };

  render() {
    return (
      <tr>
        <td>
          {this.props.data.productId}
        </td>
        <td>
          {this.props.data.productCode}
        </td>
        <td>
          {this.props.data.productName}
        </td>
        <td>
          {this.props.data.productDesc}
        </td>
        <td>
          {this.props.data.productPrice}
        </td>
        <td>
          {this.props.data.productType}
        </td>
        <td>
          {this.props.data.productFor}
        </td>
        <td>
          {this.props.data.productPeriod}
        </td>
        <td>
          {this.props.data.productApplyToSitterCount}
        </td>
        <td>
          {this.props.data.productApplyToParentCount}
        </td>
        <td>
          <RaisedButton
            label="삭제"
            onClick={this.onClickDeleteProduct}
            buttonStyle={buttonStyle}
          />
        </td>
      </tr>
    );
  }
}

TableRow.propTypes = {
  data: PropTypes.object.isRequired,
};

TableRow.defaultProps = {};

export default withStyles(s)(TableRow);
