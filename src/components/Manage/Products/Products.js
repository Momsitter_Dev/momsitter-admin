import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Products.css';
import SearchBar from '../Common/SearchBar';
import TableHeaderRows from '../Common/TableHeaderRows';
import TableRow from './TableRow';
import FlatPagination from 'material-ui-flat-pagination';
import qs from 'qs';
import Request from '../../../util/Request';
import classnames from 'classnames';
import AddDialog from './AddDialog';
import IconButton from 'material-ui/IconButton';
import AddIcon from 'material-ui/svg-icons/action/note-add';

class Products extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      page: 0,
      maxPage: 0,
      addDialog: false,
    };
    this.cachedOption = {
      searchFilter: 'paymentId',
      searchText: '',
      searchOrderColumn: '',
      searchOrderType: '',
    };
    this.searchOptions = {
      requestPage: 0,
      searchFilter: 'reviewId',
      searchText: '',
      searchOrderColumn: '',
      searchOrderType: '',
    };
    this.onClickSearch = this.onClickSearch.bind(this);
    this.onExportCSV = this.onExportCSV.bind(this);
  }

  async componentDidMount() {
    const queryString = qs.stringify(this.searchOptions);
    await this.doSearch(queryString)
      .then(data => {
        this.setState({
          list: data.data.rows,
          page: data.page.currentPage,
          maxPage: data.page.maxPage,
        });
      })
      .catch(() => {
        this.setState({ list: [] });
      });
  }

  doSearch = query =>
    new Promise(async (resolve, reject) => {
      // Cache Update
      this.cachedOption = Object.assign({}, this.searchOptions);

      const searchResult = await Request(
        `api/manage/products?${query}`,
        { method: 'GET' },
      ).catch(() => false);
      if (searchResult && searchResult.result === 'success') {
        resolve(searchResult);
      } else {
        reject('2');
      }
    });

  onChangeSearchFilter = (event, index, value) => {
    this.searchOptions.searchFilter = value;
  };

  onChangeSearchText = (event, value) => {
    this.searchOptions.searchText = value;
  };

  onClickSearch = async function (event, index, value) {
    // 페이지를 1로 초기화
    this.searchOptions.requestPage = 0;
    const queryString = qs.stringify(this.searchOptions);
    await this.doSearch(queryString)
      .then(data => {
        this.setState({
          list: data.data.rows,
          page: data.page.currentPage,
          maxPage: data.page.maxPage,
        });
      })
      .catch(() => {
        this.setState({ list: [] });
      });
  };

  onClickPage = value => {
    this.cachedOption.requestPage = value;
    const queryString = qs.stringify(this.cachedOption);
    this.doSearch(queryString)
      .then(data => {
        this.setState({
          list: data.data.rows,
          page: data.page.currentPage,
          maxPage: data.page.maxPage,
        });
      })
      .catch(err => {
        this.setState({ list: [] });
      });
  };

  onChangeOrderColumn = data => {
    this.searchOptions.searchOrderColumn = data.orderColumn;
    this.searchOptions.searchOrderType = data.orderType;
    // SET PAGE AS 1
    // DO SEARCH
  };

  onClickAddProduct = () => {
    this.setState({ addDialog: true });
  };

  closeDialog = () => {
    this.setState({ addDialog: false });
  };

  onExportCSV = async function () {
    const queryString = qs.stringify(this.cachedOption);
    window.open(`/api/manage/products/csv?${queryString}`);
  };

  render() {
    const headers = [
      { id: 'productId', desc: '이용권 번호' },
      { id: 'productCode', desc: '코드' },
      { id: 'productName', desc: '이름' },
      { id: 'productDesc', desc: '설명' },
      { id: 'productPrice', desc: '가격' },
      { id: 'productType', desc: '타입' },
      { id: 'productFor', desc: '대상 회원' },
      { id: 'productPeriod', desc: '기간' },
      { id: 'productApplyToSitterCount', desc: '지원 횟수(시터에게)' },
      { id: 'productApplyToParentCount', desc: '지원 횟수(부모에게)' },
      { id: 'productId', desc: 'actions' },
    ];
    const filter = [
      { name: '이용권 번호', value: 'productId' },
      { name: '코드', value: 'productCode' },
      { name: '이름', value: 'productName' },
    ];

    return (
      <div className={classnames(s.root, ' box box-default')}>
        <SearchBar
          onChangeSearchFilter={this.onChangeSearchFilter}
          onChangeSearchText={this.onChangeSearchText}
          onClickSearch={this.onClickSearch}
          filter={filter}
        >
          <IconButton onClick={this.onClickAddProduct}>
            <AddIcon />
          </IconButton>
        </SearchBar>
        <div className="box-body padding-sm">
          <table>
            <thead>
              <TableHeaderRows
                onChangeOrderColumn={this.onChangeOrderColumn}
                headers={headers}
              />
            </thead>
            <tbody>
              {this.state.list.map((item, index) =>
                <TableRow key={index} data={item} />,
              )}
            </tbody>
          </table>
          <div>
            <FlatPagination
              offset={this.state.page}
              limit={1}
              total={this.state.maxPage}
              onClick={(e, offset) => this.onClickPage(offset)}
            />
          </div>
        </div>
        <AddDialog open={this.state.addDialog} closeDialog={this.closeDialog} />
      </div>
    );
  }
}

Products.propTypes = {};

Products.defaultProps = {};

export default withStyles(s)(Products);
