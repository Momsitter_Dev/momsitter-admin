/* eslint-disable prettier/prettier */
import { compose } from 'recompose';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Grid from '@material-ui/core/Grid';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './AddDialog.css';
import TextField from '../../../TextField';
import Request from '../../../../util/Request';
import ResponseResultTypeGroup from "../../../../util/const/ResponseResultTypeGroup";

const propTypes = {
  open: PropTypes.bool,
  closeDialog: PropTypes.func,
};
const defaultProps = {
  open: false,
  closeDialog: () => {},
};

class AddDialog extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      productCode: '',
      productName: '',
      productDesc: '',
      productPrice: '',
      productType: '',
      productFor: '',
      productPeriod: '',
      productApplyToSitterCount: '',
      productApplyToParentCount: '',
    };
    this.onCloseDialog = this.onCloseDialog.bind(this);
    this.onSaveDialog = this.onSaveDialog.bind(this);
  }

  onChangeFrom = (type, value) => {
    const state = {};
    state[type] = value;
    return this.setState(state);
  };

  onCloseDialog = () => {
    return this.props.closeDialog();
  };

  onSaveDialog = async function () {
    try {
      const response = await Request(
        'api/manage/products',
        {
          method: 'POST',
          data: this.state,
        },
      );
      if (response && response.result === ResponseResultTypeGroup.SUCCESS) {
        return this.onCloseDialog();
      }
      throw new Error('request fail');
    } catch (err) {
      console.log(err);
    }
  };

  render() {
    const { open } = this.props;
    const {
      productType,
      productFor,
      productCode,
      productName,
      productDesc,
      productPrice,
      productPeriod,
      productApplyToSitterCount,
      productApplyToParentCount,
    } = this.state;
    return (
      <div className={s.root}>
        <Dialog open={open} onClose={this.onCloseDialog}>
          <DialogTitle> 상품 추가 </DialogTitle>
          <DialogContent>
            <DialogContentText style={{ marginBottom: '15px' }}> 새로운 상품을 정의합니다. </DialogContentText>
            <Grid container spacing={16}>
              <Grid item xs={12}>
                <Select
                  value={productType}
                  onChange={(e) =>
                    this.onChangeFrom('productType', e.target.value)}
                >
                  <MenuItem value="membership"> 멤버쉽 </MenuItem>
                  <MenuItem value="certPersonality"> 인성검사 </MenuItem>
                </Select>
              </Grid>
              <Grid item xs={12}>
                <Select
                  value={productFor}
                  onChange={(e) =>
                    this.onChangeFrom('productFor', e.target.value)}
                >
                  <MenuItem value="sitter"> 시터 </MenuItem>
                  <MenuItem value="parent"> 부모 </MenuItem>
                </Select>
              </Grid>
              <Grid item xs={12}>
                <TextField
                  hintText="이용권 코드"
                  floatingLabelText="이용권 코드"
                  floatingLabelFixed
                  value={productCode}
                  onChange={(value) => this.onChangeFrom('productCode', value)}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  hintText="이용권 이름"
                  floatingLabelText="이용권 이름"
                  floatingLabelFixed
                  value={productName}
                  onChange={(value) => this.onChangeFrom('productName', value)}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  hintText="이용권 설명"
                  floatingLabelText="이용권 설명"
                  floatingLabelFixed
                  value={productDesc}
                  onChange={(value) => this.onChangeFrom('productDesc', value)}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  hintText="이용권 금액"
                  floatingLabelText="이용권 금액"
                  floatingLabelFixed
                  value={productPrice}
                  onChange={(value) => this.onChangeFrom('productPrice', value)}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  hintText="기간"
                  floatingLabelText="이용권 기간"
                  floatingLabelFixed
                  value={productPeriod}
                  onChange={(value) => this.onChangeFrom('productPeriod', value)}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  hintText="지원가능 횟수(시터에게)"
                  floatingLabelText="지원가능 횟수"
                  floatingLabelFixed
                  value={productApplyToSitterCount}
                  onChange={(value) => this.onChangeFrom('productApplyToSitterCount', value)}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  hintText="지원가능 횟수(부모에게)"
                  floatingLabelText="지원가능 횟수"
                  floatingLabelFixed
                  value={productApplyToParentCount}
                  onChange={(value) => this.onChangeFrom('productApplyToParentCount', value)}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button variant={'flat'} onClick={this.onCloseDialog}>
              취소
            </Button>
            <Button
              variant={'raised'}
              onClick={this.onSaveDialog}
            >
              저장
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

AddDialog.propTypes = propTypes;
AddDialog.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(AddDialog);
