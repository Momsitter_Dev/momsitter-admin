import RaisedButton from 'material-ui/RaisedButton';
import EnhancedButton from 'material-ui/internal/EnhancedButton';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './TableRow.css';
import SimpleDateFormat from '../../../../util/dataFormat/SimpleDateFormat';
import getUserNameText from '../../../../util/user/getUserNameText';
import getUserRoleText from '../../../../util/user/getUserRoleText';
import isParent from '../../../../util/user/isParent';

const buttonStyle = {
  zIndex: '0',
};

class TableRow extends React.Component {
  getApplyCount = productUser => {
    let text = '없음';
    if (productUser && productUser.userRole) {
      const userRole = productUser.userRole[0];
      if (isParent(userRole.userTypeId)) {
        text = this.getApplyToSitterCountText(productUser);
      } else {
        text = this.getApplyToParentCountText(productUser);
      }
    }
    return text;
  };

  getApplyToSitterCountText = productUser => {
    let text = '없음';
    if (productUser && productUser.userApplyCount) {
      text = productUser.userApplyCount.applyToSitterCount;
    }
    return text;
  };

  getApplyToParentCountText = productUser => {
    let text = '없음';
    if (productUser && productUser.userApplyCount) {
      text = productUser.userApplyCount.applyToParentCount;
    }
    return text;
  };

  onClickProductMemo = memo => {
    if (memo) {
      return this.props.openNotificationDialog(memo);
    }
    return this.props.openNotificationDialog('작성된 메모가 없습니다.');
  };

  onClickUserId = userId => {
    window.open(`/users?userId=${userId}`);
  };

  onClickDelete = userProductId => {
    this.props.onDelete(userProductId);
  };

  onClickEdit = data => {
    this.props.onClickEdit(data);
  };

  getPaymentIdText = paymentId => {
    let text = '없음';
    if (paymentId) {
      text = paymentId;
    }
    return text;
  };

  getMemoText = memo => {
    let text = '없음';
    if (memo) {
      text = memo;
    }
    return text;
  };

  getProductNameText = product => {
    let text = '없음';
    if (product && product.productName) {
      text = product.productName;
    }
    return text;
  };

  render() {
    const { data } = this.props;
    const {
      userProductId,
      userId,
      productId,
      paymentId,
      purchaseDate,
      expireDate,
      memo,
      productUser,
      productStatus,
      product,
    } = data;
    return (
      <tr className={s.row}>
        <td>{userProductId}</td>
        <td>
          <RaisedButton
            label={userId}
            onClick={() => this.onClickUserId(userId)}
            style={{ width: '100%', padding: '0' }}
            buttonStyle={buttonStyle}
            labelStyle={{ padding: '0' }}
          />
        </td>
        <td>{getUserNameText(productUser)}</td>
        <td>{getUserRoleText(productUser)}</td>
        <td>{productId}</td>
        <td>{this.getProductNameText(product)}</td>
        <td>{this.getPaymentIdText(paymentId)}</td>
        <td>{SimpleDateFormat(purchaseDate)}</td>
        <td>{SimpleDateFormat(expireDate)}</td>
        <td>
          <EnhancedButton
            onClick={() => this.onClickProductMemo(memo)}
            style={buttonStyle}
          >
            <p className={s.productMemo}>{this.getMemoText(memo)}</p>
          </EnhancedButton>
        </td>
        <td>{this.getApplyCount(productUser)}</td>
        <td>
          <RaisedButton
            label="수정"
            onClick={() => this.onClickEdit(this.props.data)}
            buttonStyle={buttonStyle}
          />
          <RaisedButton
            label="삭제"
            secondary
            onClick={() => this.onClickDelete(userProductId)}
            buttonStyle={buttonStyle}
          />
        </td>
      </tr>
    );
  }
}

TableRow.propTypes = {
  data: PropTypes.object.isRequired,
  onDelete: PropTypes.func.isRequired,
  onClickEdit: PropTypes.func.isRequired,
  openNotificationDialog: PropTypes.func.isRequired,
};

TableRow.defaultProps = {};

export default withStyles(s)(TableRow);
