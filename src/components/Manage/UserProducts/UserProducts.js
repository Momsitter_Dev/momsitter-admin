/* eslint-disable prettier/prettier */
import qs from 'qs';
import _ from 'lodash';
import uuid from 'uuid/v4';
import moment from 'moment';
import classnames from 'classnames';
import { compose } from 'recompose';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import Snackbar from '@material-ui/core/Snackbar';
import RaisedButton from 'material-ui/RaisedButton';
import FlatPagination from 'material-ui-flat-pagination';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './UserProducts.css';
import SearchBar from '../../SearchBar';
import TableHeaderRows from '../../TableHeaderRow';
import TableRow from './TableRow';
import Request from '../../../util/Request';
import AddProductDialog from '../../Dialog/AddProductDialog';
import ResponseResultTypeGroup from '../../../util/const/ResponseResultTypeGroup';
import EditProductDialog from '../../Dialog/EditProductDialog';
import FilterDataTypeGroup from '../../../util/const/FilterDataTypeGroup';
import isParent from '../../../util/user/isParent';

moment.locale('ko');

const propTypes = {};
const defaultProps = {};

class UserProducts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      page: 0,
      maxPage: 0,
      notificationDialog: false,
      notificationDialogMessage: '',
      addProductDialog: false,
      addProductDialogData: {
        userId: '',
        productId: '',
        paymentId: null,
        purchaseDate: null,
        expireDate: null,
        memo: '',
      },
      editProductDialog: false,
      editProductDialogData: {
        userId: '',
        productId: '',
        paymentId: '',
        purchaseDate: '',
        expireDate: '',
        memo: '',
        productStatus: '',
      },
      searchOptions: [],
      orderOption: {
        column: '',
        method: '',
      },
    };
    this.cachedOption = {};
    this.onClickSearch = this.onClickSearch.bind(this);
    this.onSaveAddUserProductData = this.onSaveAddUserProductData.bind(this);
    this.onEdit = this.onEdit.bind(this);
    this.onExportCSV = this.onExportCSV.bind(this);
    this.onCloseAddUserProductDialog = this.onCloseAddUserProductDialog.bind(this);
    this.openNotificationDialog = this.openNotificationDialog.bind(this);
    this.doSearch = this.doSearch.bind(this);
    this.onClickCloseNotificationDialog = this.onClickCloseNotificationDialog.bind(this);
  }

  async componentDidMount() {
    try {
      const data = await this.doSearch({});
      this.setState({
        list: data.data.rows,
        page: data.page.currentPage,
        maxPage: data.page.maxPage,
      });
    } catch (err) {
      console.log(err);
      this.setState({
        list: [],
        page: 0,
        maxPage: 0,
      });
    }
  }

  doSearch = async function (options) {
    try {
      const query = qs.stringify(options);
      this.cachedOption = Object.assign({}, options);
      const response = await Request(`api/manage/user/products?${query}`, {
        method: 'GET',
      });
      if (response.result === ResponseResultTypeGroup.SUCCESS) {
        return response;
      }
      throw new Error('request fail');
    } catch (err) {
      console.log(err);
      return [];
    }
  };

  onClickSearch = async function () {
    try {
      const { searchOptions, orderOption } = this.state;
      const response = await this.doSearch({
        searchOptions,
        orderOption,
        requestPage: 0,
      });
      return this.setState({
        list: response.data.rows,
        page: response.page.currentPage,
        maxPage: response.page.maxPage,
      });
    } catch (err) {
      console.log(err);
      return this.setState({
        list: [],
        page: 0,
        maxPage: 0,
      });
    }
  };

  onClickPage = async function (value) {
    try {
      this.cachedOption.requestPage = value;
      const response = await this.doSearch(this.cachedOption);
      return this.setState({
        list: response.data.rows,
        page: response.page.currentPage,
        maxPage: response.page.maxPage,
      });
    } catch (err) {
      console.log(err);
      return this.setState({
        list: [],
        page: 0,
        maxPage: 0,
      });
    }
  };

  onChangeOrderColumn = async function(columnId) {
    const { orderOption, searchOptions } = this.state;
    if (orderOption.column !== columnId) {
      orderOption.method = '';
    }
    orderOption.column = columnId;
    if (orderOption.method === 'DESC') {
      orderOption.method = 'ASC';
    } else if (orderOption.method === 'ASC') {
      orderOption.method = '';
    } else {
      orderOption.method = 'DESC';
    }
    this.setState({ orderOption });

    try {
      const response = await this.doSearch({
        searchOptions,
        orderOption,
        requestPage: 0,
      });
      return this.setState({
        list: response.data.rows,
        page: response.page.currentPage,
        maxPage: response.page.maxPage,
      });
    } catch (err) {
      return this.setState({
        list: [],
        page: 0,
        maxPage: 0,
      });
    }
  };

  onClickAddUserProduct = () => {
    return this.setState({
      addProductDialog: true,
    });
  };

  onCloseAddUserProductDialog = () => {
    return this.setState({
      addProductDialog: false,
      addProductDialogData: {
        userId: '',
        productId: '',
        paymentId: null,
        purchaseDate: null,
        expireDate: null,
        memo: '',
      },
    });
  };

  onChangeAddUserProductData = (type, value) => {
    const { addProductDialogData } = this.state;
    const newData = Object.assign({}, addProductDialogData);
    newData[type] = value;
    this.setState({
      addProductDialogData: newData,
    });
  };

  onSaveAddUserProductData = async function () {
    const { addProductDialogData, list } = this.state;
    if (this.validateAddProductDialogData(addProductDialogData)) {
      try {
        const response = await Request(
          `api/manage/user/${addProductDialogData.userId}/products`,
          {
            method: 'POST',
            data: addProductDialogData,
          },
        );
        if (response.result === ResponseResultTypeGroup.SUCCESS) {
          this.setState({
            list: [response.data, ...list],
          });
          // TODO : notification message saved;
          this.openNotificationDialog('저장되었습니다.');
          return this.onCloseAddUserProductDialog();
        }
        throw new Error('request fail');
      } catch (err) {
        console.log(err);
        return this.openNotificationDialog('네트워크 오류입니다.');
      }
    }
  };

  validateAddProductDialogData = data => {
    let result = true;
    if (data) {
      for (const key in data) {
        if (key === 'userId' && !data[key]) {
          this.openNotificationDialog('사용자 번호를 입력해 주세요.');
          return false;
        }
        if (key === 'productId' && !data[key]) {
          this.openNotificationDialog('이용권 번호를 입력해 주세요.');
          return false;
        }
        if (key === 'purchaseDate' && !data[key]) {
          this.openNotificationDialog('이용권 만료일자를 입력해 주세요.');
          return false;
        }
        if (key === 'expireDate' && !data[key]) {
          this.openNotificationDialog('이용권 만료일자를 입력해 주세요.');
          return false;
        }
      }
    } else {
      result = false;
    }
    return result;
  };

  openNotificationDialog = (message) => {
    return this.setState({
      notificationDialog: true,
      notificationDialogMessage: message,
    });
  };

  onClickCloseNotificationDialog = () => {
    return this.setState({
      notificationDialog: false,
      notificationDialogMessage: '',
    });
  };

  findAndReplaceProductItem = data => {
    const { list } = this.state;
    if (data && data.userProductId) {
      try {
        const targetInd = _.findIndex(
          list,
          o => o.userProductId === data.userProductId,
        );
        if (targetInd !== -1) {
          let newList = Object.assign([], list);
          if (targetInd > 0) {
            const forward = newList.slice(targetInd - 1);
            const backward = newList.slice(targetInd + 1);
            newList = [...forward, data, backward];
          } else {
            newList[targetInd] = data;
          }
          this.setState({
            list: newList,
          });
        }
      } catch (e) {
        console.log(e);
      }
    }
  };

  findAndDeleteProductItemByUserProductId = userProductId => {
    const { list } = this.state;
    const targetInd = _.findIndex(list, o => o.userProductId === userProductId);
    if (targetInd !== -1) {
      const newList = _.filter(list, (o, index) => index !== targetInd);
      this.setState({
        list: newList,
      });
    } else {
      // TODO: Handle Error
    }
  };

  onClickEdit = data => {
    try {
      return this.setState(
        {
          editProductDialogData: {
            ...data,
            purchaseDate: moment(data.purchaseDate).toDate(),
            expireDate: moment(data.expireDate).toDate(),
          },
        },
        () => {
          return this.openUserProductEditDialog();
        },
      );
    } catch (err) {
      return this.openNotificationDialog('오류가 발생했습니다.');
    }
  };

  openUserProductEditDialog = () => {
    return this.setState({
      editProductDialog: true,
    });
  };

  closeUserProductEditDialog = () => {
    return this.setState({
      editProductDialog: false,
      editProductDialogData: {
        userId: '',
        productId: '',
        paymentId: '',
        purchaseDate: '',
        expireDate: '',
        memo: '',
        productStatus: '',
      },
    });
  };

  onChangeEditData = (type, value) => {
    const { editProductDialogData } = this.state;
    const newEditData = Object.assign({}, editProductDialogData);
    newEditData[type] = value;
    this.setState({
      editProductDialogData: newEditData,
    });
  };

  onEdit = async function () {
    try {
      const { editProductDialogData } = this.state;
      if (!this.validateAddProductDialogData(editProductDialogData)) {
        throw new Error('입력값을 확인해 주세요.');
      }

      let userApplyCount = null;
      if (editProductDialogData.applyCount) {
        userApplyCount = editProductDialogData.applyCount || 0;
      } else {
        try {
          if (isParent(editProductDialogData.productUser.userRole[0]['userTypeId'])) {
            userApplyCount = editProductDialogData.userProduct.userApplyCount.applyToSitterCount;
          } else {
            userApplyCount = editProductDialogData.userProduct.userApplyCount.applyToParentCount;
          }
        } catch (err) {
          userApplyCount = 0;
        }
      }

      const response = await Request(
        `api/manage/user/products/${editProductDialogData.userProductId}`,
        {
          method: 'PUT',
          data: {
            ...editProductDialogData,
            applyCount: userApplyCount,
          },
        },
      );

      if (response.result === ResponseResultTypeGroup.SUCCESS) {
        this.openNotificationDialog('수정되었습니다.');
        this.findAndReplaceProductItem(editProductDialogData);
        return this.closeUserProductEditDialog();
      }
      throw new Error('네트워크 오류입니다.');
    } catch (err) {
      return this.openNotificationDialog(err.message);
    }
  };

  onDelete = async userProductId => {
    if (typeof window !== 'undefined') {
      if (userProductId) {
        if (window.confirm('정말 삭제하시겠습니까?')) {
          const response = await Request(
            `api/manage/user/products/${userProductId}`,
            {
              method: 'DELETE',
            },
          ).catch(err => {
            console.log(err);
            return false;
          });
          if (response && response.result === ResponseResultTypeGroup.SUCCESS) {
            this.openNotificationDialog('삭제되었습니다.');
            this.findAndDeleteProductItemByUserProductId(userProductId);
          } else {
            this.openNotificationDialog('네트워크 오류입니다.');
          }
        }
      } else {
        this.openNotificationDialog('필수정보가 누락되었습니다.');
      }
    } else {
      this.openNotificationDialog('브라우저 환경에서 이용해 주세요.');
    }
  };

  onExportCSV = async function () {
    const queryString = qs.stringify(this.cachedOption);
    window.open(`/api/manage/user/products/csv?${queryString}`);
  };

  onAddSearchOption = () => {
    const { searchOptions } = this.state;
    searchOptions.push({
      generatedKey: uuid(),
      filterType: '',
      filterValue: '',
      filterDataType: '',
    });
    this.setState({ searchOptions });
  };

  onDeleteSearchOption = generatedKey => {
    const { searchOptions } = this.state;
    _.remove(searchOptions, o => o.generatedKey === generatedKey);
    this.setState({ searchOptions });
  };

  onChangeSearchOption = (generatedKey, type, value, dataType) => {
    const { searchOptions } = this.state;
    const targetIndex = _.findIndex(
      searchOptions,
      o => o.generatedKey === generatedKey,
    );
    if (targetIndex !== -1) {
      searchOptions[targetIndex] = {
        ...searchOptions[targetIndex],
        filterType: type,
        filterValue: value,
        filterDataType: dataType,
      };
      this.setState({ searchOptions });
    } else {
      console.log(`DEBUG : ON CHANGE BUT NOT FOUND : ${generatedKey}`);
    }
  };

  renderUserProductList = list => {
    let ui = null;
    if (list) {
      ui = list.map(item => (
        <TableRow
          key={uuid()}
          data={item}
          onDelete={this.onDelete}
          onClickEdit={this.onClickEdit}
          openNotificationDialog={this.openNotificationDialog}
        />
      ));
    }
    return ui;
  };

  render() {
    const {
      list,
      page,
      maxPage,
      notificationDialog,
      notificationDialogMessage,
      addProductDialog,
      addProductDialogData,
      editProductDialog,
      editProductDialogData,
      searchOptions,
      orderOption,
    } = this.state;
    const headers = [
      { id: 'userProductId', desc: '식별번호', sortable: true },
      { id: 'userId', desc: '회원 번호', sortable: true },
      { id: 'userName', desc: '회원 이름', sortable: false },
      { id: 'userName', desc: '회원 권한', sortable: false },
      { id: 'productId', desc: '이용권 번호', sortable: true },
      { id: 'productName', desc: '이용권 이름', sortable: false },
      { id: 'paymentId', desc: '결제번호', sortable: true },
      { id: 'purchaseDate', desc: '구매일자', sortable: true },
      { id: 'expireDate', desc: '만료일자', sortable: true },
      { id: 'memo', desc: '이용권 메모', sortable: true },
      { id: 'applyCount', desc: '지원가능한 횟수', sortable: false },
      { id: 'actions', desc: '', sortable: false },
    ];
    const filter = [
      {
        name: '회원 이용권 번호',
        type: 'userProductId',
        dataType: FilterDataTypeGroup.UNIQUE_TEXT,
      },
      {
        name: '회원 번호',
        type: 'userId',
        dataType: FilterDataTypeGroup.UNIQUE_TEXT,
      },
      {
        name: '이용권 번호',
        type: 'productId',
        dataType: FilterDataTypeGroup.UNIQUE_TEXT,
      },
      {
        name: '결제 번호',
        type: 'paymentId',
        dataType: FilterDataTypeGroup.UNIQUE_TEXT,
      },
      {
        name: '구매일자',
        type: 'purchaseDate',
        dataType: FilterDataTypeGroup.DATE_RANGE,
      },
      {
        name: '만료일자',
        type: 'expireDate',
        dataType: FilterDataTypeGroup.DATE_RANGE,
      },
    ];

    return (
      <div className={classnames(s.root, ' box box-default')}>
        <SearchBar
          filter={filter}
          searchOptions={searchOptions}
          onClickSearch={this.onClickSearch}
          openNotificationDialog={this.openNotificationDialog}
          onExportCSV={this.onExportCSV}
          onClickAddSearchOption={this.onAddSearchOption}
          onClickDeleteSearchOption={this.onDeleteSearchOption}
          onChangeSearchOption={this.onChangeSearchOption}
        >
          <RaisedButton
            label="회원에게 이용권 추가하기"
            onClick={this.onClickAddUserProduct}
          />
        </SearchBar>
        <div className="box-body padding-sm">
          <table className={'table'}>
            <thead>
              <TableHeaderRows
                data={headers}
                orderOption={orderOption}
                onChangeOrderColumn={this.onChangeOrderColumn}
                openNotificationDialog={this.openNotificationDialog}
              />
            </thead>
            <tbody>{this.renderUserProductList(list)}</tbody>
          </table>
          <div>
            <FlatPagination
              offset={page}
              limit={1}
              total={maxPage}
              onClick={(e, offset) => this.onClickPage(offset)}
            />
          </div>
        </div>

        <AddProductDialog
          title="사용자에게 이용권 추가하기"
          open={addProductDialog}
          data={addProductDialogData}
          closeDialog={this.onCloseAddUserProductDialog}
          onChange={this.onChangeAddUserProductData}
          onCreate={this.onSaveAddUserProductData}
        />
        <EditProductDialog
          title="이용권 수정하기"
          open={editProductDialog}
          data={editProductDialogData}
          onChange={this.onChangeEditData}
          onSubmit={this.onEdit}
          closeDialog={this.closeUserProductEditDialog}
        />
        <Snackbar
          open={notificationDialog}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          ContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={(<span id="message-id"> {notificationDialogMessage} </span>)}
          onClose={this.onClickCloseNotificationDialog}
        />
      </div>
    );
  }
}

UserProducts.propTypes = propTypes;
UserProducts.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: false }),
)(UserProducts);
