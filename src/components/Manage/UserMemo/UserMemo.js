/* eslint-disable prettier/prettier */
import _ from 'lodash';
import uuid from 'uuid/v4';

import { compose } from 'recompose';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import CircularProgress from '@material-ui/core/CircularProgress';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './UserMemo.css';
import MemoItem from './MemoItem';
import FormDialog from './FormDialog';
import Request from '../../../util/Request';
import ResponseResultTypeGroup from '../../../util/const/ResponseResultTypeGroup';

import TextField from '../../TextField';
import MemoStatusGroup from '../../../util/const/MemoStatusGroup';
import MemoClassGroup from '../../../util/const/MemoClassGroup';
import MemoTypeGroup from '../../../util/const/MemoTypeGroup';
import NotificationDialog from '../../Dialog/NotificationDialog';

const propTypes = {
  userId: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
};
const defaultProps = {};

class UserMemo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      formDialog: false,
      formDialogTitle: '',
      formDialogData: {
        memoResId: null,
        memoType: MemoTypeGroup.QUESTION,
        memoClass: MemoClassGroup.INFO_ERROR,
        memoStatus: MemoStatusGroup.NEW,
        memoContent: '',
        memoReqMethod: 'call',
      },
      simpleFormDialog: false,
      simpleFormDialogValue: '',
      simpleFormDialogType: null,
      simpleFormDialogData: null,
      simpleFormDialogTitle: '',
      userData: null,
      userMemo: null,
      userMemoHierarchy: null,
      notificationDialog: false,
      notificationDialogTitle: '',
      notificationDialogMessage: '',
      onFetching: true,
    };
    this.onDeleteMemo = this.onDeleteMemo.bind(this);
    this.onEditMemo = this.onEditMemo.bind(this);
    this.onCreateMemo = this.onCreateMemo.bind(this);
  }
  async componentDidMount() {
    const userId = this.props.userId;
    const response = await Request(
      `api/manage/user/${userId}/memos`,
      { method: 'GET' },
    ).catch(() => false);

    if (response && response.result === 'success') {
      try {
        const tempUserMemo = response.data.rows[0].userMemo;
        const tempUserData = response.data.rows[0];
        this.setState({
          userMemo: tempUserMemo,
          userMemoHierarchy: this.generateMemoHierarchy(tempUserMemo),
          userData: Object.assign({}, tempUserData, { userMemo: null }),
          onFetching: false,
        });
      } catch (err) {
        console.log(err);
        this.setState({
          userMemo: null,
          userData: null,
          onFetching: false,
        });
      }
    } else {
      this.openNotificationDialog('오류', '네트워크 오류입니다.');
    }
  }

  generateMemoHierarchy = (list) => {
    let result = Object.assign([], list);
    if (result && result.length > 0) {
      for (let i = 0; i < result.length; i += 1) {
        result[i].childMemo = this.getChildHierarchy(result[i], list);
      }
    }
    return _.filter(result, (o) => {
      return o.memoResId === null
    });
  };

  getChildHierarchy = (memo, list) => {
    if (memo && memo.memoId) {
      return memo.childMemo = _.filter(list, (o) => {
        return o.memoResId === memo.memoId;
      });
    } else {
      return null;
    }
  };

  onDeleteMemo = async function (userId, memoId) {
    const { userMemo } = this.state;
    const response = await Request(
      `api/manage/user/${userId}/memos/${memoId}`,
      { method: 'DELETE' },
    ).catch(() => false);
    if (response && response.result === 'success') {
      this.openNotificationDialog('알림', '메모가 삭제되었습니다.');
      const targetMemoDataIndex = _.findIndex(userMemo, (o) => {
        return o.memoId === memoId
      });
      if (targetMemoDataIndex !== -1) {
        const newUserMemo = _.filter(userMemo, (o) => {
          return o.memoId !== memoId;
        });
        this.openNotificationDialog('알림', '새로고침 후에 이용해 주세요.');
        this.setState({
          userMemo: newUserMemo,
          userMemoHierarchy: this.generateMemoHierarchy(newUserMemo),
        });
      } else {
        this.openNotificationDialog('알림', '새로고침 후에 이용해 주세요.');
      }
    } else {
      this.openNotificationDialog('오류', '네트워크 오류입니다.');
    }
  };

  onEditMemo = async function (memoId) {
    const { userMemo } = this.state;
    const targetMemoId = _.findIndex(userMemo, (o) => {
      return o.memoId === memoId;
    });
    if (targetMemoId !== -1) {
      const targetMemoData = userMemo[targetMemoId];
      if (targetMemoData) {
        const response = await Request(`api/manage/user/${targetMemoData.userId}/memos/${targetMemoData.memoId}`, {
          method: 'PUT',
          data: targetMemoData,
        });
        if (response && response.result === ResponseResultTypeGroup.SUCCESS) {
          this.openNotificationDialog('알림', '수정되었습니다.');
        } else {
          this.openNotificationDialog('오류', '네트워크 오류입니다.');
        }
      } else {
        this.openNotificationDialog('오류', '메모 정보를 찾을 수 없습니다. 개발자에게 문의해 주세요.');
      }
    } else {
      this.openNotificationDialog('오류', '메모 정보를 찾을 수 없습니다. 개발자에게 문의해 주세요.');
    }
  };

  onCreateMemo = async function () {
    const { userData, formDialogData, userMemo } = this.state;
    if (userData && userData.userId) {
      const response = await Request(`api/manage/user/${userData.userId}/memos`, {
        method: 'POST',
        data: {
          ...formDialogData,
          userId: userData.userId,
        }
      });
      if (response && response.result === ResponseResultTypeGroup.SUCCESS) {
        this.closeFormDialog();
        this.openNotificationDialog('알림', '저장되었습니다.');
        let newUserMemo = Object.assign([], userMemo);
        newUserMemo.push(response.data);
        this.setState({
          userMemo: newUserMemo,
          userMemoHierarchy: this.generateMemoHierarchy(newUserMemo),
        });
      } else {
        this.openNotificationDialog('오류', '네트워크 오류입니다.');
      }
    } else {
      this.openNotificationDialog('오류', '회원정보를 찾을 수 없습니다.');
    }
  };

  onChangeNewMemo = (type, value) => {
    const { formDialogData } = this.state;
    let newMemoData = Object.assign({}, formDialogData);
    newMemoData[type] = value;
    this.setState({
      formDialogData: newMemoData,
    });
  };

  openFormDialog = () => {
    this.setState({
      formDialog: true,
      formDialogTitle: '새로운 메모 등록',
    });
  };

  closeFormDialog = () => {
    this.setState({
      formDialog: false,
      formDialogTitle: '',
      formDialogData: {
        memoResId: null,
        memoType: MemoTypeGroup.QUESTION,
        memoClass: MemoClassGroup.INFO_ERROR,
        memoStatus: MemoStatusGroup.NEW,
        memoContent: '',
        memoReqMethod: 'call',
      }
    });
  };

  onClickAddNewMemo = (data) => {
    if (data) {
      this.setState({
        formDialogData: {
          ...this.state.formDialogData,
          ...data,
        },
      }) ;
    }
    this.openFormDialog();
  };

  findMemoIndByMemoId = (memoId) => {
    const { userMemo } = this.state;
    const targetMemoInd = _.findIndex(userMemo, (o) => {
      return o.memoId === memoId;
    });
    return targetMemoInd;
  };

  findMemoDataByMemoId = (memoId) => {
    let memoData = null;
    const { userMemo } = this.state;
    const targetMemoId = _.findIndex(userMemo, (o) => {
      return o.memoId === memoId;
    });
    if (targetMemoId !== -1) {
      memoData = userMemo[targetMemoId];
    }
    return memoData;
  };

  onChangeMemo = (memoId, type, value) => {
    const { userMemo } = this.state;
    const targetMemoInd = _.findIndex(userMemo, (o) => {
      return o.memoId === memoId;
    });

    if (targetMemoInd !== -1) {
      const targetMemoData = Object.assign({}, userMemo[targetMemoInd]);
      targetMemoData[type] = value;

      const newUserMemo = Object.assign([], userMemo);
      newUserMemo[targetMemoInd] = targetMemoData;
      this.setState({
        userMemo: newUserMemo,
        userMemoHierarchy: this.generateMemoHierarchy(newUserMemo),
      });
    } else {
      this.openNotificationDialog('오류', '메모정보를 찾을 수 없습니다. 개발자에게 문의해주세요.');
    }
  };

  onClickEditData = (type, memoData) => {
    this.setState({
      simpleFormDialogType: type,
      simpleFormDialogTitle: type,
      simpleFormDialogValue: memoData[type],
      simpleFormDialogData: memoData,
    });
    this.openSimpleFormDialog();
  };

  saveSimpleFormDialog = () => {
    const {
      simpleFormDialogType,
      simpleFormDialogValue,
      simpleFormDialogData,
    } = this.state;
    let userMemo = this.state.userMemo;
    // TODO: FIND MEMO AND SET DATA;
    const targetMemoId = simpleFormDialogData.memoId;
    if (targetMemoId) {
      const targetInd = _.findIndex(userMemo, (o) => {
        return o.memoId === targetMemoId;
      });

      if (targetInd !== -1) {
        let targetMemo = userMemo[targetInd];
        if (simpleFormDialogType === 'memoResId') {
          targetMemo[simpleFormDialogType] = parseInt(simpleFormDialogValue);
        } else {
          targetMemo[simpleFormDialogType] = simpleFormDialogValue;
        }
        userMemo[targetInd] = targetMemo;
        this.setState({
          userMemo: userMemo,
          userMemoHierarchy: this.generateMemoHierarchy(userMemo),
        });
        this.openNotificationDialog('반영하시려면 수정버튼을 눌러주세요.');
        this.closeSimpleFormDialog();
      } else {
        this.openNotificationDialog('오류', '메모 정보를 찾을 수 없습니다.\n 새로고침 후 이용해 주세요.');
        this.closeSimpleFormDialog();
      }
    } else {
      this.openNotificationDialog('오류', '메모 정보를 찾을 수 없습니다.\n 새로고침 후 이용해 주세요.');
      this.closeSimpleFormDialog();
    }
  };

  changeSimpleFormDialogData = (value) => {
    this.setState({
      simpleFormDialogValue: value,
    });
  };

  openSimpleFormDialog = () => {
    this.setState({
      simpleFormDialog: true,
    });
  };

  closeSimpleFormDialog = () => {
    this.setState({
      simpleFormDialog: false,
    });
  };

  openNotificationDialog = (title, message) => {
    this.setState({
      notificationDialog: true,
      notificationDialogTitle: title,
      notificationDialogMessage: message,
    });
  };

  closeNotificationDialog = () => {
    this.setState({
      notificationDialog: false,
      notificationDialogTitle: '',
      notificationDialogMessage: '',
    });
  };

  renderMemoListUI = (onFetching, userMemo) => {
    let ui = (<div> 메모가 없습니다. </div>);
    if (onFetching) {
      ui = (
        <div className={s.fetching}>
          <CircularProgress
            size={100}
          />
          <div className={s.fetchingDesc}>정보를 가져오는 중입니다.</div>
          <div className={s.fetchingDesc}>잠시만 기다려주세요.</div>
        </div>
      );
    } else {
      if (userMemo && userMemo.length > 0) {
        ui = userMemo.map((item) => {
          return (
            <div key={uuid()}>
              <MemoItem
                data={item}
                openNotificationDialog={this.openNotificationDialog}
                onDeleteMemo={this.onDeleteMemo}
                onEditMemo={this.onEditMemo}
                onChangeMemo={this.onChangeMemo}
                onClickMemoId={this.onClickAddNewMemo}
                onClickMemoResId={this.onClickEditData}
                onClickMemoContent={this.onClickEditData}
              />
              {this.renderChildMemoListUI(item.childMemo, 1)}
            </div>
          );
        });
      }
    }
    return ui;
  };

  renderChildMemoListUI = (memo, depth) => {
    let ui = null;
    if (memo && memo.length > 0) {
      ui = memo.map((item) => {
        return (
          <div style={{ paddingLeft: `${20 * depth}px` }} key={uuid()}>
            <MemoItem
              key={uuid()}
              data={item}
              openNotificationDialog={this.openNotificationDialog}
              onDeleteMemo={this.onDeleteMemo}
              onEditMemo={this.onEditMemo}
              onChangeMemo={this.onChangeMemo}
              onClickMemoId={this.onClickAddNewMemo}
              onClickMemoResId={this.onClickEditData}
              onClickMemoContent={this.onClickEditData}
            />
            {this.renderChildMemoListUI(item.childMemo, depth + 1)}
          </div>
        );
      });
    }
    return ui;
  };

  render() {
    const {
      formDialog,
      formDialogTitle,
      formDialogData,
      notificationDialog,
      notificationDialogTitle,
      notificationDialogMessage,
      onFetching,
      userMemoHierarchy,
      simpleFormDialog,
      simpleFormDialogValue,
      simpleFormDialogTitle
    } = this.state;
    return (
      <div className={s.root}>
        <Grid container spacing={16}>
          <Grid item xs={12}>
            <Button variant={'raised'} onClick={this.onClickAddNewMemo}> 새로운 메모 추가 </Button>
          </Grid>
          <Grid item xs={12}>
            {this.renderMemoListUI(onFetching, userMemoHierarchy)}
          </Grid>
        </Grid>
        <Dialog
          open={simpleFormDialog}
          onClose={this.closeSimpleFormDialog}
        >
          <DialogTitle> {simpleFormDialogTitle} </DialogTitle>
          <DialogContent style={{ minWidth: '500px' }}>
            <Grid container spacing={16}>
              <TextField
                id={uuid()}
                onChange={(value) => this.changeSimpleFormDialogData(value)}
                value={simpleFormDialogValue || ''}
              />
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button variant={'flat'} onClick={this.closeSimpleFormDialog}> 취소 </Button>
            <Button variant={'raised'} onClick={this.saveSimpleFormDialog}> 저장 </Button>
          </DialogActions>
        </Dialog>
        <FormDialog
          modal
          open={formDialog}
          title={formDialogTitle}
          closeDialog={this.closeFormDialog}
          onCreateMemo={this.onCreateMemo}
          onChangeNewMemo={this.onChangeNewMemo}
          data={formDialogData}
        />
        <NotificationDialog
          open={notificationDialog}
          title={notificationDialogTitle}
          message={notificationDialogMessage}
          onClose={this.closeNotificationDialog}
        />
      </div>
    );
  }
}

UserMemo.propTypes = propTypes;
UserMemo.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(UserMemo);
