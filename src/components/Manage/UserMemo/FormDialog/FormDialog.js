/* eslint-disable prettier/prettier */
import uuid from 'uuid';
import { compose } from 'recompose';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './FormDialog.css';
import TextField from '../../../TextField';
import MemoTypeGroup from '../../../../util/const/MemoTypeGroup';
import MemoStatusGroup from '../../../../util/const/MemoStatusGroup';
import MemoReqMethodGroup from '../../../../util/const/MemoReqMethodGroup';
import MemoClassGroup from '../../../../util/const/MemoClassGroup';

const commonRadioButtonGroupStyle = {
  marginBottom: '20px',
  paddingLeft: '15px',
};
const commonRadioButtonStyle = {
  display: 'inline-block',
  maxWidth: '125px',
};

const propTypes = {
  open: PropTypes.bool,
  title: PropTypes.string,
  closeDialog: PropTypes.func,
  data: PropTypes.object,
  onCreateMemo: PropTypes.func.isRequired,
  onChangeNewMemo: PropTypes.func.isRequired,
};
const defaultProps = {
  open: false,
  title: '',
  closeDialog: () => {},
  data: {},
};

class FormDialog extends React.Component {
  constructor(props) {
    super(props);
    this.onClickSubmit = this.onClickSubmit.bind(this);
  }
  onClickSubmit = async function () {
    return this.props.onCreateMemo();
  };

  onChangeForm = (type, value) => {
    return this.props.onChangeNewMemo(type, value);
  };

  onCloseDialog = () => {
    return this.props.closeDialog();
  };

  renderResId = (memoResId) => {
    if (memoResId) {
      return (
        <React.Fragment>
          <Grid item xs={3}>
            <Typography variant={'caption'}> 연관 메모 번호 </Typography>
          </Grid>
          <Grid item xs={9}>
            <TextField
              id={uuid()}
              value={memoResId}
              disabled
              onChange={(value) => this.onChangeForm('memoResId', value)}
            />
          </Grid>
        </React.Fragment>
      );
    }
    return null;
  };

  renderMemoClass = () => {
    const { memoClass, memoType } = this.props.data;
    if (memoType && memoType === MemoTypeGroup.ADMIN) {
      return (
        <React.Fragment>
          <Grid item xs={3}>
            <Typography variant={'caption'}> 메모 구분 </Typography>
          </Grid>
          <Grid item xs={9}>
            <RadioButtonGroup
              name="memoClassGroup"
              defaultSelected={memoClass || 'info_error'}
              onChange={(event, value) => this.onChangeForm('memoClass', value)}
              style={commonRadioButtonGroupStyle}
            >
              <RadioButton style={commonRadioButtonStyle} value={MemoClassGroup.INFO_ERROR} label="정보오류"/>
              <RadioButton style={commonRadioButtonStyle} value={MemoClassGroup.PROFILE_ERROR} label="사진오류"/>
              <RadioButton style={commonRadioButtonStyle} value={MemoClassGroup.OTHER} label="기타"/>
              <RadioButton style={commonRadioButtonStyle} value={MemoClassGroup.NOT_IN_USE} label="미사용"/>
            </RadioButtonGroup>
          </Grid>
        </React.Fragment>
      );
    }
    return null;
  };

  renderReqMethod = () => {
    const { memoReqMethod, memoType } = this.props.data;
    let ui = null;
    if (memoType && memoType !== MemoTypeGroup.ADMIN) {
      ui = (
        <React.Fragment>
          <Grid item xs={3}>
            <Typography variant={'caption'}> 메모 요청 방법 </Typography>
          </Grid>
          <Grid item xs={9}>
            <RadioButtonGroup
              name="memoReqMethod"
              defaultSelected={memoReqMethod || 'call'}
              onChange={(event, value) => this.onChangeForm('memoReqMethod', value)}
              style={commonRadioButtonGroupStyle}
            >
              <RadioButton value={MemoReqMethodGroup.CALL} label="전화" style={commonRadioButtonStyle} />
              <RadioButton value={MemoReqMethodGroup.KAKAO} label="카카오톡" style={commonRadioButtonStyle} />
              <RadioButton value={MemoReqMethodGroup.MAIL} label="메일" style={commonRadioButtonStyle} />
              <RadioButton value={MemoReqMethodGroup.SMS} label="문자" style={commonRadioButtonStyle} />
            </RadioButtonGroup>
          </Grid>
        </React.Fragment>
      );
    }
    return ui;
  };

  render() {
    const {
      title,
      open,
    } = this.props;

    const {
      memoResId,
      memoType,
      memoContent,
      memoStatus,
    } = this.props.data;

    return (
      <Dialog
        open={open}
        onClose={this.onCloseDialog}
      >
        <DialogTitle> {title} </DialogTitle>
        <DialogContent>
          <Grid container spacing={16}>
            {this.renderResId(memoResId)}
            <Grid item xs={3}>
              <Typography variant={'caption'}> 메모 타입 </Typography>
            </Grid>
            <Grid item xs={9}>
              <RadioButtonGroup
                name="memoTypeGroup"
                defaultSelected={memoType || MemoTypeGroup.QUESTION}
                onChange={(event, value) => this.onChangeForm('memoType', value)}
                style={commonRadioButtonGroupStyle}
              >
                <RadioButton
                  style={commonRadioButtonStyle}
                  value={MemoTypeGroup.QUESTION}
                  label="문의"
                />
                <RadioButton
                  style={commonRadioButtonStyle}
                  value={MemoTypeGroup.REQUEST}
                  label="요청"
                />
                <RadioButton
                  style={commonRadioButtonStyle}
                  value={MemoTypeGroup.COMPLAIN}
                  label="불만"
                />
                <RadioButton
                  style={commonRadioButtonStyle}
                  value={MemoTypeGroup.ADMIN}
                  label="관리자"
                />
              </RadioButtonGroup>
            </Grid>

            {this.renderMemoClass()}
            {this.renderReqMethod()}

            <Grid item xs={3}>
              <Typography variant={'caption'}> 메모 상태 </Typography>
            </Grid>
            <Grid item xs={9}>
              <RadioButtonGroup
                name="memoStatusGroup"
                defaultSelected={memoStatus || MemoStatusGroup.NEW}
                onChange={(event, value) => this.onChangeForm('memoStatus', value)}
                style={commonRadioButtonGroupStyle}
              >
                <RadioButton
                  style={commonRadioButtonStyle}
                  value={MemoStatusGroup.NEW}
                  label="신규"
                />
                <RadioButton
                  style={commonRadioButtonStyle}
                  value={MemoStatusGroup.DONE}
                  label="완료"
                />
              </RadioButtonGroup>
            </Grid>

            <Grid item xs={3}>
              <Typography variant={'caption'}> 메모 내용 </Typography>
            </Grid>
            <Grid item xs={9}>
              <TextField
                id={uuid()}
                value={memoContent}
                onChange={(value) => this.onChangeForm('memoContent', value)}
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button variant={'flat'} onClick={this.onCloseDialog}>
            취소
          </Button>
          <Button variant={'raised'} onClick={this.onClickSubmit}>
            저장
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

FormDialog.propTypes = propTypes;
FormDialog.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(FormDialog);
