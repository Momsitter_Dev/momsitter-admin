import Paper from 'material-ui/Paper';
import EnhancedButton from 'material-ui/internal/EnhancedButton';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './DataHeaderRow.css';

const propTypes = {
  onChangeOrderColumn: PropTypes.func,
};
const defaultProps = {
  onChangeOrderColumn: () => {},
};

class DataHeaderRow extends React.Component {
  onClickColumn = (columnName) => {
    return console.log(columnName);
  };

  render() {
    return (
      <Paper zDepth={0} style={{ backgroundColor: 'transparent' }}>
        <div className={s.root} style={{ borderBottom: '1px solid #e9e9e9' }}>
          <div style={{ flexBasis: '5%' }}>

          </div>
          <div style={{ flexBasis: '5%', textAlign: 'center', display: 'flex', overflow: 'hidden' }}>
            <EnhancedButton onClick={() => this.onClickColumn()}>
              <div className={s.btnSort}> 쿠폰번호 </div>
            </EnhancedButton>
          </div>
          <div style={{ flexBasis: '5%', textAlign: 'center' }}>
            <EnhancedButton onClick={() => this.onClickColumn()}>
              <div className={s.btnSort}> 회원번호 </div>
            </EnhancedButton>
          </div>
          <div style={{ flexBasis: '15%', textAlign: 'center' }}>
            <EnhancedButton onClick={() => this.onClickColumn()}>
              <div className={s.btnSort}> 쿠폰이름 </div>
            </EnhancedButton>
          </div>
          <div style={{ flexBasis: '5%', textAlign: 'center' }}>
            <EnhancedButton onClick={() => this.onClickColumn()}>
              <div className={s.btnSort}> 쿠폰타입 </div>
            </EnhancedButton>
          </div>
          <div style={{ flexBasis: '5%', textAlign: 'center' }}>
            <EnhancedButton onClick={() => this.onClickColumn()}>
              <div className={s.btnSort}> 코드번호 </div>
            </EnhancedButton>
          </div>
          <div style={{ flexBasis: '15%', textAlign: 'center' }}>
            <EnhancedButton onClick={() => this.onClickColumn()}>
              <div className={s.btnSort}> 만료일자 </div>
            </EnhancedButton>
          </div>
          <div style={{ flexBasis: '15%', textAlign: 'center' }}>
            <EnhancedButton onClick={() => this.onClickColumn()}>
              <div className={s.btnSort}> 발급일자 </div>
            </EnhancedButton>
          </div>
          <div style={{ flexBasis: '15%', textAlign: 'center' }}>
            <EnhancedButton onClick={() => this.onClickColumn()}>
              <div className={s.btnSort}> 사용일자 </div>
            </EnhancedButton>
          </div>
          <div style={{ flexBasis: '10%', textAlign: 'center' }}>
            <EnhancedButton onClick={() => this.onClickColumn()}>
              <div className={s.btnSort}> 쿠폰상태 </div>
            </EnhancedButton>
          </div>
          <div style={{ flexBasis: '5%', textAlign: 'center' }}>
            <EnhancedButton onClick={() => this.onClickColumn()}>
              <div className={s.btnSort}> 결제번호 </div>
            </EnhancedButton>
          </div>
          <div style={{ flexBasis: '5%', textAlign: 'center' }}>
            <EnhancedButton onClick={() => this.onClickColumn()}>
              <div className={s.btnSort}> 확인여부 </div>
            </EnhancedButton>
          </div>
        </div>
      </Paper>
    );
  }
}

DataHeaderRow.propTypes = propTypes;
DataHeaderRow.defaultProps = defaultProps;

export default withStyles(s)(DataHeaderRow);
