import IconButton from 'material-ui/IconButton';
import EnhancedButton from 'material-ui/internal/EnhancedButton';
import IconCircle from 'material-ui/svg-icons/av/fiber-manual-record';
import IconCheckBox from 'material-ui/svg-icons/toggle/radio-button-unchecked';
import IconCheckdBox from 'material-ui/svg-icons/action/check-circle';
import Paper from 'material-ui/Paper';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './DataRow.css';
import SimpleDateFormat from '../../../../util/dataFormat/SimpleDateFormat';
import CouponStatusGroup from '../../../../util/const/CouponStatusGroup';
import CouponCodeTypeGroup from '../../../../util/const/CouponCodeTypeGroup';

const propTypes = {
  data: PropTypes.shape({
    couponId: PropTypes.number,
    userId: PropTypes.number,
    couponTypeId: PropTypes.number,
    codeId: PropTypes.number,
    expireDate: PropTypes.string,
    useDate: PropTypes.string,
    pubDate: PropTypes.string,
    status: PropTypes.string,
    paymentId: PropTypes.number,
    read: PropTypes.bool,
    isChecked: PropTypes.bool,
    guid: PropTypes.string,
    couponType: PropTypes.shape({
      couponDate: PropTypes.string,
      couponName: PropTypes.string,
      couponPrice: PropTypes.number,
      couponTypeId: PropTypes.number,
      registerDate: PropTypes.string,
      status: PropTypes.string,
      target: PropTypes.string,
    }),
  }),
  onClick: PropTypes.func,
};
const defaultProps = {
  data: {},
  onClick: () => {},
};

class DataRow extends React.Component {
  onClickUserId = () => {
    return window.open(`/users?userId=${this.props.data.userId}`);
  };

  onToggleCheckbox = () => {
    return this.props.onClick(this.props.data.guid);
  };

  getCheckedStyle = () => {
    if (this.props.data.isChecked) {
      return {
        border: '1px solid #8ac34a',
      };
    }
    return {};
  };

  renderCheckbox = () => {
    if (this.props.data.isChecked) {
      return (
        <IconButton
          onClick={this.onToggleCheckbox}
          iconStyle={{ width: '25px', height: '25px', color: '#8ac34a' }}
        >
          <IconCheckdBox />
        </IconButton>
      );
    }
    return (
      <IconButton
        onClick={this.onToggleCheckbox}
        iconStyle={{ width: '25px', height: '25px', color: '#e1e1e1' }}
      >
        <IconCheckBox />
      </IconButton>
    );
  };

  renderCouponStatus = (status) => {
    if (status === CouponStatusGroup.USED) {
      return (
        <div className={s.couponStatus}>
          <IconCircle style={{ width: '15px', height: '15px', color: '#656665' }} />
          <span className={s.label}> 사용됨 </span>
        </div>
      )
    }

    if (status === CouponStatusGroup.AVAILABLE) {
      return (
        <div className={s.couponStatus}>
          <IconCircle style={{ width: '15px', height: '15px', color: '#8bc34a' }} />
          <span className={s.label}> 사용가능 </span>
        </div>
      )
    }

    return (
      <div className={s.couponStatus}>
        <IconCircle style={{ width: '15px', height: '15px', color: '#F44338' }} />
        <span className={s.label}> 알수없음 </span>
      </div>
    )
  };

  render() {
    const { data } = this.props;
    return (
      <Paper zDepth={1}>
        <div className={s.root} style={this.getCheckedStyle()}>
          <div className={s.column} style={{ flexBasis: '5%' }}>
            {this.renderCheckbox()}
          </div>
          <div className={s.column} style={{ flexBasis: '5%' }}> {data.couponId} </div>
          <div className={s.column} style={{ flexBasis: '5%' }}>
            <EnhancedButton>
              <div
                onClick={this.onClickUserId}
                style={{ padding: '5px' }}
              >
                {data.userId}
              </div>
            </EnhancedButton>
          </div>
          <div className={s.column} style={{ flexBasis: '15%' }}>
            {data.couponType.couponName}
          </div>
          <div className={s.column} style={{ flexBasis: '5%' }}> {data.couponTypeId} </div>
          <div className={s.column} style={{ flexBasis: '5%' }}> {data.codeId} </div>
          <div className={s.column} style={{ flexBasis: '15%' }}>
            <span className={s.date}> {SimpleDateFormat(data.expireDate, 'YYYY년 MM월 DD일')} </span>
          </div>
          <div className={s.column} style={{ flexBasis: '15%' }}>
            <span className={s.date}> {SimpleDateFormat(data.pubDate, 'YYYY년 MM월 DD일')} </span>
          </div>
          <div className={s.column} style={{ flexBasis: '15%' }}>
            <span className={s.date}> {SimpleDateFormat(data.useDate, 'YYYY년 MM월 DD일') || '미사용'} </span>
          </div>
          <div className={s.column} style={{ flexBasis: '10%' }}>
            {this.renderCouponStatus(data.status)}
          </div>
          <div className={s.column} style={{ flexBasis: '5%' }}>
            {data.paymentId}
          </div>
          <div className={s.column} style={{ flexBasis: '5%' }}>
            {data.read}
          </div>
        </div>
      </Paper>
    );
  }
}

DataRow.propTypes = propTypes;
DataRow.defaultProps = defaultProps;

export default withStyles(s)(DataRow);
