import qs from 'qs';
import _ from 'lodash';
import uuid from 'uuid/v4';
import FlatPagination from 'material-ui-flat-pagination';
import CircularProgress from 'material-ui/CircularProgress';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Coupons.css';
import SearchBar from '../../SearchBar';
import Request from '../../../util/Request';
import DataHeaderRow from './DataHeaderRow';
import DataRow from './DataRow';
import FilterDataTypeGroup from '../../../util/const/FilterDataTypeGroup';
import ResponseResultTypeGroup from '../../../util/const/ResponseResultTypeGroup';

class Coupons extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      page: 0,
      maxPage: 0,
      addDialog: false,
      pending: true,
      searchOptions: [],
      orderOption: {
        column: '',
        method: '',
      },
    };
    this.cachedOption = {};
    this.onClickSearch = this.onClickSearch.bind(this);
  }

  async componentDidMount() {
    await this.doSearch({})
      .then(data => {
        this.setState({
          list: data.data.rows.map((item) => {
            return {
              ...item,
              guid: uuid(),
              isChecked: false,
            };
          }),
          page: data.page.currentPage,
          maxPage: data.page.maxPage,
          pending: false,
        });
      })
      .catch(() => {
        this.setState({
          list: [],
          pending: false,
        });
      });
  }

  onClickExportCSV = () => {
    try {
      const queryString = qs.stringify(this.cachedOption);
      return window.open(`/api/manage/coupons/csv?${queryString}`);
    } catch (err) {
      // TODO: Log tracking
      return null;
    }
  };

  onAddSearchOption = () => {
    const { searchOptions } = this.state;
    searchOptions.push({
      generatedKey: uuid(),
      filterType: '',
      filterValue: '',
      filterDataType: '',
    });
    this.setState({ searchOptions });
  };

  onDeleteSearchOption = (generatedKey) => {
    const { searchOptions } = this.state;
    _.remove(searchOptions, (o) => {
      return o.generatedKey === generatedKey;
    });
    this.setState({ searchOptions });
  };

  onChangeSearchOption = (generatedKey, type, value, dataType) => {
    const { searchOptions } = this.state;
    const targetIndex = _.findIndex(searchOptions, (o) => {
      return o.generatedKey === generatedKey;
    });
    if (targetIndex !== -1) {
      searchOptions[targetIndex] = {
        ...searchOptions[targetIndex],
        filterType: type,
        filterValue: value,
        filterDataType: dataType,
      };
      this.setState({ searchOptions });
    } else {
      console.log('DEBUG : ON CHANGE BUT NOT FOUND : ' + generatedKey);
    }
  };

  onClickCheckBox = (guid) => {
    try {
      const { list } = this.state;
      const targetIndex = list.findIndex((item) => {
        return item.guid === guid;
      });
      if (targetIndex === -1) {
        throw new Error('오류 데이터');
      }
      const updateList = Object.assign([], list);
      updateList[targetIndex].isChecked = !updateList[targetIndex].isChecked;
      return this.setState({ list: updateList });
    } catch (err) {
      console.log(err);
      return null;
    }
  };

  doSearch = options =>
    new Promise(async (resolve, reject) => {
      try {
        const query = qs.stringify(options);
        this.cachedOption = Object.assign({}, options);
        const searchResult = await Request(
          `api/manage/coupons?${query}`,
          { method: 'GET' },
        ).catch(() => false);
        if (searchResult && searchResult.result === ResponseResultTypeGroup.SUCCESS) {
          return resolve(searchResult);
        }
        throw new Error('network error');
      } catch (err) {
        return reject(err);
      }
    });

  onClickSearch = async function () {
    // 페이지를 1로 초기화
    const { searchOptions, orderOption } = this.state;
    await this.doSearch({
      searchOptions,
      orderOption,
      requestPage: 0,
    }).then(data => {
      this.setState({
        list: data.data.rows,
        page: data.page.currentPage,
        maxPage: data.page.maxPage,
      });
    }).catch(() => {
      this.setState({ list: [] });
    });
  };

  onClickPage = value => {
    this.cachedOption.requestPage = value;
    this.doSearch(this.cachedOption).then(data => {
      this.setState({
        list: data.data.rows.map((item) => {
          return {
            ...item,
            guid: uuid(),
            isChecked: false,
          };
        }),
        page: data.page.currentPage,
        maxPage: data.page.maxPage,
      });
    }).catch(() => {
      this.setState({ list: [] });
    });
  };

  onChangeOrderColumn = async function (columnId) {
    const { orderOption, searchOptions } = this.state;
    if (orderOption.column !== columnId) {
      orderOption.method = '';
    }
    orderOption.column = columnId;
    if (orderOption.method === 'DESC') {
      orderOption.method = 'ASC';
    } else if (orderOption.method === 'ASC') {
      orderOption.method = '';
    } else {
      orderOption.method = 'DESC'
    }
    this.setState({ orderOption });

    await this.doSearch({
      searchOptions,
      orderOption,
      requestPage: 0,
    }).then(data => {
      this.setState({
        list: data.data.rows,
        page: data.page.currentPage,
        maxPage: data.page.maxPage,
      });
    }).catch(() => {
      this.setState({ list: [] });
    });
  };

  onClickAddCoupon = () => {
    return this.setState({ addDialog: true });
  };

  renderContent = () => {
    if (this.state.pending) {
      return this.renderPendingUI();
    }

    return (
      <div>
        {this.renderCouponList()}
        <div>
          <FlatPagination
            offset={this.state.page}
            limit={1}
            total={this.state.maxPage}
            onClick={(e, offset) => this.onClickPage(offset)}
          />
        </div>
      </div>
    )
  };

  renderCouponList = () => {
    return this.state.list.map((item, index) => {
      return (
        <DataRow
          key={index}
          data={item}
          onClick={this.onClickCheckBox}
        />
      );
    });
  };

  renderPendingUI = () => {
    return (
      <div className={s.pending}>
        <CircularProgress size={100} />
      </div>
    );
  };

  render() {
    const { searchOptions } = this.state;
    const filter = [
      { name: '쿠폰번호', type: 'couponId', dataType: FilterDataTypeGroup.UNIQUE_TEXT },
      { name: '회원번호', type: 'userId', dataType: FilterDataTypeGroup.UNIQUE_TEXT },
      { name: '코드번호', type: 'codeId', dataType: FilterDataTypeGroup.UNIQUE_TEXT },
      { name: '만료일자', type: 'expireDate', dataType: FilterDataTypeGroup.DATE_RANGE },
      { name: '발급일자', type: 'pubDate', dataType: FilterDataTypeGroup.DATE_RANGE },
      { name: '사용일자', type: 'userDate', dataType: FilterDataTypeGroup.DATE_RANGE },
    ];

    return (
      <div>
        <SearchBar
          filter={filter}
          searchOptions={searchOptions}
          onClickSearch={this.onClickSearch}
          onClickAddSearchOption={this.onAddSearchOption}
          onClickDeleteSearchOption={this.onDeleteSearchOption}
          onChangeSearchOption={this.onChangeSearchOption}
          onExportCSV={this.onClickExportCSV}
        />
        <div>
          <DataHeaderRow onClickColumn={this.onChangeOrderColumn} />
        </div>
        {this.renderContent()}
      </div>
    );
  }
}

Coupons.propTypes = {};
Coupons.defaultProps = {};

export default withStyles(s)(Coupons);
