import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './TableRow.css';
import RaisedButton from 'material-ui/RaisedButton';
import SimpleDateFormat from '../../../../util/dataFormat/SimpleDateFormat';
import EnhancedButton from 'material-ui/internal/EnhancedButton';
import MemoStatusGroup from '../../../../util/const/MemoStatusGroup';
import MemoClassGroup from '../../../../util/const/MemoClassGroup';
import MemoReqMethodGroup from '../../../../util/const/MemoReqMethodGroup';
import MemoTypeGroup from '../../../../util/const/MemoTypeGroup';
import getUserRoleText from '../../../../util/user/getUserRoleText';
import getUserNameText from '../../../../util/user/getUserNameText';

const buttonStyle = {
  zIndex: '0',
};

class TableRow extends React.Component {
  getMemoTypeText = (memoType) => {
    let text = '없음';
    if (memoType) {
      text = memoType;
    }
    return text;
  };

  getMemoClassText = (memoType, memoClass) => {
    let text = '없음';
    if (memoType && memoType === MemoTypeGroup.ADMIN) {
      if (memoClass) {
        if (memoClass === MemoClassGroup.INFO_ERROR) {
          text = '정보오류';
        }
        if (memoClass === MemoClassGroup.PROFILE_ERROR) {
          text = '사진오류';
        }
        if (memoClass === MemoClassGroup.OTHER) {
          text = '기타';
        }
        if (memoClass === MemoClassGroup.NOT_IN_USE) {
          text = '미사용';
        }
      }
    }

    return text;
  };

  getMemoStatusText = (memoStatus) => {
    let text = '';
    if (memoStatus) {
      if (memoStatus === MemoStatusGroup.DONE) {
        text = '완료';
      }
      if (memoStatus === MemoStatusGroup.NEW) {
        text = '신규';
      }
    }
    return text;
  };

  getAdminInfoText = (memoAdmin) => {
    let text = '정보없음';
    if (memoAdmin) {
      text = memoAdmin.adminName;
    }
    return text;
  };

  getMemoReqMethod = (memoType, memoReqMethod) => {
    let text = '정보없음';
    if (memoType && memoType !== MemoTypeGroup.ADMIN) {
      if (memoReqMethod) {
        if (memoReqMethod === MemoReqMethodGroup.KAKAO) {
          text = '카카오톡';
        }
        if (memoReqMethod === MemoReqMethodGroup.SMS) {
          text = '문자';
        }
        if (memoReqMethod === MemoReqMethodGroup.MAIL) {
          text = '이메일';
        }
        if (memoReqMethod === MemoReqMethodGroup.CALL) {
          text = '전화';
        }
      }
    }
    return text;
  };

  onClickMemoContent = (memoContent) => {
    if (memoContent) {
      this.props.openNotificationDialog('메모 내용', memoContent);
    } else {
      this.props.openNotificationDialog('메모 내용', '작성한 메모 내용이 없습니다.');
    }
  };

  render() {
    const {
      memoId,
      memoRegDate,
      memoType,
      memoStatus,
      memoClass,
      memoContent,
      memoReqMethod,
      adminId,
      memoUser,
      userId,
      memoAdmin,
    } = this.props.data;
    return (
      <tr className={s.row}>
        <td>{memoId}</td>
        <td>{this.getAdminInfoText(memoAdmin)}</td>
        <td>
          <RaisedButton
            label={userId}
            href={`/manage/user/${userId}/memos`}
            buttonStyle={buttonStyle}
          />
        </td>
        <td>{getUserNameText(memoUser)}</td>
        <td>{getUserRoleText(memoUser)}</td>
        <td>{this.getMemoTypeText(memoType)}</td>
        <td>{this.getMemoClassText(memoType, memoClass)}</td>
        <td>{this.getMemoReqMethod(memoType, memoReqMethod)}</td>
        <td>
          <EnhancedButton
            onClick={() => this.onClickMemoContent(memoContent)}
            style={{ width: '100%', ...buttonStyle }}
          >
            <p className={s.memoContent}>{memoContent}</p>
          </EnhancedButton>
        </td>
        <td>{this.getMemoStatusText(memoStatus)}</td>
        <td>{SimpleDateFormat(memoRegDate)}</td>
      </tr>
    );
  }
}

TableRow.propTypes = {
  data: PropTypes.object.isRequired,
  openNotificationDialog: PropTypes.func.isRequired,
};

TableRow.defaultProps = {};

export default withStyles(s)(TableRow);
