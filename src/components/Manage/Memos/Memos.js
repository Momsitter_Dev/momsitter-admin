import qs from 'qs';
import _ from 'lodash';
import uuid from 'uuid/v4';
import classnames from 'classnames';
import FlatPagination from 'material-ui-flat-pagination';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Memos.css';
import SearchBar from '../../SearchBar';
import TableHeaderRow from '../../TableHeaderRow';
import TableRow from './TableRow';
import Request from '../../../util/Request';
import AddMemoDialog from './AddMemoDialog';
import ResponseResultTypeGroup from '../../../util/const/ResponseResultTypeGroup';
import MemoTypeGroup from '../../../util/const/MemoTypeGroup';
import MemoStatusGroup from '../../../util/const/MemoStatusGroup';
import MemoClassGroup from '../../../util/const/MemoClassGroup';
import FilterDataTypeGroup from '../../../util/const/FilterDataTypeGroup';
import NotificationDialog from '../../Dialog/NotificationDialog';

class Memos extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      page: 0,
      maxPage: 0,
      notificationDialog: false,
      notificationDialogTitle: '',
      notificationDialogMessage: '',
      addMemoDialog: false,
      addMemoDialogData: {
        userId: '',
        memoResId: null,
        memoType: MemoTypeGroup.QUESTION,
        memoClass: MemoClassGroup.INFO_ERROR,
        memoStatus: MemoStatusGroup.NEW,
        memoContent: '',
        memoReqMethod: 'call',
      },
      searchOptions: [],
      orderOption: {
        column: '',
        method: '',
      },
    };

    this.onClickSearch = this.onClickSearch.bind(this);
    this.onChangeOrderColumn = this.onChangeOrderColumn.bind(this);
    this.onSaveAddMemo = this.onSaveAddMemo.bind(this);
  }

  async componentDidMount() {
    await this.doSearch({}).then(data => {
      this.setState({
        list: data.data.rows,
        page: data.page.currentPage,
        maxPage: data.page.maxPage,
      });
    }).catch(() => {
      this.setState({ list: [] });
    });
  }

  doSearch = options =>
    new Promise(async (resolve, reject) => {
      const query = qs.stringify(options);
      this.cachedOption = Object.assign({}, options);

      const searchResult = await Request(
        `api/manage/user/memos?${query}`,
        { method: 'GET' },
      ).catch(() => false);

      if (searchResult && searchResult.result === 'success') {
        resolve(searchResult);
      } else {
        reject();
      }
    });

  onClickSearch = async function () {
    const { searchOptions, orderOption } = this.state;
    await this.doSearch({
      searchOptions,
      orderOption,
      requestPage: 0,
    }).then(data => {
      this.setState({
        list: data.data.rows,
        page: data.page.currentPage,
        maxPage: data.page.maxPage,
      });
    }).catch(() => {
      this.setState({ list: [] });
    });
  };

  onClickPage = value => {
    this.cachedOption.requestPage = value;
    this.doSearch(this.cachedOption).then(data => {
      this.setState({
        list: data.data.rows,
        page: data.page.currentPage,
        maxPage: data.page.maxPage,
      });
    }).catch(() => {
      this.setState({ list: [] });
    });
  };

  onChangeOrderColumn = async function (columnId) {
    const { orderOption, searchOptions } = this.state;
    if (orderOption.column !== columnId) {
      orderOption.method = '';
    }
    orderOption.column = columnId;
    if (orderOption.method === 'DESC') {
      orderOption.method = 'ASC';
    } else if (orderOption.method === 'ASC') {
      orderOption.method = '';
    } else {
      orderOption.method = 'DESC'
    }
    this.setState({ orderOption });

    await this.doSearch({
      searchOptions,
      orderOption,
      requestPage: 0,
    }).then(data => {
      this.setState({
        list: data.data.rows,
        page: data.page.currentPage,
        maxPage: data.page.maxPage,
      });
    }).catch(() => {
      this.setState({ list: [] });
    });
  };

  onClickAddMemo = () => {
    this.setState({
      addMemoDialog: true,
    });
  };

  onChangeAddMemoData = (type, value) => {
    const { addMemoDialogData } = this.state;
    let newMemoData = Object.assign({}, addMemoDialogData);
    newMemoData[type] = value;
    this.setState({
      addMemoDialogData: newMemoData,
    });
  };

  checkAddMemoDataValid = (data) => {
    let result = true;
    if (data) {
      for (let key in data) {
        if (key === 'userId' && !data[key]) {
          this.openNotificationDialog('알림', '회원번호를 입력해 주세요.');
          return false;
        }
        if (key === 'memoType' && !data[key]) {
          this.openNotificationDialog('알림', '메모 타입을 입력해 주세요.');
          return false;
        }
        if (key === 'memoClass' && !data[key]) {
          this.openNotificationDialog('알림', '메모 구분을 입력해 주세요.');
          return false;
        }
        if (key === 'memoContent' && !data[key]) {
          this.openNotificationDialog('알림', '메모 내용을 입력해 주세요.');
          return false;
        }
        if (key === 'memoReqMethod' && !data[key]) {
          this.openNotificationDialog('알림', '메모 요청방법을 입력해 주세요.');
          return false;
        }
        if (key === 'memoStatus' && !data[key]) {
          this.openNotificationDialog('알림', '메모 상태를 입력해 주세요.');
          return false;
        }
      }
    } else {
      result = false;
      this.openNotificationDialog('오류', '정보가 누락되어 진행할 수 없습니다.');
    }
    return result;
  };

  onSaveAddMemo = async function () {
    const { addMemoDialogData, list } = this.state;
    if (this.checkAddMemoDataValid(addMemoDialogData)) {
      const response = await Request(`api/manage/user/${addMemoDialogData.userId}/memos`, {
        method: 'POST',
        data: {
          ...addMemoDialogData,
        }
      });
      if (response && response.result === ResponseResultTypeGroup.SUCCESS) {
        this.onCloseAddMemoDialog();
        this.openNotificationDialog('알림', '저장되었습니다.');
        let newUserMemo = Object.assign([], list);
        newUserMemo = [response.data, ...newUserMemo];
        this.setState({
          list: newUserMemo,
        });
      } else {
        this.openNotificationDialog('오류', '네트워크 오류입니다.');
      }
    }
  };

  onCloseAddMemoDialog = () => {
    this.setState({
      addMemoDialog: false,
      addMemoDialogData: {
        userId: '',
        memoResId: null,
        memoType: MemoTypeGroup.QUESTION,
        memoClass: MemoClassGroup.INFO_ERROR,
        memoStatus: MemoStatusGroup.NEW,
        memoContent: '',
        memoReqMethod: 'call',
      },
    });
  };

  openNotificationDialog = (title, message) => {
    this.setState({
      notificationDialog: true,
      notificationDialogTitle: title || '알림',
      notificationDialogMessage: message,
    });
  };

  onClickCloseNotificationDialog = () => {
    this.setState({
      notificationDialog: false,
    });
  };

  onAddSearchOption = () => {
    const { searchOptions } = this.state;
    searchOptions.push({
      generatedKey: uuid(),
      filterType: '',
      filterValue: '',
      filterDataType: '',
    });
    this.setState({ searchOptions });
  };

  onDeleteSearchOption = (generatedKey) => {
    const { searchOptions } = this.state;
    _.remove(searchOptions, (o) => {
      return o.generatedKey === generatedKey;
    });
    this.setState({ searchOptions });
  };

  onChangeSearchOption = (generatedKey, type, value, dataType) => {
    const { searchOptions } = this.state;
    const targetIndex = _.findIndex(searchOptions, (o) => {
      return o.generatedKey === generatedKey;
    });
    if (targetIndex !== -1) {
      searchOptions[targetIndex] = {
        ...searchOptions[targetIndex],
        filterType: type,
        filterValue: value,
        filterDataType: dataType,
      };
      this.setState({ searchOptions });
    } else {
      console.log('DEBUG : ON CHANGE BUT NOT FOUND : ' + generatedKey);
    }
  };

  renderMemoList = (list) => {
    let ui = null;
    if (list && list.length > 0) {
      ui = list.map((item) => {
        return (
          <TableRow
            key={uuid()}
            data={item}
            openNotificationDialog={this.openNotificationDialog}
          />
        );
      });
    }
    return ui;
  };

  render() {
    const {
      list,
      page,
      maxPage,
      notificationDialog,
      notificationDialogTitle,
      notificationDialogMessage,
      addMemoDialog,
      addMemoDialogData,
      orderOption,
      searchOptions,
    } = this.state;
    const headers = [
      { id: 'memoId', desc: '메모번호', sortable: true },
      { id: 'adminName', desc: '관리자 이름', sortable: false },
      { id: 'userId', desc: '회원 번호', sortable: true },
      { id: 'userName', desc: '회원 이름', sortable: false },
      { id: 'userType', desc: '회원 타입', sortable: false },
      { id: 'memoType', desc: '메모 타입', sortable: true },
      { id: 'memoClass', desc: '메모 구분', sortable: true },
      { id: 'memoReqMethod', desc: '메모요청방법', sortable: true },
      { id: 'memoContent', desc: '메모 내용', sortable: true },
      { id: 'memoStatus', desc: '메모 상태', sortable: true },
      { id: 'memoRegDate', desc: '생성일자', sortable: true },
    ];
    const filter = [
      { name: '메모번호', type: 'memoId', dataType: FilterDataTypeGroup.UNIQUE_TEXT },
      { name: '관리자 번호', type: 'adminId', dataType: FilterDataTypeGroup.UNIQUE_TEXT },
      { name: '회원번호', type: 'userId', dataType: FilterDataTypeGroup.UNIQUE_TEXT },
      { name: '메모 타입', type: 'memoType', dataType: FilterDataTypeGroup.MEMO_TYPE },
      { name: '메모 상태', type: 'memoStatus', dataType: FilterDataTypeGroup.MEMO_STATUS },
      { name: '메모 종류', type: 'memoClass', dataType: FilterDataTypeGroup.MEMO_CLASS },
      { name: '메모 요청 방법', type: 'memoReqMethod', dataType: FilterDataTypeGroup.MEMO_REQ_METHOD },
      { name: '메모 내용', type: 'memoContent', dataType: FilterDataTypeGroup.TEXT },
      { name: '생성 일자', type: 'memoRegDate', dataType: FilterDataTypeGroup.DATE_RANGE },
    ];

    return (
      <div className={classnames(s.root, ' box box-default')}>
        <SearchBar
          filter={filter}
          searchOptions={searchOptions}
          onClickSearch={this.onClickSearch}
          openNotificationDialog={this.openNotificationDialog}
          onClickAddSearchOption={this.onAddSearchOption}
          onClickDeleteSearchOption={this.onDeleteSearchOption}
          onChangeSearchOption={this.onChangeSearchOption}
        />
        <div className="box-body padding-sm">
          <table className={'table'}>
            <thead>
              <TableHeaderRow
                data={headers}
                orderOption={orderOption}
                onChangeOrderColumn={this.onChangeOrderColumn}
                openNotificationDialog={this.openNotificationDialog}
              />
            </thead>
            <tbody>{this.renderMemoList(list)}</tbody>
          </table>
          <div>
            <FlatPagination
              offset={page}
              limit={1}
              total={maxPage}
              onClick={(e, offset) => this.onClickPage(offset)}
            />
          </div>
        </div>
        <AddMemoDialog
          title="메모 추가"
          open={addMemoDialog}
          data={addMemoDialogData}
          onCreateMemo={this.onSaveAddMemo}
          onChangeNewMemo={this.onChangeAddMemoData}
          closeDialog={this.onCloseAddMemoDialog}
        />
        <NotificationDialog
          open={notificationDialog}
          title={notificationDialogTitle}
          message={notificationDialogMessage}
          onClose={this.onClickCloseNotificationDialog}
        />
      </div>
    );
  }
}

Memos.propTypes = {};
Memos.defaultProps = {};

export default withStyles(s)(Memos);
