import RaisedButton from 'material-ui/RaisedButton';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './TableRow.css';
import SimpleDateFormat from '../../../../util/dataFormat/SimpleDateFormat';
import UserOutRequestStatusTypeGroup from '../../../../util/const/UserOutRequestStatusTypeGroup';
import getUserNameText from '../../../../util/user/getUserNameText';
import getUserRoleText from '../../../../util/user/getUserRoleText';

const buttonStyle = {
  zIndex: '0',
};

const propTypes = {
  data: PropTypes.object.isRequired,
  openNotificationDialog: PropTypes.func.isRequired,
  onClickCancelOut: PropTypes.func,
  onClickDelete: PropTypes.func,
};
const defaultProps = {
  onClickCancelOut: () => {},
  onClickDelete: () => {},
};

class TableRow extends React.Component {
  onClickUserId = (userId) => {
    window.open('/users?userId=' + userId);
  };

  onClickDelete = () => {
    if (!this.props.data.userOutRequestId) {
      return false;
    }
    if (!this.props.data.status) {
      return false;
    }
    if (this.props.data.status === UserOutRequestStatusTypeGroup.REQUEST) {
      return this.props.onClickDelete(this.props.data.userOutRequestId);
    }
    return false;
  };

  onChangeUserOutRequestStatus = (event, index, value) => {
    if (this.props.data.status !== value) {
      this.props.onChangeUserOutRequestStatus(
        this.props.data.userId, value
      );
    }
  };

  onClickCancelOut = () => {
    if (this.props.data.userId) {
      return this.props.onClickCancelOut(this.props.data.userId);
    }
    return null;
  };

  renderDeleteButton = () => {
    if (this.props.data.status === UserOutRequestStatusTypeGroup.REQUEST) {
      return (
        <RaisedButton label='삭제' secondary onClick={this.onClickDelete} />
      );
    }
    return null;
  };

  renderCancelButton = () => {
    return null;
    /*
    if (this.props.data.status !== UserOutRequestStatusTypeGroup.REQUEST) {
      return (
        <RaisedButton label='탈퇴 철회' onClick={this.onClickCancelOut} />
      );
    }
    return null;
    */
  };

  render() {
    const { data } = this.props;
    const {
      userOutRequestId,
      userId,
      outRequestUser,
      doneDate,
      regDate,
      status,
      outReason,
    } = data;
    return (
      <tr className={s.row}>
        <td> {userOutRequestId} </td>
        <td>
          <RaisedButton
            label={userId}
            onClick={() => this.onClickUserId(userId)}
            buttonStyle={buttonStyle}
          />
        </td>
        <td> {getUserNameText(outRequestUser)} </td>
        <td> {getUserRoleText(outRequestUser)} </td>
        <td>
          <div style={{ width: '100px', overflow: 'hidden' }}> {outReason} </div>
        </td>
        <td>
          <SelectField
            value={status}
            onChange={this.onChangeUserOutRequestStatus}
            style={{ width: '100px' }}
          >
            <MenuItem primaryText="요청" value={UserOutRequestStatusTypeGroup.REQUEST} />
            <MenuItem primaryText="보류" value={UserOutRequestStatusTypeGroup.HOLD} />
            <MenuItem primaryText="본인탈퇴처리" value={UserOutRequestStatusTypeGroup.CONFIRMED} />
            <MenuItem primaryText="블랙리스트" value={UserOutRequestStatusTypeGroup.BLACK_LIST} />
          </SelectField>
        </td>
        <td> {SimpleDateFormat(regDate)} </td>
        <td> {SimpleDateFormat(doneDate)} </td>
        <td>
          {this.renderCancelButton()}
          {this.renderDeleteButton()}
        </td>
      </tr>
    );
  }
}

TableRow.propTypes = propTypes;
TableRow.defaultProps = defaultProps;

export default withStyles(s)(TableRow);
