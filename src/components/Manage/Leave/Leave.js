/* eslint-disable prettier/prettier */
import _ from 'lodash';
import qs from 'qs';
import uuid from 'uuid/v4';
import classnames from 'classnames';
import FlatPagination from 'material-ui-flat-pagination';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Leave.css';
import TableHeaderRow from '../../TableHeaderRow';
import TableRow from './TableRow';
import Request from '../../../util/Request';
import SearchBar from '../../SearchBar';
import ResponseResultTypeGroup from '../../../util/const/ResponseResultTypeGroup';
import FilterDataTypeGroup from '../../../util/const/FilterDataTypeGroup';
import NotificationDialog from '../../Dialog/NotificationDialog';

const propTypes = {};
const defaultProps = {};

class Leave extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      page: 0,
      maxPage: 0,
      dialogOpen: false,
      notificationDialog: false,
      notificationDialogTitle: '',
      notificationDialogMessage: '',
      searchOptions: [],
      orderOption: {
        column: '',
        method: '',
      },
    };
    this.cachedOption = {};
    this.onClickSearch = this.onClickSearch.bind(this);
    this.onChangeUserOutRequestStatus = this.onChangeUserOutRequestStatus.bind(this);
    this.onChangeOrderColumn = this.onChangeOrderColumn.bind(this);
    this.onClickCancelOut = this.onClickCancelOut.bind(this);
    this.onClickDelete = this.onClickDelete.bind(this);
  }

  async componentDidMount() {
    await this.doSearch({}).then(data => {
      this.setState({
        list: data.data.rows,
        page: data.page.currentPage,
        maxPage: data.page.maxPage,
      });
    }).catch(() => {
      this.setState({ list: [] });
    });
  }

  doSearch = (options) =>
    new Promise(async (resolve, reject) => {
      const query = qs.stringify(options);
      this.cachedOption = Object.assign({}, options);

      const searchResult = await Request(
        `api/manage/user/outRequest?${query}`,
        { method: 'GET' },
      ).catch(() => false);
      if (searchResult && searchResult.result === ResponseResultTypeGroup.SUCCESS) {
        resolve(searchResult);
      } else {
        reject();
      }
    });

  doRefresh = async function (options) {
    const searchResult = await this.doSearch(options).catch((err) => {
      console.log(err);
      return false;
    });

    if (!searchResult) {
      return false;
    }

    return this.setState({
      list: searchResult.data.rows,
      page: searchResult.page.currentPage,
      maxPage: searchResult.page.maxPage,
    });
  };

  onClickSearch = async function () {
    const { searchOptions, orderOption } = this.state;
    await this.doSearch({
      searchOptions,
      orderOption,
      requestPage: 0,
    }).then(data => {
      this.setState({
        list: data.data.rows,
        page: data.page.currentPage,
        maxPage: data.page.maxPage,
      });
    }).catch((err) => {
      console.log(err);
      this.setState({ list: [] });
    });
  };

  onClickExportCSV = () => {
    const queryString = qs.stringify(this.cachedOption);
    window.open(`/api/manage/user/outRequest/csv?${queryString}`);
  };

  onClickPage = value => {
    this.cachedOption.requestPage = value;
    this.doSearch(this.cachedOption).then(data => {
      this.setState({
        list: data.data.rows,
        page: data.page.currentPage,
        maxPage: data.page.maxPage,
      });
    }).catch((err) => {
      console.log(err);
      this.setState({ list: [] });
    });
  };

  onChangeOrderColumn = async function (columnId) {
    const { orderOption, searchOptions } = this.state;
    if (orderOption.column !== columnId) {
      orderOption.method = '';
    }
    orderOption.column = columnId;
    if (orderOption.method === 'DESC') {
      orderOption.method = 'ASC';
    } else if (orderOption.method === 'ASC') {
      orderOption.method = '';
    } else {
      orderOption.method = 'DESC'
    }
    this.setState({ orderOption });

    await this.doSearch({
      searchOptions,
      orderOption,
      requestPage: 0,
    }).then(data => {
      this.setState({
        list: data.data.rows,
        page: data.page.currentPage,
        maxPage: data.page.maxPage,
      });
    }).catch((err) => {
      console.log(err);
      this.setState({ list: [] });
    });
  };

  onChangeUserOutRequestStatus = async function (userId, status) {
    const response = await Request(
      `api/manage/user/outRequest/${userId}/status`,
      {
        method: 'PUT',
        data: {
          status
        },
      },
    ).catch((err) => {
      console.log(err);
      return false;
    });

    if (!response || response.result === ResponseResultTypeGroup.FAIL) {
      return this.openNotificationDialog('오류', '처리상태를 변경할 수 없습니다.');
    }

    if (response.result === ResponseResultTypeGroup.SUCCESS) {
      await this.doRefresh(this.cachedOption);
      return this.openNotificationDialog('알림', '처리상태가 변경되었습니다.');
    }

    return this.openNotificationDialog('오류', '처리상태를 변경할 수 없습니다.');
  };

  onCloseDialog = () => {
    this.setState({ dialogOpen: false });
  };

  openNotificationDialog = (title, message) => {
    this.setState({
      notificationDialog: true,
      notificationDialogTitle: title || '알림',
      notificationDialogMessage: message,
    });
  };

  onClickCloseNotificationDialog = () => {
    this.setState({
      notificationDialog: false,
    });
  };

  onAddSearchOption = () => {
    const { searchOptions } = this.state;
    searchOptions.push({
      generatedKey: uuid(),
      filterType: '',
      filterValue: '',
      filterDataType: '',
    });
    this.setState({ searchOptions });
  };

  onDeleteSearchOption = (generatedKey) => {
    const { searchOptions } = this.state;
    _.remove(searchOptions, (o) => {
      return o.generatedKey === generatedKey;
    });
    this.setState({ searchOptions });
  };

  onChangeSearchOption = (generatedKey, type, value, dataType) => {
    const { searchOptions } = this.state;
    const targetIndex = _.findIndex(searchOptions, (o) => {
      return o.generatedKey === generatedKey;
    });
    if (targetIndex !== -1) {
      searchOptions[targetIndex] = {
        ...searchOptions[targetIndex],
        filterType: type,
        filterValue: value,
        filterDataType: dataType,
      };
      this.setState({ searchOptions });
    } else {
      console.log('DEBUG : ON CHANGE BUT NOT FOUND : ' + generatedKey);
    }
  };

  onClickCancelOut = async function (userId) {
    return this.openNotificationDialog('알림', '정책미정');
    /*
    if (!userId) {
      return this.openNotificationDialog('오류', '회원정보를 찾을 수 없습니다.');
    }
    const result = await Request(`api/manage/user/outRequest/${userId}/cancel`, {
      method: 'PUT',
    }).catch((err) => {
      console.log(err);
      return new Error(err);
    });

    if (result instanceof Error) {
      return this.openNotificationDialog('오류', '네트워크 오류입니다.');
    }

    if (result.result === ResponseResultTypeGroup.SUCCESS) {
      return this.openNotificationDialog('알림', '삭제되었습니다.');
    }

    return this.openNotificationDialog('오류', '알수없는 오류입니다.');
    */
  };

  onClickDelete = async function (id) {
    if (!id) {
      return this.openNotificationDialog('오류', '데이터를 찾을 수 없습니다.');
    }

    const result = await Request(`api/manage/user/outRequest/${id}/delete`, {
      method: 'DELETE',
    }).catch((err) => {
      return new Error(err);
    });

    if (result instanceof Error) {
      return this.openNotificationDialog('오류', '네트워크 오류입니다.');
    }

    if (result.result === ResponseResultTypeGroup.SUCCESS) {
      return this.openNotificationDialog('알림', '삭제되었습니다.');
    }

    return this.openNotificationDialog('오류', '알 수 없는 오류입니다.');
  };

  renderReviewItem = () => {
    const { list } = this.state;
    let ui = null;
    if (list && list instanceof Array) {
      ui = list.map((item, index) => {
        return (
          <TableRow
            key={index}
            data={item}
            onClickDelete={this.onClickDelete}
            onClickCancelOut={this.onClickCancelOut}
            openNotificationDialog={this.openNotificationDialog}
            onChangeUserOutRequestStatus={this.onChangeUserOutRequestStatus}
          />
        );
      });
    }
    return ui;
  };

  render() {
    const {
      notificationDialog,
      notificationDialogTitle,
      notificationDialogMessage,
      orderOption,
      searchOptions,
      page,
      maxPage,
    } = this.state;
    const headers = [
      { id: 'userOutRequestId', desc: '탈퇴 요청번호', sortable: true },
      { id: 'userId', desc: '회원번호', sortable: true },
      { id: 'userName', desc: '회원이름', sortable: false },
      { id: 'userRole', desc: '회원타입', sortable: false },
      { id: 'outReason', desc: '탈퇴사유', sortable: true },
      { id: 'status', desc: '처리상태', sortable: true },
      { id: 'regDate', desc: '요청일자', sortable: true },
      { id: 'doneDate', desc: '처리일자', sortable: true },
      { id: 'action', desc: ' ', sortable: false },
    ];
    const filter = [
      { name: '탈퇴 요청번호', type: 'userOutRequestId', dataType: FilterDataTypeGroup.UNIQUE_TEXT },
      { name: '회원번호', type: 'userId', dataType: FilterDataTypeGroup.UNIQUE_TEXT },
      { name: '회원이름', type: 'userName', dataType: FilterDataTypeGroup.UNIQUE_TEXT },
      { name: '탈퇴사유', type: 'outReason', dataType: FilterDataTypeGroup.TEXT },
      { name: '처리상태', type: 'status', dataType: FilterDataTypeGroup.USER_OUT_REQUEST_STATUS },
      { name: '요청일자', type: 'regDate', dataType: FilterDataTypeGroup.DATE_RANGE },
      { name: '처리일자', type: 'doneDate', dataType: FilterDataTypeGroup.DATE_RANGE },
    ];

    return (
      <div className={classnames(s.root, ' box box-default')}>
        <SearchBar
          filter={filter}
          searchOptions={searchOptions}
          onClickSearch={this.onClickSearch}
          openNotificationDialog={this.openNotificationDialog}
          onClickAddSearchOption={this.onAddSearchOption}
          onClickDeleteSearchOption={this.onDeleteSearchOption}
          onChangeSearchOption={this.onChangeSearchOption}
          onExportCSV={this.onClickExportCSV}
        />
        <div className="box-body padding-sm">
          <table className={classnames('table', s.table)} style={{ tableLayout: 'fixed' }}>
            <thead>
            <TableHeaderRow
              data={headers}
              orderOption={orderOption}
              onChangeOrderColumn={this.onChangeOrderColumn}
              openNotificationDialog={this.openNotificationDialog}
            />
            </thead>
            <tbody>{this.renderReviewItem()}</tbody>
          </table>
          <div>
            <FlatPagination
              offset={page}
              limit={1}
              total={maxPage}
              onClick={(e, offset) => this.onClickPage(offset)}
            />
          </div>
        </div>

        <NotificationDialog
          open={notificationDialog}
          title={notificationDialogTitle}
          message={notificationDialogMessage}
          onClose={this.onClickCloseNotificationDialog}
        />
      </div>
    );
  }
}

Leave.propTypes = propTypes;
Leave.defaultProps = defaultProps;

export default withStyles(s)(Leave);
