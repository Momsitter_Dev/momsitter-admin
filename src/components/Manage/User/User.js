import qs from 'qs';
import uuid from 'uuid/v4';
import FlatPagination from 'material-ui-flat-pagination';
import classnames from 'classnames';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './User.css';
import TableHeaderRow from './TableHeaderRow';
import TableRow from './TableRow';
import SearchBar from './SearchBar';
import Request from '../../../util/Request';
import ProgressCard from '../../ProgressCard';
import ResponseResultTypeGroup from '../../../util/const/ResponseResultTypeGroup';
import NotificationDialog from '../../Dialog/NotificationDialog';

class User extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      page: null,
      maxPage: null,
      notificationDialog: false,
      notificationDialogTitle: '',
      notificationDialogMessage: '',
    };
    this.cachedOption = {
      searchFilter: 'userId',
      searchText: '',
      searchOrderColumn: '',
      searchOrderType: '',
    };
    this.searchOptions = {
      requestPage: 0,
      searchFilter: 'userId',
      searchText: '',
      searchOrderColumn: '',
      searchOrderType: '',
    };
    this.onExportCSV = this.onExportCSV.bind(this);
    this.onClickSearch = this.onClickSearch.bind(this);
    this.onRefresh = this.onRefresh.bind(this);
  }

  async componentDidMount() {
    const queryString = qs.stringify(this.searchOptions);
    await this.doSearch(queryString)
      .then(data => {
        this.setState({
          list: data.data.rows,
          page: data.page.currentPage,
          maxPage: data.page.maxPage,
        });
      })
      .catch(() => {
        this.setState({ list: [] });
      });
  }

  onExportCSV = async function () {
    const queryString = qs.stringify(this.cachedOption);
    window.open(`/api/manage/user/csv?${queryString}`);
  };

  onChangeSearchFilter = (event, index, value) => {
    this.searchOptions.searchFilter = value;
  };

  onChangeSearchText = (value) => {
    this.searchOptions.searchText = value;
  };

  onClickSearch = async function (event, index, value) {
    // 페이지를 1로 초기화
    this.searchOptions.requestPage = 0;
    const queryString = qs.stringify(this.searchOptions);
    await this.doSearch(queryString)
      .then(data => {
        this.setState({
          list: data.data.rows,
          page: data.page.currentPage,
          maxPage: data.page.maxPage,
        });
      })
      .catch(() => {
        this.setState({ list: [] });
      });
  };

  onClickPage = value => {
    this.cachedOption.requestPage = value;
    const queryString = qs.stringify(this.cachedOption);
    this.doSearch(queryString)
      .then(data => {
        this.setState({
          list: data.data.rows,
          page: data.page.currentPage,
          maxPage: data.page.maxPage,
        });
      })
      .catch(() => {
        this.setState({ list: [] });
      });
  };

  doSearch = query =>
    new Promise(async (resolve, reject) => {
      // Cache Update
      this.cachedOption = Object.assign({}, this.searchOptions);

      const searchResult = await Request(
        `api/manage/user?${query}`,
        { method: 'GET' },
      ).catch(() => false);

      if (searchResult && searchResult.result === ResponseResultTypeGroup.SUCCESS) {
        resolve(searchResult);
      } else {
        reject();
      }
    });

  onRefresh = async function () {
    const queryString = qs.stringify(this.cachedOption);
    await this.doRefresh(queryString).then((data) => {
      this.setState({
        list: data.data.rows,
        page: data.page.currentPage,
        maxPage: data.page.maxPage,
      });
    }).catch(() => {
      this.setState({ list: [] });
    });
  };

  doRefresh = query =>
    new Promise(async (resolve, reject) => {
      // Cache Update
      this.cachedOption = Object.assign({}, this.cachedOption);

      const searchResult = await Request(
        `api/manage/user?${query}`,
        { method: 'GET' },
      ).catch(() => false);

      if (searchResult && searchResult.result === 'success') {
        resolve(searchResult);
      } else {
        reject();
      }
    });

  onChangeOrderColumn = data => {
    this.openNotificationDialog('알림', '구현예정인 기능입니다.');
    /*
    this.searchOptions.searchOrderColumn = data.orderColumn;
    this.searchOptions.searchOrderType = data.orderType;
    */
  };

  openNotificationDialog = (title, message) => {
    this.setState({
      notificationDialog: true,
      notificationDialogTitle: title || '알림',
      notificationDialogMessage: message,
    });
  };

  onClickCloseNotificationDialog = () => {
    this.setState({
      notificationDialog: false,
    });
  };


  renderUserList = (list) => {
    let ui = null;
    if (list && list.length > 0) {
      ui = list.map((item) => (
        <TableRow
          data={item}
          key={uuid()}
          openNotificationDialog={this.openNotificationDialog}
          onRefresh={this.onRefresh}
        />
      ));
    }
    return ui;
  };

  render() {
    const {
      list,
      page,
      maxPage,
      notificationDialog,
      notificationDialogTitle,
      notificationDialogMessage
    } = this.state;
    let paginationUI = null;

    const headers = [
      { id: 'userId', desc: '회원 번호' },
      { id: 'userName', desc: '이름' },
      { id: 'userConnectType', desc: '가입 경로' },
      { id: 'userRole', desc: '회원 타입' },
      { id: 'userPhone', desc: '핸드폰 번호' },
      { id: 'userGender', desc: '성별' },
      { id: 'userEmail', desc: '이메일' },
      { id: 'userKey', desc: '로그인 Key' },
      { id: 'userBirthday', desc: '생일' },
      { id: 'userSignUpdate', desc: '가입 일자' },
      { id: 'profileStatus', desc: '프로필 상태' },
      { id: 'userStatus', desc: '회원 상태' },
      { id: 'searchable', desc: '검색 가능 여부' },
      { id: 'action', desc: 'action' },
    ];
    const filter = [
      { name: '회원 번호', value: 'userId', type: 'text' },
      { name: '이름', value: 'userName', type: 'text' },
      { name: '성별', value: 'userGender', type: 'userGender' },
      { name: '이메일', value: 'userEmail', type: 'text' },
      { name: '핸드폰 번호', value: 'userPhone', type: 'text' },
      { name: '회원 상태', value: 'userStatus', type: 'userStatus' }
    ];

    if (typeof page === 'number' || typeof maxPage === 'number') {
      paginationUI = (
        <FlatPagination
          offset={page}
          limit={1}
          total={maxPage}
          onClick={(e, offset) => this.onClickPage(offset)}
        />
      );
    } else {
      paginationUI = (<ProgressCard />);
    }
    return (
      <div className={classnames(s.root, ' box box-default')}>
        <SearchBar
          filter={filter}
          onChangeSearchFilter={this.onChangeSearchFilter}
          onChangeSearchText={this.onChangeSearchText}
          onClickSearch={this.onClickSearch}
          openNotificationDialog={this.openNotificationDialog}
          onExportCSV={this.onExportCSV}
        />
        <div className="box-body padding-sm">
          <table className={classnames("table")}>
            <thead>
              <TableHeaderRow onChangeOrderColumn={this.onChangeOrderColumn} openNotificationDialog={this.openNotificationDialog} />
            </thead>
            <tbody>
              {this.renderUserList(list)}
            </tbody>
          </table>
          <div>
            {paginationUI}
          </div>
        </div>

        <NotificationDialog
          open={notificationDialog}
          title={notificationDialogTitle}
          message={notificationDialogMessage}
          onClose={this.onClickCloseNotificationDialog}
        />
      </div>
    );
  }
}

User.propTypes = {};
User.defaultProps = {};

export default withStyles(s)(User);
