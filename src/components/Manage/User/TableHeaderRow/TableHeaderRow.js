import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './TableHeaderRow.css';
import EnhancedButton from 'material-ui/internal/EnhancedButton';
import ArrowUpIcon from 'material-ui/svg-icons/navigation/arrow-upward';
import ArrowDownIcon from 'material-ui/svg-icons/navigation/arrow-downward';

class TableHeaderRow extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      orderColumn: '',
      orderType: '',
    };
  }

  onChangeOrderColumn(value) {
    const { orderColumn, orderType } = this.state;
    if (orderColumn !== value) {
      this.setState({ orderColumn: value, orderType: 'ASC' });
    } else if (orderType === 'ASC') {
      this.setState({ orderType: 'DESC' });
    } else {
      this.setState({ orderType: 'ASC' });
    }
    this.props.onChangeOrderColumn({
      orderType: this.state.orderType,
      orderColumn: this.state.orderColumn,
    });
  }

  render() {
    const iconStyle = {
      position: 'absolute',
      top: '25%',
      right: '-5px',
      width: '15px',
      height: '15px',
    };
    const menus = [
      { id: 'userId', desc: '회원번호' },
      { id: 'userName', desc: '이름' },
      { id: 'contactWay', desc: '가입방법' },
      { id: 'userRole', desc: '회원종류' },
      { id: 'userPhone', desc: '휴대폰' },
      { id: 'userGender', desc: '성별' },
      { id: 'userEmail', desc: '이메일' },
      { id: 'userKey', desc: '유저키' },
      { id: 'userBirthday', desc: '생년월일' },
      { id: 'userSignUpDate', desc: '가입날짜' },
      { id: 'test', desc: '프로필 수정일' },
      { id: 'userStatus', desc: '회원상태' },
      { id: 'userSearchable', desc: '검색 가능여부' },
      { id: 'userDelete', desc: '사용자 삭제' },
    ];

    const showArrow = id => {
      let result = null;
      if (this.state.orderColumn === id) {
        if (this.state.orderType === 'ASC') {
          result = <ArrowUpIcon style={iconStyle} />;
        }
        if (this.state.orderType === 'DESC') {
          result = <ArrowDownIcon style={iconStyle} />;
        }
      }
      return result;
    };

    return (
      <tr>
        {menus.map((item, index) =>
          <th key={index}>
            <EnhancedButton
              onClick={this.onChangeOrderColumn.bind(this, item.id)}
              style={{ width: '100%' }}
            >
              <div>
                <span>{item.desc}</span>
                <span>{showArrow(item.id)}</span>
              </div>
            </EnhancedButton>
          </th>,
        )}
      </tr>
    );
  }
}

TableHeaderRow.propTypes = {
  onChangeOrderColumn: PropTypes.func,
  openNotificationDialog: PropTypes.func.isRequired,
};

TableHeaderRow.defaultProps = {};

export default withStyles(s)(TableHeaderRow);
