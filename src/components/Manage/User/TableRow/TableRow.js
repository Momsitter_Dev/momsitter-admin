import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './TableRow.css';
import MenuItem from 'material-ui/MenuItem';
import DropDownMenu from 'material-ui/DropDownMenu';
import RaisedButton from 'material-ui/RaisedButton';
import moment from 'moment';
import Request from '../../../../util/Request';
import Chip from 'material-ui/Chip';
import uuid from 'uuid/v4';
import SimpleDateFormat from '../../../../util/dataFormat/SimpleDateFormat';
import UserStatusGroup from '../../../../util/const/UserStatusGroup';
import UserConnectType from '../../../../util/const/UserConnectType';
import isParent from '../../../../util/user/isParent';
import isSitter from '../../../../util/user/isSitter';

class TableRow extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: props.data.userId,
      userStatus: null,
      searchable: null,
    };

    if (!this.state.userStatus && props.data) {
      this.state.userStatus = props.data.userStatus;
    }

    if (!this.state.searchable && props.data && props.data.sitterProfile) {
      this.state.searchable = props.data.sitterProfile.searchable;
    }

    if (!this.state.searchable && props.data && props.data.parentProfile) {
      this.state.searchable = props.data.parentProfile.searchable;
    }
    this.onClickDeleteUser = this.onClickDeleteUser.bind(this);
    this.onChangeUserStatus = this.onChangeUserStatus.bind(this);
    this.onChangeUserSearchable = this.onChangeUserSearchable.bind(this);
  }

  getUserConnectTypeText = (userConnectType) => {
    let text = '없음';
    if (userConnectType) {
      if (userConnectType === UserConnectType.EMAIL) {
        text = '이메일';
      }
      if (userConnectType === UserConnectType.FACEBOOK) {
        text = '페이스북';
      }
      if (userConnectType === UserConnectType.KAKAO) {
        text = '카카오';
      }
      if (userConnectType === UserConnectType.NAVER) {
        text = '네이버';
      }
    }
    return text;
  };

  onClickUserId = (userId) => {
    window.open('/users?userId=' + userId);
  };

  onClickDeleteUser = async function () {
    if (typeof window !== 'undefined') {
      if (window.confirm('정말 삭제하시겠습니까?')) {
        const { userId } = this.props.data;
        const response = await Request(`api/manage/user/${userId}`, { method: 'DELETE' });
        if (response && response.result === 'success') {
          this.props.openNotificationDialog('알림', '회원정보가 제거되었습니다.');
        } else {
          this.props.openNotificationDialog('오류', '회원정보 제거에 실패하였습니다.');
        }
      }
    } else {
      this.props.openNotificationDialog('오류', '비정상 접근입니다.');
    }
  };

  onChangeUserStatus = async function (event, data, value) {
    const { userStatus, userId } = this.state;
    if (userStatus !== value) {
      const response = await Request(
        `api/manage/user/${userId}/status`,
        {
          method: 'PUT',
          data: { userStatus: value },
        },
      ).catch(() => false);
      if (response && response.result === 'success') {
        this.setState({ userStatus: value });
        this.props.openNotificationDialog('알림', '반영되었습니다.');
        this.props.onRefresh();
      } else {
        this.props.openNotificationDialog('오류', '오류로 인해 "회원상태" 변경에 실패하였습니다.');
      }
    }
  };

  onChangeUserSearchable = async function (event, data, value) {
    const { searchable, userId } = this.state;
    if (searchable !== value) {
      const response = await Request(
        `api/manage/user/${userId}/searchable`,
        {
          method: 'PUT',
          data: { userSearchable: value },
        },
      ).catch(() => false);
      if (response && response.result === 'success') {
        this.setState({ searchable: value });
        this.props.openNotificationDialog('알림', '반영되었습니다.');
        this.props.onRefresh();
      } else {
        this.props.openNotificationDialog('오류', '오류로 인해 "검색가능여부" 변경에 실패하였습니다.');
      }
    }
  };

  render() {
    const { userStatus, searchable } = this.state;
    const {
      userId,
      userName,
      userConnectType,
      userRole,
      userPhone,
      userGender,
      userEmail,
      userSnsKey,
      userBirthday,
      userSignUpDate,
      parentProfile,
      sitterProfile,
    } = this.props.data;
    const userRoleUI = userRole.map((item) => {
      const { userType } = item;
      let roleLabel = '없음';
      if (isParent(item.userTypeId)) {
        if (userType && userType.userDetailTypeDesc) {
          roleLabel = userType.userDetailTypeDesc;
        } else {
          roleLabel = '부모';
        }
      }
      if (isSitter(item.userTypeId)) {
        if (userType && userType.userDetailTypeDesc) {
          roleLabel = userType.userDetailTypeDesc;
        } else {
          roleLabel = '시터';
        }
      }
      return (<Chip key={uuid()}>{roleLabel}</Chip>);
    });
    let profileUpdateDate = '프로필 없음';
    if (parentProfile) {
      profileUpdateDate = SimpleDateFormat(parentProfile.updateDate);
    }

    if (sitterProfile) {
      profileUpdateDate = SimpleDateFormat(sitterProfile.updateDate);
    }

    return (
      <tr className={s.row}>
        <td>
          <RaisedButton
            label={userId}
            onClick={() => this.onClickUserId(userId)}
          />
        </td>
        <td>{userName}</td>
        <td>{this.getUserConnectTypeText(userConnectType)}</td>
        <td>{userRoleUI}</td>
        <td>{userPhone}</td>
        <td>{userGender === 'women' ? '여자' : '남자'}</td>
        <td>{userEmail}</td>
        <td>{userSnsKey}</td>
        <td>{moment(userBirthday).format('YYMMDD')} / 만 {moment().diff(moment(userBirthday), 'years')}세</td>
        <td>{SimpleDateFormat(userSignUpDate)}</td>
        <td>{profileUpdateDate} </td>
        <td>
          <DropDownMenu
            value={userStatus}
            onChange={this.onChangeUserStatus}
            style={{ padding: '0' }}
          >
            <MenuItem value={UserStatusGroup.ACTIVE} primaryText="활성" />
            <MenuItem value={UserStatusGroup.INACTIVE} primaryText="비활성" />
            <MenuItem value={UserStatusGroup.OUT} primaryText="탈퇴" />
          </DropDownMenu>
        </td>
        <td>
          <DropDownMenu
            value={searchable}
            onChange={this.onChangeUserSearchable}
          >
            <MenuItem value={true} primaryText="검색가능" />
            <MenuItem value={false} primaryText="검색불가" />
          </DropDownMenu>
        </td>
        <td>
          <RaisedButton label="삭제" onClick={this.onClickDeleteUser} />
        </td>
      </tr>
    );
  }
}

TableRow.propTypes = {
  data: PropTypes.object.isRequired,
  openNotificationDialog: PropTypes.func.isRequired,
  onRefresh: PropTypes.func.isRequired,
};

TableRow.defaultProps = {
};

export default withStyles(s)(TableRow);
