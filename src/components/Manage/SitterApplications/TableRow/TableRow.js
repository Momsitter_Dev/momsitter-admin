import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './TableRow.css';
import SimpleDateFormat from '../../../../util/dataFormat/SimpleDateFormat';
import ApplyStatusGroup from '../../../../util/const/ApplyStatusGroup';
import RaisedButton from 'material-ui/RaisedButton';
import ApplyTypeGroup from '../../../../util/const/ApplyTypeGroup';
import ApplyDeclineTypeTextGroup from '../../../../util/const/ApplyDeclineTypeTextGroup';
import getUserRoleText from '../../../../util/user/getUserRoleText';
import getUserNameText from '../../../../util/user/getUserNameText';

class TableRow extends React.Component {
  getReadDate = (readDate) => {
    let readDateStr = '확인 일자 없음';
    if (readDate) {
      readDateStr = SimpleDateFormat(readDate);
    }
    return readDateStr;
  };

  getResponseDate = (responseDate) => {
    let responseDateStr = '응답 일자 없음';
    if (responseDate) {
      responseDateStr = SimpleDateFormat(responseDate);
    }
    return responseDateStr;
  };

  onClickUserId = (userId) => {
    window.open('/users?userId=' + userId);
  };

  onClickApplyStatus = (data) => {
    const {
      applyStatus,
      declineType,
      declineReason,
      tuningReason,
    } = data;

    if (applyStatus && applyStatus === ApplyStatusGroup.TUNING) {
      if (tuningReason) {
        this.props.openNotificationDialog('조율 이유', tuningReason);
      } else {
        this.props.openNotificationDialog('조율 이유', '회원이 입력한 값이 없습니다.');
      }
    }
    if (applyStatus === ApplyStatusGroup.DECLINE) {
      if (declineType) {
        const declineTypeText = ApplyDeclineTypeTextGroup[declineType];
        this.props.openNotificationDialog('거절 사유', (
          <div>
            <div style={{ marginBottom: '15px' }}>{declineType} - {declineTypeText}</div>
            <div>{declineReason || '회원이 입력한 값이 없습니다.'}</div>
          </div>
          )
        );
      } else {
        this.props.openNotificationDialog('거절 사유', (
          <div>
            <div style={{ marginBottom: '15px' }}>거절 타입이 없습니다.</div>
            <div>{declineReason || '회원이 입력한 값이 없습니다.'}</div>
          </div>
        ));
      }
    }
    return true;
  };

  getApplyStatusText = (data) => {
    let text = '없음';
    const { applyStatus } = data;
    if (applyStatus && applyStatus === ApplyStatusGroup.REQUEST) {
      text = '요청';
    }
    if (applyStatus && applyStatus === ApplyStatusGroup.TUNING) {
      text = (
        <RaisedButton label='조율' onClick={() => this.onClickApplyStatus(data)}/>
      );
    }
    if (applyStatus && applyStatus === ApplyStatusGroup.DECLINE) {
      text = (
        <RaisedButton label='거절' onClick={() => this.onClickApplyStatus(data)} />
      );
    }
    if (applyStatus && applyStatus === ApplyStatusGroup.ACCEPT) {
      text = '수락';
    }
    return text;
  };

  getApplyTypeText = (applyType) => {
    let text = '없음';
    if (applyType && applyType === ApplyTypeGroup.PARENT_TO_SITTER) {
      text = '부모 -> 시터';
    }
    if (applyType && applyType === ApplyTypeGroup.SITTER_TO_PARENT) {
      text = '시터 -> 부모';
    }
    return text;
  };

  render() {
    const { data } = this.props;
    const {
      applyId,
      applicantId,
      applicant,
      applyType,
      receiverId,
      receiver,
      applyStatus,
      applyDate,
      readDate,
      responseDate,
    } = data;
    return (
      <tr className={s.row}>
        <td>{applyId}</td>
        <td>
          <RaisedButton
            onClick={() => this.onClickUserId(applicantId)}
            label={applicantId}
          />
        </td>
        <td>{getUserNameText(applicant)}</td>
        <td>{getUserRoleText(applicant)}</td>
        <td>
          <RaisedButton
            onClick={() => this.onClickUserId(receiverId)}
            label={receiverId}
          />
        </td>
        <td>{getUserNameText(receiver)}</td>
        <td>{getUserRoleText(receiver)}</td>
        <td>{this.getApplyStatusText(data)}</td>
        <td>{SimpleDateFormat(applyDate)}</td>
        <td>{this.getReadDate(readDate)}</td>
        <td>{this.getResponseDate(responseDate)}</td>
      </tr>
    );
  }
}

TableRow.propTypes = {
  data: PropTypes.object.isRequired,
  openNotificationDialog: PropTypes.func.isRequired,
};

TableRow.defaultProps = {};

export default withStyles(s)(TableRow);
