import _ from 'lodash';
import qs from 'qs';
import uuid from 'uuid/v4';
import classnames from 'classnames';
import FlatPagination from 'material-ui-flat-pagination';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './SitterApplications.css';
import SearchBar from '../../SearchBar';
import TableHeaderRow from '../../TableHeaderRow';
import Request from '../../../util/Request';
import TableRow from './TableRow';
import ResponseResultTypeGroup from '../../../util/const/ResponseResultTypeGroup';
import FilterDataTypeGroup from '../../../util/const/FilterDataTypeGroup';
import NotificationDialog from '../../Dialog/NotificationDialog';

class SitterApplications extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      page: 0,
      maxPage: 0,
      notificationDialog: false,
      notificationDialogTitle: '',
      notificationDialogMessage: '',
      searchOptions: [],
      orderOption: {
        column: '',
        method: '',
      },
    };
    this.cachedOption = {};
    this.onClickSearch = this.onClickSearch.bind(this);
    this.onChangeOrderColumn = this.onChangeOrderColumn.bind(this);
    this.onExportCSV = this.onExportCSV.bind(this);
  }

  async componentDidMount() {
    await this.doSearch({})
      .then(data => {
        this.setState({
          list: data.data.rows,
          page: data.page.currentPage,
          maxPage: data.page.maxPage,
        });
      })
      .catch(() => {
        this.setState({ list: [] });
      });
  }

  doSearch = options =>
    new Promise(async (resolve, reject) => {
      const query = qs.stringify(options);
      this.cachedOption = Object.assign({}, options);

      const searchResult = await Request(
        `api/manage/sitter/applications?${query}`,
        { method: 'GET' },
      ).catch(() => false);
      if (searchResult && searchResult.result === ResponseResultTypeGroup.SUCCESS) {
        resolve(searchResult);
      } else {
        reject();
      }
    });

  onClickSearch = async function () {
    // 페이지를 1로 초기화
    const { searchOptions, orderOption } = this.state;
    await this.doSearch({
      searchOptions,
      orderOption,
      requestPage: 0,
    }).then(data => {
      this.setState({
        list: data.data.rows,
        page: data.page.currentPage,
        maxPage: data.page.maxPage,
      });
    }).catch(() => {
      this.setState({ list: [] });
    });
  };

  onClickPage = value => {
    this.cachedOption.requestPage = value;
    this.doSearch(this.cachedOption).then(data => {
      this.setState({
        list: data.data.rows,
        page: data.page.currentPage,
        maxPage: data.page.maxPage,
      });
    }).catch(() => {
      this.setState({ list: [] });
    });
  };

  onChangeOrderColumn = async function (columnId) {
    const { orderOption, searchOptions } = this.state;
    if (orderOption.column !== columnId) {
      orderOption.method = '';
    }
    orderOption.column = columnId;
    if (orderOption.method === 'DESC') {
      orderOption.method = 'ASC';
    } else if (orderOption.method === 'ASC') {
      orderOption.method = '';
    } else {
      orderOption.method = 'DESC'
    }
    this.setState({ orderOption });

    await this.doSearch({
      searchOptions,
      orderOption,
      requestPage: 0,
    }).then(data => {
      this.setState({
        list: data.data.rows,
        page: data.page.currentPage,
        maxPage: data.page.maxPage,
      });
    }).catch(() => {
      this.setState({ list: [] });
    });
  };

  openNotificationDialog = (title, message) => {
    this.setState({
      notificationDialog: true,
      notificationDialogTitle: title || '알림',
      notificationDialogMessage: message,
    });
  };

  onClickCloseNotificationDialog = () => {
    this.setState({
      notificationDialog: false,
    });
  };

  onExportCSV = async function () {
    const queryString = qs.stringify(this.cachedOption);
    window.open(`/api/manage/sitter/applications/csv?${queryString}`);
  };

  getApplicationListUI = (list) => {
    let ui = null;
    if (list && list.length > 0) {
      ui = list.map((item) => {
        return (
          <TableRow
            key={uuid()}
            data={item}
            openNotificationDialog={this.openNotificationDialog}
          />);
      });
    }
    return ui;
  };

  onAddSearchOption = () => {
    const { searchOptions } = this.state;
    searchOptions.push({
      generatedKey: uuid(),
      filterType: '',
      filterValue: '',
      filterDataType: '',
    });
    this.setState({ searchOptions });
  };

  onDeleteSearchOption = (generatedKey) => {
    const { searchOptions } = this.state;
    _.remove(searchOptions, (o) => {
      return o.generatedKey === generatedKey;
    });
    this.setState({ searchOptions });
  };

  onChangeSearchOption = (generatedKey, type, value, dataType) => {
    const { searchOptions } = this.state;
    const targetIndex = _.findIndex(searchOptions, (o) => {
      return o.generatedKey === generatedKey;
    });
    if (targetIndex !== -1) {
      searchOptions[targetIndex] = {
        ...searchOptions[targetIndex],
        filterType: type,
        filterValue: value,
        filterDataType: dataType,
      };
      this.setState({ searchOptions });
    } else {
      console.log('DEBUG : ON CHANGE BUT NOT FOUND : ' + generatedKey);
    }
  };

  render() {
    const {
      list,
      page,
      maxPage,
      notificationDialog,
      notificationDialogTitle,
      notificationDialogMessage,
      searchOptions,
      orderOption,
    } = this.state;
    const headers = [
      { id: 'applyId', desc: '지원번호', sortable: true },
      { id: 'applicantId', desc: '지원자 번호', sortable: true },
      { id: 'applicantName', desc: '지원자 이름', sortable: false },
      { id: 'applicantRole', desc: '지원자 권한', sortable: false },
      { id: 'receiverId', desc: '피 지원자 번호', sortable: true },
      { id: 'receiverName', desc: '피 지원자 이름', sortable: false },
      { id: 'receiverRole', desc: '피 지원자 권한', sortable: false },
      { id: 'applyStatus', desc: '지원 상태', sortable: true },
      { id: 'applyDate', desc: '지원 일자', sortable: true },
      { id: 'readDate', desc: '지원서 확인 일자', sortable: true},
      { id: 'responseDate', desc: '응답 일자', sortable: true },
    ];
    const filter = [
      { name: '신청 번호', type: 'applyId', dataType: FilterDataTypeGroup.UNIQUE_TEXT },
      { name: '신청자 번호', type: 'applicantId', dataType: FilterDataTypeGroup.UNIQUE_TEXT },
      { name: '피 신청자 번호', type: 'receiverId', dataType: FilterDataTypeGroup.UNIQUE_TEXT },
      { name: '신청 상태', type: 'applyStatus', dataType: FilterDataTypeGroup.APPLY_STATUS },
      { name: '신청 일자', type: 'applyDate', dataType: FilterDataTypeGroup.DATE_RANGE },
      { name: '신청서 확인 일자', type: 'readDate', dataType: FilterDataTypeGroup.DATE_RANGE },
      { name: '신청 응답 일자', type: 'responseDate', dataType: FilterDataTypeGroup.DATE_RANGE },
    ];
    return (
      <div className={classnames(s.root, ' box box-default')}>
        <SearchBar
          filter={filter}
          searchOptions={searchOptions}
          onClickSearch={this.onClickSearch}
          openNotificationDialog={this.openNotificationDialog}
          onExportCSV={this.onExportCSV}
          onClickAddSearchOption={this.onAddSearchOption}
          onClickDeleteSearchOption={this.onDeleteSearchOption}
          onChangeSearchOption={this.onChangeSearchOption}
        />
        <div className={classnames("box-body", "padding-sm", s.tableWrap)}>
          <table
            className={classnames('table', s.table)}
            cellPadding="0px"
          >
            <thead>
              <TableHeaderRow
                data={headers}
                orderOption={orderOption}
                onChangeOrderColumn={this.onChangeOrderColumn}
                openNotificationDialog={this.openNotificationDialog}
              />
            </thead>
            <tbody>
              {this.getApplicationListUI(list)}
            </tbody>
          </table>
          <div>
            <FlatPagination
              offset={page}
              limit={1}
              total={maxPage}
              onClick={(e, offset) => this.onClickPage(offset)}
            />
          </div>
        </div>

        <NotificationDialog
          open={notificationDialog}
          title={notificationDialogTitle}
          message={notificationDialogMessage}
          onClose={this.onClickCloseNotificationDialog}
        />
      </div>
    );
  }
}

SitterApplications.propTypes = {};

SitterApplications.defaultProps = {};

export default withStyles(s)(SitterApplications);
