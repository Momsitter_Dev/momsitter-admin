import { compose } from 'recompose';
import React from 'react';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Grid from '@material-ui/core/Grid';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ProfileActivity.css';
import ActivityTypeIdGroup from '../../util/const/ActivityTypeIdGroup';

const ACTIVITYS = [
  {
    id: ActivityTypeIdGroup.IN_DOOR_ACTIVITY,
    label: '실내놀이',
    iconOn: 'change-indoorplay-on.svg',
    iconOff: 'change-indoorplay-off.svg',
  },
  {
    id: ActivityTypeIdGroup.KOREAN_STUDY,
    label: '한글놀이',
    iconOn: 'p-details-1-korean-on.svg',
    iconOff: 'p-details-1-korean-off.svg',
  },
  {
    id: ActivityTypeIdGroup.SIMPLE_CLEANING,
    label: '간단 청소',
    iconOn: 'change-cleaning-on.svg',
    iconOff: 'change-cleaning-off.svg',
  },
  {
    id: ActivityTypeIdGroup.LONG_RESIDENT_SITTER,
    label: '장기입주',
    iconOn: 'change-longterm-on.svg',
    iconOff: 'change-longterm-off.svg',
  },

  {
    id: ActivityTypeIdGroup.SCHOOL_COMPANION,
    label: '등하원돕기',
    iconOn: 'change-join-walk-on.svg',
    iconOff: 'change-join-walk-off.svg',
  },
  {
    id: ActivityTypeIdGroup.ENGLISH_PLAY,
    label: '영어놀이',
    iconOn: 'change-english-on.svg',
    iconOff: 'change-english-off.svg',
  },
  {
    id: ActivityTypeIdGroup.SIMPLE_COOKING,
    label: '밥 챙겨주기',
    iconOn: 'change-cook-on.svg',
    iconOff: 'change-cook-off.svg',
  },
  {
    id: ActivityTypeIdGroup.SHORT_RESIDENT_SITTER,
    label: '단기입주',
    iconOn: 'change-shortperide-on.svg',
    iconOff: 'change-shortperide-off.svg',
  },
  {
    id: ActivityTypeIdGroup.STORY_TELLING,
    label: '책읽기',
    iconOn: 'change-read-on.svg',
    iconOff: 'change-read-off.svg',
  },
  {
    id: ActivityTypeIdGroup.STUDY_GUIDE,
    label: '학습지도',
    iconOn: 'change-specially-on.svg',
    iconOff: 'change-specially-off.svg',
  },
  {
    id: ActivityTypeIdGroup.SIMPLE_DISH_WASHING,
    label: '간단 설거지',
    iconOn: 'change-washing-on.svg',
    iconOff: 'change-washing-off.svg',
  },
  {
    id: ActivityTypeIdGroup.OUT_DOOR_ACTIVITY,
    label: '야외활동',
    iconOn: 'change-outdooractivities-on.svg',
    iconOff: 'change-outdooractivities-off.svg',
  },
  {
    id: ActivityTypeIdGroup.ATHLETIC_PLAY,
    label: '체육놀이',
    iconOn: 'change-pe-on.svg',
    iconOff: 'change-pe-off.svg',
  },
];

const ROW_PER_ITEM = [4, 4, 3, 2];

const getIconPath = fileName => {
  return `https://s3.ap-northeast-2.amazonaws.com/momsitter-service/momsitter-app/static/public/profile/${fileName}`;
};

const getActivityIcon = options => {
  const { active, item } = options;
  if (active) {
    return getIconPath(item.iconOn);
  } else {
    return getIconPath(item.iconOff);
  }
};

const ProfileActivityItem = ({ iconType, active }) => {
  const item = ACTIVITYS.find(n => n.id === iconType);
  return (
    <div className={active ? s.itemWrapActive : s.itemWrapInactive}>
      {item && (
        <div>
          <img className={s.itemImage} src={getActivityIcon({ active, item })} alt={item.label} />
          <br />
          <span> {item.label} </span>
        </div>
      )}
    </div>
  );
};

const propTypes = {
  title: PropTypes.string.isRequired,
  activityIds: PropTypes.arrayOf(PropTypes.number),
  onEdit: PropTypes.func,
};
const defaultProps = {
  activityIds: [],
  onEdit: () => {},
};

class ProfileActivity extends React.Component {
  onClickEdit = () => {
    return this.props.onEdit();
  };

  renderItemByRow = (start, end) =>
    ACTIVITYS.filter((item, index) => index >= start && index <= end).map(item => (
      <ProfileActivityItem
        key={item.id}
        iconType={item.id}
        active={!!this.props.activityIds.find(id => id === item.id)}
      />
    ));

  renderItems = () => {
    const point = {
      start: 0,
      end: 0,
    };
    return ROW_PER_ITEM.map((item, index) => {
      if (index === 0) {
        point.start = index;
        point.end = item - 1;
      } else {
        point.start = point.end + 1;
        point.end += item;
      }
      return <div key={`${point.start}-${point.end}`}>{this.renderItemByRow(point.start, point.end)}</div>;
    });
  };

  render() {
    const { title } = this.props;
    return (
      <ExpansionPanel>
        <ExpansionPanelSummary>
          <Typography variant={'body1'}>{title}</Typography>
        </ExpansionPanelSummary>

        <ExpansionPanelDetails style={{ paddingTop: '0', paddingBottom: '0' }}>
          <Grid container spacing={16}>
            <Grid item xs={12}>
              <div className={s.container}>{this.renderItems()}</div>
            </Grid>
          </Grid>
        </ExpansionPanelDetails>
        <ExpansionPanelActions>
          <Button variant={'raised'} onClick={this.onClickEdit}>
            수정하기
          </Button>
        </ExpansionPanelActions>
      </ExpansionPanel>
    );
  }
}

ProfileActivity.propTypes = propTypes;
ProfileActivity.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(ProfileActivity);
