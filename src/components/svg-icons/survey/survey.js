import styleProps from 'react-style-proptype';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './survey.css';

const propTypes = {
  style: styleProps,
  paperColor: PropTypes.string,
};
const defaultProps = {
  style: {},
};

class survey extends React.Component {
  render() {
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        style={{ width: '56px', height: '58px', ...this.props.style }}
      >
        <g fill="none" fillRule="evenodd" transform="translate(-517 -1004)">
          <g transform="translate(517 1004)">
            <path fill={this.props.paperColor || '#EBE6E5'} d="M0 0h38.419v57.033H8.615L0 47.746z" />
            <path fill="#D0C9C7" d="M8.692 47.698L8.62 57l-8.642-9.23z" />
            <rect width="24.744" height="1.967" x="7.163" y="5.9" fill="#8F8683" rx=".983" />
            <rect width="24.744" height="1.967" x="7.163" y="11.144" fill="#8F8683" rx=".983" />
            <rect width="24.744" height="1.967" x="7.163" y="16.389" fill="#8F8683" rx=".983" />
            <rect width="24.744" height="1.967" x="7.163" y="21.633" fill="#8F8683" rx=".983" />
            <rect width="24.744" height="1.967" x="7.163" y="26.878" fill="#8F8683" rx=".983" />
            <rect width="11.721" height="1.967" x="19.535" y="44.578" fill="#8F8683" rx=".983" />
          </g>
          <g fillRule="nonzero">
            <path fill="#FD003A" d="M568.392 1028.401l-3.351-3.374a1.598 1.598 0 0 1 0-2.249l1.684-1.695a2.358 2.358 0 0 1 3.342 0l2.24 2.257a2.37 2.37 0 0 1 .693 1.683c0 .635-.246 1.233-.693 1.683l-1.683 1.695a1.57 1.57 0 0 1-2.232 0z" />
            <path fill="#E50027" d="M573 1025.012c0-.637-.245-1.236-.692-1.687l-1.118-1.132-4.461 4.515 1.672 1.692a1.562 1.562 0 0 0 2.228 0l1.68-1.7c.446-.451.691-1.05.691-1.688z" />
            <path fill="#E1E4FB" d="M566.229 1030.568l-3.34-3.362a1.591 1.591 0 0 1 0-2.24l2.218-2.232 5.564 5.602-2.218 2.232a1.566 1.566 0 0 1-2.224 0z" />
            <path fill="#C5C9F7" d="M566.243 1030.575a1.574 1.574 0 0 0 2.217 0l2.21-2.198-2.772-2.757-3.32 3.3 1.665 1.655z" />
            <path fill="#FEA832" d="M554.346 1044.741l14.175-14.27-5.536-5.572-14.174 14.27 1.112 2.224a2.32 2.32 0 0 0 1.53.689c.031.56.26 1.112.684 1.54l2.209 1.12z" />
            <path fill="#FE9923" d="M551.32 1042.25c.03.563.257 1.117.681 1.547l2.203 1.125 14.137-14.338-2.76-2.8-14.262 14.465z" />
            <path fill="#E09A85" d="M552.686 1043.272a1.6 1.6 0 0 1 0-2.252 1.575 1.575 0 0 1-2.237 0l-1.68-1.69-.004.005a.804.804 0 0 0-.192.312l-1.554 4.704a3.17 3.17 0 0 1 2.358 2.375l4.674-1.565a.797.797 0 0 0 .31-.193l.004-.005-1.679-1.69z" />
            <path fill="#E09A85" d="M546.482 1047.037a.777.777 0 0 0 .795.19l6.6-2.206a.784.784 0 0 0 .304-.19l.005-.005-1.652-1.66a1.57 1.57 0 0 1 0-2.213l-6.052 6.084z" />
            <path fill="#063E8B" d="M547.012 1044.2l-.67 2.03a.793.793 0 0 0 .19.806.781.781 0 0 0 .801.19l2.015-.674a3.14 3.14 0 0 0-2.336-2.352z" />
            <path fill="#032E68" d="M547.045 1047.267a.83.83 0 0 0 .252-.04l2.051-.655a3.027 3.027 0 0 0-.85-1.47l-2.016 1.94a.814.814 0 0 0 .563.225z" />
          </g>
        </g>
      </svg>
    );
  }
}

survey.propTypes = propTypes;
survey.defaultProps = defaultProps;

export default withStyles(s)(survey);
