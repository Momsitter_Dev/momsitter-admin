import styleProps from 'react-style-proptype';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './cctv.css';

const propTypes = {
  color: PropTypes.string,
  style: styleProps,
};
const defaultProps = {
  color: '#999999',
  style: {
    width: '15px',
    height: '10px',
  },
};

class cctv extends React.Component {
  render() {
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        style={this.props.style}
      >
        <path
          fill={this.props.color}
          fillRule="nonzero"
          d="M10.889 3.375V.75c0-.45-.311-.75-.778-.75H.778C.31 0 0 .3 0 .75v7.5C0 8.7.311 9 .778 9h9.333c.467 0 .778-.3.778-.75V5.625l3.111 3V.375l-3.111 3z"
        />
      </svg>
    );
  }
}

cctv.propTypes = propTypes;
cctv.defaultProps = defaultProps;

export default withStyles(s)(cctv);
