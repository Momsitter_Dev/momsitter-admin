import styleProps from 'react-style-proptype';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './search.css';

const propTypes = {
  color: PropTypes.string,
  style: styleProps,
};
const defaultProps = {
  color: '#000000',
  style: {
    width: '11px',
    height: '11px',
  },
};

class search extends React.Component {
  render() {
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        style={this.props.style}
      >
        <path
          fill={this.props.color}
          fillRule="nonzero"
          d="M10.833 10.02L8.12 7.295a4.336 4.336 0 0 0 1.08-2.855C9.2 1.992 7.136 0 4.6 0 2.063 0 0 1.992 0 4.441c0 2.45 2.064 4.441 4.6 4.441.952 0 1.86-.277 2.636-.803l2.732 2.743A.606.606 0 0 0 10.4 11a.608.608 0 0 0 .416-.162.566.566 0 0 0 .017-.819zM4.6 1.158C6.475 1.159 8 2.63 8 4.44S6.475 7.724 4.6 7.724 1.2 6.25 1.2 4.44c0-1.81 1.525-3.282 3.4-3.282z"
        />
      </svg>
    );
  }
}

search.propTypes = propTypes;
search.defaultProps = defaultProps;

export default withStyles(s)(search);
