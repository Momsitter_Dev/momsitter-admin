import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Done.css';

const propTypes = {
  color: PropTypes.string,
};
const defaultProps = {
  color: '#fff',
};

class Done extends React.Component {
  render() {
    return (
      <svg xmlns="http://www.w3.org/2000/svg" width="19" height="16" viewBox="0 0 19 16">
        <g fill="none" fillRule="evenodd" transform="translate(-226 -437)">
          <path fill={this.props.color} fillRule="nonzero" d="M226.286 445.719a.8.8 0 0 1-.218-.515.8.8 0 0 1 .218-.515l1.019-1.03a.698.698 0 0 1 1.018 0l.073.074 4.001 4.34a.349.349 0 0 0 .51 0l9.748-10.225h.073a.698.698 0 0 1 1.018 0l1.019 1.03a.717.717 0 0 1 0 1.03l-11.64 12.21a.657.657 0 0 1-.51.221.657.657 0 0 1-.509-.22l-5.674-6.18-.146-.22z" />
        </g>
      </svg>
    );
  }
}

Done.propTypes = propTypes;
Done.defaultProps = defaultProps;

export default withStyles(s)(Done);
