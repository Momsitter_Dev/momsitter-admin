import styleProps from 'react-style-proptype';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './check.css';

const propTypes = {
  style: styleProps,
  color: PropTypes.string,
};
const defaultProps = {
  style: {},
  color: '#FFBB21',
};

class check extends React.Component {
  render() {
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        style={{ width: '32px', height: '20px', ...this.props.style }}
      >
        <path
          fill={this.props.color}
          fillRule="nonzero"
          d="M28.345.518c.92-.765 2.314-.672 3.114.21.8.88.702 2.216-.218 2.982L12.29 19.482a2.279 2.279 0 0 1-3.009-.101L.647 11.11a2.052 2.052 0 0 1 0-2.99 2.277 2.277 0 0 1 3.121 0l7.18 6.877L28.346.518z"
        />
      </svg>
    );
  }
}

check.propTypes = propTypes;
check.defaultProps = defaultProps;

export default withStyles(s)(check);
