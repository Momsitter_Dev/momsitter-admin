import styleProps from 'react-style-proptype';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './clock.css';

const propTypes = {
  color: PropTypes.string,
  style: styleProps,
};
const defaultProps = {
  color: '#000',
  style: {
    width: '11px',
    height: '11px',
  },
};

class clock extends React.Component {
  render() {
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        style={this.props.style}
      >
        <g fill={this.props.color} fillRule="nonzero">
          <path d="M10.56 3.358C9.982 2.005 8.996 1.02 7.633.439A5.404 5.404 0 0 0 5.491 0c-.746 0-1.454.147-2.135.44-1.352.58-2.342 1.563-2.923 2.918A5.358 5.358 0 0 0 0 5.494c0 .746.142 1.463.433 2.142a5.42 5.42 0 0 0 2.923 2.925c.68.292 1.39.439 2.135.439.746 0 1.46-.149 2.141-.44 1.363-.581 2.349-1.571 2.929-2.924A5.385 5.385 0 0 0 11 5.494c0-.746-.147-1.455-.44-2.136zm-1.114 4.43a4.554 4.554 0 0 1-1.668 1.663 4.467 4.467 0 0 1-2.286.614c-.619 0-1.209-.12-1.77-.364a4.64 4.64 0 0 1-1.453-.97 4.614 4.614 0 0 1-.97-1.46A4.472 4.472 0 0 1 .94 5.494c0-.823.202-1.585.607-2.282.407-.7.965-1.255 1.663-1.663A4.433 4.433 0 0 1 5.49.935c.823 0 1.589.206 2.287.614.703.41 1.26.965 1.668 1.663a4.44 4.44 0 0 1 .613 2.282c0 .824-.203 1.59-.613 2.294z"/>
          <path d="M6.008 5.747V2.745c0-.18-.174-.325-.374-.325-.186 0-.354.145-.354.325v3.048c0 .006.013.029.013.046a.285.285 0 0 0 .097.261l1.926 1.733a.397.397 0 0 0 .496 0c.149-.133.139-.327 0-.452L6.008 5.747z"/>
        </g>
      </svg>
    );
  }
}

clock.propTypes = propTypes;
clock.defaultProps = defaultProps;

export default withStyles(s)(clock);
