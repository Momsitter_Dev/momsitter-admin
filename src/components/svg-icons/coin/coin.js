import styleProps from 'react-style-proptype';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './coin.css';

const propTypes = {
  style: styleProps,
};
const defaultProps = {
  style: {},
};

class coin extends React.Component {
  render() {
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        style={{ width: '17px', height: '13px', ...this.props.style}}
      >
        <g fill="none" fillRule="evenodd">
          <g fill="#ACC9C3">
            <path fillRule="nonzero" d="M16.981 6.49c0-3.578-2.99-6.48-6.677-6.48-.412 0-.816.037-1.208.107 3.112.551 5.47 3.195 5.47 6.374s-2.358 5.823-5.47 6.374c.392.07.796.106 1.208.106 3.688 0 6.677-2.901 6.677-6.48z"/>
            <path d="M12.93 6.49c0 3.466-2.895 6.275-6.466 6.275C2.894 12.765 0 9.955 0 6.49 0 3.026 2.894.217 6.464.217c3.57 0 6.465 2.809 6.465 6.274zM2.785 6.057v.831h.99l.741 3.058h1.16l.697-3.066h.714l.696 3.066h1.17l.732-3.058h1v-.831H9.82l.616-2.478h-1.08l-.518 2.478h-.991l-.545-2.478H6.16l-.545 2.478h-1l-.508-2.478h-1.08l.607 2.478h-.848zm3.713.017l.17-.823.062-.546.072.546.151.823H6.5zm2.196.797l-.241 1.265-.098.624-.098-.624L7.98 6.87h.714zm-3.231 0l-.268 1.265-.09.624-.098-.624-.25-1.265h.706z"/>
          </g>
        </g>
      </svg>
    );
  }
}

coin.propTypes = propTypes;
coin.defaultProps = defaultProps;

export default withStyles(s)(coin);
