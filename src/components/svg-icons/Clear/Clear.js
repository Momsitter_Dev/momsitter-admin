import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Clear.css';

const propTypes = {
  color: PropTypes.string,
};
const defaultProps = {
  color: '#fff',
};

class Clear extends React.Component {
  render() {
    return (
      <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 15 15">
        <g fill="none" fillRule="evenodd" transform="translate(-59 -438)">
          <g stroke={this.props.color} strokeLinecap="round" strokeWidth="3">
            <path d="M71.95 440.343l-10.63 10.63M71.973 450.973l-10.63-10.63" />
          </g>
        </g>
      </svg>
    );
  }
}

Clear.propTypes = propTypes;
Clear.defaultProps = defaultProps;

export default withStyles(s)(Clear);
