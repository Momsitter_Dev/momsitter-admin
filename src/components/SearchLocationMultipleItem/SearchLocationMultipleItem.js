import FlatButton from 'material-ui/FlatButton';
import DoneIcon from 'material-ui/svg-icons/action/done';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './SearchLocationMultipleItem.css';

const propTypes = {
  data: PropTypes.shape({
    address: PropTypes.string,
    addressId: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    detail: PropTypes.string,
    locationLatitude: PropTypes.string,
    locationLongitude: PropTypes.string,
    main: PropTypes.string,
    sub: PropTypes.string,
  }).isRequired,
  addressType: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  selected: PropTypes.bool.isRequired,
};
const defaultProps = {};

const ADDRESS_TYPE = {
  MAIN: 'main',
  SUB: 'sub',
  DETAIL: 'detail',
};

class SearchLocationMultipleItem extends React.Component {
  onClick = () => {
    const { addressType, data } = this.props;
    return this.props.onClick(addressType, data);
  };

  renderItem = () => {
    const { data, addressType } = this.props;
    return (
      <FlatButton
        fullWidth
        label={data[addressType]}
        labelStyle={{
          fontSize: '0.9rem',
          fontWeight: '400',
          color: '#1e1e1e',
        }}
        style={{
          borderBottom: '1px solid #f0f0f0',
          height: '45px',
        }}
        onClick={this.onClick}
      />
    );
  };

  renderCheckedItem = () => {
    const { addressType, data } = this.props;
    return (
      <FlatButton
        fullWidth
        icon={
          addressType === ADDRESS_TYPE.DETAIL || data[addressType] === '전체' ? (
            <DoneIcon style={{ width: '15px', height: '15px', fill: '#ff5300' }} />
          ) : null
        }
        label={data[addressType]}
        onClick={this.onClick}
        backgroundColor="#fff8f2"
        style={{
          borderBottom: '1px solid #f0f0f0',
          height: '45px',
        }}
        labelStyle={{
          fontSize: '0.9rem',
          fontWeight: 'bold',
          color: '#ff7100',
        }}
      />
    );
  };

  renderContent = () => {
    const { selected } = this.props;
    if (selected) {
      return this.renderCheckedItem();
    }
    return this.renderItem();
  };

  render() {
    return <div className={s.root}>{this.renderContent()}</div>;
  }
}

SearchLocationMultipleItem.propTypes = propTypes;
SearchLocationMultipleItem.defaultProps = defaultProps;

export default withStyles(s)(SearchLocationMultipleItem);
