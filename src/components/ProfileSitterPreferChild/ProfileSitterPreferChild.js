import { compose } from 'recompose';
import React from 'react';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles/index';
import Button from '@material-ui/core/Button';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ProfileSitterPreferChild.css';
import ChildAgeIdGroup from '../../util/const/ChildAgeIdGroup';

const CARE_AGE = [
  {
    id: ChildAgeIdGroup.NEW,
    label: '신생아',
    iconOn: 'sitter-profile-age-new-on.svg',
    iconOff: 'sitter-profile-age-new-off.svg',
  },
  {
    id: ChildAgeIdGroup.YONG,
    label: '영아',
    iconOn: 'sitter-profile-age-yong-on.svg',
    iconOff: 'sitter-profile-age-yong-off.svg',
  },
  {
    id: ChildAgeIdGroup.CHILD,
    label: '유아',
    iconOn: 'sitter-profile-age-child-on.svg',
    iconOff: 'sitter-profile-age-child-off.svg',
  },
  {
    id: ChildAgeIdGroup.ELEMENT,
    label: '초등학생',
    iconOn: 'sitter-profile-age-element-on.svg',
    iconOff: 'sitter-profile-age-element-off.svg',
  },
];

const propTypes = {
  title: PropTypes.string.isRequired,
  childAgeIds: PropTypes.arrayOf(PropTypes.number),
  onEdit: PropTypes.func.isRequired,
};
const defaultProps = {
  childAgeIds: [],
  onEdit: () => {},
};

class ProfileSitterPreferChild extends React.Component {
  onClickEdit = () => {
    return this.props.onEdit();
  };

  renderEmpty = () => {
    return <Typography variant={'catpion'}> 표시할 정보가 없습니다. </Typography>;
  };

  renderPreferChild = () => {
    const { childAgeIds } = this.props;

    return CARE_AGE.map((item, index) => {
      const active = !!childAgeIds.find(id => id === item.id);
      return (
        <div
          key={`care-age-item-${index}`}
          className={active ? s.itemWrapActive : s.itemWrapInactive}
        >
          {item && (
            <div>
              <img
                className={s.itemImage}
                src={`https://s3.ap-northeast-2.amazonaws.com/momsitter-service/momsitter-app/static/public/profile/${
                  active ? item.iconOn : item.iconOff
                }`}
                alt={item.label}
              />
              <br />
              <span> {item.label} </span>
            </div>
          )}
        </div>
      );
    });
  };

  renderContent = () => {
    const { childAgeIds } = this.props;
    if (!childAgeIds) {
      return this.renderEmpty();
    }
    if (childAgeIds.length === 0) {
      return this.renderEmpty();
    }
    return this.renderPreferChild();
  };

  render() {
    const { title } = this.props;
    return (
      <ExpansionPanel>
        <ExpansionPanelSummary>
          <Typography variant={'body1'}> {title} </Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails style={{ paddingTop: '0', paddingBottom: '0' }}>
          <Grid container spacing={16}>
            <Grid item xs={12}>
              <div className={s.container}>
                {this.renderContent()}
              </div>
            </Grid>
          </Grid>
        </ExpansionPanelDetails>
        <ExpansionPanelActions>
          <Button variant={'flat'} onClick={this.onClickEdit}> 수정하기 </Button>
        </ExpansionPanelActions>
      </ExpansionPanel>
    );
  }
}

ProfileSitterPreferChild.propTypes = propTypes;
ProfileSitterPreferChild.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(ProfileSitterPreferChild);
