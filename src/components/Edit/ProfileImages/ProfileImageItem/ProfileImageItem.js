import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ProfileImageItem.css';

class ProfileImageItem extends React.Component {
  render() {
    return (
      <div className={s.root}>
        <div className={s.container}>

        </div>
      </div>
    );
  }
}

ProfileImageItem.propTypes = {};

ProfileImageItem.defaultProps = {};

export default withStyles(s)(ProfileImageItem);
