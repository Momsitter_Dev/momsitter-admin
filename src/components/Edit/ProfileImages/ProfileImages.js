import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ProfileImages.css';
import ProfileImageItem from './ProfileImageItem';

class ProfileImages extends React.Component {
  /*basicInfo.userProfileImage
   .filter((item, index) => {
   if (
   item.profileImageType === 'parentProfileMain' ||
   item.profileImageType === 'sitterProfileMain'
   ) {
   return true;
   }
   })
   .map((item, index) =>
   <img
   key={index}
   src={item.profileFile.fileUrl}
   style={{ width: '150px', height: '150px' }}
   />,
   )*/
  render() {
    const { profileImages } = this.props;
    return (
      <div className="col-xl-12">
        {}
      </div>
    );
  }
}

ProfileImages.propTypes = {
  profileImages: PropTypes.array,
};

ProfileImages.defaultProps = {};

export default withStyles(s)(ProfileImages);
