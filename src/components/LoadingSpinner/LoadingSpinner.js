import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './LoadingSpinner.css';
import CircularProgress from 'material-ui/CircularProgress';

class LoadingSpinner extends React.Component {
  getMessage = (message) => {
    let result = '잠시만 기다려 주세요 . . . ';
    if (message) {
      result = message;
    }
    return result;
  };

  getCircleSize = (size) => {
    let result = 0;
    if (size) {
      result = size;
    }
    return size;
  };

  render() {
    const { message, size } = this.props;
    return (
      <div className={s.root}>
        <CircularProgress size={this.getCircleSize(size)}/>
        <div className={s.message}>{this.getMessage(message)}</div>
      </div>
    );
  }
}

LoadingSpinner.propTypes = {
  message: PropTypes.string,
  size: PropTypes.number,
};

LoadingSpinner.defaultProps = {};

export default withStyles(s)(LoadingSpinner);
