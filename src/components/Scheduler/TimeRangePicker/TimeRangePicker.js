/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './TimeRangePicker.css';

import TimePicker from 'material-ui/TimePicker';
import ChevronRight from 'material-ui/svg-icons/navigation/chevron-right';
import {grey400} from 'material-ui/styles/colors';

/*
  Note
  material-ui/TimePicker 의 locale 설정을 위해선 아래 링크를 참조
  See ,http://www.material-ui.com/#/components/date-picker, to localize the TimePicker
 */

/*
 Note
 다른 사용가능한 콤포넌트

 Datetime picker (not react component)
 https://ripjar.github.io/material-datetime-picker/

 More perceivable TimePicker
 https://github.com/callemall/material-ui/issues/5306
 */

class TimeRangePicker extends React.Component {
  constructor(props) {
    super(props);
  }

  getStyles = () => {
    return {
      timePicker: {
        floatingLabelStyle: {
          color: '#383838',
          fontSize: '12px',
          fontWeight: '300',
          left: '50%',
          transform: 'translateX(-50%)',
        },
        floatingLabelShrinkStyle: {
          width: '100%',
          fontWeight: 'normal',
          transform: 'scale(1) translate(-50%, -30px)',
        },
        textFieldStyle: {
          width: '100%',
          fontSize: '14px',
        },
        inputStyle: {
          textAlign: 'center',
          color: '#ff7000',
          fontWeight: 'bold',
          marginTop: '5px',
        },
        underlineStyle: {
          display: 'none',
        },
        style: {
          width: '100%',
        }
      },
      iconStyles: {
        position: 'absolute', top: '50%', left: '50%',
        transform: 'translate(-50%, -50%)',
      }
    };
  };

  handleStartTimeChange = (event, value) => {
    this.props.onChange(event, {
      startTime: value,
      endTime: this.props.endTime,
    });
  };

  handleEndTimeChange = (event, value) => {
    this.props.onChange(event, {
      startTime: this.props.startTime,
      endTime: value,
    });
  };

  render() {
    const {startTime, endTime} = this.props;
    const styles = this.getStyles();

    return (
      <div className={s.root}>
        <div className={s.container}>
          <div className={s.timePicker}>
            {
              /*
               Note
               TimePicker 현재 버그 존재 (TimePicker alignment issue, 110px 정도 왼쪽으로 치우짐,
               App.js  Note comment 참고
               */
              /*
               Note
               제한된 Device height 을 가지고 있는 경우, TimePicker 내 엘리먼트가 짤리는 현상 존재
               https://github.com/callemall/material-ui/issues/6682
               */
            }
            <TimePicker
              className="TimePickerTEST"
              floatingLabelText="시작 시간"
              floatingLabelStyle={styles.timePicker.floatingLabelStyle}
              floatingLabelShrinkStyle={styles.timePicker.floatingLabelShrinkStyle}
              style={styles.timePicker.style}
              textFieldStyle={styles.timePicker.textFieldStyle}
              inputStyle={styles.timePicker.inputStyle}
              underlineStyle={styles.timePicker.underlineStyle}
              autoOk={true}
              okLabel="확인"
              cancelLabel="취소"
              defaultTime={startTime}
              onChange={this.handleStartTimeChange}
            />
          </div>

          <div className={s.icon}>
            <ChevronRight style={styles.iconStyle} color={grey400} />
          </div>

          <div className={s.timePicker}>
            <TimePicker
              floatingLabelText="종료 시간"
              floatingLabelStyle={styles.timePicker.floatingLabelStyle}
              floatingLabelShrinkStyle={styles.timePicker.floatingLabelShrinkStyle}
              style={styles.timePicker.style}
              textFieldStyle={styles.timePicker.textFieldStyle}
              inputStyle={styles.timePicker.inputStyle}
              underlineStyle={styles.timePicker.underlineStyle}
              autoOk={true}
              okLabel="확인"
              cancelLabel="취소"
              defaultTime={endTime}
              onChange={this.handleEndTimeChange}
            />
          </div>
        </div>
      </div>
    );
  }
}

TimeRangePicker.propTypes = {
  style: PropTypes.object,
  startTime: PropTypes.instanceOf(Date),
  endTime: PropTypes.instanceOf(Date),
  onChange: PropTypes.func,
};

TimeRangePicker.defaultProps = {
  style: {},
  startTime: new Date(),
  endTime: new Date(),
  onChange: () => {},
};

export default withStyles(s)(TimeRangePicker);
