/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Scheduler.css';
import moment from 'moment';
import RaisedButton from 'material-ui/RaisedButton';
import DateRow from './DateRow';
import ScheduleCellContainer from './ScheduleCellContainer';
import DialogSitterScheduleAdd from './DialogSitterScheduleAdd';
import DialogScheduleUpdate from './DialogScheduleUpdate';
import DialogParentScheduleAdd from './DialogParentScheduleAdd';

// TODO: event 기반의 네이밍을 schedule 기반의 네이밍으로 바꿀 것
class Scheduler extends React.Component {
  constructor(props) {
    super(props);

    const schedules = [];
    for (const item of props.schedules) {
      if (typeof item.scheduleDate === 'string') {
        item.scheduleDate = moment(item.scheduleDate).toDate();
      }
      schedules.push(item);
    }

    this.state = {
      dialogType: (props.userTypeId) ? 'parent' : 'sitter',
      add_dialog: {
        open: false,
      },
      edit_dialog: {
        open: false,
      },
      schedules,
    };
  }

  getStyles = () => ({
    raisedButton: {
      labelStyle: { fontSize: '15px', fontWeight: 'bold', color: 'white', verticalAlign: 'top' },
      buttonStyle: { backgroundColor: 'transparent', height: 46 },
      style: { borderRadius: 100, position: 'relative', overflow: 'hidden', zIndex: '9' },
    },
    tableContainer: {
      display: this.state.add_dialog.open || this.state.edit_dialog.open ? 'none' : 'block',
    },
    add_dialog: {
      style: { display: this.state.add_dialog.open ? 'block' : 'none' },
    },
    edit_dialog: {
      style: { display: this.state.edit_dialog.open ? 'block' : 'none' },
    },
  });

  sundayBaseToMondayBase = momentDate => (momentDate.weekday() + 6) % 7;

  generateSchedules = (weekdayItem, repeat, startDate) => {
    const schedules = this.state.schedules;
    const base = moment(startDate.getTime());
    for (let i = 0; i < repeat; i++) {
      if (weekdayItem[this.sundayBaseToMondayBase(base)]) {
        const { startTime, endTime, location } = weekdayItem[this.sundayBaseToMondayBase(base)];
        // TODO: 시간 충돌 확인
        schedules.push({
          scheduleDate: base.toDate(),
          scheduleStartTime: moment(startTime.getTime()).format('HH:mm'),
          scheduleEndTime: moment(endTime.getTime()).format('HH:mm'),
          scheduleLocation: location,
        });
      }
      base.add(1, 'days');
    }
    return schedules;
  };

  getTimeColumnRender = () => {
    const { startTime, endTime, unitTime, unitHeight } = this.props;
    const start = moment(startTime, 'HH:mm'),
      end = moment(endTime, 'HH:mm'),
      render = [];
    let timeDiff = end.diff(start, 'minutes') + unitTime;
    let index = 0;

    while (timeDiff >= unitTime) {
      render.push(
        <div key={index} className={s.timeCell} style={{ height: unitHeight, lineHeight: `${unitHeight}px` }}>
          {start.clone().format('HH:00')}
        </div>,
      );
      start.add(unitTime, 'minutes');
      timeDiff -= unitTime;
      index++;
    }

    return render;
  };

  getGuideLines = () => {
    const { startTime, endTime, unitTime, unitHeight, unitWidth, duration } = this.props;
    const start = moment(startTime, 'HH:mm'),
      end = moment(endTime, 'HH:mm'),
      render = [];
    const timeDiff = end.diff(start, 'minutes') + unitTime;
    const guideLineCount = timeDiff / unitTime;
    const guideLineWidth = unitWidth * duration;

    for (let i = 0; i < guideLineCount; i++) {
      render.push(
        <div key={i} style={{ marginBottom: unitHeight - 1, width: guideLineWidth }} className={s.guideLine} />,
      );
    }

    return render;
  };

  handleSave = (event, value) => {
    const { dialogType } = this.state;
    let schedules = null;
    if (dialogType === 'sitter') {
      schedules = this.generateSchedules(value.weekdayItems, value.repeat, value.startDate);
      this.setState({
        add_dialog: {
          ...this.state.add_dialog,
          open: false,
        },
        schedules,
      }, () => {
        this.props.onDialogClose();
        this.props.onChange(null, schedules);
      });
    } else {
      // TODO Save Parent
      schedules = value.schedules;
      this.setState({
        add_dialog: {
          ...this.state.add_dialog,
          open: false,
        },
        schedules,
      }, () => {
        this.props.onDialogClose();
        // 부모는 스케줄 이외에 협의가능 등 부수적인 값들도 함께 넘겨준다.
        this.props.onChange(null, schedules, value);
      });
    }
  };

  handleScheduleClick = (event, value) => {
    if (this.props.mode === 'edit') {
      this.setState({
        edit_dialog: {
          ...this.state.edit_dialog,
          open: true,
          target: value,
        },
      }, () => this.props.onDialogOpen());
    }
  };

  onClickDialogAddOpen = () => {
    this.setState({
      add_dialog: {
        ...this.state.add_dialog,
        open: true,
      },
    }, () => this.props.onDialogOpen());
  };

  onClickDialogAddClose = () => {
    this.setState({
      add_dialog: {
        ...this.state.add_dialog,
        open: false,
      },
    }, () => this.props.onDialogClose());
  };

  handleUpdateDialogClose = () => {
    this.setState({
      edit_dialog: {
        ...this.state.edit_dialog,
        open: false,
      },
    }, () => this.props.onDialogClose());
  };

  handleScheduleUpdate = (event, value) => {
    const schedules = this.state.schedules;
    schedules[value.target] = {
      ...schedules[value.target],
      ...value,
    };

    this.setState({
      edit_dialog: {
        ...this.state.edit_dialog,
        open: false,
      },
      schedules,
    }, () => {
      this.props.onDialogClose();
      this.props.onChange(null, schedules);
    });
  };

  handleScheduleDelete = (event, value) => {
    const schedules = this.state.schedules;
    schedules.splice(value, 1);

    this.setState({
      edit_dialog: {
        ...this.state.edit_dialog,
        open: false,
      },
      schedules,
    }, () => {
      this.props.onDialogClose();
      this.props.onChange(null, schedules);
    });
  };

  render() {
    const { duration, unitWidth, mode, userTypeId } = this.props;
    const { schedules, edit_dialog } = this.state;
    const styles = this.getStyles();
    const timeColumnRender = this.getTimeColumnRender();
    const guideLineRender = this.getGuideLines();
    let addDialogUI = null;

    if (userTypeId === 5 && mode === 'edit') {
      // NOTE 부모
      addDialogUI = (
        <div className={s.dialogContainer}>
          <DialogParentScheduleAdd
            style={styles.add_dialog.style}
            onSave={this.handleSave}
            onClose={this.onClickDialogAddClose}
          />
          <DialogScheduleUpdate
            {...schedules[edit_dialog.target]}
            target={edit_dialog.target}
            style={styles.edit_dialog.style}
            onClose={this.handleUpdateDialogClose}
            onSave={this.handleScheduleUpdate}
            onDelete={this.handleScheduleDelete}
          />
        </div>
      );
    } else {
      // NOTE 시터
      addDialogUI = (
        <div className={s.dialogContainer}>
          <DialogSitterScheduleAdd
            style={styles.add_dialog.style}
            availableLocation={['서울특별시 성북구 안암동5가', '서울특별시 성북구 종암로 13 고려대학교교우회']}
            onSave={this.handleSave}
            onClose={this.onClickDialogAddClose}
          />
          <DialogScheduleUpdate
            {...schedules[edit_dialog.target]}
            target={edit_dialog.target}
            style={styles.edit_dialog.style}
            onClose={this.handleUpdateDialogClose}
            onSave={this.handleScheduleUpdate}
            onDelete={this.handleScheduleDelete}
          />
        </div>
      );
    }

    return (
      <div className={s.root}>
        <div className={s.container}>
          <div className={s.tableContainer} style={styles.tableContainer}>
            <DateRow duration={duration} unitWidth={unitWidth} />

            <div className={s.timeColumn}>
              {timeColumnRender}
            </div>

            <div className={s.scheduleCellContainer}>
              {guideLineRender}
              <ScheduleCellContainer schedules={schedules} onScheduleClick={this.handleScheduleClick} />
            </div>

            {
              mode === 'edit' &&
              <RaisedButton
                label="일정 추가"
                className={s.btnAddSchedule}
                labelStyle={styles.raisedButton.labelStyle}
                buttonStyle={styles.raisedButton.buttonStyle}
                style={styles.raisedButton.style}
                onClick={this.onClickDialogAddOpen}
              />
            }
          </div>
          {addDialogUI}
        </div>
      </div>
    );
  }
}

Scheduler.propTypes = {
  duration: PropTypes.number,
  startTime: PropTypes.string,
  endTime: PropTypes.string,
  unitTime: PropTypes.number,
  unitHeight: PropTypes.number,
  unitWidth: PropTypes.number,
  locationItem: PropTypes.arrayOf(PropTypes.string),
  schedules: PropTypes.arrayOf(
    PropTypes.shape({
      scheduleDate: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.instanceOf(Date),
      ]),
      scheduleStartTime: PropTypes.string,
      scheduleEndTime: PropTypes.string,
      scheduleLocation: PropTypes.string,
    }),
  ),
  mode: PropTypes.oneOf(['edit', 'view']),
  onChange: PropTypes.func,
  onDialogOpen: PropTypes.func,
  onDialogClose: PropTypes.func,
  userTypeId: PropTypes.number,
};

Scheduler.defaultProps = {
  duration: 60,
  startTime: '00:00',
  endTime: '24:00',
  unitTime: 120,
  unitHeight: 30,
  unitWidth: 50,
  locationItem: [],
  schedules: [],
  mode: 'edit',
  onChange: () => {},
  onDialogOpen: () => {},
  onDialogClose: () => {},
  userTypeId: null,
};

export default withStyles(s)(Scheduler);
