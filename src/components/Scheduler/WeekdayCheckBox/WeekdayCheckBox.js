/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './WeekdayCheckBox.css';

import FlatButton from 'material-ui/FlatButton';

class WeekdayCheckBox extends React.Component {
  constructor(props) {
    super(props);
  }

  getCheckBoxRender = () => {
    const checked = this.props.checked;
    const weekday = ['월', '화', '수', '목', '금', '토', '일'], render = [];
    let index = 0;

    for (let item of weekday) {
      const key = index;
      render.push(
        <FlatButton
          key={key}
          label={item}
          style={{
            width: 38, height: 38,
            minWidth: 38,
            borderRadius: 100,
            border: checked[index] ? '1px solid #ff6000' : '1px solid #d3d3d3',
            backgroundColor: checked[index] ? '#ff6000' : 'white',
          }}
          labelStyle={{
            padding: 0,
            color: checked[index] ? 'white' : '#3f3434',
            verticalAlign: 'top',
            fontSize: '14px',
            fontWeight: 300,
          }}
          onClick={() => this.handleClick(null, key)}
        />
      );
      index++;
    }
    return render;
  };

  handleClick = (event, value) => {
    this.props.onChange(event, value);
  };

  render() {
    const { style } = this.props;
    const checkBoxRender = this.getCheckBoxRender();

    return (
      <div className={s.root}>
        <div className={s.container} style={style}>

          <div className={s.checkBoxContainer}>
            { checkBoxRender }
          </div>

        </div>
      </div>
    );
  }
}

WeekdayCheckBox.propTypes = {
  checked: PropTypes.arrayOf(
    PropTypes.oneOfType([
      PropTypes.bool,
      PropTypes.shape({
        startTime: PropTypes.instanceOf(Date),
        endTime: PropTypes.instanceOf(Date),
        location: PropTypes.string
      })
    ])
  ),
  style: PropTypes.object,
  onChange: PropTypes.func,
};

WeekdayCheckBox.defaultProps = {
  checked: [false, false, false, false, false, false, false],
  style: {},
  onChange: () => {},
};

export default withStyles(s)(WeekdayCheckBox);
