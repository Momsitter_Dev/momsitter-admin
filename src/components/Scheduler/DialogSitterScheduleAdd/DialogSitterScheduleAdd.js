/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './DialogSitterScheduleAdd.css';

import moment from 'moment';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';
import IconButton from 'material-ui/IconButton';
import Close from 'material-ui/svg-icons/navigation/close';
import {grey600, grey400} from 'material-ui/styles/colors';
import DatePicker from 'material-ui/DatePicker';
import Divider from 'material-ui/Divider';

import WeekdayCheckBox from '../WeekdayCheckBox';
import TimeRangePicker from '../TimeRangePicker';
import WeekdayTimeLocationPicker from '../WeekdayTimeLocationPicker';

class DialogSitterScheduleAdd extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      weekdayItems: props.weekdayItems,
      timeRange: props.timeRange,
      repeat: props.repeat,
      startDate: props.defaultStartDate,
    };
  }

  getStyles = () => {
    return {
      datePicker: {
        underlineStyle: {display: 'none'},
        inputStyle: {textAlign: 'right', color: '#ff7000', fontWeight: 'bold', marginRight: '50px'},
        style: {width: '100%'},
        textFieldStyle: {width: '100%'},
      },
      IconButton: {position: 'absolute', left: 11, top: 11},
      divider: {
        marginTop: 12, marginBottom: 12,
        backgroundColor: '#f0f0f0'
      }
    }
  };

  handleWeekdayCheckBoxChange = (event, value) => {
    const {weekdayItems, timeRange} = this.state;
    if (weekdayItems[value]) {
      weekdayItems[value] = false;
    } else {
      weekdayItems[value] = {
        ...timeRange
      };
    }
    this.setState({weekdayItems});
  };

  handleTimeRangeChange = (event, value) => {
    const {weekdayItems} = this.state, newWeekdayItems = [];
    for (let item of weekdayItems) {
      if (item) {
        newWeekdayItems.push({ ...item, ...value});
      } else {
        newWeekdayItems.push(false);
      }
    }
    this.setState({ timeRange: value, weekdayItems: newWeekdayItems });
  };

  handleWeekdayTimeLocationChange = (event, value) => {
    const weekdayItems = this.state.weekdayItems;
    weekdayItems[value.index] = value;
    this.setState({weekdayItems});
  };

  handleRepeatChange = (event, value) => {
    this.setState({repeat: value});
  };

  handleStartDateChange = (event, value) => {
    this.setState({startDate: value});
  };

  handleSaveClick = () => {
    const {weekdayItems, repeat, startDate} = this.state;
    this.props.onSave(null, {weekdayItems, repeat, startDate});
  };

  render() {
    const {style, availableLocation, defaultRepeat} = this.props;
    const {weekdayItems, repeat, startDate, timeRange} = this.state;

    const styles = this.getStyles();

    const minDate = new Date();
    const maxDate = moment(minDate.getTime()).add(100, 'days').toDate();

    return (
      <div className={s.root}>
        <div className={s.container} style={style}>

          <IconButton style={styles.IconButton} onClick={this.props.onClose}>
            <Close color={grey600}/>
          </IconButton>

          <h3 className={s.title}> 활동 일정 추가 </h3>

          <div className={s.flexContainer}>
            <div className={s.label}> 활동 시작일 </div>
            <div className={s.flexBox}>
              <DatePicker
                defaultDate={startDate}
                minDate={minDate}
                maxDate={maxDate}
                underlineStyle={styles.datePicker.underlineStyle}
                inputStyle={styles.datePicker.inputStyle}
                style={styles.datePicker.style}
                textFieldStyle={styles.datePicker.textFieldStyle}
                onChange={this.handleStartDateChange}
                id="startDate"
              />
            </div>
          </div>

          <Divider style={styles.divider}/>

          <div className={s.label}> 활동 요일 </div>
          <WeekdayCheckBox onChange={this.handleWeekdayCheckBoxChange} checked={weekdayItems}/>

          <Divider style={styles.divider}/>

          <TimeRangePicker onChange={this.handleTimeRangeChange} {...timeRange}/>

          <Divider style={styles.divider}/>

          <div> 요일별로 시간과 장소를 조정할 수 있습니다. </div>

          <WeekdayTimeLocationPicker weekdayItems={weekdayItems} onChange={this.handleWeekdayTimeLocationChange} availableLocation={availableLocation}/>

          <Divider style={styles.divider}/>

          <div className={s.repeatOption}>
            <div className={s.label}> 반복 설정 </div>
            <div className={s.contents}>
              <RadioButtonGroup name="repeat" onChange={this.handleRepeatChange} defaultSelected={defaultRepeat} valueSelected={repeat}>
                <RadioButton
                  value={7}
                  label="반복 없음"
                />
                <RadioButton
                  value={30}
                  label="1달간 반복"
                />
                <RadioButton
                  value={60}
                  label="2달간 반복"
                />
              </RadioButtonGroup>
            </div>
          </div>

          <button className={s.btnSave} onClick={this.handleSaveClick}> 저장하기 </button>

          <div className={s.void}> </div>
        </div>
      </div>
    );
  }
}

DialogSitterScheduleAdd.propTypes = {
  style: PropTypes.object,
  weekdayItems: PropTypes.arrayOf(
    PropTypes.oneOfType([
      PropTypes.bool,
      PropTypes.shape({
        startTime: PropTypes.instanceOf(Date),
        endTime: PropTypes.instanceOf(Date),
        location: PropTypes.string
      })
    ])
  ),
  timeRange: PropTypes.shape({
    startTime: PropTypes.instanceOf(Date),
    endTime: PropTypes.instanceOf(Date),
  }),
  repeat: PropTypes.number,
  defaultStartDate: PropTypes.instanceOf(Date),
  defaultRepeat: PropTypes.number,
  availableLocation: PropTypes.arrayOf(PropTypes.string),
  onSave: PropTypes.func,
  onClose: PropTypes.func,
};

DialogSitterScheduleAdd.defaultProps = {
  style: {padding: '10px'},
  weekdayItems: [false, false, false, false, false, false, false],
  timeRange: {
    startTime: new Date(),
    endTime: new Date(),
  },
  repeat: 30,
  defaultStartDate: moment().add(7, 'days').toDate(),
  defaultRepeat: 30,
  availableLocation: [],
  onSave: () => {},
  onClose: () => {},
};

export default withStyles(s)(DialogSitterScheduleAdd);
