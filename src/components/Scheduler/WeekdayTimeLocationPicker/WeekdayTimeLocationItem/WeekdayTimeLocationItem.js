/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './WeekdayTimeLocationItem.css';

import TimePicker from 'material-ui/TimePicker';
import Place from 'material-ui/svg-icons/maps/place';

class WeekdayTimeLocationItem extends React.Component {
  constructor(props) {
    super(props);
  }

  getStyles = () => {
    return {
      timePicker: {
        style: {width: '90%', margin: 'auto', textAlign: 'center'},
        textFieldStyle: {width: '90%'},
        inputStyle: {textAlign: 'center', color: '#a6a6a6'},
        underlineStyle: {display: 'none'},
      },
      iconStyle: {color: '#a6a6a6'}
    };
  };

  onChange = (event, value) => {
    this.props.onChange(event, value);
  };

  handleLocationClick = () => {
    this.props.onLocationClick(null, this.props.index);
  };

  handleStartTimeChange = (event, startTime) => {
    const {index, endTime, location} = this.props;
    this.props.onChange(event, {index, startTime, endTime, location});
  };

  handleEndTimeChange = (event, endTime) => {
    const {index, startTime, location} = this.props;
    this.props.onChange(event, {index, startTime, endTime, location});
  };

  render() {
    const styles = this.getStyles();
    const {startTime, endTime, label, location, locationPicker} = this.props;

    return (
      <div className={s.root}>
        <div className={s.container}>

          <div className={s.flexContainer}>
            <div className={s.label}> {label} </div>
            <div className={s.contents}>
              <div className={s.duration}>
                <div className={s.startTime}>
                  <TimePicker
                    id="startTime"
                    autoOk={true}
                    okLabel="확인"
                    cancelLabel="취소"
                    style={styles.timePicker.style}
                    textFieldStyle={styles.timePicker.textFieldStyle}
                    inputStyle={styles.timePicker.inputStyle}
                    underlineStyle={styles.timePicker.underlineStyle}
                    value={startTime}
                    onChange={this.handleStartTimeChange}
                  />
                </div>
                <div className={s.endTime}>
                  <TimePicker
                    id="endTime"
                    autoOk={true}
                    okLabel="확인"
                    cancelLabel="취소"
                    style={styles.timePicker.style}
                    textFieldStyle={styles.timePicker.textFieldStyle}
                    inputStyle={styles.timePicker.inputStyle}
                    underlineStyle={styles.timePicker.underlineStyle}
                    value={endTime}
                    onChange={this.handleEndTimeChange}
                  />
                </div>
              </div>

              {
                locationPicker &&
                <div className={s.location} onClick={this.handleLocationClick}>
                  <Place style={styles.iconStyle} />
                  {location || '가능한 지역을 선택해주세요.'}
                </div>
              }

            </div>
          </div>

        </div>
      </div>
    );
  }
}

WeekdayTimeLocationItem.propTypes = {
  label: PropTypes.string.isRequired,
  startTime: PropTypes.instanceOf(Date).isRequired,
  endTime: PropTypes.instanceOf(Date).isRequired,
  location: PropTypes.string,
  index: PropTypes.number,
  locationPicker: PropTypes.bool,
  onLocationClick: PropTypes.func,
  onChange: PropTypes.func,
};

WeekdayTimeLocationItem.defaultProps = {
  index: -1,
  locationPicker: true,
  onLocationClick: () => {},
  onChange: () => {},
};

export default withStyles(s)(WeekdayTimeLocationItem);
