/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './WeekdayLocationPicker.css';

import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';

class WeekdayLocationPicker extends React.Component {
  constructor(props) {
    super(props);
  }

  getStyles = () => {
    return {
      container: {
        display: this.props.show ? 'block' : 'none',
        position: 'fixed',
        top: 0,
        left: 0,
        width: '100%',
        height: '100vh',
        backgroundColor: 'rgba(0, 0, 0, 0.6)',
        zIndex: 9,
        padding: '10px 0',
      },
      radioButtonGroup: {
        padding: 20,
      },
    }
  };

  getAvailableLocationRender = () => {
    const {availableLocation} = this.props, render = [];
    for (let item of availableLocation) {
      render.push(
        <RadioButton key={item} value={item} label={item}/>
      );
    }
    return render;
  };

  handleSelect = (event, value) => {
    this.props.onSelect(event, value);
  };

  render() {
    const styles = this.getStyles();
    const availableLocationRender = this.getAvailableLocationRender();

    return (
      <div className={s.root}>
        <div className={s.container} style={styles.container} onClick={this.props.onDismiss}>
          <div className={s.locationContainer}>
            <div className={s.head}> 활동 가능지역 선택 </div>
            <hr className={s.hr}/>

            <RadioButtonGroup name="location" style={styles.radioButtonGroup} onChange={this.handleSelect} valueSelected="NONE">
              <RadioButton
                value="내 지역"
                label="내 지역 모두 가능"
              />
              {availableLocationRender}
            </RadioButtonGroup>
          </div>
        </div>
      </div>
    );
  }
}

WeekdayLocationPicker.propTypes = {
  availableLocation: PropTypes.arrayOf(PropTypes.string),
  show: PropTypes.bool,
  onSelect: PropTypes.func,
  onDismiss: PropTypes.func,
};

WeekdayLocationPicker.defaultProps = {
  availableLocation: [],
  show: false,
  onSelect: () => {},
  onDismiss: () => {},
};

export default withStyles(s)(WeekdayLocationPicker);
