/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './WeekdayTimeLocationPicker.css';

import WeekdayTimeLocationItem from './WeekdayTimeLocationItem';
import WeekdayLocationPicker from './WeekdayLocationPicker';

class WeekdayTimeLocationPicker extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showWeekdayLocationPicker: false,
      currentTargetItem: -1,
    };
  }

  getWeekdayTimeLocationItemRender = () => {
    const weekday = ['월', '화', '수', '목', '금', '토', '일'], render = [];
    const {weekdayItems, locationPicker} = this.props, defaultTime = new Date();
    let i = 0;
    for (let item of weekdayItems) {
      if (item) {
        render.push(
          <WeekdayTimeLocationItem
            key={i}
            index={i}
            label={weekday[i]}
            startTime={item.startTime || defaultTime}
            endTime={item.endTime || defaultTime}
            location={item.location}
            locationPicker={locationPicker}
            onLocationClick={this.handleWeekdayLocationClick}
            onChange={this.props.onChange}
          />
        );
      }
      i++;
    }
    return render;
  };

  handleWeekdayLocationClick = (event, value) => {
    this.setState({
      currentTargetItem: value,
      showLocationPicker: true,
    });
  };

  handleWeekdayLocationSelect = (event, value) => {
    const index = this.state.currentTargetItem;
    const {startTime, endTime} = this.props.weekdayItems[index];
    this.setState({
      showLocationPicker: false,
    });
    this.props.onChange(event, {
      index, startTime, endTime,
      location: value
    });
  };

  handleWeekdayLocationDismiss = () => {
    this.setState({
      showLocationPicker: false,
    });
  };

  render() {
    const {availableLocation, locationPicker} = this.props;
    const weekdayTimeLocationItemRender = this.getWeekdayTimeLocationItemRender();

    return (
      <div className={s.root}>
        <div className={s.container}>

          {weekdayTimeLocationItemRender}

          {
            locationPicker &&
            <WeekdayLocationPicker
              show={this.state.showLocationPicker}
              availableLocation={availableLocation}
              onSelect={this.handleWeekdayLocationSelect}
              onDismiss={this.handleWeekdayLocationDismiss}
            />
          }

        </div>
      </div>
    );
  }
}

WeekdayTimeLocationPicker.propTypes = {
  weekdayItems: PropTypes.arrayOf(
    PropTypes.oneOfType([
      PropTypes.bool,
      PropTypes.shape({
        startTime: PropTypes.instanceOf(Date),
        endTime: PropTypes.instanceOf(Date),
        location: PropTypes.string
      })
    ])
  ),
  showWeekdayLocationPicker: PropTypes.bool,
  availableLocation: PropTypes.arrayOf(PropTypes.string),
  locationPicker: PropTypes.bool,
  onChange: PropTypes.func,
};

WeekdayTimeLocationPicker.defaultProps = {
  weekdayItems: [],
  showWeekdayLocationPicker: false,
  availableLocation: [],
  locationPicker: true,
  onChange: () => {},
};

export default withStyles(s)(WeekdayTimeLocationPicker);
