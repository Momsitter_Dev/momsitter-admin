/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './DateRow.css';

import moment from 'moment';

import DateCell from '../DateCell';

class DateRow extends React.Component {
  constructor(props) {
    super(props);
  }

  getDateCellRender = () => {
    const {duration, unitWidth} = this.props;
    const baseDate = moment(), render = [];

    for (let i = 0; i < duration; i++) {
      render.push(
        <DateCell key={i} date={baseDate.clone()} unitWidth={unitWidth}/>
      );
      baseDate.add(1, 'day');
    }

    return render;
  };

  render() {
    const dateCellRender = this.getDateCellRender();

    return (
      <div className={s.root}>
        <div className={s.container}>

          {dateCellRender}

        </div>
      </div>
    );
  }
}

DateRow.propTypes = {
  duration: PropTypes.number,
  unitWidth: PropTypes.number,
};

DateRow.defaultProps = {
  duration: 60,
  unitWidth: 50,
};

export default withStyles(s)(DateRow);
