/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './DateTimeItem.css';

import TimePicker from 'material-ui/TimePicker';

class DateTimeItem extends React.Component {
  constructor(props) {
    super(props);
  }

  onStartTimeChange = (event, value) => {
    this.props.onChange(event, {
      ...this.props,
      startTime: value,
    });
  };

  onEndTimeChange = (event, value) => {
    this.end = value;
    this.props.onChange(event, {
      ...this.props,
      endTime: value,
    });
  };

  render() {
    return (
      <div className={s.wrap} style={this.props.style}>
        <div className={s.label}>{this.props.label}</div>
        <TimePicker
          id={`dateTimeItemStart-${this.props.index}`}
          value={this.props.startTime}
          style={{display: 'inline-flex', maxWidth: '100px'}}
          inputStyle={{textAlign: 'center', color: '#a6a6a6'}}
          underlineShow={false}
          onChange={this.onStartTimeChange}
          okLabel="확인"
          cancelLabel="취소"
        />
        <div className={s.separator}> | </div>
        <TimePicker
          id={`dateTimeItemEnd-${this.props.index}`}
          value={this.props.endTime}
          style={{display: 'inline-flex', maxWidth: '100px'}}
          inputStyle={{textAlign: 'center', color: '#a6a6a6'}}
          underlineShow={false}
          onChange={this.onEndTimeChange}
          okLabel="확인"
          cancelLabel="취소"
        />
      </div>
    );
  }
}

DateTimeItem.propTypes = {
  label: PropTypes.string.isRequired,
  startTime: PropTypes.instanceOf(Date).isRequired,
  endTime: PropTypes.instanceOf(Date).isRequired,
  style: PropTypes.object,
  index: PropTypes.number,
  onChange: PropTypes.func,
};

DateTimeItem.defaultProps = {
  style: {},
  index: -1,
  onChange: () => {},
};

export default withStyles(s)(DateTimeItem);
