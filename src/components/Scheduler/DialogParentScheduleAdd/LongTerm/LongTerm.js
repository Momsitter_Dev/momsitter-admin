/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './LongTerm.css';

import moment from 'moment';
import IconChecked from 'material-ui/svg-icons/toggle/check-box';
import IconUnchecked from 'material-ui/svg-icons/toggle/check-box-outline-blank';
import DatePicker from 'material-ui/DatePicker';

import WeekdayCheckBox from '../../../Scheduler/WeekdayCheckBox';
import TimeRangePicker from '../../../Scheduler/TimeRangePicker';
import WeekdayTimeLocationPicker from '../../../Scheduler/WeekdayTimeLocationPicker';
import CheckboxGroup from '../../../CheckboxGroup/';
import CheckboxItem from '../../../CheckboxGroup/CheckboxItem';

class LongTerm extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      moreThanThreeMonth: false,
      startDate: props.defaultStartDate,
      endDate: props.defaultEndDate,
      weekdayItems: props.weekdayItems,
      timeRange: props.timeRange,
      activeDate: [],
    };
  }

  getStyles = () => {
    return {
      datePicker: {
        style: {
          flex: 1,
          overflow: 'hidden',
          position: 'relative',
        },
        underlineStyle: {
          display: 'none',
        },
        inputStyle: {
          textAlign: 'center',
          color: '#ff7000',
          fontWeight: 'bold',
        },
        textFieldStyle: {
          width: '100%',
        },
        floatingLabelStyle: {
          fontSize: '18px',
          fontWeight: '400',
          color: 'black',
          marginLeft: '15px',
        }
      },
      checkboxItem: {
        style: {
          height: '20px',
          marginBottom: 0,
          padding: '5px',
        },
        iconWrapStyle: {
          marginRight: '10px'
        },
        mainLabelStyle: {
          fontSize: '16px',
          fontWeight: '300',
          color: '#a6a6a6',
        },
      }
    }
  };

  handleStartDateChange = (event, value) => {
    this.setState({startDate: value}, this.handleChange);
  };

  handleEndDateChange = (event, value) => {
    this.setState({endDate: value}, this.handleChange);
  };

  handleMoreThanThreeMonthChange = (event, value) => {
    this.setState({moreThanThreeMonth: value.length > 0}, this.handleChange);
  };

  handleTimeRangeChange = (event, value) => {
    const {weekdayItems} = this.state, newWeekdayItems = [];
    for (let item of weekdayItems) {
      if (item) {
        newWeekdayItems.push({ ...item, ...value});
      } else {
        newWeekdayItems.push(false);
      }
    }
    this.setState({timeRange: value, weekdayItems: newWeekdayItems}, this.handleChange);
  };

  handleWeekdayCheckBoxChange = (event, value) => {
    const {weekdayItems, timeRange} = this.state;
    if (weekdayItems[value]) {
      weekdayItems[value] = false;
    } else {
      weekdayItems[value] = {...timeRange};
    }
    this.setState({weekdayItems}, this.handleChange);
  };

  handleWeekdayTimeLocationChange = (event, value) => {
    const weekdayItems = this.state.weekdayItems;
    weekdayItems[value.index] = value;
    this.setState({weekdayItems}, this.handleChange);
  };

  onDurationNegotiableChange = (event, value) => {
    this.durationNegotiable = value.length > 0;
    this.handleChange();
  };

  onWeekdayNegotiableChange = (event, value) => {
    this.weekdayNegotiable = value.length > 0;
    this.handleChange();
  };

  onTimeNegotiableChange = (event, value) => {
    this.timeNegotiable = value.length > 0;
    this.handleChange();
  };

  handleChange = () => {
    const {weekdayItems, startDate, endDate, moreThanThreeMonth} = this.state;
    this.props.onChange(null, {
      weekdayItems, startDate, endDate, moreThanThreeMonth,
      durationNegotiable: this.durationNegotiable,
      weekdayNegotiable: this.weekdayNegotiable,
      timeNegotiable: this.timeNegotiable,
    });
  };

  render() {
    const {weekdayItems, startDate, endDate, timeRange} = this.state;
    const styles = this.getStyles();
    const minDate = new Date();
    const maxDate = moment(minDate.getTime()).add(100, 'days').toDate();

    return (
      <div>
        <div>
          <div className={s.flexContainer}>
            <DatePicker
              defaultDate={startDate}
              onChange={this.handleStartDateChange}
              minDate={minDate}
              maxDate={maxDate}
              style={styles.datePicker.style}
              underlineStyle={styles.datePicker.underlineStyle}
              inputStyle={styles.datePicker.inputStyle}
              textFieldStyle={styles.datePicker.textFieldStyle}
              floatingLabelStyle={styles.datePicker.floatingLabelStyle}
              floatingLabelText="시작일"
            />
            <DatePicker
              onChange={this.handleEndDateChange}
              minDate={minDate}
              maxDate={maxDate}
              style={styles.datePicker.style}
              underlineStyle={styles.datePicker.underlineStyle}
              inputStyle={styles.datePicker.inputStyle}
              textFieldStyle={styles.datePicker.textFieldStyle}
              floatingLabelStyle={this.state.moreThanThreeMonth ? {...styles.datePicker.floatingLabelStyle, color: '#e1e1e1'} : styles.datePicker.floatingLabelStyle}
              floatingLabelText={this.state.moreThanThreeMonth ? '3개월 이상' : '종료일'}
              disabled={this.state.moreThanThreeMonth}
              defaultDate={this.state.moreThanThreeMonth ? null : endDate}
              value={this.state.moreThanThreeMonth ? null : endDate}
            />
          </div>

          <br/>

          <CheckboxGroup name="term" onChange={this.handleMoreThanThreeMonthChange}>
            <CheckboxItem
              value="true"
              mainLabel="3개월 이상 돌봐주길 바랍니다."
              style={styles.checkboxItem.style}
              iconWrapStyle={styles.checkboxItem.style}
              mainLabelStyle={styles.checkboxItem.mainLabelStyle}
              checkedIcon={(<IconChecked style={{width: '20px', height: '20px'}} color="#ff4500" />)}
              unCheckedIcon={(<IconUnchecked style={{width: '20px', height: '20px'}} color="#a6a6a6" />)}
            />
          </CheckboxGroup>
          <CheckboxGroup name="durationNegotiable" onChange={this.onDurationNegotiableChange}>
            <CheckboxItem
              value="true"
              mainLabel="기간 협의 가능합니다."
              style={styles.checkboxItem.style}
              iconWrapStyle={styles.checkboxItem.style}
              mainLabelStyle={styles.checkboxItem.mainLabelStyle}
              checkedIcon={(<IconChecked style={{width: '20px', height: '20px'}} color="#ff4500" />)}
              unCheckedIcon={(<IconUnchecked style={{width: '20px', height: '20px'}} color="#a6a6a6" />)}
            />
          </CheckboxGroup>
        </div>
        <div>
          <h4 style={{fontSize: '16px', fontWeight: '400'}}> 활동요일 </h4>
          <WeekdayCheckBox onChange={this.handleWeekdayCheckBoxChange} checked={weekdayItems}/>
          <CheckboxGroup name="weekDayNegotiable" onChange={this.onWeekdayNegotiableChange}>
            <CheckboxItem
              value="true"
              mainLabel="요일 협의 가능합니다."
              style={{}}
              iconWrapStyle={styles.checkboxItem.style}
              mainLabelStyle={styles.checkboxItem.mainLabelStyle}
              checkedIcon={(<IconChecked style={{width: '20px', height: '20px'}} color="#ff4500" />)}
              unCheckedIcon={(<IconUnchecked style={{width: '20px', height: '20px'}} color="#a6a6a6" />)}
            />
          </CheckboxGroup>
        </div>

        <div>
          <TimeRangePicker onChange={this.handleTimeRangeChange} {...timeRange}/>
          <CheckboxGroup name="timeNegotiable" onChange={this.onTimeNegotiableChange}>
            <CheckboxItem
              value="true"
              mainLabel="시간 협의 가능합니다."
              style={{}}
              iconWrapStyle={styles.checkboxItem.style}
              mainLabelStyle={styles.checkboxItem.mainLabelStyle}
              checkedIcon={(<IconChecked style={{width: '20px', height: '20px'}} color="#ff4500" />)}
              unCheckedIcon={(<IconUnchecked style={{width: '20px', height: '20px'}} color="#a6a6a6" />)}
            />
          </CheckboxGroup>
        </div>

        <div>
          <h4 style={{fontSize: '16px', fontWeight: '400'}}> 요일별 시간 조정 </h4>
          <WeekdayTimeLocationPicker weekdayItems={weekdayItems} onChange={this.handleWeekdayTimeLocationChange} locationPicker={false}/>
        </div>

      </div>
    );
  }
}

LongTerm.propTypes = {
  weekdayItems: PropTypes.arrayOf(
    PropTypes.oneOfType([
      PropTypes.bool,
      PropTypes.shape({
        startTime: PropTypes.instanceOf(Date),
        endTime: PropTypes.instanceOf(Date),
        location: PropTypes.string
      })
    ])
  ),
  timeRange: PropTypes.shape({
    startTime: PropTypes.instanceOf(Date),
    endTime: PropTypes.instanceOf(Date),
  }),
  defaultStartDate: PropTypes.instanceOf(Date),
  defaultEndDate: PropTypes.instanceOf(Date),
  onChange: PropTypes.func,
};

LongTerm.defaultProps = {
  weekdayItems: [false, false, false, false, false, false, false],
  timeRange: {
    startTime: new Date(),
    endTime: new Date(),
  },
  defaultStartDate: moment().add(7, 'days').toDate(),
  defaultEndDate: moment().add(7, 'days').toDate(),
  onChange: () => {},
};

export default withStyles(s)(LongTerm);
