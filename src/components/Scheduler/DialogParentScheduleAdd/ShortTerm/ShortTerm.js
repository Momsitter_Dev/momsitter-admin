/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ShortTerm.css';
import moment from 'moment';
import CheckBox from 'material-ui/svg-icons/toggle/check-box';
import CheckBoxOutlineBlank from 'material-ui/svg-icons/toggle/check-box-outline-blank';
import DayPicker, { DateUtils } from 'react-day-picker';
import DayPickerStyle from '!!isomorphic-style-loader!css-loader?modules=false!./overwrite.css';
import TimeRangePicker from '../../../Scheduler/TimeRangePicker';
import DateTimeItem from '../DateTImeItem';
import CheckboxGroup from '../../../../components/CheckboxGroup/';
import CheckboxItem from './../../../../components/CheckboxGroup/CheckboxItem/';
import _ from 'lodash';

class ShortTerm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedDays: [],
      timeRange: props.timeRange,
    };
  };

  getDateTimeItemLabel = (day) => {
    day = moment(day.getTime());
    return `${day.get('months') + 1} / ${day.get('date')} ${['일', '월', '화', '수', '목', '금', '토', '일'][day.get('days')]}`;
  };

  setDaysItem = (prevDays, timeRange) => {
    const days = [];
    if (prevDays && prevDays.length > 0 && timeRange && timeRange.startTime && timeRange.endTime) {
      for (let day of prevDays) {
        days.push({
          ...day, ...timeRange,
        });
      }
      this.setState({selectedDays: days, timeRange}, this.onChange);
    }
  };

  handleTimeRangeChange = (event, value) => {
    this.setDaysItem(this.state.selectedDays, value);
  };

  onDayClick = (day, {selected}) => {
    const {selectedDays, timeRange} = this.state;
    if (selected) {
      let selectedIndex = selectedDays.findIndex(dt => DateUtils.isSameDay(dt.day, day));
      selectedDays.splice(selectedIndex, 1);
    } else {
      selectedDays.push({
        day,
        ...timeRange,
      });
    }
    const orderedSelectedDays = _.sortBy(selectedDays, (o) => {
      return moment(o.day);
    });
    this.setState({selectedDays: orderedSelectedDays}, this.onChange);
  };

  handleDateTimeChange = (event, value) => {
    const {index, startTime, endTime} = value;
    const {selectedDays} = this.state;
    selectedDays[index] = {
      ...selectedDays[index],
      startTime, endTime,
    };
    this.setState({selectedDays}, this.onChange);
  };

  onDurationNegotiableChange = (event, value) => {
    this.durationNegotiable = value.length > 0;
    this.onChange();
  };

  onChange = () => {
    const {selectedDays} = this.state, durationNegotiable = this.durationNegotiable;
    this.props.onChange(null, {
      selectedDays, durationNegotiable
    });
  };

  render() {
    const {selectedDays, timeRange} = this.state;
    const selectedDaysRender = [];

    for (let i = 0; i < selectedDays.length; i++) {
      // TODO: rednebula01018) 일자에 따라 정렬해서 렌더링 필요
      selectedDaysRender.push(
        <DateTimeItem
          label={this.getDateTimeItemLabel(selectedDays[i].day)}
          startTime={selectedDays[i].startTime}
          endTime={selectedDays[i].endTime}
          key={i}
          index={i}
          onChange={this.handleDateTimeChange}
        />
      );
    }

    /* NOTE: 체크박스 스타일 */
    const checkBoxItemStyle = {
      height: '20px',
      marginBottom: 0,
      padding: '5px',
    };
    const mainLabelStyle = {
      fontSize: '16px',
      fontWeight: '300',
      color: '#a6a6a6',
    };

    return (
      <div>
        <div>
          {/*TODO: day picker wrapper 콤포넌트를 구현해서 재활용 할 수 있도록 할 것 */}
          <DayPicker
            selectedDays={selectedDays.map(item => item.day )}
            onDayClick={this.onDayClick}
            months={['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']}
            labels={{nextMonth: '다음달', previousMonth: '이전달'}}
            weekdaysShort={['일', '월', '화', '수', '목', '금', '토']}
            enableOutsideDays
          />
        </div>

        <div>
          <TimeRangePicker onChange={this.handleTimeRangeChange} {...timeRange} />
          <CheckboxGroup name="durationNegotiable" onChange={this.onDurationNegotiableChange}>
            <CheckboxItem
              mainLabel="날짜 협의 가능합니다."
              style={checkBoxItemStyle}
              iconWrapStyle={{marginRight: '10px'}}
              mainLabelStyle={mainLabelStyle}
              value={true}
              checkedIcon={(<CheckBox style={{width: '20px', height: '20px', color: '#ff4500'}} />)}
              unCheckedIcon={(<CheckBoxOutlineBlank style={{width: '20px', height: '20px'}} />)}
            />
          </CheckboxGroup>
        </div>

        <div>
          <h4 style={{fontSize: '16px', fontWeight: '400'}}> 요일별 시간 조정 </h4>
          {selectedDaysRender}
        </div>
      </div>
    );
  }
}

ShortTerm.propTypes = {
  timeRange: PropTypes.shape({
    startTime: PropTypes.instanceOf(Date),
    endTime: PropTypes.instanceOf(Date),
  }),
  onChange: PropTypes.func,
};

ShortTerm.defaultProps = {
  timeRange: {
    startTime: new Date(),
    endTime: new Date(),
  },
  onChange: () => {},
};

export default withStyles(s, DayPickerStyle)(ShortTerm);
