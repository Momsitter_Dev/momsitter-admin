/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */
import moment from 'moment';
import Divider from 'material-ui/Divider';
import RadioButtonChecked from 'material-ui/svg-icons/toggle/radio-button-checked';
import RadioButtonUnChecked from 'material-ui/svg-icons/toggle/radio-button-unchecked';
import IconButton from 'material-ui/IconButton';
import Close from 'material-ui/svg-icons/navigation/close';
import { grey600, grey400 } from 'material-ui/styles/colors';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './DialogParentScheduleAdd.css';
import ShortTerm from './ShortTerm/';
import LongTerm from './LongTerm/';
import RadioGroup from '../../RadioGroup/';
import RadioItem from '../../RadioGroup/RadioItem';
import baseSunToBaseMonMoment from '../../../util/schdule/baseSunToBaseMonMoment';

class DialogParentScheduleAdd extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      schedules: null,
      weekdayItems: props.weekdayItems,
      timeRange: props.timeRange,
      repeat: props.repeat,
      startDate: props.defaultStartDate,
      term: 'long',
      durationNegotiable: false,
      weekdayNegotiable: false,
      timeNegotiable: false,
      moreThanThreeMonth: false,
      isScheduleValid: false,
    };
  }

  isSchedulesValid = (schedules) => {
    if (schedules && schedules.length > 0) {
      for (const item of schedules) {
        if (!(item.scheduleDate && item.scheduleStartTime && item.scheduleEndTime)) return false;
      }
      return true;
    }
    return false;
  };

  generateLongTermSchedule = (startDate, endDate, weekdayItems, moreThanThreeMonth) => {
    const schedules = [];
    if (startDate && weekdayItems) {
      try {
        let i = 0;
        const base = moment(startDate.getTime());
        const end = moreThanThreeMonth ? moment(base).add(90, 'days') : moment(endDate.getTime());

        const days = moment(end).diff(base, 'days') + 1;

        // TODO: Scheduler#generateSchedules 함수와의 중복코드 삭제
        while (i++ < days) {
          const weekday = baseSunToBaseMonMoment(base);
          if (weekdayItems[weekday]) {
            const { startTime, endTime } = weekdayItems[weekday];
            schedules.push({
              scheduleDate: base.toDate(),
              scheduleStartTime: moment(startTime.getTime()).format('HH:mm'),
              scheduleEndTime: moment(endTime.getTime()).format('HH:mm'),
            });
          }
          base.add(1, 'days');
        }
        return schedules;
      } catch (err) {
        console.error(err);
        return null;
      }
    } else {
      return null;
    }
  };

  generateShortTermSchedule = (data) => {
    const schedules = [];
    try {
      if (data && data.length > 0) {
        for (const item of data) {
          schedules.push({
            scheduleDate: item.day,
            scheduleStartTime: moment(item.startTime.getTime()).format('HH:mm'),
            scheduleEndTime: moment(item.endTime.getTime()).format('HH:mm'),
          });
        }
      }
      return schedules;
    } catch (err) {
      console.error(err);
      return null;
    }
  };

  handleTermChange = (event, value) => {
    if (this.state.term !== value.value) {
      this.setState({ term: value.value }, () => this.handleChange(event, {}));
    }
  };

  handleChange = (event, value) => {
    // TODO: 예외처리, 종료일이 시작일 앞, 시작일이 종료일 뒤, 시작시간이 종료시간 뒤, 종료시간이 시작시간 뒤
    let schedules;

    if (this.state.term === 'long') {
      schedules = this.generateLongTermSchedule(value.startDate, value.endDate, value.weekdayItems, value.moreThanThreeMonth);
      this.setState({
        schedules,
        term: this.state.term,
        durationNegotiable: value.durationNegotiable,
        weekdayNegotiable: value.weekdayNegotiable,
        timeNegotiable: value.timeNegotiable,
        moreThanThreeMonth: value.moreThanThreeMonth,
        isScheduleValid: this.isSchedulesValid(schedules),
      });
    } else {
      schedules = this.generateShortTermSchedule(value.selectedDays);
      this.setState({
        schedules,
        term: this.state.term,
        durationNegotiable: value.durationNegotiable,
        weekdayNegotiable: false,
        timeNegotiable: false,
        moreThanThreeMonth: false,
        isScheduleValid: this.isSchedulesValid(schedules),
      });
    }
  };

  getStyles = () => ({
    datePicker: {
      underlineStyle: { display: 'none' },
      inputStyle: { textAlign: 'right', color: '#ff7000', fontWeight: 'bold', marginRight: '50px' },
      style: { width: '100%' },
      textFieldStyle: { width: '100%' },
    },
    IconButton: { position: 'absolute', left: 11, top: 11 },
    divider: {
      marginTop: 12,
      marginBottom: 12,
      backgroundColor: '#f0f0f0',
    },
  });

  handleSaveClick = () => {
    const {
      schedules,
      term,
      moreThanThreeMonth,
      durationNegotiable,
      weekdayNegotiable,
      timeNegotiable,
    } = this.state;
    this.props.onSave(null, {
      schedules,
      term,
      durationNegotiable,
      weekdayNegotiable,
      timeNegotiable,
      moreThanThreeMonth,
      isScheduleValid: this.isSchedulesValid(schedules),
    });
  };

  render() {
    const { style } = this.props;

    const styles = this.getStyles();

    let termUI = null;

    if (this.state.term === 'long') {
      termUI = (<LongTerm onChange={this.handleChange} />);
    } else if (this.state.term === 'short') {
      termUI = (<ShortTerm onChange={this.handleChange} />);
    }

    const radioItemStyle = {
      itemStyle: { display: 'inline-flex' },
      containerStyle: { position: 'relative', margin: '0 auto' },
      iconWrapStyle: { marginTop: '15px', marginRight: '10px' },
      mainLabelStyle: { fontSize: '30px', fontWeight: '500', color: '#c8c8c8' },
      descriptionStyle: { fontSize: '16px', color: '#c8c8c8', fontWeight: '400' },
      activeMainLabelStyle: { color: '#000' },
      activeDescriptionStyle: { color: '#000' },
    };

    const radioButtonChecked = (<RadioButtonChecked color="#ff4500" />);
    const radioButtonUnChecked = (<RadioButtonUnChecked color="#c8c8c8" />);

    return (
      <div className={s.root}>
        <div className={s.container} style={style}>

          <IconButton style={styles.IconButton} onClick={this.props.onClose}>
            <Close color={grey600} />
          </IconButton>

          <h3 className={s.title}> 활동 일정 추가 </h3>
          <div>
            <RadioGroup name="term" onChange={this.handleTermChange} defaultSelected={this.state.term} style={{ display: 'flex', justifyContent: 'space-between' }}>
              <RadioItem
                value="long"
                mainLabel="장기"
                description="1개월 이상"
                style={radioItemStyle.itemStyle}
                containerStyle={radioItemStyle.containerStyle}
                iconWrapStyle={radioItemStyle.iconWrapStyle}
                mainLabelStyle={radioItemStyle.mainLabelStyle}
                descriptionStyle={radioItemStyle.descriptionStyle}
                activeMainLabelStyle={radioItemStyle.activeMainLabelStyle}
                activeDescriptionStyle={radioItemStyle.activeDescriptionStyle}
                checkedIcon={radioButtonChecked}
                unCheckedIcon={radioButtonUnChecked}
                checked
              />
              <RadioItem
                value="short"
                mainLabel="단기"
                description="1개월 미만"
                style={radioItemStyle.itemStyle}
                containerStyle={radioItemStyle.containerStyle}
                iconWrapStyle={radioItemStyle.iconWrapStyle}
                mainLabelStyle={radioItemStyle.mainLabelStyle}
                descriptionStyle={radioItemStyle.descriptionStyle}
                activeMainLabelStyle={radioItemStyle.activeMainLabelStyle}
                activeDescriptionStyle={radioItemStyle.activeDescriptionStyle}
                checkedIcon={radioButtonChecked}
                unCheckedIcon={radioButtonUnChecked}
              />
            </RadioGroup>
          </div>
          <Divider style={{ backgroundColor: '#f0f0f0', margin: '14px 0' }} />
          {termUI}

          <button className={s.btnSave} onClick={this.handleSaveClick}> 저장하기 </button>

          <div className={s.void} />
        </div>
      </div>
    );
  }
}

DialogParentScheduleAdd.propTypes = {
  style: PropTypes.object,
  onSave: PropTypes.func,
  onClose: PropTypes.func,
};

DialogParentScheduleAdd.defaultProps = {
  style: { padding: '10px' },
  onSave: () => {},
  onClose: () => {},
};

export default withStyles(s)(DialogParentScheduleAdd);
