/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ScheduleCellContainer.css';
import Moment from 'moment';
import { extendMoment } from 'moment-range';

const moment = extendMoment(Moment);
class ScheduleCellContainer extends React.Component {
  constructor(props) {
    super(props);
  }

  getStyles = () => {
    const {startTime, endTime, unitWidth, duration, unitHeight, unitTime} = this.props;
    const start = moment(startTime, 'HH:mm'), end = moment(endTime, 'HH:mm');
    const timeDiff = end.diff(start, 'minutes') + unitTime;

    return {
      container: {
        width: duration * unitWidth,
        height: (timeDiff / unitTime) * unitHeight,
      }
    }
  };

  getScheduleCellStyle = (scheduleDate, scheduleStartTime, scheduleEndTime) => {
    const {unitHeight, unitWidth, unitTime, startTime} = this.props;
    const aDayMinutes = 1440;
    const heightPerTimeRatio = unitHeight / unitTime;
    const start = moment(startTime, 'HH:mm');

    let baseDate = moment(this.props.baseDate.getTime());
    let width = this.props.unitWidth - 2;
    let height = heightPerTimeRatio * scheduleEndTime.diff(scheduleStartTime, 'minutes');
    let left =  scheduleDate.format('YYYY-MM-DD') === baseDate.format('YYYY-MM-DD') ? (scheduleDate.diff(baseDate, 'day')) * unitWidth : (scheduleDate.diff(baseDate, 'day') + 1) * unitWidth;
    let top = (scheduleStartTime.diff(start, 'minutes') % aDayMinutes) * heightPerTimeRatio;

    return {width, height, left, top};
  };

  getScheduleRender = () => {
    const {schedules, duration} = this.props, render = [];
    const baseDate = moment(this.props.baseDate.getTime()).subtract(1, 'days');
    const range = moment.range(baseDate, baseDate.clone().add(duration, 'day'));

    let index = 0;
    for (let item of schedules) {
      let scheduleDate = moment(moment(item.scheduleDate.getTime()).format('YYYY-MM-DD'), 'YYYY-MM-DD');
      let scheduleStartTime = moment(item.scheduleStartTime, 'HH:mm'), scheduleEndTime = moment(item.scheduleEndTime, 'HH:mm');

      if (range.contains(scheduleDate)) {
        const key = index;
        render.push(
          <div
            key={key}
            className={s.scheduleCell}
            style={this.getScheduleCellStyle(scheduleDate, scheduleStartTime, scheduleEndTime)}
            onClick={(event) => this.props.onScheduleClick(event, key)}
          >
            <div> {item.scheduleStartTime} </div>
            <div> {item.scheduleEndTime} </div>
            <br/>
            <div> {item.scheduleLocation} </div>
          </div>
        );
      }
      index++;
    }

    return render;
  };

  render() {
    const styles = this.getStyles();
    const scheduleRender = this.getScheduleRender();

    return (
      <div className={s.root}>
        <div className={s.container} style={styles.container}>
          {scheduleRender}
        </div>
      </div>
    );
  }
}

ScheduleCellContainer.propTypes = {
  schedules: PropTypes.arrayOf(PropTypes.shape({
    scheduleDate: PropTypes.instanceOf(Date),
    scheduleStartTime: PropTypes.string,
    scheduleEndTime: PropTypes.string,
    scheduleLocation: PropTypes.string,
  })),
  startTime: PropTypes.string,
  endTime: PropTypes.string,
  unitTime: PropTypes.number,
  unitHeight: PropTypes.number,
  unitWidth: PropTypes.number,
  duration: PropTypes.number,
  baseDate: PropTypes.instanceOf(Date),
  onScheduleClick: PropTypes.func,
};

ScheduleCellContainer.defaultProps = {
  schedules: [],
  startTime: '00:00',
  endTime: '24:00',
  unitTime: 120,
  unitHeight: 30,
  unitWidth: 50,
  duration: 60,
  baseDate: new Date(),
  onScheduleClick: () => {},
};

export default withStyles(s)(ScheduleCellContainer);
