/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './DateCell.css';

import moment from 'moment';

moment.locale('ko');
class DateCell extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {date, unitWidth, rootStyle} = this.props;

    return (
    <div className={s.root} style={{...rootStyle, paddingLeft: unitWidth / 2, paddingRight: unitWidth / 2}}>
      <div className={s.container}>
        <div className={s.dayOfWeek}>
          {date.format('dd')}
        </div>
        <div className={s.dayOfMonth}>
          {date.format('MM/D')}
        </div>
      </div>
    </div>
    );
  }
}

DateCell.propTypes = {
  date: PropTypes.instanceOf(moment).isRequired,
  rootStyle: PropTypes.object,
  unitWidth: PropTypes.number,
};

DateCell.defaultProps = {
  rootStyle: {},
  unitWidth: 50,
};

export default withStyles(s)(DateCell);
