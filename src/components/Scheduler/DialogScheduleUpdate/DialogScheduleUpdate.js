/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './DialogScheduleUpdate.css';

import moment from 'moment';

import FlatButton from 'material-ui/FlatButton';
import TimePicker from 'material-ui/TimePicker';
import Delete from 'material-ui/svg-icons/action/delete';
import {grey400, grey600} from 'material-ui/styles/colors';
import IconButton from 'material-ui/IconButton';
import Close from 'material-ui/svg-icons/navigation/close';

moment.locale('ko');
class DialogScheduleUpdate extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ...this.parseProps(props)
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      ...this.parseProps(nextProps)
    });
  }

  parseProps = (props) => {
    return {
      scheduleDate: props.scheduleDate,
      scheduleStartTime: moment(props.scheduleStartTime, 'HH:mm').toDate(),
      scheduleEndTime: moment(props.scheduleEndTime, 'HH:mm').toDate(),
      target: props.target,
    };
  };

  getStyles = () => {
    return {
      timePicker: {
        style: {width: '100%'},
        textFieldStyle: {width: '100%'},
        inputStyle: {
          textAlign: 'right',
          fontSize: '20px',
          color: '#a6a6a6',
        },
        underlineStyle: {display: 'none'},
      },
      saveButton: {
        labelStyle: {
          fontSize: '18px',
          fontWeight: 'bold',
          color: 'white',
          verticalAlign: 'top',
        },
        style: {
          backgroundImage: 'linear-gradient(to left, #FE6368, #ff6700)',
          border: 'none',
          height: 50,
          borderRadius: 5,
        },
      },
      deleteButton: {
        labelStyle: {
          fontSize: '15px',
          verticalAlign: 'top',
          fontWeight: 400,
          color: '#828282',
        },
        style: {
          margin: '0 10%',
          display: 'block',
          marginBottom: '15px',
        },
      },
      iconStyle: {position: 'absolute', left: 11, top: 11},
    }
  };

  handleScheduleStartTime = (event, value) => {
    this.setState({
      scheduleStartTime: value,
    });
  };

  handleScheduleEndTime = (event, value) => {
    this.setState({
      scheduleEndTime: value,
    });
  };

  handleScheduleSave = (event) => {
    const {scheduleDate, scheduleStartTime, scheduleEndTime, target} = this.state;
    this.props.onSave(event, {
      target, scheduleDate,
      scheduleStartTime: moment(scheduleStartTime.getTime()).format('HH:mm'),
      scheduleEndTime: moment(scheduleEndTime.getTime()).format('HH:mm'),
    });
  };

  handleScheduleDelete = (event) => {
    this.props.onDelete(event, this.state.target);
  };

  render() {
    const styles = this.getStyles();
    const {style} = this.props;
    const {scheduleDate, scheduleStartTime, scheduleEndTime} = this.state;
    let title = '';
    if (scheduleDate) {
      title = moment(scheduleDate.getTime()).format('M월 D일, dd요일');
    }

    return (
      <div className={s.root}>
        <div className={s.container} style={style}>
          <IconButton style={styles.iconStyle} onClick={this.props.onClose}>
            <Close color={grey600}/>
          </IconButton>

          <div className={s.title}> {title} </div>

          <hr className={s.hr}/>

          <div className={s.flexContainer}>
            <div className={s.flexBox}>
              <span className={s.timeLabel}> 시작시간 </span>
            </div>
            <div className={s.flexBox}>
              <TimePicker
                id="scheduleStartTime"
                style={styles.timePicker.style}
                textFieldStyle={styles.timePicker.textFieldStyle}
                inputStyle={styles.timePicker.inputStyle}
                underlineStyle={styles.timePicker.underlineStyle}
                value={scheduleStartTime}
                onChange={this.handleScheduleStartTime}
              />
            </div>
          </div>

          <hr className={s.hr}/>

          <div className={s.flexContainer}>
            <div className={s.flexBox}>
              <span className={s.timeLabel}> 종료시간 </span>
            </div>
            <div className={s.flexBox}>
              <TimePicker
                id="scheduleEndTime"
                style={styles.timePicker.style}
                textFieldStyle={styles.timePicker.textFieldStyle}
                inputStyle={styles.timePicker.inputStyle}
                underlineStyle={styles.timePicker.underlineStyle}
                value={scheduleEndTime}
                onChange={this.handleScheduleEndTime}
              />
            </div>
          </div>

          <hr className={s.hr}/>

          <FlatButton
            label="이 일정 삭제하기"
            icon={<Delete color={grey400}/>}
            labelStyle={styles.deleteButton.labelStyle}
            style={styles.deleteButton.style}
            onClick={this.handleScheduleDelete}
          />

          <FlatButton
            label="저장"
            fullWidth={true}
            labelStyle={styles.saveButton.labelStyle}
            style={styles.saveButton.style}
            onClick={this.handleScheduleSave}
          />
        </div>
      </div>
    );
  }
}

DialogScheduleUpdate.propTypes = {
  style: PropTypes.object,
  scheduleDate: PropTypes.instanceOf(Date),
  scheduleStartTime: PropTypes.string,
  scheduleEndTime: PropTypes.string,
  target: PropTypes.number,
  onClose: PropTypes.func,
  onSave: PropTypes.func,
  onDelete: PropTypes.func,
};

DialogScheduleUpdate.defaultProps = {
  style: {},
  scheduleDate: new Date(),
  scheduleStartTime: '00:00',
  scheduleEndTime: '00:00',
  target: -1,
  onSave: () => {},
  onClose: () => {},
  onDelete: () => {},
};

export default withStyles(s)(DialogScheduleUpdate);
