import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './InputField.css';

const propTypes = {
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  onChange: PropTypes.func.isRequired,
  onKeyPress: PropTypes.func,
  disabled: PropTypes.bool,
  style: PropTypes.object,
};

const defaultProps = {
  onKeyPress: null,
  disabled: false,
  value: '',
  type: 'text',
  style: {},
};

class InputField extends React.Component {
  constructor(props) {
    super(props);
    // TODO: Input Field As UnControlled component
    this.input = null;
  }

  onChange = (event) => {
    this.props.onChange(event.target.value);
  };

  onKeyPress = (event) => {
    if (this.props.onKeyPress instanceof Function) {
      this.props.onKeyPress(event);
    }
  };

  render() {
    const { value, type, disabled, style } = this.props;
    return (
      <input
        ref={(r) => { this.input = r; }}
        type={type}
        value={value}
        disabled={disabled}
        onChange={this.onChange}
        className={s.inputField}
        onKeyPress={this.onKeyPress}
        style={style}
      />
    );
  }
}

InputField.propTypes = propTypes;
InputField.defaultProps = defaultProps;

export default withStyles(s)(InputField);
