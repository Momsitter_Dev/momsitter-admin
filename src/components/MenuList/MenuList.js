import { compose } from 'recompose';
import React from 'react';
import { withStyles as withMaterialStyles, List } from '@material-ui/core';
import {
  Face as FaceIcon,
  Group as GroupIcon,
  Send as SendIcon,
  RateReview as RateReviewIcon,
  Feedback as FeedbackIcon,
  Wallpaper as WallpaperIcon,
  Assignment as AssignmentIcon,
  Dashboard as DashboardIcon,
  CardGiftcard as CardGiftcardIcon,
  Loyalty as LoyaltyIcon,
  PersonAddDisabled as PersonAddDisabledIcon,
  AttachMoney as AttachMoneyIcon,
  Payment as PaymentIcon,
  EventNote as EventNoteIcon,
  Report as ReportIcon,
  VerifiedUser as VerifiedUserIcon,
  ShoppingCart as ShoppingCartIcon,
} from '@material-ui/icons';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './MenuList.css';
import MenuListItem from '../MenuListItem';

const styles = theme => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
});

const propTypes = {
  classes: PropTypes.object.isRequired,
};
const defaultProps = {};

class MenuList extends React.Component {
  renderUserManageList = () => {
    const menus = [
      {
        label: '사용자 개인 관리',
        link: '/users',
        icon: <FaceIcon />,
      },
    ];
    return menus.map((item, index) => (
      <MenuListItem key={`user-manage-list-item-${index}`} icon={item.icon} link={item.link} label={item.label} />
    ));
  };

  renderAllUserManageList = () => {
    const menus = [
      {
        label: '부모회원 관리',
        link: '/manage/parents',
        icon: <GroupIcon />,
      },
      {
        label: '시터회원 관리',
        link: '/manage/sitters',
        icon: <GroupIcon />,
      },
      {
        label: '[부모 -> 시터] 신청 내역',
        link: '/manage/parents/applications',
        icon: <SendIcon />,
      },
      {
        label: '[시터 -> 부모] 지원 내역',
        link: '/manage/sitters/applications',
        icon: <SendIcon />,
      },
      {
        label: '후기내역',
        link: '/manage/user/reviews',
        icon: <RateReviewIcon />,
      },
      {
        label: '신고내역',
        link: '/manage/user/reports',
        icon: <ReportIcon />,
      },
      {
        label: '메모내역',
        link: '/manage/user/memos',
        icon: <EventNoteIcon />,
      },
      {
        label: '인증내역',
        link: '/manage/user/certifications',
        icon: <VerifiedUserIcon />,
      },
      {
        label: '이용권 내역',
        link: '/manage/user/products',
        icon: <AssignmentIcon />,
      },
      {
        label: '결제내역',
        link: '/manage/user/payments',
        icon: <PaymentIcon />,
      },
      {
        label: '적립금 관리',
        link: '/manage/user/point',
        icon: <AttachMoneyIcon />,
      },
      {
        label: '탈퇴 회원 관리',
        link: '/manage/user/leave',
        icon: <PersonAddDisabledIcon />,
      },
    ];
    return menus.map((item, index) => (
      <MenuListItem key={`all-user-manage-list-item-${index}`} icon={item.icon} link={item.link} label={item.label} />
    ));
  };

  renderOtherManageList = () => {
    const menus = [
      {
        label: '공지사항 관리',
        link: '/manage/board/notice',
        icon: <WallpaperIcon />,
      },
      {
        label: '피드백 현황',
        link: '/manage/feedback',
        icon: <FeedbackIcon />,
      },
      {
        label: '쿠폰 관리',
        link: '/manage/coupons',
        icon: <LoyaltyIcon />,
      },
      {
        label: '이용권 관리',
        link: '/manage/products',
        icon: <CardGiftcardIcon />,
      },
    ];
    return menus.map((item, index) => (
      <MenuListItem key={`other-manage-list-item-${index}`} icon={item.icon} link={item.link} label={item.label} />
    ));
  };

  renderStatisticMangeList = () => {
    const menus = [
      {
        label: '통계',
        link: '/statistics',
        icon: <DashboardIcon />,
      },
    ];
    return menus.map((item, index) => (
      <MenuListItem key={`statistics-manage-list-item-${index}`} icon={item.icon} link={item.link} label={item.label} />
    ));
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <List component={'nav'}>
          {/*<ListSubheader> 회원 개인 관리 </ListSubheader>*/}
          {this.renderUserManageList()}
          {/*<ListSubheader> 회원 전체 관리 </ListSubheader>*/}
          {this.renderAllUserManageList()}
          {/*<ListSubheader> 기타 항목 관리 </ListSubheader>*/}
          {this.renderOtherManageList()}
          {/*<ListSubheader> 통계 </ListSubheader>*/}
          {this.renderStatisticMangeList()}
        </List>
      </div>
    );
  }
}

MenuList.propTypes = propTypes;
MenuList.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(styles, { withTheme: true }),
)(MenuList);
