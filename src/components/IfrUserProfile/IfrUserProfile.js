import Paper from 'material-ui/Paper';
import styleProps from 'react-style-proptype';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './IfrUserProfile.css';

const propTypes = {
  src: PropTypes.string.isRequired,
  ifrStyle: styleProps,
  style: styleProps,
};
const defaultProps = {
  style: {},
  ifrStyle: {},
};

class IfrUserProfile extends React.Component {
  render() {
    return (
      <Paper className={s.root} style={this.props.style}>
        <div style={{ padding: '15px' }}>
          <iframe
            src={this.props.src}
            style={this.props.ifrStyle}
            frameBorder={0}
          />
        </div>
      </Paper>
    );
  }
}

IfrUserProfile.propTypes = propTypes;
IfrUserProfile.defaultProps = defaultProps;

export default withStyles(s)(IfrUserProfile);
