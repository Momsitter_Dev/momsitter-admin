import { compose } from 'recompose';
import moment from 'moment';
import uuid from 'uuid/v4';
import AddIcon from 'material-ui/svg-icons/content/add';
import DeleteIcon from 'material-ui/svg-icons/action/delete';
import CloudDownloadIcon from 'material-ui/svg-icons/file/cloud-download';
import React from 'react';
import {
  withStyles as withMaterialStyles,
  AppBar,
  Toolbar,
  IconButton,
  Button,
  Grid,
  Select,
  MenuItem,
  Typography,
  Paper,
} from '@material-ui/core';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './SearchBar.css';

import UserStatusFilter from '../Filter/UserStatus';
import UserGenderFilter from '../Filter/UserGender';
import DateRangePickerWrapper from '../Filter/DateRangePickerWrapper';
import FilterDataTypeGroup from '../../util/const/FilterDataTypeGroup';
import ApplyStatusFilter from '../Filter/ApplyStatus';
import ReviewTypeFilter from '../Filter/ReviewType';
import ReviewStatusFilter from '../Filter/ReviewStatus';
import ReportTypeFilter from '../Filter/ReportType';
import MemoClassFilter from '../Filter/MemoClass';
import MemoStatusFilter from '../Filter/MemoStatus';
import MemoTypeFilter from '../Filter/MemoType';
import MemoReqMethodFilter from '../Filter/MemoReqMethod';
import CertStatusFilter from '../Filter/CertStatus';
import CertTypeFilter from '../Filter/CertType';
import PaymentStatusFilter from '../Filter/PaymentStatus';
import UserOutRequestStatus from '../Filter/UserOutRequestStatus';
import JoinType from '../Filter/JoinType';
import PointType from '../Filter/PointType';
import UserType from '../Filter/UserType';
import InputField from '../InputField';
import UserOwnPhone from '../Filter/UserOwnPhone';

const propTypes = {
  onClickSearch: PropTypes.func.isRequired,
  filter: PropTypes.array.isRequired,
  children: PropTypes.node,
  onExportCSV: PropTypes.func,
  searchOptions: PropTypes.array.isRequired,
  onClickAddSearchOption: PropTypes.func.isRequired,
  onClickDeleteSearchOption: PropTypes.func.isRequired,
  onChangeSearchOption: PropTypes.func.isRequired,
};
const defaultProps = {
  onExportCSV: null,
};

class SearchBar extends React.Component {
  onClickExportCsv = () => {
    return this.props.onExportCSV();
  };

  onClickSearch = () => {
    return this.props.onClickSearch();
  };

  onPressEnter = ev => {
    if (ev.key === 'Enter') {
      ev.preventDefault();
      this.onClickSearch();
    }
  };

  onClickAddSearchOption = () => {
    return this.props.onClickAddSearchOption();
  };

  onClickDeleteSearchOption = generatedKey => {
    return this.props.onClickDeleteSearchOption(generatedKey);
  };

  onChangeSearchOption = (generatedKey, type, value, dataType) => {
    if (dataType === FilterDataTypeGroup.DATE_RANGE) {
      const { startDate, endDate } = value;
      let filterValue = {};
      if (startDate instanceof moment) {
        filterValue.startDate = startDate
          .set('hours', 0)
          .set('minutes', 0)
          .format('YYYY-MM-DD HH:mm');
      }
      if (endDate instanceof moment) {
        filterValue.endDate = endDate
          .set('hours', 23)
          .set('minutes', 59)
          .set('seconds', 59)
          .format('YYYY-MM-DD HH:mm:ss');
      }
      return this.props.onChangeSearchOption(generatedKey, type, filterValue, dataType);
    } else {
      return this.props.onChangeSearchOption(generatedKey, type, value, dataType);
    }
  };

  renderFilterItems = () => {
    const { filter, searchOptions } = this.props;
    if (!searchOptions) {
      return <Typography variant={'caption'}> 필터를 추가해 주세요. </Typography>;
    }
    if (searchOptions.length === 0) {
      return <Typography variant={'caption'}> 필터를 추가해 주세요. </Typography>;
    }
    return searchOptions.map((item, index) => {
      const targetFilterData = filter.filter(filterItem => {
        return filterItem.type === item.filterType;
      });
      let dataType = null;
      if (targetFilterData && targetFilterData instanceof Array && targetFilterData.length > 0) {
        dataType = targetFilterData[0].dataType;
      }
      return (
        <Paper key={`filter-item-${index}`} style={{ padding: '5px' }}>
          <Grid container spacing={8} alignItems={'center'} alignContent={'center'}>
            <Grid item xs={3}>
              <Select
                value={item.filterType}
                onChange={event => this.onChangeSearchOption(item.generatedKey, event.target.value, '', dataType)}
              >
                {this.renderFilterMenu(filter)}
              </Select>
            </Grid>
            <Grid item xs={7}>
              {this.renderFilter(item.generatedKey, item.filterType, item.filterValue, dataType)}
            </Grid>
            <Grid item xs={2}>
              <IconButton onClick={() => this.onClickDeleteSearchOption(item.generatedKey)}>
                <DeleteIcon />
              </IconButton>
            </Grid>
          </Grid>
        </Paper>
      );
    });
  };

  renderFilter = (key, type, value, dataType) => {
    if (!dataType) {
      return <span> 필터 타입을 선택해 주세요. </span>;
    }

    if (dataType === FilterDataTypeGroup.USER_OUT_REQUEST_STATUS) {
      return (
        <UserOutRequestStatus value={value} onChange={v => this.onChangeSearchOption(key, type, v, dataType)} />
      );
    }

    if (dataType === FilterDataTypeGroup.TEXT || dataType === FilterDataTypeGroup.UNIQUE_TEXT) {
      return (
        <InputField
          id={'filterTextInput'}
          onKeyPress={this.onPressEnter}
          onChange={v => this.onChangeSearchOption(key, type, v, dataType)}
          value={value}
        />
      );
    }
    if (dataType === FilterDataTypeGroup.USER_STATUS) {
      return (
        <UserStatusFilter
          id={uuid()}
          value={value}
          onChange={v => this.onChangeSearchOption(key, type, v, dataType)}
        />
      );
    }
    if (dataType === FilterDataTypeGroup.USER_GENDER) {
      return (
        <UserGenderFilter
          id={uuid()}
          value={value}
          onChange={v => this.onChangeSearchOption(key, type, v, dataType)}
        />
      );
    }
    if (dataType === FilterDataTypeGroup.DATE_RANGE) {
      return <DateRangePickerWrapper onDatesChange={v => this.onChangeSearchOption(key, type, v, dataType)} />;
    }
    if (dataType === FilterDataTypeGroup.APPLY_STATUS) {
      return (
        <ApplyStatusFilter value={value} onChange={v => this.onChangeSearchOption(key, type, v, dataType)} />
      );
    }
    if (dataType === FilterDataTypeGroup.REVIEW_TYPE) {
      return (
        <ReviewTypeFilter value={value} onChange={v => this.onChangeSearchOption(key, type, v, dataType)} />
      );
    }
    if (dataType === FilterDataTypeGroup.REVIEW_STATUS) {
      return (
        <ReviewStatusFilter value={value} onChange={v => this.onChangeSearchOption(key, type, v, dataType)} />
      );
    }
    if (dataType === FilterDataTypeGroup.REPORT_TYPE) {
      return (
        <ReportTypeFilter value={value} onChange={v => this.onChangeSearchOption(key, type, v, dataType)} />
      );
    }
    if (dataType === FilterDataTypeGroup.MEMO_CLASS) {
      return (
        <MemoClassFilter value={value} onChange={v => this.onChangeSearchOption(key, type, v, dataType)} />
      );
    }
    if (dataType === FilterDataTypeGroup.MEMO_STATUS) {
      return (
        <MemoStatusFilter value={value} onChange={v => this.onChangeSearchOption(key, type, v, dataType)} />
      );
    }
    if (dataType === FilterDataTypeGroup.MEMO_TYPE) {
      return <MemoTypeFilter value={value} onChange={v => this.onChangeSearchOption(key, type, v, dataType)} />;
    }
    if (dataType === FilterDataTypeGroup.MEMO_REQ_METHOD) {
      return (
        <MemoReqMethodFilter value={value} onChange={v => this.onChangeSearchOption(key, type, v, dataType)} />
      );
    }
    if (dataType === FilterDataTypeGroup.CERT_STATUS) {
      return (
        <CertStatusFilter value={value} onChange={v => this.onChangeSearchOption(key, type, v, dataType)} />
      );
    }
    if (dataType === FilterDataTypeGroup.CERT_TYPE) {
      return <CertTypeFilter value={value} onChange={v => this.onChangeSearchOption(key, type, v, dataType)} />;
    }
    if (dataType === FilterDataTypeGroup.PAYMENT_STATUS) {
      return (
        <PaymentStatusFilter value={value} onChange={v => this.onChangeSearchOption(key, type, v, dataType)} />
      );
    }

    if (dataType === FilterDataTypeGroup.JOIN_TYPE) {
      return <JoinType value={value} onChange={v => this.onChangeSearchOption(key, type, v, dataType)} />;
    }

    if (dataType === FilterDataTypeGroup.POINT_TYPE) {
      return <PointType value={value} onChange={v => this.onChangeSearchOption(key, type, v, dataType)} />;
    }

    if (dataType === FilterDataTypeGroup.USER_TYPE) {
      return <UserType value={value} onChange={v => this.onChangeSearchOption(key, type, v, dataType)} />;
    }

    if (dataType === FilterDataTypeGroup.OWN_PHONE) {
      return <UserOwnPhone value={value} onChange={v => this.onChangeSearchOption(key, type, v, dataType)} />;
    }

    if (dataType === FilterDataTypeGroup.INVALID) {
      return <span> 유효하지 않은 필터입니다. 필터를 선택해 주세요.</span>;
    }
    return (<div>검색 조건을 선택해 주세요.</div>);
  };

  renderFilterMenu = filter => {
    if (filter && filter.length > 0) {
      return filter.map((item, index) => (
        <MenuItem key={`filter-menu-item-${index}`} value={item.type}>
          {item.name}
        </MenuItem>
      ));
    }
    return null;
  };

  renderExportCSV = () => {
    const { onExportCSV } = this.props;
    if (onExportCSV && onExportCSV instanceof Function) {
      return (
        <IconButton onClick={this.onClickExportCsv} tooltip={'CSV 내려받기'}>
          <CloudDownloadIcon />
        </IconButton>
      );
    }
    return null;
  };

  render() {
    return (
      <AppBar position={'static'} color={'default'}>
        <Toolbar style={{ height: 'auto' }}>
          <Grid container spacing={16} alignContent={'center'} alignItems={'center'}>
            <Grid item xs={1} style={{ textAlign: 'center' }}>
              <IconButton onClick={this.onClickAddSearchOption}>
                <AddIcon />
              </IconButton>
            </Grid>
            <Grid item xs={4}>
              {this.renderFilterItems()}
            </Grid>
            <Grid item xs={1}>
              <Button variant={'raised'} color={'primary'} fullWidth onClick={this.onClickSearch}>
                검색하기
              </Button>
            </Grid>
            <Grid item xs={1}>
              {this.renderExportCSV()}
            </Grid>
            <Grid item xs={5}>
              {this.props.children}
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
    );
  }
}

SearchBar.propTypes = propTypes;
SearchBar.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(SearchBar);
