import uuid from 'uuid/v4';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './SearchLocationMultipleList.css';
import SearchLocationMultipleItem from '../SearchLocationMultipleItem';

const propTypes = {
  list: PropTypes.array.isRequired,
  addressType: PropTypes.string.isRequired,
  onClickItem: PropTypes.func.isRequired,
  selectedItems: PropTypes.array,
  currentSelected: PropTypes.object,
  totalShow: PropTypes.bool,
};
const defaultProps = {
  selectedItems: [],
  currentSelected: null,
  totalShow: false,
};

const ADDRESS_TYPE = {
  MAIN: 'main',
  SUB: 'sub',
  DETAIL: 'detail',
};

class SearchLocationMultipleList extends React.Component {
  onClickItem = (type, data) => {
    return this.props.onClickItem(type, data);
  };

  isAlreadySelectedItem = data => {
    const { addressType, selectedItems } = this.props;
    return selectedItems.reduce((acc, item) => {
      if (addressType === ADDRESS_TYPE.DETAIL) {
        if (
          item[ADDRESS_TYPE.MAIN] === data[ADDRESS_TYPE.MAIN] &&
          item[ADDRESS_TYPE.SUB] === data[ADDRESS_TYPE.SUB] &&
          (data[ADDRESS_TYPE.DETAIL] === '전체' && item[ADDRESS_TYPE.DETAIL] === '')
        ) {
          return true;
        }
        if (
          item[ADDRESS_TYPE.MAIN] === data[ADDRESS_TYPE.MAIN] &&
          item[ADDRESS_TYPE.SUB] === data[ADDRESS_TYPE.SUB] &&
          item[ADDRESS_TYPE.DETAIL] === data[ADDRESS_TYPE.DETAIL]
        ) {
          return true;
        }
      }
      return acc;
    }, false);
  };

  isSelectedItem = item => {
    const { addressType, currentSelected } = this.props;
    if (currentSelected) {
      if (addressType === ADDRESS_TYPE.MAIN) {
        if (currentSelected[ADDRESS_TYPE.MAIN] === item[ADDRESS_TYPE.MAIN]) {
          return true;
        }
      }
      if (addressType === ADDRESS_TYPE.SUB) {
        if (
          currentSelected[ADDRESS_TYPE.MAIN] === item[ADDRESS_TYPE.MAIN] &&
          currentSelected[ADDRESS_TYPE.SUB] === item[ADDRESS_TYPE.SUB]
        ) {
          return true;
        }
      }
      if (addressType === ADDRESS_TYPE.DETAIL) {
        if (
          currentSelected[ADDRESS_TYPE.MAIN] === item[ADDRESS_TYPE.MAIN] &&
          currentSelected[ADDRESS_TYPE.SUB] === item[ADDRESS_TYPE.SUB] &&
          currentSelected[ADDRESS_TYPE.DETAIL] === item[ADDRESS_TYPE.DETAIL]
        ) {
          return true;
        }
      }
    }
    return false;
  };

  renderTotalItem = () => {
    const { addressType, currentSelected, list } = this.props;
    try {
      const totalData = {};
      if (currentSelected) {
        totalData.main = currentSelected.main;
        totalData.sub = currentSelected.sub;
        totalData.detail = '전체';
        totalData.address = `${currentSelected.main} ${currentSelected.sub}`;
        totalData.addressId = uuid();
        totalData.locationLatitude = currentSelected.locationLatitude;
        totalData.locationLongitude = currentSelected.locationLongitude;
      } else {
        const target = list[0];
        totalData.main = target.main;
        totalData.sub = target.sub;
        totalData.detail = '전체';
        totalData.address = `${target.main} ${target.sub}`;
        totalData.addressId = uuid();
        totalData.locationLatitude = target.locationLatitude;
        totalData.locationLongitude = target.locationLongitude;
      }

      const isSelected = this.isSelectedItem(totalData);
      const isAlreadySelected = this.isAlreadySelectedItem(totalData);
      return (
        <SearchLocationMultipleItem
          key={`search-location-${addressType}-${uuid}`}
          data={totalData}
          addressType={addressType}
          onClick={this.onClickItem}
          selected={isSelected || isAlreadySelected}
        />
      );
    } catch (err) {
      return null;
    }
  };

  renderList = () => {
    const { addressType, list, totalShow } = this.props;
    let items = [];
    // TODO: 전체
    if (!list) {
      return null;
    }

    if (totalShow) {
      items.push(this.renderTotalItem());
    }

    items = [
      ...items,
      list.map((item, index) => {
        const isSelected = this.isSelectedItem(item);
        const isAlreadySelected = this.isAlreadySelectedItem(item);
        return (
          <SearchLocationMultipleItem
            key={`search-location-${addressType}-item-${index}`}
            data={item}
            addressType={addressType}
            onClick={this.onClickItem}
            selected={isSelected || isAlreadySelected}
          />
        );
      }),
    ];
    return items;
  };

  render() {
    return <div className={s.root}>{this.renderList()}</div>;
  }
}

SearchLocationMultipleList.propTypes = propTypes;
SearchLocationMultipleList.defaultProps = defaultProps;

export default withStyles(s)(SearchLocationMultipleList);
