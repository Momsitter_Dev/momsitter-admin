import { connect } from 'react-redux';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Layout.css';

const propTypes = {};
const defaultProps = {};

class Layout extends React.Component {
  render() {
    return (
      <div>
        <AppBar position="static" color="default">
          <Toolbar style={{ justifyContent: 'space-between' }}>
            <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
              <IconButton aria-label="Menu">
                <MenuIcon />
              </IconButton>
              <Typography variant="title" color="default"> 맘시터 </Typography>
            </div>
            <div>
              <IconButton>
                <AccountCircle />
              </IconButton>
            </div>
          </Toolbar>
        </AppBar>
        {this.props.children}
      </div>
    );
  }
}

Layout.propTypes = propTypes;
Layout.defaultProps = defaultProps;

export default withStyles(s)(Layout);
