import moment from 'moment';
import { compose } from 'recompose';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Checkbox from '@material-ui/core/Checkbox';

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './UserPointItem.css';
import getUserTypeName from '../../util/user/getUserTypeName';

const propTypes = {
  data: PropTypes.shape({
    expireDate: PropTypes.string,
    issueReason: PropTypes.string,
    pointId: PropTypes.number,
    pointPrice: PropTypes.number,
    pointType: PropTypes.string,
    pointTypeId: PropTypes.number,
    pointTypeName: PropTypes.string,
    pubDate: PropTypes.string,
    read: PropTypes.number,
    remainPoint: PropTypes.number,
    removeReason: PropTypes.string,
    removed: PropTypes.number,
    totalPoint: PropTypes.number,
    userId: PropTypes.number,
    userName: PropTypes.string,
    userTypeId: PropTypes.number,
  }).isRequired,
};
const defaultProps = {};

class UserPointItem extends React.Component {
  render() {
    const { data } = this.props;
    return (
      <div className={s.root}>
        <Grow in>
          <Paper style={{ padding: '0.8rem' }}>
            <Grid
              container
              spacing={16}
              alignItems="center"
              justify="center"
              direction="row"
            >
              <Grid item xs={1}>
                <Checkbox />
              </Grid>
              <Grid item xs={1} style={{ textAlign: 'center' }}>
                <Typography> {data.pointId} </Typography>
              </Grid>
              <Grid item xs={1} style={{ textAlign: 'center' }}>
                <Typography> {data.userId} </Typography>
              </Grid>
              <Grid item xs={1} style={{ textAlign: 'center' }}>
                <Typography> {getUserTypeName(data.userTypeId)} </Typography>
              </Grid>
              <Grid item xs={2} style={{ textAlign: 'center' }}>
                <Typography> {data.userName} </Typography>
              </Grid>
              <Grid item xs={2} style={{ textAlign: 'center' }}>
                <Typography> {data.pointTypeName} </Typography>
              </Grid>
              <Grid item xs={2} style={{ textAlign: 'center' }}>
                <Typography> {moment(data.pubDate).format('YYYY년 MM월 DD일')} </Typography>
              </Grid>
              <Grid item xs={2} style={{ textAlign: 'center' }}>
                <Typography> {moment(data.expireDate).format('YYYY년 MM월 DD일')} </Typography>
              </Grid>
            </Grid>
          </Paper>
        </Grow>
      </div>
    );
  }
}

UserPointItem.propTypes = propTypes;
UserPointItem.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(UserPointItem);
