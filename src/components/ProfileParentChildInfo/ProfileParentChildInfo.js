import { compose } from 'recompose';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles/index';
import React from 'react';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelActions from '@material-ui/core/ExpansionPanelActions';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ProfileParentChildInfo.css';

import ChildAgeIdGroup from '../../util/const/ChildAgeIdGroup';
import ChildAgeNameGroup from '../../util/const/ChildAgeNameGroup';
import ChildAgeMonthGroup from '../../util/const/ChildAgeMonthGroup';
import getFormattedMoney from '../../util/dataFormat/getFormattedMoney';

const propTypes = {
  title: PropTypes.string.isRequired,
  parentChildInfo: PropTypes.arrayOf(
    PropTypes.shape({
      childAgeId: PropTypes.number,
      childBirthDay: PropTypes.string,
      childId: PropTypes.number,
      gender: PropTypes.string,
      userId: PropTypes.number,
    }),
  ),
  wantedPayment: PropTypes.number,
  paymentNegotiable: PropTypes.bool,
  onEdit: PropTypes.func,
};
const defaultProps = {
  parentChildInfo: [],
  wantedPayment: null,
  paymentNegotiable: false,
  onEdit: () => {},
};

class ProfileParentChildInfo extends React.Component {
  onClickEdit = () => this.props.onEdit();

  getChildIconPath = (childCount, ageId) => {
    const path = 'https://s3.ap-northeast-2.amazonaws.com/momsitter-service/momsitter-app/static/public/childinfo/';
    if (childCount === 1) {
      if (ageId === ChildAgeIdGroup.NEW) {
        return `${path}n-newbornbaby.png`;
      }
      if (ageId === ChildAgeIdGroup.YONG) {
        return `${path}n-baby.png`;
      }
      if (ageId === ChildAgeIdGroup.CHILD) {
        return `${path}n-child.png`;
      }
      if (ageId === ChildAgeIdGroup.ELEMENT) {
        return `${path}n-schoolchild.png`;
      }
    } else {
      if (ageId === ChildAgeIdGroup.NEW) {
        return `${path}n-newbornbaby-2.png`;
      }
      if (ageId === ChildAgeIdGroup.YONG) {
        return `${path}n-baby-2.png`;
      }
      if (ageId === ChildAgeIdGroup.CHILD) {
        return `${path}n-child-2.png`;
      }
      if (ageId === ChildAgeIdGroup.ELEMENT) {
        return `${path}n-schoolchild-2.png`;
      }
    }
    // TODO: 기본 이미지 ?
    return null;
  };

  renderParentChildInfo = parentChildInfo => {
    const childInfo = Object.values(ChildAgeIdGroup).reduce((acc, item) => {
      acc[item] = {
        childCount: 0,
      };
      return acc;
    }, {});

    parentChildInfo.forEach(item => {
      childInfo[item.childAgeId].childCount += 1;
    });

    return Object.keys(childInfo)
      .filter(item => childInfo[item].childCount > 0)
      .map((item, index) => (
        <div className={s.childItem} key={`parent-child-item-${index}`}>
          <img
            alt="아이 아이콘"
            src={this.getChildIconPath(childInfo[item].childCount, parseInt(item, 10))}
            width={40}
            height={40}
            className={s.childIcon}
          />
          <div className={s.childLabel}>
            {ChildAgeNameGroup[item]} {childInfo[item].childCount} 명 {ChildAgeMonthGroup[item]}
          </div>
        </div>
      ));
  };

  renderContent = () => {
    const { parentChildInfo } = this.props;
    if (!parentChildInfo) {
      return this.renderEmpty();
    }
    if (parentChildInfo.length === 0) {
      return this.renderEmpty();
    }
    return this.renderParentChildInfo(parentChildInfo);
  };

  renderEmpty = () => <div> 표시할 정보가 없습니다. </div>;

  renderWantedPayment = () => {
    const { wantedPayment, paymentNegotiable } = this.props;
    const negoriableText = paymentNegotiable ? ' / 협의가능' : null;
    if (!wantedPayment) {
      return this.renderEmpty();
    }
    return (
      <div className={s.content}>
        <img
          src="https://s3.ap-northeast-2.amazonaws.com/momsitter-service/momsitter-app/static/public/p-modify-1-moneyicon.png"
          width={40}
          height={40}
          className={s.icon}
          alt="icon"
        />
        <div className={s.description}>
          {' '}
          희망시급: {getFormattedMoney(wantedPayment)}원 {negoriableText}{' '}
        </div>
      </div>
    );
  };

  render() {
    const { title } = this.props;
    return (
      <ExpansionPanel>
        <ExpansionPanelSummary>
          <Typography variant="body1">{title}</Typography>
        </ExpansionPanelSummary>

        <ExpansionPanelDetails style={{ paddingTop: '0', paddingBottom: '0' }}>
          <Grid container spacing={16}>
            <Grid item xs={12}>
              <div className={s.root}>{this.renderContent()}</div>
            </Grid>
            <Grid item xs={12}>
              <Divider />
            </Grid>
            <Grid item xs={12}>
              {this.renderWantedPayment()}
            </Grid>
          </Grid>
        </ExpansionPanelDetails>
        <ExpansionPanelActions>
          <Button variant="raised" onClick={this.onClickEdit}>
            수정하기
          </Button>
        </ExpansionPanelActions>
      </ExpansionPanel>
    );
  }
}

ProfileParentChildInfo.propTypes = propTypes;
ProfileParentChildInfo.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(ProfileParentChildInfo);
