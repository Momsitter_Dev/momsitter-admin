import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './FormParentLocation.css';
import SearchLocationPicker from '../SearchLocationPicker';

const propTypes = {
  onChange: PropTypes.func,
  defaultValue: PropTypes.object,
};
const defaultProps = {
  onChange: () => {},
  defaultValue: null,
};

class FormParentLocation extends React.Component {
  onChange = (data) => {
    return this.props.onChange(data);
  };

  getDefaultLocation = () => {
    const { defaultValue } = this.props;
    if (!defaultValue) {
      return null;
    }
    if (defaultValue.main) {
      return defaultValue;
    }
    if (defaultValue.siNm) {
      return {
        ...defaultValue,
        main: defaultValue.siNm,
        sub: `${defaultValue.siNm || ''} ${defaultValue.sub || ''}`.trim(),
        detail: `${defaultValue.siNm || ''} ${defaultValue.sub || ''} ${defaultValue.detail || ''}`.trim(),
      };
    }
    return null;
  };

  render() {
    return (
      <div className={s.root}>
        <SearchLocationPicker
          onChange={this.onChange}
          defaultLocation={this.getDefaultLocation()}
        />
      </div>
    );
  }
}

FormParentLocation.propTypes = propTypes;
FormParentLocation.defaultProps = defaultProps;

export default withStyles(s)(FormParentLocation);
