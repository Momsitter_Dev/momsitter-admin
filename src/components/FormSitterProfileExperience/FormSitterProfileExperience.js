import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './FormSitterProfileExperience.css';
import Experience from '../../components/Experience';

const propTypes = {
  experiences: PropTypes.arrayOf(
    PropTypes.shape({
      experienceTitle: PropTypes.string,
      startDate: PropTypes.oneOfType([PropTypes.instanceOf(Date), PropTypes.string]),
      endDate: PropTypes.oneOfType([PropTypes.instanceOf(Date), PropTypes.string]),
    }),
  ),
  onChange: PropTypes.func,
};
const defaultProps = {
  experiences: [],
  onChange: () => {},
};

class FormSitterProfileExperience extends React.Component {
  onChangeExperience = (event, data) => {
    return this.props.onChange(data);
  };

  renderContent = () => {
    return (
      <div className={s.wrap}>
        <div style={{ marginTop: '15px', padding: '0 20px' }}>
          <p className={s.help}>
            아이를 키운 경험부터 사촌과 놀아준 경험까지, <br />
            아이와 돌봄에 관련된 경험이라면 무엇이든 적어주세요.
          </p>
          <Experience experiences={this.props.experiences} onChange={this.onChangeExperience} />
        </div>
      </div>
    );
  };

  render() {
    return <div className={s.root}>{this.renderContent()}</div>;
  }
}

FormSitterProfileExperience.propTypes = propTypes;
FormSitterProfileExperience.defaultProps = defaultProps;

export default withStyles(s)(FormSitterProfileExperience);
