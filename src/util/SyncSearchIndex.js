import request from 'request-promise';
import Debug from 'debug';
import ResponseResultTypeGroup from '../util/const/ResponseResultTypeGroup';
import config from '../config/config';

const debug = Debug('api');
export default async function (userId, userTypeId) {
  try {
    const response = await request({
      method: 'PUT',
      uri: `${config.apiServer}/api/sync/${userId}`,
      body: {
        key: 'D0F43E39AC923B74F805E19B7109084DF3D71662E4299B6465EEF97435F9EE54',
        userTypeId,
      },
      json: true,
    });

    if (response.result === ResponseResultTypeGroup.SUCCESS) {
      return true;
    }
    throw new Error(`Sync request has been fail ${userId} / ${userTypeId}`);
  } catch (err) {
    debug(err);
    return false;
  }
}
