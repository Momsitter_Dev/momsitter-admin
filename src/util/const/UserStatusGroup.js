const UserStatusGroup = {
  ACTIVE: 'active',
  INACTIVE: 'inactive',
  OUT: 'out',
};

export default UserStatusGroup;
