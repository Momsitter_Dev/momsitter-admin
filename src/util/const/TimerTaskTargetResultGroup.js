const TimerTaskTargetResultGroup = {
  DONE: true,
  FAIL: false,
  NO_STATUS: null,
};

export default TimerTaskTargetResultGroup;
