/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

const RecruitTypeGroup = {
  HIRED: 'hired',
  RECRUITED: 'recruited',
};

export default RecruitTypeGroup;
