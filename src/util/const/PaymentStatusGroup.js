/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

const PaymentStatusGroup = {
  REQUEST: 'request',
  DONE: 'done',
  USER_ABORT: 'userAbort',
  ADMIN_ABORT: 'adminAbort',
  WAITING: 'waiting',
};

export default PaymentStatusGroup;
