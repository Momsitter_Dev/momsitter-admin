/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

const ReportTypeGroup = {
  WRONG_INFORMATION: 'wrongInformation',
  IMPROPER_IMAGE: 'improperImage',
  IMPROPER_CONTENT: 'improperContent',
  VIOLATE_REGULATION: 'violateRegulation',
  IMPROPER_USER: 'improperUser',
  OTHER: 'other',
};

export default ReportTypeGroup;
