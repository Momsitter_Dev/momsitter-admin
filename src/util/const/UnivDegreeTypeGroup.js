/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

const UnivDegreeTypeGroup = {
  BACHELOR: 1,
  MASTER: 2,
  DOCTOR: 3,
};

export default UnivDegreeTypeGroup;
