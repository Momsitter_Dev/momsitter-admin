import CertificateTypeIdGroup from './CertificateTypeIdGroup';

const data = {};
data[CertificateTypeIdGroup.ENROLLMENT] = '학교';
data[CertificateTypeIdGroup.MEDICAL_EXAMINATION] = '건강';
data[CertificateTypeIdGroup.FAMILY_RELATION] = '엄마';
data[CertificateTypeIdGroup.CHILDCARE] = '선생님';
data[CertificateTypeIdGroup.RESIDENT_REGISTRATION] = '등초본';
data[CertificateTypeIdGroup.PERSONALITY] = '인성';
export default data;
