/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

// TODO: DB(momsitter_code_decline 테이블) 와의 연동이 더 적절한지 검토해보아야 함
const ApplyDeclineTypeGroup = {
  ALREADY_MATCHED: 'alreadyMatched',
  ALREADY_INTERVIEW: 'alreadyInterview',
  TOO_FAR: 'tooFar',
  PERIOD_ISSUE: 'periodIssue',
  TIME_ISSUE: 'timeIssue',
  UNAVAILABLE_ACTIVITY: 'unavailableActivity',
  YOUNG_CHILD: 'youngChild',
  NOT_FROM_NOW: 'notFromNow',
  ETC: 'etc',
};

export default ApplyDeclineTypeGroup;
