/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

// 쿠폰 유형의 상태값을 정의하는 상수 테이블
export default {
  AVAILABLE: 'available', // 발행 가능 상태
  UNAVAILABLE: 'unavailable', // 발행 불가능 상태
};
