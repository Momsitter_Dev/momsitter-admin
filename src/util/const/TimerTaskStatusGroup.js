const TimerStatusGroup = {
  DONE: 'done',
  CANCEL: 'cancel',
  PENDING: 'pending',
};

export default TimerStatusGroup;
