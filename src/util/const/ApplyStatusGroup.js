/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

const ApplyStatusGroup = {
  REQUEST: 'request',
  ACCEPT: 'accept',
  DECLINE: 'decline',
  TUNING: 'tuning',
};

export default ApplyStatusGroup;
