/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import ChildAgeIdGroup from './ChildAgeIdGroup';

const ChildAgeMonthGroup = {};
ChildAgeMonthGroup[ChildAgeIdGroup.NEW] = '0-6개월';
ChildAgeMonthGroup[ChildAgeIdGroup.YONG] = '7-36개월';
ChildAgeMonthGroup[ChildAgeIdGroup.CHILD] = '4-6세';
ChildAgeMonthGroup[ChildAgeIdGroup.ELEMENT] = '초등학생';

export default ChildAgeMonthGroup;
