const MemoTypeGroup = {
  REQUEST: '요청',
  COMPLAIN: '불만',
  QUESTION: '문의',
  ADMIN: '관리자',

  // @deprecated
  OTHER: '기타',
};

export default MemoTypeGroup;
