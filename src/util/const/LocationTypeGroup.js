/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

const LocationTypeGroup = {
  SITTER_HOME: 'home',
  SITTER_OTHER: 'other',
  PARENT_CARE: 'care',
};

export default LocationTypeGroup;
