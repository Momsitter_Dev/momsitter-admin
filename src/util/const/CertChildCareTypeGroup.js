/*
<MenuItem value={'보육교사1급'} primaryText="보육교사1급" />
<MenuItem value={'보육교사2급'} primaryText="보육교사2급" />
<MenuItem value={'보육교사3급'} primaryText="보육교사3급" />
<MenuItem value={'유치원 정교사'} primaryText="유치원 정교사" />
<MenuItem value={'특수학교 정교사'} primaryText="특수학교 정교사" />
<MenuItem value={'초등학교 정교사'} primaryText="초등학교 정교사" />
*/

const CertChildCareTypeGroup = {
  CHILD_CARE_TEACHER_LEVEL_1: '보육교사 1급',
  CHILD_CARE_TEACHER_LEVEL_2: '보육교사 2급',
  CHILD_CARE_TEACHER_LEVEL_3: '보육교사 3급',
  KINDERGARDEN_TEACHER: '유치원 정교사',
  SPECIAL_SCHOOL_TEACHER: '특수학교 정교사',
  ELEMENTARY_SCHOOL_TEACHER: '초등학교 정교사',
};

export default CertChildCareTypeGroup;
