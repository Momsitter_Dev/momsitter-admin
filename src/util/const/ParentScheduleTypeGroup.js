/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

// Note. 부모가 신청서 작성/수정시 선택할 수 있는 스케쥴 유형
export default {
  SCHEDULE_TYPE_REGULAR: 'regular',
  SCHEDULE_TYPE_SHORT_TERM: 'shortTerm',
  SCHEDULE_TYPE_UNPLANNED: 'unplanned',
};
