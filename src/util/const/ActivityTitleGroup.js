/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import ActivityTypeIdGroup from './ActivityTypeIdGroup';

const ActivityTitleGroup = {};
ActivityTitleGroup[ActivityTypeIdGroup.SHORT_RESIDENT_SITTER] = '단기입주';
ActivityTitleGroup[ActivityTypeIdGroup.LONG_RESIDENT_SITTER] = '장기입주';
ActivityTitleGroup[ActivityTypeIdGroup.SIMPLE_COOKING] = '간단한 요리';
ActivityTitleGroup[ActivityTypeIdGroup.SIMPLE_DISH_WASHING] = '간단한 설거지';
ActivityTitleGroup[ActivityTypeIdGroup.SIMPLE_CLEANING] = '간단한 청소';
ActivityTitleGroup[ActivityTypeIdGroup.IN_DOOR_ACTIVITY] = '실내놀이';
ActivityTitleGroup[ActivityTypeIdGroup.OUT_DOOR_ACTIVITY] = '야외활동';
ActivityTitleGroup[ActivityTypeIdGroup.ATHLETIC_PLAY] = '체육놀이';
ActivityTitleGroup[ActivityTypeIdGroup.ENGLISH_PLAY] = '영어놀이';
ActivityTitleGroup[ActivityTypeIdGroup.STORY_TELLING] = '동화구연';
ActivityTitleGroup[ActivityTypeIdGroup.BRIEF_EDUCATION] = '간단한 학습지도';
ActivityTitleGroup[ActivityTypeIdGroup.PROFESSIONAL_EDUCATION] = '전문 학습지도';
ActivityTitleGroup[ActivityTypeIdGroup.SCHOOL_COMPANION] = '등하원 돕기';

export default ActivityTitleGroup;
