const UserConnectType = {
  EMAIL: 'email',
  NAVER: 'naver',
  FACEBOOK: 'facebook',
  KAKAO: 'kakao',
};

export default UserConnectType;
