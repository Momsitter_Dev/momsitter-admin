export default {
  BOT: 'bot',
  DESKTOP: 'desktop',
  PHONE: 'phone',
  TABLET: 'tablet',
  TV: 'tv',
};
