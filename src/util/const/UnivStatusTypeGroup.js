/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

const UnivStatusTypeGroup = {
  ATTENDING: 1,
  LEAVE: 2,
  GRADUATE: 3,
  WITHDRAW: 4,
  EXPELLED: 5,
};

export default UnivStatusTypeGroup;
