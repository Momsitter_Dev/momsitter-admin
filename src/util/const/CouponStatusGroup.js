/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

// 쿠폰의 status 값을 정의하는 상수 테이블
export default {
  AVAILABLE: 'available', // 사용 가능
  USED: 'used', // 사용 완료
};
