export default {
  REQUEST: 'request',
  BLACK_LIST: 'blacklist',
  CONFIRMED: 'confirmed',
  HOLD: 'hold',
};
