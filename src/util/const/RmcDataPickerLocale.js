/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

const RmcDataPickerLocale = {
  year: '년',
  month: '월',
  day: '일',
  hour: '시',
  minute: '분',
};

export default RmcDataPickerLocale;
