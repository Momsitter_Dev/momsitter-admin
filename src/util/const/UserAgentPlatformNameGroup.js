import UserAgentPlatformGroup from './UserAgentPlatformGroup';

const name = {};
name[UserAgentPlatformGroup.BOT] = '봇';
name[UserAgentPlatformGroup.DESKTOP] = 'PC';
name[UserAgentPlatformGroup.PHONE] = '모바일';
name[UserAgentPlatformGroup.TABLET] = '태블릿';
name[UserAgentPlatformGroup.TV] = 'TV';

export default name;
