/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

const ReportTypeDescriptionGroup = {
  wrongInformation: '허위정보를 기재하였습니다.',
  improperImage: '부적절한 사진입니다.',
  improperContent: '부적절한 내용입니다.',
  violateRegulation: '규정을 위반하였습니다.',
  improperUser: '맘시터로 활동하기에 적절하지 않습니다.',
  other: '기타',
};

export default ReportTypeDescriptionGroup;
