/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

const CareWayGroup = {
  ALONE: 'alone',
  WITH_PARENT: 'withParent',
  WITH_ELDER: 'withElder',
  WITH_HELPER: 'withHelper',
};

export default CareWayGroup;
