const MemoReqMethodGroup = {
  CALL: 'call',
  KAKAO: 'kakao',
  SMS: 'sms',
  MAIL: 'mail',
};

export default MemoReqMethodGroup;
