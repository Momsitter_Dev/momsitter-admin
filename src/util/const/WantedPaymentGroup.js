/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@mfort.co.kr
 */

export default {
  MINIMUM: 7530,
  UNCERTIFIED_LIMIT: 7600,
  SITTER_AVG_PAYMENT: 7800,
  TWO_CHILD_MINIMUM: 11295,
  TWO_CHILD_FACTOR: 1.5,
};
