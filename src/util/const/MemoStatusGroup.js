const MemoStatusGroup = {
  DONE: 'done',
  NEW: 'new',
};

export default MemoStatusGroup;
