/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

const ProfileImageTypeGroup = {
  SITTER_PROFILE: 'sitterProfile',
  SITTER_PROFILE_MAIN: 'sitterProfileMain',
  PARENT_PROFILE: 'parentProfile',
  PARENT_PROFILE_MAIN: 'parentProfileMain',
};

export default ProfileImageTypeGroup;
