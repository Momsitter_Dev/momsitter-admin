/**
 * 1XX  조건부 응답
 * 2XX  성공
 * 3XX  리다이렉션 완료
 * 4XX  클라이언트 오류
 * 5XX  서버 오류
 */
export default {
  CLIENT_WRONG_REQUEST: 400,
  CLIENT_NOT_FOUND: 404,
}
