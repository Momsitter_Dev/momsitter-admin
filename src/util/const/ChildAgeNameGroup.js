/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import ChildAgeIdGroup from './ChildAgeIdGroup';

const ChildAgeNameGroup = {};
ChildAgeNameGroup[ChildAgeIdGroup.NEW] = '신생아';
ChildAgeNameGroup[ChildAgeIdGroup.YONG] = '영아';
ChildAgeNameGroup[ChildAgeIdGroup.CHILD] = '유아';
ChildAgeNameGroup[ChildAgeIdGroup.ELEMENT] = '초등학생';

export default ChildAgeNameGroup;
