/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

const CertificateTypeIdGroup = {
  ENROLLMENT: 1,
  MEDICAL_EXAMINATION: 2,
  FAMILY_RELATION: 3,
  CHILDCARE: 5,
  RESIDENT_REGISTRATION: 6,
  PERSONALITY: 7,
};

export default CertificateTypeIdGroup;
