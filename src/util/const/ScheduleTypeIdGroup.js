const ScheduleTypeIdGroup = {
  SITTER_WANTED: 1,
  PARENT_WANTED: 2,
  SITTER_UNABLE: 3,
  PARENT_UNABLE: 4,
};

export default ScheduleTypeIdGroup;
