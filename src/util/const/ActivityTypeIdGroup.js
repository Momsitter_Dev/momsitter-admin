/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

export default {
  SHORT_RESIDENT_SITTER: 1, // 단기 입주
  LONG_RESIDENT_SITTER: 2, // 장기 입주
  SIMPLE_COOKING: 3, // 간단 요리
  SIMPLE_DISH_WASHING: 4, // 간단 설거지
  SIMPLE_CLEANING: 5, // 간단 청소
  IN_DOOR_ACTIVITY: 6, // 실내활동
  OUT_DOOR_ACTIVITY: 7, // 야외활동
  ATHLETIC_PLAY: 8, // 체육활동
  ENGLISH_PLAY: 9, // 영어놀이
  STORY_TELLING: 10, // 책 읽기
  BRIEF_EDUCATION: 11, // 일반 교육 (Deprecated)
  PROFESSIONAL_EDUCATION: 12, // 전문 교육 (Deprecated)
  SCHOOL_COMPANION: 13, // 등하원 도우미
  KOREAN_STUDY: 14, // 한글놀이
  STUDY_GUIDE: 15, // 학습지도
};
