const ApplyTypeGroup = {
  PARENT_TO_SITTER: 'parentToSitter',
  SITTER_TO_PARENT: 'sitterToParent',
};

export default ApplyTypeGroup;
