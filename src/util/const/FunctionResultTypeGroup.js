const FunctionResultTypeGroup = {
  DONE: 'done',
  FAIL: 'fail',
  ERROR: 'error',
};

export default FunctionResultTypeGroup;
