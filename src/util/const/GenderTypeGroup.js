/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

const GenderTypeGroup = {
  MAN: 'man',
  WOMEN: 'women',
  IDC: 'idc',
};

export default GenderTypeGroup;
