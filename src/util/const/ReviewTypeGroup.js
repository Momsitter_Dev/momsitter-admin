/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

const ReviewTypeGroup = {
  INTERVIEW: 'interviewReview',
  RECRUIT: 'recruitReview',
  FAILURE: 'failureReview',
  UNFINISHED: 'unfinishedReview',
};

export default ReviewTypeGroup;
