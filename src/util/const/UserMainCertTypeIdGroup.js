/**
 * userTypeId: certTypeId
 * @type {{1}}
 */
const UserMainCertTypeIdGroup = {
  1: 1,
  2: 5,
  3: 3,
  4: 7,
};

export default UserMainCertTypeIdGroup;
