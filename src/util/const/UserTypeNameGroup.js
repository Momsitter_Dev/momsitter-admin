import UserTypeIdGroup from './UserTypeIdGroup';

const userTypeNameGroup = {};

userTypeNameGroup[UserTypeIdGroup.PARENT] = '부모회원';
userTypeNameGroup[UserTypeIdGroup.NORMAL] = '일반인시터';
userTypeNameGroup[UserTypeIdGroup.MOM] = '엄마시터';
userTypeNameGroup[UserTypeIdGroup.COLLEGE] = '대학생시터';
userTypeNameGroup[UserTypeIdGroup.TEACHER] = '선생님시터';

export default userTypeNameGroup;
