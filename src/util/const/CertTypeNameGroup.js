const CertTypeNameGroup = {
  1: '학교',
  2: '건강',
  3: '엄마',
  5: '선생님',
  6: '등초본',
  7: '인성',
};

export default CertTypeNameGroup;
