/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

export default {
  SITTER_ACTIVE: 'active',
  SITTER_INACTIVE: 'inactive',
  PARENT_ACTIVE: 'active',
  PARENT_INACTIVE: 'inactive',
  PARENT_FINISHED: 'finished',
};
