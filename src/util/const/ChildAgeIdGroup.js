/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

export default {
  NEW: 1,
  YONG: 2,
  CHILD: 3,
  ELEMENT: 4,
};
