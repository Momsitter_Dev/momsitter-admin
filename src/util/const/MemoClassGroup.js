const MemoClassGroup = {
  INFO_ERROR: 'info_error',
  PROFILE_ERROR: 'profile_error',
  OTHER: 'other',
  NOT_IN_USE: 'not_in_use'
};

export default MemoClassGroup;
