/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

// TODO: DB(momsitter_code_decline 테이블) 와의 연동이 더 적절한지 검토해보아야 함
import ApplyDeclineTypeGroup from './ApplyDeclineTypeGroup';

const ApplyDeclineTypeTextGroup = [];
ApplyDeclineTypeTextGroup[ApplyDeclineTypeGroup.ALREADY_MATCHED] = '최근에 다른 부모님과 활동을 시작하였습니다.';
ApplyDeclineTypeTextGroup[ApplyDeclineTypeGroup.ALREADY_INTERVIEW] = '다른 부모님과 인터뷰 후, 결과를 기다리고 있습니다.';
ApplyDeclineTypeTextGroup[ApplyDeclineTypeGroup.TOO_FAR] = '이동하기에 먼 거리입니다.';
ApplyDeclineTypeTextGroup[ApplyDeclineTypeGroup.PERIOD_ISSUE] = '요청하신 기간에는 어렵습니다.';
ApplyDeclineTypeTextGroup[ApplyDeclineTypeGroup.TIME_ISSUE] = '요청하신 시간에는 어렵습니다.';
ApplyDeclineTypeTextGroup[ApplyDeclineTypeGroup.UNAVAILABLE_ACTIVITY] = '요청하신 활동은 자신있는 분야가 아닙니다.';
ApplyDeclineTypeTextGroup[ApplyDeclineTypeGroup.YOUNG_CHILD] = '아이 나이가 너무 어립니다.';
ApplyDeclineTypeTextGroup[ApplyDeclineTypeGroup.NOT_FROM_NOW] = '피치 못할 사정으로 앞으로 맘시터 활동이 어렵습니다.';
ApplyDeclineTypeTextGroup[ApplyDeclineTypeGroup.ETC] = '그 밖에 다른 이유로 거절합니다.';

export default ApplyDeclineTypeTextGroup;
