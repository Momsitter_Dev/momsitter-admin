import JoinTypeIdGroup from './JoinTypeIdGroup';

const JoinTypeDescGroup = {};
JoinTypeDescGroup[JoinTypeIdGroup.BRIEF] = '간편가입';
JoinTypeDescGroup[JoinTypeIdGroup.DETAIL] = '상세가입';

export default JoinTypeDescGroup;
