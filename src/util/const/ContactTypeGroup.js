/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

const ContactTypeGroup = {
  SEARCH: 'search',
  NOTICE_BOARD: 'noticeboard',
  RECOMMEND: 'recommend',
  SNS: 'sns',
  OTHER: 'other'
};

export default ContactTypeGroup;
