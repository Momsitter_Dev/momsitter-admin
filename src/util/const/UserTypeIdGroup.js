/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

// TODO: UserTypeIdGroup 과 합칠 것

const UserTypeIdGroup = {
  COLLEGE: 1,
  TEACHER: 2,
  MOM: 3,
  NORMAL: 4,
  PARENT: 5,
};

export default UserTypeIdGroup;
