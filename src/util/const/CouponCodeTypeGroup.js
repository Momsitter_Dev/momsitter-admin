/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

// 쿠폰 코드의 사용 가능 대상 target 를 정의하는 상수 테이블
export default {
  INVITE: 'invite', // 초대 쿠폰 코드
  EVENT: 'event', // 이벤트 쿠폰 코드
};
