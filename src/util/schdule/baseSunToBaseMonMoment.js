/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import moment from 'moment';

moment.locale('ko');

/**
 * 0: sun, 1: mon ... 기반의 weekday 값을 0: mon, 1: tue, ... 6: sun 기반의 weekday 값으로 치환해준다.
 * @param {object} momentDate - moment 객체
 * @returns {number}
 */
function baseSunToBaseMonMoment(momentDate) {
  return (momentDate.weekday() + 6) % 7;
}

export default baseSunToBaseMonMoment;
