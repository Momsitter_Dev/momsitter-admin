/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */
import moment from 'moment';

moment.locale('ko');

/**
 * 0: sun, 1: mon ... 기반의 weekday 값을 0: mon, 1: tue, ... 6: sun 기반의 weekday 값으로 치환해준다.
 * @param {object} date - Date 객체
 * @returns {number}
 */
function baseSunToBaseMonMoment(date) {
  if (date instanceof Date) {
    return (date.getDay() + 6) % 7;
  }
  if (date instanceof moment) {
    return (date.toDate().getDay() + 6) % 7;
  }
  return (moment(date).toDate().getDay() + 6) % 7;
}

export default baseSunToBaseMonMoment;
