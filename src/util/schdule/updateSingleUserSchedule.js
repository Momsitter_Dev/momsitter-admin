/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import fetch from '../../core/fetch';
import APIUrlPathGroup from '../constant/APIUrlPathGroup';

export default function (accessToken, schedule, options) {
  return fetch(`${APIUrlPathGroup.URL_PUT_ME_SINGLE_SCHEDULE}/${schedule.scheduleId}`, {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${accessToken}`,
    },
    body: JSON.stringify({
      schedule,
      options,
    }),
  });
}
