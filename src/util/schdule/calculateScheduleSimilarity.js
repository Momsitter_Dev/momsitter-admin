/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import Moment from 'moment';
import { extendMoment } from 'moment-range';
const moment = extendMoment(Moment);
moment.locale('kr');

/**
 * base 스케쥴 데이터를 기반으로 target 스케쥴 데이터와 얼마나 유사한지 시간 단위로 계산한다.
 * base 와 target 데이터는 scheduleDate 를 기반으로 ASC 방향으로 정렬되어 있다고 가정한다.
 * @param base
 * @param target
 * @returns {number} result
 */
function calculateScheduleSimilarity(base, target) {
  let
    baseIndex = 0,
    targetIndex = 0,
    baseTotalMinutes = 0,
    targetAvailableMinutes = 0,
    isBaseIncreased = true;

  if (base && target && base.length > 0 && target.length > 0) {
    while (baseIndex < base.length) {
      let baseSchedule = base[baseIndex];
      let targetSchedule = target[targetIndex];

      if (!baseSchedule) {
        break;
      }

      let baseScheduleDate = moment(moment(baseSchedule.scheduleDate).format('YYYY-MM-DD'), 'YYYY-MM-DD');
      let baseScheduleStartDate = moment(baseSchedule.scheduleStartTime, 'HH:mm');
      let baseScheduleEndDate = moment(baseSchedule.scheduleEndTime, 'HH:mm');

      if (isBaseIncreased) baseTotalMinutes += baseScheduleEndDate.diff(baseScheduleStartDate, 'minutes');

      if (!targetSchedule) {
        isBaseIncreased = true;
        baseIndex++;
        continue;
      }

      let targetScheduleDate = moment(moment(targetSchedule.scheduleDate).format('YYYY-MM-DD'), 'YYYY-MM-DD');
      let targetScheduleStartDate = moment(targetSchedule.scheduleStartTime, 'HH:mm');
      let targetScheduleEndDate = moment(targetSchedule.scheduleEndTime, 'HH:mm');
      let diff = baseScheduleDate.diff(targetScheduleDate, 'days');

      if (diff === 0) {
        let baseRange = moment.range(baseScheduleStartDate, baseScheduleEndDate);
        let targetRange = moment.range(targetScheduleStartDate, targetScheduleEndDate);
        let intersect = baseRange.intersect(targetRange);

        if (intersect) {
          targetAvailableMinutes += intersect.end.diff(intersect.start, 'minutes');
        }

        isBaseIncreased = true;
        baseIndex++;
        targetIndex++;
      } else if (diff > 0) {
        isBaseIncreased = false;
        targetIndex++;
      } else if (diff < 0) {
        isBaseIncreased = true;
        baseIndex++;
      } else {
        throw new Error('Invalid schedule date.');
      }
    }
  }
  //console.log('result======');
  //console.log(targetAvailableMinutes, baseTotalMinutes);

  return baseTotalMinutes !== 0 ? (targetAvailableMinutes / baseTotalMinutes) : 0;
}

export default  calculateScheduleSimilarity
