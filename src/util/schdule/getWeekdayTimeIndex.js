/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import moment from 'moment';
moment.locale('ko');

function getWeekdayTimeIndex(schedules) {
  try {
    const weekdayLabel = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
    const index = {};

    let startDate = null;
    let endDate = null;

    for (let i = 0; i < schedules.length; i += 1) {
      const item = schedules[i];
      let scheduleDate = moment(item.scheduleDate), startTime, endTime, weekday;

      if (i === 0) {
        startDate = scheduleDate;
        endDate = scheduleDate;
      } else if (i === schedules.length - 1) {
        endDate = scheduleDate;
      }

      weekday = (scheduleDate.day() + 6) % 7;
      startTime = moment(item.scheduleStartTime, 'HH:mm').hour();
      endTime = moment(item.scheduleEndTime, 'HH:mm').hour();

      index[ weekdayLabel[weekday] ] = true;

      if ( startTime >= 7 && startTime <= 12 ) {
        index[ weekdayLabel[weekday] + 'Morning' ] = true;
      } else if ( startTime >= 12 && startTime <= 18) {
        index[ weekdayLabel[weekday] + 'Noon' ] = true;
      } else {
        index[ weekdayLabel[weekday] + 'Night' ] = true;
      }

      if ( endTime >= 7 && endTime <= 12 ) {
        index[ weekdayLabel[weekday] + 'Morning' ] = true;
      } else if ( endTime >= 12 && endTime <= 18) {
        index[ weekdayLabel[weekday] + 'Noon' ] = true;
      } else {
        index[ weekdayLabel[weekday] + 'Night' ] = true;
      }
    }

    index['startDate'] = startDate;
    index['endDate'] = endDate;

    return index;
  } catch (err) {
    throw err;
  }
}

export default getWeekdayTimeIndex;
