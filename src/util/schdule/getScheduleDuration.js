/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import moment from 'moment';

export default function (startDate, endDate) {
  try {
    if (startDate && endDate) {
      return moment(endDate).diff(moment(startDate), 'days') + 1;
    } else {
      return null;
    }
  } catch (err) {
    console.error(err);
    return null;
  }
}
