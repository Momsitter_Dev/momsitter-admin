/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import fetch from '../../core/fetch';
import APIUrlPathGroup from '../constant/APIUrlPathGroup';

export default function (accessToken, scheduleId) {
  return fetch(`${APIUrlPathGroup.URL_DELETE_ME_SINGLE_SCHEDULE}/${scheduleId}`, {
    method: 'DELETE',
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });
}
