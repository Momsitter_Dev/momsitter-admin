/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import baseSunToBaseMonDate from './baseSunToBaseMonDate';

function getWeekdayCheck(schedules) {
  const weekday = [false, false, false, false, false, false, false];
  try {
    for (let item of schedules) {
      weekday[baseSunToBaseMonDate(item.scheduleDate)] = true;
    }
    return weekday;
  } catch (err) {
    console.log(err);
    throw err;
  }
}

export default getWeekdayCheck;
