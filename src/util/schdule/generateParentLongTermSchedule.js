/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import moment from 'moment';
import ScheduleTypeGroup from '../constant/ScheduleTypeGroup';
import baseSunToBaseMonMoment from './baseSunToBaseMonMoment';

const MAX_DAYS = 150;

/**
 * 부모 장기 일정 데이터를 받아서 스케쥴 데이터 리스트로 만들어 리턴하는 함수
 * @param {Date | moment} startDate - 시작날짜
 * @param {Date | moment} [endDate] - 종료날짜
 * @param {object[]} weekdayItems - 주간 일정 데이터
 * @param {bool} [moreThanThreeMonth] - 3개월 이상 플래그
 * @returns {Array}
 */
export default function (startDate, endDate, weekdayItems, moreThanThreeMonth) {
  const schedules = [];

  if (!startDate) throw new Error('Invalid `startDate` parameter.');
  if (!(moreThanThreeMonth || endDate)) throw new Error('Must be valid either one of `endDate` or `moreThanThreeMonth`.');
  if (!weekdayItems) throw new Error('Invalid `weekdayItems` parameter.');
  if (!(weekdayItems instanceof Array)) throw new Error('`weekdayItems` is not a array.');

  const base = moment(startDate);
  const end = moreThanThreeMonth ? moment(base).add(MAX_DAYS, 'days') : moment(endDate);
  const days = moment(end).diff(base, 'days') + 1;

  for (let i = 0; i < days; i += 1) {
    const weekday = baseSunToBaseMonMoment(base);
    if (weekdayItems[weekday]) {
      const { startTime, endTime } = weekdayItems[weekday];
      schedules.push({
        scheduleDate: base.toDate(),
        scheduleStartTime: moment(startTime).format('HH:mm'),
        scheduleEndTime: moment(endTime).format('HH:mm'),
        scheduleTypeId: ScheduleTypeGroup.PARENT_WANTED_TIME,
      });
    }
    base.add(1, 'days');
  }

  return schedules;
}
