/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import moment from 'moment';
import ScheduleTypeGroup from '../constant/ScheduleTypeGroup';

const sundayBaseToMondayBase = momentDate => (momentDate.weekday() + 6) % 7;

/**
 * 파라미터 옵션을 받아서 시터용 스케쥴 데이터를 생성하는 함수
 *
 * @param {Object} weekdayItem - 요일 데이터
 * @param {string} weekdayItem.startTime - i 번째 요일의 시작시간 (HH:mm)
 * @param {string} weekdayItem.endTime - i 번째 요일의 종료시간 (HH:mm)
 * @param {string} weekdayItem.location - 해당 요일에 가능한 장소
 * @param {number} repeat - 반복일, 시작날짜부터 며칠까지의 스케쥴을 만들것인지
 * @param {Date} startDate - 시작날짜
 */
export default function (weekdayItem, repeat, startDate) {
  const schedules = [];
  const base = moment(startDate.getTime());
  for (let i = 0; i < repeat; i += 1) {
    if (weekdayItem[sundayBaseToMondayBase(base)]) {
      const { startTime, endTime, location } = weekdayItem[sundayBaseToMondayBase(base)];
      schedules.push({
        scheduleDate: base.toDate(),
        scheduleStartTime: moment(startTime.getTime()).format('HH:mm'),
        scheduleEndTime: moment(endTime.getTime()).format('HH:mm'),
        scheduleLocation: location,
        scheduleTypeId: ScheduleTypeGroup.SITTER_WANTED_TIME,
      });
    }
    base.add(1, 'days');
  }
  return schedules;
}
