/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import moment from 'moment';

const DEFAULT_SCHEDULE_RANGE = 60;
function getDefaultScheduleRange() {
  const now = moment(new Date()).set('hours', 0).set('minutes', 0).toDate();
  const end = new Date();
  end.setDate(end.getDate() + DEFAULT_SCHEDULE_RANGE);
  return [now, end];
}

export default getDefaultScheduleRange;
