/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import fetch from '../../core/fetch';
import APIUrlPathGroup from '../constant/APIUrlPathGroup';
import ResponseResultTypeGroup from '../constant/ResponseResultTypeGroup';

export default async function (accessToken) {
  const resp = await fetch(APIUrlPathGroup.URL_GET_ME_LAST_SCHEDULE, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${accessToken}`,
      pragma: 'no-cache',
      'cache-control': 'no-cache',
    },
  });

  if (resp.status === 200) {
    const json = await resp.json();

    if (json.result === ResponseResultTypeGroup.SUCCESS) {
      return json.lastSchedule;
    }

    throw new Error(json.err.message);
  } else {
    throw new Error('Server error');
  }
}
