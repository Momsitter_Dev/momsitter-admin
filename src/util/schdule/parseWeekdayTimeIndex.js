/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

function parseWeekdayTimeIndex(index) {
  const weekday = [false, false, false, false, false, false, false];
  const weekdayLabel = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun'];
  try {
    for (let i = 0; i < weekdayLabel.length; i++) {
      weekday[i] = !!index[weekdayLabel[i]];
    }
    return weekday;
  } catch (err) {
    throw err;
  }
}

export default parseWeekdayTimeIndex;
