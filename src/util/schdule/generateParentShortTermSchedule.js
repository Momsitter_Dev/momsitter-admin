/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

import moment from 'moment';
import ScheduleTypeGroup from '../constant/ScheduleTypeGroup';

/**
 * 부모 단기 일정 데이터를 받아서 스케쥴 데이터 리스트로 만들어 리턴하는 함수
 * @param {object[]} list
 * @param {object | string | Date} list.day
 * @param {object | string | Date} list.startTime
 * @param {object | string | Date} list.endTime
 * @returns {Array}
 */
export default function (list) {
  if (!list) throw new Error('Invalid `list` parameter.');
  if (!(list instanceof Array)) throw new Error('`list` is not a array.');
  return list.map(item => ({
    scheduleDate: item.day,
    scheduleStartTime: moment(item.startTime).format('HH:mm'),
    scheduleEndTime: moment(item.endTime).format('HH:mm'),
    scheduleTypeId: ScheduleTypeGroup.PARENT_WANTED_TIME,
  }));
}
