/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@mfort.co.kr
 */

import WantedPaymentGroup from '../const/WantedPaymentGroup';

export default function(wantedPayment, childCount) {
  if (typeof wantedPayment !== 'number') return false;
  if (childCount > 1) {
    return wantedPayment >= WantedPaymentGroup.TWO_CHILD_MINIMUM;
  }
  return wantedPayment >= WantedPaymentGroup.MINIMUM;
}
