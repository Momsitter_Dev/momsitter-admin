import Debug from 'debug';
import moment from 'moment';
import { LinkToken } from '../../data/models/index';

moment.locale('ko');

const debug = Debug('api');

export default async function (linkToken, userId, link) {
  const now = moment();
  const EXPIRE_DATE = 7; // 만료 1주일
  try {
    await LinkToken.create({
      linkToken,
      userId,
      link,
      genDate: now.format('YYYY-MM-DD HH:mm:ss'),
      accessDate: null,
      expireDate: now.add(EXPIRE_DATE, 'days').format('YYYY-MM-DD HH:mm:ss'),
    });
  } catch (err) {
    debug(err);
  }
}
