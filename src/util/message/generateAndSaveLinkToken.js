import Debug from 'debug';
import generateLinkToken from './generateLinkToken';
import saveLinkToken from './saveLinkToken';

const debug = Debug('api');

/**
 * @param attempts 시도횟수
 * @param userId 회원번호
 * @param link 지원링크
 * @return {Promise<*>} token 토큰
 */
export default async function generateAndSaveLinkToken(attempts = 0, userId, link) {
  // NOTE: 5회의 다시시도 횟수가 주어지지만 만약 5회 연속으로 중복 토큰 이 발급된 경우에 대한 핸들링 필요
  const MANUAL_RETRY_COUNT = 5; // 다시 시도 횟수
  if (attempts < MANUAL_RETRY_COUNT) {
    // NOTE: 재시도 가능
    try {
      const token = generateLinkToken();
      await saveLinkToken(token, userId, link);
      return token;
    } catch (err) {
      debug(err);
      return generateAndSaveLinkToken(attempts + 1, userId, link);
    }
  } else {
    // NOTE: 중복토큰이 발행되거나, DB 커넥션 오류로 인한 5회 토큰 insert fail
    throw new Error('중복 토큰이 5회 이상 발행');
  }
}
