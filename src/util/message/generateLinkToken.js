import randomatic from 'randomatic';

export default function () {
  return randomatic('aA0', 50);
}
