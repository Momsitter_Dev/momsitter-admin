import serverConfig from '../../config/config';

export default function (token) {
  if (!serverConfig.production) {
    return `${serverConfig.appServer}/tredirect?t=${token}`;
  }
  return `${serverConfig.appServer}/handleTokenLogin?t=${token}`;
}
