import json2csv from 'json2csv';
import Debug from 'debug';

const debug = Debug('csv');
/**
 *
 * @param options
 * @param options.data
 * @param options.fields
 * @param options.fieldNames
 * @return {Promise}
 * @constructor
 */
const ExportCSV = (options) => {
  return new Promise((resolve, reject) => {
    try {
      if (!options) {
        debug('options is not exist');
        return reject('options is not exist');
      }
      debug('json 2 csv');
      json2csv({
        data: options.data,
        fields: options.fields,
        fieldNames: options.fieldNames,
      }, function (csvError, csvStr) {
        if (csvError) {
          debug(csvError);
          return reject('CSV ERROR');
        }
        debug('???');
        const BOM = "\uFEFF";
        csvStr += BOM;
        return resolve(csvStr);
      });
    } catch (err) {
      debug(err);
      return reject('Error Occur in Export CSV');
    }
  });
};

export default ExportCSV;
