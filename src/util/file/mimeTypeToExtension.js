/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

function mimeTypeToExtension(mimeType) {
  return {
    'image/gif': 'gif',
    'image/png': 'png',
    'image/jpeg': 'jpg',
    'image/bmp': 'bmp',
    'image/webp': 'webp',
  }[mimeType];
}

export default mimeTypeToExtension;
