const CertDataParser = data => {
  let result = null;
  try {
    const parseData = JSON.parse(data);
    result = {};
    if (parseData.univ || parseData.univName) {
      result.univ = parseData.univ || parseData.univName;
    }
    if (parseData.major) {
      result.major = parseData.major;
    }
    if (parseData.degree || parseData.univDegree) {
      result.degree = parseData.degree || parseData.univDegree;
    }
    if (parseData.grade || parseData.univGrade) {
      result.grade = parseData.grade || parseData.univGrade;
    }
    if (parseData.status || parseData.univStatus) {
      result.status = parseData.status || parseData.univStatus;
    }
    if (parseData.issueDate) {
      result.issueDate = parseData.issueDate;
    }
  } catch (e) {
    console.log(e);
  }
  return result;
};

export default CertDataParser;
