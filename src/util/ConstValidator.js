
export default function (constObj, value) {
  return new Promise((resolve, reject) => {
    const keys = Object.keys(constObj);

    for (let i = 0; i < keys.length; i += 1) {
      if (constObj[keys[i]] === value) {
        return resolve(value);
      }
    }

    return reject(new Error('invalid value'));
  });
}
