import request from 'request-promise';
import ATAMessageTemplateCode from './const/ATAMessageTemplateCode';
import LMSMessageTemplateCode from './const/LMSMessageTemplateCode';
import config from '../config/config';

const MESSAGE_SERVER = {
  LMS: `${config.messageServer}/lms`,
  ATA: `${config.messageServer}/ata`,
};

/**
 * @param options
 * @param options.url
 * @param options.templateCode
 * @param options.to
 * @param options.receiverId
 * @param options.params
 */
async function send(options) {
  try {
    const { url, templateCode, to, receiverId, params } = options;
    await request({
      url,
      method: 'POST',
      headers: { 'x-api-key': 'A66D1C13AF96CFE5AAD893E16810596A19FA0F41EF9B1C070E266193CB4D8DFA' },
      json: true,
      form: {
        data: [
          {
            templateCode,
            to,
            receiverId,
            params,
          },
        ],
      },
    });
  } catch (err) {
    console.log(err);
  }
}

/**
 *
 * @param options
 * @param options.templateCode
 * @param options.params
 * @param options.to
 * @param options.receiverId
 * @return {Promise<void>}
 */
export default async function(options) {
  try {
    const { templateCode, to, params, receiverId } = options;
    const isLMS = Object.values(LMSMessageTemplateCode).reduce((acc, item) => {
      if (item === templateCode) {
        acc = true;
      }
      return acc;
    }, false);

    const isATA = Object.values(ATAMessageTemplateCode).reduce((acc, item) => {
      if (item === templateCode) {
        acc = true;
      }
      return acc;
    }, false);

    if (isLMS) {
      // TODO: send as LMS
      await send({
        url: MESSAGE_SERVER.LMS,
        templateCode,
        to,
        params,
        receiverId,
      });
      return true;
    }

    if (isATA) {
      // TODO: send as ATA
      await send({
        url: MESSAGE_SERVER.ATA,
        templateCode,
        to,
        params,
        receiverId,
      });
      return false;
    }

    throw new Error('unknown message');
  } catch (err) {
    console.log(`${err.message} : ${JSON.stringify(options)}`);
  }
}
