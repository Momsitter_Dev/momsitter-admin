import moment from 'moment';

moment.locale('ko');

const getDateStrAsDate = (value) => {
  let result = '';
  if (value) {
    if (value instanceof Date) {
      result = value;
    } else {
      result = moment(value).toDate();
    }
  }
  return result;
};

export default getDateStrAsDate;
