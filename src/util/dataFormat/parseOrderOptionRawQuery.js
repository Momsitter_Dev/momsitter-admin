export default function(orderOption) {
  let query = '';
  if (orderOption) {
    const { column, method } = orderOption;
    if (column && method) {
      query = `ORDER BY ${column} ${method} `;
    }
  }
  return query;
}
