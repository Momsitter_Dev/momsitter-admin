import Debug from 'debug';
import FilterDataTypeGroup from '../const/FilterDataTypeGroup';

const debug = Debug('util');
const parsingSearchOptions = (searchOptions) => {
  let result = {};
  if (searchOptions && searchOptions instanceof Array) {
    const length = searchOptions.length;
    if (length > 0) {
      let currentOption = null;
      for (let i = 0; i < length; i += 1) {
        currentOption = searchOptions[i];
        try {
          if (currentOption) {
            const filterDataType = currentOption['filterDataType'];
            if (filterDataType === FilterDataTypeGroup.DATE_RANGE) {
              result[currentOption['filterType']] = {
                $between: [currentOption['filterValue']['startDate'], currentOption['filterValue']['endDate']],
              };
            } else if (filterDataType === FilterDataTypeGroup.TEXT) {
              result[currentOption['filterType']] = {
                $like: '%' + currentOption['filterValue'] + '%',
              };
            } else {
              result[currentOption['filterType']] = currentOption['filterValue'];
            }
          }
        } catch (err) {
          debug(err);
        }
      }
    }
  }
  return result;
};

export default parsingSearchOptions;
