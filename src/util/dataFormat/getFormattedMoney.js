/**
 * Copyright (C) Momsitter, Inc - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by contact.momsitter@gmail.com
 */

export default function(value) {
  try {
    if (value || value === 0) {
      return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    }
    return value;
  } catch (err) {
    console.error(err);
    return value;
  }
}
