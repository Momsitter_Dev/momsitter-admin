import Debug from 'debug';
import FilterDataTypeGroup from '../const/FilterDataTypeGroup';

const debug = Debug('api');

export default function(searchOptions) {
  let result = '';
  if (!searchOptions) {
    return result;
  }
  if (!(searchOptions instanceof Array)) {
    return result;
  }
  const { length } = searchOptions;
  if (length === 0) {
    return result;
  }
  searchOptions.forEach(item => {
    try {
      const { filterType, filterValue, filterDataType } = item;
      if (!filterType) {
        return null;
      }
      switch (filterDataType) {
        case FilterDataTypeGroup.DATE_RANGE:
          result += `AND ${filterType} BETWEEN '${filterValue['startDate']}' AND '${filterValue['endDate']}' `;
          break;
        case FilterDataTypeGroup.TEXT:
          result += `AND ${filterType} LIKE '%${filterValue}%' `;
          break;
        case FilterDataTypeGroup.OWN_PHONE:
          if (filterValue === 'false') {
            result += `AND ${filterType} = false `;
          } else {
            result += `AND (${filterType} IS NULL OR ${filterType} = true) `;
          }
          break;
        default:
          result += `AND ${filterType} = '${filterValue}' `;
      }
    } catch (err) {
      debug(err);
    }
  });
  return result;
}
