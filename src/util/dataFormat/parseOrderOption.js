import { ParentProfile } from '../../data/models/index';

export default function (orderOption) {
  let query = [];
  if (orderOption) {
    const { column, method } = orderOption;
    if (column && method) {
      if (column === '_parentProfileUpdateDate') {
        query.push([{ model: ParentProfile, as: 'parentProfile' }, 'updateDate', method]);
      } else {
        query.push([column, method]);
      }
    }
  }
  return query;
};
