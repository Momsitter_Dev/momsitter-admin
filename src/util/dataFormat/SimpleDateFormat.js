import moment from 'moment';

const SimpleDateFormat = (date, format) => {
  if (!format) {
    if (moment(date).isValid()) {
      return moment(date).format('YYYY년 MM월 DD일 HH시 mm분');
    }
  } else {
    if (moment(date).isValid()) {
      return moment(date).format(format);
    }
  }

  return '';
};

export default SimpleDateFormat;
