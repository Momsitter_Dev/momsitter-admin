import fetch from 'isomorphic-fetch';
import config from '../config/clientConfig';

const serverURI = config.domain;
// const serverURI = config.url;

export default async function(url, options = {}) {
  const urlIncludeProtocol = url.indexOf('http');
  const result = await fetch(urlIncludeProtocol === -1 ? `${serverURI}/${url}` : url, {
    method: options.method || 'GET',
    headers: options.header || {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${window ? window.localStorage.getItem('access_token') : ''}`,
    },
    mode: options.modeType,
    cache: options.cache || 'no-cache',
    body: options.dataParse ? options.data : JSON.stringify(options.data),
  });

  if (!result) {
    throw new Error('Request Fail');
  }

  if (result.status === 401) {
    alert('로그인 후에 이용 가능합니다.\n로그인 페이지로 이동합니다.');
    window.location.href = '/login';
    throw new Error('token not exist');
  }

  try {
    const parseAsJson = await result.json();
    if (!parseAsJson) {
      throw new Error('Cannot Parsing Response');
    }
    return parseAsJson;
  } catch (err) {
    throw new Error('Cannot Parsing Response');
  }
}
