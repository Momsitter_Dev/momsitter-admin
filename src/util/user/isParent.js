export default function (userTypeId) {
  try {
    return {
      1: false,
      2: false,
      3: false,
      4: false,
      5: true,
    }[Number(userTypeId)];
  } catch (e) {
    console.log(e);
    return false;
  }
}
