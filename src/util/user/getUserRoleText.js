const getUserRoleText = (data) => {
  let text = '없음';
  if (data) {
    const { userRole } = data;
    if (userRole && userRole.length > 0) {
      text = userRole.map((item) => {
        const { userType } = item;
        let userTypeText = '없음';
        if (userType && userType.userDetailTypeDesc) {
          userTypeText = userType.userDetailTypeDesc;
        }
        return userTypeText;
      });
    }
  }
  return text;
};

export default getUserRoleText;
