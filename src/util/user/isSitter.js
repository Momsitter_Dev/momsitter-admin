const isSitter = (userTypeId) => {
  try {
    const userType = {
      "1": true,
      "2": true,
      "3": true,
      "4": true,
      "5": false,
    }[String(userTypeId)];
    return userType;
  } catch (e) {
    return false;
  }
};

export default isSitter;
