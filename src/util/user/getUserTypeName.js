import UserTypeIdGroup from '../const/UserTypeIdGroup';

export default function (userTypeId) {
  const DEFAULT = '알 수 없음';
  try {
    if (userTypeId === UserTypeIdGroup.PARENT) {
      return '부모';
    } else if (userTypeId === UserTypeIdGroup.NORMAL) {
      return '일반';
    } else if (userTypeId === UserTypeIdGroup.COLLEGE) {
      return '대학생';
    } else if (userTypeId === UserTypeIdGroup.MOM) {
      return '엄마';
    } else if (userTypeId === UserTypeIdGroup.TEACHER) {
      return '선생님';
    } else {
      throw new Error('unknown userTypeId');
    }
  } catch (err) {
    return DEFAULT;
  }
}
