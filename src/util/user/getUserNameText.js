const getUserNameText = (data) => {
  let text = '없음';
  if (data && data.userName) {
    text = data.userName;
  }
  return text;
};

export default getUserNameText;
