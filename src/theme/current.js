import { createMuiTheme } from '@material-ui/core/styles';
import {
  blue,
  deepOrange,
  red,
} from '@material-ui/core/colors';

// https://material-ui.com/customization/default-theme/
export default createMuiTheme({
  palette: {
    primary: blue,
    secondary: deepOrange,
    error: red,
    contrastThreshold: 3,
    tonalOffset: 0.2,
  },
  typography: {
    fontFamily: ['Roboto'].join(','),
  },
});
