import { toast } from 'react-toastify';
import { createAction, handleActions } from 'redux-actions';
import Request from '../util/Request';
import ResponseResultTypeGroup from '../util/const/ResponseResultTypeGroup';

// ACTIONS
const SET_PARENT_PROFILE_DATA = 'SET_PARENT_PROFILE_DATA';
const FETCH_PARENT_PROFILE_DATA_PENDING = 'FETCH_PARENT_PROFILE_DATA_PENDING';
const FETCH_PARENT_PROFILE_DATA_ERROR = 'FETCH_PARENT_PROFILE_DATA_ERROR';
const FETCH_PARENT_PROFILE_DATA_SUCCESS = 'FETCH_PARENT_PROFILE_DATA_SUCCESS';

const PUT_PARENT_PROFILE_DATA_PENDING = 'PUT_PARENT_PROFILE_DATA_PENDING';
const PUT_PARENT_PROFILE_DATA_ERROR = 'PUT_PARENT_PROFILE_DATA_ERROR';
const PUT_PARENT_PROFILE_DATA_SUCCESS = 'PUT_PARENT_PROFILE_DATA_SUCCESS';

// initialState
const defaultData = {
  pending: false,
  exception: false,
  exceptionMessage: null,
  data: null,
};

const initialState = {
  ...defaultData,
};

// Reducer
export default handleActions(
  {
    [SET_PARENT_PROFILE_DATA]: (state, action) => {
      return {
        ...state,
        ...action.payload,
      };
    },
    [FETCH_PARENT_PROFILE_DATA_PENDING]: (state, action) => {
      if (action.payload && action.payload.type) {
        return {
          ...state,
          [action.payload.type]: {
            ...defaultData,
            pending: true,
          },
        };
      }
      return {
        ...state,
        pending: true,
      };
    },
    [FETCH_PARENT_PROFILE_DATA_SUCCESS]: (state, action) => {
      return {
        ...state,
        ...action.payload,
      };
    },
    [FETCH_PARENT_PROFILE_DATA_ERROR]: (state, action) => {
      if (action.payload && action.payload.type) {
        return {
          ...state,
          [action.payload.type]: {
            ...defaultData,
            exception: true,
            exceptionMessage: action.payload.message,
          },
        };
      }
      return {
        ...state,
        exception: true,
        exceptionMessage: action.payload.message,
      };
    },
    [PUT_PARENT_PROFILE_DATA_PENDING]: (status) => {
      return {
        ...status,
        pending: true,
      };
    },
    [PUT_PARENT_PROFILE_DATA_ERROR]: (status, action) => {
      return {
        ...status,
        pending: false,
        exception: true,
        exceptionMessage: action.payload.message,
      };
    },
    [PUT_PARENT_PROFILE_DATA_SUCCESS]: (status) => {
      return {
        ...status,
        pending: false,
      };
    },
  },
  initialState,
);

// Action Creators
export const setParentProfileData = createAction(SET_PARENT_PROFILE_DATA);
export const fetchParentProfileDataSuccess = createAction(FETCH_PARENT_PROFILE_DATA_SUCCESS);
export const fetchParentProfileDataPending = createAction(FETCH_PARENT_PROFILE_DATA_PENDING);
export const fetchParentProfileDataException = createAction(FETCH_PARENT_PROFILE_DATA_ERROR);
export const putParentProfileDataPending = createAction(PUT_PARENT_PROFILE_DATA_PENDING);
export const putParentProfileDataSuccess = createAction(PUT_PARENT_PROFILE_DATA_SUCCESS);
export const putParentProfileDataError = createAction(PUT_PARENT_PROFILE_DATA_ERROR);

export const fetchParentProfileData = (userId) => async (dispatch, getState) => {
  const prevState = getState().parentProfile;
  if (prevState.pending) {
    return null;
  }
  try {
    dispatch(fetchParentProfileDataPending());
    const response = await Request(`api/users/${userId}/profile`);

    if (response.result === ResponseResultTypeGroup.FAIL) {
      throw new Error('request fail');
    }

    return dispatch(
      fetchParentProfileDataSuccess({
        ...defaultData,
        data: response.data,
      }),
    );
  } catch (err) {
    console.log(err);
    return dispatch(fetchParentProfileDataException(err));
  }
};

export const putParentProfileData = (userId, userTypeId) => async (dispatch, getState) => {
  try {
    dispatch(putParentProfileDataPending());
    const { parentProfile } = getState();
    const response = await Request(`api/users/${userId}/profile`, {
      method: 'PUT',
      data: {
        ...parentProfile.data,
        userTypeId,
      },
    });
    if (response === ResponseResultTypeGroup.FAIL) {
      throw new Error('request fail');
    }
    toast.success('저장되었습니다.');
    return dispatch(putParentProfileDataSuccess());
  } catch (err) {
    console.log(err);
    toast.error('다시 시도해 주세요.');
    return dispatch(putParentProfileDataError(err));
  }
};
