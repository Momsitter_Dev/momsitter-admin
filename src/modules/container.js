import { createAction, handleActions } from 'redux-actions';

// ACTIONS
const OPEN_DRAWER = 'OPEN_DRAWER';
const CLOSE_DRAWER = 'CLOSE_DRAWER';

// initialState
const initialState = {
  open: false,
};

// Reducer
export default handleActions(
  {
    [OPEN_DRAWER]: () => {
      return {
        open: true,
      };
    },
    [CLOSE_DRAWER]: () => {
      return {
        open: false,
      };
    },
  },
  initialState,
);

// Action Creators
export const openDrawer = createAction(OPEN_DRAWER);
export const closeDrawer = createAction(CLOSE_DRAWER);
