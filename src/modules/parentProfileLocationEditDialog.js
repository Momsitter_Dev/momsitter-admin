import { createAction, handleActions } from 'redux-actions';

// ACTIONS
const OPEN_PARENT_PROFILE_LOCATION_EDIT_DIALOG = 'OPEN_PARENT_PROFILE_LOCATION_EDIT_DIALOG';
const CLOSE_PARENT_PROFILE_LOCATION_EDIT_DIALOG = 'CLOSE_PARENT_PROFILE_LOCATION_EDIT_DIALOG';

// initialState
const initialState = {
  open: false,
};

// Reducer
export default handleActions(
  {
    [OPEN_PARENT_PROFILE_LOCATION_EDIT_DIALOG]: state => {
      return {
        ...state,
        open: true,
      };
    },
    [CLOSE_PARENT_PROFILE_LOCATION_EDIT_DIALOG]: state => {
      return {
        ...state,
        open: false,
      };
    },
  },
  initialState,
);

// Action Creators
export const openDialog = createAction(OPEN_PARENT_PROFILE_LOCATION_EDIT_DIALOG);
export const closeDialog = createAction(CLOSE_PARENT_PROFILE_LOCATION_EDIT_DIALOG);
