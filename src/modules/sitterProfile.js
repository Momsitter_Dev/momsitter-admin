import { toast } from 'react-toastify';
import { createAction, handleActions } from 'redux-actions';
import Request from '../util/Request';
import ResponseResultTypeGroup from '../util/const/ResponseResultTypeGroup';

// ACTIONS
const SET_SITTER_PROFILE_DATA = 'SET_SITTER_PROFILE_DATA';
const FETCH_SITTER_PROFILE_DATA_PENDING = 'FETCH_SITTER_PROFILE_DATA_PENDING';
const FETCH_SITTER_PROFILE_DATA_ERROR = 'FETCH_SITTER_PROFILE_DATA_ERROR';
const FETCH_SITTER_PROFILE_DATA_SUCCESS = 'FETCH_SITTER_PROFILE_DATA_SUCCESS';

const PUT_SITTER_PROFILE_DATA_PENDING = 'PUT_SITTER_PROFILE_DATA_PENDING';
const PUT_SITTER_PROFILE_DATA_ERROR = 'PUT_SITTER_PROFILE_DATA_ERROR';
const PUT_SITTER_PROFILE_DATA_SUCCESS = 'PUT_SITTER_PROFILE_DATA_SUCCESS';

// initialState
const defaultData = {
  pending: false,
  exception: false,
  exceptionMessage: null,
  data: null,
};

const initialState = {
  ...defaultData,
};

// Reducer
export default handleActions(
  {
    [SET_SITTER_PROFILE_DATA]: (state, action) => {
      return {
        ...state,
        ...action.payload,
      };
    },
    [FETCH_SITTER_PROFILE_DATA_PENDING]: (state) => {
      return {
        ...state,
        pending: true,
      };
    },
    [FETCH_SITTER_PROFILE_DATA_ERROR]: (state, action) => {
      return {
        ...state,
        pending: false,
        exception: true,
        exceptionMessage: action.payload.message,
      };
    },
    [FETCH_SITTER_PROFILE_DATA_SUCCESS]: (state, action) => {
      return {
        ...state,
        ...defaultData,
        ...action.payload,
      };
    },
    [PUT_SITTER_PROFILE_DATA_PENDING]: (state) => {
      return {
        ...state,
        pending: true,
      };
    },
    [PUT_SITTER_PROFILE_DATA_ERROR]: (state, action) => {
      return {
        ...state,
        pending: false,
        exception: true,
        exceptionMessage: action.payload.message,
      };
    },
    [PUT_SITTER_PROFILE_DATA_SUCCESS]: (state) => {
      return {
        ...state,
        pending: false,
        exception: false,
        exceptionMessage: null,
      };
    },
  },
  initialState,
);

// Action Creators
export const setSitterProfileData = createAction(SET_SITTER_PROFILE_DATA);
export const fetchSitterProfileDataPending = createAction(FETCH_SITTER_PROFILE_DATA_PENDING);
export const fetchSitterProfileDataError = createAction(FETCH_SITTER_PROFILE_DATA_ERROR);
export const fetchSitterProfileDataSuccess = createAction(FETCH_SITTER_PROFILE_DATA_SUCCESS);

export const putSitterProfileDataPending = createAction(PUT_SITTER_PROFILE_DATA_PENDING);
export const putSitterProfileDataError = createAction(PUT_SITTER_PROFILE_DATA_ERROR);
export const putSitterProfileDataSuccess = createAction(PUT_SITTER_PROFILE_DATA_SUCCESS);

export const fetchSitterProfileData = (userId) => async (dispatch, getState) => {
  const prevState = getState().sitterProfile;
  if (prevState.pending) {
    return null;
  }
  try {
    dispatch(fetchSitterProfileDataPending());
    const profile = await Request(`api/users/${userId}/profile`);

    return dispatch(
      fetchSitterProfileDataSuccess({
        ...defaultData,
        data: profile.data,
      }),
    );
  } catch (err) {
    return dispatch(fetchSitterProfileDataError(err));
  }
};

export const putSitterProfileData = (userId, userTypeId) => async (dispatch, getState) => {
  try {
    dispatch(putSitterProfileDataPending());
    const { sitterProfile } = getState();
    const response = await Request(`api/users/${userId}/profile`, {
      method: 'PUT',
      data: {
        ...sitterProfile.data,
        userTypeId,
      },
    });
    if (response === ResponseResultTypeGroup.FAIL) {
      throw new Error('request fail');
    }
    toast.success('저장되었습니다.');
    return dispatch(putSitterProfileDataSuccess());
  } catch (err) {
    console.log(err);
    toast.error('다시 시도해 주세요.');
    return dispatch(putSitterProfileDataError(err));
  }
};
