import { combineReducers } from 'redux';
import container from './container';
import parentProfile from './parentProfile';
import parentProfileLocationEditDialog from './parentProfileLocationEditDialog';
import parentProfileActivityEditDialog from './parentProfileActivityEditDialog';
import parentProfileChildAndPaymentEditDialog from './parentProfileChildAndPaymentEditDialog';
import parentProfileScheduleEditDialog from './parentProfileScheduleEditDialog';
import parentProfileOtherInfoEditDialog from './parentProfileOtherInfoEditDialog';

import sitterProfile from './sitterProfile';
import sitterProfileActivityEditDialog from './sitterProfileActivityEditDialog';
import sitterProfileExperienceEditDialog from './sitterProfileExperienceEditDialog';
import sitterProfileLocationEditDialog from './sitterProfileLocationEditDialog';
import sitterProfilePreferChildEditDialog from './sitterProfilePreferChildEditDialog';
import sitterProfileScheduleEditDialog from './sitterProfileScheduleEditDialog';

import detailScheduleDialog from './detailScheduleDialog';
import widgetContainer from './widgetContainer';
import widgetCoupon from './widgetCoupon';

import constCouponType from './constCouponType';
import certRejectDialog from './certRejectDialog';
import certDataEditDialog from './certDataEditDialog';

import widgetMessages from './widgetMessages';

import certPersonalityAddDialog from './certPersonalityAddDialog';
import personalityResultDrawer from './personalityResultDrawer';

export default combineReducers({
  container,
  parentProfile,
  parentProfileLocationEditDialog,
  parentProfileActivityEditDialog,
  parentProfileChildAndPaymentEditDialog,
  parentProfileScheduleEditDialog,
  parentProfileOtherInfoEditDialog,

  sitterProfile,
  sitterProfileActivityEditDialog,
  sitterProfileExperienceEditDialog,
  sitterProfileLocationEditDialog,
  sitterProfilePreferChildEditDialog,
  sitterProfileScheduleEditDialog,
  widgetContainer,

  detailScheduleDialog,
  widgetCoupon,
  constCouponType,
  certRejectDialog,
  certDataEditDialog,
  widgetMessages,
  certPersonalityAddDialog,
  personalityResultDrawer,
});
