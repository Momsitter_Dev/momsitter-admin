import { createAction, handleActions } from 'redux-actions';
import { toast } from 'react-toastify';
import ResponseResultTypeGroup from '../util/const/ResponseResultTypeGroup';
import Request from '../util/Request';
import CertificateTypeIdGroup from '../util/const/CertificateTypeIdGroup';

// ACTIONS
const OPEN = 'CONSOLE/CERT_DATA_EDIT_DIALOG/OPEN';
const CLOSE = 'CONSOLE/CERT_DATA_EDIT_DIALOG/CLOSE';
const CHANGE = 'CONSOLE/CERT_DATA_EDIT_DIALOG/CHANGE';
const SAVE_PENDING = 'CONSOLE/CERT_DATA_EDIT_DIALOG/SAVE_PENDING';
const SAVE_SUCCESS = 'CONSOLE/CERT_DATA_EDIT_DIALOG/SAVE_SUCCESS';
const SAVE_ERROR = 'CONSOLE/CERT_DATA_EDIT_DIALOG/SAVE_ERROR';

// initialState
const initialState = {
  open: false,
  pending: false,
  exception: false,
  exceptionMessage: '',
  data: null,
};

// Reducer
export default handleActions(
  {
    [OPEN]: (state, action) => {
      return {
        ...initialState,
        open: true,
        data: action.payload,
      };
    },
    [CLOSE]: () => {
      return {
        ...initialState,
      };
    },
    [CHANGE]: (state, action) => {
      return {
        ...initialState,
        ...state,
        data: {
          ...state.data,
          certDataParse: {
            ...state.data.certDataParse,
            ...action.payload,
          },
        },
      };
    },
    [SAVE_PENDING]: state => {
      return {
        ...initialState,
        ...state,
        pending: true,
      };
    },
    [SAVE_SUCCESS]: state => {
      return {
        ...initialState,
        ...state,
        pending: false,
      };
    },
    [SAVE_ERROR]: (state, action) => {
      return {
        ...initialState,
        ...state,
        exception: true,
        exceptionMessage: action.payload.message,
      };
    },
  },
  initialState,
);

// Action Creators
export const open = createAction(OPEN);
export const close = createAction(CLOSE);
export const change = createAction(CHANGE);
export const savePending = createAction(SAVE_PENDING);
export const saveSuccess = createAction(SAVE_SUCCESS);
export const saveError = createAction(SAVE_ERROR);

export const save = () => async (dispatch, getState) => {
  const prevState = getState().certDataEditDialog;
  if (prevState.pending) {
    return null;
  }
  try {
    const { data } = prevState;
    if (!data) {
      throw new Error('인증 데이터 누락');
    }
    const { certId, certTypeId, userId, callback, certDataParse } = data;
    dispatch(savePending());

    let finalCertData = {};
    // NOTE: CertDataEditDialog 에 사용된 기본값을 여기서 재활용 할 수 있도록 개선 필요.
    switch (certTypeId) {
      case CertificateTypeIdGroup.ENROLLMENT:
        finalCertData = {
          univ: certDataParse.m_univ,
          status: certDataParse.m_status,
          degree: certDataParse.m_degree,
          grade: certDataParse.m_grade,
          major: certDataParse.major,
          issueDate: certDataParse.issueDate,
          graduateDate: certDataParse.graduateDate,
          expelledDate: certDataParse.expelledDate,
        };
        break;
      case CertificateTypeIdGroup.CHILDCARE:
        finalCertData = {
          qualificationType: certDataParse.qualificationType,
          issueDate: certDataParse.issueDate,
        };
        break;
      case CertificateTypeIdGroup.MEDICAL_EXAMINATION:
        finalCertData = {
          issueDate: certDataParse.issueDate,
        };
        break;
      case CertificateTypeIdGroup.RESIDENT_REGISTRATION:
        if (certDataParse.location instanceof Object) {
          finalCertData = {
            issueDate: certDataParse.issueDate,
            location: certDataParse.location.jibunAddr,
          };
        }
        finalCertData = {
          issueDate: certDataParse.issueDate,
          location: certDataParse.location,
        };
        break;
      case CertificateTypeIdGroup.FAMILY_RELATION:
        finalCertData = {
          issueDate: certDataParse.issueDate,
          childCount: certDataParse.children.length,
          children: [...certDataParse.children],
        };
        break;
      default:
        break;
    }
    const response = await Request(`api/users/${userId}/certifications/${certId}`, {
      method: 'PUT',
      data: {
        ...data,
        certData: JSON.stringify(finalCertData),
      },
    });
    if (response.result === ResponseResultTypeGroup.SUCCESS) {
      dispatch(saveSuccess());
      dispatch(close());
      await callback();
      return toast.success('저장되었습니다.');
    }
    throw new Error('쿠폰정보를 조회할 수 없습니다.');
  } catch (err) {
    return dispatch(saveError(err));
  }
};
