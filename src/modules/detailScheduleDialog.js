import { createAction, handleActions } from 'redux-actions';

// ACTIONS
const OPEN_DETAIL_SCHEDULE_DIALOG = 'OPEN_DETAIL_SCHEDULE_DIALOG';
const CLOSE_DETAIL_SCHEDULE_DIALOG = 'CLOSE_DETAIL_SCHEDULE_DIALOG';
const SET_DETAIL_SCHEDULE_DIALOG_DATA = 'SET_DETAIL_SCHEDULE_DIALOG_DATA';

// initialState
const initialState = {
  open: false,
  data: [],
};

// Reducer
export default handleActions(
  {
    [OPEN_DETAIL_SCHEDULE_DIALOG]: (state) => {
      return {
        ...state,
        open: true,
      };
    },
    [CLOSE_DETAIL_SCHEDULE_DIALOG]: (state) => {
      return {
        ...state,
        open: false,
        data: [],
      };
    },
    [SET_DETAIL_SCHEDULE_DIALOG_DATA]: (state, action) => {
      return {
        ...state,
        data: action.payload,
      };
    },
  },
  initialState,
);

// Action Creators
export const openDialog = createAction(OPEN_DETAIL_SCHEDULE_DIALOG);
export const closeDialog = createAction(CLOSE_DETAIL_SCHEDULE_DIALOG);
export const setDialogData = createAction(SET_DETAIL_SCHEDULE_DIALOG_DATA);

export const setAndOpen = (data) => (dispatch) => {
  dispatch(setDialogData(data));
  return dispatch(openDialog());
};
