import { createAction, handleActions } from 'redux-actions';
import Request from '../util/Request';
import ResponseResultTypeGroup from '../util/const/ResponseResultTypeGroup';

// ACTIONS
const FETCH_PENDING = 'console/widgetCoupon/FETCH_PENDING';
const FETCH_SUCCESS = 'console/widgetCoupon/FETCH_SUCCESS';
const FETCH_ERROR = 'console/widgetCoupon/FETCH_ERROR';

// initialState
const initialState = {
  pending: false,
  exception: false,
  exceptionMessage: null,
  data: null,
};

// Reducer
export default handleActions(
  {
    [FETCH_PENDING]: (state) => {
      return {
        ...initialState,
        ...state,
        pending: true,
      };
    },
    [FETCH_SUCCESS]: (state, action) => {
      return {
        ...initialState,
        ...state,
        data: {
          ...action.payload.data,
        },
        pending: false,
      };
    },
    [FETCH_ERROR]: (state, action) => {
      return {
        ...initialState,
        ...state,
        exception: true,
        exceptionMessage: action.payload.message,
      };
    },
  },
  initialState,
);

// Action Creators
export const fetchUserCouponPending = createAction(FETCH_PENDING);
export const fetchUserCouponSuccess = createAction(FETCH_SUCCESS);
export const fetchUserCouponError = createAction(FETCH_ERROR);

export const fetchUserCoupon = (userId) => async (dispatch, getState) => {
  const prevState = getState().widgetCoupon;
  if (prevState.pending) {
    return null;
  }
  try {
    dispatch(fetchUserCouponPending());
    const response = await Request(`api/users/${userId}/coupons`, { method: 'GET' });
    if (response.result === ResponseResultTypeGroup.SUCCESS) {
      return dispatch(
        fetchUserCouponSuccess({
          data: response.data,
        }),
      );
    }
    throw new Error('쿠폰정보를 조회할 수 없습니다.');
  } catch (err) {
    return dispatch(fetchUserCouponError(err));
  }
};
