import { createAction, handleActions } from 'redux-actions';
import fetch from 'node-fetch';
import { toast } from 'react-toastify';

// ACTIONS
const OPEN = 'console/CERT_PERSONALITY_ADD_DIALOG/OPEN';
const CLOSE = 'console/CERT_PERSONALITY_ADD_DIALOG/CLOSE';
const SAVE_PENDING = 'console/CERT_PERSONALITY_ADD_DIALOG/SAVE_PENDING';
const SAVE_SUCCESS = 'console/CERT_PERSONALITY_ADD_DIALOG/SAVE_SUCCESS';
const SAVE_EXCEPTION = 'console/CERT_PERSONALITY_ADD_DIALOG/SAVE_EXCEPTION';

// initialState
const initialState = {
  open: false,
  pending: false,
  exception: false,
  exceptionMessage: '',
};

// Reducer
export default handleActions(
  {
    [OPEN]: () => {
      return {
        ...initialState,
        open: true,
      };
    },
    [CLOSE]: () => {
      return {
        ...initialState,
        open: false,
      };
    },
    [SAVE_PENDING]: state => {
      return {
        ...initialState,
        ...state,
        pending: true,
      };
    },
    [SAVE_SUCCESS]: () => {
      return {
        ...initialState,
      };
    },
    [SAVE_EXCEPTION]: (state, action) => {
      return {
        ...initialState,
        exception: true,
        exceptionMessage: action.payload.message,
      };
    },
  },
  initialState,
);

// Action Creators
export const open = createAction(OPEN);
export const close = createAction(CLOSE);
export const saveSuccess = createAction(SAVE_SUCCESS);
export const savePending = createAction(SAVE_PENDING);
export const saveException = createAction(SAVE_EXCEPTION);

export const save = sequence => async (dispatch, getState) => {
  console.log(`DEBUG : SEQUENCE : ${sequence}`);
  try {
    const { certPersonalityAddDialog } = getState();
    if (certPersonalityAddDialog.pending) {
      return null;
    }
    dispatch(savePending());
    try {
      await fetch(
        `https://ednsdit860.execute-api.ap-northeast-2.amazonaws.com/default/test-crawler?sequence=${sequence}`,
        {
          method: 'GET',
          mode: 'cors',
        },
      );
    } catch (err) {
      // TODO: handle cors issue
      console.log(err);
    }
    dispatch(saveSuccess());
    toast.success('저장되었습니다.');
  } catch (err) {
    dispatch(saveException(err));
  }
};
