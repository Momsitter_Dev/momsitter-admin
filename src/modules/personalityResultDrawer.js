import { createAction, handleActions } from 'redux-actions';
import fetch from 'node-fetch';
import { toast } from 'react-toastify';
import ResponseResultTypeGroup from '../util/const/ResponseResultTypeGroup';
import Request from '../util/Request';

// ACTIONS
const OPEN = 'console/PERSONALITY_RESULT_DRAWER/OPEN';
const CLOSE = 'console/PERSONALITY_RESULT_DRAWER/CLOSE';
const LOAD_PENDING = 'console/PERSONALITY_RESULT_DRAWER/SAVE_PENDING';
const LOAD_SUCCESS = 'console/PERSONALITY_RESULT_DRAWER/SAVE_SUCCESS';
const LOAD_EXCEPTION = 'console/PERSONALITY_RESULT_DRAWER/SAVE_EXCEPTION';

// initialState
const initialState = {
  open: false,
  pending: false,
  exception: false,
  exceptionMessage: '',
  userId: null,
  data: null,
};

// Reducer
export default handleActions(
  {
    [OPEN]: (state, action) => {
      return {
        ...initialState,
        open: true,
        userId: action.payload,
      };
    },
    [CLOSE]: () => {
      return {
        ...initialState,
        open: false,
      };
    },
    [LOAD_PENDING]: state => {
      return {
        ...initialState,
        ...state,
        pending: true,
      };
    },
    [LOAD_SUCCESS]: (state, action) => {
      return {
        ...initialState,
        ...state,
        data: action.payload,
        pending: false,
      };
    },
    [LOAD_EXCEPTION]: (state, action) => {
      return {
        ...initialState,
        ...state,
        exception: true,
        exceptionMessage: action.payload.message,
      };
    },
  },
  initialState,
);

// Action Creators
export const open = createAction(OPEN);
export const close = createAction(CLOSE);
export const loadSuccess = createAction(LOAD_SUCCESS);
export const loadPending = createAction(LOAD_PENDING);
export const loadException = createAction(LOAD_EXCEPTION);

export const load = () => async (dispatch, getState) => {
  try {
    const { personalityResultDrawer } = getState();
    if (personalityResultDrawer.pending) {
      return null;
    }
    dispatch(loadPending());
    const result = await Request(`api/users/${personalityResultDrawer.userId}/personality`, {
      method: 'GET',
    });
    if (result.result === ResponseResultTypeGroup.SUCCESS) {
      return dispatch(loadSuccess(result.data));
    }
    throw new Error('네트워크 오류입니다.');
  } catch (err) {
    return dispatch(loadException(err));
  }
};
