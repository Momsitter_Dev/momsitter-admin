import { createAction, handleActions } from 'redux-actions';

// ACTIONS
const OPEN_SITTER_PROFILE_EXPERIENCE_EDIT_DIALOG = 'OPEN_SITTER_PROFILE_EXPERIENCE_EDIT_DIALOG';
const CLOSE_SITTER_PROFILE_EXPERIENCE_EDIT_DIALOG = 'CLOSE_SITTER_PROFILE_EXPERIENCE_EDIT_DIALOG';

// initialState
const initialState = {
  open: false,
};

// Reducer
export default handleActions(
  {
    [OPEN_SITTER_PROFILE_EXPERIENCE_EDIT_DIALOG]: state => {
      return {
        ...state,
        open: true,
      };
    },
    [CLOSE_SITTER_PROFILE_EXPERIENCE_EDIT_DIALOG]: state => {
      return {
        ...state,
        open: false,
      };
    },
  },
  initialState,
);

// Action Creators
export const openDialog = createAction(OPEN_SITTER_PROFILE_EXPERIENCE_EDIT_DIALOG);
export const closeDialog = createAction(CLOSE_SITTER_PROFILE_EXPERIENCE_EDIT_DIALOG);
