import { createAction, handleActions } from 'redux-actions';
import ResponseResultTypeGroup from '../util/const/ResponseResultTypeGroup';
import Request from '../util/Request';

// ACTIONS
const FETCH_PENDING = 'console/constCouponType/FETCH_PENDING';
const FETCH_SUCCESS = 'console/constCouponType/FETCH_SUCCESS';
const FETCH_ERROR = 'console/constCouponType/FETCH_ERROR';

// initialState
const initialState = {
  pending: false,
  exception: false,
  exceptionMessage: null,
  data: null,
};

// Reducer
export default handleActions(
  {
    [FETCH_PENDING]: state => {
      return {
        ...initialState,
        ...state,
        pending: true,
      };
    },
    [FETCH_SUCCESS]: (state, action) => {
      return {
        ...initialState,
        ...state,
        data: action.payload,
        pending: false,
      };
    },
    [FETCH_ERROR]: (state, action) => {
      return {
        ...initialState,
        ...state,
        exception: true,
        exceptionMessage: action.payload.message,
      };
    },
  },
  initialState,
);

// Action Creators
export const fetchCouponTypePending = createAction(FETCH_PENDING);
export const fetchCouponTypeSuccess = createAction(FETCH_SUCCESS);
export const fetchCouponTypeError = createAction(FETCH_ERROR);

export const fetchCouponType = () => async (dispatch, getState) => {
  const prevState = getState().constCouponType;
  if (prevState.pending) {
    return null;
  }
  try {
    dispatch(fetchCouponTypePending());
    const response = await Request(`api/coupon/types`, { method: 'GET' });
    if (response.result === ResponseResultTypeGroup.SUCCESS) {
      return dispatch(
        fetchCouponTypeSuccess(response.data.couponTypes),
      );
    }
    throw new Error('쿠폰정보를 조회할 수 없습니다.');
  } catch (err) {
    return dispatch(fetchCouponTypeError(err));
  }
};
