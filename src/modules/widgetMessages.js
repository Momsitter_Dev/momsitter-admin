import qs from 'qs';
import { createAction, handleActions } from 'redux-actions';
import Request from '../util/Request';
import ResponseResultTypeGroup from '../util/const/ResponseResultTypeGroup';

// ACTIONS
const FETCH_PENDING = 'console/widgetMessages/FETCH_PENDING';
const FETCH_SUCCESS = 'console/widgetMessages/FETCH_SUCCESS';
const FETCH_ERROR = 'console/widgetMessages/FETCH_ERROR';

// initialState
const initialState = {
  pending: false,
  exception: false,
  exceptionMessage: null,
  data: [],
  page: {},
};

// Reducer
export default handleActions(
  {
    [FETCH_PENDING]: (state) => {
      return {
        ...initialState,
        ...state,
        pending: false,
      };
    },
    [FETCH_SUCCESS]: (state, action) => {
      return {
        ...initialState,
        ...state,
        pending: false,
        data: action.payload.data,
        page: action.payload.page,
      };
    },
    [FETCH_ERROR]: (state, action) => {
      return {
        ...initialState,
        ...state,
        pending: false,
        exception: true,
        exceptionMessage: action.payload.message,
      };
    },
  },
  initialState,
);

// Action Creators
export const fetchPending = createAction(FETCH_PENDING);
export const fetchSuccess = createAction(FETCH_SUCCESS);
export const fetchError = createAction(FETCH_ERROR);

export const fetch = (options) => async (dispatch, getState) => {
  try {
    const prevState = getState().widgetMessages;
    if (prevState.pending) {
      return null;
    }
    dispatch(fetchPending());
    const response = await Request(`api/messages?${qs.stringify(options)}`, { method: 'GET' });
    if (response.result === ResponseResultTypeGroup.SUCCESS) {
      return dispatch(
        fetchSuccess({ data: response.data, page: response.page }),
      );
    }
    throw new Error('메세지 정보를 조회할 수 없습니다.');
  } catch (err) {
    return dispatch(fetchError(err));
  }
};
