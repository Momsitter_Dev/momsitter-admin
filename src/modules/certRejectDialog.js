import { createAction, handleActions } from 'redux-actions';
import { toast } from 'react-toastify';
import ResponseResultTypeGroup from '../util/const/ResponseResultTypeGroup';
import Request from '../util/Request';

// ACTIONS
const OPEN = 'CONSOLE/CERT_REJECT_DIALOG/OPEN';
const CLOSE = 'CONSOLE/CERT_REJECT_DIALOG/CLOSE';
const SAVE_PENDING = 'CONSOLE/CERT_REJECT_DIALOG/SAVE_PENDING';
const SAVE_SUCCESS = 'CONSOLE/CERT_REJECT_DIALOG/SAVE_SUCCESS';
const SAVE_ERROR = 'CONSOLE/CERT_REJECT_DIALOG/SAVE_ERROR';

// initialState
const initialState = {
  open: false,
  pending: false,
  exception: false,
  exceptionMessage: '',
  data: null,
};

// Reducer
export default handleActions(
  {
    [OPEN]: (state, action) => {
      return {
        ...initialState,
        open: true,
        data: action.payload,
      };
    },
    [CLOSE]: () => {
      return {
        ...initialState,
      };
    },
    [SAVE_PENDING]: (state) => {
      return {
        ...initialState,
        ...state,
        pending: true,
      };
    },
    [SAVE_SUCCESS]: (state) => {
      return {
        ...initialState,
        ...state,
        pending: false,
      };
    },
    [SAVE_ERROR]: (state, action) => {
      return {
        ...initialState,
        ...state,
        exception: true,
        exceptionMessage: action.payload.message,
      };
    },
  },
  initialState,
);

// Action Creators
export const open = createAction(OPEN);
export const close = createAction(CLOSE);
export const savePending = createAction(SAVE_PENDING);
export const saveSuccess = createAction(SAVE_SUCCESS);
export const saveError = createAction(SAVE_ERROR);

export const save = (rejectMsg) => async (dispatch, getState) => {
  const prevState = getState().certRejectDialog;
  if (prevState.pending) {
    return null;
  }
  try {
    const { data } = prevState;
    if (!data) {
      throw new Error('인증 데이터 누락');
    }
    const { certId, certStatus, userId, callback } = data;
    dispatch(savePending());
    const response = await Request(`api/manage/user/certifications/${certId}/status`, {
      method: 'PUT',
      data: {
        certStatus,
        userId,
        rejectMsg,
      },
    });
    if (response.result === ResponseResultTypeGroup.SUCCESS) {
      dispatch(saveSuccess());
      dispatch(close());
      await callback();
      return toast.success('저장되었습니다.');
    }
    throw new Error('쿠폰정보를 조회할 수 없습니다.');
  } catch (err) {
    return dispatch(saveError(err));
  }
};
