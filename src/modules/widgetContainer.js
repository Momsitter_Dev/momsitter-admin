import { createAction, handleActions } from 'redux-actions';

// ACTIONS
const SET = 'console/widgetContainer/SET';

// initialState
const initialState = {
  userId: null,
  userTypeId: null,
};

// Reducer
export default handleActions(
  {
    [SET]: (state, action) => {
      return {
        userId: action.payload,
      };
    },
  },
  initialState,
);

// Action Creators
export const set = createAction(SET);
