import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';

import thunk from 'redux-thunk';
import rootReducer from '../modules';

export default (initialState = {}) => {
  const simpleCustomEnhancer = store => next => action => {
    // action = object
    if (typeof action === 'object') {
      // TODO: 어떤 상태로 변경하려고 하는지
      //console.log('DEBUG : SIMPLE CUSTOM ENHANCER');
      // TODO : Elasticsearch logging
      //console.log(action);
      //console.log(store.getState());
    }
    try {
      return next(action);
    } catch (err) {
      console.error(err);
      // TODO : Crash Report
      throw err;
    }
  };

  const logger = createLogger({ collapsed: true });
  const middleware = [simpleCustomEnhancer, thunk, logger];
  const enhancer = applyMiddleware(...middleware);
  const store = createStore(rootReducer, initialState, enhancer);

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../modules', () => {
      store.replaceReducer(require('../modules').default());
    });
  }

  return store;
};
