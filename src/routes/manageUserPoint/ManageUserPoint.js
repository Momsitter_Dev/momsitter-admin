/* eslint-disable prettier/prettier */
import qs from 'qs';
import uuid from 'uuid/v4';
import { compose } from 'recompose';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import ErrorIcon from '@material-ui/icons/Error';
import WarningIcon from '@material-ui/icons/Warning';
import Button from '@material-ui/core/Button';
import FlatPagination from 'material-ui-flat-pagination';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ManageUserPoint.css';

import SearchBar from '../../components/SearchBar';
import UserPointItem from '../../components/UserPointItem';
import ResponseResultTypeGroup from '../../util/const/ResponseResultTypeGroup';
import Request from '../../util/Request';
import FilterDataTypeGroup from '../../util/const/FilterDataTypeGroup';

const propTypes = {};
const defaultProps = {};

class ManageUserPoint extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      pending: true,
      error: false,
      page: 0,
      maxPage: 0,
      searchOptions: [],
      orderOption: {
        column: '',
        method: '',
      },
    };
    this.cachedOption = {};
    this.onClickSearch = this.onClickSearch.bind(this);
    this.search = this.search.bind(this);
    this.onChangeOrderColumn = this.onChangeOrderColumn.bind(this);
  }

  async componentDidMount() {
    try {
      const searchResult = await this.search();
      this.setState({
        list: searchResult.data,
        page: searchResult.page.currentPage,
        maxPage: searchResult.page.maxPage,
        pending: false,
        error: false,
      });
    } catch (err) {
      this.setState({
        pending: false,
        error: true,
      });
    }
  }

  onClickSearch = async function () {
    try {
      this.setState({
        pending: true,
      }, async function () {
        const { searchOptions, orderOption } = this.state;
        const result = await this.search({
          searchOptions,
          orderOption,
          requestPage: 0,
        });

        this.setState({
          pending: false,
          list: result.data,
          page: result.page.currentPage,
          maxPage: result.page.maxPage,
          error: false,
        });
      });
    } catch (err) {
      this.setState({
        pending: false,
        error: true,
      });
    }
  };

  onExportCSV = () => {
    const queryString = qs.stringify(this.cachedOption);
    window.open(`/api/points/csv?${queryString}`);
  };

  onChangeSearchOption = (generatedKey, type, value, dataType) => {
    const { searchOptions } = this.state;
    const targetIndex = searchOptions.findIndex((item) => {
      return item.generatedKey === generatedKey;
    });
    if (targetIndex !== -1) {
      searchOptions[targetIndex] = {
        ...searchOptions[targetIndex],
        filterType: type,
        filterValue: value,
        filterDataType: dataType,
      };
      return this.setState({ searchOptions });
    } else {
      console.log('DEBUG : ON CHANGE BUT NOT FOUND : ' + generatedKey);
    }
  };

  onDeleteSearchOption = (generatedKey) => {
    const { searchOptions } = this.state;
    return this.setState({
      searchOptions: searchOptions.filter((item) => {
        return item.generatedKey !== generatedKey;
      }),
    });
  };

  onAddSearchOption = () => {
    const { searchOptions } = this.state;
    searchOptions.push({
      generatedKey: uuid(),
      filterType: '',
      filterValue: '',
      filterDataType: '',
    });
    return this.setState({ searchOptions });
  };

  onChangeOrderColumn = async function (columnId) {
    try {
      const { orderOption, searchOptions } = this.state;
      if (orderOption.column !== columnId) {
        orderOption.method = '';
      }
      orderOption.column = columnId;
      if (orderOption.method === 'DESC') {
        orderOption.method = 'ASC';
      } else if (orderOption.method === 'ASC') {
        orderOption.method = '';
      } else {
        orderOption.method = 'DESC'
      }

      return this.setState({
        orderOption,
        pending: true,
      }, async function () {
        const result = await this.search({
          searchOptions,
          orderOption,
          requestPage: 0,
        });
        return this.setState({
          list: result.data,
          page: result.page.currentPage,
          maxPage: result.page.maxPage,
          pending: false,
        });
      });
    } catch (err) {
      console.log(err);
      return this.setState({
        error: true,
      });
    }
  };

  onClickPage = async function (value) {
    try {
      this.cachedOption.requestPage = value;
      return this.setState({
        pending: true,
      }, async function () {
        const result = await this.search(this.cachedOption);
        return this.setState({
          list: result.data,
          page: result.page.currentPage,
          maxPage: result.page.maxPage,
          pending: false,
          error: false,
        });
      });

    } catch (err) {
      return this.setState({
        error: true,
      });
    }
  };

  search = async function (options) {
    try {
      const query = qs.stringify(options);
      this.cachedOption = Object.assign({}, options);
      const response = await Request(`api/points?${query}`, { method: 'GET' });
      if (response.result === ResponseResultTypeGroup.SUCCESS) {
        return response;
      }
      throw new Error('request fail');
    } catch (err) {
      throw new Error(err);
    }
  };

  renderPagenation = () => {
    const { page, maxPage } = this.state;
    return (
      <Paper style={{ padding: '0.8rem' }}>
        <FlatPagination
          offset={page}
          limit={1}
          total={maxPage}
          onClick={(e, offset) => this.onClickPage(offset)}
        />
      </Paper>
    );
  };

  renderArrow = (columnId) => {
    const { orderOption } = this.state;
    if (columnId === orderOption.column) {
      if (orderOption.method === 'DESC') {
        return (
          <ArrowDownwardIcon style={{ width: '15px', height: '15px' }} />
        );
      }
      if (orderOption.method === 'ASC') {
        return (
          <ArrowUpwardIcon style={{ width: '15px', height: '15px' }} />
        );
      }
    }
    return null;
  };

  renderPointHeader = () => {
    return (
      <Paper style={{ padding: '0.8rem' }}>
        <Grid
          container
          spacing={16}
          alignItems="center"
          alignContent="center"
          justify="center"
          direction="row"
        >
          <Grid item xs={1} />
          <Grid item xs={1} style={{ textAlign: 'center' }}>
            <Button onClick={() => this.onChangeOrderColumn('pointId')}>
              {this.renderArrow('pointId')}
              <Typography variant="caption">
                적립금 번호
              </Typography>
            </Button>
          </Grid>
          <Grid item xs={1} style={{ textAlign: 'center' }}>
            <Button onClick={() => this.onChangeOrderColumn('userId')}>
              {this.renderArrow('userId')}
              <Typography variant="caption">
                회원 번호
              </Typography>
            </Button>
          </Grid>
          <Grid item xs={1} style={{ textAlign: 'center' }}>
            <Button onClick={() => this.onChangeOrderColumn('userTypeId')}>
              {this.renderArrow('userTypeId')}
              <Typography variant="caption">
                회원 유형
              </Typography>
            </Button>
          </Grid>
          <Grid item xs={2} style={{ textAlign: 'center' }}>
            <Button onClick={() => this.onChangeOrderColumn('userName')}>
              {this.renderArrow('userName')}
              <Typography variant="caption">
                이름
              </Typography>
            </Button>
          </Grid>
          <Grid item xs={2} style={{ textAlign: 'center' }}>
            <Button onClick={() => this.onChangeOrderColumn('pointTypeId')}>
              {this.renderArrow('pointTypeId')}
              <Typography variant="caption">
                지급 유형
              </Typography>
            </Button>
          </Grid>
          <Grid item xs={2} style={{ textAlign: 'center' }}>
            <Button onClick={() => this.onChangeOrderColumn('pubDate')}>
              {this.renderArrow('pubDate')}
              <Typography variant="caption">
                발급일자
              </Typography>
            </Button>
          </Grid>
          <Grid item xs={2} style={{ textAlign: 'center' }}>
            <Button onClick={() => this.onChangeOrderColumn('expireDate')}>
              {this.renderArrow('expireDate')}
              <Typography variant="caption">
                만료일자
              </Typography>
            </Button>
          </Grid>
        </Grid>
      </Paper>
    );
  };

  renderPointItems = () => {
    const { list } = this.state;
    return list.map((item, index) => {
      return (
        <UserPointItem data={item} key={`point-list-item-${index}`} />
      );
    });
  };

  renderPointList = () => {
    try {
      const { list } = this.state;
      if (list.length === 0) {
        return this.renderEmptyUI();
      }
      return (
        <div>
          {this.renderPointHeader()}
          {this.renderPointItems()}
          {this.renderPagenation()}
        </div>
      );
    } catch (err) {
      console.log(err);
      return this.renderEmptyUI();
    }
  };

  renderEmptyUI = () => {
    return (
      <Paper style={{ padding: '0.8rem' }}>
        <div className={s.flexRow}>
          <WarningIcon style={{ width: '25px', height: '25px' }} />
          <Typography variant="body2"> 조건에 맞는 데이터가 없습니다. </Typography>
        </div>
      </Paper>
    )
  };

  renderPendingUI = () => {
    return (
      <Paper style={{ padding: '0.8rem' }}>
        <div className={s.flexRow}>
          <CircularProgress style={{ marginRight: '10px' }} />
          <Typography variant="body2"> 데이터를 불러오는 중입니다. </Typography>
        </div>
      </Paper>
    );
  };

  renderErrorUI = () => {
    return (
      <Paper style={{ padding: '1.25rem' }}>
        <div className={s.flexRow}>
          <ErrorIcon style={{ width: '25px', height: '25px' }} />
          <div> 오류가 발생했습니다. 다시 시도해 주세요. </div>
        </div>
      </Paper>
    );
  };

  renderContent = () => {
    try {
      const { pending, error } = this.state;
      if (pending) {
        return this.renderPendingUI();
      }
      if (error) {
        return this.renderErrorUI();
      }
      return this.renderPointList();
    } catch (err) {
      return this.renderErrorUI();
    }
  };

  render() {
    const { searchOptions } = this.state;
    const filter = [
      { name: '적립금 번호', type: 'pointId', dataType: FilterDataTypeGroup.UNIQUE_TEXT },
      { name: '회원 번호', type: 'userId', dataType: FilterDataTypeGroup.UNIQUE_TEXT },
      { name: '포인트 타입', type: 'pointTypeId', dataType: FilterDataTypeGroup.POINT_TYPE },
      { name: '회원 이름', type: 'userName', dataType: FilterDataTypeGroup.TEXT },
      { name: '회원 유형', type: 'userTypeId', dataType: FilterDataTypeGroup.USER_TYPE },
      { name: '지급일자', type: 'pubDate', dataType: FilterDataTypeGroup.DATE_RANGE },
      { name: '만료일자', type: 'expireDate', dataType: FilterDataTypeGroup.DATE_RANGE },
    ];

    return (
      <div>
        <Paper style={{ borderRadius: '5px' }}>
          <AppBar position="static" style={{ backgroundColor: '#fff', borderRadius: '5px 5px 0 0' }}>
            <Toolbar>
              <Typography variant="title" style={{ color: '#717C97' }}> 적립금 현황 </Typography>
            </Toolbar>
          </AppBar>
          <SearchBar
            filter={filter}
            searchOptions={searchOptions}
            onClickSearch={this.onClickSearch}
            onClickAddSearchOption={this.onAddSearchOption}
            onClickDeleteSearchOption={this.onDeleteSearchOption}
            onChangeSearchOption={this.onChangeSearchOption}
            onExportCSV={this.onExportCSV}
          />
          <div className={s.container}>
            {this.renderContent()}
          </div>
        </Paper>
      </div>
    );
  }
}

ManageUserPoint.propTypes = propTypes;
ManageUserPoint.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(ManageUserPoint);
