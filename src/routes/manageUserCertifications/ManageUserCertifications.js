import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';
import qs from 'qs';
import uuid from 'uuid/v4';
import { toast } from 'react-toastify';
import { compose } from 'recompose';
import React from 'react';
import FlatPagination from 'material-ui-flat-pagination';
import {
  ArrowUpward as ArrowUpIcon,
  ArrowDownward as ArrowDownIcon,
  ErrorOutline as ErrorIcon,
  Warning as WarningIcon,
  Delete as DeleteIcon,
  ExpandMore as ExpandMoreIcon,
  ExpandLess as ExpandLessIcon,
  Save as SaveIcon,
  VerifiedUser as MainCertIcon,
} from '@material-ui/icons';
import {
  withStyles as withMaterialStyles,
  Button,
  Grid,
  Paper,
  Typography,
  CircularProgress,
  Select,
  MenuItem,
  IconButton,
  Collapse,
  Divider,
  Tooltip,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
} from '@material-ui/core';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ManageUserCertifications.css';
import SearchBar from '../../components/SearchBar';
import FilterDataTypeGroup from '../../util/const/FilterDataTypeGroup';
import Request from '../../util/Request';
import ResponseResultTypeGroup from '../../util/const/ResponseResultTypeGroup';
import CertificateStatusGroup from '../../util/const/CertificateStatusGroup';
import UserMainCertTypeIdGroup from '../../util/const/UserMainCertTypeIdGroup';
import CertificateTypeIdGroup from '../../util/const/CertificateTypeIdGroup';
import UnivDegreeTypeGroup from '../../util/const/UnivDegreeTypeGroup';
import UnivStatusTypeGroup from '../../util/const/UnivStatusTypeGroup';

import * as certRejectDialogActions from '../../modules/certRejectDialog';
import * as certDataEditDialogActions from '../../modules/certDataEditDialog';

import CertRejectDialog from '../../containers/CertRejectDialog';
import CertDataEditDialog from '../../containers/CertDataEditDialog';
import PersonalityResult from '../../containers/PersonalityResult';
import * as personalityResultDrawerActions from '../../modules/personalityResultDrawer';

moment.locale('ko');

const styles = theme => ({
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
});

const propTypes = {
  classes: PropTypes.object.isRequired,
  CertDataEditDialogActions: PropTypes.object.isRequired,
  CertRejectDialogActions: PropTypes.object.isRequired,
  PersonalityResultDrawerActions: PropTypes.object.isRequired,
};
const defaultProps = {};

class ManageUserCertifications extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pending: true,
      exception: false,
      exceptionMessage: null,
      list: [],
      page: null,
      maxPage: null,
      searchOptions: [],
      orderOption: {
        column: '',
        method: '',
      },
      removeDialog: false,
      // NOTE: expand ui
      expandItems: {},
      previewDialog: false,
      previewFileUrl: null,
    };
    this.cachedOption = {};
    this.removeCertId = null;
  }

  async componentDidMount() {
    const response = await this.search();
    if (response) {
      this.setState({
        exception: false,
        pending: false,
        list: response.data,
        page: response.page.currentPage,
        maxPage: response.page.maxPage,
      });
    }
  }

  onClickOpenResult = (userId) => {
    return this.props.PersonalityResultDrawerActions.open(userId);
  };

  onClickRemoveCertConfirm = async () => {
    try {
      const response = await Request(`api/manage/user/certifications/${this.removeCertId}`, {
        method: 'DELETE',
      });
      if (response.result === ResponseResultTypeGroup.SUCCESS) {
        toast.success('인증정보가 삭제되었습니다.');
        await this.refresh();
        return this.closeRemoveCertConfirm();
      }
      throw new Error('요청에 실패하였습니다.');
    } catch (err) {
      return toast.error(`인증정보를 삭제할 수 없습니다. : ${err.message}`);
    }
  };

  onClickRemoveCert = (certId) => {
    this.removeCertId = certId;
    return this.setState({ removeDialog: true });
  };

  onClickEditCertData = data => {
    const certDataDefault = {
      [CertificateTypeIdGroup.ENROLLMENT]: {
        m_univ: '',
        m_degree: '',
        m_status: '',
        m_grade: '',
        major: '',
        issueDate: '',
        graduateDate: '',
        expelledDate: '',
      },
      [CertificateTypeIdGroup.MEDICAL_EXAMINATION]: {
        issueDate: '',
      },
      [CertificateTypeIdGroup.CHILDCARE]: {
        qualificationType: '',
        issueDate: '',
      },
      [CertificateTypeIdGroup.FAMILY_RELATION]: {},
      [CertificateTypeIdGroup.PERSONALITY]: {},
      [CertificateTypeIdGroup.RESIDENT_REGISTRATION]: {
        issueDate: '',
        location: '',
      },
    };

    const { certData, certTypeId } = data;
    let certDataParse = null;
    try {
      certDataParse = JSON.parse(certData);
      switch (certTypeId) {
        case CertificateTypeIdGroup.ENROLLMENT:
          certDataParse = {
            ...certDataDefault[certTypeId],
            ...certDataParse,
            major: certDataParse.major || certDataParse.major,
            m_univ: certDataParse.univ || certDataParse.univName,
            m_status: certDataParse.status || certDataParse.univStatus,
            m_grade: certDataParse.grade || certDataParse.univGrade,
            m_degree: certDataParse.degree || certDataParse.univDegree,
          };
          break;
        case CertificateTypeIdGroup.MEDICAL_EXAMINATION:
          certDataParse = {
            ...certDataDefault[certTypeId],
            ...certDataParse,
          };
          break;
        case CertificateTypeIdGroup.CHILDCARE:
          certDataParse = {
            ...certDataDefault[certTypeId],
            ...certDataParse,
          };
          break;
        case CertificateTypeIdGroup.FAMILY_RELATION:
          break;
        case CertificateTypeIdGroup.PERSONALITY:
          break;
        case CertificateTypeIdGroup.RESIDENT_REGISTRATION:
          if (typeof certDataParse.location === 'string') {
            certDataParse = {
              ...certDataDefault[certTypeId],
              ...certDataParse,
              location: certDataParse.location,
            };
          } else if (certDataParse.location instanceof Object) {
            certDataParse = {
              ...certDataDefault[certTypeId],
              ...certDataParse,
              location: certDataParse.location.jibunAddr,
            };
          } else {
            certDataParse = {
              ...certDataDefault[certTypeId],
            };
          }
          break;
        default:
          break;
      }
    } catch (err) {
      certDataParse = certDataDefault[certTypeId];
    }
    return this.props.CertDataEditDialogActions.open({
      ...data,
      certDataParse,
      callback: this.refresh,
    });
  };

  /**
   * @param data
   * @param data.fileId
   * @param data.fileUrl
   */
  onClickFile = data => {
    const { fileUrl } = data;
    return this.setState({
      previewDialog: true,
      previewFileUrl: fileUrl,
    });
  };

  onClickDownloadFile = () => {
    const { previewFileUrl } = this.state;
    if (previewFileUrl) {
      return window.open(previewFileUrl);
    }
    return null;
  };

  onClickExpand = certId => {
    const { expandItems } = this.state;
    try {
      // NOTE: 이미 펼쳐진 상태인 경우 false
      if (expandItems[certId]) {
        return this.setState({
          expandItems: {
            ...expandItems,
            [certId]: false,
          },
        });
      } else {
        return this.setState({
          expandItems: {
            ...expandItems,
            [certId]: true,
          },
        });
      }
    } catch (err) {
      console.log(err);
    }
  };

  onClickUserId = userId => {
    return window.open(`/users?userId=${userId}`);
  };

  /**
   *
   * @param event
   * @param data
   * @param data.userId
   * @param data.userName
   * @param data.userTypeId
   * @param data.certId
   * @param data.certTypeId
   * @param data.certStatus
   * @param data.certData
   * @return {Promise<void>}
   */
  onChangeCertStatus = async (event, data) => {
    const { userId, userName, userTypeId, certId, certData, certTypeId, certStatus } = data;
    const { value } = event.target;
    if (value === certStatus) {
      return null;
    }
    return this.setState(
      {
        pending: true,
      },
      async () => {
        try {
          if (value === CertificateStatusGroup.APPROVED) {
            const isMainCertification = this.isMainCert({ userTypeId, certTypeId });
            if (isMainCertification) {
              // TODO: 메인인증이여도 수기로 메인인증 라벨을 입력할 수 있도록 수정.

              if (!certData) {
                throw new Error('유효한 인증 데이터를 입력한 후에 시도해 주세요.');
              }

              const response = await Request(`api/manage/user/certifications/${certId}/main/auto`, {
                method: 'PUT',
                data: {
                  certStatus: CertificateStatusGroup.APPROVED,
                  certTypeId,
                  userId,
                  certData,
                },
              });

              if (response.result === ResponseResultTypeGroup.SUCCESS) {
                await this.refresh();
                return toast.success(`${userName}(${userId}) 회원의 인증상태가 '승인'으로 수정되었습니다.`);
              }
              throw new Error('request fail');
            }

            const response = await Request(`api/manage/user/certifications/${certId}/status`, {
              method: 'PUT',
              data: {
                certStatus: CertificateStatusGroup.APPROVED,
                userId,
              },
            });

            if (response.result === ResponseResultTypeGroup.SUCCESS) {
              await this.refresh();
              return toast.success(`${userName}(${userId}) 회원의 인증상태가 '승인'으로 수정되었습니다.`);
            }
            throw new Error('request fail');
          }

          if (value === CertificateStatusGroup.REJECTED) {
            // TODO: 인증 반려 사유 입력 UI
            return this.setState(
              {
                pending: false,
              },
              () => {
                return this.props.CertRejectDialogActions.open({
                  userId,
                  certId,
                  certTypeId,
                  certStatus: value,
                  callback: this.refresh,
                });
              },
            );
          }

          if (value === CertificateStatusGroup.WAITING) {
            const response = await Request(`api/manage/user/certifications/${certId}/status`, {
              method: 'PUT',
              data: {
                certStatus: CertificateStatusGroup.WAITING,
                userId,
              },
            });

            if (response.result === ResponseResultTypeGroup.SUCCESS) {
              await this.refresh();
              return toast.success(`${userName}(${userId}) 회원의 인증상태가 '대기'로 수정되었습니다.`);
            }
            throw new Error('request fail');
          }

          throw new Error(`알 수 없는 인증 타입 : ${value}`);
        } catch (err) {
          toast.error(`${userName}(${userId}) 회원의 인증상태를 ${value}로 수정할 수 없습니다. : ${err.message}`);
          return this.setState({
            pending: false,
            exception: false,
            exceptionMessage: '',
          });
        }
      },
    );
  };

  onChangeOrderColumn = async columnId => {
    const { orderOption, searchOptions } = this.state;
    if (orderOption.column !== columnId) {
      orderOption.method = '';
    }
    orderOption.column = columnId;
    if (orderOption.method === 'DESC') {
      orderOption.method = 'ASC';
    } else if (orderOption.method === 'ASC') {
      orderOption.method = '';
    } else {
      orderOption.method = 'DESC';
    }
    return this.setState(
      {
        orderOption,
        pending: true,
      },
      async () => {
        const response = await this.search({
          searchOptions,
          orderOption,
          requestPage: 0,
        });
        if (response) {
          return this.setState({
            exception: false,
            pending: false,
            list: response.data,
            page: response.page.currentPage,
            maxPage: response.page.maxPage,
          });
        }
      },
    );
  };

  onClickSearch = async () => {
    const { searchOptions, orderOption } = this.state;
    return this.setState(
      {
        pending: true,
      },
      async () => {
        const response = await this.search({ searchOptions, orderOption, requestPage: 0 });
        if (response) {
          return this.setState({
            exception: false,
            pending: false,
            list: response.data,
            page: response.page.currentPage,
            maxPage: response.page.maxPage,
          });
        }
      },
    );
  };

  onExportCSV = () => {
    const queryString = qs.stringify(this.cachedOption);
    return window.open(`/api/manage/user/certifications/csv?${queryString}`);
  };

  onAddSearchOption = () => {
    const { searchOptions } = this.state;
    searchOptions.push({
      generatedKey: uuid(),
      filterType: '',
      filterValue: '',
      filterDataType: '',
    });
    return this.setState({ searchOptions });
  };

  onDeleteSearchOption = generatedKey => {
    const { searchOptions } = this.state;
    return this.setState({
      searchOptions: searchOptions.filter(item => {
        return item.generatedKey !== generatedKey;
      }),
    });
  };

  onChangeSearchOption = (generatedKey, type, value, dataType) => {
    const { searchOptions } = this.state;
    const targetIndex = searchOptions.findIndex(item => {
      return item.generatedKey === generatedKey;
    });
    if (targetIndex !== -1) {
      return this.setState({
        searchOptions: searchOptions.map((item, index) => {
          if (index === targetIndex) {
            return {
              ...item,
              filterType: type,
              filterValue: value,
              filterDataType: dataType,
            };
          }
          return item;
        }),
      });
    }
  };

  onClickPage = async (e, offset) => {
    this.cachedOption.requestPage = offset;
    return this.setState(
      {
        pending: true,
      },
      async () => {
        const response = await this.search(this.cachedOption);
        if (response) {
          return this.setState({
            exception: false,
            pending: false,
            list: response.data,
            page: response.page.currentPage,
            maxPage: response.page.maxPage,
          });
        }
      },
    );
  };

  openNotificationDialog = (title, message) => {
    return toast.info(`[${title || ''}] ${message} `);
  };

  /**
   * @param data
   * @param data.certTypeId
   * @param data.userTypeId
   */
  isMainCert = data => {
    const { userTypeId, certTypeId } = data;
    return UserMainCertTypeIdGroup[userTypeId] === certTypeId;
  };

  refresh = () => {
    return this.setState(
      {
        pending: true,
      },
      async () => {
        const response = await this.search(this.cachedOption);
        if (response) {
          return this.setState({
            exception: false,
            pending: false,
            list: response.data,
            page: response.page.currentPage,
            maxPage: response.page.maxPage,
          });
        }
      },
    );
  };

  closePreviewDialog = () => {
    return this.setState({ previewDialog: false });
  };

  closeRemoveCertConfirm = () => {
    return this.setState({ removeDialog: false });
  };

  /**
   *
   * @param options
   * @param options.searchOptions
   * @param options.orderOption
   * @param options.requestPage
   * @return {Promise<void>}
   */
  search = async options => {
    try {
      const query = qs.stringify(options);
      this.cachedOption = Object.assign({}, options);
      const response = await Request(`api/manage/user/certifications?${query}`, { method: 'GET' });
      if (response && response.result === ResponseResultTypeGroup.SUCCESS) {
        return response;
      }
      throw new Error(`request fail ${JSON.stringify(response)}`);
    } catch (err) {
      return this.setState({
        pending: false,
        exception: true,
        exceptionMessage: `검색중 오류가 발생하였습니다. [${err.message}]`,
      });
    }
  };

  /**
   * @param data
   * @param data.certTypeId
   * @return {*}
   */
  renderCertInfo = data => {
    const { classes } = this.props;
    const { certTypeId, certData, certId, userId } = data;
    try {
      const certDataParse = JSON.parse(certData);
      switch (certTypeId) {
        case CertificateTypeIdGroup.ENROLLMENT:
          return (
            <Grid container spacing={0}>
              <Grid item xs={12}>
                <Paper className={classes.paper}>
                  <Grid
                    container
                    spacing={8}
                    style={{ textAlign: 'left' }}
                    alignContent={'center'}
                    alignItems={'center'}
                  >
                    <Grid item md={1} style={{ textAlign: 'right' }}>
                      <Typography variant={'caption'}>발급일자</Typography>
                    </Grid>
                    <Grid item md={11} style={{ textAlign: 'left' }}>
                      <Typography variant={'body2'}>
                        {moment(certDataParse.issueDate).format('YYYY년 MM월 DD일')}
                      </Typography>
                    </Grid>
                    <Grid item md={1} style={{ textAlign: 'right' }}>
                      <Typography variant={'caption'}>학교</Typography>
                    </Grid>
                    <Grid item md={11} style={{ textAlign: 'left' }}>
                      <Typography variant={'body2'}>{certDataParse.univ || certDataParse.univName}</Typography>
                    </Grid>
                    <Grid item md={1} style={{ textAlign: 'right' }}>
                      <Typography variant={'caption'}>학과</Typography>
                    </Grid>
                    <Grid item md={11} style={{ textAlign: 'left' }}>
                      <Typography variant={'body2'}>{certDataParse.major}</Typography>
                    </Grid>
                    <Grid item md={1} style={{ textAlign: 'right' }}>
                      <Typography variant={'caption'}>학년</Typography>
                    </Grid>
                    <Grid item md={11} style={{ textAlign: 'left' }}>
                      <Typography variant={'body2'}>{certDataParse.grade || certDataParse.univGrade}</Typography>
                    </Grid>
                    <Grid item md={1} style={{ textAlign: 'right' }}>
                      <Typography variant={'caption'}>학위</Typography>
                    </Grid>
                    <Grid item md={11} style={{ textAlign: 'left' }}>
                      <Typography variant={'body2'}>
                        {certDataParse.degree === UnivDegreeTypeGroup.BACHELOR ? '학사' : ''}
                        {certDataParse.degree === UnivDegreeTypeGroup.MASTER ? '석사' : ''}
                        {certDataParse.degree === UnivDegreeTypeGroup.DOCTOR ? '박사' : ''}

                        {certDataParse.univDegree === UnivDegreeTypeGroup.BACHELOR ? '학사' : ''}
                        {certDataParse.univDegree === UnivDegreeTypeGroup.MASTER ? '석사' : ''}
                        {certDataParse.univDegree === UnivDegreeTypeGroup.DOCTOR ? '박사' : ''}
                      </Typography>
                    </Grid>
                    <Grid item md={1} style={{ textAlign: 'right' }}>
                      <Typography variant={'caption'}>학사상태</Typography>
                    </Grid>
                    <Grid item md={11} style={{ textAlign: 'left' }}>
                      <Typography variant={'body2'}>
                        {certDataParse.status === UnivStatusTypeGroup.ATTENDING ? '재학' : ''}
                        {certDataParse.status === UnivStatusTypeGroup.LEAVE ? '휴학' : ''}
                        {certDataParse.status === UnivStatusTypeGroup.GRADUATE ? '졸업' : ''}
                        {certDataParse.status === UnivStatusTypeGroup.WITHDRAW ? '퇴학' : ''}
                        {certDataParse.status === UnivStatusTypeGroup.EXPELLED ? '제적' : ''}

                        {certDataParse.univStatus === UnivStatusTypeGroup.ATTENDING ? '재학' : ''}
                        {certDataParse.univStatus === UnivStatusTypeGroup.LEAVE ? '휴학' : ''}
                        {certDataParse.univStatus === UnivStatusTypeGroup.GRADUATE ? '졸업' : ''}
                        {certDataParse.univStatus === UnivStatusTypeGroup.WITHDRAW ? '퇴학' : ''}
                        {certDataParse.univStatus === UnivStatusTypeGroup.EXPELLED ? '제적' : ''}
                      </Typography>
                    </Grid>

                    {certDataParse.status === UnivStatusTypeGroup.GRADUATE ||
                    certDataParse.univStatus === UnivStatusTypeGroup.GRADUATE ? (
                      <Grid container spacing={16}>
                        <Grid item md={1} style={{ textAlign: 'right' }}>
                          <Typography variant={'caption'}>졸업일자</Typography>
                        </Grid>
                        <Grid item md={11} style={{ textAlign: 'left' }}>
                          <Typography variant={'body2'}>
                            {moment(certDataParse.graduateDate).format('YYYY년 MM월 DD일')}
                          </Typography>
                        </Grid>
                      </Grid>
                    ) : null}

                    {certDataParse.status === UnivStatusTypeGroup.EXPELLED ||
                    certDataParse.univStatus === UnivStatusTypeGroup.EXPELLED ? (
                      <Grid container spacing={16}>
                        <Grid item md={1} style={{ textAlign: 'right' }}>
                          <Typography variant={'caption'}>제적일자</Typography>
                        </Grid>
                        <Grid item md={11} style={{ textAlign: 'left' }}>
                          <Typography variant={'body2'}>
                            {moment(certDataParse.expelledDate).format('YYYY년 MM월 DD일')}
                          </Typography>
                        </Grid>
                      </Grid>
                    ) : null}
                    <Grid item xs={12} style={{ textAlign: 'right' }}>
                      <Button
                        variant={'raised'}
                        color={'secondary'}
                        fullWidth
                        onClick={() => this.onClickEditCertData(data)}
                      >
                        인증정보 수정
                      </Button>
                    </Grid>
                  </Grid>
                </Paper>
              </Grid>
            </Grid>
          );
        case CertificateTypeIdGroup.MEDICAL_EXAMINATION:
          return (
            <Grid container spacing={0}>
              <Grid item xs={12}>
                <Paper className={classes.paper}>
                  <Grid container spacing={16} alignContent={'center'} alignItems={'center'}>
                    <Grid item md={1} style={{ textAlign: 'right' }}>
                      <Typography variant={'caption'}>발급일자</Typography>
                    </Grid>
                    <Grid item md={11} style={{ textAlign: 'left' }}>
                      <Typography variant={'body2'}>
                        {moment(certDataParse.issueDate).format('YYYY년 MM월 DD일')}
                      </Typography>
                    </Grid>
                    <Grid item xs={12} style={{ textAlign: 'right' }}>
                      <Button
                        variant={'raised'}
                        color={'secondary'}
                        fullWidth
                        onClick={() => this.onClickEditCertData(data)}
                      >
                        인증정보 수정
                      </Button>
                    </Grid>
                  </Grid>
                </Paper>
              </Grid>
            </Grid>
          );
        case CertificateTypeIdGroup.FAMILY_RELATION:
          return (
            <Grid container spacing={0}>
              <Grid item xs={12}>
                <Paper className={classes.paper}>
                  <Grid container spacing={16} alignItems={'center'} alignContent={'center'}>
                    <Grid item md={1} style={{ textAlign: 'right' }}>
                      <Typography variant={'caption'}>발급일자</Typography>
                    </Grid>
                    <Grid item md={11} style={{ textAlign: 'left' }}>
                      <Typography variant={'body2'}>
                        {moment(certDataParse.issueDate).format('YYYY년 MM월 DD일')}
                      </Typography>
                    </Grid>
                    <Grid item md={1} style={{ textAlign: 'right' }}>
                      <Typography variant={'caption'}>자녀수</Typography>
                    </Grid>
                    <Grid item md={11} style={{ textAlign: 'left' }}>
                      <Typography variant={'body2'}>
                        {certDataParse.childCount || certDataParse.children.length}
                      </Typography>
                    </Grid>
                    <Grid item md={1} style={{ textAlign: 'right' }}>
                      <Typography variant={'caption'}>아이정보</Typography>
                    </Grid>
                    <Grid item md={10}>
                      {certDataParse.children.map((item, index) => {
                        return (
                          <Grid
                            container
                            spacing={16}
                            key={`cert-info-child-item-${certId}-${index}`}
                            style={{ textAlign: 'left' }}
                          >
                            <Grid item xs={12}>
                              <Paper className={classes.paper}>
                                <Grid container spacing={16}>
                                  <Grid item md={1} style={{ textAlign: 'right' }}>
                                    <Typography variant={'caption'}>성별</Typography>
                                  </Grid>
                                  <Grid item md={11} style={{ textAlign: 'left' }}>
                                    <Typography variant={'body2'}>
                                      {item.gender === 'man' ? '남자' : ''}
                                      {item.gender === 'women' ? '여자' : ''}
                                    </Typography>
                                  </Grid>
                                  <Grid item md={1} style={{ textAlign: 'right' }}>
                                    <Typography variant={'caption'}>생년월일</Typography>
                                  </Grid>
                                  <Grid item md={11} style={{ textAlign: 'left' }}>
                                    <Typography variant={'body2'}>
                                      {moment(item.birthday).format('YYYY년 MM월 DD일')}
                                    </Typography>
                                  </Grid>
                                </Grid>
                              </Paper>
                            </Grid>
                          </Grid>
                        );
                      })}
                    </Grid>
                    <Grid item xs={12} style={{ textAlign: 'right' }}>
                      <Button
                        variant={'raised'}
                        color={'secondary'}
                        fullWidth
                        onClick={() => this.onClickEditCertData(data)}
                      >
                        인증정보 수정
                      </Button>
                    </Grid>
                  </Grid>
                </Paper>
              </Grid>
            </Grid>
          );
        case CertificateTypeIdGroup.CHILDCARE:
          return (
            <Grid container spacing={0}>
              <Grid item xs={12}>
                <Paper className={classes.paper}>
                  <Grid container spacing={16} alignItems={'center'} alignContent={'center'}>
                    <Grid item md={1} style={{ textAlign: 'right' }}>
                      <Typography variant={'caption'}>발급일자</Typography>
                    </Grid>
                    <Grid item md={11} style={{ textAlign: 'left' }}>
                      <Typography variant={'body2'}>
                        {moment(certDataParse.issueDate).format('YYYY년 MM월 DD일')}
                      </Typography>
                    </Grid>
                    <Grid item md={1} style={{ textAlign: 'right' }}>
                      <Typography variant={'caption'}>자격증 이름</Typography>
                    </Grid>
                    <Grid item md={11} style={{ textAlign: 'left' }}>
                      <Typography variant={'body2'}>{certDataParse.qualificationType || '없음'}</Typography>
                    </Grid>
                    <Grid item xs={12} style={{ textAlign: 'right' }}>
                      <Button
                        variant={'raised'}
                        color={'secondary'}
                        fullWidth
                        onClick={() => this.onClickEditCertData(data)}
                      >
                        인증정보 수정
                      </Button>
                    </Grid>
                  </Grid>
                </Paper>
              </Grid>
            </Grid>
          );
        case CertificateTypeIdGroup.RESIDENT_REGISTRATION:
          return (
            <Grid container spacing={0}>
              <Grid item xs={12}>
                <Paper className={classes.paper}>
                  <Grid container spacing={16} alignContent={'center'} alignItems={'center'}>
                    <Grid item md={1} style={{ textAlign: 'right' }}>
                      <Typography variant={'caption'}>발급일자</Typography>
                    </Grid>
                    <Grid item md={11} style={{ textAlign: 'left' }}>
                      <Typography variant={'body2'}>
                        {moment(certDataParse.issueDate).format('YYYY년 MM월 DD일')}
                      </Typography>
                    </Grid>
                    <Grid item md={1} style={{ textAlign: 'right' }}>
                      <Typography variant={'caption'}>주소</Typography>
                    </Grid>
                    <Grid item md={11} style={{ textAlign: 'left' }}>
                      <Typography variant={'body2'}>
                        {typeof certDataParse.location === 'string' ? certDataParse.location : ''}
                        {certDataParse.location instanceof Object ? `${certDataParse.location.jibunAddr}` : ''}
                      </Typography>
                    </Grid>
                    <Grid item xs={12} style={{ textAlign: 'right' }}>
                      <Button
                        variant={'raised'}
                        color={'secondary'}
                        fullWidth
                        onClick={() => this.onClickEditCertData(data)}
                      >
                        인증정보 수정
                      </Button>
                    </Grid>
                  </Grid>
                </Paper>
              </Grid>
            </Grid>
          );
        case CertificateTypeIdGroup.PERSONALITY:
          return (
            <Grid container spacing={0}>
              <Grid item xs={12}>
                <Paper className={classes.paper}>
                  <Grid container spacing={16} alignContent={'center'} alignItems={'center'}>
                    <Grid item xs={12}>
                      <Button variant={'raised'} color={'primary'} fullWidth onClick={() => this.onClickOpenResult(userId)}>
                        인성검사 결과보기
                      </Button>
                    </Grid>
                  </Grid>
                </Paper>
              </Grid>
            </Grid>
          );
        default:
          throw new Error(`Unknown certTypeId : ${certTypeId}`);
      }
      throw new Error(`cannot display certInfo ${JSON.stringify(data)}`);
    } catch (err) {
      return (
        <Grid container spacing={0}>
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <Grid container spacing={16}>
                <Grid item xs={12}>
                  <Typography variant={'caption'}>
                    인증 상세 정보를 표시할 수 없습니다. : {err.message} : {JSON.stringify(certData)}
                  </Typography>
                </Grid>
                {certTypeId !== CertificateTypeIdGroup.PERSONALITY ? (
                  <Grid item xs={12} style={{ textAlign: 'right' }}>
                    <Button
                      variant={'raised'}
                      color={'secondary'}
                      fullWidth
                      onClick={() => this.onClickEditCertData(data)}
                    >
                      인증정보 수정
                    </Button>
                  </Grid>
                ) : (
                  ''
                )}
              </Grid>
            </Paper>
          </Grid>
        </Grid>
      );
    }
  };

  renderDataHeader = () => {
    const { classes } = this.props;
    const header = [
      {
        label: '메인인증',
        column: 'certLabelName',
        grid: 1,
        sortable: false,
      },
      {
        label: '인증번호',
        column: 'certId',
        grid: 1,
        sortable: true,
      },
      {
        label: '회원번호',
        column: 'userId',
        grid: 1,
        sortable: true,
      },
      {
        label: '회원이름',
        column: 'userName',
        grid: 1,
        sortable: true,
      },
      {
        label: '회원종류',
        column: 'userTypeId',
        grid: 1,
        sortable: true,
      },
      {
        label: '인증타입',
        column: 'certTypeId',
        grid: 1,
        sortable: true,
      },
      {
        label: '인증상태',
        column: 'certStatus',
        grid: 1,
        sortable: true,
      },
      {
        label: '등록일자',
        column: 'regDate',
        grid: 1,
        sortable: true,
      },
      {
        label: '수정된일자',
        column: 'updateDate',
        grid: 1,
        sortable: true,
      },
      {
        label: '발급일자',
        column: 'issueDate',
        grid: 1,
        sortable: true,
      },
      {
        label: '인증파일',
        column: 'fileId',
        grid: 1,
        sortable: true,
      },
      {
        label: '',
        column: '',
        grid: 1,
        sortable: false,
      },
    ];

    return (
      <Grid container spacing={0}>
        <Grid item xs={12}>
          <Paper className={classes.paper}>
            <Grid container spacing={0}>
              {header.map((item, index) => {
                const { label, grid, sortable, column } = item;
                return (
                  <Grid item xs={grid} key={`header-item-${index}`}>
                    <Button
                      variant={'flat'}
                      fullWidth
                      disabled={!sortable}
                      onClick={() => this.onChangeOrderColumn(column)}
                    >
                      {this.renderArrow(item)}
                      <Typography variant={'caption'}> {label} </Typography>
                    </Button>
                  </Grid>
                );
              })}
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    );
  };

  renderDataContent = () => {
    const { pending, exception, exceptionMessage, list } = this.state;
    if (exception) {
      return this.renderDataException(exceptionMessage);
    }
    if (pending) {
      return this.renderDataPending();
    }

    if (!list) {
      return this.renderDataEmpty();
    }

    if (list.length === 0) {
      return this.renderDataEmpty();
    }

    return (
      <Grid container spacing={0}>
        <Grid item xs={12}>
          {this.renderDataItems(list)}
        </Grid>
        <Grid item xs={12}>
          {this.renderDataPagination()}
        </Grid>
      </Grid>
    );
  };

  /**
   * @param data
   * @param data.column
   * @param data.grid
   * @param data.sortable
   * @param data.label
   */
  renderArrow = data => {
    const { orderOption } = this.state;
    if (data.column === orderOption.column) {
      if (orderOption.method === 'ASC') {
        return <ArrowUpIcon style={{ width: '15px', height: '15px' }} />;
      }
      if (orderOption.method === 'DESC') {
        return <ArrowDownIcon style={{ width: '15px', height: '15px' }} />;
      }
    }
    return null;
  };

  renderDataItems = list => {
    const { classes } = this.props;
    const { expandItems } = this.state;
    return list.map((item, index) => {
      const {
        auth,
        certApprovedMsg,
        certData,
        certId,
        certInfoMsg,
        certRejectedMsg,
        certStatus,
        certType,
        certTypeDesc,
        certTypeId,
        certTypeName,
        certWaitingMsg,
        contactWay,
        contactWayDetail,
        fileId,
        fileName,
        fileSize,
        fileUrl,
        fileUuid,
        isOwnPhone,
        issueDate,
        joinType,
        mimeType,
        referenceType,
        regDate,
        rejectMsg,
        tag,
        updateDate,
        uploadDate,
        userAge,
        userAssignMentMsg,
        userAssignmentPp,
        userAssignmentTos,
        userBirthday,
        userConnectType,
        userDetailTypeDesc,
        userDup,
        userEmail,
        userGender,
        userId,
        userKey,
        userName,
        userNationality,
        userPassword,
        userPhone,
        userSearchable,
        userSignUpDate,
        userSnsKey,
        userStatus,
        userTypeId,
        certLabelName,
      } = item;
      const isMainCertification = this.isMainCert({ userTypeId, certTypeId });
      return (
        <Grid container spacing={0} key={`manage-parent-item-${certId}`}>
          <Grid item xs={12}>
            <Paper className={classes.paper} style={{ backgroundColor: '#F4F8FB' }}>
              <Grid container spacing={0} alignContent={'center'} justify={'center'} alignItems={'center'}>
                <Grid item xs={1}>
                  {isMainCertification ? (
                    <Tooltip title={certLabelName || '메인 인증 라벨 미등록'} placement={'right'}>
                      <MainCertIcon color={'primary'} />
                    </Tooltip>
                  ) : (
                    ''
                  )}
                </Grid>
                <Grid item xs={1}>
                  <Typography variant={'body1'}> {certId} </Typography>
                </Grid>
                <Grid item xs={1}>
                  <Button variant={'flat'} onClick={() => this.onClickUserId(userId)} fullWidth>
                    <Typography variant={'body1'}> {userId} </Typography>
                  </Button>
                </Grid>
                <Grid item xs={1}>
                  <Typography variant={'body1'}> {userName} </Typography>
                </Grid>
                <Grid item xs={1}>
                  <Typography variant={'body1'}> {userDetailTypeDesc} </Typography>
                </Grid>
                <Grid item xs={1}>
                  <Typography variant={'body1'}> {certTypeName} </Typography>
                </Grid>
                <Grid item xs={1}>
                  <Tooltip title={certStatus === CertificateStatusGroup.REJECTED ? rejectMsg || '반려사유 없음' : ''}>
                    <Select
                      value={certStatus}
                      onChange={event =>
                        this.onChangeCertStatus(event, {
                          certId,
                          userId,
                          userName,
                          userTypeId,
                          certTypeId,
                          certData,
                          certStatus,
                        })
                      }
                    >
                      <MenuItem value={CertificateStatusGroup.WAITING}> 대기 </MenuItem>
                      <MenuItem value={CertificateStatusGroup.APPROVED}> 승인 </MenuItem>
                      <MenuItem value={CertificateStatusGroup.REJECTED}> 반려 </MenuItem>
                    </Select>
                  </Tooltip>
                </Grid>
                <Grid item xs={1}>
                  <Typography variant={'body1'}> {moment(regDate).format('YYYY년 MM월 DD일')} </Typography>
                  <Typography variant={'caption'}> {moment(regDate).format('HH시 mm분')} </Typography>
                </Grid>
                <Grid item xs={1}>
                  <Typography variant={'body1'}> {moment(updateDate).format('YYYY년 MM월 DD일')} </Typography>
                  <Typography variant={'caption'}> {moment(updateDate).format('HH시 mm분')} </Typography>
                </Grid>
                <Grid item xs={1}>
                  <Typography variant={'body1'}> {moment(issueDate).format('YYYY년 MM월 DD일')} </Typography>
                  <Typography variant={'caption'}> {moment(issueDate).format('HH시 mm분')} </Typography>
                </Grid>
                <Grid item xs={1}>
                  {/**
                   * @TODO 인증파일 미리보기 기능
                   */}
                  {fileId && fileUrl ? (
                    <Tooltip title={'인증 파일 다운로드'}>
                      <IconButton onClick={() => this.onClickFile({ fileId, fileUrl })}>
                        <SaveIcon />
                      </IconButton>
                    </Tooltip>
                  ) : (
                    <Typography variant={'caption'}> 파일이 없습니다. </Typography>
                  )}
                </Grid>
                <Grid item xs={1}>
                  <IconButton onClick={() => this.onClickRemoveCert(certId)}>
                    <DeleteIcon />
                  </IconButton>
                  <IconButton onClick={() => this.onClickExpand(certId)}>
                    {expandItems[certId] ? <ExpandLessIcon /> : <ExpandMoreIcon />}
                  </IconButton>
                </Grid>
              </Grid>
              <Collapse in={!!expandItems[certId]}>
                <Grid container spacing={16}>
                  <Grid item xs={12}>
                    <Divider light />
                  </Grid>
                  {this.renderCertInfo(item)}
                </Grid>
              </Collapse>
            </Paper>
          </Grid>
        </Grid>
      );
    });
  };

  renderDataPagination = () => {
    const { classes } = this.props;
    const { page, maxPage } = this.state;
    return (
      <Paper className={classes.paper} style={{ textAlign: 'left' }}>
        <Grid container spacing={0}>
          <Grid item xs={12}>
            <FlatPagination offset={page} limit={1} total={maxPage} onClick={this.onClickPage} />
          </Grid>
        </Grid>
      </Paper>
    );
  };

  renderDataEmpty = () => {
    const { classes } = this.props;
    return (
      <Grid container spacing={0} justify={'center'} alignContent={'center'} alignItems={'center'}>
        <Grid item xs={12}>
          <Paper className={classes.paper} style={{ backgroundColor: '#F4F8FB' }}>
            <Grid item xs={12}>
              <WarningIcon />
            </Grid>
            <Grid item xs={12}>
              <Typography variant={'caption'}> 데이터가 없습니다. </Typography>
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    );
  };

  renderDataException = exceptionMessage => {
    const { classes } = this.props;
    return (
      <Grid container spacing={0}>
        <Grid item xs={12}>
          <Paper className={classes.paper} style={{ backgroundColor: '#F4F8FB' }}>
            <Grid container spacing={16}>
              <Grid item xs={12}>
                <ErrorIcon />
              </Grid>
              <Grid item xs={12}>
                <Typography variant={'caption'}> {exceptionMessage} </Typography>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    );
  };

  renderDataPending = () => {
    const { classes } = this.props;
    return (
      <Grid container spacing={0}>
        <Grid item xs={12}>
          <Paper className={classes.paper} style={{ backgroundColor: '#F4F8FB' }}>
            <Grid container spacing={16}>
              <Grid item xs={12}>
                <CircularProgress />
              </Grid>
              <Grid item xs={12}>
                <Typography variant={'caption'}> 데이터를 불러오는 중입니다. </Typography>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    );
  };

  render() {
    const { searchOptions } = this.state;
    const filter = [
      {
        name: '신청번호',
        type: 'certId',
        dataType: FilterDataTypeGroup.UNIQUE_TEXT,
      },
      {
        name: '사용자번호',
        type: 'userId',
        dataType: FilterDataTypeGroup.UNIQUE_TEXT,
      },
      {
        name: '인증 타입',
        type: 'certTypeId',
        dataType: FilterDataTypeGroup.CERT_TYPE,
      },
      {
        name: '인증 상태',
        type: 'certStatus',
        dataType: FilterDataTypeGroup.CERT_STATUS,
      },
      {
        name: '갱신일자',
        type: 'updateDate',
        dataType: FilterDataTypeGroup.DATE_RANGE,
      },
      {
        name: '발급일자',
        type: 'issueDate',
        dataType: FilterDataTypeGroup.DATE_RANGE,
      },
      {
        name: '등록일자',
        type: 'regDate',
        dataType: FilterDataTypeGroup.DATE_RANGE,
      },
    ];

    return (
      <Paper style={{ backgroundColor: 'transparent' }}>
        <Grid container spacing={0}>
          <Grid item xs={12}>
            <SearchBar
              filter={filter}
              searchOptions={searchOptions}
              onClickSearch={this.onClickSearch}
              onExportCSV={this.onExportCSV}
              onClickAddSearchOption={this.onAddSearchOption}
              onClickDeleteSearchOption={this.onDeleteSearchOption}
              onChangeSearchOption={this.onChangeSearchOption}
              openNotificationDialog={this.openNotificationDialog}
            />
          </Grid>
        </Grid>
        {this.renderDataHeader()}
        {this.renderDataContent()}
        <CertRejectDialog />
        <CertDataEditDialog />
        <Dialog open={this.state.removeDialog}>
          <DialogTitle>정말 삭제하시겠습니까?</DialogTitle>
          <DialogActions>
            <Button variant={'flat'} onClick={this.closeRemoveCertConfirm} style={{ marginRight: '10px' }}>취소</Button>
            <Button variant={'raised'} color={'secondary'} onClick={this.onClickRemoveCertConfirm}>삭제</Button>
          </DialogActions>
        </Dialog>
        <Dialog open={this.state.previewDialog}>
          <DialogTitle>인증파일 미리보기</DialogTitle>
          <DialogContent>
            <Paper style={{ padding: '10px' }}>
              <Grid container spacing={16}>
                <Grid item xs={12}>
                  <img
                    src={this.state.previewFileUrl}
                    style={{ width: '100%', maxHeight: '900px' }}
                  />
                </Grid>
              </Grid>
            </Paper>
          </DialogContent>
          <DialogActions>
            <Button variant={'raised'} onClick={this.onClickDownloadFile} style={{ marginRight: '10px' }}>인증 파일 다운로드</Button>
            <Button variant={'raised'} color={'primary'} onClick={this.closePreviewDialog}>닫기</Button>
          </DialogActions>
        </Dialog>
        <PersonalityResult/>
      </Paper>
    );
  }
}

ManageUserCertifications.propTypes = propTypes;
ManageUserCertifications.defaultProps = defaultProps;

export default compose(
  connect(
    state => ({}),
    dispatch => ({
      CertDataEditDialogActions: bindActionCreators(certDataEditDialogActions, dispatch),
      CertRejectDialogActions: bindActionCreators(certRejectDialogActions, dispatch),
      PersonalityResultDrawerActions: bindActionCreators(personalityResultDrawerActions, dispatch),
    }),
  ),
  withStyles(s),
  withMaterialStyles(styles, { withTheme: true }),
)(ManageUserCertifications);
