import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ManageUserReports.css';
import ManageUserReport from '../../components/Manage/UserReports';

class ManageUserReports extends React.Component {
  render() {
    return (
      <div className={s.root}>
        <h2 className="article-title">회원 - 신고현황</h2>
        <ManageUserReport />
      </div>
    );
  }
}

ManageUserReports.propTypes = {};

ManageUserReports.defaultProps = {};

export default withStyles(s)(ManageUserReports);
