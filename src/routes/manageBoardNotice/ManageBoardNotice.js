/* eslint-disable prettier/prettier */
import qs from 'qs';
import uuid from 'uuid/v4';
import { compose } from 'recompose';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import ErrorIcon from '@material-ui/icons/Error';
import WarningIcon from '@material-ui/icons/Warning';
import Button from '@material-ui/core/Button';
import FlatPagination from 'material-ui-flat-pagination';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import AddIcon from '@material-ui/icons/Add';
import Snackbar from '@material-ui/core/Snackbar';

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ManageBoardNotice.css';

import SearchBar from '../../components/SearchBar';
import BoardNoticeItem from '../../components/BoardNoticeItem';
import ResponseResultTypeGroup from '../../util/const/ResponseResultTypeGroup';
import Request from '../../util/Request';
import FilterDataTypeGroup from '../../util/const/FilterDataTypeGroup';
import AddNoticeDialog from '../../components/AddNoticeDialog';
import history from '../../history';

const propTypes = {};
const defaultProps = {};

class ManageBoardNotice extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      list: [],
      pending: true,
      error: false,
      page: 0,
      maxPage: 0,
      searchOptions: [],
      orderOption: {
        column: '',
        method: '',
      },
      addDialog: false,
      snackbar: false,
      snackbarMessage: '',
    };
    this.cachedOption = {};
    this.onClickSearch = this.onClickSearch.bind(this);
    this.search = this.search.bind(this);
    this.onChangeOrderColumn = this.onChangeOrderColumn.bind(this);
    this.onSaveAddDialog = this.onSaveAddDialog.bind(this);
    this.openSnackbar = this.openSnackbar.bind(this);
    this.closeSnackbar = this.closeSnackbar.bind(this);
    this.onClickDeleteItem = this.onClickDeleteItem.bind(this);
  }

  async componentDidMount() {
    try {
      const searchResult = await this.search();
      this.setState({
        list: searchResult.data.rows,
        page: searchResult.page.currentPage,
        maxPage: searchResult.page.maxPage,
        pending: false,
        error: false,
      });
    } catch (err) {
      this.setState({
        pending: false,
        error: true,
      });
    }
  }

  onClickSearch = async function () {
    try {
      this.setState({
        pending: true,
      }, async function () {
        const { searchOptions, orderOption } = this.state;
        const result = await this.search({
          searchOptions,
          orderOption,
          requestPage: 0,
        });

        this.setState({
          pending: false,
          list: result.data.rows,
          page: result.page.currentPage,
          maxPage: result.page.maxPage,
          error: false,
        });
      });
    } catch (err) {
      this.setState({
        pending: false,
        error: true,
      });
    }
  };

  onExportCSV = () => {
    // const queryString = qs.stringify(this.cachedOption);
    //window.open(`/api/points/csv?${queryString}`);
  };

  onChangeSearchOption = (generatedKey, type, value, dataType) => {
    const { searchOptions } = this.state;
    const targetIndex = searchOptions.findIndex((item) => {
      return item.generatedKey === generatedKey;
    });
    if (targetIndex !== -1) {
      searchOptions[targetIndex] = {
        ...searchOptions[targetIndex],
        filterType: type,
        filterValue: value,
        filterDataType: dataType,
      };
      return this.setState({ searchOptions });
    } else {
      console.log('DEBUG : ON CHANGE BUT NOT FOUND : ' + generatedKey);
    }
  };

  onDeleteSearchOption = (generatedKey) => {
    const { searchOptions } = this.state;
    return this.setState({
      searchOptions: searchOptions.filter((item) => {
        return item.generatedKey !== generatedKey;
      }),
    });
  };

  onAddSearchOption = () => {
    const { searchOptions } = this.state;
    searchOptions.push({
      generatedKey: uuid(),
      filterType: '',
      filterValue: '',
      filterDataType: '',
    });
    return this.setState({ searchOptions });
  };

  onChangeOrderColumn = async function (columnId) {
    try {
      const { orderOption, searchOptions } = this.state;
      if (orderOption.column !== columnId) {
        orderOption.method = '';
      }
      orderOption.column = columnId;
      if (orderOption.method === 'DESC') {
        orderOption.method = 'ASC';
      } else if (orderOption.method === 'ASC') {
        orderOption.method = '';
      } else {
        orderOption.method = 'DESC'
      }

      return this.setState({
        orderOption,
        pending: true,
      }, async function () {
        const result = await this.search({
          searchOptions,
          orderOption,
          requestPage: 0,
        });
        return this.setState({
          list: result.data.rows,
          page: result.page.currentPage,
          maxPage: result.page.maxPage,
          pending: false,
        });
      });
    } catch (err) {
      console.log(err);
      return this.setState({
        error: true,
      });
    }
  };

  onClickPage = async function (value) {
    try {
      this.cachedOption.requestPage = value;
      return this.setState({
        pending: true,
      }, async function () {
        const result = await this.search(this.cachedOption);
        return this.setState({
          list: result.data.rows,
          page: result.page.currentPage,
          maxPage: result.page.maxPage,
          pending: false,
          error: false,
        });
      });
    } catch (err) {
      return this.setState({
        error: true,
      });
    }
  };

  onSaveAddDialog = async function (data) {
    try {
      const response = await Request('api/boards/notice', {
        method: 'POST',
        data,
      });
      if (response.result === ResponseResultTypeGroup.SUCCESS) {
        this.openSnackbar('저장되었습니다.');
        this.closeAddDialog();
        return history.go(0);
      }
      throw new Error('request fail');
    } catch (err) {
      console.log(err);
      this.openSnackbar('게시글 작성중 오류가 발생했습니다.');
      return false;
    }
  };

  onClickEditItem = (id) => {
    return history.push(`/manage/board/notice/${id}`);
  };

  onClickDeleteItem = async function (id) {
    try {
      if (confirm(`정말 ${id} 게시글을 삭제하시겠습니까?`)) {
        const response = await Request(`api/boards/notice/${id}`, {
          method: 'DELETE',
        });
        if (response.result === ResponseResultTypeGroup.SUCCESS) {
          this.openSnackbar('삭제되었습니다.');
          return history.go(0);
        }
        throw new Error('request fail');
      }
      return false;
    } catch (err) {
      console.log(err);
      return this.openSnackbar('삭제중 오류가 발생했습니다.');
    }
  };

  openAddDialog = () => {
    return this.setState({
      addDialog: true,
    });
  };

  closeAddDialog = () => {
    return this.setState({
      addDialog: false,
    });
  };

  openSnackbar = (message) => {
    return this.setState({
      snackbar: true,
      snackbarMessage: message,
    });
  };

  closeSnackbar = () => {
    return this.setState({
      snackbar: false,
    });
  };

  search = async function (options) {
    try {
      const query = qs.stringify(options);
      this.cachedOption = Object.assign({}, options);
      const response = await Request(`api/boards/notice?${query}`, { method: 'GET' });
      if (response.result === ResponseResultTypeGroup.SUCCESS) {
        return response;
      }
      throw new Error('request fail');
    } catch (err) {
      throw new Error(err);
    }
  };

  renderPagenation = () => {
    const { page, maxPage } = this.state;
    return (
      <Paper style={{ padding: '0.8rem' }}>
        <FlatPagination
          offset={page}
          limit={1}
          total={maxPage}
          onClick={(e, offset) => this.onClickPage(offset)}
        />
      </Paper>
    );
  };

  renderArrow = (columnId) => {
    const { orderOption } = this.state;
    if (columnId === orderOption.column) {
      if (orderOption.method === 'DESC') {
        return (
          <ArrowDownwardIcon style={{ width: '15px', height: '15px' }} />
        );
      }
      if (orderOption.method === 'ASC') {
        return (
          <ArrowUpwardIcon style={{ width: '15px', height: '15px' }} />
        );
      }
    }
    return null;
  };

  renderPointHeader = () => {
    return (
      <Paper style={{ padding: '0.8rem' }}>
        <Grid
          container
          spacing={16}
          alignItems='center'
          alignContent='center'
          justify='center'
          direction='row'
        >
          <Grid item xs={1} />
          <Grid item xs={1} style={{ textAlign: 'center' }}>
            <Button onClick={() => this.onChangeOrderColumn('id')}>
              {this.renderArrow('id')}
              <Typography variant='caption'>
                게시글 번호
              </Typography>
            </Button>
          </Grid>
          <Grid item xs={1} style={{ textAlign: 'center' }}>
            <Button onClick={() => this.onChangeOrderColumn('author')}>
              {this.renderArrow('author')}
              <Typography variant='caption'>
                작성자
              </Typography>
            </Button>
          </Grid>
          <Grid item xs={3} style={{ textAlign: 'center' }}>
            <Button onClick={() => this.onChangeOrderColumn('title')}>
              {this.renderArrow('title')}
              <Typography variant='caption'>
                재목
              </Typography>
            </Button>
          </Grid>
          <Grid item xs={2} style={{ textAlign: 'center' }}>
            <Button onClick={() => this.onChangeOrderColumn('registerDate')}>
              {this.renderArrow('registerDate')}
              <Typography variant='caption'>
                등록일자
              </Typography>
            </Button>
          </Grid>
          <Grid item xs={2} style={{ textAlign: 'center' }}>
            <Button onClick={() => this.onChangeOrderColumn('updateDate')}>
              {this.renderArrow('updateDate')}
              <Typography variant='caption'>
                최종수정일자
              </Typography>
            </Button>
          </Grid>
          <Grid item xs={1} style={{ textAlign: 'center' }}>
            <Button onClick={() => this.onChangeOrderColumn('view')}>
              {this.renderArrow('view')}
              <Typography variant='caption'>
                조회수
              </Typography>
            </Button>
          </Grid>
          <Grid item xs={1} style={{ textAlign: 'center' }} />
        </Grid>
      </Paper>
    );
  };

  renderPointItems = () => {
    const { list } = this.state;
    return list.map((item, index) => {
      return (
        <BoardNoticeItem
          data={item}
          key={`board-notice-list-item-${index}`}
          onClickEdit={this.onClickEditItem}
          onClickDelete={this.onClickDeleteItem}
        />
      );
    });
  };

  renderPointList = () => {
    try {
      const { list } = this.state;
      if (list.length === 0) {
        return this.renderEmptyUI();
      }
      return (
        <div>
          {this.renderPointHeader()}
          {this.renderPointItems()}
          {this.renderPagenation()}
        </div>
      );
    } catch (err) {
      console.log(err);
      return this.renderEmptyUI();
    }
  };

  renderEmptyUI = () => {
    return (
      <Paper style={{ padding: '0.8rem' }}>
        <div className={s.flexRow}>
          <WarningIcon style={{ width: '25px', height: '25px' }} />
          <Typography variant='body2'> 조건에 맞는 데이터가 없습니다. </Typography>
        </div>
      </Paper>
    )
  };

  renderPendingUI = () => {
    return (
      <Paper style={{ padding: '0.8rem' }}>
        <div className={s.flexRow}>
          <CircularProgress style={{ marginRight: '10px' }} />
          <Typography variant='body2'> 데이터를 불러오는 중입니다. </Typography>
        </div>
      </Paper>
    );
  };

  renderErrorUI = () => {
    return (
      <Paper style={{ padding: '1.25rem' }}>
        <div className={s.flexRow}>
          <ErrorIcon style={{ width: '25px', height: '25px' }} />
          <div> 오류가 발생했습니다. 다시 시도해 주세요. </div>
        </div>
      </Paper>
    );
  };

  renderContent = () => {
    try {
      const { pending, error } = this.state;
      if (pending) {
        return this.renderPendingUI();
      }
      if (error) {
        return this.renderErrorUI();
      }
      return this.renderPointList();
    } catch (err) {
      return this.renderErrorUI();
    }
  };

  render() {
    const { addDialog, snackbar, snackbarMessage } = this.state;
    const { searchOptions } = this.state;
    const filter = [
      { name: '게시글 번호', type: 'id', dataType: FilterDataTypeGroup.UNIQUE_TEXT },
      { name: '작성자', type: 'author', dataType: FilterDataTypeGroup.TEXT },
      { name: '제목', type: 'title', dataType: FilterDataTypeGroup.TEXT },
      { name: '등록일자', type: 'registerDate', dataType: FilterDataTypeGroup.DATE_RANGE },
      { name: '최종수정일자', type: 'updateDate', dataType: FilterDataTypeGroup.DATE_RANGE },
    ];

    return (
      <div>
        <Paper style={{ borderRadius: '5px' }}>
          <AppBar position='static' style={{ backgroundColor: '#fff', borderRadius: '5px 5px 0 0' }}>
            <Toolbar>
              <Typography variant='title' style={{ color: '#717C97' }}> 공지사항 관리 </Typography>
            </Toolbar>
          </AppBar>
          <SearchBar
            filter={filter}
            searchOptions={searchOptions}
            onClickSearch={this.onClickSearch}
            onClickAddSearchOption={this.onAddSearchOption}
            onClickDeleteSearchOption={this.onDeleteSearchOption}
            onChangeSearchOption={this.onChangeSearchOption}
            onExportCSV={this.onExportCSV}
          >
            <Button variant={'raised'} onClick={this.openAddDialog}>
              <AddIcon style={{ width: '20px', height: '20px'}} />
              <Typography variant={'button'}> 게시글 추가 </Typography>
            </Button>
          </SearchBar>
          <div className={s.container}>
            {this.renderContent()}
          </div>
        </Paper>
        <AddNoticeDialog
          open={addDialog}
          onSave={this.onSaveAddDialog}
          onClose={this.closeAddDialog}
        />
        <Snackbar
          open={snackbar}
          message={<span> {snackbarMessage} </span>}
          onClose={this.closeSnackbar}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
        />
      </div>
    );
  }
}

ManageBoardNotice.propTypes = propTypes;
ManageBoardNotice.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(ManageBoardNotice);
