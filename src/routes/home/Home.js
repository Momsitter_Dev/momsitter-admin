/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */
import { compose } from 'recompose';
import { withStyles as withMaterialStyle } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Home.css';

const propTypes = {};
const defaultProps = {};

class Home extends React.Component {
  render() {
    return (
      <div>
        <Grid container spacing={16}>
          <Grid item xs={12}>
          </Grid>
        </Grid>
      </div>
    );
  }
}

Home.propTypes = propTypes;
Home.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyle(s, { withTheme: true }),
)(Home);
