import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ManageUserProducts.css';
import ManageUserProduct from '../../components/Manage/UserProducts';

class ManageUserProducts extends React.Component {
  render() {
    return (
      <div className={s.root}>
        <h2 className="article-title">회원 - 이용권 현황</h2>
        <ManageUserProduct />
      </div>
    );
  }
}

ManageUserProducts.propTypes = {};

ManageUserProducts.defaultProps = {};

export default withStyles(s)(ManageUserProducts);
