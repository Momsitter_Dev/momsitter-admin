import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ManageUsersMemo.css';
import UserMemo from '../../components/Manage/UserMemo';

class ManageUsersMemo extends React.Component {
  render() {
    const { userId } = this.props;
    return (
      <div className={s.root}>
        <h2 className="article-title">회원 - 메모관리 </h2>
        <UserMemo userId={userId} />
      </div>
    );
  }
}

ManageUsersMemo.propTypes = {
  userId: PropTypes.string,
};

ManageUsersMemo.defaultProps = {};

export default withStyles(s)(ManageUsersMemo);
