import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ManageUser.css';
import UserManage from '../../components/Manage/User';

class ManageUser extends React.Component {
  render() {
    return (
      <div className={s.root}>
        <h2 className="article-title">회원 - 회원관리</h2>
        <UserManage />
      </div>
    );
  }
}

ManageUser.propTypes = {};

ManageUser.defaultProps = {};

export default withStyles(s)(ManageUser);
