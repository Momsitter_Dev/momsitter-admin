import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ManageSitterApplications.css';
import SitterApplications from '../../components/Manage/SitterApplications';

class ManageSitterApplications extends React.Component {
  render() {
    return (
      <div className={s.root}>
        <h2 className="article-title">[시터 -> 부모] 지원내역</h2>
        <SitterApplications />
      </div>
    );
  }
}

ManageSitterApplications.propTypes = {};

ManageSitterApplications.defaultProps = {};

export default withStyles(s)(ManageSitterApplications);
