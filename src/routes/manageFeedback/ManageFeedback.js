import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ManageFeedback.css';
import Feedback from '../../components/Manage/Feedback';

class ManageFeedback extends React.Component {
  render() {
    return (
      <div className={s.root}>
        <h2 className="article-title"> 사용자 피드백 현황 </h2>
        <Feedback />
      </div>
    );
  }
}

ManageFeedback.propTypes = {};

ManageFeedback.defaultProps = {};

export default withStyles(s)(ManageFeedback);
