import { compose } from 'recompose';
import { Grid, withStyles as withMaterialStyles } from '@material-ui/core';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Users.css';
import UserManage from '../../components/UserManage';

const propTypes = {
  userId: PropTypes.string,
};
const defaultProps = {
  userId: null,
};

class Users extends React.Component {
  render() {
    return (
      <Grid container spacing={16}>
        <Grid item xs={12}>
          <UserManage userId={this.props.userId} />
        </Grid>
      </Grid>
    );
  }
}

Users.propTypes = propTypes;
Users.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(Users);
