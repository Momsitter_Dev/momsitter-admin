/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import Users from './Users';
import Layout from '../../components/Layout';

async function action(context) {
  let userId = null;
  if (context && context.query && context.query.userId) {
    userId = context.query.userId;
  }
  return {
    chunks: ['users'],
    title: '개인 관리',
    component: (
      <Layout>
        <Users userId={context.query.userId} />
      </Layout>
    ),
  };
}

export default action;
