import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ManageUserReviews.css';
import ManageReviews from '../../components/Manage/Reviews';

class ManageUserReviews extends React.Component {
  render() {
    return (
      <div className={s.root}>
        <h2 className="article-title">회원 - 후기관리</h2>
        <ManageReviews />
      </div>
    );
  }
}

ManageUserReviews.propTypes = {};

ManageUserReviews.defaultProps = {};

export default withStyles(s)(ManageUserReviews);
