import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ManageUserApplications.css';
import ManageApplication from '../../components/Manage/Applications';

class ManageUserApplications extends React.Component {
  render() {
    return (
      <div className={s.root}>
        <h2 className="article-title">회원 - 신청내역</h2>
        <ManageApplication />
      </div>
    );
  }
}

ManageUserApplications.propTypes = {};

ManageUserApplications.defaultProps = {};

export default withStyles(s)(ManageUserApplications);
