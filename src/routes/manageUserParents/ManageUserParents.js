import moment from 'moment';
import uuid from 'uuid/v4';
import qs from 'qs';
import { compose } from 'recompose';
import React from 'react';
import { toast } from 'react-toastify';
import FlatPagination from 'material-ui-flat-pagination';
import {
  ArrowUpward as ArrowUpIcon,
  ArrowDownward as ArrowDownIcon,
  ErrorOutline as ErrorIcon,
  Warning as WarningIcon,
} from '@material-ui/icons';
import {
  withStyles as withMaterialStyles,
  Button,
  Grid,
  Paper,
  Typography,
  CircularProgress,
  Select,
  MenuItem,
} from '@material-ui/core';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ManageUserParents.css';
import SearchBar from '../../components/SearchBar';
import FilterDataTypeGroup from '../../util/const/FilterDataTypeGroup';
import ResponseResultTypeGroup from '../../util/const/ResponseResultTypeGroup';
import Request from '../../util/Request';
import UserStatusGroup from '../../util/const/UserStatusGroup';
import UserGenderGroup from '../../util/const/UserGenderGroup';
import UserConnectType from '../../util/const/UserConnectType';

moment.locale('ko');

const styles = theme => ({
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
});

const propTypes = {
  classes: PropTypes.object.isRequired,
};
const defaultProps = {};

class ManageUserParents extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pending: true,
      exception: false,
      exceptionMessage: null,
      list: [],
      page: null,
      maxPage: null,
      searchOptions: [],
      orderOption: {
        column: 'userId',
        method: 'DESC',
      },
    };
    this.cachedOption = {};
  }

  async componentDidMount() {
    const response = await this.search({
      orderOption: {
        ...this.state.orderOption,
      },
    });
    if (response) {
      this.setState({
        exception: false,
        pending: false,
        list: response.data,
        page: response.page.currentPage,
        maxPage: response.page.maxPage,
      });
    }
  }

  onClickUserId = (userId) => {
    return window.open(`/users?userId=${userId}`);
  };

  onChangeUserStatus = async (event, userId) => {
    return this.setState(
      {
        pending: true,
      },
      async () => {
        try {
          const updateResponse = await Request(`api/manage/user/${userId}/status`, {
            method: 'PUT',
            data: { userStatus: event.target.value },
          });

          if (updateResponse.result === ResponseResultTypeGroup.SUCCESS) {
            // TODO reload
            toast.success(`${userId} 회원의 '회원상태'가 ${event.target.value}로 변경되었습니다.`);
            const response = await this.search(this.cachedOption);
            return this.setState({
              pending: false,
              exception: false,
              list: response.data,
              page: response.page.currentPage,
              maxPage: response.page.maxPage,
            });
          }
          throw new Error(`${userId} 회원의 '회원상태'를 업데이트 할 수 없습니다.`);
        } catch (err) {
          return this.setState({
            pending: false,
            exception: true,
            exceptionMessage: err.message,
          });
        }
      },
    );
  };

  onChangeOrderColumn = async columnId => {
    const { orderOption, searchOptions } = this.state;
    if (orderOption.column !== columnId) {
      orderOption.method = '';
    }
    orderOption.column = columnId;
    if (orderOption.method === 'DESC') {
      orderOption.method = 'ASC';
    } else if (orderOption.method === 'ASC') {
      orderOption.method = '';
    } else {
      orderOption.method = 'DESC';
    }
    return this.setState(
      {
        orderOption,
        pending: true,
      },
      async () => {
        const response = await this.search({
          searchOptions,
          orderOption,
          requestPage: 0,
        });
        if (response) {
          return this.setState({
            exception: false,
            pending: false,
            list: response.data,
            page: response.page.currentPage,
            maxPage: response.page.maxPage,
          });
        }
      },
    );
  };

  onClickSearch = async () => {
    const { searchOptions, orderOption } = this.state;
    return this.setState(
      {
        pending: true,
      },
      async () => {
        const response = await this.search({ searchOptions, orderOption, requestPage: 0 });
        if (response) {
          return this.setState({
            exception: false,
            pending: false,
            list: response.data,
            page: response.page.currentPage,
            maxPage: response.page.maxPage,
          });
        }
      },
    );
  };

  onExportCSV = () => {
    const queryString = qs.stringify(this.cachedOption);
    return window.open(`/api/manage/user/parent/csv?${queryString}`);
  };

  onAddSearchOption = () => {
    const { searchOptions } = this.state;
    searchOptions.push({
      generatedKey: uuid(),
      filterType: '',
      filterValue: '',
      filterDataType: '',
    });
    return this.setState({ searchOptions });
  };

  onDeleteSearchOption = generatedKey => {
    const { searchOptions } = this.state;
    return this.setState({
      searchOptions: searchOptions.filter(item => {
        return item.generatedKey !== generatedKey;
      }),
    });
  };

  onChangeSearchOption = (generatedKey, type, value, dataType) => {
    const { searchOptions } = this.state;
    const targetIndex = searchOptions.findIndex(item => {
      return item.generatedKey === generatedKey;
    });
    if (targetIndex !== -1) {
      return this.setState({
        searchOptions: searchOptions.map((item, index) => {
          if (index === targetIndex) {
            return {
              ...item,
              filterType: type,
              filterValue: value,
              filterDataType: dataType,
            };
          }
          return item;
        }),
      });
    }
  };

  onClickPage = async (e, offset) => {
    this.cachedOption.requestPage = offset;
    return this.setState(
      {
        pending: true,
      },
      async () => {
        const response = await this.search(this.cachedOption);
        if (response) {
          return this.setState({
            exception: false,
            pending: false,
            list: response.data,
            page: response.page.currentPage,
            maxPage: response.page.maxPage,
          });
        }
      },
    );
  };

  openNotificationDialog = (title, message) => {
    return toast.info(`[${title || ''}] ${message} `);
  };

  /**
   *
   * @param options
   * @param options.searchOptions
   * @param options.orderOption
   * @param options.requestPage
   * @return {Promise<void>}
   */
  search = async options => {
    try {
      const query = qs.stringify(options);
      this.cachedOption = Object.assign({}, options);
      const response = await Request(`api/manage/user/parent?${query}`, { method: 'GET' });
      if (response && response.result === ResponseResultTypeGroup.SUCCESS) {
        return response;
      }
      throw new Error(`request fail ${JSON.stringify(response)}`);
    } catch (err) {
      return this.setState({
        pending: false,
        exception: true,
        exceptionMessage: `검색중 오류가 발생하였습니다. [${err.message}]`,
      });
    }
  };

  renderDataHeader = () => {
    const { classes } = this.props;
    const header = [
      {
        label: '회원번호',
        column: 'userId',
        grid: 1,
        sortable: true,
      },
      {
        label: '이름',
        column: 'userName',
        grid: 1,
        sortable: true,
      },
      {
        label: '가입방법',
        column: 'userConnectWay',
        grid: 1,
        sortable: true,
      },
      {
        label: '회원종류',
        column: 'userTypeId',
        grid: 1,
        sortable: true,
      },
      {
        label: '휴대폰',
        column: 'userPhone',
        grid: 1,
        sortable: true,
      },
      {
        label: '성별',
        column: 'userGender',
        grid: 1,
        sortable: true,
      },
      {
        label: '생년월일',
        column: 'userBirthDay',
        grid: 1,
        sortable: true,
      },
      {
        label: '가입일자',
        column: 'userSignUpDate',
        grid: 1,
        sortable: true,
      },
      {
        label: '프로필 수정일',
        column: 'updateDate',
        grid: 1,
        sortable: true,
      },
      {
        label: '회원상태',
        column: 'userStatus',
        grid: 1,
        sortable: true,
      },
      {
        label: '본인명의',
        column: 'isOwnPhone',
        grid: 1,
        sortable: true,
      },
      {
        label: '',
        column: '',
        grid: 1,
        sortable: false,
      },
    ];

    return (
      <Grid container spacing={0}>
        <Grid item xs={12}>
          <Paper className={classes.paper}>
            <Grid container spacing={0}>
              {header.map((item, index) => {
                const { label, grid, sortable, column } = item;
                return (
                  <Grid item xs={grid} key={`header-item-${index}`}>
                    <Button
                      variant={'flat'}
                      fullWidth
                      disabled={!sortable}
                      onClick={() => this.onChangeOrderColumn(column)}
                    >
                      {this.renderArrow(item)}
                      <Typography variant={'caption'}> {label} </Typography>
                    </Button>
                  </Grid>
                );
              })}
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    );
  };

  renderDataContent = () => {
    const { pending, exception, exceptionMessage, list } = this.state;
    if (exception) {
      return this.renderDataException(exceptionMessage);
    }
    if (pending) {
      return this.renderDataPending();
    }

    if (!list) {
      return this.renderDataEmpty();
    }

    if (list.length === 0) {
      return this.renderDataEmpty();
    }

    return (
      <Grid container spacing={0}>
        <Grid item xs={12}>
          {this.renderDataItems(list)}
        </Grid>
        <Grid item xs={12}>
          {this.renderDataPagination()}
        </Grid>
      </Grid>
    );
  };

  /**
   * @param data
   * @param data.column
   * @param data.grid
   * @param data.sortable
   * @param data.label
   */
  renderArrow = data => {
    const { orderOption } = this.state;
    if (data.column === orderOption.column) {
      if (orderOption.method === 'ASC') {
        return <ArrowUpIcon style={{ width: '15px', height: '15px' }} />;
      }
      if (orderOption.method === 'DESC') {
        return <ArrowDownIcon style={{ width: '15px', height: '15px' }} />;
      }
    }
    return null;
  };

  renderDataItems = list => {
    const { classes } = this.props;
    return list.map((item, index) => {
      const {
        updateDate,
        userBirthday,
        userConnectType,
        userDetailTypeDesc,
        userGender,
        userId,
        userName,
        userPhone,
        userSignUpDate,
        userStatus,
        userAge,
        isOwnPhone,
      } = item;
      return (
        <Grid container spacing={0} key={`manage-parent-item-${index}`}>
          <Grid item xs={12}>
            <Paper className={classes.paper}>
              <Grid container spacing={0}>
                <Grid item xs={1}>
                  <Button variant={'flat'} onClick={() => this.onClickUserId(userId)}>
                    <Typography variant={'body1'}> {userId} </Typography>
                  </Button>
                </Grid>
                <Grid item xs={1}>
                  <Typography variant={'body1'}> {userName} </Typography>
                </Grid>
                <Grid item xs={1}>
                  <Typography variant={'body1'}>
                    <Typography variant={'body1'}>
                      {userConnectType === UserConnectType.EMAIL ? '이메일' : ''}
                      {userConnectType === UserConnectType.FACEBOOK ? '페이스북' : ''}
                      {userConnectType === UserConnectType.KAKAO ? '카카오' : ''}
                      {userConnectType === UserConnectType.NAVER ? '네이버' : ''}
                    </Typography>
                  </Typography>
                </Grid>
                <Grid item xs={1}>
                  <Typography variant={'body1'}> {userDetailTypeDesc} </Typography>
                </Grid>
                <Grid item xs={1}>
                  <Typography variant={'body1'}> {userPhone} </Typography>
                </Grid>
                <Grid item xs={1}>
                  <Typography variant={'body1'}>
                    {userGender === UserGenderGroup.WOMEN ? '여자' : ''}
                    {userGender === UserGenderGroup.MAN ? '남자' : ''}
                  </Typography>
                </Grid>
                <Grid item xs={1}>
                  <Typography variant={'body1'}>
                    {' '}
                    {moment(userBirthday, 'YYYYMMDD').format('YYYY년 MM월 DD일')}{' '}
                  </Typography>
                  <Typography variant={'caption'}> {`${userAge}세`} </Typography>
                </Grid>
                <Grid item xs={1}>
                  <Typography variant={'body1'}> {moment(userSignUpDate).format('YYYY년 MM월 DD일')} </Typography>
                  <Typography variant={'caption'}> {moment(userSignUpDate).format('HH시 mm분')} </Typography>
                </Grid>
                <Grid item xs={1}>
                  <Typography variant={'body1'}> {moment(updateDate).format('YYYY년 MM월 DD일')} </Typography>
                  <Typography variant={'caption'}> {moment(updateDate).format('HH시 mm분')} </Typography>
                </Grid>
                <Grid item xs={1}>
                  <Select value={userStatus} onChange={event => this.onChangeUserStatus(event, userId)}>
                    <MenuItem value={UserStatusGroup.ACTIVE}> 활성 </MenuItem>
                    <MenuItem value={UserStatusGroup.INACTIVE}> 비활성 </MenuItem>
                    <MenuItem value={UserStatusGroup.OUT}> 탈퇴 </MenuItem>
                  </Select>
                </Grid>
                <Grid item xs={1}>
                  <Typography variant={'body1'}>
                    {isOwnPhone || isOwnPhone === null ? '본인명의' : '본인명의 아님'}
                  </Typography>
                </Grid>
                <Grid item xs={1}>
                  <Typography variant={'body1'}> </Typography>
                </Grid>
              </Grid>
            </Paper>
          </Grid>
        </Grid>
      );
    });
  };

  renderDataPagination = () => {
    const { classes } = this.props;
    const { page, maxPage } = this.state;
    return (
      <Paper className={classes.paper} style={{ textAlign: 'left' }}>
        <Grid container spacing={0}>
          <Grid item xs={12}>
            <FlatPagination offset={page} limit={1} total={maxPage} onClick={this.onClickPage} />
          </Grid>
        </Grid>
      </Paper>
    );
  };

  renderDataEmpty = () => {
    const { classes } = this.props;
    return (
      <Grid container spacing={0} justify={'center'} alignContent={'center'} alignItems={'center'}>
        <Grid item xs={12}>
          <Paper className={classes.paper}>
            <Grid item xs={12}>
              <WarningIcon />
            </Grid>
            <Grid item xs={12}>
              <Typography variant={'caption'}> 데이터가 없습니다. </Typography>
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    );
  };

  renderDataException = exceptionMessage => {
    const { classes } = this.props;
    return (
      <Grid container spacing={0}>
        <Grid item xs={12}>
          <Paper className={classes.paper}>
            <Grid container spacing={16}>
              <Grid item xs={12}>
                <ErrorIcon />
              </Grid>
              <Grid item xs={12}>
                <Typography variant={'caption'}> {exceptionMessage} </Typography>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    );
  };

  renderDataPending = () => {
    const { classes } = this.props;
    return (
      <Grid container spacing={0}>
        <Grid item xs={12}>
          <Paper className={classes.paper}>
            <Grid container spacing={16}>
              <Grid item xs={12}>
                <CircularProgress />
              </Grid>
              <Grid item xs={12}>
                <Typography variant={'caption'}> 데이터를 불러오는 중입니다. </Typography>
              </Grid>
            </Grid>
          </Paper>
        </Grid>
      </Grid>
    );
  };

  render() {
    const { searchOptions } = this.state;
    const filter = [
      { name: '회원 번호', type: 'userId', dataType: FilterDataTypeGroup.UNIQUE_TEXT },
      { name: '이름', type: 'userName', dataType: FilterDataTypeGroup.TEXT },
      { name: '성별', type: 'userGender', dataType: FilterDataTypeGroup.USER_GENDER },
      { name: '이메일', type: 'userEmail', dataType: FilterDataTypeGroup.TEXT },
      { name: '핸드폰 번호', type: 'userPhone', dataType: FilterDataTypeGroup.TEXT },
      { name: '회원 상태', type: 'userStatus', dataType: FilterDataTypeGroup.USER_STATUS },
      { name: '가입 일자', type: 'userSignUpDate', dataType: FilterDataTypeGroup.DATE_RANGE },
      { name: '가입 방법', type: 'joinType', dataType: FilterDataTypeGroup.JOIN_TYPE },
      { name: '본인명의', type: 'isOwnPhone', dataType: FilterDataTypeGroup.OWN_PHONE },
    ];

    return (
      <Paper style={{ backgroundColor: 'transparent' }}>
        <Grid container spacing={0}>
          <Grid item xs={12}>
            <SearchBar
              filter={filter}
              searchOptions={searchOptions}
              onClickSearch={this.onClickSearch}
              onExportCSV={this.onExportCSV}
              onClickAddSearchOption={this.onAddSearchOption}
              onClickDeleteSearchOption={this.onDeleteSearchOption}
              onChangeSearchOption={this.onChangeSearchOption}
              openNotificationDialog={this.openNotificationDialog}
            />
          </Grid>
        </Grid>
        {this.renderDataHeader()}
        {this.renderDataContent()}
      </Paper>
    );
  }
}

ManageUserParents.propTypes = propTypes;
ManageUserParents.defaultProps = defaultProps;

export default compose(
  withStyles(s),
  withMaterialStyles(styles, { withTheme: true }),
)(ManageUserParents);
