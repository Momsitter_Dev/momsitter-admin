/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import Layout from '../../components/Layout';
import Manage from './Manage';
import NotFound from '../not-found/NotFound';

function action(context) {
  console.log(context.params.id);

  const manageMenus = {
    policy: {
      title: '정책 관리',
      apiPath: '',
    },
    coupon: {
      title: '쿠폰 관리',
      api: '/api/manage/coupon',
    },
    product: {
      title: '이용권 관리',
    },
  };

  if (manageMenus[context.params.id]) {
    return {
      chunks: ['manage'],
      component: (
        <Layout>
          <Manage data={manageMenus[context.params.id]} />
        </Layout>
      ),
    };
  } else {
    return {
      chunks: ['not-found'],
      component: (
        <Layout>
          <NotFound title="Notfound" />
        </Layout>
      ),
      status: 404,
    };
  }
}

export default action;
