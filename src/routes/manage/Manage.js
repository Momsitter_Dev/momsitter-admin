import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Manage.css';
import ManageComponent from '../../components/Manage';
import Request from '../../util/Request';

class Manage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      page: 0,
    };
  }

  onChangePage = (value) => {

  };

  async componentDidMount(){
    const response = await Request('api/manage/coupon', { method: 'GET' });
    if (response && response.result === 'success') {
      this.state.list = response.data;
    }
  }

  render() {
    return (
      <div className={s.root}>
        <div className={s.container}>
          <ManageComponent
            title={this.props.data.title}
            list={this.state.data}
          />
        </div>
      </div>
    );
  }
}

Manage.propTypes = {
  data: PropTypes.object.isRequired,
};

Manage.defaultProps = {};

export default withStyles(s)(Manage);
