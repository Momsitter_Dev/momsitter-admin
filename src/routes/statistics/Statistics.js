import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Statistics.css';
import StatisticsContainer from '../../components/Statistics';

class Statistics extends React.Component {
  render() {
    return (
      <div className={s.root}>
        <h2 className="article-title">통계</h2>
        <StatisticsContainer />
      </div>
    );
  }
}

Statistics.propTypes = {};

Statistics.defaultProps = {};

export default withStyles(s)(Statistics);
