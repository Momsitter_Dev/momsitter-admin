import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ManageCoupons.css';
import Coupons from '../../components/Manage/Coupons';

class ManageCoupons extends React.Component {
  render() {
    return (
      <div className={s.root}>
        <h2 className="article-title">쿠폰관리</h2>
        <Coupons />
      </div>
    );
  }
}

ManageCoupons.propTypes = {};

ManageCoupons.defaultProps = {};

export default withStyles(s)(ManageCoupons);
