import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ManageProducts.css';
import Products from '../../components/Manage/Products';

class ManageProducts extends React.Component {
  render() {
    return (
      <div className={s.root}>
        <h2 className="article-title">이용권 관리</h2>
        <Products />
      </div>
    );
  }
}

ManageProducts.propTypes = {};

ManageProducts.defaultProps = {};

export default withStyles(s)(ManageProducts);
