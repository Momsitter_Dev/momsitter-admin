import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ManageUserMemos.css';
import ManageMemos from '../../components/Manage/Memos';

class ManageUserMemos extends React.Component {
  render() {
    return (
      <div className={s.root}>
        <h2 className="article-title">회원 - 메모관리</h2>
        <ManageMemos />
      </div>
    );
  }
}

ManageUserMemos.propTypes = {};

ManageUserMemos.defaultProps = {};

export default withStyles(s)(ManageUserMemos);
