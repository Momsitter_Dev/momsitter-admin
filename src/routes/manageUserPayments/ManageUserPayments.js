import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ManageUserPayments.css';
import ManagePayments from '../../components/Manage/Payments';

class ManageUserPayments extends React.Component {
  render() {
    return (
      <div className={s.root}>
        <h2 className="article-title">회원 - 결제관리</h2>
        <ManagePayments />
      </div>
    );
  }
}

ManageUserPayments.propTypes = {};

ManageUserPayments.defaultProps = {};

export default withStyles(s)(ManageUserPayments);
