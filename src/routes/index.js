/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

/* eslint-disable global-require */

// The top-level (parent) route
const routes = {
  path: '',

  // Keep in mind, routes are evaluated in order
  children: [
    {
      path: '/',
      load: () => import(/* webpackChunkName: 'home' */ './home'),
    },
    {
      path: '/users',
      load: () => import(/* webpackChunkName: 'users' */ './users'),
    },
    {
      path: '/manage',
      //      load: () => import(/* webpackChunkName: 'manage' */ './manage'),
      children: [
        {
          path: '/feedback',
          load: () =>
            import(/* webpackChunkName: 'manage' */ './manageFeedback'),
        },
        {
          path: '/coupons',
          load: () =>
            import(/* webpackChunkName: 'manage' */ './manageCoupons'),
        },
        {
          path: '/products',
          load: () =>
            import(/* webpackChunkName: 'manage' */ './manageProducts'),
        },
        {
          path: '/parents',
          children: [
            {
              path: ['', '/'],
              load: () =>
                import(/* webpackChunkName: 'manage' */ './manageUserParents'),
            },
            {
              path: '/applications',
              load: () =>
                import(/* webpackChunkName: 'manage' */ './manageParentApplications'),
            },
          ],
        },
        {
          path: '/sitters',
          children: [
            {
              path: ['', '/'],
              load: () =>
                import(/* webpackChunkName: 'manage' */ './manageUserSitters'),
            },
            {
              path: '/applications',
              load: () =>
                import(/* webpackChunkName: 'manage' */ './manageSitterApplications'),
            },
          ],
        },
        {
          path: '/board',
          children: [
            {
              path: '/notice',
              children: [
                {
                  path: ['', '/'],
                  load: () =>
                    import(/* webpackChunkName: 'manage' */ './manageBoardNotice'),
                },
                {
                  path: '/:id',
                  load: () =>
                    import(/* webpackChunkName: 'manage' */ './manageBoardNoticeItem'),
                },
              ],
            },
          ],
        },
        {
          path: '/user',
          children: [
            {
              path: '/point',
              load: () =>
                import(/* webpackChunkName: 'manage' */ './manageUserPoint'),
            },
            {
              path: '/users',
              load: () =>
                import(/* webpackChunkName: 'manage' */ './manageUser'),
            },
            {
              path: '/leave',
              load: () => import(/* webpackChunkName: 'manage' */ './manageLeaveUser'),
            },
            {
              path: '/products',
              load: () =>
                import(/* webpackChunkName: 'manage' */ './manageUserProducts'),
            },
            {
              path: '/applications',
              load: () =>
                import(/* webpackChunkName: 'manage' */ './manageUserApplications'),
            },
            {
              path: '/reviews',
              load: () =>
                import(/* webpackChunkName: 'manage' */ './manageUserReviews'),
            },
            {
              path: '/payments',
              load: () =>
                import(/* webpackChunkName: 'manage' */ './manageUserPayments'),
            },
            {
              path: '/certifications',
              load: () =>
                import(/* webpackChunkName: 'manage' */ './manageUserCertifications'),
            },
            {
              path: '/reports',
              load: () =>
                import(/* webpackChunkName: 'manage' */ './manageUserReports'),
            },
            {
              path: '/memos',
              load: () =>
                import(/* webpackChunkName: 'manage' */ './manageUserMemos'),
            },
            {
              path: '/:userId',
              children: [
                {
                  path: '/memos',
                  load: () =>
                    import(/* webpackChunkName: 'manage' */ './manageUsersMemo'),
                },
              ],
            },
          ],
        },
      ],
    },
    {
      path: '/statistics',
      load: () => import(/* webpackChunkName: 'statistics' */ './statistics'),
    },
    {
      path: '/guide',
      load: () => import(/* webpackChunkName: 'guide' */ './guide'),
    },
    {
      path: '/login',
      load: () => import(/* webpackChunkName: 'login' */ './login'),
    },
    // Wildcard routes, e.g. { path: '*', ... } (must go last)
    {
      path: '*',
      load: () => import(/* webpackChunkName: 'not-found' */ './not-found'),
    },
  ],

  async action({ next }) {
    // Execute each child route until one of them return the result
    const route = await next();
    // Provide default values for title, description etc.
    route.title = `${route.title || '맘시터'}`;
    route.description = route.description || '';

    return route;
  },
};

// The error page is available by permanent url for development mode
if (__DEV__) {
  routes.children.unshift({
    path: '/error',
    action: require('./error').default,
  });
}

export default routes;
