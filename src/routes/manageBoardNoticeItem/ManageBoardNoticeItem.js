/* eslint-disable prettier/prettier */
import moment from 'moment';
import { compose } from 'recompose';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import { withStyles as withMaterialStyle } from '@material-ui/core/styles';
import Grow from '@material-ui/core/Grow';
import draftToHtml from 'draftjs-to-html';
import { EditorState, convertToRaw } from 'draft-js';
import { stateFromHTML } from 'draft-js-import-html';
import { Editor } from 'react-draft-wysiwyg';
import EditorStyle from '!!isomorphic-style-loader!css-loader?modules=false!react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ManageBoardNoticeItem.css';
import BoardAuthorSelect from '../../components/Filter/BoardAuthorSelect';
import Request from '../../util/Request';
import ResponseResultTypeGroup from '../../util/const/ResponseResultTypeGroup';
import history from '../../history';

moment.locale('ko');

const propTypes = {
  id: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
};
const defaultProps = {};

class ManageBoardNoticeItem extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      author: '',
      title: '',
      content: '',
      editorState: EditorState.createEmpty(),
      registerDate: '',
      updateDate: '',
      view: '',
    };
    this.onChangeAuthor = this.onChangeAuthor.bind(this);
    this.onChangeContent = this.onChangeContent.bind(this);
    this.getBoardInfo = this.getBoardInfo.bind(this);
    this.onChangeEditState = this.onChangeEditState.bind(this);
    this.onClickSave = this.onClickSave.bind(this);
  }

  async componentDidMount() {
    try {
      const { id } = this.props;
      if (!id) {
        throw new Error('noticeId is null');
      }
      const data = await this.getBoardInfo(id);
      if (data) {
        this.setState({
          author: data.author,
          title: data.title,
          content: data.content,
          registerDate: moment(data.registerDate).format('YYYY-MM-DD'),
          updateDate: data.updateDate,
          view: data.view,
          editorState: EditorState.createWithContent(stateFromHTML(data.content)),
        });
      }
    } catch (err) {
      console.log(err);
    }
  };

  async componentWillReceiveProps(props) {
    try {
      const { id } = props;
      if (!id) {
        throw new Error('noticeId is null');
      }
      const data = await this.getBoardInfo(id);
      if (data) {
        this.setState({
          author: data.author,
          title: data.title,
          content: data.content,
          registerDate: moment(data.registerDate).format('YYYY-MM-DD'),
          updateDate: data.updateDate,
          view: data.view,
          editorState: EditorState.createWithContent(stateFromHTML(data.content)),
        });
      }
    } catch (err) {
      console.log(err);
    }
  };

  onClickSave = async function () {
    try {
      const { id } = this.props;
      const response = await Request(`api/boards/notice/${id}`, {
        method: 'PUT',
        data: {
          ...this.state,
        },
      });
      if (response.result === ResponseResultTypeGroup.SUCCESS) {
        alert('저장되었습니다.');
        return history.push('/manage/board/notice');
      }
      throw new Error('request fail');
    } catch (err) {
      console.log(err);
      alert('게시글을 수정할 수 없습니다.');
    }
  };

  onChangeTitle = (event) => {
    return this.setState({
      title: event.target.value,
    });
  };

  onChangeAuthor = (value) => {
    return this.setState({
      author: value,
    });
  };

  onChangeRegisterDate = (event) => {
    return this.setState({
      registerDate: event.target.value,
    });
  };

  onChangeEditState = (editorState) => {
    return this.setState({
      editorState,
      content: draftToHtml(convertToRaw(editorState.getCurrentContent())),
    });
  };

  onChangeContent = (contentState) => {
    this.inputContent = contentState;
    return true;
  };

  onClickCancel = () => {
    return history.go(-1);
  };

  getBoardInfo = async function (id) {
    try {
      const response = await Request(`api/boards/notice/${id}`, {
        method: 'GET',
      });

      if (response.result === ResponseResultTypeGroup.SUCCESS) {
        return response.data;
      }
    } catch (err) {
      console.log(err);
      return null;
    }
  };

  render() {
    const {
      author,
      title,
      registerDate,
      editorState,
    } = this.state;
    return (
      <div className={s.root}>
        <Grid container spacing={16}>
          <Grid item xs={12}>
            <Paper style={{ padding: '10px' }}>
              <Typography variant="title"> 게시글 수정 </Typography>
              <div className={s.contentWrap}>
                <Grow in>
                  <Grid container spacing={16}>
                    <Grid container spacing={16} style={{ marginBottom: '10px' }}>
                      <Grid item xs={1}>
                        <Typography variant={'subheading'} style={{ textAlign: 'right' }}> 작성자 </Typography>
                      </Grid>
                      <Grid item xs={11}>
                        <BoardAuthorSelect value={author} onChange={this.onChangeAuthor} />
                      </Grid>
                    </Grid>
                    <Grid container spacing={16} style={{ marginBottom: '10px' }}>
                      <Grid item xs={1}>
                        <Typography variant={'subheading'} style={{ textAlign: 'right' }}> 작성일자 </Typography>
                      </Grid>
                      <Grid item xs={11}>
                        <TextField
                          fullWidth
                          value={registerDate}
                          type={'date'}
                          onChange={this.onChangeRegisterDate}
                        />
                      </Grid>
                    </Grid>
                    <Grid container spacing={16} style={{ marginBottom: '10px' }}>
                      <Grid item xs={1}>
                        <Typography variant={'subheading'} style={{ textAlign: 'right' }}> 제목 </Typography>
                      </Grid>
                      <Grid item xs={11}>
                        <TextField
                          fullWidth
                          value={title}
                          onChange={this.onChangeTitle}
                        />
                      </Grid>
                    </Grid>
                    <Grid container spacing={16} style={{ marginBottom: '10px' }}>
                      <Grid item xs={1}>
                        <Typography variant={'subheading'} style={{ textAlign: 'right' }}> 내용 </Typography>
                      </Grid>
                      <Grid item xs={11}>
                        <Editor
                          editorState={editorState}
                          onEditorStateChange={this.onChangeEditState}
                          editorClassName={s.editor}
                        />
                      </Grid>
                    </Grid>
                  </Grid>
                </Grow>
              </div>
              <div>
                <Button variant={'flat'} onClick={this.onClickCancel}> 취소 </Button>
                <Button variant={'raised'} onClick={this.onClickSave}> 저장 </Button>
              </div>
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }
}

ManageBoardNoticeItem.propTypes = propTypes;
ManageBoardNoticeItem.defaultProps = defaultProps;

export default compose(
  withStyles(s, EditorStyle),
  withMaterialStyle(s, { withTheme: true }),
)(ManageBoardNoticeItem);
