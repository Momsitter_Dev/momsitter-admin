import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ManageLeaveUser.css';
import Leave from '../../components/Manage/Leave';

const propTypes = {};
const defaultProps = {};

class ManageLeaveUser extends React.Component {
  render() {
    return (
      <div className={s.root}>
        <h2 className="article-title"> 본인탈퇴 회원 관리 </h2>
        <Leave />
      </div>
    );
  }
}

ManageLeaveUser.propTypes = propTypes;
ManageLeaveUser.defaultProps = defaultProps;

export default withStyles(s)(ManageLeaveUser);
