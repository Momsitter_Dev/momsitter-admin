/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import { compose } from 'recompose';
import Button from '@material-ui/core/Button';
import { withStyles as withMaterialStyles } from '@material-ui/core/styles';
import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './Login.css';
import InputField from '../../components/InputField';
import Request from '../../util/Request';
import NotificationDialog from '../../components/Dialog/NotificationDialog';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pending: false,
      userId: '',
      userPw: '',
      notificationDialog: false,
      notificationDialogTitle: '',
      notificationDialogMessage: '',
    };
    this.onClickLogin = this.onClickLogin.bind(this);
  }

  static propTypes = {
    title: PropTypes.string.isRequired,
  };

  onChangeEmail = value => {
    this.setState({ userId: value });
  };

  onChangePassword = value => {
    this.setState({ userPw: value });
  };

  onClickLogin = async function () {
    console.log('DEBUG : ON CLICK ET');
    const { userId, userPw } = this.state;
    const response = await Request('api/login', {
      method: 'POST',
      data: {
        userId,
        userPw,
      },
    }).catch(() => false);

    if (response && response.result === 'success') {
      this.openNotificationDialog(
        '잠시만 기다려 주세요',
        <div>
          <div> 로그인 되었습니다. </div>
          <div> 메인 페이지로 이동합니다. </div>
        </div>,
      );
      this.settingLocalStorage(response);
      this.redirectToMain(2500);
    } else {
      this.openNotificationDialog(null, '로그인에 실패하였습니다.');
    }
  };

  settingLocalStorage = data => {
    if (typeof window !== 'undefined') {
      window.localStorage.setItem('access_token', data.token);
    } else {
      this.openNotificationDialog('오류', '로그인에 실패하였습니다.');
    }
  };

  redirectToMain = timeout => {
    if (typeof window !== 'undefined') {
      if (timeout) {
        window.setTimeout(() => {
          window.location.href = '/';
        }, timeout);
      } else {
        window.location.href = '/';
      }
    } else {
      this.openNotificationDialog(null, '브라우저 환경에서 이용해 주세요.');
    }
  };

  openNotificationDialog = (title, message) => {
    this.setState({
      notificationDialog: true,
      notificationDialogTitle: title || '알림',
      notificationDialogMessage: message,
    });
  };

  onClickCloseNotificationDialog = () => {
    this.setState({
      notificationDialog: false,
      notificationDialogTitle: '',
      notificationDialogMessage: '',
    });
  };

  onEnterPress = event => {
    if (event.charCode === 13) {
      this.onClickLogin();
    }
  };

  render() {
    const {
      userId,
      userPw,
      notificationDialog,
      notificationDialogTitle,
      notificationDialogMessage,
      pending,
    } = this.state;
    return (
      <div className="body-inner">
        <div className="card bg-white">
          <div className="card-content">
            <section className="logo text-center">
              <h1>
                <a href="#">관리자 인증</a>
              </h1>
            </section>
            <form className="form-horizontal">
              <fieldset>
                <div className="form-group">
                  <InputField
                    value={userId}
                    onChange={this.onChangeEmail}
                    style={{ width: '100%', margin: '5px' }}
                  />
                </div>
                <div className="form-group">
                  <InputField
                    value={userPw}
                    onChange={this.onChangePassword}
                    onKeyPress={this.onEnterPress}
                    style={{ width: '100%', margin: '5px' }}
                    type="password"
                  />
                </div>
              </fieldset>
            </form>
          </div>
          <div className="card-action no-border text-right">
            <Button
              variant={"raised"}
              onClick={this.onClickLogin}
              disabled={!!pending}
            >
              로그인
            </Button>
          </div>
        </div>
        <NotificationDialog
          open={notificationDialog}
          title={notificationDialogTitle}
          message={notificationDialogMessage}
          onClose={this.onClickCloseNotificationDialog}
        />
      </div>
    );
  }
}

export default compose(
  withStyles(s),
  withMaterialStyles(s, { withTheme: true }),
)(Login);
