import React from 'react';
import PropTypes from 'prop-types';
import withStyles from 'isomorphic-style-loader/lib/withStyles';
import s from './ManageParentApplications.css';
import ParentApplications from '../../components/Manage/ParentApplications';

class ManageParentApplications extends React.Component {
  render() {
    return (
      <div className={s.root}>
        <h2 className="article-title">[부모 -> 시터] 신청내역</h2>
        <ParentApplications />
      </div>
    );
  }
}

ManageParentApplications.propTypes = {};

ManageParentApplications.defaultProps = {};

export default withStyles(s)(ManageParentApplications);
